package com.mdp.fusionapp.custom.font;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewLatoLigth extends TextView {

    public TextViewLatoLigth(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextViewLatoLigth(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewLatoLigth(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato_Light.ttf");
            setTypeface(tf);
        }
    }
}