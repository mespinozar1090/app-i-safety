package com.mdp.fusionapp.custom.font;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewLatoBold extends TextView {

    public TextViewLatoBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextViewLatoBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewLatoBold(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato_Bold.ttf");
            setTypeface(tf);
        }
    }
}
