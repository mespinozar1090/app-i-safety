package com.mdp.fusionapp.database.service;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.EmpresaMaestraEntity;

@Dao
public interface EmpresaMaestraDao {

    @Insert
    void insert(EmpresaMaestraEntity empresaMaestraEntity);

    @Query("DELETE FROM empresa_maestra_table")
    void deleteAll();

    @Query("SELECT * FROM empresa_maestra_table WHERE idProyecto=:idProyecto")
    EmpresaMaestraEntity findEmpresaMaestraByProyecto(Integer idProyecto);
}
