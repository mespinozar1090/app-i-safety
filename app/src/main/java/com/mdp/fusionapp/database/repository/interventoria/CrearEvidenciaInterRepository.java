package com.mdp.fusionapp.database.repository.interventoria;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.interventoria.CrearEvidenciaInterEntity;
import com.mdp.fusionapp.database.service.interventoria.CrearEvidenciaInterDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class CrearEvidenciaInterRepository {

    private CrearEvidenciaInterDao crearEvidenciaInterDao;

    public CrearEvidenciaInterRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        crearEvidenciaInterDao = database.crearEvidenciaInterDao();
    }

    public Long insert(CrearEvidenciaInterEntity evidenciaInterEntity) {
        try {
            return  new InsertAsyncTask(crearEvidenciaInterDao).execute(evidenciaInterEntity).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteAll() {
        new DeleteAllAsyncTask(crearEvidenciaInterDao).execute();
    }

    public void deleteById(Long id) {
        new DeleteByIdAsyncTask(crearEvidenciaInterDao).execute(id);
    }

    public List<CrearEvidenciaInterEntity> findAll(Long idOffline) {
        try {
            return new GetAllAsyncTask(crearEvidenciaInterDao).execute(idOffline).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void updateImage(String base64, Long idRow) {
        new UpdateImageAsyncTask(crearEvidenciaInterDao, base64, idRow).execute();
    }

    private static class UpdateImageAsyncTask extends AsyncTask<Void, Void, Long> {
        private CrearEvidenciaInterDao crearEvidenciaInterDao;
        private String base64;
        private Long idRow;

        private UpdateImageAsyncTask(CrearEvidenciaInterDao crearEvidenciaInterDao, String base64, Long idRow) {
            this.crearEvidenciaInterDao = crearEvidenciaInterDao;
            this.base64 = base64;
            this.idRow = idRow;
        }

        @Override
        protected Long doInBackground(Void... voids) {
            crearEvidenciaInterDao.updaImageEvidencia(base64, idRow);
            return null;
        }
    }


    private static class GetAllAsyncTask extends AsyncTask<Long, String, List<CrearEvidenciaInterEntity>> {
        private CrearEvidenciaInterDao crearEvidenciaInterDao;

        public GetAllAsyncTask(CrearEvidenciaInterDao crearEvidenciaInterDao) {
            this.crearEvidenciaInterDao = crearEvidenciaInterDao;
        }

        @Override
        protected List<CrearEvidenciaInterEntity> doInBackground(Long... integers) {
            return crearEvidenciaInterDao.findAllById(integers[0]);
        }
    }

    private static class DeleteByIdAsyncTask extends AsyncTask<Long, Void, Void> {
        private CrearEvidenciaInterDao crearEvidenciaInterDao;

        private DeleteByIdAsyncTask(CrearEvidenciaInterDao crearEvidenciaInterDao) {
            this.crearEvidenciaInterDao = crearEvidenciaInterDao;
        }

        @Override
        protected Void doInBackground(Long... entities) {
            crearEvidenciaInterDao.deleteById(entities[0]);
            return null;
        }
    }

    private static class InsertAsyncTask extends AsyncTask<CrearEvidenciaInterEntity, Void, Long> {
        private CrearEvidenciaInterDao crearEvidenciaInterDao;

        private InsertAsyncTask(CrearEvidenciaInterDao crearEvidenciaInterDao) {
            this.crearEvidenciaInterDao = crearEvidenciaInterDao;
        }

        @Override
        protected Long doInBackground(CrearEvidenciaInterEntity... entities) {
            return crearEvidenciaInterDao.insert(entities[0]);
        }
    }

    private static class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        private CrearEvidenciaInterDao crearEvidenciaInterDao;

        private DeleteAllAsyncTask(CrearEvidenciaInterDao crearEvidenciaInterDao) {
            this.crearEvidenciaInterDao = crearEvidenciaInterDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            crearEvidenciaInterDao.deleteAll();
            return null;
        }
    }
}
