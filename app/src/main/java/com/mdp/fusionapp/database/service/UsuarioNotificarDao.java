package com.mdp.fusionapp.database.service;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.mdp.fusionapp.database.entity.UsuarioEntity;
import com.mdp.fusionapp.database.entity.UsuarioNotificarEntity;

import java.util.List;

@Dao
public interface UsuarioNotificarDao {

    @Insert
    void insert(UsuarioNotificarEntity usuarioNotificarEntity);

    @Query("DELETE FROM usuario_noticadores")
    void deleteAll();

    @Query("SELECT * FROM usuario_noticadores")
    List<UsuarioNotificarEntity> findAll();
}
