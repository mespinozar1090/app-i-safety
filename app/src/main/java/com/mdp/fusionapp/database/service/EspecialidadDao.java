package com.mdp.fusionapp.database.service;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.ContratistaEntity;
import com.mdp.fusionapp.database.entity.CrearInspeccionEntity;
import com.mdp.fusionapp.database.entity.EspecialidadEntity;

import java.util.List;

@Dao
public interface EspecialidadDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(EspecialidadEntity especialidadEntity);

    @Query("DELETE FROM especialidad_table")
    void deleteAll();

    @Query("SELECT * FROM especialidad_table")
    List<EspecialidadEntity> findAll();
}
