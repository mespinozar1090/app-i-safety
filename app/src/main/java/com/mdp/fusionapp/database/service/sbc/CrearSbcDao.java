package com.mdp.fusionapp.database.service.sbc;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.sbc.CrearSbcEntity;

import java.util.List;

@Dao
public interface CrearSbcDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(CrearSbcEntity crearSbcEntity);

    @Query("DELETE FROM crear_sbc_table WHERE id=:id")
    void deleteById(Integer id);

    @Query("DELETE FROM crear_sbc_table")
    void deleteAll();

    @Query("SELECT * FROM crear_sbc_table")
    List<CrearSbcEntity> findAll();

    @Query("UPDATE  crear_sbc_table SET estadoRegistroDB = 3 WHERE id=:id  ")
    void updateEstadoEliminar(Integer id);

    @Query("SELECT * FROM crear_sbc_table  where estadoRegistroDB == 2 OR estadoRegistroDB == 1 OR estadoRegistroDB == 3")
    List<CrearSbcEntity> findAllByEstado();

    @Query("SELECT * FROM crear_sbc_table WHERE id=:id")
    CrearSbcEntity findAllById(Long id);
}
