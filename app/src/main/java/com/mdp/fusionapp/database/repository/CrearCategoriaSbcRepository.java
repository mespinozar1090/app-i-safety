package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.sbc.CrearCategoriaSbcEntity;
import com.mdp.fusionapp.database.service.sbc.CrearCategoriaSbcDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class CrearCategoriaSbcRepository {

    private CrearCategoriaSbcDao crearCategoriaSbcDao;

    public CrearCategoriaSbcRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        crearCategoriaSbcDao = database.crearCategoriaSbcDao();
    }

    public void insert(CrearCategoriaSbcEntity crearCategoriaSbcEntity) {
        new InsertAsyncTask(crearCategoriaSbcDao).execute(crearCategoriaSbcEntity);
    }

    public void deleteAll(CrearCategoriaSbcEntity crearCategoriaSbcEntity) {
        new DeleteAllAsyncTask(crearCategoriaSbcDao).execute(crearCategoriaSbcEntity);
    }

    public void deleteById(Long id) {
        new DeleteByIdAsyncTask(crearCategoriaSbcDao).execute(id);
    }


    public List<CrearCategoriaSbcEntity> findAll(Long idOffline) {
        try {
            return new GetAllAsyncTask(crearCategoriaSbcDao).execute(idOffline).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


    private static class InsertAsyncTask extends AsyncTask<CrearCategoriaSbcEntity, Void, Void> {
        private CrearCategoriaSbcDao crearCategoriaSbcDao;

        private InsertAsyncTask(CrearCategoriaSbcDao crearCategoriaSbcDao) {
            this.crearCategoriaSbcDao = crearCategoriaSbcDao;
        }

        @Override
        protected Void doInBackground(CrearCategoriaSbcEntity... entities) {
            crearCategoriaSbcDao.insert(entities[0]);
            return null;
        }
    }


    private static class DeleteAllAsyncTask extends AsyncTask<CrearCategoriaSbcEntity, Void, Void> {
        private CrearCategoriaSbcDao crearCategoriaSbcDao;

        private DeleteAllAsyncTask(CrearCategoriaSbcDao crearCategoriaSbcDao) {
            this.crearCategoriaSbcDao = crearCategoriaSbcDao;
        }

        @Override
        protected Void doInBackground(CrearCategoriaSbcEntity... entities) {
            crearCategoriaSbcDao.delete(entities[0]);
            return null;
        }
    }

    private static class GetAllAsyncTask extends AsyncTask<Long, String, List<CrearCategoriaSbcEntity>> {
        private CrearCategoriaSbcDao crearCategoriaSbcDao;

        public GetAllAsyncTask(CrearCategoriaSbcDao crearCategoriaSbcDao) {
            this.crearCategoriaSbcDao = crearCategoriaSbcDao;

        }

        @Override
        protected List<CrearCategoriaSbcEntity> doInBackground(Long... integers) {
            return crearCategoriaSbcDao.findAllById(integers[0]);
        }
    }

    private static class DeleteByIdAsyncTask extends AsyncTask<Long, Void, Void> {
        private CrearCategoriaSbcDao crearCategoriaSbcDao;

        private DeleteByIdAsyncTask(CrearCategoriaSbcDao crearCategoriaSbcDao) {
            this.crearCategoriaSbcDao = crearCategoriaSbcDao;
        }

        @Override
        protected Void doInBackground(Long... entities) {
            crearCategoriaSbcDao.deleteById(entities[0]);
            return null;
        }
    }
}
