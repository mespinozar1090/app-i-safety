package com.mdp.fusionapp.database.repository.incidente;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.incidente.TipoEventoEntity;
import com.mdp.fusionapp.database.service.incidente.IncidenteEmpresaDao;
import com.mdp.fusionapp.database.service.incidente.TipoEventoDao;
import com.mdp.fusionapp.network.response.InspeccionEmpresaEntity;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class TipoEventoRepository {

    private TipoEventoDao tipoEventoDao;

    public TipoEventoRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        this.tipoEventoDao = database.tipoEventoDao();
    }

    public void insert(TipoEventoEntity tipoEventoEntity) {
        new InsertAsyncTask(tipoEventoDao).execute(tipoEventoEntity);
    }

    public void deleteAll() {
        new DeleteAsyncTask(tipoEventoDao).execute();
    }

    public List<TipoEventoEntity> findAll() {
        try {
            return new FindAllAsyncTask(tipoEventoDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Select
    private static class FindAllAsyncTask extends AsyncTask<Void, Void, List<TipoEventoEntity>> {
        private TipoEventoDao tipoEventoDao;

        private FindAllAsyncTask(TipoEventoDao tipoEventoDao) {
            this.tipoEventoDao = tipoEventoDao;
        }

        @Override
        protected List<TipoEventoEntity> doInBackground(Void... voids) {
            return tipoEventoDao.findAll();
        }
    }


    //Insert
    private static class InsertAsyncTask extends AsyncTask<TipoEventoEntity, Void, Void> {
        private TipoEventoDao tipoEventoDao;

        private InsertAsyncTask(TipoEventoDao tipoEventoDao) {
            this.tipoEventoDao = tipoEventoDao;
        }

        @Override
        protected Void doInBackground(TipoEventoEntity... entities) {
            tipoEventoDao.insert(entities[0]);
            return null;
        }
    }

    //Delete
    private static class DeleteAsyncTask extends AsyncTask<Void, Void, Void> {
        private TipoEventoDao tipoEventoDao;

        private DeleteAsyncTask(TipoEventoDao tipoEventoDao) {
            this.tipoEventoDao = tipoEventoDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            tipoEventoDao.deleteAll();
            return null;
        }
    }
}
