package com.mdp.fusionapp.database.entity;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "proyecto_table")
public class ProyectoEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("idProyecto")
    private Integer idProyecto;

    @SerializedName("Codigo")
    private String codigo;

    @SerializedName("Nombre")
    private String nombre;

    @SerializedName("Id_Empresa_Maestra")
    private Integer idEmpresaMaestra;

    @SerializedName("Id_Rol_General")
    private Integer idRolGeneral;

    @SerializedName("Id_Rol_Usuario")
    private Integer idRolUsuario;

    @SerializedName("Id_Usuario")
    private Integer idUsuario;

    @SerializedName("Modulo")
    private String modulo;

    public ProyectoEntity(Integer idProyecto, String nombre) {
        this.idProyecto = idProyecto;
        this.nombre = nombre;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getIdEmpresaMaestra() {
        return idEmpresaMaestra;
    }

    public void setIdEmpresaMaestra(Integer idEmpresaMaestra) {
        this.idEmpresaMaestra = idEmpresaMaestra;
    }

    public Integer getIdRolGeneral() {
        return idRolGeneral;
    }

    public void setIdRolGeneral(Integer idRolGeneral) {
        this.idRolGeneral = idRolGeneral;
    }

    public Integer getIdRolUsuario() {
        return idRolUsuario;
    }

    public void setIdRolUsuario(Integer idRolUsuario) {
        this.idRolUsuario = idRolUsuario;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
