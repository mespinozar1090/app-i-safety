package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.service.CategoriaSbcItemDao;
import com.mdp.fusionapp.database.entity.CategoriaSbcItemEntity;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class CategoriaSbcItemRepository {

    private CategoriaSbcItemDao categoriaSbcItemDao;

    public CategoriaSbcItemRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        categoriaSbcItemDao = database.categoriaSbcItemDao();
    }

    public void insert(CategoriaSbcItemEntity entity) {
        new InsertAsyncTask(categoriaSbcItemDao).execute(entity);
    }

    public void deleteAll() {
        new DeleteAlltAsyncTask(categoriaSbcItemDao).execute();
    }

    public List<CategoriaSbcItemEntity> findAll() {
        try {
            return new GetAllAsyncTask(categoriaSbcItemDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


    private static class InsertAsyncTask extends AsyncTask<CategoriaSbcItemEntity, Void, Void> {
        private CategoriaSbcItemDao categoriaSbcItemDao;

        private InsertAsyncTask(CategoriaSbcItemDao categoriaSbcItemDao) {
            this.categoriaSbcItemDao = categoriaSbcItemDao;
        }

        @Override
        protected Void doInBackground(CategoriaSbcItemEntity... tickets) {
            categoriaSbcItemDao.insert(tickets[0]);
            return null;
        }
    }

    private static class DeleteAlltAsyncTask extends AsyncTask<Void, Void, Void> {
        private CategoriaSbcItemDao categoriaSbcItemDao;

        private DeleteAlltAsyncTask(CategoriaSbcItemDao categoriaSbcItemDao) {
            this.categoriaSbcItemDao = categoriaSbcItemDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            categoriaSbcItemDao.deleteAll();
            return null;
        }
    }

    private static class GetAllAsyncTask extends AsyncTask<Integer, String, List<CategoriaSbcItemEntity>> {
        private CategoriaSbcItemDao categoriaSbcItemDao;

        public GetAllAsyncTask(CategoriaSbcItemDao categoriaSbcItemDao) {
            this.categoriaSbcItemDao = categoriaSbcItemDao;
        }

        @Override
        protected List<CategoriaSbcItemEntity> doInBackground(Integer... integers) {
            return categoriaSbcItemDao.findAll();
        }
    }


}
