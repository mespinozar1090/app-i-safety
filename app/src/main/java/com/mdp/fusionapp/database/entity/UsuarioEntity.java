package com.mdp.fusionapp.database.entity;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "usuario_table")
public class UsuarioEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private Integer idUsers;
    private String usuario;
    private String clave;
    private Integer idPerfilUsuario;
    private String nombreUsuario;
    private Integer idRazonSocial;
    private String descripcionEmpresa;
    private String email;
    private String ruc;
    private Integer idEmpresa;
    private String descripcionRol;
    private String descripcionRolUsuario;
    private Integer idRolUsuario;
    private Integer idRolGeneral;

  /*  @Ignore
    public UsuarioEntity() {
    }*/

    public Integer getIdUsers() {
        return idUsers;
    }

    public void setIdUsers(Integer idUsers) {
        this.idUsers = idUsers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public Integer getIdPerfilUsuario() {
        return idPerfilUsuario;
    }

    public void setIdPerfilUsuario(Integer idPerfilUsuario) {
        this.idPerfilUsuario = idPerfilUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public Integer getIdRazonSocial() {
        return idRazonSocial;
    }

    public void setIdRazonSocial(Integer idRazonSocial) {
        this.idRazonSocial = idRazonSocial;
    }

    public String getDescripcionEmpresa() {
        return descripcionEmpresa;
    }

    public void setDescripcionEmpresa(String descripcionEmpresa) {
        this.descripcionEmpresa = descripcionEmpresa;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getDescripcionRol() {
        return descripcionRol;
    }

    public void setDescripcionRol(String descripcionRol) {
        this.descripcionRol = descripcionRol;
    }

    public String getDescripcionRolUsuario() {
        return descripcionRolUsuario;
    }

    public void setDescripcionRolUsuario(String descripcionRolUsuario) {
        this.descripcionRolUsuario = descripcionRolUsuario;
    }

    public Integer getIdRolUsuario() {
        return idRolUsuario;
    }

    public void setIdRolUsuario(Integer idRolUsuario) {
        this.idRolUsuario = idRolUsuario;
    }

    public Integer getIdRolGeneral() {
        return idRolGeneral;
    }

    public void setIdRolGeneral(Integer idRolGeneral) {
        this.idRolGeneral = idRolGeneral;
    }

    @Override
    public String toString() {
        return "UsuarioEntity{" +
                "id=" + id +
                ", idUsers=" + idUsers +
                ", usuario='" + usuario + '\'' +
                ", clave='" + clave + '\'' +
                ", idPerfilUsuario=" + idPerfilUsuario +
                ", nombreUsuario='" + nombreUsuario + '\'' +
                ", idRazonSocial=" + idRazonSocial +
                ", descripcionEmpresa='" + descripcionEmpresa + '\'' +
                ", email='" + email + '\'' +
                ", ruc='" + ruc + '\'' +
                ", idEmpresa=" + idEmpresa +
                ", descripcionRol='" + descripcionRol + '\'' +
                ", descripcionRolUsuario='" + descripcionRolUsuario + '\'' +
                ", idRolUsuario=" + idRolUsuario +
                ", idRolGeneral=" + idRolGeneral +
                '}';
    }
}
