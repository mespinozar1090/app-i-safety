package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.sbc.NotificarEntity;
import com.mdp.fusionapp.database.service.NotificarDao;
import com.mdp.fusionapp.database.service.sbc.CrearSbcDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class NotificarRepository {

    private NotificarDao notificarDao;

    public NotificarRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        this.notificarDao = database.notificarDao();
    }

    public void insert(NotificarEntity entity) {
        new InsertAsyncTask(notificarDao).execute(entity);
    }

    public void deleteById(Integer modulo, Long idOffline) {
        new DeleteByIdOfflineAsyncTask(notificarDao, modulo, idOffline).execute();
    }

    public List<NotificarEntity> findAllByModulo(Integer idModulo, Long idOffline) {
        try {
            return new GetAllByModuloAsyncTask(notificarDao, idModulo, idOffline).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Delete by idOffline
    private static class DeleteByIdOfflineAsyncTask extends AsyncTask<Integer, Void, Void> {
        private NotificarDao notificarDao;
        private Integer modulo;
        private Long idOffline;

        private DeleteByIdOfflineAsyncTask(NotificarDao notificarDao, Integer modulo, Long idOffline) {
            this.notificarDao = notificarDao;
            this.modulo = modulo;
            this.idOffline = idOffline;
        }


        @Override
        protected Void doInBackground(Integer... integers) {
            notificarDao.deleteByIds(modulo, idOffline);
            return null;
        }
    }

    //Insert
    private static class InsertAsyncTask extends AsyncTask<NotificarEntity, Void, Void> {
        private NotificarDao notificarDao;

        private InsertAsyncTask(NotificarDao notificarDao) {
            this.notificarDao = notificarDao;
        }

        @Override
        protected Void doInBackground(NotificarEntity... entities) {
            notificarDao.insert(entities[0]);
            return null;
        }
    }

    private static class GetAllByModuloAsyncTask extends AsyncTask<Void, String, List<NotificarEntity>> {
        private NotificarDao notificarDao;
        private Integer idModulo;
        private Long idOffline;

        public GetAllByModuloAsyncTask(NotificarDao notificarDao, Integer idModulo, Long idOffline) {
            this.notificarDao = notificarDao;
            this.idModulo = idModulo;
            this.idOffline = idOffline;
        }

        @Override
        protected List<NotificarEntity> doInBackground(Void... voids) {
            return notificarDao.findAllById(idModulo, idOffline);
        }
    }

}
