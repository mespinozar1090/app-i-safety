package com.mdp.fusionapp.database.service.incidente;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.incidente.ParteCuerpoEntity;

import java.util.List;

@Dao
public interface ParteCuerpoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(ParteCuerpoEntity equipoEntity);

    @Query("DELETE FROM parte_cuerpo_table")
    void deleteAll();

    @Query("SELECT * FROM parte_cuerpo_table")
    List<ParteCuerpoEntity> findAll();
}
