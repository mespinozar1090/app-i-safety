package com.mdp.fusionapp.database.entity.sbc;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "crear_sbc_table")
public class CrearSbcEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private Integer idSbc;
    private String nombreObservador;
    private Integer idSedeProyecto;
    private String cargoObservador;
    private String lugarTrabajo;
    private String fechaRegistroSbc;
    private Integer idEmpresaObservadora;
    private Integer horarioObservacion;
    private Integer especialidaObservado;
    private String actividadObservado;
    private Integer idAreaTrabajo;
    private String fechaRegristo;
    private String descripcionAreaObservada;
    private Integer idSedeProyectoObservado;
    private Integer idUsuario;
    private String nombreFormato;
    private Integer estadoOffline;
    private Integer tiempoExpObservada;
    private Integer estadoRegistroDB = 0;

    public Integer getEstadoOffline() {
        return estadoOffline;
    }

    public void setEstadoOffline(Integer estadoOffline) {
        this.estadoOffline = estadoOffline;
    }

    public Integer getTiempoExpObservada() {
        return tiempoExpObservada;
    }

    public void setTiempoExpObservada(Integer tiempoExpObservada) {
        this.tiempoExpObservada = tiempoExpObservada;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdSbc() {
        return idSbc;
    }

    public void setIdSbc(Integer idSbc) {
        this.idSbc = idSbc;
    }

    public String getNombreObservador() {
        return nombreObservador;
    }

    public void setNombreObservador(String nombreObservador) {
        this.nombreObservador = nombreObservador;
    }

    public Integer getIdSedeProyecto() {
        return idSedeProyecto;
    }

    public void setIdSedeProyecto(Integer idSedeProyecto) {
        this.idSedeProyecto = idSedeProyecto;
    }

    public String getCargoObservador() {
        return cargoObservador;
    }

    public void setCargoObservador(String cargoObservador) {
        this.cargoObservador = cargoObservador;
    }

    public String getLugarTrabajo() {
        return lugarTrabajo;
    }

    public void setLugarTrabajo(String lugarTrabajo) {
        this.lugarTrabajo = lugarTrabajo;
    }

    public String getFechaRegistroSbc() {
        return fechaRegistroSbc;
    }

    public void setFechaRegistroSbc(String fechaRegistroSbc) {
        this.fechaRegistroSbc = fechaRegistroSbc;
    }

    public Integer getIdEmpresaObservadora() {
        return idEmpresaObservadora;
    }

    public void setIdEmpresaObservadora(Integer idEmpresaObservadora) {
        this.idEmpresaObservadora = idEmpresaObservadora;
    }

    public Integer getHorarioObservacion() {
        return horarioObservacion;
    }

    public void setHorarioObservacion(Integer horarioObservacion) {
        this.horarioObservacion = horarioObservacion;
    }

    public Integer getEspecialidaObservado() {
        return especialidaObservado;
    }

    public void setEspecialidaObservado(Integer especialidaObservado) {
        this.especialidaObservado = especialidaObservado;
    }

    public String getActividadObservado() {
        return actividadObservado;
    }

    public void setActividadObservado(String actividadObservado) {
        this.actividadObservado = actividadObservado;
    }

    public Integer getIdAreaTrabajo() {
        return idAreaTrabajo;
    }

    public void setIdAreaTrabajo(Integer idAreaTrabajo) {
        this.idAreaTrabajo = idAreaTrabajo;
    }

    public String getFechaRegristo() {
        return fechaRegristo;
    }

    public void setFechaRegristo(String fechaRegristo) {
        this.fechaRegristo = fechaRegristo;
    }

    public String getDescripcionAreaObservada() {
        return descripcionAreaObservada;
    }

    public void setDescripcionAreaObservada(String descripcionAreaObservada) {
        this.descripcionAreaObservada = descripcionAreaObservada;
    }

    public Integer getIdSedeProyectoObservado() {
        return idSedeProyectoObservado;
    }

    public void setIdSedeProyectoObservado(Integer idSedeProyectoObservado) {
        this.idSedeProyectoObservado = idSedeProyectoObservado;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombreFormato() {
        return nombreFormato;
    }

    public void setNombreFormato(String nombreFormato) {
        this.nombreFormato = nombreFormato;
    }

    public Integer getEstadoRegistroDB() {
        return estadoRegistroDB;
    }

    public void setEstadoRegistroDB(Integer estadoRegistroDB) {
        this.estadoRegistroDB = estadoRegistroDB;
    }
}
