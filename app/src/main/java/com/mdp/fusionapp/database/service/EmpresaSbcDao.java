package com.mdp.fusionapp.database.service;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.EmpresaSbcEntity;

import java.util.List;

@Dao
public interface EmpresaSbcDao {

    @Insert
    void insert(EmpresaSbcEntity empresaSbcEntity);

    @Query("DELETE FROM empresa_sbc_table")
    void deleteAll();

    @Query("SELECT * FROM empresa_sbc_table")
    List<EmpresaSbcEntity> findAll();
}
