package com.mdp.fusionapp.database.service.incidente;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mdp.fusionapp.network.response.ComboMaestroEntity;

import java.util.List;

@Dao
public interface ComboMaestroDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(ComboMaestroEntity comboMaestroEntity);

    @Query("DELETE FROM incidente_combo_maestro_table")
    void deleteAll();

    @Query("SELECT * FROM incidente_combo_maestro_table")
    List<ComboMaestroEntity> findAll();
}
