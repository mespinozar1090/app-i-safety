package com.mdp.fusionapp.database.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "crear_inspeccion_table")
public class CrearInspeccionEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private Integer idInspeccion;
    private Integer idProyecto;
    private Integer idEmpresacontratista;
    private Integer idTipoUbicacion;
    private Integer idLugar;
    private String torre;
    private Integer idEmpresaObservadora;
    private String ruc;
    private Integer idActividadEconomica;
    private String domicilioLegal;
    private Integer nroTrabajadores;
    private Integer tipoInspeccion;
    private String fechaHoraIncidenteCreate;
    private String responsableInspeccion;
    private String areaInspeccionada;
    private String responsableAreaInspeccion;
    private String fechaHoraInspeccion;
    private Integer estadoInspeccion;
    private Integer idUsuario;
    private String nombreFormato;
    private Integer idUsuarioRegistro;
    private String sfechaHoraInspeccion;
    private String nombreUsuario;
    private String nombreSede;
    private String nombreContratista;
    private Integer estadoRegistroDB = 0;

    public Integer getIdInspeccion() {
        return idInspeccion;
    }

    public void setIdInspeccion(Integer idInspeccion) {
        this.idInspeccion = idInspeccion;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getNombreSede() {
        return nombreSede;
    }

    public void setNombreSede(String nombreSede) {
        this.nombreSede = nombreSede;
    }

    public String getNombreContratista() {
        return nombreContratista;
    }

    public void setNombreContratista(String nombreContratista) {
        this.nombreContratista = nombreContratista;
    }

    public String getSfechaHoraInspeccion() {
        return sfechaHoraInspeccion;
    }

    public void setSfechaHoraInspeccion(String sfechaHoraInspeccion) {
        this.sfechaHoraInspeccion = sfechaHoraInspeccion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getEstadoInspeccion() {
        return estadoInspeccion;
    }

    public void setEstadoInspeccion(Integer estadoInspeccion) {
        this.estadoInspeccion = estadoInspeccion;
    }

    public Integer getIdEmpresaObservadora() {
        return idEmpresaObservadora;
    }

    public void setIdEmpresaObservadora(Integer idEmpresaObservadora) {
        this.idEmpresaObservadora = idEmpresaObservadora;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public Integer getIdEmpresacontratista() {
        return idEmpresacontratista;
    }

    public void setIdEmpresacontratista(Integer idEmpresacontratista) {
        this.idEmpresacontratista = idEmpresacontratista;
    }

    public Integer getIdTipoUbicacion() {
        return idTipoUbicacion;
    }

    public void setIdTipoUbicacion(Integer idTipoUbicacion) {
        this.idTipoUbicacion = idTipoUbicacion;
    }

    public Integer getIdLugar() {
        return idLugar;
    }

    public void setIdLugar(Integer idLugar) {
        this.idLugar = idLugar;
    }

    public String getTorre() {
        return torre;
    }

    public void setTorre(String torre) {
        this.torre = torre;
    }

    public Integer getIdActividadEconomica() {
        return idActividadEconomica;
    }

    public void setIdActividadEconomica(Integer idActividadEconomica) {
        this.idActividadEconomica = idActividadEconomica;
    }

    public String getDomicilioLegal() {
        return domicilioLegal;
    }

    public void setDomicilioLegal(String domicilioLegal) {
        this.domicilioLegal = domicilioLegal;
    }

    public Integer getNroTrabajadores() {
        return nroTrabajadores;
    }

    public void setNroTrabajadores(Integer nroTrabajadores) {
        this.nroTrabajadores = nroTrabajadores;
    }

    public Integer getTipoInspeccion() {
        return tipoInspeccion;
    }

    public void setTipoInspeccion(Integer tipoInspeccion) {
        this.tipoInspeccion = tipoInspeccion;
    }

    public String getFechaHoraIncidenteCreate() {
        return fechaHoraIncidenteCreate;
    }

    public void setFechaHoraIncidenteCreate(String fechaHoraIncidenteCreate) {
        this.fechaHoraIncidenteCreate = fechaHoraIncidenteCreate;
    }

    public String getResponsableInspeccion() {
        return responsableInspeccion;
    }

    public void setResponsableInspeccion(String responsableInspeccion) {
        this.responsableInspeccion = responsableInspeccion;
    }

    public String getAreaInspeccionada() {
        return areaInspeccionada;
    }

    public void setAreaInspeccionada(String areaInspeccionada) {
        this.areaInspeccionada = areaInspeccionada;
    }

    public String getResponsableAreaInspeccion() {
        return responsableAreaInspeccion;
    }

    public void setResponsableAreaInspeccion(String responsableAreaInspeccion) {
        this.responsableAreaInspeccion = responsableAreaInspeccion;
    }

    public String getFechaHoraInspeccion() {
        return fechaHoraInspeccion;
    }

    public void setFechaHoraInspeccion(String fechaHoraInspeccion) {
        this.fechaHoraInspeccion = fechaHoraInspeccion;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombreFormato() {
        return nombreFormato;
    }

    public void setNombreFormato(String nombreFormato) {
        this.nombreFormato = nombreFormato;
    }


    public Integer getIdUsuarioRegistro() {
        return idUsuarioRegistro;
    }

    public void setIdUsuarioRegistro(Integer idUsuarioRegistro) {
        this.idUsuarioRegistro = idUsuarioRegistro;
    }

    public Integer getEstadoRegistroDB() {
        return estadoRegistroDB;
    }

    public void setEstadoRegistroDB(Integer estadoRegistroDB) {
        this.estadoRegistroDB = estadoRegistroDB;
    }
}
