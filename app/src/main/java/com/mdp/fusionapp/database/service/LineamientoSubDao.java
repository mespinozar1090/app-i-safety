package com.mdp.fusionapp.database.service;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.LineamientoSubEntity;

import java.util.List;

@Dao
public interface LineamientoSubDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(LineamientoSubEntity lineamientoSubEntity);

    @Query("DELETE FROM lineamiento_sub_table")
    void deleteAll();

    @Query("SELECT * FROM lineamiento_sub_table")
    List<LineamientoSubEntity> findAll();
}
