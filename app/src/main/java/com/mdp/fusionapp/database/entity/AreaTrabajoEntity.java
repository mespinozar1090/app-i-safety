package com.mdp.fusionapp.database.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "area_trabajo_table")
public class AreaTrabajoEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("Id_Area_inspeccionada")
    private Integer idAreainspeccionada;

    @SerializedName("Descripcion")
    private String descripcion;

    @SerializedName("Estado")
    private Boolean estado;

    public AreaTrabajoEntity(Integer idAreainspeccionada, String descripcion, Boolean estado) {
        this.idAreainspeccionada = idAreainspeccionada;
        this.descripcion = descripcion;
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdAreainspeccionada() {
        return idAreainspeccionada;
    }

    public void setIdAreainspeccionada(Integer idAreainspeccionada) {
        this.idAreainspeccionada = idAreainspeccionada;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return descripcion;
    }
}
