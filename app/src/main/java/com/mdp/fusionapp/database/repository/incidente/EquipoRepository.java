package com.mdp.fusionapp.database.repository.incidente;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.incidente.EquipoEntity;
import com.mdp.fusionapp.database.service.incidente.ActividadDao;
import com.mdp.fusionapp.database.service.incidente.EquipoDao;
import com.mdp.fusionapp.network.response.ActividadEntity;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class EquipoRepository {

    private EquipoDao equipoDao;

    public EquipoRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        this.equipoDao = database.equipoDao();
    }

    public void insert(EquipoEntity equipoEntity) {
        new InsertAsyncTask(equipoDao).execute(equipoEntity);
    }

    public void deleteAll() {
        new DeleteAsyncTask(equipoDao).execute();
    }

    public List<EquipoEntity> findAll() {
        try {
            return new FindAllAsyncTask(equipoDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Select
    private static class FindAllAsyncTask extends AsyncTask<Void, Void, List<EquipoEntity>> {
        private EquipoDao equipoDao;

        private FindAllAsyncTask(EquipoDao equipoDao) {
            this.equipoDao = equipoDao;
        }

        @Override
        protected List<EquipoEntity> doInBackground(Void... voids) {
            return equipoDao.findAll();
        }
    }

    //Insert
    private static class InsertAsyncTask extends AsyncTask<EquipoEntity, Void, Void> {
        private EquipoDao equipoDao;

        private InsertAsyncTask(EquipoDao equipoDao) {
            this.equipoDao = equipoDao;
        }

        @Override
        protected Void doInBackground(EquipoEntity... entities) {
            equipoDao.insert(entities[0]);
            return null;
        }
    }

    //Delete
    private static class DeleteAsyncTask extends AsyncTask<Void, Void, Void> {
        private EquipoDao equipoDao;

        private DeleteAsyncTask(EquipoDao equipoDao) {
            this.equipoDao = equipoDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            equipoDao.deleteAll();
            return null;
        }
    }


}
