package com.mdp.fusionapp.database.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "categoria_buenapractica_table")
public class CategoriaBuenaPracticaEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("idCategoriaBuenaPractica")
    private Integer idCategoriaBuenaPractica;

    @SerializedName("Nombre")
    private String nombre;

    @SerializedName("nota")
    private String nota;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdCategoriaBuenaPractica() {
        return idCategoriaBuenaPractica;
    }

    public void setIdCategoriaBuenaPractica(Integer idCategoriaBuenaPractica) {
        this.idCategoriaBuenaPractica = idCategoriaBuenaPractica;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
