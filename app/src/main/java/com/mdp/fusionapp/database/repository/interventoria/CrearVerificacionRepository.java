package com.mdp.fusionapp.database.repository.interventoria;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.interventoria.CrearVerificacionEntity;
import com.mdp.fusionapp.database.service.interventoria.CrearVerificacionDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class CrearVerificacionRepository {

    private CrearVerificacionDao crearVerificacionDao;

    public CrearVerificacionRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        crearVerificacionDao = database.crearVerificacionDao();
    }

    public void insert(CrearVerificacionEntity crearVerificacionEntity) {
        new InsertAsyncTask(crearVerificacionDao).execute(crearVerificacionEntity);
    }

    public void deleteAll() {
        new DeleteAllAsyncTask(crearVerificacionDao).execute();
    }

    public void deleteById(Long id) {
        new DeleteByIdAsyncTask(crearVerificacionDao).execute(id);
    }

    public List<CrearVerificacionEntity> findAll(Long idOffline) {
        try {
            return new GetAllAsyncTask(crearVerificacionDao).execute(idOffline).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static class GetAllAsyncTask extends AsyncTask<Long, String, List<CrearVerificacionEntity>> {
        private CrearVerificacionDao crearVerificacionDao;

        public GetAllAsyncTask(CrearVerificacionDao crearVerificacionDao) {
            this.crearVerificacionDao = crearVerificacionDao;

        }

        @Override
        protected List<CrearVerificacionEntity> doInBackground(Long... integers) {
            return crearVerificacionDao.findAllById(integers[0]);
        }
    }

    private static class InsertAsyncTask extends AsyncTask<CrearVerificacionEntity, Void, Void> {
        private CrearVerificacionDao crearVerificacionDao;

        private InsertAsyncTask(CrearVerificacionDao crearVerificacionDao) {
            this.crearVerificacionDao = crearVerificacionDao;
        }

        @Override
        protected Void doInBackground(CrearVerificacionEntity... entities) {
            crearVerificacionDao.insert(entities[0]);
            return null;
        }
    }

    private static class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        private CrearVerificacionDao crearVerificacionDao;

        private DeleteAllAsyncTask(CrearVerificacionDao crearVerificacionDao) {
            this.crearVerificacionDao = crearVerificacionDao;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            crearVerificacionDao.deleteAll();
            return null;
        }
    }

    private static class DeleteByIdAsyncTask extends AsyncTask<Long, Void, Void> {
        private CrearVerificacionDao crearVerificacionDao;

        private DeleteByIdAsyncTask(CrearVerificacionDao crearVerificacionDao) {
            this.crearVerificacionDao = crearVerificacionDao;
        }

        @Override
        protected Void doInBackground(Long... entities) {
            crearVerificacionDao.deleteById(entities[0]);
            return null;
        }
    }

}
