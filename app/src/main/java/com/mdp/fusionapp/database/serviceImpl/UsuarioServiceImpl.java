package com.mdp.fusionapp.database.serviceImpl;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.mdp.fusionapp.database.entity.UsuarioEntity;
import com.mdp.fusionapp.database.repository.UsuarioRepository;

import java.util.concurrent.ExecutionException;

public class UsuarioServiceImpl extends AndroidViewModel {
    private UsuarioRepository repository;

    public UsuarioServiceImpl(@NonNull Application application) {
        super(application);
        repository = new UsuarioRepository(application);
    }

    public void insert(UsuarioEntity user){
        repository.insert(user);
    }

    public void update(UsuarioEntity user){
        repository.update(user);
    }

    public void delete(UsuarioEntity user){
        repository.delete(user);
    }

    public LiveData<UsuarioEntity> getByUserAndPassword(String user, String clave){
        return repository.getUserAndPassword(user, clave);
    }

    public LiveData<UsuarioEntity> getByUser(String user){
        return repository.getByUser(user);
    }

    public UsuarioEntity getByUserName(String user)  {
        try {
            return repository.getUserByUserName(user);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return  null;
    }

}
