package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.auditoria.CrearDetalleAudotiraEntity;
import com.mdp.fusionapp.database.service.CrearDetalleAuditoriaDao;
import com.mdp.fusionapp.database.service.CrearHallazgoDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class CrearDetalleAuditoriaRepository {

    private CrearDetalleAuditoriaDao crearDetalleAuditoriaDao;

    public CrearDetalleAuditoriaRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        this.crearDetalleAuditoriaDao = database.crearDetalleAuditoriaDao();
    }

    public Long insert(CrearDetalleAudotiraEntity crearDetalleAudotiraEntity) {
        try {
            return new InsertAsyncTask(crearDetalleAuditoriaDao).execute(crearDetalleAudotiraEntity).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteByIdAuditoria(Long id) {
        new DeleteByIdAuditoriaAsyncTask(crearDetalleAuditoriaDao).execute(id);
    }

    public void deleteAll() {
        new DeleteAllAsyncTask(crearDetalleAuditoriaDao).execute();
    }

    public List<CrearDetalleAudotiraEntity> findByIdOffline(Long idInspeccionOffline) {
        try {
            return new GetByIdOfflineAsyncTask(crearDetalleAuditoriaDao).execute(idInspeccionOffline).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void updateImage(String base64, Long idRow) {
        new UpdateImageAsyncTask(crearDetalleAuditoriaDao, base64, idRow).execute();
    }

    //Insert
    private static class InsertAsyncTask extends AsyncTask<CrearDetalleAudotiraEntity, Void, Long> {
        private CrearDetalleAuditoriaDao crearDetalleAuditoriaDao;

        private InsertAsyncTask(CrearDetalleAuditoriaDao crearDetalleAuditoriaDao) {
            this.crearDetalleAuditoriaDao = crearDetalleAuditoriaDao;
        }

        @Override
        protected Long doInBackground(CrearDetalleAudotiraEntity... entities) {
            return crearDetalleAuditoriaDao.insert(entities[0]);
        }
    }

    //Delete
    private static class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        private CrearDetalleAuditoriaDao crearDetalleAuditoriaDao;

        private DeleteAllAsyncTask(CrearDetalleAuditoriaDao crearDetalleAuditoriaDao) {
            this.crearDetalleAuditoriaDao = crearDetalleAuditoriaDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            crearDetalleAuditoriaDao.deleteAll();
            return null;
        }
    }


    //Delete by id
    private static class DeleteByIdAuditoriaAsyncTask extends AsyncTask<Long, Void, Void> {
        private CrearDetalleAuditoriaDao crearDetalleAuditoriaDao;

        private DeleteByIdAuditoriaAsyncTask(CrearDetalleAuditoriaDao crearDetalleAuditoriaDao) {
            this.crearDetalleAuditoriaDao = crearDetalleAuditoriaDao;
        }

        @Override
        protected Void doInBackground(Long... entities) {
            crearDetalleAuditoriaDao.deleteByIdAuditoria(entities[0]);
            return null;
        }
    }

    //select by idOffline
    private static class GetByIdOfflineAsyncTask extends AsyncTask<Long, Void, List<CrearDetalleAudotiraEntity>> {
        private CrearDetalleAuditoriaDao crearDetalleAuditoriaDao;

        private GetByIdOfflineAsyncTask(CrearDetalleAuditoriaDao crearDetalleAuditoriaDao) {
            this.crearDetalleAuditoriaDao = crearDetalleAuditoriaDao;
        }

        @Override
        protected List<CrearDetalleAudotiraEntity> doInBackground(Long... integers) {
            return crearDetalleAuditoriaDao.findByIdAuditoria(integers[0]);
        }
    }

    //Upload image
    private static class UpdateImageAsyncTask extends AsyncTask<Void, Void, Long> {
        private CrearDetalleAuditoriaDao crearDetalleAuditoriaDao;
        private String base64;
        private Long idRow;

        private UpdateImageAsyncTask(CrearDetalleAuditoriaDao crearDetalleAuditoriaDao, String base64, Long idRow) {
            this.crearDetalleAuditoriaDao = crearDetalleAuditoriaDao;
            this.base64 = base64;
            this.idRow = idRow;
        }

        @Override
        protected Long doInBackground(Void... voids) {
            crearDetalleAuditoriaDao.updateImage(base64, idRow);
            return null;
        }
    }
}
