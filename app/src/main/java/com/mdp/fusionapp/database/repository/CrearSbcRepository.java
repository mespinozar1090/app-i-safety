package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.CrearInspeccionEntity;
import com.mdp.fusionapp.database.entity.sbc.CrearSbcEntity;
import com.mdp.fusionapp.database.service.CrearInspeccionDao;
import com.mdp.fusionapp.database.service.sbc.CrearSbcDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class CrearSbcRepository {

    private CrearSbcDao crearSbcDao;

    public CrearSbcRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        crearSbcDao = database.crearSbcDao();
    }


    public void deleteById(Integer id) {
        new DeleteByIdAsyncTask(crearSbcDao).execute(id);
    }

    public void deleteAll() {
        new DeleteAllAsyncTask(crearSbcDao).execute();
    }

    public void updateEstadoEliminar(Integer id) {
        new UpdateEstadoEliminarAllAsyncTask(crearSbcDao).execute(id);
    }

    public Long insert(CrearSbcEntity crearSbcEntity) {
        try {
            return new InsertAsyncTask(crearSbcDao).execute(crearSbcEntity).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public CrearSbcEntity findById(Long id) {
        try {
            return new GetByIdAsyncTask(crearSbcDao).execute(id).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<CrearSbcEntity> findAll() {
        try {
            return new GetAllAsyncTask(crearSbcDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<CrearSbcEntity> findAllByEstado() {
        try {
            return new GetAllByEstadoAsyncTask(crearSbcDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Insert
    private static class InsertAsyncTask extends AsyncTask<CrearSbcEntity, Void, Long> {
        private CrearSbcDao crearSbcDao;

        private InsertAsyncTask(CrearSbcDao crearSbcDao) {
            this.crearSbcDao = crearSbcDao;
        }

        @Override
        protected Long doInBackground(CrearSbcEntity... entities) {
            return crearSbcDao.insert(entities[0]);
        }
    }

    //Delete all
    private static class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        private CrearSbcDao crearSbcDao;

        private DeleteAllAsyncTask(CrearSbcDao crearSbcDao) {
            this.crearSbcDao = crearSbcDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            crearSbcDao.deleteAll();
            return null;
        }
    }

    //Delete
    private static class DeleteByIdAsyncTask extends AsyncTask<Integer, Void, Void> {
        private CrearSbcDao crearSbcDao;

        private DeleteByIdAsyncTask(CrearSbcDao crearSbcDao) {
            this.crearSbcDao = crearSbcDao;
        }


        @Override
        protected Void doInBackground(Integer... integers) {
            crearSbcDao.deleteById(integers[0]);
            return null;
        }
    }

    //Select
    private static class GetByIdAsyncTask extends AsyncTask<Long, Void, CrearSbcEntity> {
        private CrearSbcDao crearSbcDao;

        private GetByIdAsyncTask(CrearSbcDao crearSbcDao) {
            this.crearSbcDao = crearSbcDao;
        }

        @Override
        protected CrearSbcEntity doInBackground(Long... integers) {
            return crearSbcDao.findAllById(integers[0]);
        }
    }

    //Select
    private static class GetAllAsyncTask extends AsyncTask<Void, Void, List<CrearSbcEntity>> {
        private CrearSbcDao crearSbcDao;

        private GetAllAsyncTask(CrearSbcDao crearSbcDao) {
            this.crearSbcDao = crearSbcDao;
        }


        @Override
        protected List<CrearSbcEntity> doInBackground(Void... voids) {
            return crearSbcDao.findAll();
        }
    }

    //Select by estado
    private static class GetAllByEstadoAsyncTask extends AsyncTask<Void, Void, List<CrearSbcEntity>> {
        private CrearSbcDao crearSbcDao;

        private GetAllByEstadoAsyncTask(CrearSbcDao crearSbcDao) {
            this.crearSbcDao = crearSbcDao;
        }


        @Override
        protected List<CrearSbcEntity> doInBackground(Void... voids) {
            return crearSbcDao.findAllByEstado();
        }
    }

    private static class UpdateEstadoEliminarAllAsyncTask extends AsyncTask<Integer, Void, Void> {
        private CrearSbcDao crearSbcDao;

        private UpdateEstadoEliminarAllAsyncTask(CrearSbcDao crearSbcDao) {
            this.crearSbcDao = crearSbcDao;
        }


        @Override
        protected Void doInBackground(Integer... integers) {
            crearSbcDao.updateEstadoEliminar(integers[0]);
            return null;
        }
    }
}
