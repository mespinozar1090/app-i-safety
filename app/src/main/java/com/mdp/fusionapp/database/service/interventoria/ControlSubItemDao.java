package com.mdp.fusionapp.database.service.interventoria;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.interventoria.ControlSubItemEntity;

import java.util.List;

@Dao
public interface ControlSubItemDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ControlSubItemEntity controlSubItemEntity);

    @Query("DELETE FROM control_subitem_table")
    void deleteAll();

    @Query("SELECT * FROM control_subitem_table")
    List<ControlSubItemEntity> findAll();
}
