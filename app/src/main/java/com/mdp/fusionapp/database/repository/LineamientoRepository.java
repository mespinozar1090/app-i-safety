package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.service.LineamientoDao;
import com.mdp.fusionapp.database.entity.LineamientoEntity;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class LineamientoRepository {
    private LineamientoDao lineamientoDao;

    public LineamientoRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        this.lineamientoDao = database.lineamientoDao();
    }

    public void insert(LineamientoEntity lineamientoEntity) {
        new InsertAsyncTask(lineamientoDao).execute(lineamientoEntity);
    }

    public void deleteAll() {
        new DeleteAsyncTask(lineamientoDao).execute();
    }

    public List<LineamientoEntity> findAll() {
        try {
            return new GetAllAsyncTask(lineamientoDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Insert
    private static class InsertAsyncTask extends AsyncTask<LineamientoEntity, Void, Void> {
        private LineamientoDao lineamientoDao;

        private InsertAsyncTask(LineamientoDao lineamientoDao) {
            this.lineamientoDao = lineamientoDao;
        }

        @Override
        protected Void doInBackground(LineamientoEntity... entities) {
            lineamientoDao.insert(entities[0]);
            return null;
        }
    }

    //Delete
    private static class DeleteAsyncTask extends AsyncTask<Void, Void, Void> {
        private LineamientoDao lineamientoDao;

        private DeleteAsyncTask(LineamientoDao lineamientoDao) {
            this.lineamientoDao = lineamientoDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            lineamientoDao.deleteAll();
            return null;
        }
    }

    private static class GetAllAsyncTask extends AsyncTask<Integer, String, List<LineamientoEntity>> {
        private LineamientoDao lineamientoDao;

        public GetAllAsyncTask(LineamientoDao especialidadDao) {
            this.lineamientoDao = especialidadDao;
        }

        @Override
        protected List<LineamientoEntity> doInBackground(Integer... integers) {
            return lineamientoDao.findAll();
        }
    }

}
