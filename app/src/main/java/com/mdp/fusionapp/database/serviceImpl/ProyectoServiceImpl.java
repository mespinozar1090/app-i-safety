package com.mdp.fusionapp.database.serviceImpl;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.mdp.fusionapp.database.entity.ContratistaEntity;
import com.mdp.fusionapp.database.entity.ProyectoEntity;
import com.mdp.fusionapp.database.entity.UsuarioEntity;
import com.mdp.fusionapp.database.repository.ContratistaRepository;
import com.mdp.fusionapp.database.repository.ProyectoRepository;
import com.mdp.fusionapp.database.repository.UsuarioRepository;

import java.util.HashMap;
import java.util.List;

public class ProyectoServiceImpl extends AndroidViewModel {
    private ProyectoRepository repository;
    private ContratistaRepository contratistaRepository;

    public ProyectoServiceImpl(@NonNull Application application) {
        super(application);
        repository = new ProyectoRepository(application);
        contratistaRepository = new ContratistaRepository(application);
    }

  /*  public void insert(ProyectoEntity proyectoEntity) {
        repository.insert(proyectoEntity);
    }

    public void deleteAllProject() {
        repository.deleteAllProject();
    }

    public List<ProyectoEntity> findAllProject(HashMap hashMap) {
        return repository.findAllProject(hashMap);
    }

    public List<ContratistaEntity> findContratistaByProyecto(Integer idProyecto) {
        return contratistaRepository.findContratistaByProyecto(idProyecto);
    }*/

}
