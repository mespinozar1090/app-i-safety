package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.CategoriaBuenaPracticaEntity;
import com.mdp.fusionapp.database.service.CategoriaBuenaPracticaDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class CategoriaBuenaPracticaRepository {

    private CategoriaBuenaPracticaDao categoriaBuenaPracticaDao;

    public CategoriaBuenaPracticaRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        categoriaBuenaPracticaDao = database.categoriaBuenaPracticaDao();
    }

    public void insert(CategoriaBuenaPracticaEntity categoriaBuenaPracticaEntity) {
        new InsertAsyncTask(categoriaBuenaPracticaDao).execute(categoriaBuenaPracticaEntity);
    }

    public void deleteAll() {
        new DeleteAlltAsyncTask(categoriaBuenaPracticaDao).execute();
    }

    public List<CategoriaBuenaPracticaEntity> findCategoriaBuenaPractica() {
        try {
            return new GetCategoriaBuenaPracticaAsyncTask(categoriaBuenaPracticaDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static class InsertAsyncTask extends AsyncTask<CategoriaBuenaPracticaEntity, Void, Void> {
        private CategoriaBuenaPracticaDao categoriaBuenaPracticaDao;

        private InsertAsyncTask(CategoriaBuenaPracticaDao categoriaBuenaPracticaDao) {
            this.categoriaBuenaPracticaDao = categoriaBuenaPracticaDao;
        }

        @Override
        protected Void doInBackground(CategoriaBuenaPracticaEntity... tickets) {
            categoriaBuenaPracticaDao.insert(tickets[0]);
            return null;
        }
    }


    private static class DeleteAlltAsyncTask extends AsyncTask<Void, Void, Void> {
        private CategoriaBuenaPracticaDao categoriaBuenaPracticaDao;

        private DeleteAlltAsyncTask(CategoriaBuenaPracticaDao categoriaBuenaPracticaDao) {
            this.categoriaBuenaPracticaDao = categoriaBuenaPracticaDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            categoriaBuenaPracticaDao.deleteAll();
            return null;
        }
    }

    private static class GetCategoriaBuenaPracticaAsyncTask extends AsyncTask<Void, String, List<CategoriaBuenaPracticaEntity>> {
        private CategoriaBuenaPracticaDao categoriaBuenaPracticaDao;

        public GetCategoriaBuenaPracticaAsyncTask(CategoriaBuenaPracticaDao categoriaBuenaPracticaDao) {
            this.categoriaBuenaPracticaDao = categoriaBuenaPracticaDao;
        }


        @Override
        protected List<CategoriaBuenaPracticaEntity> doInBackground(Void... voids) {
            return categoriaBuenaPracticaDao.findCategoriaBuenaPractica();
        }
    }
}
