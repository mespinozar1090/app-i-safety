package com.mdp.fusionapp.database.repository.interventoria;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.interventoria.CrearControlEntity;
import com.mdp.fusionapp.database.service.interventoria.CrearControlDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class CrearControlRepository {

    private CrearControlDao crearControlDao;

    public CrearControlRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        crearControlDao = database.crearControlDao();
    }

    public void insert(CrearControlEntity crearControlEntity) {
        new InsertAsyncTask(crearControlDao).execute(crearControlEntity);
    }

    public void deleteAll() {
        new DeleteAllAsyncTask(crearControlDao).execute();
    }

    public void deleteById(Long id) {
        new DeleteByIdAsyncTask(crearControlDao).execute(id);
    }

    public List<CrearControlEntity> findAll(Long idOffline) {
        try {
            return new GetAllAsyncTask(crearControlDao).execute(idOffline).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static class GetAllAsyncTask extends AsyncTask<Long, String, List<CrearControlEntity>> {
        private CrearControlDao crearControlDao;

        public GetAllAsyncTask(CrearControlDao crearControlDao) {
            this.crearControlDao = crearControlDao;
        }

        @Override
        protected List<CrearControlEntity> doInBackground(Long... integers) {
            return crearControlDao.findAllById(integers[0]);
        }
    }

    private static class InsertAsyncTask extends AsyncTask<CrearControlEntity, Void, Void> {
        private CrearControlDao crearControlDao;

        private InsertAsyncTask(CrearControlDao crearControlDao) {
            this.crearControlDao = crearControlDao;
        }

        @Override
        protected Void doInBackground(CrearControlEntity... entities) {
            crearControlDao.insert(entities[0]);
            return null;
        }
    }


    private static class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        private CrearControlDao crearControlDao;

        private DeleteAllAsyncTask(CrearControlDao crearControlDao) {
            this.crearControlDao = crearControlDao;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            crearControlDao.deleteAll();
            return null;
        }
    }

    private static class DeleteByIdAsyncTask extends AsyncTask<Long, Void, Void> {
        private CrearControlDao crearControlDao;

        private DeleteByIdAsyncTask(CrearControlDao crearControlDao) {
            this.crearControlDao = crearControlDao;
        }

        @Override
        protected Void doInBackground(Long... entities) {
            crearControlDao.deleteById(entities[0]);
            return null;
        }
    }

}
