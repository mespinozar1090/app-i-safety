package com.mdp.fusionapp.database.service;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.auditoria.CrearAuditoriaEntity;
import com.mdp.fusionapp.database.entity.auditoria.CrearLineamientoEntity;

import java.util.List;

@Dao
public interface CrearLineamientoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(CrearLineamientoEntity crearLineamientoEntity);

    @Query("DELETE FROM crear_lineamiento_table")
    void deleteAll();

    @Query("SELECT * FROM crear_lineamiento_table")
    List<CrearLineamientoEntity> findAll();

    @Query("SELECT * FROM crear_lineamiento_table WHERE id=:id")
    CrearLineamientoEntity findAllById(Long id);
}
