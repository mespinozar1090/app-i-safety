package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.auditoria.CrearAuditoriaEntity;
import com.mdp.fusionapp.database.service.CrearAuditoriaDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class CrearAuditoriaRepository {

    private CrearAuditoriaDao crearAuditoriaDao;

    public CrearAuditoriaRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        crearAuditoriaDao = database.crearAuditoriaDao();
    }

    public Long insert(CrearAuditoriaEntity crearAuditoriaEntity) {
        try {
            return new InsertAsyncTask(crearAuditoriaDao).execute(crearAuditoriaEntity).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteAll() {
        new DeleteAsyncTask(crearAuditoriaDao).execute();
    }

    public void deleteById(Integer id) {
        new DeleteByIdAsyncTask(crearAuditoriaDao).execute(id);
    }

    public List<CrearAuditoriaEntity> findAll() {
        try {
            return new GetAllAsyncTask(crearAuditoriaDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

   public List<CrearAuditoriaEntity> findAllByEstado() {
        try {
            return new GetAllAsyncTask(crearAuditoriaDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public CrearAuditoriaEntity findById(Integer id) {
        try {
            return new GetByIdAsyncTask(crearAuditoriaDao).execute(id).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<CrearAuditoriaEntity> findByIdEstado() {
        try {
            return new GetAllByEstadoAsyncTask(crearAuditoriaDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Delete
    private static class DeleteByIdAsyncTask extends AsyncTask<Integer, Void, Void> {
        private CrearAuditoriaDao crearAuditoriaDao;

        private DeleteByIdAsyncTask(CrearAuditoriaDao crearAuditoriaDao) {
            this.crearAuditoriaDao = crearAuditoriaDao;
        }

        @Override
        protected Void doInBackground(Integer... integers) {
            crearAuditoriaDao.deleteById(integers[0]);
            return null;
        }
    }


    //Select by id
    private static class GetByIdAsyncTask extends AsyncTask<Integer, Void, CrearAuditoriaEntity> {
        private CrearAuditoriaDao crearAuditoriaDao;

        private GetByIdAsyncTask(CrearAuditoriaDao crearAuditoriaDao) {
            this.crearAuditoriaDao = crearAuditoriaDao;
        }


        @Override
        protected CrearAuditoriaEntity doInBackground(Integer... integers) {
            return crearAuditoriaDao.findAllById(integers[0]);
        }
    }

    //Select
    private static class GetAllAsyncTask extends AsyncTask<Void, Void, List<CrearAuditoriaEntity>> {
        private CrearAuditoriaDao crearAuditoriaDao;

        private GetAllAsyncTask(CrearAuditoriaDao crearAuditoriaDao) {
            this.crearAuditoriaDao = crearAuditoriaDao;
        }


        @Override
        protected List<CrearAuditoriaEntity> doInBackground(Void... voids) {
            return crearAuditoriaDao.findAll();
        }
    }

    //Select by estado
    private static class GetAllByEstadoAsyncTask extends AsyncTask<Void, Void, List<CrearAuditoriaEntity>> {
        private CrearAuditoriaDao crearAuditoriaDao;

        private GetAllByEstadoAsyncTask(CrearAuditoriaDao crearAuditoriaDao) {
            this.crearAuditoriaDao = crearAuditoriaDao;
        }

        @Override
        protected List<CrearAuditoriaEntity> doInBackground(Void... voids) {
            return crearAuditoriaDao.findAllByEstado();
        }
    }

    //Delete
    private static class DeleteAsyncTask extends AsyncTask<Void, Void, Void> {
        private CrearAuditoriaDao crearAuditoriaDao;

        private DeleteAsyncTask(CrearAuditoriaDao crearAuditoriaDao) {
            this.crearAuditoriaDao = crearAuditoriaDao;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            crearAuditoriaDao.deleteAll();
            return null;
        }
    }

    //Insert
    private static class InsertAsyncTask extends AsyncTask<CrearAuditoriaEntity, Void, Long> {
        private CrearAuditoriaDao crearAuditoriaDao;

        private InsertAsyncTask(CrearAuditoriaDao crearAuditoriaDao) {
            this.crearAuditoriaDao = crearAuditoriaDao;
        }

        @Override
        protected Long doInBackground(CrearAuditoriaEntity... entities) {
            return crearAuditoriaDao.insert(entities[0]);
        }
    }
}
