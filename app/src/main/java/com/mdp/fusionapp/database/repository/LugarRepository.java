package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.LugarEntity;
import com.mdp.fusionapp.database.entity.ProyectoEntity;
import com.mdp.fusionapp.database.entity.TipoUbicacionEntity;
import com.mdp.fusionapp.database.service.LugarDao;
import com.mdp.fusionapp.database.service.ProyectoDao;
import com.mdp.fusionapp.database.service.TipoUbicacionDao;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class LugarRepository {

    private LugarDao lugarDao;

    public LugarRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        this.lugarDao = database.lugarDao();
    }

    public void insert(LugarEntity lugarEntity) {
        new InsertTipoUnibacionAsyncTask(lugarDao).execute(lugarEntity);
    }

    public void deleteAll() {
        new DeleteTipoUbioacionAsyncTask(lugarDao).execute();
    }

    public List<LugarEntity> findLugarByUbicacion(Integer idTipoUbicacion) {
        try {
            return new GetLugarAsyncTask(lugarDao,idTipoUbicacion).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


    //Insert
    private static class InsertTipoUnibacionAsyncTask extends AsyncTask<LugarEntity, Void, Void> {
        private LugarDao lugarDao;

        private InsertTipoUnibacionAsyncTask(LugarDao lugarDao) {
            this.lugarDao = lugarDao;
        }

        @Override
        protected Void doInBackground(LugarEntity... lugarEntities) {
            lugarDao.insert(lugarEntities[0]);
            return null;
        }
    }

    //Delete
    private static class DeleteTipoUbioacionAsyncTask extends AsyncTask<LugarEntity, Void, Void> {
        private LugarDao lugarDao;

        private DeleteTipoUbioacionAsyncTask(LugarDao lugarDao) {
            this.lugarDao = lugarDao;
        }

        @Override
        protected Void doInBackground(LugarEntity... lugarEntities) {
            lugarDao.deleteAll();
            return null;
        }
    }

    private static class GetLugarAsyncTask extends AsyncTask<Integer, String, List<LugarEntity>> {
        private LugarDao lugarDao;
        private Integer idTipoUbicacion;

        public GetLugarAsyncTask(LugarDao lugarDao, Integer idTipoUbicacion) {
            this.lugarDao = lugarDao;
            this.idTipoUbicacion = idTipoUbicacion;
        }

        @Override
        protected List<LugarEntity> doInBackground(Integer... integers) {
            return lugarDao.findTipoLugar(idTipoUbicacion);
        }
    }

}
