package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.EspecialidadEntity;
import com.mdp.fusionapp.database.service.EspecialidadDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class EspecilidadRepository {

    private EspecialidadDao especialidadDao;

    public EspecilidadRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        this.especialidadDao = database.especialidadDao();
    }

    public void insert(EspecialidadEntity especialidadEntity) {
        new InsertAsyncTask(especialidadDao).execute(especialidadEntity);
    }

    public void deleteAll() {
        new DeleteAsyncTask(especialidadDao).execute();
    }

    public List<EspecialidadEntity> findAll() {
        try {
            return new GetAllAsyncTask(especialidadDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Insert
    private static class InsertAsyncTask extends AsyncTask<EspecialidadEntity, Void, Void> {
        private EspecialidadDao especialidadDao;

        private InsertAsyncTask(EspecialidadDao especialidadDao) {
            this.especialidadDao = especialidadDao;
        }

        @Override
        protected Void doInBackground(EspecialidadEntity... entities) {
            especialidadDao.insert(entities[0]);
            return null;
        }
    }

    //Delete
    private static class DeleteAsyncTask extends AsyncTask<Void, Void, Void> {
        private EspecialidadDao especialidadDao;

        private DeleteAsyncTask(EspecialidadDao especialidadDao) {
            this.especialidadDao = especialidadDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            especialidadDao.deleteAll();
            return null;
        }
    }

    private static class GetAllAsyncTask extends AsyncTask<Integer, String, List<EspecialidadEntity>> {
        private EspecialidadDao especialidadDao;

        public GetAllAsyncTask(EspecialidadDao especialidadDao) {
            this.especialidadDao = especialidadDao;
        }

        @Override
        protected List<EspecialidadEntity> doInBackground(Integer... integers) {
            return especialidadDao.findAll();
        }
    }

}
