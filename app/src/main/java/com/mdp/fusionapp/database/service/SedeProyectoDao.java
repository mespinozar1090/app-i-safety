package com.mdp.fusionapp.database.service;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.LugarEntity;
import com.mdp.fusionapp.database.entity.ProyectoEntity;
import com.mdp.fusionapp.database.entity.SedeProyectoEntity;

import java.util.List;

@Dao
public interface SedeProyectoDao {

    @Insert
    void insert(SedeProyectoEntity sedeProyectoEntity);

    @Query("DELETE FROM sede_proyecto_table")
    void deleteAll();

    @Query("SELECT * FROM sede_proyecto_table where modulo=:modulo")
    List<SedeProyectoEntity> findAll(String modulo);
}
