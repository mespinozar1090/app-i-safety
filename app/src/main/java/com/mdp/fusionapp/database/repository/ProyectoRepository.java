package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.ProyectoEntity;
import com.mdp.fusionapp.database.service.ProyectoDao;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ProyectoRepository {

    private ProyectoDao proyectoDao;

    public ProyectoRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        proyectoDao = database.proyectoDao();
    }

    public void insert(ProyectoEntity proyectoEntity) {
        new InsertProjectAsyncTask(proyectoDao).execute(proyectoEntity);
    }

    public void deleteAllProject() {
        new DeleteAllProjectAsyncTask(proyectoDao).execute();
    }

    //Select all Project
    public List<ProyectoEntity> findAllProject(HashMap hashMap) {
        try {
            return new GetProyectAsyncTask(proyectoDao).execute(hashMap).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Select all Project
   /* public LiveData<List<ProyectoEntity>> findAll() {
        return proyectoDao.findAll();
    }
*/
    //Listar proyectos
    private static class GetProyectAsyncTask extends AsyncTask<HashMap, String, List<ProyectoEntity>> {
        private ProyectoDao proyectoDao;

        public GetProyectAsyncTask(ProyectoDao proyectoDao) {
            this.proyectoDao = proyectoDao;
        }

        @Override
        protected List<ProyectoEntity> doInBackground(HashMap... hashMap) {
            Integer idRolGeneral = (Integer) hashMap[0].get("Id_Rol_General");
            Integer idRolUsuario = (Integer) hashMap[0].get("Id_Rol_Usuario");
            Integer idUsuario = (Integer) hashMap[0].get("Id_Usuario");
            Log.e("doInBackground",""+idRolGeneral+" "+idRolUsuario+" "+idUsuario);
            return  proyectoDao.findAll();
        }
    }


    //Insert
    private static class InsertProjectAsyncTask extends AsyncTask<ProyectoEntity, Void, Void> {
        private ProyectoDao proyectoDao;

        private InsertProjectAsyncTask(ProyectoDao proyectoDao) {
            this.proyectoDao = proyectoDao;
        }

        @Override
        protected Void doInBackground(ProyectoEntity... proyectoEntities) {
            proyectoDao.insert(proyectoEntities[0]);
            return null;
        }
    }

    //Delete
    private static class DeleteAllProjectAsyncTask extends AsyncTask<ProyectoEntity, Void, Void> {
        private ProyectoDao proyectoDao;

        private DeleteAllProjectAsyncTask(ProyectoDao proyectoDao) {
            this.proyectoDao = proyectoDao;
        }

        @Override
        protected Void doInBackground(ProyectoEntity... proyectoEntities) {
            proyectoDao.deleteAllProject();
            return null;
        }
    }

}
