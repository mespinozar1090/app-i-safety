package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.SedeProyectoEntity;
import com.mdp.fusionapp.database.service.SedeProyectoDao;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class SedeProyectoRepository {

    private SedeProyectoDao sedeProyectoDao;

    public SedeProyectoRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        this.sedeProyectoDao = database.sedeProyectoDao();
    }

    public void insert(SedeProyectoEntity sedeProyectoEntity) {
        new InsertAsyncTask(sedeProyectoDao).execute(sedeProyectoEntity);
    }

    public void deleteAll() {
        new DeleteAsyncTask(sedeProyectoDao).execute();
    }

    public List<SedeProyectoEntity> findAll(HashMap hashMap) {
        try {
            return new GetAllAsyncTask(sedeProyectoDao).execute(hashMap).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Insert
    private static class InsertAsyncTask extends AsyncTask<SedeProyectoEntity, Void, Void> {
        private SedeProyectoDao sedeProyectoDao;

        private InsertAsyncTask(SedeProyectoDao sedeProyectoDao) {
            this.sedeProyectoDao = sedeProyectoDao;
        }

        @Override
        protected Void doInBackground(SedeProyectoEntity... entities) {
            sedeProyectoDao.insert(entities[0]);
            return null;
        }
    }

    //Delete
    private static class DeleteAsyncTask extends AsyncTask<SedeProyectoEntity, Void, Void> {
        private SedeProyectoDao sedeProyectoDao;

        private DeleteAsyncTask(SedeProyectoDao sedeProyectoDao) {
            this.sedeProyectoDao = sedeProyectoDao;
        }

        @Override
        protected Void doInBackground(SedeProyectoEntity... entities) {
            sedeProyectoDao.deleteAll();
            return null;
        }
    }

    private static class GetAllAsyncTask extends AsyncTask<HashMap, String, List<SedeProyectoEntity>> {
        private SedeProyectoDao sedeProyectoDao;

        public GetAllAsyncTask(SedeProyectoDao sedeProyectoDao) {
            this.sedeProyectoDao = sedeProyectoDao;
        }

       /* @Override
        protected List<SedeProyectoEntity> doInBackground(Integer... integers) {
            return sedeProyectoDao.findAll();
        }*/

        @Override
        protected List<SedeProyectoEntity> doInBackground(HashMap... hashMaps) {
            String modulo = (String) hashMaps[0].get("Modulo");
            return sedeProyectoDao.findAll(modulo);
        }
    }

}
