package com.mdp.fusionapp.database.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "lugar_table")
public class LugarEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("idLugar")
    private Integer idLugar;

    @SerializedName("Nombre")
    private String nombre;

    @SerializedName("idProyecto")
    private Integer idProyecto;

    @SerializedName("idTipoUbicacion")
    private Integer idTipoUbicacion;


    public LugarEntity(Integer idLugar, String nombre) {
        this.idLugar = idLugar;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdLugar() {
        return idLugar;
    }

    public void setIdLugar(Integer idLugar) {
        this.idLugar = idLugar;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public Integer getIdTipoUbicacion() {
        return idTipoUbicacion;
    }

    public void setIdTipoUbicacion(Integer idTipoUbicacion) {
        this.idTipoUbicacion = idTipoUbicacion;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
