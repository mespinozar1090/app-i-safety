package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.EmpresaMaestraEntity;
import com.mdp.fusionapp.database.entity.TipoUbicacionEntity;
import com.mdp.fusionapp.database.service.EmpresaMaestraDao;
import com.mdp.fusionapp.database.service.TipoUbicacionDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class EmpresaMaestraRepository {

    private EmpresaMaestraDao empresaMaestraDao;

    public EmpresaMaestraRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        this.empresaMaestraDao = database.empresaMaestraDao();
    }

    public void insert(EmpresaMaestraEntity empresaMaestraEntity) {
        new InsertAsyncTask(empresaMaestraDao).execute(empresaMaestraEntity);
    }

    public void deleteAll() {
        new DeleteAllAsyncTask(empresaMaestraDao).execute();
    }

    public EmpresaMaestraEntity findEmpresaMaestraByProyecto(Integer idProyecto) {
        try {
            return new GetListObjectAsyncTask(empresaMaestraDao, idProyecto).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


    private static class InsertAsyncTask extends AsyncTask<EmpresaMaestraEntity, Void, Void> {
        private EmpresaMaestraDao empresaMaestraDao;

        private InsertAsyncTask(EmpresaMaestraDao empresaMaestraDao) {
            this.empresaMaestraDao = empresaMaestraDao;
        }

        @Override
        protected Void doInBackground(EmpresaMaestraEntity... entities) {
            empresaMaestraDao.insert(entities[0]);
            return null;
        }
    }

    //Delete
    private static class DeleteAllAsyncTask extends AsyncTask<EmpresaMaestraEntity, Void, Void> {
        private EmpresaMaestraDao empresaMaestraDao;

        private DeleteAllAsyncTask(EmpresaMaestraDao empresaMaestraDao) {
            this.empresaMaestraDao = empresaMaestraDao;
        }

        @Override
        protected Void doInBackground(EmpresaMaestraEntity... entities) {
            empresaMaestraDao.deleteAll();
            return null;
        }
    }

    private static class GetListObjectAsyncTask extends AsyncTask<Integer, String, EmpresaMaestraEntity> {
        private EmpresaMaestraDao empresaMaestraDao;
        private Integer idProceso;

        public GetListObjectAsyncTask(EmpresaMaestraDao empresaMaestraDao, Integer idProceso) {
            this.empresaMaestraDao = empresaMaestraDao;
            this.idProceso = idProceso;
        }

        @Override
        protected EmpresaMaestraEntity doInBackground(Integer... integers) {
            return empresaMaestraDao.findEmpresaMaestraByProyecto(idProceso);
        }
    }
}
