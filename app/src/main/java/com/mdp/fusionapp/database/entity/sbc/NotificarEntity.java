package com.mdp.fusionapp.database.entity.sbc;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "notificar_table")
public class NotificarEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private Integer idUser;
    private String nombre;
    private String email;
    private Integer idModulo;
    private Long idOffline;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getIdModulo() {
        return idModulo;
    }

    public void setIdModulo(Integer idModulo) {
        this.idModulo = idModulo;
    }

    public Long getIdOffline() {
        return idOffline;
    }

    public void setIdOffline(Long idOffline) {
        this.idOffline = idOffline;
    }
}
