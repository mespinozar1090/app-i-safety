package com.mdp.fusionapp.database.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "crear_hallazgo_table")
public class CrearHallazgoEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private Integer idInspeccionesReporteDetalle;
    private Integer idHallazgo;
    private Long idInspeccionOffline;
    private Integer idInspeccion;
    private Integer idTipoGestion;
    private Integer idActoSubEstandar;
    private Integer idCondicionSubEstandar;
    private String actoSubestandar;
    private Boolean riesgoA;
    private Boolean riesgoM;
    private Boolean riesgoB;
    private Integer idTipoHallazgo;
    private String resposableAreaDetalle;
    private String evidenciaFotoFecha;
    private String evidenciaFotoImg64;
    private String plazaString;
    private Boolean estadoDetalleInspeccion;
    private String actionMitigadora;
    private String evidenciaCierreFotoFecha;
    private String evidenciaCierreFotoImg64;
    private Integer idUsuarioRegistro;
    private Integer idRolUsuario;
    private String nombreActoSubestandar;
    private String nombreTipoGestion;
    private String nombreTipoHallazgo;
    private Integer estadoRegistroDB = 0;

    public Integer getIdInspeccionesReporteDetalle() {
        return idInspeccionesReporteDetalle;
    }

    public void setIdInspeccionesReporteDetalle(Integer idInspeccionesReporteDetalle) {
        this.idInspeccionesReporteDetalle = idInspeccionesReporteDetalle;
    }

    public Long getIdInspeccionOffline() {
        return idInspeccionOffline;
    }

    public void setIdInspeccionOffline(Long idInspeccionOffline) {
        this.idInspeccionOffline = idInspeccionOffline;
    }

    public Integer getIdTipoHallazgo() {
        return idTipoHallazgo;
    }

    public void setIdTipoHallazgo(Integer idTipoHallazgo) {
        this.idTipoHallazgo = idTipoHallazgo;
    }

    public Integer getIdRolUsuario() {
        return idRolUsuario;
    }

    public void setIdRolUsuario(Integer idRolUsuario) {
        this.idRolUsuario = idRolUsuario;
    }

    public String getNombreTipoHallazgo() {
        return nombreTipoHallazgo;
    }

    public void setNombreTipoHallazgo(String nombreTipoHallazgo) {
        this.nombreTipoHallazgo = nombreTipoHallazgo;
    }

    public String getNombreTipoGestion() {
        return nombreTipoGestion;
    }

    public void setNombreTipoGestion(String nombreTipoGestion) {
        this.nombreTipoGestion = nombreTipoGestion;
    }

    public String getNombreActoSubestandar() {
        return nombreActoSubestandar;
    }

    public void setNombreActoSubestandar(String nombreActoSubestandar) {
        this.nombreActoSubestandar = nombreActoSubestandar;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdInspeccion() {
        return idInspeccion;
    }

    public void setIdInspeccion(Integer idInspeccion) {
        this.idInspeccion = idInspeccion;
    }

    public Integer getIdTipoGestion() {
        return idTipoGestion;
    }

    public void setIdTipoGestion(Integer idTipoGestion) {
        this.idTipoGestion = idTipoGestion;
    }

    public Integer getIdActoSubEstandar() {
        return idActoSubEstandar;
    }

    public void setIdActoSubEstandar(Integer idActoSubEstandar) {
        this.idActoSubEstandar = idActoSubEstandar;
    }

    public Integer getIdCondicionSubEstandar() {
        return idCondicionSubEstandar;
    }

    public void setIdCondicionSubEstandar(Integer idCondicionSubEstandar) {
        this.idCondicionSubEstandar = idCondicionSubEstandar;
    }

    public String getActoSubestandar() {
        return actoSubestandar;
    }

    public void setActoSubestandar(String actoSubestandar) {
        this.actoSubestandar = actoSubestandar;
    }

    public Boolean getRiesgoA() {
        return riesgoA;
    }

    public void setRiesgoA(Boolean riesgoA) {
        this.riesgoA = riesgoA;
    }

    public Boolean getRiesgoM() {
        return riesgoM;
    }

    public void setRiesgoM(Boolean riesgoM) {
        this.riesgoM = riesgoM;
    }

    public Boolean getRiesgoB() {
        return riesgoB;
    }

    public void setRiesgoB(Boolean riesgoB) {
        this.riesgoB = riesgoB;
    }

    public Integer getIdHallazgo() {
        return idHallazgo;
    }

    public void setIdHallazgo(Integer idHallazgo) {
        this.idHallazgo = idHallazgo;
    }

    public String getResposableAreaDetalle() {
        return resposableAreaDetalle;
    }

    public void setResposableAreaDetalle(String resposableAreaDetalle) {
        this.resposableAreaDetalle = resposableAreaDetalle;
    }

    public String getEvidenciaFotoFecha() {
        return evidenciaFotoFecha;
    }

    public void setEvidenciaFotoFecha(String evidenciaFotoFecha) {
        this.evidenciaFotoFecha = evidenciaFotoFecha;
    }

    public String getEvidenciaFotoImg64() {
        return evidenciaFotoImg64;
    }

    public void setEvidenciaFotoImg64(String evidenciaFotoImg64) {
        this.evidenciaFotoImg64 = evidenciaFotoImg64;
    }

    public String getPlazaString() {
        return plazaString;
    }

    public void setPlazaString(String plazaString) {
        this.plazaString = plazaString;
    }

    public Boolean getEstadoDetalleInspeccion() {
        return estadoDetalleInspeccion;
    }

    public void setEstadoDetalleInspeccion(Boolean estadoDetalleInspeccion) {
        this.estadoDetalleInspeccion = estadoDetalleInspeccion;
    }

    public String getActionMitigadora() {
        return actionMitigadora;
    }

    public void setActionMitigadora(String actionMitigadora) {
        this.actionMitigadora = actionMitigadora;
    }

    public String getEvidenciaCierreFotoFecha() {
        return evidenciaCierreFotoFecha;
    }

    public void setEvidenciaCierreFotoFecha(String evidenciaCierreFotoFecha) {
        this.evidenciaCierreFotoFecha = evidenciaCierreFotoFecha;
    }

    public String getEvidenciaCierreFotoImg64() {
        return evidenciaCierreFotoImg64;
    }

    public void setEvidenciaCierreFotoImg64(String evidenciaCierreFotoImg64) {
        this.evidenciaCierreFotoImg64 = evidenciaCierreFotoImg64;
    }

    public Integer getIdUsuarioRegistro() {
        return idUsuarioRegistro;
    }

    public void setIdUsuarioRegistro(Integer idUsuarioRegistro) {
        this.idUsuarioRegistro = idUsuarioRegistro;
    }

    public Integer getEstadoRegistroDB() {
        return estadoRegistroDB;
    }

    public void setEstadoRegistroDB(Integer estadoRegistroDB) {
        this.estadoRegistroDB = estadoRegistroDB;
    }
}
