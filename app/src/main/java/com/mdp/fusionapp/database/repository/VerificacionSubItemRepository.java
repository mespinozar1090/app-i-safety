package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.interventoria.VerificacionSubItemEntity;
import com.mdp.fusionapp.database.service.interventoria.VerificacionSubItemDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class VerificacionSubItemRepository {

    private VerificacionSubItemDao verificacionSubItemDao;

    public VerificacionSubItemRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        this.verificacionSubItemDao = database.verificacionSubItemDao();
    }


    public void insert(VerificacionSubItemEntity verificacionSubItemEntity) {
        new InsertAsyncTask(verificacionSubItemDao).execute(verificacionSubItemEntity);
    }

    public void deleteAll() {
        new DeleteAsyncTask(verificacionSubItemDao).execute();
    }


    public List<VerificacionSubItemEntity> findVerificacionSubItem() {
        try {
            return new GetAllAsyncTask(verificacionSubItemDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static class GetAllAsyncTask extends AsyncTask<Void, String, List<VerificacionSubItemEntity>> {
        private VerificacionSubItemDao verificacionSubItemDao;

        public GetAllAsyncTask(VerificacionSubItemDao verificacionSubItemDao) {
            this.verificacionSubItemDao = verificacionSubItemDao;
        }

        @Override
        protected List<VerificacionSubItemEntity> doInBackground(Void... voids) {
            return verificacionSubItemDao.findAll();
        }
    }

    //Insert
    private static class InsertAsyncTask extends AsyncTask<VerificacionSubItemEntity, Void, Void> {
        private VerificacionSubItemDao verificacionSubItemDao;

        private InsertAsyncTask(VerificacionSubItemDao verificacionSubItemDao) {
            this.verificacionSubItemDao = verificacionSubItemDao;
        }

        @Override
        protected Void doInBackground(VerificacionSubItemEntity... lugarEntities) {
            verificacionSubItemDao.insert(lugarEntities[0]);
            return null;
        }
    }

    //Delete
    private static class DeleteAsyncTask extends AsyncTask<Void, Void, Void> {
        private VerificacionSubItemDao verificacionSubItemDao;

        private DeleteAsyncTask(VerificacionSubItemDao verificacionSubItemDao) {
            this.verificacionSubItemDao = verificacionSubItemDao;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            verificacionSubItemDao.deleteAll();
            return null;
        }
    }
}
