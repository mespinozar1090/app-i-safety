package com.mdp.fusionapp.database.service.interventoria;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.interventoria.CrearEvidenciaInterEntity;

import java.util.List;

@Dao
public interface CrearEvidenciaInterDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(CrearEvidenciaInterEntity crearEvidenciaInterEntity);


    @Query("DELETE FROM crear_evidencia_inter_table")
    void deleteAll();

    @Query("DELETE FROM crear_evidencia_inter_table WHERE idOffline=:idOffline")
    void deleteById(Long idOffline);

    @Query("SELECT * FROM crear_evidencia_inter_table")
    List<CrearEvidenciaInterEntity> findAll();

    @Query("SELECT * FROM crear_evidencia_inter_table where idOffline=:idOffline")
    List<CrearEvidenciaInterEntity> findAllById(Long idOffline);

    @Query("UPDATE crear_evidencia_inter_table SET imgBase64=:base64 where id=:idRow")
    void updaImageEvidencia(String base64, Long idRow);
}
