package com.mdp.fusionapp.database.service.sbc;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.ProyectoEntity;
import com.mdp.fusionapp.database.entity.sbc.CrearCategoriaSbcEntity;
import com.mdp.fusionapp.database.entity.sbc.CrearSbcEntity;

import java.util.List;

@Dao
public interface CrearCategoriaSbcDao {

    @Insert
    void insert(CrearCategoriaSbcEntity crearCategoriaSbcEntity);

    @Delete
    void delete(CrearCategoriaSbcEntity crearCategoriaSbcEntity);

    @Query("DELETE FROM crear_categorisbc_table WHERE idOffline=:idOffline")
    void deleteById(Long idOffline);

    @Query("SELECT * FROM crear_categorisbc_table")
    List<CrearCategoriaSbcEntity> findAll();

    @Query("SELECT * FROM crear_categorisbc_table where idOffline=:idOffline")
    List<CrearCategoriaSbcEntity> findAllById(Long idOffline);
}
