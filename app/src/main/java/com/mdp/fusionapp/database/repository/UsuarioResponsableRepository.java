package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.ContratistaEntity;
import com.mdp.fusionapp.database.entity.UsuarioResponsableEntity;
import com.mdp.fusionapp.database.service.ContratistaDao;
import com.mdp.fusionapp.database.service.UsuarioResponsableDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class UsuarioResponsableRepository {

    private UsuarioResponsableDao usuarioResponsableDao;

    public UsuarioResponsableRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        usuarioResponsableDao = database.usuarioResponsableDao();
    }

    public void insert(UsuarioResponsableEntity usuarioEntity) {
        new InsertAsyncTask(usuarioResponsableDao).execute(usuarioEntity);
    }


    //Select all Project
    public List<UsuarioResponsableEntity> findUsuarioResponsables() {
        try {
            return new GetUsuarioResponsablesAsyncTask(usuarioResponsableDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteAll() {
        new DeleteAlltAsyncTask(usuarioResponsableDao).execute();
    }

    private static class InsertAsyncTask extends AsyncTask<UsuarioResponsableEntity, Void, Void> {
        private UsuarioResponsableDao usuarioResponsableDao;

        private InsertAsyncTask(UsuarioResponsableDao usuarioResponsableDao) {
            this.usuarioResponsableDao = usuarioResponsableDao;
        }

        @Override
        protected Void doInBackground(UsuarioResponsableEntity... tickets) {
            usuarioResponsableDao.insert(tickets[0]);
            return null;
        }
    }

    private static class DeleteAlltAsyncTask extends AsyncTask<Void, Void, Void> {
        private UsuarioResponsableDao usuarioResponsableDao;

        private DeleteAlltAsyncTask(UsuarioResponsableDao usuarioResponsableDao) {
            this.usuarioResponsableDao = usuarioResponsableDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            usuarioResponsableDao.deleteAll();
            return null;
        }
    }

    private static class GetUsuarioResponsablesAsyncTask extends AsyncTask<Integer, String, List<UsuarioResponsableEntity>> {
        private UsuarioResponsableDao usuarioResponsableDao;

        public GetUsuarioResponsablesAsyncTask(UsuarioResponsableDao usuarioResponsableDao) {
            this.usuarioResponsableDao = usuarioResponsableDao;

        }

        @Override
        protected List<UsuarioResponsableEntity> doInBackground(Integer... integers) {
            return usuarioResponsableDao.findUsuarioResponsables();
        }
    }

}
