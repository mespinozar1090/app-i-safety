package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.service.LineamientoSubDao;
import com.mdp.fusionapp.database.entity.LineamientoSubEntity;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class LineamientoSubRepository {

    private LineamientoSubDao lineamientoSubDao;

    public LineamientoSubRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        this.lineamientoSubDao = database.lineamientoSubDao();
    }

    public void insert(LineamientoSubEntity lineamientoSubEntity) {
        new InsertAsyncTask(lineamientoSubDao).execute(lineamientoSubEntity);
    }

    public void deleteAll() {
        new DeleteAsyncTask(lineamientoSubDao).execute();
    }

    public List<LineamientoSubEntity> findAll() {
        try {
            return new GetAllAsyncTask(lineamientoSubDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


    //Insert
    private static class InsertAsyncTask extends AsyncTask<LineamientoSubEntity, Void, Void> {
        private LineamientoSubDao lineamientoSubDao;

        private InsertAsyncTask(LineamientoSubDao lineamientoSubDao) {
            this.lineamientoSubDao = lineamientoSubDao;
        }

        @Override
        protected Void doInBackground(LineamientoSubEntity... entities) {
            lineamientoSubDao.insert(entities[0]);
            return null;
        }
    }

    //Delete
    private static class DeleteAsyncTask extends AsyncTask<Void, Void, Void> {
        private LineamientoSubDao lineamientoSubDao;

        private DeleteAsyncTask(LineamientoSubDao lineamientoSubDao) {
            this.lineamientoSubDao = lineamientoSubDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            lineamientoSubDao.deleteAll();
            return null;
        }
    }

    private static class GetAllAsyncTask extends AsyncTask<Integer, String, List<LineamientoSubEntity>> {
        private LineamientoSubDao lineamientoSubDao;

        public GetAllAsyncTask(LineamientoSubDao lineamientoSubDao) {
            this.lineamientoSubDao = lineamientoSubDao;
        }

        @Override
        protected List<LineamientoSubEntity> doInBackground(Integer... integers) {
            return lineamientoSubDao.findAll();
        }
    }

}
