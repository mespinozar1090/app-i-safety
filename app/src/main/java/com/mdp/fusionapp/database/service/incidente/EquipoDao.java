package com.mdp.fusionapp.database.service.incidente;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.incidente.EquipoEntity;

import java.util.List;

@Dao
public interface EquipoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(EquipoEntity equipoEntity);

    @Query("DELETE FROM equipo_table")
    void deleteAll();

    @Query("SELECT * FROM equipo_table")
    List<EquipoEntity> findAll();
}
