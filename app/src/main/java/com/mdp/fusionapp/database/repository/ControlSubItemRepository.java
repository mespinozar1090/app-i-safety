package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.interventoria.ControlSubItemEntity;
import com.mdp.fusionapp.database.service.interventoria.ControlSubItemDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class ControlSubItemRepository {

    private ControlSubItemDao controlSubItemDao;

    public ControlSubItemRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        this.controlSubItemDao = database.controlSubItemDao();
    }

    public void insert(ControlSubItemEntity controlSubItemEntity) {
        new InsertAsyncTask(controlSubItemDao).execute(controlSubItemEntity);
    }

    public void deleteAll() {
        new DeleteAsyncTask(controlSubItemDao).execute();
    }

    public List<ControlSubItemEntity> findAll() {
        try {
            return new GetAllAsyncTask(controlSubItemDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static class GetAllAsyncTask extends AsyncTask<Void, String, List<ControlSubItemEntity>> {
        private ControlSubItemDao controlSubItemDao;

        public GetAllAsyncTask(ControlSubItemDao controlSubItemDao) {
            this.controlSubItemDao = controlSubItemDao;
        }

        @Override
        protected List<ControlSubItemEntity> doInBackground(Void... voids) {
            return controlSubItemDao.findAll();
        }
    }

    //Insert
    private static class InsertAsyncTask extends AsyncTask<ControlSubItemEntity, Void, Void> {
        private ControlSubItemDao controlSubItemDao;

        private InsertAsyncTask(ControlSubItemDao controlSubItemDao) {
            this.controlSubItemDao = controlSubItemDao;
        }

        @Override
        protected Void doInBackground(ControlSubItemEntity... entities) {
            controlSubItemDao.insert(entities[0]);
            return null;
        }
    }


    //Delete
    private static class DeleteAsyncTask extends AsyncTask<Void, Void, Void> {
        private ControlSubItemDao controlSubItemDao;

        private DeleteAsyncTask(ControlSubItemDao controlSubItemDao) {
            this.controlSubItemDao = controlSubItemDao;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            controlSubItemDao.deleteAll();
            return null;
        }
    }
}
