package com.mdp.fusionapp.database.service;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.LugarEntity;

import java.util.List;

@Dao
public interface LugarDao {

    @Insert
    void insert(LugarEntity lugarEntity);

    @Query("DELETE FROM lugar_table")
    void deleteAll();

    @Query("SELECT * FROM lugar_table WHERE idTipoUbicacion=:idTipoUbicacion")
    List<LugarEntity> findTipoLugar(Integer idTipoUbicacion);

}
