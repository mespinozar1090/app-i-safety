package com.mdp.fusionapp.database.entity.interventoria;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "crear_verifiacion_table")
public class CrearVerificacionEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private Integer IdInterventoriaDetalle;
    private Integer IdVerificacionSubitem;
    private Integer IdVerificacion;
    private Integer Cumple;
    private Integer IdInterventoria;
    private Long idOffline;
    private String descripcion;
    private Integer idVerificacionPadre;
    private Integer idVerificacionItem;
    private String descripcionItem;
    private String descripcionPadre;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdVerificacionPadre() {
        return idVerificacionPadre;
    }

    public void setIdVerificacionPadre(Integer idVerificacionPadre) {
        this.idVerificacionPadre = idVerificacionPadre;
    }

    public Integer getIdVerificacionItem() {
        return idVerificacionItem;
    }

    public void setIdVerificacionItem(Integer idVerificacionItem) {
        this.idVerificacionItem = idVerificacionItem;
    }

    public String getDescripcionItem() {
        return descripcionItem;
    }

    public void setDescripcionItem(String descripcionItem) {
        this.descripcionItem = descripcionItem;
    }

    public String getDescripcionPadre() {
        return descripcionPadre;
    }

    public void setDescripcionPadre(String descripcionPadre) {
        this.descripcionPadre = descripcionPadre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdInterventoriaDetalle() {
        return IdInterventoriaDetalle;
    }

    public void setIdInterventoriaDetalle(Integer idInterventoriaDetalle) {
        IdInterventoriaDetalle = idInterventoriaDetalle;
    }

    public Integer getIdVerificacionSubitem() {
        return IdVerificacionSubitem;
    }

    public void setIdVerificacionSubitem(Integer idVerificacionSubitem) {
        IdVerificacionSubitem = idVerificacionSubitem;
    }

    public Integer getIdVerificacion() {
        return IdVerificacion;
    }

    public void setIdVerificacion(Integer idVerificacion) {
        IdVerificacion = idVerificacion;
    }

    public Integer getCumple() {
        return Cumple;
    }

    public void setCumple(Integer cumple) {
        Cumple = cumple;
    }

    public Integer getIdInterventoria() {
        return IdInterventoria;
    }

    public void setIdInterventoria(Integer idInterventoria) {
        IdInterventoria = idInterventoria;
    }

    public Long getIdOffline() {
        return idOffline;
    }

    public void setIdOffline(Long idOffine) {
        this.idOffline = idOffine;
    }
}
