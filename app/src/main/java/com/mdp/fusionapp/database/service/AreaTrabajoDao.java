package com.mdp.fusionapp.database.service;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.AreaTrabajoEntity;

import java.util.List;

@Dao
public interface AreaTrabajoDao {

    @Insert
    void insert(AreaTrabajoEntity areaTrabajoEntity);

    @Query("DELETE FROM area_trabajo_table")
    void deleteAll();

    @Query("SELECT * FROM area_trabajo_table")
    List<AreaTrabajoEntity> findAll();
}
