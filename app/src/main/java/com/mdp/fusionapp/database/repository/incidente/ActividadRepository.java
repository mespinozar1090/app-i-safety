package com.mdp.fusionapp.database.repository.incidente;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.service.incidente.ActividadDao;
import com.mdp.fusionapp.network.response.ActividadEntity;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class ActividadRepository {

    private ActividadDao actividadDao;

    public ActividadRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        this.actividadDao = database.actividadDao();
    }

    public void insert(ActividadEntity actividadEntity) {
        new InsertAsyncTask(actividadDao).execute(actividadEntity);
    }

    public void deleteAll() {
        new DeleteAsyncTask(actividadDao).execute();
    }

    public List<ActividadEntity> findAll() {
        try {
            return new FindAllAsyncTask(actividadDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Select
    private static class FindAllAsyncTask extends AsyncTask<Void, Void, List<ActividadEntity>> {
        private ActividadDao actividadDao;

        private FindAllAsyncTask(ActividadDao actividadDao) {
            this.actividadDao = actividadDao;
        }

        @Override
        protected List<ActividadEntity> doInBackground(Void... voids) {
            return actividadDao.findAll();
        }
    }

    //Insert
    private static class InsertAsyncTask extends AsyncTask<ActividadEntity, Void, Void> {
        private ActividadDao actividadDao;

        private InsertAsyncTask(ActividadDao actividadDao) {
            this.actividadDao = actividadDao;
        }

        @Override
        protected Void doInBackground(ActividadEntity... entities) {
            actividadDao.insert(entities[0]);
            return null;
        }
    }

    //Delete
    private static class DeleteAsyncTask extends AsyncTask<Void, Void, Void> {
        private ActividadDao actividadDao;

        private DeleteAsyncTask(ActividadDao actividadDao) {
            this.actividadDao = actividadDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            actividadDao.deleteAll();
            return null;
        }
    }


}
