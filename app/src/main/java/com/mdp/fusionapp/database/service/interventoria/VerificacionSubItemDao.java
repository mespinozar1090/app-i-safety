package com.mdp.fusionapp.database.service.interventoria;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.interventoria.VerificacionSubItemEntity;
import java.util.List;

@Dao
public interface VerificacionSubItemDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(VerificacionSubItemEntity verificacionSubItemEntity);

    @Query("DELETE FROM verificacion_subitem_table")
    void deleteAll();

    @Query("SELECT * FROM verificacion_subitem_table")
    List<VerificacionSubItemEntity> findAll();

}
