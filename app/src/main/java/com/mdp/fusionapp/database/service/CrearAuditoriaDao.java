package com.mdp.fusionapp.database.service;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.auditoria.CrearAuditoriaEntity;
import com.mdp.fusionapp.database.entity.sbc.CrearSbcEntity;

import java.util.List;

@Dao
public interface CrearAuditoriaDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(CrearAuditoriaEntity crearAuditoriaEntity);

    @Query("DELETE FROM crear_auditoria_table")
    void deleteAll();

    @Query("DELETE FROM crear_auditoria_table WHERE id=:id")
    void deleteById(Integer id);

    @Query("SELECT * FROM crear_auditoria_table")
    List<CrearAuditoriaEntity> findAll();

    @Query("SELECT * FROM crear_auditoria_table where estadoRegistroDB == 2 OR estadoRegistroDB == 1")
    List<CrearAuditoriaEntity> findAllByEstado();

    @Query("SELECT * FROM crear_auditoria_table WHERE id=:id")
    CrearAuditoriaEntity findAllById(Integer id);
}
