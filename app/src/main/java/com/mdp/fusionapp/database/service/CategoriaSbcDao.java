package com.mdp.fusionapp.database.service;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.mdp.fusionapp.database.entity.CategoriaSbcEntity;

import java.util.List;

@Dao
public interface CategoriaSbcDao {

    @Insert
    void insert(CategoriaSbcEntity categoriaSbcEntity);

    @Update
    void update(CategoriaSbcEntity categoriaSbcEntity);

    @Query("DELETE FROM categoria_sbc_table")
    void deleteAll();

    @Query("SELECT * FROM categoria_sbc_table")
    List<CategoriaSbcEntity> findAll();
}
