package com.mdp.fusionapp.database.entity.incidente;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "parte_cuerpo_table")
public class ParteCuerpoEntity {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("Id_Elemento")
    private Integer idElemento;

    @SerializedName("Descripcion_Elemento")
    private String descripcionElemento;

    @SerializedName("Estado_Elemento")
    private Boolean estadoElemento;

    public ParteCuerpoEntity(Integer idElemento, String descripcionElemento) {
        this.idElemento = idElemento;
        this.descripcionElemento = descripcionElemento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdElemento() {
        return idElemento;
    }

    public void setIdElemento(Integer idElemento) {
        this.idElemento = idElemento;
    }

    public String getDescripcionElemento() {
        return descripcionElemento;
    }

    public void setDescripcionElemento(String descripcionElemento) {
        this.descripcionElemento = descripcionElemento;
    }

    public Boolean getEstadoElemento() {
        return estadoElemento;
    }

    public void setEstadoElemento(Boolean estadoElemento) {
        this.estadoElemento = estadoElemento;
    }

    @Override
    public String toString() {
        return descripcionElemento;
    }
}
