package com.mdp.fusionapp.database.service.incidente;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mdp.fusionapp.network.response.ActividadEntity;

import java.util.List;

@Dao
public interface ActividadDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(ActividadEntity actividadEntity);

    @Query("DELETE FROM actividad_table")
    void deleteAll();

    @Query("SELECT * FROM actividad_table")
    List<ActividadEntity> findAll();
}
