package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.CategoriaSbcEntity;
import com.mdp.fusionapp.database.service.CategoriaSbcDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class CategoriaSbcRepository {

    private CategoriaSbcDao categoriaSbcDao;

    public CategoriaSbcRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        this.categoriaSbcDao = database.categoriaSbcDao();
    }

    public void insert(CategoriaSbcEntity entity) {
        new InsertAsyncTask(categoriaSbcDao).execute(entity);
    }

    public void deleteAll() {
        new DeleteAsyncTask(categoriaSbcDao).execute();
    }

    public List<CategoriaSbcEntity> findAll() {
        try {
            return new GetAllAsyncTask(categoriaSbcDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Insert
    private static class InsertAsyncTask extends AsyncTask<CategoriaSbcEntity, Void, Void> {
        private CategoriaSbcDao categoriaSbcDao;

        private InsertAsyncTask(CategoriaSbcDao categoriaSbcDao) {
            this.categoriaSbcDao = categoriaSbcDao;
        }

        @Override
        protected Void doInBackground(CategoriaSbcEntity... entities) {
            categoriaSbcDao.insert(entities[0]);
            return null;
        }
    }

    //Delete
    private static class DeleteAsyncTask extends AsyncTask<Void, Void, Void> {
        private CategoriaSbcDao categoriaSbcDao;

        private DeleteAsyncTask(CategoriaSbcDao categoriaSbcDao) {
            this.categoriaSbcDao = categoriaSbcDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            categoriaSbcDao.deleteAll();
            return null;
        }
    }

    private static class GetAllAsyncTask extends AsyncTask<Void, String, List<CategoriaSbcEntity>> {
        private CategoriaSbcDao categoriaSbcDao;

        public GetAllAsyncTask(CategoriaSbcDao areaTrabajoDao) {
            this.categoriaSbcDao = areaTrabajoDao;
        }

        @Override
        protected List<CategoriaSbcEntity> doInBackground(Void... voids) {
            return  categoriaSbcDao.findAll();
        }
    }
}
