package com.mdp.fusionapp.database.service.interventoria;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.interventoria.CrearInterventoriaEntiy;
import com.mdp.fusionapp.database.entity.sbc.CrearSbcEntity;

import java.util.List;

@Dao
public interface CrearInterventoriaDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(CrearInterventoriaEntiy crearInterventoriaEntiy);

    @Query("DELETE FROM crear_interventoria_table WHERE id=:id")
    void deleteById(Integer id);

    @Query("DELETE FROM crear_interventoria_table")
    void deleteAll();

    @Query("UPDATE  crear_interventoria_table SET estadoRegistroDB = 3 WHERE id=:id  ")
    void updateEstadoEliminar(Integer id);

    @Query("SELECT * FROM crear_interventoria_table")
    List<CrearInterventoriaEntiy> findAll();

    @Query("SELECT * FROM crear_interventoria_table  where estadoRegistroDB == 2 OR estadoRegistroDB == 1 OR estadoRegistroDB == 3")
    List<CrearInterventoriaEntiy> findAllByEstado();

    @Query("SELECT * FROM crear_interventoria_table WHERE id=:id")
    CrearInterventoriaEntiy findAllById(Integer id);

}
