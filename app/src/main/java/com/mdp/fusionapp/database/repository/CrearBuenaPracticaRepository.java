package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.CrearBuenaPracticaEntity;
import com.mdp.fusionapp.database.service.CrearBuenaPracticaDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class CrearBuenaPracticaRepository {

    private CrearBuenaPracticaDao crearBuenaPracticaDao;

    public CrearBuenaPracticaRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        crearBuenaPracticaDao = database.crearBuenaPracticaDao();
    }

    public void insert(CrearBuenaPracticaEntity entity) {
        new InsertAsyncTask(crearBuenaPracticaDao).execute(entity);
    }

    public void deleteAllByInspeccion(Long idInspeccion) {
        new DeleteByInspeccionAsyncTask(crearBuenaPracticaDao).execute(idInspeccion);
    }


    public List<CrearBuenaPracticaEntity> findBuenasPracticasByInspeccion(Long idInspeccionOffline) {
        try {
            return new GetBuenasPracticasAsyncTask(crearBuenaPracticaDao).execute(idInspeccionOffline).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


    //Insert
    private static class InsertAsyncTask extends AsyncTask<CrearBuenaPracticaEntity, Void, Void> {
        private CrearBuenaPracticaDao crearBuenaPracticaDao;

        private InsertAsyncTask(CrearBuenaPracticaDao crearBuenaPracticaDao) {
            this.crearBuenaPracticaDao = crearBuenaPracticaDao;
        }

        @Override
        protected Void doInBackground(CrearBuenaPracticaEntity... entities) {
            crearBuenaPracticaDao.insert(entities[0]);
            return null;
        }
    }

    //Delete
    private static class DeleteByInspeccionAsyncTask extends AsyncTask<Long, Void, Void> {
        private CrearBuenaPracticaDao crearBuenaPracticaDao;

        private DeleteByInspeccionAsyncTask(CrearBuenaPracticaDao crearBuenaPracticaDao) {
            this.crearBuenaPracticaDao = crearBuenaPracticaDao;
        }

        @Override
        protected Void doInBackground(Long... integers) {
            crearBuenaPracticaDao.deleteObjectByIdInspeccion(integers[0]);
            return null;
        }
    }

    //Select
    private static class GetBuenasPracticasAsyncTask extends AsyncTask<Long, String, List<CrearBuenaPracticaEntity>> {
        private CrearBuenaPracticaDao crearBuenaPracticaDao;

        public GetBuenasPracticasAsyncTask(CrearBuenaPracticaDao crearBuenaPracticaDao) {
            this.crearBuenaPracticaDao = crearBuenaPracticaDao;
        }

        @Override
        protected List<CrearBuenaPracticaEntity> doInBackground(Long... integers) {
            return crearBuenaPracticaDao.findCrearBuenaPracticaEntity(integers[0]);
        }
    }
}
