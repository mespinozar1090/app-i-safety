package com.mdp.fusionapp.database.service;

import android.graphics.Bitmap;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.CrearHallazgoEntity;
import com.mdp.fusionapp.database.entity.auditoria.CrearDetalleAudotiraEntity;

import java.util.List;

@Dao
public interface CrearDetalleAuditoriaDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(CrearDetalleAudotiraEntity crearDetalleAudotiraEntity);

    @Query("DELETE FROM crear_detalleauditoria_table WHERE idOffline=:idOffline")
    void deleteByIdAuditoria(Long idOffline);

    @Query("DELETE FROM crear_detalleauditoria_table")
    void deleteAll();

    @Query("SELECT * FROM crear_detalleauditoria_table where idOffline=:idOffline")
    List<CrearDetalleAudotiraEntity> findByIdAuditoria(Long idOffline);

    @Query("UPDATE crear_detalleauditoria_table SET imagenEvidencia=:base64 where id=:idRow")
    void updateImage(String base64, Long idRow);

}
