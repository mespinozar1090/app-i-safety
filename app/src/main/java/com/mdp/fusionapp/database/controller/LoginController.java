package com.mdp.fusionapp.database.controller;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.MainThread;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.mdp.fusionapp.database.entity.ContratistaEntity;
import com.mdp.fusionapp.database.entity.EmpresaMaestraEntity;
import com.mdp.fusionapp.database.entity.ProyectoEntity;
import com.mdp.fusionapp.database.entity.UsuarioEntity;
import com.mdp.fusionapp.database.service.EmpresaMaestraDao;
import com.mdp.fusionapp.database.serviceImpl.ProyectoServiceImpl;
import com.mdp.fusionapp.database.serviceImpl.UsuarioServiceImpl;
import com.mdp.fusionapp.model.UsuarioModel;
import com.mdp.fusionapp.network.response.ContratistaResponse;
import com.mdp.fusionapp.network.response.EmpresaProyectoResponse;
import com.mdp.fusionapp.network.response.LoginResponse;
import com.mdp.fusionapp.network.response.ProyectoResponse;
import com.mdp.fusionapp.network.response.TipoUbicacionResponse;
import com.mdp.fusionapp.ui.activity.NavigationActivity;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.viewModel.AsynAuditoriaViewModel;
import com.mdp.fusionapp.viewModel.AsynSbcViewModel;
import com.mdp.fusionapp.viewModel.AsyncInspeccionViewModel;
import com.mdp.fusionapp.viewModel.IncidenteViewModel;
import com.mdp.fusionapp.viewModel.InspeccionViewModel;
import com.mdp.fusionapp.viewModel.InterventoriaViewModel;
import com.mdp.fusionapp.viewModel.ListaAuditoriaViewModel;
import com.mdp.fusionapp.viewModel.LoginViewModel;
import com.mdp.fusionapp.viewModel.SbcViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class LoginController extends AppCompatActivity {

    private LoginViewModel loginViewModel;
    private InterventoriaViewModel interventoriaViewModel;
    private InspeccionViewModel inspeccionViewModel;
    private UsuarioServiceImpl usuarioServiceImpl;
    private ListaAuditoriaViewModel auditoriaViewModel;
    private ProyectoServiceImpl proyectoServiceImpl;
    private SbcViewModel sbcViewModel;
    private AsyncInspeccionViewModel asyncInspeccionViewModel;
    private AsynSbcViewModel asynSbcViewModel;
    private AsynAuditoriaViewModel asynAuditoriaViewModel;
    private IncidenteViewModel incidenteViewModel;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResourceId());
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        inspeccionViewModel = new ViewModelProvider(this).get(InspeccionViewModel.class);
        usuarioServiceImpl = new ViewModelProvider(this).get(UsuarioServiceImpl.class);
        proyectoServiceImpl = new ViewModelProvider(this).get(ProyectoServiceImpl.class);
        sbcViewModel = new ViewModelProvider(this).get(SbcViewModel.class);
        auditoriaViewModel = new ViewModelProvider(this).get(ListaAuditoriaViewModel.class);
        interventoriaViewModel = new ViewModelProvider(this).get(InterventoriaViewModel.class);

        asynAuditoriaViewModel = new ViewModelProvider(this).get(AsynAuditoriaViewModel.class);
        asyncInspeccionViewModel = new ViewModelProvider(this).get(AsyncInspeccionViewModel.class);
        asynSbcViewModel = new ViewModelProvider(this).get(AsynSbcViewModel.class);

        incidenteViewModel = new ViewModelProvider(this).get(IncidenteViewModel.class);

        sharedPreferences = getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

    }

    protected abstract int getLayoutResourceId();

    public void validarUsuarioDB(String userName, String password) {
        UsuarioEntity usuario = usuarioServiceImpl.getByUserName(userName);
        if (null != usuario) {
            Log.e("SUCCCES1", "usuarioEntity correctas");
            validarLoginAndPasswordDB(userName, password);
        } else {
            UtilMDP.hideProgreesDialog();
            Toast.makeText(this, "Usuario no registrado en el aplicativo", Toast.LENGTH_SHORT).show();
        }
    }

    public void validarLoginAndPasswordDB(String usuario, String clave) {
        usuarioServiceImpl.getByUserAndPassword(usuario, clave).observe(this, new Observer<UsuarioEntity>() {
            @Override
            public void onChanged(UsuarioEntity usuarioEntity) {
                if (null != usuarioEntity) {
                    showActivityNavigation(usuarioEntity);
                } else {
                    UtilMDP.hideProgreesDialog();
                    if (usuario.toLowerCase().contains("@rep.com.pe")) {
                        showModal("Ahora puedes ingresar con tu contraseña de tu cuenta de REP, en caso el error persista significa que tu correo no se encuentra en el AD.");
                    }else{
                        showModal("Ingresar correctamente la contraseña. En caso el error persista por favor, contactarse con el administrador del sistema.");
                    }
                  //  Log.e("ERROR", "Credenciales incorrectas");
                }
            }
        });
    }

    public void showModal(String mensaje) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Contraseña Incorrecta");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage(mensaje);
        alertDialogBuilder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void insertUser(LoginResponse loginResponse) {
        UsuarioEntity usuarioEntity = new UsuarioEntity();
        usuarioEntity.setRuc(loginResponse.getData().getRuc());
        usuarioEntity.setIdEmpresa(loginResponse.getData().getIdEmpresa());
        usuarioEntity.setIdRolGeneral(loginResponse.getData().getIdRolGeneral());
        usuarioEntity.setIdRolUsuario(loginResponse.getData().getIdRolUsuario());
        usuarioEntity.setIdUsers(loginResponse.getData().getIdUsers());
        usuarioEntity.setUsuario(loginResponse.getData().getUsuario_Login());
        usuarioEntity.setClave(loginResponse.getData().getContraseña_Login());
        usuarioEntity.setDescripcionEmpresa(loginResponse.getData().getDescripcionEmpresa());
        usuarioEntity.setDescripcionRol(loginResponse.getData().getDescripcionRol());
        usuarioEntity.setDescripcionRolUsuario(loginResponse.getData().getDescripcionRolUsuario());
        usuarioEntity.setEmail(loginResponse.getData().getEmailCorporativo());
        usuarioEntity.setIdPerfilUsuario(loginResponse.getData().getIdPerfilUsuario());
        usuarioEntity.setIdRazonSocial(loginResponse.getData().getIdRazonSocial());
        usuarioEntity.setNombreUsuario(loginResponse.getData().getNombreUsuario());

        UsuarioEntity usuarios = usuarioServiceImpl.getByUserName(usuarioEntity.getUsuario());
        if (usuarios != null) {
            usuarioServiceImpl.delete(usuarios);
        }
        usuarioServiceImpl.insert(usuarioEntity);

        showActivityNavigation(usuarioEntity);
    }

    private void showActivityNavigation(UsuarioEntity usuarioEntity) {

        UsuarioModel usuarioModel = new UsuarioModel();
        usuarioModel.setNombreUsuario(usuarioEntity.getNombreUsuario());
        usuarioModel.setDescripcionRol(usuarioEntity.getDescripcionRol());

        editor.putInt("IdUsers", usuarioEntity.getIdUsers());
        editor.putString("usuario", usuarioEntity.getNombreUsuario());
        editor.putString("nombreUsuario", usuarioEntity.getNombreUsuario());
        editor.putString("rol", usuarioEntity.getDescripcionRol());
        editor.putString("Descripcion_Rol_Usuario", usuarioEntity.getDescripcionRolUsuario());
        editor.putInt("Id_Rol_Usuario", usuarioEntity.getIdRolUsuario());
        editor.putInt("Id_Rol_General", usuarioEntity.getIdRolGeneral());
        editor.putInt("IdPerfil_Usuario", usuarioEntity.getIdPerfilUsuario());
        editor.putInt("Id_Empresa", usuarioEntity.getIdEmpresa());
        editor.apply();

        inspeccionViewModel.obtenerUsuarios();
        inspeccionViewModel.categoriaBuenaPractica();
        loginViewModel.obtenerUsuarioNotificar();


        subProcesos1();
        subProcesos2();
        subProcesos3();
        subProcesos4();
        subProcesos5();
        subProcesos6();
        subProcesos7();
        subProcesos8();
        subProcesos9();
        subProcesos10();
        subProcesos11();
        subProcesos12();
        subProcesos13();
        subProcesos14();
        subProcesos15();
        subProcesos16();
        subProcesos17();
        subProcesos18();
        subProcesos19();
        subProcesos20();
        subProcesos21();


        UtilMDP.hideProgreesDialog();
        Intent intent = new Intent(LoginController.this, NavigationActivity.class);
        intent.putExtra("usuarioModel", usuarioModel);
        startActivity(intent);
        //   finish();
    }

    private void subProcesos1() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("Modulo", "offline");
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        inspeccionViewModel.obtenerProyectoOffline(map);
                    }
                });
            }
        }).start();
    }

    private void subProcesos2() {
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        inspeccionViewModel.obtenerTipoUbicacionOffline();
                    }
                });
            }
        }).start();
    }

    private void subProcesos3() {
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        inspeccionViewModel.obtenerLugarOffline();
                    }
                });
            }
        }).start();
    }

    private void subProcesos4() {
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        sbcViewModel.obtenerEmpresaSBSResponse();


                    }
                });
            }
        }).start();
    }

    private void subProcesos5() {
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        sbcViewModel.obtenerCategoriaItemSBC();
                    }
                });
            }
        }).start();
    }

    private void subProcesos6() {
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        inspeccionViewModel.obtenerAreaInspeccion();
                    }
                });
            }
        }).start();
    }

    private void subProcesos7() {
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        sbcViewModel.obtenerCategoriaSBC();
                    }
                });
            }
        }).start();
    }

    private void subProcesos8() {
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        inspeccionViewModel.obtenerEmpresaMaestraOffline();
                    }
                });
            }
        }).start();
    }

    private void subProcesos9() {
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        inspeccionViewModel.obtenerContratistaOffline();
                    }
                });
            }
        }).start();
    }

    private void subProcesos10() {
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        auditoriaViewModel.obtenerLineaminetos();
                    }
                });
            }
        }).start();
    }

    private void subProcesos11() {
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        auditoriaViewModel.obtenerLineaminetosSub();

                    }
                });
            }
        }).start();
    }

    private void subProcesos12() {
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        sbcViewModel.obtenerEspecialidad();
                    }
                });
            }
        }).start();
    }

    private void subProcesos13() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("Modulo", "offline");

        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        sbcViewModel.obtenerSedeM(hashMap);
                    }
                });
            }
        }).start();
    }

    private void subProcesos14() {
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        interventoriaViewModel.listaVerificacionesSubitems();
                    }
                });
            }
        }).start();
    }

    private void subProcesos15() {
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        interventoriaViewModel.obtenerControlSubItem();
                    }
                });
            }
        }).start();
    }

    private void subProcesos16() {
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String[] array = {"1", "1"};
                        inspeccionViewModel.obtenerComboMaestro(array);
                    }
                });
            }
        }).start();
    }

    private void subProcesos17() {
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        inspeccionViewModel.obtenerEmpresa();
                    }
                });
            }
        }).start();
    }

    private void subProcesos18() {
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        inspeccionViewModel.obtenerActividad();
                    }
                });
            }
        }).start();
    }

    private void subProcesos19() {
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String[] array = {"4", "1"};
                        inspeccionViewModel.tipoEvento(array);
                    }
                });
            }
        }).start();
    }

    private void subProcesos20() {
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String[] array = {"tb_MAESTRA_Equipos", "1"};
                        incidenteViewModel.equipoInvolucrado(array);
                    }
                });
            }
        }).start();
    }

    private void subProcesos21() {
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String[] array = {"tb_MAESTRA_ParteCuerpo", "1"};
                        incidenteViewModel.parteCuerpoInvolucrado(array);
                    }
                });
            }
        }).start();
    }


}
