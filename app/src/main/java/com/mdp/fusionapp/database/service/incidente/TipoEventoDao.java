package com.mdp.fusionapp.database.service.incidente;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.incidente.TipoEventoEntity;
import com.mdp.fusionapp.network.response.ActividadEntity;

import java.util.List;

@Dao
public interface TipoEventoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(TipoEventoEntity tipoEventoEntity);

    @Query("DELETE FROM tipo_evento_table")
    void deleteAll();

    @Query("SELECT * FROM tipo_evento_table")
    List<TipoEventoEntity> findAll();
}
