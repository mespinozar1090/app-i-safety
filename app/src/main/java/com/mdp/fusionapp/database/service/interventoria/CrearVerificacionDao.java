package com.mdp.fusionapp.database.service.interventoria;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.interventoria.CrearVerificacionEntity;

import java.util.List;

@Dao
public interface CrearVerificacionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(CrearVerificacionEntity crearVerificacionEntity);

    @Query("DELETE FROM crear_verifiacion_table")
    void deleteAll();

    @Query("DELETE FROM crear_verifiacion_table WHERE idOffline=:idOffline")
    void deleteById(Long idOffline);

    @Query("SELECT * FROM crear_verifiacion_table")
    List<CrearVerificacionEntity> findAll();

    @Query("SELECT * FROM crear_verifiacion_table where idOffline=:idOffline")
    List<CrearVerificacionEntity> findAllById(Long idOffline);
}
