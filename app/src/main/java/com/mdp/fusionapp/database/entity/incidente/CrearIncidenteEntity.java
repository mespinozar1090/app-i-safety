package com.mdp.fusionapp.database.entity.incidente;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "crear_incidente_table")
public class CrearIncidenteEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private Integer idIncidente;
    private String empleadorTipo;
    private Integer fempleadorIdActividadEconomica;
    private Integer fempleadorIdEmpresa;
    private Integer fempleadorIdTamanioEmpresa;
    private Integer fempleadorIIdActividadEconomica;
    private String fempleadorIRazonSocial;
    private String fempleadorIRuc;
    private String fempleadorRazonSocial;
    private String fempleadorRuc;
    private String feventoFechaAccidente;
    private String feventoFechaInicioInvestigacion;
    private String feventoHoraAccidente;
    private String feventoHuboDanioMaterial;
    private Integer feventoIdEquipoAfectado;
    private Integer feventoIdParteAfectada;
    private Integer feventoIdTipoEvento;
    private String feventoLugarExacto;
    private String feventoNumTrabajadoresAfectadas;
    private String ftrabajadorDetalleCategoriaOcupacional;
    private Integer pempleadorIdEmpresa;
    private Integer fempleadorIIdEmpresa;
    private String ftrabajadorNombresApellidos;
    private Integer gidProyectoSede;
    private String gfechaCreado;
    private String gidFechaModifica;
    private Integer gidUsuarioCreado;
    private Integer gidUsuarioModifica;

    private String fdescripcionIncidente;
    private String gdescripcionFormato;
    private Integer estadoRegistroDB;
    private String tipoInformePF;

    public String getGdescripcionFormato() {
        return gdescripcionFormato;
    }

    public void setGdescripcionFormato(String gdescripcionFormato) {
        this.gdescripcionFormato = gdescripcionFormato;
    }

    public String getTipoInformePF() {
        return tipoInformePF;
    }

    public void setTipoInformePF(String tipoInformePF) {
        this.tipoInformePF = tipoInformePF;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdIncidente() {
        return idIncidente;
    }

    public void setIdIncidente(Integer idIncidente) {
        this.idIncidente = idIncidente;
    }

    public String getEmpleadorTipo() {
        return empleadorTipo;
    }

    public void setEmpleadorTipo(String empleadorTipo) {
        this.empleadorTipo = empleadorTipo;
    }

    public Integer getFempleadorIdActividadEconomica() {
        return fempleadorIdActividadEconomica;
    }

    public void setFempleadorIdActividadEconomica(Integer fempleadorIdActividadEconomica) {
        this.fempleadorIdActividadEconomica = fempleadorIdActividadEconomica;
    }

    public Integer getFempleadorIdEmpresa() {
        return fempleadorIdEmpresa;
    }

    public void setFempleadorIdEmpresa(Integer fempleadorIdEmpresa) {
        this.fempleadorIdEmpresa = fempleadorIdEmpresa;
    }

    public Integer getFempleadorIdTamanioEmpresa() {
        return fempleadorIdTamanioEmpresa;
    }

    public void setFempleadorIdTamanioEmpresa(Integer fempleadorIdTamanioEmpresa) {
        this.fempleadorIdTamanioEmpresa = fempleadorIdTamanioEmpresa;
    }

    public Integer getFempleadorIIdActividadEconomica() {
        return fempleadorIIdActividadEconomica;
    }

    public void setFempleadorIIdActividadEconomica(Integer fempleadorIIdActividadEconomica) {
        this.fempleadorIIdActividadEconomica = fempleadorIIdActividadEconomica;
    }

    public String getFempleadorIRazonSocial() {
        return fempleadorIRazonSocial;
    }

    public void setFempleadorIRazonSocial(String fempleadorIRazonSocial) {
        this.fempleadorIRazonSocial = fempleadorIRazonSocial;
    }

    public String getFempleadorIRuc() {
        return fempleadorIRuc;
    }

    public void setFempleadorIRuc(String fempleadorIRuc) {
        this.fempleadorIRuc = fempleadorIRuc;
    }

    public String getFempleadorRazonSocial() {
        return fempleadorRazonSocial;
    }

    public void setFempleadorRazonSocial(String fempleadorRazonSocial) {
        this.fempleadorRazonSocial = fempleadorRazonSocial;
    }

    public String getFempleadorRuc() {
        return fempleadorRuc;
    }

    public void setFempleadorRuc(String fempleadorRuc) {
        this.fempleadorRuc = fempleadorRuc;
    }

    public String getFeventoFechaAccidente() {
        return feventoFechaAccidente;
    }

    public void setFeventoFechaAccidente(String feventoFechaAccidente) {
        this.feventoFechaAccidente = feventoFechaAccidente;
    }

    public String getFeventoFechaInicioInvestigacion() {
        return feventoFechaInicioInvestigacion;
    }

    public void setFeventoFechaInicioInvestigacion(String feventoFechaInicioInvestigacion) {
        this.feventoFechaInicioInvestigacion = feventoFechaInicioInvestigacion;
    }

    public String getFeventoHoraAccidente() {
        return feventoHoraAccidente;
    }

    public void setFeventoHoraAccidente(String feventoHoraAccidente) {
        this.feventoHoraAccidente = feventoHoraAccidente;
    }

    public String getFeventoHuboDanioMaterial() {
        return feventoHuboDanioMaterial;
    }

    public void setFeventoHuboDanioMaterial(String feventoHuboDanioMaterial) {
        this.feventoHuboDanioMaterial = feventoHuboDanioMaterial;
    }

    public Integer getFeventoIdEquipoAfectado() {
        return feventoIdEquipoAfectado;
    }

    public void setFeventoIdEquipoAfectado(Integer feventoIdEquipoAfectado) {
        this.feventoIdEquipoAfectado = feventoIdEquipoAfectado;
    }

    public Integer getFeventoIdParteAfectada() {
        return feventoIdParteAfectada;
    }

    public void setFeventoIdParteAfectada(Integer feventoIdParteAfectada) {
        this.feventoIdParteAfectada = feventoIdParteAfectada;
    }

    public Integer getFeventoIdTipoEvento() {
        return feventoIdTipoEvento;
    }

    public void setFeventoIdTipoEvento(Integer feventoIdTipoEvento) {
        this.feventoIdTipoEvento = feventoIdTipoEvento;
    }

    public String getFeventoLugarExacto() {
        return feventoLugarExacto;
    }

    public void setFeventoLugarExacto(String feventoLugarExacto) {
        this.feventoLugarExacto = feventoLugarExacto;
    }

    public String getFeventoNumTrabajadoresAfectadas() {
        return feventoNumTrabajadoresAfectadas;
    }

    public void setFeventoNumTrabajadoresAfectadas(String feventoNumTrabajadoresAfectadas) {
        this.feventoNumTrabajadoresAfectadas = feventoNumTrabajadoresAfectadas;
    }

    public String getFtrabajadorDetalleCategoriaOcupacional() {
        return ftrabajadorDetalleCategoriaOcupacional;
    }

    public void setFtrabajadorDetalleCategoriaOcupacional(String ftrabajadorDetalleCategoriaOcupacional) {
        this.ftrabajadorDetalleCategoriaOcupacional = ftrabajadorDetalleCategoriaOcupacional;
    }

    public Integer getPempleadorIdEmpresa() {
        return pempleadorIdEmpresa;
    }

    public void setPempleadorIdEmpresa(Integer pempleadorIdEmpresa) {
        this.pempleadorIdEmpresa = pempleadorIdEmpresa;
    }

    public Integer getFempleadorIIdEmpresa() {
        return fempleadorIIdEmpresa;
    }

    public void setFempleadorIIdEmpresa(Integer fempleadorIIdEmpresa) {
        this.fempleadorIIdEmpresa = fempleadorIIdEmpresa;
    }

    public String getFtrabajadorNombresApellidos() {
        return ftrabajadorNombresApellidos;
    }

    public void setFtrabajadorNombresApellidos(String ftrabajadorNombresApellidos) {
        this.ftrabajadorNombresApellidos = ftrabajadorNombresApellidos;
    }

    public Integer getGidProyectoSede() {
        return gidProyectoSede;
    }

    public void setGidProyectoSede(Integer gidProyectoSede) {
        this.gidProyectoSede = gidProyectoSede;
    }

    public String getGfechaCreado() {
        return gfechaCreado;
    }

    public void setGfechaCreado(String gfechaCreado) {
        this.gfechaCreado = gfechaCreado;
    }

    public String getGidFechaModifica() {
        return gidFechaModifica;
    }

    public void setGidFechaModifica(String gidFechaModifica) {
        this.gidFechaModifica = gidFechaModifica;
    }

    public Integer getGidUsuarioCreado() {
        return gidUsuarioCreado;
    }

    public void setGidUsuarioCreado(Integer gidUsuarioCreado) {
        this.gidUsuarioCreado = gidUsuarioCreado;
    }

    public Integer getGidUsuarioModifica() {
        return gidUsuarioModifica;
    }

    public void setGidUsuarioModifica(Integer gidUsuarioModifica) {
        this.gidUsuarioModifica = gidUsuarioModifica;
    }

    public String getFdescripcionIncidente() {
        return fdescripcionIncidente;
    }

    public void setFdescripcionIncidente(String fdescripcionIncidente) {
        this.fdescripcionIncidente = fdescripcionIncidente;
    }

    public Integer getEstadoRegistroDB() {
        return estadoRegistroDB;
    }

    public void setEstadoRegistroDB(Integer estadoRegistroDB) {
        this.estadoRegistroDB = estadoRegistroDB;
    }
}
