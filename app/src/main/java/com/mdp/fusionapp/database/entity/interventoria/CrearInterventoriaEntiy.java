package com.mdp.fusionapp.database.entity.interventoria;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "crear_interventoria_table")
public class CrearInterventoriaEntiy {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private Integer idInterventoria;
    private String actividad;
    private String codigoInterventoria;
    private String conclucion;
    private Integer empresaIntervenida;
    private Integer idEmpresaIntervenida;
    private Integer empresaInterventor;
    private Boolean estado;
    private String fechaHora;
    private String fechaRegistro;
    private Integer idPlanTrabajo;
    private Integer planTrabajo;
    private Integer idUsuario;
    private String interventor;
    private Integer lineaSubEstacion;
    private Integer tipoUbicacion;
    private Integer lugarZona;
    private String nroPlanTrabajo;
    private String supervisor;
    private String supervisorSustituto;
    private String nombreFormato;
    private String nombreObservador;
    private Integer estadoRegistroDB = 0;


    public Integer getTipoUbicacion() {
        return tipoUbicacion;
    }

    public void setTipoUbicacion(Integer tipoUbicacion) {
        this.tipoUbicacion = tipoUbicacion;
    }

    public String getNombreObservador() {
        return nombreObservador;
    }

    public void setNombreObservador(String nombreObservador) {
        this.nombreObservador = nombreObservador;
    }

    public String getCodigoInterventoria() {
        return codigoInterventoria;
    }

    public void setCodigoInterventoria(String codigoInterventoria) {
        this.codigoInterventoria = codigoInterventoria;
    }

    public Integer getIdEmpresaIntervenida() {
        return idEmpresaIntervenida;
    }

    public void setIdEmpresaIntervenida(Integer idEmpresaIntervenida) {
        this.idEmpresaIntervenida = idEmpresaIntervenida;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdInterventoria() {
        return idInterventoria;
    }

    public void setIdInterventoria(Integer idInterventoria) {
        this.idInterventoria = idInterventoria;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getConclucion() {
        return conclucion;
    }

    public void setConclucion(String conclucion) {
        this.conclucion = conclucion;
    }

    public Integer getEmpresaIntervenida() {
        return empresaIntervenida;
    }

    public void setEmpresaIntervenida(Integer empresaIntervenida) {
        this.empresaIntervenida = empresaIntervenida;
    }

    public Integer getEmpresaInterventor() {
        return empresaInterventor;
    }

    public void setEmpresaInterventor(Integer empresaInterventor) {
        this.empresaInterventor = empresaInterventor;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getIdPlanTrabajo() {
        return idPlanTrabajo;
    }

    public void setIdPlanTrabajo(Integer idPlanTrabajo) {
        this.idPlanTrabajo = idPlanTrabajo;
    }

    public Integer getPlanTrabajo() {
        return planTrabajo;
    }

    public void setPlanTrabajo(Integer planTrabajo) {
        this.planTrabajo = planTrabajo;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getInterventor() {
        return interventor;
    }

    public void setInterventor(String interventor) {
        this.interventor = interventor;
    }

    public Integer getLineaSubEstacion() {
        return lineaSubEstacion;
    }

    public void setLineaSubEstacion(Integer lineaSubEstacion) {
        this.lineaSubEstacion = lineaSubEstacion;
    }

    public Integer getLugarZona() {
        return lugarZona;
    }

    public void setLugarZona(Integer lugarZona) {
        this.lugarZona = lugarZona;
    }

    public String getNroPlanTrabajo() {
        return nroPlanTrabajo;
    }

    public void setNroPlanTrabajo(String nroPlanTrabajo) {
        this.nroPlanTrabajo = nroPlanTrabajo;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    public String getSupervisorSustituto() {
        return supervisorSustituto;
    }

    public void setSupervisorSustituto(String supervisorSustituto) {
        this.supervisorSustituto = supervisorSustituto;
    }

    public String getNombreFormato() {
        return nombreFormato;
    }

    public void setNombreFormato(String nombreFormato) {
        this.nombreFormato = nombreFormato;
    }

    public Integer getEstadoRegistroDB() {
        return estadoRegistroDB;
    }

    public void setEstadoRegistroDB(Integer estadoRegistroDB) {
        this.estadoRegistroDB = estadoRegistroDB;
    }
}
