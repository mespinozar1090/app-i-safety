package com.mdp.fusionapp.database.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "categoria_sbcitem_table")
public class CategoriaSbcItemEntity implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("Id_sbc_Categoria_Items")
    private Integer idsbcCategoriaItems;

    @SerializedName("Descripcion_Item")
    private String descripcionItem;

    @SerializedName("Id_sbc_Categoria")
    private Integer idsbcCategoria;

    private Integer Idbarrera = 1;
    private String observacionSbcDetalle = "";
    private Boolean riesgoso = false;
    private Boolean seguro = false;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdsbcCategoriaItems() {
        return idsbcCategoriaItems;
    }

    public void setIdsbcCategoriaItems(Integer idsbcCategoriaItems) {
        this.idsbcCategoriaItems = idsbcCategoriaItems;
    }

    public String getDescripcionItem() {
        return descripcionItem;
    }

    public void setDescripcionItem(String descripcionItem) {
        this.descripcionItem = descripcionItem;
    }

    public Integer getIdsbcCategoria() {
        return idsbcCategoria;
    }

    public void setIdsbcCategoria(Integer idsbcCategoria) {
        this.idsbcCategoria = idsbcCategoria;
    }

    public String getObservacionSbcDetalle() {
        return observacionSbcDetalle;
    }

    public void setObservacionSbcDetalle(String observacionSbcDetalle) {
        this.observacionSbcDetalle = observacionSbcDetalle;
    }

    public Boolean getRiesgoso() {
        return riesgoso;
    }

    public void setRiesgoso(Boolean riesgoso) {
        this.riesgoso = riesgoso;
    }

    public Boolean getSeguro() {
        return seguro;
    }

    public void setSeguro(Boolean seguro) {
        this.seguro = seguro;
    }

    public Integer getIdbarrera() {
        return Idbarrera;
    }

    public void setIdbarrera(Integer idbarrera) {
        Idbarrera = idbarrera;
    }
}
