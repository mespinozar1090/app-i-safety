package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.service.AreaTrabajoDao;
import com.mdp.fusionapp.database.entity.AreaTrabajoEntity;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class AreaTrabajoRepository {

    private AreaTrabajoDao areaTrabajoDao;

    public AreaTrabajoRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        this.areaTrabajoDao = database.areaTrabajoDao();
    }

    public void insert(AreaTrabajoEntity entity) {
        new InsertAsyncTask(areaTrabajoDao).execute(entity);
    }

    public void deleteAll() {
        new DeleteAsyncTask(areaTrabajoDao).execute();
    }

    public List<AreaTrabajoEntity> findAll() {
        try {
            return new GetAllAsyncTask(areaTrabajoDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Insert
    private static class InsertAsyncTask extends AsyncTask<AreaTrabajoEntity, Void, Void> {
        private AreaTrabajoDao areaTrabajoDao;

        private InsertAsyncTask(AreaTrabajoDao areaTrabajoDao) {
            this.areaTrabajoDao = areaTrabajoDao;
        }

        @Override
        protected Void doInBackground(AreaTrabajoEntity... entities) {
            areaTrabajoDao.insert(entities[0]);
            return null;
        }
    }

    //Delete
    private static class DeleteAsyncTask extends AsyncTask<Void, Void, Void> {
        private AreaTrabajoDao areaTrabajoDao;

        private DeleteAsyncTask(AreaTrabajoDao areaTrabajoDao) {
            this.areaTrabajoDao = areaTrabajoDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            areaTrabajoDao.deleteAll();
            return null;
        }
    }

    private static class GetAllAsyncTask extends AsyncTask<Integer, String, List<AreaTrabajoEntity>> {
        private AreaTrabajoDao areaTrabajoDao;

        public GetAllAsyncTask(AreaTrabajoDao areaTrabajoDao) {
            this.areaTrabajoDao = areaTrabajoDao;
        }

        @Override
        protected List<AreaTrabajoEntity> doInBackground(Integer... integers) {
            return areaTrabajoDao.findAll();
        }
    }
}
