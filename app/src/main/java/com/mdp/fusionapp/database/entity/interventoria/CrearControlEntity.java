package com.mdp.fusionapp.database.entity.interventoria;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "crear_control_table")
public class CrearControlEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private Double Cantidad;
    private Integer IdControl;
    private Integer IdControlItem;
    private Integer idControlDetalle;
    private Integer idInteventoria;
    private String descripcion;
    private Long idOffline;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdInteventoria() {
        return idInteventoria;
    }

    public void setIdInteventoria(Integer idInteventoria) {
        this.idInteventoria = idInteventoria;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getCantidad() {
        return Cantidad;
    }

    public void setCantidad(Double cantidad) {
        Cantidad = cantidad;
    }

    public Integer getIdControl() {
        return IdControl;
    }

    public void setIdControl(Integer idControl) {
        IdControl = idControl;
    }

    public Integer getIdControlItem() {
        return IdControlItem;
    }

    public void setIdControlItem(Integer idControlItem) {
        IdControlItem = idControlItem;
    }

    public Integer getIdControlDetalle() {
        return idControlDetalle;
    }

    public void setIdControlDetalle(Integer idControlDetalle) {
        this.idControlDetalle = idControlDetalle;
    }

    public Long getIdOffline() {
        return idOffline;
    }

    public void setIdOffline(Long idOffline) {
        this.idOffline = idOffline;
    }
}
