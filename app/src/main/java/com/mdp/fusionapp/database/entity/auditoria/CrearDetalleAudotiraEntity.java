package com.mdp.fusionapp.database.entity.auditoria;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "crear_detalleauditoria_table")
public class CrearDetalleAudotiraEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private Long idOffline;
    private Integer idAudit;

    private Integer idAuditDetalle;
    private Integer idAuditItems;
    private Integer idAuditLinSubItems;
    private Integer calificacion;
    private Integer lugar;
    private String notas;
    private String textEvidencia;
    private String descripcionItem;

    private Integer idElementoEvidencia;
    private String imagenEvidencia;
    private String nombreImagenEvidencia;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Long getIdOffline() {
        return idOffline;
    }

    public void setIdOffline(Long idOffline) {
        this.idOffline = idOffline;
    }

    public Integer getIdAudit() {
        return idAudit;
    }

    public void setIdAudit(Integer idAudit) {
        this.idAudit = idAudit;
    }

    public Integer getIdAuditDetalle() {
        return idAuditDetalle;
    }

    public void setIdAuditDetalle(Integer idAuditDetalle) {
        this.idAuditDetalle = idAuditDetalle;
    }

    public Integer getIdAuditItems() {
        return idAuditItems;
    }

    public void setIdAuditItems(Integer idAuditItems) {
        this.idAuditItems = idAuditItems;
    }

    public Integer getIdAuditLinSubItems() {
        return idAuditLinSubItems;
    }

    public void setIdAuditLinSubItems(Integer idAuditLinSubItems) {
        this.idAuditLinSubItems = idAuditLinSubItems;
    }

    public Integer getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(Integer calificacion) {
        this.calificacion = calificacion;
    }

    public Integer getLugar() {
        return lugar;
    }

    public void setLugar(Integer lugar) {
        this.lugar = lugar;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public String getTextEvidencia() {
        return textEvidencia;
    }

    public void setTextEvidencia(String textEvidencia) {
        this.textEvidencia = textEvidencia;
    }

    public String getDescripcionItem() {
        return descripcionItem;
    }

    public void setDescripcionItem(String descripcionItem) {
        this.descripcionItem = descripcionItem;
    }

    public Integer getIdElementoEvidencia() {
        return idElementoEvidencia;
    }

    public void setIdElementoEvidencia(Integer idElementoEvidencia) {
        this.idElementoEvidencia = idElementoEvidencia;
    }

    public String getImagenEvidencia() {
        return imagenEvidencia;
    }

    public void setImagenEvidencia(String imagenEvidencia) {
        this.imagenEvidencia = imagenEvidencia;
    }

    public String getNombreImagenEvidencia() {
        return nombreImagenEvidencia;
    }

    public void setNombreImagenEvidencia(String nombreImagenEvidencia) {
        this.nombreImagenEvidencia = nombreImagenEvidencia;
    }
}
