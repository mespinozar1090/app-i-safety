package com.mdp.fusionapp.database.service;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.CategoriaBuenaPracticaEntity;
import com.mdp.fusionapp.database.entity.ContratistaEntity;

import java.util.List;

@Dao
public interface CategoriaBuenaPracticaDao {

    @Insert
    void insert(CategoriaBuenaPracticaEntity categoriaBuenaPracticaEntity);

    @Query("DELETE FROM categoria_buenapractica_table")
    void deleteAll();

    @Query("SELECT * FROM categoria_buenapractica_table")
    List<CategoriaBuenaPracticaEntity> findCategoriaBuenaPractica();

}
