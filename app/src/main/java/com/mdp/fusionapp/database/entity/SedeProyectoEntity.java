package com.mdp.fusionapp.database.entity;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "sede_proyecto_table")
public class SedeProyectoEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("IdSede_Proyecto")
    private Integer idSedeProyecto;

    @SerializedName("Descripcion")
    private String descripcion;

    @SerializedName("Estado_Sede_Proyecto")
    private Boolean estadoSedeProyecto;

    @SerializedName("Modulo")
    private String modulo;

    @Ignore
    public SedeProyectoEntity() {
    }


    public SedeProyectoEntity(Integer idSedeProyecto, String descripcion, Boolean estadoSedeProyecto) {
        this.idSedeProyecto = idSedeProyecto;
        this.descripcion = descripcion;
        this.estadoSedeProyecto = estadoSedeProyecto;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdSedeProyecto() {
        return idSedeProyecto;
    }

    public void setIdSedeProyecto(Integer idSedeProyecto) {
        this.idSedeProyecto = idSedeProyecto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getEstadoSedeProyecto() {
        return estadoSedeProyecto;
    }

    public void setEstadoSedeProyecto(Boolean estadoSedeProyecto) {
        this.estadoSedeProyecto = estadoSedeProyecto;
    }

    @Override
    public String toString() {
        return descripcion;
    }
}
