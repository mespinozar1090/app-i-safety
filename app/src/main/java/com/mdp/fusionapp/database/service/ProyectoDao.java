package com.mdp.fusionapp.database.service;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.mdp.fusionapp.database.entity.ProyectoEntity;
import com.mdp.fusionapp.database.entity.UsuarioEntity;

import java.util.List;

@Dao
public interface ProyectoDao {

    @Insert
    void insert(ProyectoEntity proyectoEntity);

    @Update
    void update(ProyectoEntity proyectoEntity);

    @Delete
    void delete(ProyectoEntity proyectoEntity);

    @Query("DELETE FROM proyecto_table")
    void deleteAllProject();

  /*  @Query("SELECT * FROM proyecto_table WHERE idRolGeneral=:idRolGeneral AND idRolUsuario=:idRolUsuario AND idUsuario=:idUsuario")
    List<ProyectoEntity> findAllProject(Integer idRolGeneral,Integer idRolUsuario, Integer idUsuario);*/

    @Query("SELECT * FROM proyecto_table WHERE idRolGeneral=:idRolGeneral AND idRolUsuario=:idRolUsuario AND idUsuario=:idUsuario")
    List<ProyectoEntity> findAllProject(Integer idRolGeneral,Integer idRolUsuario, Integer idUsuario);

    @Query("SELECT * FROM proyecto_table")
    List<ProyectoEntity> findAll();

}
