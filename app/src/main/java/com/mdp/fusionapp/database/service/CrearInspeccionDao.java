package com.mdp.fusionapp.database.service;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.ContratistaEntity;
import com.mdp.fusionapp.database.entity.CrearInspeccionEntity;

import java.util.List;

@Dao
public interface CrearInspeccionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(CrearInspeccionEntity createInspeccionEntity);

    @Query("DELETE FROM crear_inspeccion_table WHERE id=:id")
    void deleteInspeccion(Integer id);

    @Query("DELETE FROM crear_inspeccion_table ")
    void deleteAll();

    @Query("UPDATE  crear_inspeccion_table SET estadoRegistroDB = 3 WHERE id=:id  ")
    void updateEstadoEliminar(Integer id);

    @Query("SELECT * FROM crear_inspeccion_table WHERE id=:id")
    CrearInspeccionEntity findInspeccionById(Integer id);

    @Query("SELECT * FROM crear_inspeccion_table")
    List<CrearInspeccionEntity> findInspecciones();

    @Query("SELECT * FROM crear_inspeccion_table where estadoRegistroDB == 2 OR estadoRegistroDB == 1 OR estadoRegistroDB == 3")
    List<CrearInspeccionEntity> findInspeccionesByEstado();

}
