package com.mdp.fusionapp.database.repository.incidente;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.incidente.EquipoEntity;
import com.mdp.fusionapp.database.entity.incidente.ParteCuerpoEntity;
import com.mdp.fusionapp.database.service.incidente.EquipoDao;
import com.mdp.fusionapp.database.service.incidente.ParteCuerpoDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class ParteCuerpoRepository {

    private ParteCuerpoDao parteCuerpoDao;

    public ParteCuerpoRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        this.parteCuerpoDao = database.parteCuerpoDao();
    }

    public void insert(ParteCuerpoEntity parteCuerpoEntity) {
        new InsertAsyncTask(parteCuerpoDao).execute(parteCuerpoEntity);
    }

    public void deleteAll() {
        new DeleteAsyncTask(parteCuerpoDao).execute();
    }

    public List<ParteCuerpoEntity> findAll() {
        try {
            return new FindAllAsyncTask(parteCuerpoDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Insert
    private static class InsertAsyncTask extends AsyncTask<ParteCuerpoEntity, Void, Void> {
        private ParteCuerpoDao parteCuerpoDao;

        private InsertAsyncTask(ParteCuerpoDao parteCuerpoDao) {
            this.parteCuerpoDao = parteCuerpoDao;
        }

        @Override
        protected Void doInBackground(ParteCuerpoEntity... entities) {
            parteCuerpoDao.insert(entities[0]);
            return null;
        }
    }

    //Select
    private static class FindAllAsyncTask extends AsyncTask<Void, Void, List<ParteCuerpoEntity>> {
        private ParteCuerpoDao parteCuerpoDao;

        private FindAllAsyncTask(ParteCuerpoDao parteCuerpoDao) {
            this.parteCuerpoDao = parteCuerpoDao;
        }

        @Override
        protected List<ParteCuerpoEntity> doInBackground(Void... voids) {
            return parteCuerpoDao.findAll();
        }
    }

    //Delete
    private static class DeleteAsyncTask extends AsyncTask<Void, Void, Void> {
        private ParteCuerpoDao parteCuerpoDao;

        private DeleteAsyncTask(ParteCuerpoDao parteCuerpoDao) {
            this.parteCuerpoDao = parteCuerpoDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            parteCuerpoDao.deleteAll();
            return null;
        }
    }

}
