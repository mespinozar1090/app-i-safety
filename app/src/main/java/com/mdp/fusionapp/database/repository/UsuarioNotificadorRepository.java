package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.TipoUbicacionEntity;
import com.mdp.fusionapp.database.entity.UsuarioNotificarEntity;
import com.mdp.fusionapp.database.service.TipoUbicacionDao;
import com.mdp.fusionapp.database.service.UsuarioNotificarDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class UsuarioNotificadorRepository {
    private UsuarioNotificarDao usuarioNotificarDao;

    public UsuarioNotificadorRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        this.usuarioNotificarDao = database.usuarioNotificarDao();
    }

    public void insert(UsuarioNotificarEntity usuarioNotificarEntity) {
        new InsertAsyncTask(usuarioNotificarDao).execute(usuarioNotificarEntity);
    }

    public void deleteAll() {
        new DeleteAllAsyncTask(usuarioNotificarDao).execute();
    }

    //Select all Project
    public List<UsuarioNotificarEntity> findAll() {
        try {
            return new GetAllAsyncTask(usuarioNotificarDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Insert
    private static class InsertAsyncTask extends AsyncTask<UsuarioNotificarEntity, Void, Void> {
        private UsuarioNotificarDao usuarioNotificarDao;

        private InsertAsyncTask(UsuarioNotificarDao usuarioNotificarDao) {
            this.usuarioNotificarDao = usuarioNotificarDao;
        }

        @Override
        protected Void doInBackground(UsuarioNotificarEntity... tipoUbicacionEntities) {
            usuarioNotificarDao.insert(tipoUbicacionEntities[0]);
            return null;
        }
    }

    //Delete
    private static class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        private UsuarioNotificarDao usuarioNotificarDao;

        private DeleteAllAsyncTask(UsuarioNotificarDao usuarioNotificarDao) {
            this.usuarioNotificarDao = usuarioNotificarDao;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            usuarioNotificarDao.deleteAll();
            return null;
        }
    }

    private static class GetAllAsyncTask extends AsyncTask<Integer, String, List<UsuarioNotificarEntity>> {
        private UsuarioNotificarDao usuarioNotificarDao;

        public GetAllAsyncTask(UsuarioNotificarDao usuarioNotificarDao) {
            this.usuarioNotificarDao = usuarioNotificarDao;
        }

        @Override
        protected List<UsuarioNotificarEntity> doInBackground(Integer... integers) {
            return usuarioNotificarDao.findAll();
        }
    }

}
