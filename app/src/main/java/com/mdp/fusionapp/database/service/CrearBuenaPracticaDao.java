package com.mdp.fusionapp.database.service;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.CrearBuenaPracticaEntity;

import java.util.List;

@Dao
public interface CrearBuenaPracticaDao {

    @Insert
    void insert(CrearBuenaPracticaEntity crearBuenaPracticaEntity);

    @Query("DELETE  FROM crear_buenapractica_table WHERE idInspeccionOffline=:idInspeccionOffline")
    void deleteObjectByIdInspeccion(Long idInspeccionOffline);

    @Query("SELECT * FROM crear_buenapractica_table WHERE idInspeccionOffline=:idInspeccionOffline")
    List<CrearBuenaPracticaEntity> findCrearBuenaPracticaEntity(Long idInspeccionOffline);

}
