package com.mdp.fusionapp.database.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "lineamiento_table")
public class LineamientoEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("Id_Audit_Items")
    private Integer idAuditItems;

    @SerializedName("Descripcion")
    private String descripcion;

    @SerializedName("Estado")
    private Boolean estado;

    @SerializedName("Tipo")
    private Integer tipo;

    public LineamientoEntity(Integer idAuditItems, String descripcion) {
        this.idAuditItems = idAuditItems;
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdAuditItems() {
        return idAuditItems;
    }

    public void setIdAuditItems(Integer idAuditItems) {
        this.idAuditItems = idAuditItems;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }
}
