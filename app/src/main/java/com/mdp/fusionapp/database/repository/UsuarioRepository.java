package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.service.UsuarioDao;
import com.mdp.fusionapp.database.entity.UsuarioEntity;

import java.util.concurrent.ExecutionException;

public class UsuarioRepository {

    private UsuarioDao usuarioDao;

    public UsuarioRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        usuarioDao = database.usuarioDao();
    }

    public void insert(UsuarioEntity usuarioEntity) {
        new InsertUsuarioAsyncTask(usuarioDao).execute(usuarioEntity);
    }

    public void update(UsuarioEntity usuarioEntity) {
        new UpdateUsuarioAsyncTask(usuarioDao).execute(usuarioEntity);
    }

    public void delete(UsuarioEntity usuarioEntity) {
        new DeleteUsuariotAsyncTask(usuarioDao).execute(usuarioEntity);
    }

    public LiveData<UsuarioEntity> getUserAndPassword(String usuario, String clave) {
        return usuarioDao.getUserAndPassword(usuario, clave);
    }

    public LiveData<UsuarioEntity> getByUser(String usuario) {
        return usuarioDao.getByUser(usuario);
    }

    public UsuarioEntity getUserByUserName(String usuario) throws ExecutionException, InterruptedException {
        return new QueryUserNameAsyncTask(usuarioDao,usuario).execute().get();
    }

    //Select by user
    private static class QueryUserNameAsyncTask extends AsyncTask<UsuarioEntity, String, UsuarioEntity> {
        private UsuarioDao usuarioDao;
        private String userName;

        private QueryUserNameAsyncTask(UsuarioDao usuarioDao,String userName) {
            this.usuarioDao = usuarioDao;
            this.userName = userName;
        }

        @Override
        protected UsuarioEntity doInBackground(UsuarioEntity... usuarioEntities) {
            return usuarioDao.getByUserName(userName);
        }
    }

    //Insert
    private static class InsertUsuarioAsyncTask extends AsyncTask<UsuarioEntity, Void, Void> {
        private UsuarioDao usuarioDao;

        private InsertUsuarioAsyncTask(UsuarioDao ticketDao) {
            this.usuarioDao = ticketDao;
        }

        @Override
        protected Void doInBackground(UsuarioEntity... usuarioEntities) {
            usuarioDao.insert(usuarioEntities[0]);
            return null;
        }
    }

    //Update
    private static class UpdateUsuarioAsyncTask extends AsyncTask<UsuarioEntity, Void, Void> {
        private UsuarioDao usuarioDao;

        private UpdateUsuarioAsyncTask(UsuarioDao ticketDao) {
            this.usuarioDao = ticketDao;
        }

        @Override
        protected Void doInBackground(UsuarioEntity... tickets) {
            usuarioDao.update(tickets[0]);
            return null;
        }
    }

    //Delete
    private static class DeleteUsuariotAsyncTask extends AsyncTask<UsuarioEntity, Void, Void> {
        private UsuarioDao usuarioDao;

        private DeleteUsuariotAsyncTask(UsuarioDao usuarioDao) {
            this.usuarioDao = usuarioDao;
        }

        @Override
        protected Void doInBackground(UsuarioEntity... tickets) {
            usuarioDao.delete(tickets[0]);
            return null;
        }
    }

}
