package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.ContratistaEntity;
import com.mdp.fusionapp.database.entity.TipoUbicacionEntity;
import com.mdp.fusionapp.database.service.ContratistaDao;
import com.mdp.fusionapp.database.service.TipoUbicacionDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class TipoUbicacionRepository {
    private TipoUbicacionDao tipoUbicacionDao;

    public TipoUbicacionRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        this.tipoUbicacionDao = database.tipoUbicacionDao();
    }

    public void insert(TipoUbicacionEntity tipoUbicacionEntity) {
        new InsertAsyncTask(tipoUbicacionDao).execute(tipoUbicacionEntity);
    }

    public void deleteAll() {
        new DeleteAllAsyncTask(tipoUbicacionDao).execute();
    }

    //Select all Project
    public List<TipoUbicacionEntity> findTipoUbicacionByProyecto(Integer idProyecto) {
        try {
            return new GetTipoUbicacionAsyncTask(tipoUbicacionDao, idProyecto).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Insert
    private static class InsertAsyncTask extends AsyncTask<TipoUbicacionEntity, Void, Void> {
        private TipoUbicacionDao tipoUbicacionDao;

        private InsertAsyncTask(TipoUbicacionDao tipoUbicacionDao) {
            this.tipoUbicacionDao = tipoUbicacionDao;
        }

        @Override
        protected Void doInBackground(TipoUbicacionEntity... tipoUbicacionEntities) {
            tipoUbicacionDao.insert(tipoUbicacionEntities[0]);
            return null;
        }
    }

    //Delete
    private static class DeleteAllAsyncTask extends AsyncTask<TipoUbicacionEntity, Void, Void> {
        private TipoUbicacionDao tipoUbicacionDao;

        private DeleteAllAsyncTask(TipoUbicacionDao tipoUbicacionDao) {
            this.tipoUbicacionDao = tipoUbicacionDao;
        }

        @Override
        protected Void doInBackground(TipoUbicacionEntity... tipoUbicacionEntities) {
            tipoUbicacionDao.deleteAll();
            return null;
        }
    }

    private static class GetTipoUbicacionAsyncTask extends AsyncTask<Integer, String, List<TipoUbicacionEntity>> {
        private TipoUbicacionDao tipoUbicacionDao;
        private Integer idProceso;

        public GetTipoUbicacionAsyncTask(TipoUbicacionDao tipoUbicacionDao, Integer idProceso) {
            this.tipoUbicacionDao = tipoUbicacionDao;
            this.idProceso = idProceso;
        }

        @Override
        protected List<TipoUbicacionEntity> doInBackground(Integer... integers) {
            return tipoUbicacionDao.findTipoUbicacion(idProceso);
        }
    }
}
