package com.mdp.fusionapp.database.repository.incidente;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.auditoria.CrearAuditoriaEntity;
import com.mdp.fusionapp.database.entity.incidente.CrearIncidenteEntity;
import com.mdp.fusionapp.database.entity.interventoria.CrearEvidenciaInterEntity;
import com.mdp.fusionapp.database.entity.interventoria.CrearInterventoriaEntiy;
import com.mdp.fusionapp.database.repository.CrearAuditoriaRepository;
import com.mdp.fusionapp.database.repository.interventoria.CrearInterventoriaRepository;
import com.mdp.fusionapp.database.service.CrearAuditoriaDao;
import com.mdp.fusionapp.database.service.incidente.CrearIncidenteDao;
import com.mdp.fusionapp.database.service.interventoria.CrearEvidenciaInterDao;
import com.mdp.fusionapp.database.service.interventoria.CrearInterventoriaDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class CrearIncidentesRepository {

    private CrearIncidenteDao crearIncidenteDao;

    public CrearIncidentesRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        this.crearIncidenteDao = database.crearIncidenteDao();
    }

    public Long insert(CrearIncidenteEntity crearIncidenteEntity) {
        try {
            return new InsertAsyncTask(crearIncidenteDao).execute(crearIncidenteEntity).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteAll() {
        new DeleteAllAsyncTask(crearIncidenteDao).execute();
    }

    public void deleteById(Integer id) {
        new DeleteByIdAsyncTask(crearIncidenteDao).execute(id);
    }



    public CrearIncidenteEntity findById(Integer id) {
        try {
            return new GetByIdAsyncTask(crearIncidenteDao).execute(id).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<CrearIncidenteEntity> findAll() {
        try {
            return new GetAlldAsyncTask(crearIncidenteDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<CrearIncidenteEntity> findByIdEstado() {
        try {
            return new GetAllByEstadoAsyncTask(crearIncidenteDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Select by estado
    private static class GetAllByEstadoAsyncTask extends AsyncTask<Void, Void, List<CrearIncidenteEntity>> {
        private CrearIncidenteDao crearIncidenteDao;

        private GetAllByEstadoAsyncTask(CrearIncidenteDao crearAuditoriaDao) {
            this.crearIncidenteDao = crearAuditoriaDao;
        }

        @Override
        protected List<CrearIncidenteEntity> doInBackground(Void... voids) {
            return crearIncidenteDao.findAllByEstado();
        }
    }

    //Select
    private static class GetByIdAsyncTask extends AsyncTask<Integer, Void, CrearIncidenteEntity> {
        private CrearIncidenteDao crearIncidenteDao;

        private GetByIdAsyncTask(CrearIncidenteDao crearIncidenteDao) {
            this.crearIncidenteDao = crearIncidenteDao;
        }

        @Override
        protected CrearIncidenteEntity doInBackground(Integer... integers) {
            return crearIncidenteDao.findAllById(integers[0]);
        }
    }

    //Select by Estado
    private static class GetAlldAsyncTask extends AsyncTask<Void, Void, List<CrearIncidenteEntity>> {
        private CrearIncidenteDao crearInterventoriaDao;

        private GetAlldAsyncTask(CrearIncidenteDao crearInterventoriaDao) {
            this.crearInterventoriaDao = crearInterventoriaDao;
        }

        @Override
        protected List<CrearIncidenteEntity> doInBackground(Void... voids) {
            return crearInterventoriaDao.findAll();
        }
    }

    //Delete
    private static class DeleteByIdAsyncTask extends AsyncTask<Integer, Void, Void> {
        private CrearIncidenteDao crearIncidenteDao;

        private DeleteByIdAsyncTask(CrearIncidenteDao crearIncidenteDao) {
            this.crearIncidenteDao = crearIncidenteDao;
        }

        @Override
        protected Void doInBackground(Integer... entities) {
            crearIncidenteDao.deleteById(entities[0]);
            return null;
        }
    }

    //Insert
    private static class InsertAsyncTask extends AsyncTask<CrearIncidenteEntity, Void, Long> {
        private CrearIncidenteDao crearIncidenteDao;

        private InsertAsyncTask(CrearIncidenteDao crearIncidenteDao) {
            this.crearIncidenteDao = crearIncidenteDao;
        }

        @Override
        protected Long doInBackground(CrearIncidenteEntity... entities) {
            return crearIncidenteDao.insert(entities[0]);
        }
    }

    //Delete All
    private static class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        private CrearIncidenteDao crearIncidenteDao;

        private DeleteAllAsyncTask(CrearIncidenteDao crearIncidenteDao) {
            this.crearIncidenteDao = crearIncidenteDao;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            crearIncidenteDao.deleteAll();
            return null;
        }
    }
}
