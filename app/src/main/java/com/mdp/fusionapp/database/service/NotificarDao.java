package com.mdp.fusionapp.database.service;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.sbc.CrearCategoriaSbcEntity;
import com.mdp.fusionapp.database.entity.sbc.CrearSbcEntity;
import com.mdp.fusionapp.database.entity.sbc.NotificarEntity;

import java.util.List;

@Dao
public interface NotificarDao {

    @Insert
    void insert(NotificarEntity notificarEntity);

    @Delete
    void delete(NotificarEntity notificarEntity);

    @Query("DELETE FROM notificar_table where idModulo=:idModulo  and idOffline=:idOffline")
    void deleteByIds(Integer idModulo, Long idOffline);

    @Query("SELECT * FROM notificar_table WHERE idModulo=:idModulo  and idOffline=:idOffline")
    List<NotificarEntity> findAllById(Integer idModulo, Long idOffline);
}
