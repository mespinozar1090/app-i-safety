package com.mdp.fusionapp.database.repository.incidente;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.service.incidente.ComboMaestroDao;
import com.mdp.fusionapp.network.response.ComboMaestroEntity;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class ComboMaestroRepository {

    private ComboMaestroDao comboMaestroDao;

    public ComboMaestroRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        this.comboMaestroDao = database.comboMaestroDao();
    }

    public void insert(ComboMaestroEntity comboMaestroEntity) {
        new InsertAsyncTask(comboMaestroDao).execute(comboMaestroEntity);
    }

    public void deleteAll() {
        new DeleteAsyncTask(comboMaestroDao).execute();
    }

    public List<ComboMaestroEntity> findAll() {
        try {
            return new FindAllAsyncTask(comboMaestroDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Insert
    private static class InsertAsyncTask extends AsyncTask<ComboMaestroEntity, Void, Void> {
        private ComboMaestroDao comboMaestroDao;

        private InsertAsyncTask(ComboMaestroDao comboMaestroDao) {
            this.comboMaestroDao = comboMaestroDao;
        }

        @Override
        protected Void doInBackground(ComboMaestroEntity... entities) {
            comboMaestroDao.insert(entities[0]);
            return null;
        }
    }

    //Delete
    private static class DeleteAsyncTask extends AsyncTask<Void, Void, Void> {
        private ComboMaestroDao comboMaestroDao;

        private DeleteAsyncTask(ComboMaestroDao comboMaestroDao) {
            this.comboMaestroDao = comboMaestroDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            comboMaestroDao.deleteAll();
            return null;
        }
    }

    //Select
    private static class FindAllAsyncTask extends AsyncTask<Void, Void, List<ComboMaestroEntity>> {
        private ComboMaestroDao comboMaestroDao;

        private FindAllAsyncTask(ComboMaestroDao comboMaestroDao) {
            this.comboMaestroDao = comboMaestroDao;
        }

        @Override
        protected List<ComboMaestroEntity> doInBackground(Void... voids) {
            return comboMaestroDao.findAll();
        }
    }
}
