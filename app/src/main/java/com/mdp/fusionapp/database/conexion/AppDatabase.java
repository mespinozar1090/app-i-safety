package com.mdp.fusionapp.database.conexion;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.mdp.fusionapp.database.entity.CategoriaBuenaPracticaEntity;
import com.mdp.fusionapp.database.entity.CategoriaSbcEntity;
import com.mdp.fusionapp.database.entity.ContratistaEntity;
import com.mdp.fusionapp.database.entity.CrearBuenaPracticaEntity;
import com.mdp.fusionapp.database.entity.CrearHallazgoEntity;
import com.mdp.fusionapp.database.entity.CrearInspeccionEntity;
import com.mdp.fusionapp.database.entity.EmpresaMaestraEntity;
import com.mdp.fusionapp.database.entity.EmpresaSbcEntity;
import com.mdp.fusionapp.database.entity.EspecialidadEntity;
import com.mdp.fusionapp.database.entity.LugarEntity;
import com.mdp.fusionapp.database.entity.ProyectoEntity;
import com.mdp.fusionapp.database.entity.SedeProyectoEntity;
import com.mdp.fusionapp.database.entity.TipoUbicacionEntity;
import com.mdp.fusionapp.database.entity.UsuarioNotificarEntity;
import com.mdp.fusionapp.database.entity.UsuarioResponsableEntity;
import com.mdp.fusionapp.database.entity.auditoria.CrearAuditoriaEntity;
import com.mdp.fusionapp.database.entity.auditoria.CrearDetalleAudotiraEntity;
import com.mdp.fusionapp.database.entity.auditoria.CrearLineamientoEntity;
import com.mdp.fusionapp.database.entity.incidente.CrearIncidenteEntity;
import com.mdp.fusionapp.database.entity.incidente.ParteCuerpoEntity;
import com.mdp.fusionapp.database.entity.incidente.TipoEventoEntity;
import com.mdp.fusionapp.database.entity.interventoria.ControlSubItemEntity;
import com.mdp.fusionapp.database.entity.interventoria.CrearControlEntity;
import com.mdp.fusionapp.database.entity.interventoria.CrearEvidenciaInterEntity;
import com.mdp.fusionapp.database.entity.interventoria.CrearInterventoriaEntiy;
import com.mdp.fusionapp.database.entity.interventoria.CrearVerificacionEntity;
import com.mdp.fusionapp.database.entity.interventoria.VerificacionSubItemEntity;
import com.mdp.fusionapp.database.entity.sbc.CrearCategoriaSbcEntity;
import com.mdp.fusionapp.database.entity.sbc.CrearSbcEntity;
import com.mdp.fusionapp.database.entity.sbc.NotificarEntity;
import com.mdp.fusionapp.database.entity.incidente.EquipoEntity;
import com.mdp.fusionapp.database.service.AreaTrabajoDao;
import com.mdp.fusionapp.database.service.CategoriaBuenaPracticaDao;
import com.mdp.fusionapp.database.service.CategoriaSbcDao;
import com.mdp.fusionapp.database.service.CategoriaSbcItemDao;
import com.mdp.fusionapp.database.service.ContratistaDao;
import com.mdp.fusionapp.database.service.CrearAuditoriaDao;
import com.mdp.fusionapp.database.service.CrearBuenaPracticaDao;
import com.mdp.fusionapp.database.service.CrearDetalleAuditoriaDao;
import com.mdp.fusionapp.database.service.CrearHallazgoDao;
import com.mdp.fusionapp.database.service.CrearInspeccionDao;
import com.mdp.fusionapp.database.service.EmpresaMaestraDao;
import com.mdp.fusionapp.database.service.EmpresaSbcDao;
import com.mdp.fusionapp.database.service.EspecialidadDao;
import com.mdp.fusionapp.database.service.LineamientoDao;
import com.mdp.fusionapp.database.service.LineamientoSubDao;
import com.mdp.fusionapp.database.service.LugarDao;
import com.mdp.fusionapp.database.service.NotificarDao;
import com.mdp.fusionapp.database.service.ProyectoDao;
import com.mdp.fusionapp.database.service.SedeProyectoDao;
import com.mdp.fusionapp.database.service.TipoUbicacionDao;
import com.mdp.fusionapp.database.service.UsuarioDao;
import com.mdp.fusionapp.database.entity.UsuarioEntity;
import com.mdp.fusionapp.database.service.UsuarioNotificarDao;
import com.mdp.fusionapp.database.service.UsuarioResponsableDao;
import com.mdp.fusionapp.database.service.incidente.ActividadDao;
import com.mdp.fusionapp.database.service.incidente.ComboMaestroDao;
import com.mdp.fusionapp.database.service.incidente.CrearIncidenteDao;
import com.mdp.fusionapp.database.service.incidente.EquipoDao;
import com.mdp.fusionapp.database.service.incidente.IncidenteEmpresaDao;
import com.mdp.fusionapp.database.service.incidente.ParteCuerpoDao;
import com.mdp.fusionapp.database.service.incidente.TipoEventoDao;
import com.mdp.fusionapp.database.service.interventoria.ControlSubItemDao;
import com.mdp.fusionapp.database.service.interventoria.CrearControlDao;
import com.mdp.fusionapp.database.service.interventoria.CrearEvidenciaInterDao;
import com.mdp.fusionapp.database.service.interventoria.CrearInterventoriaDao;
import com.mdp.fusionapp.database.service.interventoria.CrearVerificacionDao;
import com.mdp.fusionapp.database.service.interventoria.VerificacionSubItemDao;
import com.mdp.fusionapp.database.service.sbc.CrearCategoriaSbcDao;
import com.mdp.fusionapp.database.service.sbc.CrearSbcDao;
import com.mdp.fusionapp.database.entity.AreaTrabajoEntity;
import com.mdp.fusionapp.database.entity.CategoriaSbcItemEntity;
import com.mdp.fusionapp.database.entity.LineamientoEntity;
import com.mdp.fusionapp.database.entity.LineamientoSubEntity;
import com.mdp.fusionapp.network.response.ActividadEntity;
import com.mdp.fusionapp.network.response.ComboMaestroEntity;
import com.mdp.fusionapp.network.response.InspeccionEmpresaEntity;

@Database(entities = {UsuarioEntity.class, ProyectoEntity.class,
        ContratistaEntity.class, TipoUbicacionEntity.class,
        LugarEntity.class, EmpresaMaestraEntity.class,
        UsuarioResponsableEntity.class, CrearInspeccionEntity.class,
        CrearHallazgoEntity.class, CategoriaBuenaPracticaEntity.class,
        CrearBuenaPracticaEntity.class, SedeProyectoEntity.class,
        EmpresaSbcEntity.class, AreaTrabajoEntity.class,
        EspecialidadEntity.class, CategoriaSbcEntity.class,
        CategoriaSbcItemEntity.class, CrearSbcEntity.class,
        CrearCategoriaSbcEntity.class, NotificarEntity.class,
        UsuarioNotificarEntity.class,
        LineamientoSubEntity.class,
        LineamientoEntity.class,
        CrearAuditoriaEntity.class,
        CrearLineamientoEntity.class,
        CrearDetalleAudotiraEntity.class,
        VerificacionSubItemEntity.class,
        ControlSubItemEntity.class,
        CrearControlEntity.class,
        CrearEvidenciaInterEntity.class,
        CrearInterventoriaEntiy.class,
        CrearVerificacionEntity.class,
        ComboMaestroEntity.class,
        InspeccionEmpresaEntity.class, TipoEventoEntity.class, EquipoEntity.class,
        ActividadEntity.class, ParteCuerpoEntity.class, CrearIncidenteEntity.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public static final String DATABASE_NAME = "isafety_database";
    private static AppDatabase instance;

    public abstract UsuarioDao usuarioDao();

    public abstract ProyectoDao proyectoDao();

    public abstract ContratistaDao contratistaDao();

    public abstract LugarDao lugarDao();

    public abstract TipoUbicacionDao tipoUbicacionDao();

    public abstract EmpresaMaestraDao empresaMaestraDao();

    public abstract UsuarioResponsableDao usuarioResponsableDao();

    public abstract CrearInspeccionDao crearInspeccionDao();

    public abstract CrearHallazgoDao crearHallazgoDao();

    public abstract CategoriaBuenaPracticaDao categoriaBuenaPracticaDao();

    public abstract CrearBuenaPracticaDao crearBuenaPracticaDao();

    public abstract SedeProyectoDao sedeProyectoDao();

    public abstract EmpresaSbcDao empresaSbcDao();

    public abstract EspecialidadDao especialidadDao();

    public abstract AreaTrabajoDao areaTrabajoDao();

    public abstract CategoriaSbcDao categoriaSbcDao();

    public abstract CategoriaSbcItemDao categoriaSbcItemDao();

    public abstract CrearSbcDao crearSbcDao();

    public abstract CrearCategoriaSbcDao crearCategoriaSbcDao();

    public abstract NotificarDao notificarDao();

    public abstract UsuarioNotificarDao usuarioNotificarDao();

    public abstract LineamientoDao lineamientoDao();

    public abstract LineamientoSubDao lineamientoSubDao();

    public abstract CrearAuditoriaDao crearAuditoriaDao();

    public abstract CrearDetalleAuditoriaDao crearDetalleAuditoriaDao();

    public abstract VerificacionSubItemDao verificacionSubItemDao();

    public abstract ControlSubItemDao controlSubItemDao();

    public abstract CrearVerificacionDao crearVerificacionDao();

    public abstract CrearEvidenciaInterDao crearEvidenciaInterDao();

    public abstract CrearControlDao crearControlDao();

    public abstract CrearInterventoriaDao crearInterventoriaDao();

    public abstract ComboMaestroDao comboMaestroDao();

    public abstract IncidenteEmpresaDao incidenteEmpresaDao();

    public abstract ActividadDao actividadDao();

    public abstract TipoEventoDao tipoEventoDao();

    public abstract EquipoDao equipoDao();

    public abstract ParteCuerpoDao parteCuerpoDao();

    public abstract CrearIncidenteDao crearIncidenteDao();

    public static synchronized AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    AppDatabase.class, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            Log.e("OnCreateDB", "entro a crear grupos");
            //  new PopulateDbAsyncTask(instance).execute();
        }
    };


  /*  private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private GrupoDao grupoDao;
        private TicketDao ticketDao;

        private PopulateDbAsyncTask(BelmondDatabase db) {
            grupoDao = db.grupoDao();
            ticketDao = db.ticketDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ticketDao.insert(new Ticket(1,"24652226/","MARIN ESPINOZA RAMIREZ","CUSCO","EXPEDITION","C","121",
                    "12/05/2401","12:45","M",null,
                    null,"CASA123"));
            return null;
        }
    }*/

}
