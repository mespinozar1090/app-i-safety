package com.mdp.fusionapp.database.entity.interventoria;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "crear_evidencia_inter_table")
public class CrearEvidenciaInterEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private Integer idInterventoriaImagen;
    private Integer idInterventoria;
    private String imgBase64;
    private String descripcion;
    private String fechaCreacion;
    private Long idOffline;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdInterventoriaImagen() {
        return idInterventoriaImagen;
    }

    public void setIdInterventoriaImagen(Integer idInterventoriaImagen) {
        this.idInterventoriaImagen = idInterventoriaImagen;
    }

    public Integer getIdInterventoria() {
        return idInterventoria;
    }

    public void setIdInterventoria(Integer idInterventoria) {
        this.idInterventoria = idInterventoria;
    }

    public String getImgBase64() {
        return imgBase64;
    }

    public void setImgBase64(String imgBase64) {
        this.imgBase64 = imgBase64;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Long getIdOffline() {
        return idOffline;
    }

    public void setIdOffline(Long idOffline) {
        this.idOffline = idOffline;
    }
}
