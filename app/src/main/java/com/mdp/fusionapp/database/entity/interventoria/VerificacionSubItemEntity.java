package com.mdp.fusionapp.database.entity.interventoria;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "verificacion_subitem_table")
public class VerificacionSubItemEntity implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("Id_Verificacion_Subitem")
    private Integer idVerificacionSubitem;

    @SerializedName("Descripcion")
    private String descripcion;

    @SerializedName("Id_Verificacion_Items")
    private Integer idVerificacionItems;

    @SerializedName("Id_Verificacion")
    private Integer idVerificacion;

    private Integer idInterventoriaDetalle;
    private Integer cumple = 3;

    private Integer idOffline;

    @Ignore
    public VerificacionSubItemEntity() {
    }

    public VerificacionSubItemEntity(Integer idVerificacionSubitem, String descripcion, Integer idVerificacionItems, Integer idVerificacion) {
        this.idVerificacionSubitem = idVerificacionSubitem;
        this.descripcion = descripcion;
        this.idVerificacionItems = idVerificacionItems;
        this.idVerificacion = idVerificacion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdOffline() {
        return idOffline;
    }

    public void setIdOffline(Integer idOffline) {
        this.idOffline = idOffline;
    }

    public Integer getIdInterventoriaDetalle() {
        return idInterventoriaDetalle;
    }

    public void setIdInterventoriaDetalle(Integer idInterventoriaDetalle) {
        this.idInterventoriaDetalle = idInterventoriaDetalle;
    }

    public Integer getIdVerificacion() {
        return idVerificacion;
    }

    public void setIdVerificacion(Integer idVerificacion) {
        this.idVerificacion = idVerificacion;
    }

    public Integer getIdVerificacionSubitem() {
        return idVerificacionSubitem;
    }

    public void setIdVerificacionSubitem(Integer idVerificacionSubitem) {
        this.idVerificacionSubitem = idVerificacionSubitem;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdVerificacionItems() {
        return idVerificacionItems;
    }

    public void setIdVerificacionItems(Integer idVerificacionItems) {
        this.idVerificacionItems = idVerificacionItems;
    }

    public Integer getCumple() {
        return cumple;
    }

    public void setCumple(Integer cumple) {
        this.cumple = cumple;
    }
}
