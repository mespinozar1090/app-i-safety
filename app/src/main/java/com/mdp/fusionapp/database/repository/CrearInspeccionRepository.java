package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.CrearInspeccionEntity;
import com.mdp.fusionapp.database.service.CrearInspeccionDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class CrearInspeccionRepository {

    private CrearInspeccionDao crearInspeccionDao;

    public CrearInspeccionRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        crearInspeccionDao = database.crearInspeccionDao();
    }

    public Long insert(CrearInspeccionEntity crearInspeccionEntity) {
        try {
            return new InsertInspeccionAsyncTask(crearInspeccionDao).execute(crearInspeccionEntity).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deteleAll() {
        new DeleteAllAsyncTask(crearInspeccionDao).execute();
    }

    public void deleteInspeccion(Integer id) {
        new DeleteInspeccionAsyncTask(crearInspeccionDao).execute(id);
    }


    public CrearInspeccionEntity findInspeccionById(Integer id) {
        try {
            return new GetInspeccionAsyncTask(crearInspeccionDao).execute(id).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void updateEstadoEliminar(Integer id) {
        new UpdateEstadoEliminarAllAsyncTask(crearInspeccionDao).execute(id);
    }

    public List<CrearInspeccionEntity> findInspecciones() {
        try {
            return new GetInspeccionesAsyncTask(crearInspeccionDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<CrearInspeccionEntity> findInspeccionesByEstado() {
        try {
            return new GetInspeccionesByEstadoAsyncTask(crearInspeccionDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


    //Insert
    private static class InsertInspeccionAsyncTask extends AsyncTask<CrearInspeccionEntity, Void, Long> {
        private CrearInspeccionDao crearInspeccionDao;

        private InsertInspeccionAsyncTask(CrearInspeccionDao crearInspeccionDao) {
            this.crearInspeccionDao = crearInspeccionDao;
        }

        @Override
        protected Long doInBackground(CrearInspeccionEntity... entities) {
            return crearInspeccionDao.insert(entities[0]);
        }
    }

    //Delete
    private static class DeleteInspeccionAsyncTask extends AsyncTask<Integer, Void, Void> {
        private CrearInspeccionDao crearInspeccionDao;

        private DeleteInspeccionAsyncTask(CrearInspeccionDao crearInspeccionDao) {
            this.crearInspeccionDao = crearInspeccionDao;
        }

        @Override
        protected Void doInBackground(Integer... entities) {
            crearInspeccionDao.deleteInspeccion(entities[0]);
            return null;
        }
    }

    //updateEstadoEliminar

    //Delete All
    private static class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        private CrearInspeccionDao crearInspeccionDao;

        private DeleteAllAsyncTask(CrearInspeccionDao crearInspeccionDao) {
            this.crearInspeccionDao = crearInspeccionDao;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            crearInspeccionDao.deleteAll();
            return null;
        }
    }

    private static class UpdateEstadoEliminarAllAsyncTask extends AsyncTask<Integer, Void, Void> {
        private CrearInspeccionDao crearInspeccionDao;

        private UpdateEstadoEliminarAllAsyncTask(CrearInspeccionDao crearInspeccionDao) {
            this.crearInspeccionDao = crearInspeccionDao;
        }


        @Override
        protected Void doInBackground(Integer... integers) {
            crearInspeccionDao.updateEstadoEliminar(integers[0]);
            return null;
        }
    }

    //Select
    private static class GetInspeccionAsyncTask extends AsyncTask<Integer, Void, CrearInspeccionEntity> {
        private CrearInspeccionDao crearInspeccionDao;

        private GetInspeccionAsyncTask(CrearInspeccionDao crearInspeccionDao) {
            this.crearInspeccionDao = crearInspeccionDao;
        }

        @Override
        protected CrearInspeccionEntity doInBackground(Integer... integers) {
            return crearInspeccionDao.findInspeccionById(integers[0]);
        }
    }

    //Select
    private static class GetInspeccionesAsyncTask extends AsyncTask<Void, Void, List<CrearInspeccionEntity>> {
        private CrearInspeccionDao crearInspeccionDao;

        private GetInspeccionesAsyncTask(CrearInspeccionDao crearInspeccionDao) {
            this.crearInspeccionDao = crearInspeccionDao;
        }

        @Override
        protected List<CrearInspeccionEntity> doInBackground(Void... voids) {
            return crearInspeccionDao.findInspecciones();
        }
    }


    //Select by Estado
    private static class GetInspeccionesByEstadoAsyncTask extends AsyncTask<Void, Void, List<CrearInspeccionEntity>> {
        private CrearInspeccionDao crearInspeccionDao;

        private GetInspeccionesByEstadoAsyncTask(CrearInspeccionDao crearInspeccionDao) {
            this.crearInspeccionDao = crearInspeccionDao;
        }

        @Override
        protected List<CrearInspeccionEntity> doInBackground(Void... voids) {
            return crearInspeccionDao.findInspeccionesByEstado();
        }
    }
}
