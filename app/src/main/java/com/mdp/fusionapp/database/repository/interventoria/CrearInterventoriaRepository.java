package com.mdp.fusionapp.database.repository.interventoria;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.CrearInspeccionEntity;
import com.mdp.fusionapp.database.entity.interventoria.CrearEvidenciaInterEntity;
import com.mdp.fusionapp.database.entity.interventoria.CrearInterventoriaEntiy;
import com.mdp.fusionapp.database.entity.interventoria.VerificacionSubItemEntity;
import com.mdp.fusionapp.database.entity.sbc.CrearSbcEntity;
import com.mdp.fusionapp.database.repository.CrearInspeccionRepository;
import com.mdp.fusionapp.database.repository.CrearSbcRepository;
import com.mdp.fusionapp.database.repository.VerificacionSubItemRepository;
import com.mdp.fusionapp.database.service.CrearInspeccionDao;
import com.mdp.fusionapp.database.service.interventoria.CrearInterventoriaDao;
import com.mdp.fusionapp.database.service.interventoria.VerificacionSubItemDao;
import com.mdp.fusionapp.database.service.sbc.CrearSbcDao;

import java.util.List;
import java.util.concurrent.ExecutionException;


public class CrearInterventoriaRepository {

    private CrearInterventoriaDao crearInterventoriaDao;

    public CrearInterventoriaRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        this.crearInterventoriaDao = database.crearInterventoriaDao();
    }

    public Long insert(CrearInterventoriaEntiy crearInterventoriaEntiy) {
        try {
            return new InsertAsyncTask(crearInterventoriaDao).execute(crearInterventoriaEntiy).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteAll() {
        new DeleteAllAsyncTask(crearInterventoriaDao).execute();
    }

    public void updateEstadoEliminar(Integer id) {
        new UpdateEstadoEliminarAllAsyncTask(crearInterventoriaDao).execute(id);
    }

    public void deleteById(Integer id) {
        new DeleteByIdAsyncTask(crearInterventoriaDao).execute(id);
    }

    public List<CrearInterventoriaEntiy> findAll() {
        try {
            return new GetAlldAsyncTask(crearInterventoriaDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<CrearInterventoriaEntiy> findAllByEstado() {
        try {
            return new GetAllByEstadoAsyncTask(crearInterventoriaDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Select by estado
    private static class GetAllByEstadoAsyncTask extends AsyncTask<Void, Void, List<CrearInterventoriaEntiy>> {
        private CrearInterventoriaDao crearInterventoriaDao;

        private GetAllByEstadoAsyncTask(CrearInterventoriaDao crearInterventoriaDao) {
            this.crearInterventoriaDao = crearInterventoriaDao;
        }


        @Override
        protected List<CrearInterventoriaEntiy> doInBackground(Void... voids) {
            return crearInterventoriaDao.findAllByEstado();
        }
    }

    public CrearInterventoriaEntiy findById(Integer id) {
        try {
            return new GetByIdAsyncTask(crearInterventoriaDao).execute(id).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<CrearInterventoriaEntiy> findByEstado() {
        try {
            return new GetByEstadoAsyncTask(crearInterventoriaDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Select by Estado
    private static class GetByEstadoAsyncTask extends AsyncTask<Void, Void, List<CrearInterventoriaEntiy>> {
        private CrearInterventoriaDao crearInterventoriaDao;

        private GetByEstadoAsyncTask(CrearInterventoriaDao crearInterventoriaDao) {
            this.crearInterventoriaDao = crearInterventoriaDao;
        }

        @Override
        protected List<CrearInterventoriaEntiy> doInBackground(Void... voids) {
            return crearInterventoriaDao.findAllByEstado();
        }
    }

    //Select by Estado
    private static class GetAlldAsyncTask extends AsyncTask<Void, Void, List<CrearInterventoriaEntiy>> {
        private CrearInterventoriaDao crearInterventoriaDao;

        private GetAlldAsyncTask(CrearInterventoriaDao crearInterventoriaDao) {
            this.crearInterventoriaDao = crearInterventoriaDao;
        }

        @Override
        protected List<CrearInterventoriaEntiy> doInBackground(Void... voids) {
            return crearInterventoriaDao.findAll();
        }
    }

    //Select
    private static class GetByIdAsyncTask extends AsyncTask<Integer, Void, CrearInterventoriaEntiy> {
        private CrearInterventoriaDao crearInterventoriaDao;

        private GetByIdAsyncTask(CrearInterventoriaDao crearInterventoriaDao) {
            this.crearInterventoriaDao = crearInterventoriaDao;
        }

        @Override
        protected CrearInterventoriaEntiy doInBackground(Integer... integers) {
            return crearInterventoriaDao.findAllById(integers[0]);
        }
    }

    //Insert
    private static class InsertAsyncTask extends AsyncTask<CrearInterventoriaEntiy, Void, Long> {
        private CrearInterventoriaDao crearInterventoriaDao;

        private InsertAsyncTask(CrearInterventoriaDao crearInterventoriaDao) {
            this.crearInterventoriaDao = crearInterventoriaDao;
        }

        @Override
        protected Long doInBackground(CrearInterventoriaEntiy... entities) {
            return crearInterventoriaDao.insert(entities[0]);
        }
    }

    //Delete
    private static class DeleteByIdAsyncTask extends AsyncTask<Integer, Void, Void> {
        private CrearInterventoriaDao crearInterventoriaDao;

        private DeleteByIdAsyncTask(CrearInterventoriaDao crearInterventoriaDao) {
            this.crearInterventoriaDao = crearInterventoriaDao;
        }

        @Override
        protected Void doInBackground(Integer... entities) {
            Log.e("DeleteByIdAsyncTask","Debe elminar la interventoria "+entities[0]);
            crearInterventoriaDao.deleteById(entities[0]);
            return null;
        }
    }

    //Delete All
    private static class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        private CrearInterventoriaDao crearInterventoriaDao;

        private DeleteAllAsyncTask(CrearInterventoriaDao crearInterventoriaDao) {
            this.crearInterventoriaDao = crearInterventoriaDao;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            crearInterventoriaDao.deleteAll();
            return null;
        }
    }

    private static class UpdateEstadoEliminarAllAsyncTask extends AsyncTask<Integer, Void, Void> {
        private CrearInterventoriaDao crearInterventoriaDao;

        private UpdateEstadoEliminarAllAsyncTask(CrearInterventoriaDao crearInterventoriaDao) {
            this.crearInterventoriaDao = crearInterventoriaDao;
        }


        @Override
        protected Void doInBackground(Integer... integers) {
            crearInterventoriaDao.updateEstadoEliminar(integers[0]);
            return null;
        }
    }

}
