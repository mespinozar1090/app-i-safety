package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.ContratistaEntity;
import com.mdp.fusionapp.database.entity.ProyectoEntity;
import com.mdp.fusionapp.database.service.ContratistaDao;
import com.mdp.fusionapp.database.service.ProyectoDao;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ContratistaRepository {

    private ContratistaDao contratistaDao;

    public ContratistaRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        contratistaDao = database.contratistaDao();
    }

    public void insert(ContratistaEntity contratistaEntity) {
        new InsertContratistaAsyncTask(contratistaDao).execute(contratistaEntity);
    }

    public void deleteAll() {
        new DeleteAllContratistaAsyncTask(contratistaDao).execute();
    }

    public void deleteObject(Integer id) {
        new DeleteContratistaAsyncTask(contratistaDao).execute(id);
    }

    //Select all Project
    public List<ContratistaEntity> findContratistaByProyecto(Integer idProyecto) {
        try {
            return new GetContratistaAsyncTask(contratistaDao, idProyecto).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static class GetContratistaAsyncTask extends AsyncTask<Integer, String, List<ContratistaEntity>> {
        private ContratistaDao contratistaDao;
        private Integer idProceso;

        public GetContratistaAsyncTask(ContratistaDao contratistaDao, Integer idProceso) {
            this.contratistaDao = contratistaDao;
            this.idProceso = idProceso;
        }

        @Override
        protected List<ContratistaEntity> doInBackground(Integer... integers) {
            return contratistaDao.findContratistaByProyecto(idProceso);
        }
    }


    //Insert
    private static class InsertContratistaAsyncTask extends AsyncTask<ContratistaEntity, Void, Void> {
        private ContratistaDao contratistaDao;

        private InsertContratistaAsyncTask(ContratistaDao contratistaDao) {
            this.contratistaDao = contratistaDao;
        }

        @Override
        protected Void doInBackground(ContratistaEntity... contratistaEntities) {
            contratistaDao.insert(contratistaEntities[0]);
            return null;
        }
    }

    //Delete
    private static class DeleteAllContratistaAsyncTask extends AsyncTask<ContratistaEntity, Void, Void> {
        private ContratistaDao contratistaDao;

        private DeleteAllContratistaAsyncTask(ContratistaDao contratistaDao) {
            this.contratistaDao = contratistaDao;
        }

        @Override
        protected Void doInBackground(ContratistaEntity... contratistaEntities) {
            contratistaDao.deleteAllContratistas();
            return null;
        }
    }

    //Delete
    private static class DeleteContratistaAsyncTask extends AsyncTask<Integer, Void, Void> {
        private ContratistaDao contratistaDao;

        private DeleteContratistaAsyncTask(ContratistaDao contratistaDao) {
            this.contratistaDao = contratistaDao;
        }

        @Override
        protected Void doInBackground(Integer... id) {
            contratistaDao.deleteObject(id[0]);
            return null;
        }
    }

}
