package com.mdp.fusionapp.database.service;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.ContratistaEntity;
import com.mdp.fusionapp.database.entity.ProyectoEntity;

import java.util.List;

@Dao
public interface ContratistaDao {

    @Insert
    void insert(ContratistaEntity contratistaEntity);

    @Query("DELETE  FROM contratista_table WHERE idEmpresaObservadora=:idEmpresaObservadora")
    void deleteObject(Integer idEmpresaObservadora);

    @Query("DELETE FROM contratista_table")
    void deleteAllContratistas();

    @Query("SELECT * FROM contratista_table WHERE idProyecto=:idProyecto")
    List<ContratistaEntity> findContratistaByProyecto(Integer idProyecto);
}
