package com.mdp.fusionapp.database.service.incidente;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.incidente.CrearIncidenteEntity;
import com.mdp.fusionapp.database.entity.interventoria.CrearInterventoriaEntiy;

import java.util.List;

@Dao
public interface CrearIncidenteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)


    long insert(CrearIncidenteEntity crearIncidenteEntity);

    @Query("DELETE FROM crear_incidente_table WHERE id=:id")
    void deleteById(Integer id);

    @Query("DELETE FROM crear_incidente_table")
    void deleteAll();

    @Query("SELECT * FROM crear_incidente_table")
    List<CrearIncidenteEntity> findAll();

    @Query("SELECT * FROM crear_incidente_table  where estadoRegistroDB == 2 OR estadoRegistroDB == 1")
    List<CrearIncidenteEntity> findAllByEstado();

    @Query("SELECT * FROM crear_incidente_table WHERE id=:id")
    CrearIncidenteEntity findAllById(Integer id);
}
