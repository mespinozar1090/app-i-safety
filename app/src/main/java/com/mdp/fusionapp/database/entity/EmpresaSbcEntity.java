package com.mdp.fusionapp.database.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "empresa_sbc_table")
public class EmpresaSbcEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("Id_Empresa_Observadora")
    private Integer idEmpresaObservadora;

    @SerializedName("Descripcion")
    private String descripcion;

    @SerializedName("Estado_empresa")
    private Boolean estadoEmpresa;

    @SerializedName("Prefijo")
    private String prefijo;

    @SerializedName("Ruc")
    private String ruc;

    public EmpresaSbcEntity(Integer idEmpresaObservadora, String descripcion) {
        this.idEmpresaObservadora = idEmpresaObservadora;
        this.descripcion = descripcion;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdEmpresaObservadora() {
        return idEmpresaObservadora;
    }

    public void setIdEmpresaObservadora(Integer idEmpresaObservadora) {
        this.idEmpresaObservadora = idEmpresaObservadora;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getEstadoEmpresa() {
        return estadoEmpresa;
    }

    public void setEstadoEmpresa(Boolean estadoEmpresa) {
        this.estadoEmpresa = estadoEmpresa;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    @Override
    public String toString() {
        return descripcion;
    }
}
