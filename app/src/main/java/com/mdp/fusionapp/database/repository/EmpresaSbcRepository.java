package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.EmpresaSbcEntity;
import com.mdp.fusionapp.database.entity.EspecialidadEntity;
import com.mdp.fusionapp.database.service.EmpresaSbcDao;
import com.mdp.fusionapp.database.service.EspecialidadDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class EmpresaSbcRepository {

    private EmpresaSbcDao empresaSbcDao;

    public EmpresaSbcRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        this.empresaSbcDao = database.empresaSbcDao();
    }

    public void insert(EmpresaSbcEntity entity) {
        new InsertAsyncTask(empresaSbcDao).execute(entity);
    }

    public void deleteAll() {
        new DeleteAsyncTask(empresaSbcDao).execute();
    }

    public List<EmpresaSbcEntity> findAll() {
        try {
            return new GetAllAsyncTask(empresaSbcDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


    //Insert
    private static class InsertAsyncTask extends AsyncTask<EmpresaSbcEntity, Void, Void> {
        private EmpresaSbcDao empresaSbcDao;

        private InsertAsyncTask(EmpresaSbcDao empresaSbcDao) {
            this.empresaSbcDao = empresaSbcDao;
        }

        @Override
        protected Void doInBackground(EmpresaSbcEntity... entities) {
            empresaSbcDao.insert(entities[0]);
            return null;
        }
    }

    //Delete
    private static class DeleteAsyncTask extends AsyncTask<Void, Void, Void> {
        private EmpresaSbcDao empresaSbcDao;

        private DeleteAsyncTask(EmpresaSbcDao empresaSbcDao) {
            this.empresaSbcDao = empresaSbcDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            empresaSbcDao.deleteAll();
            return null;
        }
    }

    private static class GetAllAsyncTask extends AsyncTask<Integer, String, List<EmpresaSbcEntity>> {
        private EmpresaSbcDao empresaSbcDao;

        public GetAllAsyncTask(EmpresaSbcDao empresaSbcDao) {
            this.empresaSbcDao = empresaSbcDao;
        }

        @Override
        protected List<EmpresaSbcEntity> doInBackground(Integer... integers) {
            return empresaSbcDao.findAll();
        }
    }
}
