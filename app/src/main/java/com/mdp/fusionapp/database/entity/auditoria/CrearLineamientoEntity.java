package com.mdp.fusionapp.database.entity.auditoria;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "crear_lineamiento_table")
public class CrearLineamientoEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private Long idOffline;
    private Integer idAuditoriaDetalle;
    private Integer idAuditoriaItems;
    private Integer idAuditoriaLinSubItems;
    private Integer idCalificacion;
    private String imgBase64;
    private Integer idLugar;
    private String mensage;
    private String descripcionItem;

    public String getDescripcionItem() {
        return descripcionItem;
    }

    public void setDescripcionItem(String descripcionItem) {
        this.descripcionItem = descripcionItem;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Long getIdOffline() {
        return idOffline;
    }

    public void setIdOffline(Long idOffline) {
        this.idOffline = idOffline;
    }

    public Integer getIdAuditoriaDetalle() {
        return idAuditoriaDetalle;
    }

    public void setIdAuditoriaDetalle(Integer idAuditoriaDetalle) {
        this.idAuditoriaDetalle = idAuditoriaDetalle;
    }

    public Integer getIdAuditoriaItems() {
        return idAuditoriaItems;
    }

    public void setIdAuditoriaItems(Integer idAuditoriaItems) {
        this.idAuditoriaItems = idAuditoriaItems;
    }

    public Integer getIdAuditoriaLinSubItems() {
        return idAuditoriaLinSubItems;
    }

    public void setIdAuditoriaLinSubItems(Integer idAuditoriaLinSubItems) {
        this.idAuditoriaLinSubItems = idAuditoriaLinSubItems;
    }

    public Integer getIdCalificacion() {
        return idCalificacion;
    }

    public void setIdCalificacion(Integer idCalificacion) {
        this.idCalificacion = idCalificacion;
    }

    public String getImgBase64() {
        return imgBase64;
    }

    public void setImgBase64(String imgBase64) {
        this.imgBase64 = imgBase64;
    }

    public Integer getIdLugar() {
        return idLugar;
    }

    public void setIdLugar(Integer idLugar) {
        this.idLugar = idLugar;
    }

    public String getMensage() {
        return mensage;
    }

    public void setMensage(String mensage) {
        this.mensage = mensage;
    }
}
