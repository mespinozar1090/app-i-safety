package com.mdp.fusionapp.database.service;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.UsuarioResponsableEntity;

import java.util.List;

@Dao
public interface UsuarioResponsableDao {

    @Insert
    void insert(UsuarioResponsableEntity usuarioResponsableEntity);

    @Query("DELETE FROM usuario_responsable_table")
    void deleteAll();

    /* @Query("SELECT * FROM usuario_responsable_table")
     LiveData<List<UsuarioResponsableEntity>> findUsuarioResponsables();*/
    @Query("SELECT * FROM usuario_responsable_table")
    List<UsuarioResponsableEntity> findUsuarioResponsables();

}
