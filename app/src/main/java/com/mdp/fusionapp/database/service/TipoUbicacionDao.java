package com.mdp.fusionapp.database.service;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.ContratistaEntity;
import com.mdp.fusionapp.database.entity.ProyectoEntity;
import com.mdp.fusionapp.database.entity.TipoUbicacionEntity;

import java.util.List;

@Dao
public interface TipoUbicacionDao {

    @Insert
    void insert(TipoUbicacionEntity tipoUbicacionEntity);

    @Delete
    void delete(TipoUbicacionEntity tipoUbicacionEntity);

    @Query("DELETE FROM tipoubicacion_table")
    void deleteAll();

    @Query("SELECT * FROM tipoubicacion_table WHERE idProyecto=:idProyecto ")
    List<TipoUbicacionEntity> findTipoUbicacion(Integer idProyecto);
}
