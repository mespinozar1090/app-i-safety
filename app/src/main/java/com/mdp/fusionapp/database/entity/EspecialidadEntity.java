package com.mdp.fusionapp.database.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "especialidad_table")
public class EspecialidadEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("Id_Especialidad")
    private Integer idEspecialidad;

    @SerializedName("Descripcion")
    private String descripcion;

    @SerializedName("Estado")
    private Boolean estado;

    public EspecialidadEntity(Integer idEspecialidad, String descripcion, Boolean estado) {
        this.idEspecialidad = idEspecialidad;
        this.descripcion = descripcion;
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdEspecialidad() {
        return idEspecialidad;
    }

    public void setIdEspecialidad(Integer idEspecialidad) {
        this.idEspecialidad = idEspecialidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return descripcion;
    }
}
