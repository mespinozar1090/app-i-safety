package com.mdp.fusionapp.database.service.incidente;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mdp.fusionapp.network.response.ComboMaestroEntity;
import com.mdp.fusionapp.network.response.InspeccionEmpresaEntity;

import java.util.List;

@Dao
public interface IncidenteEmpresaDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(InspeccionEmpresaEntity inspeccionEmpresaEntity);

    @Query("DELETE FROM incidente_empresa_table")
    void deleteAll();

    @Query("SELECT * FROM incidente_empresa_table")
    List<InspeccionEmpresaEntity> findAll();
}
