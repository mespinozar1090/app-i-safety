package com.mdp.fusionapp.database.service;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.LineamientoEntity;

import java.util.List;

@Dao
public interface LineamientoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(LineamientoEntity lineamientoEntity);

    @Query("DELETE FROM lineamiento_table")
    void deleteAll();

    @Query("SELECT * FROM lineamiento_table")
    List<LineamientoEntity> findAll();
}
