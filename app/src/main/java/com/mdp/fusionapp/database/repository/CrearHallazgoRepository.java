package com.mdp.fusionapp.database.repository;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.entity.CrearHallazgoEntity;
import com.mdp.fusionapp.database.entity.CrearInspeccionEntity;
import com.mdp.fusionapp.database.service.CrearHallazgoDao;
import com.mdp.fusionapp.database.service.CrearInspeccionDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class CrearHallazgoRepository {

    private CrearHallazgoDao crearHallazgoDao;

    public CrearHallazgoRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        this.crearHallazgoDao = database.crearHallazgoDao();
    }

    public Long insert(CrearHallazgoEntity crearHallazgoEntity) {
        try {
            return new InsertAsyncTask(crearHallazgoDao).execute(crearHallazgoEntity).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void updateImageHallazgo(String base64, Long idRow) {
        new UpdateImageHallazgoAsyncTask(crearHallazgoDao, base64, idRow).execute();
    }

    public void updateImageCierre(String base64, Long idRow) {
        new UpdateImageCierreAsyncTask(crearHallazgoDao, base64, idRow).execute();
    }

    public void deleteHallazgo(Integer id) {
        new DeleteHallazgoAsyncTask(crearHallazgoDao).execute(id);
    }

    public void deteleAll() {
        new DeleteAllAsyncTask(crearHallazgoDao).execute();
    }

    public CrearHallazgoEntity findHallazgoById(Integer id) {
        try {
            return new GetHallazgoAsyncTask(crearHallazgoDao).execute(id).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


    public List<CrearHallazgoEntity> findHallazgosByInspeccionOffline(Long idInspeccionOffline) {
        try {
            return new GetListHallazgoAsyncTask(crearHallazgoDao).execute(idInspeccionOffline).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


    //Delete All
    private static class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        private CrearHallazgoDao crearHallazgoDao;

        private DeleteAllAsyncTask(CrearHallazgoDao crearHallazgoDao) {
            this.crearHallazgoDao = crearHallazgoDao;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            crearHallazgoDao.deleteAll();
            return null;
        }
    }

    //Insert
    private static class InsertAsyncTask extends AsyncTask<CrearHallazgoEntity, Void, Long> {
        private CrearHallazgoDao crearHallazgoDao;

        private InsertAsyncTask(CrearHallazgoDao crearHallazgoDao) {
            this.crearHallazgoDao = crearHallazgoDao;
        }

        @Override
        protected Long doInBackground(CrearHallazgoEntity... entities) {
            return crearHallazgoDao.insert(entities[0]);
        }
    }

    //Delete
    private static class DeleteHallazgoAsyncTask extends AsyncTask<Integer, Void, Void> {
        private CrearHallazgoDao crearHallazgoDao;

        private DeleteHallazgoAsyncTask(CrearHallazgoDao crearHallazgoDao) {
            this.crearHallazgoDao = crearHallazgoDao;
        }

        @Override
        protected Void doInBackground(Integer... entities) {
            crearHallazgoDao.deleteHallazgos(entities[0]);
            return null;
        }
    }

    //Select
    private static class GetHallazgoAsyncTask extends AsyncTask<Integer, Void, CrearHallazgoEntity> {
        private CrearHallazgoDao crearHallazgoDao;

        private GetHallazgoAsyncTask(CrearHallazgoDao crearHallazgoDao) {
            this.crearHallazgoDao = crearHallazgoDao;
        }

        @Override
        protected CrearHallazgoEntity doInBackground(Integer... integers) {
            return crearHallazgoDao.findHallazgosById(integers[0]);
        }
    }

    //Select
    private static class GetListHallazgoAsyncTask extends AsyncTask<Long, Void, List<CrearHallazgoEntity>> {
        private CrearHallazgoDao crearHallazgoDao;

        private GetListHallazgoAsyncTask(CrearHallazgoDao crearHallazgoDao) {
            this.crearHallazgoDao = crearHallazgoDao;
        }

        @Override
        protected List<CrearHallazgoEntity> doInBackground(Long... integers) {
            return crearHallazgoDao.findHallazgosByInspecionOffline(integers[0]);
        }
    }

    //Insert
    private static class UpdateImageHallazgoAsyncTask extends AsyncTask<Void, Void, Long> {
        private CrearHallazgoDao crearHallazgoDao;
        private String base64;
        private Long idRow;

        private UpdateImageHallazgoAsyncTask(CrearHallazgoDao crearHallazgoDao, String base64, Long idRow) {
            this.crearHallazgoDao = crearHallazgoDao;
            this.base64 = base64;
            this.idRow = idRow;
        }

        @Override
        protected Long doInBackground(Void... voids) {
            crearHallazgoDao.updaImageEvidencia(base64, idRow);
            return null;
        }
    }

    //Insert
    private static class UpdateImageCierreAsyncTask extends AsyncTask<Void, Void, Long> {
        private CrearHallazgoDao crearHallazgoDao;
        private String base64;
        private Long idRow;

        private UpdateImageCierreAsyncTask(CrearHallazgoDao crearHallazgoDao, String base64, Long idRow) {
            this.crearHallazgoDao = crearHallazgoDao;
            this.base64 = base64;
            this.idRow = idRow;
        }

        @Override
        protected Long doInBackground(Void... voids) {
            crearHallazgoDao.updaImageCierre(base64, idRow);
            return null;
        }
    }

}
