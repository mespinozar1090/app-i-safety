package com.mdp.fusionapp.database.entity.interventoria;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "control_subitem_table")
public class ControlSubItemEntity implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("Id_Control_item")
    private Integer idControlitem;

    @SerializedName("Id_Control")
    private Integer idControl;

    @SerializedName("Descripcion")
    private String descripcion;

    private Integer idControlDetalle;

    private Double cantidad;

    public Integer getIdControlDetalle() {
        return idControlDetalle;
    }

    public void setIdControlDetalle(Integer idControlDetalle) {
        this.idControlDetalle = idControlDetalle;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdControlitem() {
        return idControlitem;
    }

    public void setIdControlitem(Integer idControlitem) {
        this.idControlitem = idControlitem;
    }

    public Integer getIdControl() {
        return idControl;
    }

    public void setIdControl(Integer idControl) {
        this.idControl = idControl;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }
}
