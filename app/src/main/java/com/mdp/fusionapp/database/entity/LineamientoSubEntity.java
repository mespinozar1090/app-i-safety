package com.mdp.fusionapp.database.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "lineamiento_sub_table")
public class LineamientoSubEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("Id_Audit_Lin_SubItems")
    private Integer idAuditLinSubItems;

    @SerializedName("Descripcion_Items")
    private String descripcionItems;

    @SerializedName("Id_Audit_Items")
    private Integer idAuditItems;


    public LineamientoSubEntity(Integer idAuditLinSubItems, String descripcionItems) {
        this.idAuditLinSubItems = idAuditLinSubItems;
        this.descripcionItems = descripcionItems;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdAuditLinSubItems() {
        return idAuditLinSubItems;
    }

    public void setIdAuditLinSubItems(Integer idAuditLinSubItems) {
        this.idAuditLinSubItems = idAuditLinSubItems;
    }

    public String getDescripcionItems() {
        return descripcionItems;
    }

    public void setDescripcionItems(String descripcionItems) {
        this.descripcionItems = descripcionItems;
    }

    public Integer getIdAuditItems() {
        return idAuditItems;
    }

    public void setIdAuditItems(Integer idAuditItems) {
        this.idAuditItems = idAuditItems;
    }
}
