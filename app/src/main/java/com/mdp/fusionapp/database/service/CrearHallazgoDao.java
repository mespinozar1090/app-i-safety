package com.mdp.fusionapp.database.service;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.CrearHallazgoEntity;
import com.mdp.fusionapp.database.entity.CrearInspeccionEntity;

import java.util.List;

@Dao
public interface CrearHallazgoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(CrearHallazgoEntity crearHallazgoEntity);

    @Query("DELETE FROM crear_hallazgo_table WHERE id=:id")
    void deleteHallazgos(Integer id);

    @Query("DELETE FROM crear_hallazgo_table")
    void deleteAll();

    @Query("SELECT * FROM crear_hallazgo_table WHERE id=:id")
    CrearHallazgoEntity findHallazgosById(Integer id);

    @Query("SELECT * FROM crear_hallazgo_table where idInspeccionOffline=:idInspeccionOffline")
    List<CrearHallazgoEntity> findHallazgosByInspecionOffline(Long idInspeccionOffline);

    @Query("UPDATE crear_hallazgo_table SET evidenciaFotoImg64=:base64 where id=:idRow")
    void updaImageEvidencia(String base64, Long idRow);

    @Query("UPDATE crear_hallazgo_table SET evidenciaCierreFotoImg64=:base64 where id=:idRow")
    void updaImageCierre(String base64, Long idRow);

}
