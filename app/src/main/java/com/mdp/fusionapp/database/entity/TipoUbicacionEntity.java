package com.mdp.fusionapp.database.entity;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "tipoubicacion_table")
public class TipoUbicacionEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("idTipoUbicacion")
    private Integer idTipoUbicacion;

    @SerializedName("Descripcion")
    private String descripcion;

    @SerializedName("idProyecto")
    private Integer idProyecto;

    @Ignore
    public TipoUbicacionEntity() {
    }

    public TipoUbicacionEntity(Integer idTipoUbicacion, String descripcion) {
        this.idTipoUbicacion = idTipoUbicacion;
        this.descripcion = descripcion;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdTipoUbicacion() {
        return idTipoUbicacion;
    }

    public void setIdTipoUbicacion(Integer idTipoUbicacion) {
        this.idTipoUbicacion = idTipoUbicacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return descripcion;
    }
}
