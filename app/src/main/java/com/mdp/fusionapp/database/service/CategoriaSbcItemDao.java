package com.mdp.fusionapp.database.service;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.CategoriaSbcItemEntity;

import java.util.List;

@Dao
public interface CategoriaSbcItemDao {
    @Insert
    void insert(CategoriaSbcItemEntity categoriaSbcItemEntity);

    @Query("DELETE FROM categoria_sbcitem_table")
    void deleteAll();

    @Query("SELECT * FROM categoria_sbcitem_table")
    List<CategoriaSbcItemEntity> findAll();
}
