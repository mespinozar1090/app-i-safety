package com.mdp.fusionapp.database.service.interventoria;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mdp.fusionapp.database.entity.interventoria.CrearControlEntity;

import java.util.List;

@Dao
public interface CrearControlDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(CrearControlEntity crearControlEntity);

    @Query("DELETE FROM crear_control_table")
    void deleteAll();

    @Query("DELETE FROM crear_control_table WHERE idOffline=:idOffline")
    void deleteById(Long idOffline);

    @Query("SELECT * FROM crear_control_table")
    List<CrearControlEntity> findAll();

    @Query("SELECT * FROM crear_control_table where idOffline=:idOffline")
    List<CrearControlEntity> findAllById(Long idOffline);
}
