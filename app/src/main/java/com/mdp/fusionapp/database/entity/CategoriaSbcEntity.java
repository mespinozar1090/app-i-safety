package com.mdp.fusionapp.database.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "categoria_sbc_table")
public class CategoriaSbcEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("Id_sbc_Categoria")
    private Integer idsbcCategoria;

    @SerializedName("Descripcion_Categoria")
    private String descripcionCategoria;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdsbcCategoria() {
        return idsbcCategoria;
    }

    public void setIdsbcCategoria(Integer idsbcCategoria) {
        this.idsbcCategoria = idsbcCategoria;
    }

    public String getDescripcionCategoria() {
        return descripcionCategoria;
    }

    public void setDescripcionCategoria(String descripcionCategoria) {
        this.descripcionCategoria = descripcionCategoria;
    }
}
