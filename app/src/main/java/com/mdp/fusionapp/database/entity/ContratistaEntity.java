package com.mdp.fusionapp.database.entity;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "contratista_table")
public class ContratistaEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("Id_Empresa_Observadora")
    private Integer idEmpresaObservadora;

    @SerializedName("Descripcion")
    private String descripcion;

    @SerializedName("idProyecto")
    private Integer idProyecto;


    public ContratistaEntity(Integer idEmpresaObservadora, String descripcion) {
        this.idEmpresaObservadora = idEmpresaObservadora;
        this.descripcion = descripcion;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdEmpresaObservadora() {
        return idEmpresaObservadora;
    }

    public void setIdEmpresaObservadora(Integer idEmpresaObservadora) {
        this.idEmpresaObservadora = idEmpresaObservadora;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return descripcion;
    }
}
