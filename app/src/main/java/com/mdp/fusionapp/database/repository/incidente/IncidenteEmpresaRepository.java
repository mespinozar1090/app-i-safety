package com.mdp.fusionapp.database.repository.incidente;

import android.app.Application;
import android.os.AsyncTask;

import com.mdp.fusionapp.database.conexion.AppDatabase;
import com.mdp.fusionapp.database.service.incidente.ComboMaestroDao;
import com.mdp.fusionapp.database.service.incidente.IncidenteEmpresaDao;
import com.mdp.fusionapp.network.response.ComboMaestroEntity;
import com.mdp.fusionapp.network.response.InspeccionEmpresaEntity;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class IncidenteEmpresaRepository {

    private IncidenteEmpresaDao incidenteEmpresaDao;

    public IncidenteEmpresaRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        this.incidenteEmpresaDao = database.incidenteEmpresaDao();
    }

    public void insert(InspeccionEmpresaEntity inspeccionEmpresaEntity) {
        new InsertAsyncTask(incidenteEmpresaDao).execute(inspeccionEmpresaEntity);
    }

    public void deleteAll() {
        new DeleteAsyncTask(incidenteEmpresaDao).execute();
    }

    public List<InspeccionEmpresaEntity> findAll() {
        try {
            return new FindAllAsyncTask(incidenteEmpresaDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Select
    private static class FindAllAsyncTask extends AsyncTask<Void, Void, List<InspeccionEmpresaEntity>> {
        private IncidenteEmpresaDao incidenteEmpresaDao;

        private FindAllAsyncTask(IncidenteEmpresaDao incidenteEmpresaDao) {
            this.incidenteEmpresaDao = incidenteEmpresaDao;
        }


        @Override
        protected List<InspeccionEmpresaEntity> doInBackground(Void... voids) {
            return incidenteEmpresaDao.findAll();
        }
    }

    //Insert
    private static class InsertAsyncTask extends AsyncTask<InspeccionEmpresaEntity, Void, Void> {
        private IncidenteEmpresaDao incidenteEmpresaDao;

        private InsertAsyncTask(IncidenteEmpresaDao incidenteEmpresaDao) {
            this.incidenteEmpresaDao = incidenteEmpresaDao;
        }

        @Override
        protected Void doInBackground(InspeccionEmpresaEntity... entities) {
            incidenteEmpresaDao.insert(entities[0]);
            return null;
        }
    }

    //Delete
    private static class DeleteAsyncTask extends AsyncTask<Void, Void, Void> {
        private IncidenteEmpresaDao incidenteEmpresaDao;

        private DeleteAsyncTask(IncidenteEmpresaDao incidenteEmpresaDao) {
            this.incidenteEmpresaDao = incidenteEmpresaDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            incidenteEmpresaDao.deleteAll();
            return null;
        }
    }

}
