package com.mdp.fusionapp.database.service;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.mdp.fusionapp.database.entity.UsuarioEntity;

@Dao
public interface UsuarioDao {

    @Insert
    void insert(UsuarioEntity usuarioEntity);

    @Update
    void update(UsuarioEntity usuarioEntity);

    @Delete
    void delete(UsuarioEntity usuarioEntity);

    @Query("DELETE FROM usuario_table")
    void deleteAllUser();

    @Query("SELECT * FROM usuario_table WHERE usuario=:usuario AND clave=:clave")
    LiveData<UsuarioEntity> getUserAndPassword(String usuario, String clave);

    @Query("SELECT * FROM usuario_table WHERE usuario=:usuario")
    LiveData<UsuarioEntity> getByUser(String usuario);

    @Query("SELECT * FROM usuario_table WHERE usuario=:usuario")
    UsuarioEntity getByUserName(String usuario);
}
