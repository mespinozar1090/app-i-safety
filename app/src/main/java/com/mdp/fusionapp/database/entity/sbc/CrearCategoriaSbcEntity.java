package com.mdp.fusionapp.database.entity.sbc;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "crear_categorisbc_table")
public class CrearCategoriaSbcEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private Integer idsbcdetalle;
    private Integer idsbcCategoria;
    private Integer idsbcCategoriaItems;
    private Boolean seguro;
    private Boolean riesgoso;
    private Integer idbarrera;
    private String observacionSbcDetalle;
    private String descripcionItem;
    private Integer idSbc;
    private Long idOffline;
    private Integer estadoOffline;

    public String getDescripcionItem() {
        return descripcionItem;
    }

    public void setDescripcionItem(String descripcionItem) {
        this.descripcionItem = descripcionItem;
    }

    public Long getIdOffline() {
        return idOffline;
    }

    public void setIdOffline(Long idOffline) {
        this.idOffline = idOffline;
    }

    public Integer getEstadoOffline() {
        return estadoOffline;
    }

    public void setEstadoOffline(Integer estadoOffline) {
        this.estadoOffline = estadoOffline;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdsbcdetalle() {
        return idsbcdetalle;
    }

    public void setIdsbcdetalle(Integer idsbcdetalle) {
        this.idsbcdetalle = idsbcdetalle;
    }

    public Integer getIdsbcCategoria() {
        return idsbcCategoria;
    }

    public void setIdsbcCategoria(Integer idsbcCategoria) {
        this.idsbcCategoria = idsbcCategoria;
    }

    public Integer getIdsbcCategoriaItems() {
        return idsbcCategoriaItems;
    }

    public void setIdsbcCategoriaItems(Integer idsbcCategoriaItems) {
        this.idsbcCategoriaItems = idsbcCategoriaItems;
    }

    public Boolean getSeguro() {
        return seguro;
    }

    public void setSeguro(Boolean seguro) {
        this.seguro = seguro;
    }

    public Boolean getRiesgoso() {
        return riesgoso;
    }

    public void setRiesgoso(Boolean riesgoso) {
        this.riesgoso = riesgoso;
    }

    public Integer getIdbarrera() {
        return idbarrera;
    }

    public void setIdbarrera(Integer idbarrera) {
        this.idbarrera = idbarrera;
    }

    public String getObservacionSbcDetalle() {
        return observacionSbcDetalle;
    }

    public void setObservacionSbcDetalle(String observacionSbcDetalle) {
        this.observacionSbcDetalle = observacionSbcDetalle;
    }

    public Integer getIdSbc() {
        return idSbc;
    }

    public void setIdSbc(Integer idSbc) {
        this.idSbc = idSbc;
    }
}
