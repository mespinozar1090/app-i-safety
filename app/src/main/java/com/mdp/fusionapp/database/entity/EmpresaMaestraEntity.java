package com.mdp.fusionapp.database.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "empresa_maestra_table")
public class EmpresaMaestraEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("Id_Empresa_Maestra")
    private Integer idEmpresaMaestra;

    @SerializedName("Razon_social")
    private String razon_social;

    @SerializedName("Ruc")
    private String ruc;

    @SerializedName("Descripcion")
    private String descripcion;

    @SerializedName("Domicilio_Legal")
    private String domicilioLegal;

    @SerializedName("Nro_Trabajadores")
    private Integer nroTrabajadores;

    @SerializedName("Id_Actividad_Economica")
    private Integer idActividadEconomica;

    @SerializedName("idParam")
    private Integer idParam;

    @SerializedName("idProyecto")
    private Integer idProyecto;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdEmpresaMaestra() {
        return idEmpresaMaestra;
    }

    public void setIdEmpresaMaestra(Integer idEmpresaMaestra) {
        this.idEmpresaMaestra = idEmpresaMaestra;
    }

    public String getRazon_social() {
        return razon_social;
    }

    public void setRazon_social(String razon_social) {
        this.razon_social = razon_social;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDomicilioLegal() {
        return domicilioLegal;
    }

    public void setDomicilioLegal(String domicilioLegal) {
        this.domicilioLegal = domicilioLegal;
    }

    public Integer getNroTrabajadores() {
        return nroTrabajadores;
    }

    public void setNroTrabajadores(Integer nroTrabajadores) {
        this.nroTrabajadores = nroTrabajadores;
    }

    public Integer getIdActividadEconomica() {
        return idActividadEconomica;
    }

    public void setIdActividadEconomica(Integer idActividadEconomica) {
        this.idActividadEconomica = idActividadEconomica;
    }

    public Integer getIdParam() {
        return idParam;
    }

    public void setIdParam(Integer idParam) {
        this.idParam = idParam;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }
}
