package com.mdp.fusionapp.database.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "crear_buenapractica_table")
public class CrearBuenaPracticaEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private Long idInspeccionOffline;
    private Integer idBuenaPractica;
    private Integer idProyecto;
    private Integer idInspeccion;
    private Integer idCategoriaBuenaPractica;
    private Integer idEmpresaContratista;
    private String nombreCategoria;
    private String descripcion;
    private String fechaImg;
    private String imgB64;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Long getIdInspeccionOffline() {
        return idInspeccionOffline;
    }

    public void setIdInspeccionOffline(Long idInspeccionOffline) {
        this.idInspeccionOffline = idInspeccionOffline;
    }

    public Integer getIdBuenaPractica() {
        return idBuenaPractica;
    }

    public void setIdBuenaPractica(Integer idBuenaPractica) {
        this.idBuenaPractica = idBuenaPractica;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public Integer getIdInspeccion() {
        return idInspeccion;
    }

    public void setIdInspeccion(Integer idInspeccion) {
        this.idInspeccion = idInspeccion;
    }

    public Integer getIdCategoriaBuenaPractica() {
        return idCategoriaBuenaPractica;
    }

    public void setIdCategoriaBuenaPractica(Integer idCategoriaBuenaPractica) {
        this.idCategoriaBuenaPractica = idCategoriaBuenaPractica;
    }

    public Integer getIdEmpresaContratista() {
        return idEmpresaContratista;
    }

    public void setIdEmpresaContratista(Integer idEmpresaContratista) {
        this.idEmpresaContratista = idEmpresaContratista;
    }

    public String getNombreCategoria() {
        return nombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechaImg() {
        return fechaImg;
    }

    public void setFechaImg(String fechaImg) {
        this.fechaImg = fechaImg;
    }

    public String getImgB64() {
        return imgB64;
    }

    public void setImgB64(String imgB64) {
        this.imgB64 = imgB64;
    }
}
