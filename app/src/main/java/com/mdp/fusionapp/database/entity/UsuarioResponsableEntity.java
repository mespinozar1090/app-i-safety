package com.mdp.fusionapp.database.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "usuario_responsable_table")
public class UsuarioResponsableEntity implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("IdPerfil_Usuario")
    private Integer idPerfilUsuario;

    @SerializedName("Nombre_Usuario")
    private String nombreUsuario;

    @SerializedName("IdUsuario")
    private Integer idUsuario;

    @SerializedName("Apellido_Usuario")
    private String apellidoUsuario;

    @SerializedName("Email_Corporativo")
    private String email;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getIdPerfilUsuario() {
        return idPerfilUsuario;
    }

    public void setIdPerfilUsuario(Integer idPerfilUsuario) {
        this.idPerfilUsuario = idPerfilUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getApellidoUsuario() {
        return apellidoUsuario;
    }

    public void setApellidoUsuario(String apellidoUsuario) {
        this.apellidoUsuario = apellidoUsuario;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public String toString() {
        return nombreUsuario + " " + apellidoUsuario;
    }
}
