package com.mdp.fusionapp.database.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "usuario_noticadores")
public class UsuarioNotificarEntity implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("IdUsers")
    private Integer idUsers;

    @SerializedName("Nombre_Usuario")
    private String nombreUsuario;

    @SerializedName("Email_Corporativo")
    private String emailCorporativo;

    @SerializedName("Apellido_Usuario")
    private String apellidoUsuario;

    private Boolean selectEmail = false;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean getSelectEmail() {
        return selectEmail;
    }

    public void setSelectEmail(Boolean selectEmail) {
        this.selectEmail = selectEmail;
    }

    public Integer getIdUsers() {
        return idUsers;
    }

    public void setIdUsers(Integer idUsers) {
        this.idUsers = idUsers;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getEmailCorporativo() {
        return emailCorporativo;
    }

    public void setEmailCorporativo(String emailCorporativo) {
        this.emailCorporativo = emailCorporativo;
    }

    public String getApellidoUsuario() {
        return apellidoUsuario;
    }

    public void setApellidoUsuario(String apellidoUsuario) {
        this.apellidoUsuario = apellidoUsuario;
    }
}


