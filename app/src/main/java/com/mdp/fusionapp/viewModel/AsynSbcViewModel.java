package com.mdp.fusionapp.viewModel;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.mdp.fusionapp.database.entity.CategoriaSbcEntity;
import com.mdp.fusionapp.database.entity.sbc.CrearCategoriaSbcEntity;
import com.mdp.fusionapp.database.entity.sbc.CrearSbcEntity;
import com.mdp.fusionapp.database.entity.sbc.NotificarEntity;
import com.mdp.fusionapp.database.repository.CrearCategoriaSbcRepository;
import com.mdp.fusionapp.database.repository.CrearSbcRepository;
import com.mdp.fusionapp.database.repository.NotificarRepository;
import com.mdp.fusionapp.network.reposiroty.SbcRepository;
import com.mdp.fusionapp.network.response.AsynSbcResponse;
import com.mdp.fusionapp.network.response.CreateInspeccionResponse;
import com.mdp.fusionapp.network.response.Respuesta;
import com.mdp.fusionapp.network.service.APIService;
import com.mdp.fusionapp.utilitary.Constante;
import com.mdp.fusionapp.utilitary.UtilMDP;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AsynSbcViewModel extends AndroidViewModel {

    private CrearSbcRepository crearSbcRepository;
    private CrearCategoriaSbcRepository crearCategoriaSbcRepository;
    private NotificarRepository notificarRepository;
    private SharedPreferences sharedPreferences;

    public AsynSbcViewModel(@NonNull Application application) {
        super(application);
        crearSbcRepository = new CrearSbcRepository(application);
        crearCategoriaSbcRepository = new CrearCategoriaSbcRepository(application);
        notificarRepository = new NotificarRepository(application);
        sharedPreferences = application.getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
    }

    public void sendDataToOnlineSBC() {
        List<CrearSbcEntity> sbc = crearSbcRepository.findAllByEstado();
        if (sbc != null && sbc.size() > 0) {
            List<HashMap<String, Object>> lstNotificacions = null;
            HashMap<String, Object> notificacion = null;
            HashMap<String, Object> detalle = null;
            List<HashMap<String, Object>> lstDetalle = null;
            for (int i = 0; i < sbc.size(); i++) {
                if (sbc.get(i).getEstadoRegistroDB() == Constante.ESTADO_CREADO || sbc.get(i).getEstadoRegistroDB() == Constante.ESTADO_EDITADO) {
                    lstDetalle = new ArrayList<>();
                    lstNotificacions = new ArrayList<>();

                    List<NotificarEntity> lstNotificacion = notificarRepository.findAllByModulo(Constante.MODULO_SBC, Long.valueOf(sbc.get(i).getId()));

                    //TODO NOTIFICCACIONES
                    for (int x = 0; x < lstNotificacion.size(); x++) {
                        notificacion = new HashMap<>();
                        notificacion.put("IdUser", lstNotificacion.get(x).getIdUser());
                        notificacion.put("Nombre", lstNotificacion.get(x).getNombre());
                        notificacion.put("Email", lstNotificacion.get(x).getEmail());
                        lstNotificacions.add(notificacion);
                    }

                    //TODO SBC CATEGORIAS
                    List<CrearCategoriaSbcEntity> lstDetalleSbc = crearCategoriaSbcRepository.findAll(Long.valueOf(sbc.get(i).getId()));
                    for (int x = 0; x < lstDetalleSbc.size(); x++) {
                        detalle = new HashMap<>();
                        detalle.put("Id_sbc_detalle", lstDetalleSbc.get(x).getIdsbcdetalle());
                        detalle.put("Id_sbc_Categoria", lstDetalleSbc.get(x).getIdsbcCategoria());
                        detalle.put("Id_sbc_Categoria_Items", lstDetalleSbc.get(x).getIdsbcCategoriaItems());
                        detalle.put("Seguro", lstDetalleSbc.get(x).getSeguro());
                        detalle.put("Riesgoso", lstDetalleSbc.get(x).getRiesgoso());
                        detalle.put("Idbarrera", lstDetalleSbc.get(x).getIdbarrera());
                        detalle.put("Observacion_sbc_detalle", lstDetalleSbc.get(x).getObservacionSbcDetalle());
                        detalle.put("Id_sbc", lstDetalleSbc.get(x).getIdSbc());
                        lstDetalle.add(detalle);
                    }

                    HashMap<String, Object> hashMap = new HashMap<>();
                    hashMap.put("Id_sbc", sbc.get(i).getIdSbc());
                    hashMap.put("Nombre_observador", sbc.get(i).getNombreObservador());
                    hashMap.put("IdSede_Proyecto", sbc.get(i).getIdSedeProyecto());
                    hashMap.put("Cargo_Observador", sbc.get(i).getCargoObservador());
                    //hashMap.put("IdLugar_trabajo", "");
                    hashMap.put("Lugar_trabajo ", sbc.get(i).getLugarTrabajo());
                    hashMap.put("Fecha_Registro_SBC", sbc.get(i).getFechaRegistroSbc());
                    hashMap.put("Id_Empresa_Observadora", sbc.get(i).getIdEmpresaObservadora());
                    hashMap.put("Horario_Observacion", sbc.get(i).getHorarioObservacion());
                    hashMap.put("Tiempo_exp_observada", sbc.get(i).getTiempoExpObservada());
                    hashMap.put("Especialidad_Observado", sbc.get(i).getEspecialidaObservado());
                    hashMap.put("Actividad_Observada", sbc.get(i).getActividadObservado());
                    hashMap.put("Id_Area_Trabajo", sbc.get(i).getIdAreaTrabajo());
                    hashMap.put("Hora_SBC", null);
                    hashMap.put("Fecha_registro", sbc.get(i).getFechaRegistroSbc());
                    hashMap.put("Tipo_SBC", null);
                    hashMap.put("IdCargo", null);
                    hashMap.put("Descripcion_Area_Observada", sbc.get(i).getDescripcionAreaObservada());
                    hashMap.put("lstDetalleSBC", lstDetalle);// tener presente
                    hashMap.put("IdSede_Proyecto_Observado", sbc.get(i).getIdSedeProyectoObservado());
                    hashMap.put("IdUsuario", sbc.get(i).getIdUsuario());
                    hashMap.put("Tipo_Registro", 2);
                    hashMap.put("Origen_Registro", "App");
                    hashMap.put("UsuariosNotificados", lstNotificacions);
                    hashMap.put("NombreFormato", sbc.get(i).getNombreFormato());

                    if (sbc.get(i).getIdSbc() == null) {
                        sendDataToServerWSInsert(hashMap, sbc.get(i).getId());
                    } else {
                        sendDataToServerWSEditar(hashMap, sbc.get(i).getId());
                    }
                }else{
                    HashMap<String, Integer> hashMap = new HashMap<>();
                    hashMap.put("idSBC", sbc.get(i).getIdSbc());
                    eliminarRegistro(hashMap, sbc.get(i).getId());
                }
            }
        } else {
            dataOnlineToStorage();
        }
    }

    private void eliminarRegistro(HashMap hashMap, Integer idRow){
        APIService  apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<Boolean> listCall = apiService.borrarSBC(hashMap);
        listCall.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.body() != null) {
                    crearSbcRepository.deleteById(idRow);
                    List<CrearSbcEntity> sbcEntities = crearSbcRepository.findAllByEstado();
                    if (sbcEntities != null && sbcEntities.size() == 0) {
                        dataOnlineToStorage();
                    }
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.e("SBC", "eliminarRegistro " + t.getMessage());
            }
        });
    }

    private void sendDataToServerWSInsert(HashMap hashMap, Integer idSbc) {
        APIService apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<Respuesta> listCall = apiService.insertarSBC(hashMap);
        listCall.enqueue(new Callback<Respuesta>() {
            @Override
            public void onResponse(Call<Respuesta> call, Response<Respuesta> response) {
                if (response.body() != null) {
                    Log.e("sendDataToServerWSInsert", "INSERTO SBC Y ELIMINARA");
                    crearSbcRepository.deleteById(idSbc);
                    crearCategoriaSbcRepository.deleteById(Long.valueOf(idSbc));
                    notificarRepository.deleteById(Constante.MODULO_SBC, Long.valueOf(idSbc));

                    List<CrearSbcEntity> sbc = crearSbcRepository.findAllByEstado();
                    if (sbc != null && sbc.size() == 0) {
                        Log.e("sendDataToServerWSInsert", "INSERTO SBC , ELIMINO Y DESCARGARA LOS ULTIMOS 15");
                        dataOnlineToStorage();
                    }
                }
            }

            @Override
            public void onFailure(Call<Respuesta> call, Throwable t) {
                Log.e("error", "sendDataToServerWSInsert " + t.getMessage());
            }
        });
    }


    private void sendDataToServerWSEditar(HashMap hashMap, Integer idInspeccionOffline) {
        APIService apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<Respuesta> listCall = apiService.editarSBC(hashMap);

        listCall.enqueue(new Callback<Respuesta>() {
            @Override
            public void onResponse(Call<Respuesta> call, Response<Respuesta> response) {
                if (response.body() != null) {
                    Log.e("sendDataToServerWSInsert", "EDITO SBC Y ELIMINARA");
                    crearSbcRepository.deleteById(idInspeccionOffline);
                    crearCategoriaSbcRepository.deleteById(Long.valueOf(idInspeccionOffline));
                    notificarRepository.deleteById(Constante.MODULO_SBC, Long.valueOf(idInspeccionOffline));
                    List<CrearSbcEntity> sbc = crearSbcRepository.findAllByEstado();
                    if (sbc != null && sbc.size() == 0) {
                        Log.e("sendDataToServerWSInsert", "INSERTO SBC , ELIMINO Y DESCARGARA LOS ULTIMOS 15");
                        dataOnlineToStorage();
                    }
                }
            }

            @Override
            public void onFailure(Call<Respuesta> call, Throwable t) {
                Log.e("error", "sendDataToServerWSEditar" + t.getMessage());
            }
        });
    }


    //TODO OBTENER REGISTRO DESDE ONLINE Y ALMACENAR
    public void dataOnlineToStorage() {
        HashMap<String, Object> hashMapShared = new HashMap<>();
        hashMapShared.put("Id_Usuario", sharedPreferences.getInt("IdUsers", 0));
        hashMapShared.put("Id_Rol_Usuario", sharedPreferences.getInt("Id_Rol_Usuario", 0));
        hashMapShared.put("Id_Rol_General", sharedPreferences.getInt("Id_Rol_General", 0));


        APIService apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<AsynSbcResponse>> listCall = apiService.listarSbcDetalles(hashMapShared);
        listCall.enqueue(new Callback<List<AsynSbcResponse>>() {
            @Override
            public void onResponse(Call<List<AsynSbcResponse>> call, Response<List<AsynSbcResponse>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    crearSbcRepository.deleteAll();
                    List<AsynSbcResponse> responses = response.body();
                    CrearSbcEntity object = null;
                    for (int i = 0; i < responses.size(); i++) {
                        object = new CrearSbcEntity();
                        object.setNombreObservador(responses.get(i).getNombreobservador());
                        object.setIdSedeProyecto(responses.get(i).getIdSedeProyecto());
                        object.setCargoObservador(responses.get(i).getCargoObservador());
                        object.setLugarTrabajo(responses.get(i).getLugarTrabajo());
                        object.setFechaRegistroSbc(responses.get(i).getFechaRegistroSBC());
                        object.setIdEmpresaObservadora(responses.get(i).getIdEmpresaObservadora());
                        object.setHorarioObservacion(Integer.parseInt(responses.get(i).getHorarioObservacion()));
                        object.setTiempoExpObservada(responses.get(i).getTiempoexpobservada());
                        object.setEspecialidaObservado(responses.get(i).getEspecialidadObservado());
                        object.setActividadObservado(responses.get(i).getActividadObservada());
                        object.setIdAreaTrabajo(responses.get(i).getIdAreaTrabajo());
                        object.setFechaRegristo(responses.get(i).getFecharegistro());
                        object.setDescripcionAreaObservada(responses.get(i).getDescripcionAreaObservada());
                        object.setIdSedeProyectoObservado(responses.get(i).getIdSedeProyectoObservado());
                        object.setIdUsuario(responses.get(i).getIdUsuario());
                        object.setNombreFormato(responses.get(i).getNombreFormato());
                        object.setIdSbc(responses.get(i).getIdsbc());

                        Long idRow = crearSbcRepository.insert(object);
                        if (idRow != null) {
                            List<AsynSbcResponse.Categoria> categorias = responses.get(i).getLstDetalleSBC();
                            if (categorias != null && categorias.size() > 0) {
                                CrearCategoriaSbcEntity crearCategoriaSbcEntity = null;
                                for (int x = 0; x < categorias.size(); x++) {
                                    crearCategoriaSbcEntity = new CrearCategoriaSbcEntity();
                                    crearCategoriaSbcEntity.setIdsbcdetalle(categorias.get(x).getIdSBCDetalle());
                                    crearCategoriaSbcEntity.setIdsbcCategoria(categorias.get(x).getIdSBCCategoria());
                                    crearCategoriaSbcEntity.setIdsbcCategoriaItems(categorias.get(x).getIdSBCCategoriaItems());
                                    crearCategoriaSbcEntity.setSeguro(categorias.get(x).getSeguro());
                                    crearCategoriaSbcEntity.setRiesgoso(categorias.get(x).getRiesgoso());
                                    crearCategoriaSbcEntity.setIdbarrera(categorias.get(x).getIdbarrera());
                                    crearCategoriaSbcEntity.setObservacionSbcDetalle(categorias.get(x).getObservacionSBCDetalle());
                                    crearCategoriaSbcEntity.setIdSbc(categorias.get(x).getIdsbc());
                                    crearCategoriaSbcEntity.setIdOffline(idRow);
                                    crearCategoriaSbcEntity.setDescripcionItem(categorias.get(x).getDescripcionItem());
                                    crearCategoriaSbcRepository.insert(crearCategoriaSbcEntity);
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<AsynSbcResponse>> call, Throwable t) {
                Log.e("error", "dataOnlineToStorage SBC" + t.getMessage());
            }
        });
    }
}
