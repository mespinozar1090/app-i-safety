package com.mdp.fusionapp.viewModel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.mdp.fusionapp.database.entity.sbc.CrearCategoriaSbcEntity;
import com.mdp.fusionapp.database.entity.sbc.CrearSbcEntity;
import com.mdp.fusionapp.database.entity.sbc.NotificarEntity;
import com.mdp.fusionapp.database.repository.CategoriaSbcItemRepository;
import com.mdp.fusionapp.database.repository.CategoriaSbcRepository;
import com.mdp.fusionapp.database.repository.CrearCategoriaSbcRepository;
import com.mdp.fusionapp.database.repository.CrearSbcRepository;
import com.mdp.fusionapp.database.repository.EmpresaSbcRepository;
import com.mdp.fusionapp.database.repository.EspecilidadRepository;
import com.mdp.fusionapp.database.repository.NotificarRepository;
import com.mdp.fusionapp.database.repository.SedeProyectoRepository;
import com.mdp.fusionapp.network.reposiroty.SbcRepository;
import com.mdp.fusionapp.database.entity.CategoriaSbcItemEntity;
import com.mdp.fusionapp.database.entity.CategoriaSbcEntity;
import com.mdp.fusionapp.database.entity.EmpresaSbcEntity;
import com.mdp.fusionapp.database.entity.EspecialidadEntity;
import com.mdp.fusionapp.network.response.Respuesta;
import com.mdp.fusionapp.network.response.SbcResponse;
import com.mdp.fusionapp.database.entity.SedeProyectoEntity;
import com.mdp.fusionapp.network.response.detalle.DetalleSbcResponse;

import java.util.HashMap;
import java.util.List;

public class SbcViewModel extends AndroidViewModel {

    private SedeProyectoRepository sedeProyectoRepository;
    private EspecilidadRepository especialidaRepository;
    private EmpresaSbcRepository empresaSbcRepository;
    private CategoriaSbcRepository categoriaSbcRepository;
    private CategoriaSbcItemRepository categoriaSbcItemRepository;

    private CrearSbcRepository crearSbcRepository;
    private CrearCategoriaSbcRepository crearCategoriaSbcRepository;
    private NotificarRepository notificarRepository;


    public SbcViewModel(@NonNull Application application) {
        super(application);
        sedeProyectoRepository = new SedeProyectoRepository(application);
        especialidaRepository = new EspecilidadRepository(application);
        empresaSbcRepository = new EmpresaSbcRepository(application);
        categoriaSbcRepository = new CategoriaSbcRepository(application);
        categoriaSbcItemRepository = new CategoriaSbcItemRepository(application);
        crearSbcRepository = new CrearSbcRepository(application);
        crearCategoriaSbcRepository = new CrearCategoriaSbcRepository(application);
        notificarRepository = new NotificarRepository(application);
    }

    public MutableLiveData<List<SbcResponse>> listarSBC(HashMap hashMap, Context context) {
        return SbcRepository.getInstance().listarSBC(hashMap, context);
    }

    public MutableLiveData<List<EmpresaSbcEntity>> obtenerEmpresaSBSResponse() {
        return SbcRepository.getInstance().obtenerEmpresaSBSResponse(empresaSbcRepository);
    }

    public List<EmpresaSbcEntity> obtenerEmpresaSBSResponseOffline() {
        return empresaSbcRepository.findAll();
    }

    public MutableLiveData<List<SedeProyectoEntity>> obtenerSede(HashMap hashMap) {
        return SbcRepository.getInstance().obtenerSede(hashMap);
    }

    public MutableLiveData<List<SedeProyectoEntity>> obtenerSedeM(HashMap hashMap) {
        return SbcRepository.getInstance().obtenerSedeM(hashMap, sedeProyectoRepository);
    }


    public List<SedeProyectoEntity> obtenerSedeOffline(HashMap hashMap) {
        return sedeProyectoRepository.findAll(hashMap);
    }

    public MutableLiveData<List<EspecialidadEntity>> obtenerEspecialidad() {
        return SbcRepository.getInstance().obtenerEspecialidad(especialidaRepository);
    }

    public MutableLiveData<List<CategoriaSbcEntity>> obtenerCategoriaSBC() {
        return SbcRepository.getInstance().obtenerCategoriaSBC(categoriaSbcRepository);
    }

    public MutableLiveData<List<CategoriaSbcItemEntity>> obtenerCategoriaItemSBC() {
        return SbcRepository.getInstance().obtenerCategoriaItemSBC(categoriaSbcItemRepository);
    }

    public MutableLiveData<Respuesta> insertarSBC(HashMap hashMap, Context context) {
        return SbcRepository.getInstance().insertarSBC(hashMap, context);
    }

    public MutableLiveData<Respuesta> editarSBC(HashMap hashMap, Context context) {
        return SbcRepository.getInstance().editarSBC(hashMap, context);
    }

    public MutableLiveData<DetalleSbcResponse> obtenerRegistroSBC(Integer id, Context context) {
        return SbcRepository.getInstance().obtenerRegistroSBC(id, context);
    }

    public MutableLiveData<Boolean> borrarSBC(HashMap hashMap, Context context) {
        return SbcRepository.getInstance().borrarSBC(hashMap);
    }

    public Long insertSbcOffline(CrearSbcEntity crearSbcEntity) {
        return crearSbcRepository.insert(crearSbcEntity);
    }

    //TODO OFFLINE
    public List<CrearSbcEntity> findAllSbcOffline() {
        return crearSbcRepository.findAll();
    }


    public CrearSbcEntity findBySbcOffline(Long id) {
        return crearSbcRepository.findById(id);
    }

    public List<CrearCategoriaSbcEntity> findByIdSbcOffline(Long id) {
        return crearCategoriaSbcRepository.findAll(id);
    }


    public void insertCategoriaOffline(CrearCategoriaSbcEntity crearCategoriaSbcEntity) {
        crearCategoriaSbcRepository.insert(crearCategoriaSbcEntity);
    }

    public void deleteCategoriaByIdSbcOffline(Long idInspeccionOffline) {
        crearCategoriaSbcRepository.deleteById(idInspeccionOffline);
    }


    public List<EmpresaSbcEntity> empresaContratistaOffline() {
        return empresaSbcRepository.findAll();
    }

    public void insertNotificarOffline(NotificarEntity notificarEntity) {
        notificarRepository.insert(notificarEntity);
    }

    public void updateEstadoEliminar(Integer idSbc) {
        crearSbcRepository.updateEstadoEliminar(idSbc);
    }

    public void deleteById(Integer idSbc) {
        crearSbcRepository.deleteById(idSbc);
    }
}
