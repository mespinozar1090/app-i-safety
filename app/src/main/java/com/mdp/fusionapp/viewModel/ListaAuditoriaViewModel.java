package com.mdp.fusionapp.viewModel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.mdp.fusionapp.database.entity.auditoria.CrearAuditoriaEntity;
import com.mdp.fusionapp.database.entity.auditoria.CrearDetalleAudotiraEntity;
import com.mdp.fusionapp.database.entity.sbc.NotificarEntity;
import com.mdp.fusionapp.database.repository.CrearAuditoriaRepository;
import com.mdp.fusionapp.database.repository.CrearDetalleAuditoriaRepository;
import com.mdp.fusionapp.database.repository.LineamientoRepository;
import com.mdp.fusionapp.database.repository.LineamientoSubRepository;
import com.mdp.fusionapp.database.repository.NotificarRepository;
import com.mdp.fusionapp.network.reposiroty.AuditoriaRepository;
import com.mdp.fusionapp.network.response.AuditoriaResponse;
import com.mdp.fusionapp.database.entity.LineamientoEntity;
import com.mdp.fusionapp.database.entity.LineamientoSubEntity;
import com.mdp.fusionapp.network.response.Respuesta;
import com.mdp.fusionapp.network.response.detalle.DetalleAuditoriaResponse;

import java.util.HashMap;
import java.util.List;

public class ListaAuditoriaViewModel extends AndroidViewModel {

    private LineamientoRepository lineamientoRepository;
    private LineamientoSubRepository lineamientoSubRepository;
    private NotificarRepository notificarRepository;
    private CrearAuditoriaRepository crearAuditoriaRepository;
    private CrearDetalleAuditoriaRepository crearDetalleAuditoriaRepository;

    public ListaAuditoriaViewModel(@NonNull Application application) {
        super(application);
        lineamientoRepository = new LineamientoRepository(application);
        lineamientoSubRepository = new LineamientoSubRepository(application);
        notificarRepository = new NotificarRepository(application);
        crearAuditoriaRepository = new CrearAuditoriaRepository(application);
        crearDetalleAuditoriaRepository = new CrearDetalleAuditoriaRepository(application);
    }

    public MutableLiveData<List<AuditoriaResponse>> listaAuditoria(HashMap hashMap, Context context) {
        return AuditoriaRepository.getInstance().listaAuditoria(hashMap, context);
    }

    public MutableLiveData<List<LineamientoEntity>> obtenerLineaminetos() {
        return AuditoriaRepository.getInstance().obtenerLineaminetos(lineamientoRepository);
    }

    public MutableLiveData<List<LineamientoSubEntity>> obtenerLineaminetosSub() {
        return AuditoriaRepository.getInstance().obtenerLineaminetosSub(lineamientoSubRepository);
    }

    public MutableLiveData<Respuesta> insertaAuditoria(HashMap hashMap, Context context) {
        return AuditoriaRepository.getInstance().insertaAuditoria(hashMap, context);
    }

    public MutableLiveData<DetalleAuditoriaResponse> obtenerRegistroAuditoria(Integer idAuditoria) {
        return AuditoriaRepository.getInstance().obtenerRegistroAuditoria(idAuditoria);
    }

    public MutableLiveData<Respuesta> editarAuditoria(HashMap hashMap) {
        return AuditoriaRepository.getInstance().editarAuditoria(hashMap);
    }


    public List<LineamientoEntity> lineamientoOffline() {
        return lineamientoRepository.findAll();
    }

    public List<LineamientoSubEntity> lineamientoSubOffline() {
        return lineamientoSubRepository.findAll();
    }

    public void insertNotificarOffline(NotificarEntity notificarEntity) {
        notificarRepository.insert(notificarEntity);
    }

    public Long insertAuditoriaOffline(CrearAuditoriaEntity crearAuditoriaEntity) {
        return crearAuditoriaRepository.insert(crearAuditoriaEntity);
    }

    public List<CrearAuditoriaEntity> findAllOffline() {
        return crearAuditoriaRepository.findAll();
    }

    public CrearAuditoriaEntity findById(Integer id) {
        return crearAuditoriaRepository.findById(id);
    }

    public List<CrearDetalleAudotiraEntity> findDetailByAuditoria(Long id) {
        return crearDetalleAuditoriaRepository.findByIdOffline(id);
    }

    public void deleteByIdAuditoria(Long id) {
        crearDetalleAuditoriaRepository.deleteByIdAuditoria(id);
    }



    public Long insetDetalleAudtoriaOffline(CrearDetalleAudotiraEntity crearDetalleAudotiraEntity) {
        return crearDetalleAuditoriaRepository.insert(crearDetalleAudotiraEntity);
    }
}
