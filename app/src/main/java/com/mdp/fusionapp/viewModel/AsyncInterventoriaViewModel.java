package com.mdp.fusionapp.viewModel;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.mdp.fusionapp.database.entity.interventoria.CrearControlEntity;
import com.mdp.fusionapp.database.entity.interventoria.CrearEvidenciaInterEntity;
import com.mdp.fusionapp.database.entity.interventoria.CrearInterventoriaEntiy;
import com.mdp.fusionapp.database.entity.interventoria.CrearVerificacionEntity;
import com.mdp.fusionapp.database.entity.sbc.NotificarEntity;
import com.mdp.fusionapp.database.repository.CrearAuditoriaRepository;
import com.mdp.fusionapp.database.repository.CrearSbcRepository;
import com.mdp.fusionapp.database.repository.NotificarRepository;
import com.mdp.fusionapp.database.repository.interventoria.CrearControlRepository;
import com.mdp.fusionapp.database.repository.interventoria.CrearEvidenciaInterRepository;
import com.mdp.fusionapp.database.repository.interventoria.CrearInterventoriaRepository;
import com.mdp.fusionapp.database.repository.interventoria.CrearVerificacionRepository;
import com.mdp.fusionapp.model.DetalleVerificacionInterModel;
import com.mdp.fusionapp.network.reposiroty.InterventoriaRespository;
import com.mdp.fusionapp.network.response.AsynSbcResponse;
import com.mdp.fusionapp.network.response.Respuesta;
import com.mdp.fusionapp.network.response.interventoria.InterventoriaDetalleOffineResponse;
import com.mdp.fusionapp.network.service.APIService;
import com.mdp.fusionapp.utilitary.Constante;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AsyncInterventoriaViewModel extends AndroidViewModel {

    private SharedPreferences sharedPreferences;
    private CrearInterventoriaRepository crearInterventoriaRepository;
    private CrearVerificacionRepository crearVerificacionRepository;
    private CrearControlRepository crearControlRepository;
    private CrearEvidenciaInterRepository crearEvidenciaInterRepository;
    private NotificarRepository notificarRepository;

    public AsyncInterventoriaViewModel(@NonNull Application application) {
        super(application);
        sharedPreferences = application.getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        crearInterventoriaRepository = new CrearInterventoriaRepository(application);
        crearVerificacionRepository = new CrearVerificacionRepository(application);
        crearControlRepository = new CrearControlRepository(application);
        crearEvidenciaInterRepository = new CrearEvidenciaInterRepository(application);
        notificarRepository = new NotificarRepository(application);
    }

    public void sendDataToOnlineInterventoria(Activity activity) {
        List<CrearInterventoriaEntiy> interventoria = crearInterventoriaRepository.findAllByEstado();
        if (interventoria != null && interventoria.size() > 0) {
            List<HashMap<String, Object>> lstNotificacions = null;
            HashMap<String, Object> notificacion = null;
            List<HashMap<String, Object>> lstVerificacionesMap = null;
            HashMap<String, Object> verificaciones = null;

            List<HashMap<String, Object>> lstControlsMap = null;
            HashMap<String, Object> controles = null;

            for (int i = 0; i < interventoria.size(); i++) {
                if (interventoria.get(i).getEstadoRegistroDB() == Constante.ESTADO_CREADO || interventoria.get(i).getEstadoRegistroDB() == Constante.ESTADO_EDITADO) {
                    lstNotificacions = new ArrayList<>();
                    lstVerificacionesMap = new ArrayList<>();
                    lstControlsMap = new ArrayList<>();

                    List<NotificarEntity> lstNotificacion = notificarRepository.findAllByModulo(Constante.MODULO_INTERVENTORIA, (long) interventoria.get(i).getId());
                    //TODO NOTIFICCACIONES
                    for (int x = 0; x < lstNotificacion.size(); x++) {
                        notificacion = new HashMap<>();
                        notificacion.put("IdUser", lstNotificacion.get(x).getIdUser());
                        notificacion.put("Nombre", lstNotificacion.get(x).getNombre());
                        notificacion.put("Email", lstNotificacion.get(x).getEmail());
                        lstNotificacions.add(notificacion);
                    }

                    List<CrearVerificacionEntity> data = crearVerificacionRepository.findAll((long) interventoria.get(i).getId());
                    for (int x = 0; x < data.size(); x++) {
                        verificaciones = new HashMap<>();
                        verificaciones.put("Id_Interventoria_Detalle", data.get(x).getIdInterventoriaDetalle());
                        verificaciones.put("Id_Verificacion_Subitem", data.get(x).getIdVerificacionSubitem());
                        verificaciones.put("Id_Verificacion", data.get(x).getIdVerificacion());
                        verificaciones.put("Cumple", data.get(x).getCumple());
                        verificaciones.put("Id_Interventoria", interventoria.get(i).getIdInterventoria());
                        lstVerificacionesMap.add(verificaciones);
                    }

                    List<CrearControlEntity> lstControl = crearControlRepository.findAll((long) interventoria.get(i).getId());
                    for (int x = 0; x < lstControl.size(); x++) {
                        controles = new HashMap<>();
                        controles.put("Id_Control_Detalle", lstControl.get(x).getIdControlDetalle());
                        controles.put("Id_Control_item", lstControl.get(x).getIdControlItem());
                        controles.put("Id_Control", lstControl.get(x).getIdControl());
                        controles.put("Cantidad", lstControl.get(x).getCantidad());
                        lstControlsMap.add(controles);
                    }

                    List<HashMap<String, Object>> lstEvidenciaMap = new ArrayList<>();
                    HashMap<String, Object> evidencia = null;
                    List<CrearEvidenciaInterEntity> evidentecia = crearEvidenciaInterRepository.findAll((long) interventoria.get(i).getId());

                    for (int x = 0; x < evidentecia.size(); x++) {
                        evidencia = new HashMap<>();
                        evidencia.put("Id_Interventoria_Imagen", evidentecia.get(x).getIdInterventoriaImagen());
                        evidencia.put("Id_Interventoria", evidentecia.get(x).getIdInterventoria());
                        evidencia.put("Imagen", evidentecia.get(x).getImgBase64());
                        evidencia.put("Descripcion", evidentecia.get(x).getDescripcion());
                        evidencia.put("FechaCreacion", evidentecia.get(x).getFechaCreacion());
                        lstEvidenciaMap.add(evidencia);
                    }


                    HashMap<String, Object> hashMap = new HashMap<>();
                    hashMap.put("Id_Interventoria", interventoria.get(i).getIdInterventoria());
                    hashMap.put("Actividad", interventoria.get(i).getActividad());
                    hashMap.put("Conclucion", interventoria.get(i).getConclucion());
                    hashMap.put("Empresa_Intervenida", interventoria.get(i).getEmpresaIntervenida());
                    hashMap.put("Empresa_Inteventor", interventoria.get(i).getEmpresaInterventor());
                    hashMap.put("Estado", interventoria.get(i).getEstado());
                    hashMap.put("Fecha_Hora", interventoria.get(i).getFechaHora());
                    hashMap.put("Fecha_Registro", interventoria.get(i).getFechaRegistro());
                    hashMap.put("Id_Plan_Trabajo", interventoria.get(i).getIdPlanTrabajo());
                    hashMap.put("PlanTrabajo", interventoria.get(i).getIdPlanTrabajo());
                    hashMap.put("IdUsuario", interventoria.get(i).getIdUsuario());
                    hashMap.put("Interventor", interventoria.get(i).getInterventor());
                    hashMap.put("Linea_Subestacion", interventoria.get(i).getLineaSubEstacion());
                    hashMap.put("Tipo_Ubicacion", interventoria.get(i).getTipoUbicacion());  //insertObject.getLineaSubEstacion()
                    hashMap.put("LstControles", lstControlsMap);
                    hashMap.put("LstDetalle", lstVerificacionesMap);
                    hashMap.put("Lugar_Zona", interventoria.get(i).getLugarZona());
                    hashMap.put("Nro_Plan_Trabajo", interventoria.get(i).getNroPlanTrabajo());
                    hashMap.put("Supervisor", interventoria.get(i).getSupervisor());
                    hashMap.put("Supervisor_Sustituto", interventoria.get(i).getSupervisorSustituto());
                    hashMap.put("Tipo_Plan", 1);
                    hashMap.put("Tipo_Registro", 2);
                    hashMap.put("Origen_Registro", "App");
                    hashMap.put("UsuariosNotificados", lstNotificacions);//lstNotificacions
                    hashMap.put("LstImagenes", lstEvidenciaMap);
                    hashMap.put("NombreFormato", interventoria.get(i).getNombreFormato());

                    if (null != interventoria.get(i).getIdInterventoria() && interventoria.get(i).getIdInterventoria() != 0) {
                        editarInterventoria(hashMap, interventoria.get(i).getId(), activity);
                    } else {
                        crearInterventoria(hashMap, interventoria.get(i).getId(), activity);
                    }
                } else if (interventoria.get(i).getEstadoRegistroDB() == Constante.ESTADO_ELIMINADO) {
                    HashMap<String, Integer> hashMap = new HashMap<>();
                    hashMap.put("Id_Interventoria", interventoria.get(i).getIdInterventoria());
                    eliminarRegistro(hashMap,interventoria.get(i).getId(),activity);
                }
            }

        } else {
            dataOnlineToStorage(activity);
        }
    }

    public void eliminarRegistro(HashMap hashMap, Integer idRow,Activity activity){
        APIService apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<Boolean> listCall = apiService.borrarInterventoria(hashMap);

        listCall.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                Log.e("TAG", "onResponse eliminado: " + response.body());
                crearInterventoriaRepository.deleteById(idRow);
                List<CrearInterventoriaEntiy> interventoriaEntiys = crearInterventoriaRepository.findAllByEstado();
                if (interventoriaEntiys != null && interventoriaEntiys.size() == 0) {
                    dataOnlineToStorage(activity);
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.e("TAG", "Throwable: " + t.getMessage());

            }
        });
    }

    public void editarInterventoria(HashMap hashMap, Integer idInterventoria, Activity activity) {
        Log.e("EDITAR", "ENTRO A EDITAR INTERVENTORIAAAAAAAAAAA");
        APIService apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<Respuesta> empresaProyecto = apiService.editarInterventoria(hashMap);

        empresaProyecto.enqueue(new Callback<Respuesta>() {
            @Override
            public void onResponse(Call<Respuesta> call, Response<Respuesta> response) {
                if (response.body() != null) {
                    Log.e("onResponse", "EDITADO CORRECTAMENTE");
                    crearInterventoriaRepository.deleteById(idInterventoria);
                    crearVerificacionRepository.deleteById(Long.valueOf(idInterventoria));
                    crearControlRepository.deleteById(Long.valueOf(idInterventoria));
                    crearEvidenciaInterRepository.deleteById(Long.valueOf(idInterventoria));
                    notificarRepository.deleteById(Constante.MODULO_INTERVENTORIA, Long.valueOf(idInterventoria));
                    dataOnlineToStorage(activity);
                }
            }

            @Override
            public void onFailure(Call<Respuesta> call, Throwable t) {
                Log.e("onFailure", "editarInterventoria");
            }
        });
    }

    public void crearInterventoria(HashMap hashMap, Integer idInterventoria, Activity activity) {
        APIService apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<Respuesta> listCall = apiService.insertarInterventoria(hashMap);

        listCall.enqueue(new Callback<Respuesta>() {
            @Override
            public void onResponse(Call<Respuesta> call, Response<Respuesta> response) {
                if (response.body() != null) {
                    Log.e("onResponse", "CREADO CORRECTAMENTE");
                    crearInterventoriaRepository.deleteById(idInterventoria);
                    crearVerificacionRepository.deleteById(Long.valueOf(idInterventoria));
                    crearControlRepository.deleteById(Long.valueOf(idInterventoria));
                    crearEvidenciaInterRepository.deleteById(Long.valueOf(idInterventoria));
                    notificarRepository.deleteById(Constante.MODULO_INTERVENTORIA, Long.valueOf(idInterventoria));
                    dataOnlineToStorage(activity);
                }
            }

            @Override
            public void onFailure(Call<Respuesta> call, Throwable t) {
                Log.e("onFailure", "crearInterventoria");
            }
        });
    }


    //TODO OBTENER REGISTRO DESDE ONLINE Y ALMACENAR
    public void dataOnlineToStorage(Activity activity) {

        Log.e("DEVERIA", "DEBERIA ENTRAR AQUII!");
        HashMap<String, Object> hashMapShared = new HashMap<>();
        hashMapShared.put("Id_Usuario", sharedPreferences.getInt("IdUsers", 0));
        hashMapShared.put("Id_Rol_Usuario", sharedPreferences.getInt("Id_Rol_Usuario", 0));
        hashMapShared.put("Id_Rol_General", sharedPreferences.getInt("Id_Rol_General", 0));

        APIService apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<InterventoriaDetalleOffineResponse>> listCall = apiService.detalleInterventoriaOffline(hashMapShared);
        listCall.enqueue(new Callback<List<InterventoriaDetalleOffineResponse>>() {
            @Override
            public void onResponse(Call<List<InterventoriaDetalleOffineResponse>> call, Response<List<InterventoriaDetalleOffineResponse>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    // saveDataOfflineToDB(response.body(), activity);
                    crearInterventoriaRepository.deleteAll();
                    crearVerificacionRepository.deleteAll();
                    crearControlRepository.deleteAll();
                    crearEvidenciaInterRepository.deleteAll();
                    List<InterventoriaDetalleOffineResponse> responses = response.body();
                    CrearInterventoriaEntiy crearInterventoriaEntiy = null;
                    for (int i = 0; i < responses.size(); i++) {
                        crearInterventoriaEntiy = new CrearInterventoriaEntiy();
                        crearInterventoriaEntiy.setIdInterventoria(responses.get(i).getIdInterventoria());
                        crearInterventoriaEntiy.setIdUsuario(responses.get(i).getIdUsuario());
                        crearInterventoriaEntiy.setEstado(false);
                        crearInterventoriaEntiy.setEmpresaInterventor(responses.get(i).getEmpresaInterventor());
                        crearInterventoriaEntiy.setIdEmpresaIntervenida(responses.get(i).getIdEmpresaIntervenida());
                        crearInterventoriaEntiy.setFechaRegistro(responses.get(i).getFechaRegistro());
                        crearInterventoriaEntiy.setInterventor(responses.get(i).getInterventor());
                        crearInterventoriaEntiy.setFechaHora(responses.get(i).getFechaHora());
                        crearInterventoriaEntiy.setActividad(responses.get(i).getActividad());
                        crearInterventoriaEntiy.setIdPlanTrabajo(responses.get(i).getIdPlanTrabajo());
                        crearInterventoriaEntiy.setEmpresaIntervenida(Integer.parseInt(responses.get(i).getEmpresaIntervenida()));
                        crearInterventoriaEntiy.setSupervisor(responses.get(i).getSupervisor());
                        crearInterventoriaEntiy.setSupervisorSustituto(responses.get(i).getSupervisorSustituto());
                        crearInterventoriaEntiy.setLineaSubEstacion(Integer.parseInt(responses.get(i).getLineaSubestacion()));
                        crearInterventoriaEntiy.setNroPlanTrabajo(responses.get(i).getNroPlanTrabajo());
                        crearInterventoriaEntiy.setPlanTrabajo(Integer.parseInt(responses.get(i).getPlanTrabajo()));
                        crearInterventoriaEntiy.setLugarZona(Integer.parseInt(responses.get(i).getLugarZona()));
                        crearInterventoriaEntiy.setCodigoInterventoria(responses.get(i).getCodigoInterventoria());
                        crearInterventoriaEntiy.setConclucion(responses.get(i).getConcluciones());
                        crearInterventoriaEntiy.setNombreObservador(responses.get(i).getNombreObservador());
                        crearInterventoriaEntiy.setNombreFormato(responses.get(i).getNombreFormato());
                        crearInterventoriaEntiy.setTipoUbicacion(responses.get(i).getTipoUbicacion() != null ? Integer.parseInt(responses.get(i).getTipoUbicacion()) : 0);
                        Long idRow = crearInterventoriaRepository.insert(crearInterventoriaEntiy);

                        if (idRow != null) {
                            List<InterventoriaDetalleOffineResponse.VerificacionInterventoria> verificacion = responses.get(i).getLstVerificacionesDet();
                            if (verificacion != null && verificacion.size() > 0) {
                                CrearVerificacionEntity crearVerificacionEntity = null;
                                for (int x = 0; x < verificacion.size(); x++) {
                                    crearVerificacionEntity = new CrearVerificacionEntity();
                                    crearVerificacionEntity.setIdOffline(idRow);
                                    crearVerificacionEntity.setIdVerificacion(verificacion.get(x).getIdVerificacion());
                                    crearVerificacionEntity.setIdInterventoriaDetalle(verificacion.get(x).getIdInterventoriaDetalle());
                                    crearVerificacionEntity.setCumple(verificacion.get(x).getCumple());
                                    crearVerificacionEntity.setIdVerificacionSubitem(verificacion.get(x).getIdVerificacionSubitem());
                                    crearVerificacionEntity.setIdInterventoria(verificacion.get(x).getIdInterventoria());
                                    crearVerificacionEntity.setDescripcion(verificacion.get(x).getDescripcion());
                                    crearVerificacionEntity.setIdVerificacionPadre(verificacion.get(x).getIdVerificacionPadre());
                                    crearVerificacionEntity.setIdVerificacionItem(verificacion.get(x).getIdVerificacionItem());
                                    crearVerificacionEntity.setDescripcionItem(verificacion.get(x).getDescripcionItem());
                                    crearVerificacionEntity.setDescripcionPadre(verificacion.get(x).getDescripcionPadre());
                                    crearVerificacionRepository.insert(crearVerificacionEntity);

                                }
                            }

                            List<InterventoriaDetalleOffineResponse.ControlInterventoria> control = responses.get(i).getLstControlesDet();
                            if (control != null && control.size() > 0) {
                                CrearControlEntity crearControlEntity = null;
                                for (int y = 0; y < control.size(); y++) {
                                    crearControlEntity = new CrearControlEntity();
                                    crearControlEntity.setIdOffline(idRow);
                                    crearControlEntity.setIdControlItem(control.get(y).getIdControlItem());
                                    crearControlEntity.setCantidad(control.get(y).getCantidad());
                                    crearControlEntity.setIdControlDetalle(control.get(y).getIdControlDetalle());
                                    crearControlEntity.setIdInteventoria(control.get(y).getIdInterventoria());
                                    crearControlEntity.setIdControl(control.get(y).getIdControl());
                                    crearControlEntity.setDescripcion(control.get(y).getDescripcion());
                                    crearControlRepository.insert(crearControlEntity);
                                }
                            }

                            List<InterventoriaDetalleOffineResponse.EvidenciaInterventoria> evidencia = responses.get(i).getLstImagenesDet();
                            if (evidencia != null && evidencia.size() > 0) {
                                CrearEvidenciaInterEntity crearEvidenciaInterEntity = null;
                                for (int y = 0; y < evidencia.size(); y++) {
                                    crearEvidenciaInterEntity = new CrearEvidenciaInterEntity();
                                    crearEvidenciaInterEntity.setIdOffline(idRow);
                                    crearEvidenciaInterEntity.setIdInterventoriaImagen(evidencia.get(y).getIdInterventoriaImagen());
                                    crearEvidenciaInterEntity.setIdInterventoria(evidencia.get(y).getIdInterventoria());
                                    crearEvidenciaInterEntity.setDescripcion(evidencia.get(y).getDescripcion());
                                    crearEvidenciaInterEntity.setFechaCreacion(evidencia.get(y).getFechaCreacion());
                                    Long idEvidencia = crearEvidenciaInterRepository.insert(crearEvidenciaInterEntity);

                                    if (idEvidencia != null) {
                                        if (evidencia.get(y).getImagen() != null && !evidencia.get(y).getImagen().equals("")) {
                                            downloadImage(evidencia.get(y).getImagen(), idEvidencia);
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            }

            @Override
            public void onFailure(Call<List<InterventoriaDetalleOffineResponse>> call, Throwable t) {

            }
        });
    }

    public void downloadImage(String url, Long idRow) {
        if (!url.equals("")) {
            APIService apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
            Call<ResponseBody> listCall = apiService.downloadImageUrlSync(url);
            listCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        Bitmap scaledBitmap = BitmapFactory.decodeStream(response.body().byteStream());
                        String imgBase64 = "data:image/jpeg;base64," + UtilMDP.converteToBase64(scaledBitmap);
                        crearEvidenciaInterRepository.updateImage(imgBase64, idRow);
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }
    }
}
