package com.mdp.fusionapp.viewModel;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import com.mdp.fusionapp.database.entity.CrearBuenaPracticaEntity;
import com.mdp.fusionapp.database.entity.CrearHallazgoEntity;
import com.mdp.fusionapp.database.entity.CrearInspeccionEntity;
import com.mdp.fusionapp.database.entity.sbc.CrearSbcEntity;
import com.mdp.fusionapp.database.entity.sbc.NotificarEntity;
import com.mdp.fusionapp.database.repository.CategoriaBuenaPracticaRepository;
import com.mdp.fusionapp.database.repository.CrearBuenaPracticaRepository;
import com.mdp.fusionapp.database.repository.CrearHallazgoRepository;
import com.mdp.fusionapp.database.repository.CrearInspeccionRepository;
import com.mdp.fusionapp.database.repository.NotificarRepository;
import com.mdp.fusionapp.network.reposiroty.InspeccionRepository;
import com.mdp.fusionapp.network.response.AsynInspeccionResponse;
import com.mdp.fusionapp.network.response.CreateInspeccionResponse;
import com.mdp.fusionapp.network.service.APIService;
import com.mdp.fusionapp.utilitary.Constante;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AsyncInspeccionViewModel extends AndroidViewModel {

    private CrearHallazgoRepository crearHallazgoRepository;
    private CrearInspeccionRepository crearInspeccionRepository;
    private CategoriaBuenaPracticaRepository categoriaBuenaPracticaRepository;
    private CrearBuenaPracticaRepository crearBuenaPracticaRepository;
    private SharedPreferences sharedPreferences;
    private NotificarRepository notificarRepository;

    public AsyncInspeccionViewModel(@NonNull Application application) {
        super(application);
        crearHallazgoRepository = new CrearHallazgoRepository(application);
        crearInspeccionRepository = new CrearInspeccionRepository(application);
        categoriaBuenaPracticaRepository = new CategoriaBuenaPracticaRepository(application);
        crearBuenaPracticaRepository = new CrearBuenaPracticaRepository(application);
        notificarRepository  = new NotificarRepository(application);

        sharedPreferences = application.getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
    }

    //public void dataOnlineToStorage(HashMap hashMap, Activity activity) {
    public void sendDataToOnlineInspeccion(Activity activity) {

        List<CrearInspeccionEntity> inspe = crearInspeccionRepository.findInspeccionesByEstado();

        Log.e("ins", "eliminadooooo " + inspe.size());
        if (inspe.size() > 0) {
            for (int i = 0; i < inspe.size(); i++) {
                if (inspe.get(i).getEstadoRegistroDB() == Constante.ESTADO_CREADO || inspe.get(i).getEstadoRegistroDB() == Constante.ESTADO_EDITADO) {
                    HashMap<String, Object> hashMap = new HashMap<>();
                    hashMap.put("Id_Inspecciones", inspe.get(i).getIdInspeccion());
                    hashMap.put("idProyecto", inspe.get(i).getIdProyecto());
                    hashMap.put("Id_Empresa_contratista", inspe.get(i).getIdEmpresacontratista());
                    hashMap.put("idTipoUbicacion", inspe.get(i).getIdTipoUbicacion());
                    hashMap.put("idLugar", inspe.get(i).getIdLugar());
                    hashMap.put("Torre", inspe.get(i).getTorre());
                    hashMap.put("Id_Empresa_Observadora", inspe.get(i).getIdEmpresaObservadora()); //
                    hashMap.put("RUC", inspe.get(i).getRuc());
                    hashMap.put("Id_Actividad_Economica", inspe.get(i).getIdActividadEconomica());
                    hashMap.put("Domicilio_Legal", inspe.get(i).getDomicilioLegal());
                    hashMap.put("Nro_Trabajadores", inspe.get(i).getNroTrabajadores());
                    hashMap.put("Tipo_inspeccion", inspe.get(i).getTipoInspeccion());
                    hashMap.put("Fecha_Hora_Inspeccion", inspe.get(i).getFechaHoraIncidenteCreate());
                    hashMap.put("Responsable_Inspeccion", inspe.get(i).getResponsableInspeccion()); // responsableArea
                    hashMap.put("Area_Inspeccionada", inspe.get(i).getAreaInspeccionada()); // como que envio miguel
                    hashMap.put("Responsable_Area_Inspeccion", inspe.get(i).getResponsableAreaInspeccion());

                    hashMap.put("Objectivo_Inspeccion_Interna", Constante.OBJECTICO_INSPECION_INTERNA);
                    hashMap.put("Resultado_Inspeccion", Constante.RESULTADO_INSPECCION);
                    hashMap.put("Conclusiones_Recomendaciones", Constante.CONCLUSIONES_RECOMENTACIONES);
                    hashMap.put("Origen_Registro", "App");

                    hashMap.put("fecha_Hora_Inspeccion", inspe.get(i).getFechaHoraInspeccion());
                    //hashMap.put("UsuariosNotificados", lstNotificacions);
                    hashMap.put("Estado_Inspeccion", inspe.get(i).getEstadoInspeccion());
                    hashMap.put("Id_Usuario", inspe.get(i).getIdUsuario()); // idUsuaroResponsable
                    hashMap.put("InspeccionInterna", 0);
                    hashMap.put("nombreFormato", inspe.get(i).getNombreFormato());
                    hashMap.put("s_fecha_Hora_Inspeccion", inspe.get(i).getSfechaHoraInspeccion());
                    hashMap.put("Id_Usuario_Registro", inspe.get(i).getIdUsuarioRegistro());

                    Long idInspeccionOffline = Long.valueOf(inspe.get(i).getId());

                    sendDataToServerWS(hashMap, idInspeccionOffline, inspe.get(i).getIdProyecto(), activity,inspe.get(i));

                } else if (inspe.get(i).getEstadoRegistroDB() == Constante.ESTADO_ELIMINADO) {
                    HashMap<String, Integer> hashMap = new HashMap<>();
                    hashMap.put("idInspecion", inspe.get(i).getIdInspeccion());
                    eliminarRegistro(hashMap, inspe.get(i).getId(), activity);
                }
            }
        } else {
            dataOnlineToStorage(activity);
        }
    }

    private void eliminarRegistro(HashMap hashMap, Integer idRow, Activity activity) {
        APIService apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<Boolean> empresaProyecto = apiService.eliminarInspeccion(hashMap);

        empresaProyecto.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.body() != null) {
                    crearInspeccionRepository.deleteInspeccion(idRow);
                    List<CrearInspeccionEntity> inspeccionEntities = crearInspeccionRepository.findInspeccionesByEstado();
                    if (inspeccionEntities != null && inspeccionEntities.size() == 0) {
                        dataOnlineToStorage(activity);
                    }
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {

            }
        });
    }

    private void sendDataToServerWS(HashMap hashMap, Long idInspeccionOffline, Integer idProyecto, Activity activity,CrearInspeccionEntity  item) {
        APIService apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<CreateInspeccionResponse> empresaProyecto = apiService.insertarInspeccion(hashMap);
        empresaProyecto.enqueue(new Callback<CreateInspeccionResponse>() {
            @Override
            public void onResponse(Call<CreateInspeccionResponse> call, Response<CreateInspeccionResponse> response) {
                if (response.body() != null) {
                    Log.e("sendDataToServerWS", "INSERTO INSPECCION === " + response.body().getId_Inspecciones());
                    if (response.body().getId_Inspecciones() != null && response.body().getId_Inspecciones() != 0) {
                        sendNotificacionInspeccion(response.body().getId_Inspecciones(),response.body().getCodigoInspecion(),response.body().getProyecto(),item);

                        crearInspeccionRepository.deleteInspeccion(idInspeccionOffline.intValue());
                        sendDataToOnlineHallazgo(response.body().getId_Inspecciones(), idInspeccionOffline, activity);
                        sendDataToOnlineBuenaPractica(response.body().getId_Inspecciones(), idInspeccionOffline, idProyecto);
                    }
                }
            }

            @Override
            public void onFailure(Call<CreateInspeccionResponse> call, Throwable t) {
            }
        });
    }

    private void sendNotificacionInspeccion(Integer idInspeccion,String codigoInspeccion,String nombreproyecto,CrearInspeccionEntity  item){
        List<HashMap<String, Object>> lstNotificacions = null;
        HashMap<String, Object> notificacion = null;
        List<NotificarEntity> lstNotificacion = notificarRepository.findAllByModulo(Constante.MODULO_INSPECCION, Long.valueOf(item.getId()));
        lstNotificacions = new ArrayList<>();

        for (int x = 0; x < lstNotificacion.size(); x++) {
            notificacion = new HashMap<>();
            notificacion.put("IdUser", lstNotificacion.get(x).getIdUser());
            notificacion.put("Nombre", lstNotificacion.get(x).getNombre());
            notificacion.put("Email", lstNotificacion.get(x).getEmail());
            lstNotificacions.add(notificacion);
        }

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("Id_Inspecciones", idInspeccion);
        hashMap.put("codigoInspeccion", codigoInspeccion);
        hashMap.put("nombreproyecto", nombreproyecto);
        hashMap.put("proyecto", nombreproyecto);
        hashMap.put("UsuariosNotificados", lstNotificacions);
        hashMap.put("Origen_Registro", "APP");
        hashMap.put("s_fecha_Hora_Inspeccion", item.getFechaHoraInspeccion());
        hashMap.put("Id_Usuario", sharedPreferences.getInt("IdUsers", 0));

        APIService apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<String> listCall = apiService.sendNotification(hashMap);

        listCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.e("CREO INSPECCION ", "SE DEBE ENVIAR NOTIFICACION A CREAR INSPECCION");
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e("ERRR INSPECCION ", "ERROR AL CREAR LA INSPECCION");

            }
        });
    }

    private void sendDataToOnlineBuenaPractica(Integer idInspeccion, Long idInspeccionOffline, Integer idProyecto) {
        List<CrearBuenaPracticaEntity> buenaPractica = crearBuenaPracticaRepository.findBuenasPracticasByInspeccion(idInspeccionOffline);
        if (buenaPractica != null && buenaPractica.size() > 0) {
            List<Map<String, Object>> listhashMap = new ArrayList<Map<String, Object>>();
            HashMap<String, Object> hashMap = null;
            for (int i = 0; i < buenaPractica.size(); i++) {
                hashMap = new HashMap<>();
                hashMap.put("idBuenasPracticas", buenaPractica.get(i).getIdBuenaPractica());
                hashMap.put("idProyecto", idProyecto);
                hashMap.put("Id_Inspecciones", idInspeccion);
                hashMap.put("idCategoriaBuenaPractica", buenaPractica.get(i).getIdCategoriaBuenaPractica());
                hashMap.put("Id_Empresa_contratista", buenaPractica.get(i).getIdEmpresaContratista());
                hashMap.put("Descripcion", buenaPractica.get(i).getDescripcion());
                hashMap.put("nombreImg", "");
                hashMap.put("fechaImg", buenaPractica.get(i).getFechaImg());
                hashMap.put("ImgB64", buenaPractica.get(i).getImgB64());
                listhashMap.add(hashMap);
            }
            sendBuenPracticaToServer(listhashMap, idInspeccionOffline);
        }
    }

    private void sendBuenPracticaToServer(List<Map<String, Object>> listhashMap, Long idInspeccionOffline) {
        APIService apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<String> empresaProyecto = apiService.insertarBuenaPractica(listhashMap);
        empresaProyecto.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.body() != null) {
                    crearBuenaPracticaRepository.deleteAllByInspeccion(idInspeccionOffline);
                    Log.e("BUENAPRACTICA", "INSERTO BUENAPRACTICA");
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e("BUENAPRACTICA", "onFailure" + t.getMessage());
            }
        });
    }

    private void sendDataToOnlineHallazgo(Integer idInspeccion, Long idInspeccionOffline, Activity activity) {
        List<CrearHallazgoEntity> hallazgo = crearHallazgoRepository.findHallazgosByInspeccionOffline(idInspeccionOffline);
        List<HashMap<String, Object>> hashMapListEnvidencia = null;
        List<HashMap<String, Object>> hashMapList = null;
        HashMap<String, Object> hashMap = null;
        if (hallazgo != null && hallazgo.size() > 0) {
            for (int i = 0; i < hallazgo.size(); i++) {

                if (hallazgo.get(i).getEstadoRegistroDB() != Constante.ESTADO_DOWNLOAD) {
                    Log.e("enviar", "HALLAZGO " + i + 1);
                    hashMapListEnvidencia = new ArrayList<>();
                    hashMapList = new ArrayList<>();

                    if (hallazgo.get(i).getEvidenciaCierreFotoFecha() != null && !hallazgo.get(i).getEvidenciaCierreFotoFecha().equals("") &&
                            hallazgo.get(i).getEvidenciaCierreFotoImg64() != null && !hallazgo.get(i).getEvidenciaCierreFotoImg64().equals("")) {
                        HashMap<String, Object> hashMapCierre = new HashMap<>();
                        hashMapCierre.put("imgB64", hallazgo.get(i).getEvidenciaCierreFotoImg64());
                        hashMapCierre.put("fecha", hallazgo.get(i).getEvidenciaCierreFotoFecha());
                        hashMapListEnvidencia.add(hashMap);
                    }

                    HashMap<String, Object> hashMapFoto = new HashMap<>();
                    hashMapFoto.put("fecha", hallazgo.get(i).getEvidenciaFotoFecha());
                    hashMapFoto.put("imgB64", hallazgo.get(i).getEvidenciaFotoImg64());
                    hashMapList.add(hashMapFoto);

                    hashMap = new HashMap<>();
                    hashMap.put("Id_Inspecciones_reporte_detalle", hallazgo.get(i).getIdInspeccionesReporteDetalle());
                    hashMap.put("Id_Inspeccion", idInspeccion);
                    hashMap.put("idTipoGestion", hallazgo.get(i).getIdTipoGestion());
                    hashMap.put("id_ActoSubestandar", hallazgo.get(i).getIdActoSubEstandar());
                    hashMap.put("id_CondicionSubestandar", hallazgo.get(i).getIdCondicionSubEstandar());
                    hashMap.put("Acto_Subestandar", hallazgo.get(i).getActoSubestandar()); // descripcion
                    hashMap.put("Riesgo_A", hallazgo.get(i).getRiesgoA() ? 1 : 0);
                    hashMap.put("Riesgo_M", hallazgo.get(i).getRiesgoM() ? 1 : 0);
                    hashMap.put("Riesgo_B", hallazgo.get(i).getRiesgoB() ? 1 : 0);
                    hashMap.put("idHallazgo", hallazgo.get(i).getIdTipoHallazgo());
                    hashMap.put("id_NoConformidad", 0);
                    hashMap.put("id_SubFamiliaAmbiental", 0);
                    hashMap.put("Resposable_Area_detalle", hallazgo.get(i).getResposableAreaDetalle());
                    hashMap.put("EvidenciaFotograficaObservacion", hashMapList);
                    hashMap.put("PlazoString", hallazgo.get(i).getPlazaString());
                    hashMap.put("Origen_Registro", "App");
                    hashMap.put("Estado_DetalleInspeccion", 1);
                    hashMap.put("Accion_Mitigadora", hallazgo.get(i).getActionMitigadora());
                    hashMap.put("Evidencia_Cierre", hashMapListEnvidencia.size() > 0 ? hashMapListEnvidencia : null);
                    hashMap.put("Id_Usuario_Registro", hallazgo.get(i).getIdUsuarioRegistro());

                    sendHallazgoToServer(hashMap, hallazgo.get(i).getId(), activity);
                }
            }
        } else {
            List<CrearInspeccionEntity> inspe = crearInspeccionRepository.findInspeccionesByEstado();
            if (inspe != null && inspe.size() == 0) {
                dataOnlineToStorage(activity);
            }
        }
    }

    private void sendHallazgoToServer(HashMap hashMap, Integer idRow, Activity activity) {
        APIService apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<Boolean> empresaProyecto = apiService.insertarHallazgo(hashMap);
        empresaProyecto.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.body() != null) {
                    crearHallazgoRepository.deleteHallazgo(idRow);
                    // dataOnlineToStorage(activity);
                    Log.e("HALLAZGO", "ENVIO HALLAZGO y ELIMINO REGISTRO");
                    List<CrearInspeccionEntity> inspe = crearInspeccionRepository.findInspeccionesByEstado();

                    if (inspe != null && inspe.size() == 0) {
                        Log.e("HALLAZGO", "ENVIO HALLAZGO AHORA DEBE DESCARGA LOS 15 ULTIMOS");
                        dataOnlineToStorage(activity);
                    } else {
                        Log.e("HALLAZGO", "HAY REGISTRO  DE HALLAZGOS");
                    }
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.e("HALLAZGO", "onFailure" + t.getMessage());
            }
        });
    }




    //TODO OBTENER REGISTRO DESDE ONLINE Y ALMACENAR
    public void dataOnlineToStorage(Activity activity) {

        HashMap<String, Object> hashMapShared = new HashMap<>();
        hashMapShared.put("Id_Usuario", sharedPreferences.getInt("IdUsers", 0));
        hashMapShared.put("Id_Rol_Usuario", sharedPreferences.getInt("Id_Rol_Usuario", 0));
        hashMapShared.put("Id_Rol_General", sharedPreferences.getInt("Id_Rol_General", 0));

        APIService apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<AsynInspeccionResponse>> listCall = apiService.listarInspeccionDetalles(hashMapShared);
        listCall.enqueue(new Callback<List<AsynInspeccionResponse>>() {
            @Override
            public void onResponse(Call<List<AsynInspeccionResponse>> call, Response<List<AsynInspeccionResponse>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    CrearInspeccionEntity crearInspeccionEntity = null;
                    crearInspeccionRepository.deteleAll();
                    crearHallazgoRepository.deteleAll();
                    List<AsynInspeccionResponse> asynInspeccionResponses = response.body();
                    for (int i = 0; i < asynInspeccionResponses.size(); i++) {
                        crearInspeccionEntity = new CrearInspeccionEntity();
                        crearInspeccionEntity.setIdInspeccion(asynInspeccionResponses.get(i).getIdInspecciones());
                        crearInspeccionEntity.setIdProyecto(asynInspeccionResponses.get(i).getIdProyecto());
                        crearInspeccionEntity.setIdEmpresacontratista(asynInspeccionResponses.get(i).getIdEmpresaContratista());
                        crearInspeccionEntity.setIdTipoUbicacion(asynInspeccionResponses.get(i).getIdTipoUbicacion());
                        crearInspeccionEntity.setIdLugar(asynInspeccionResponses.get(i).getIdLugar());
                        crearInspeccionEntity.setTorre(asynInspeccionResponses.get(i).getTorre());
                        crearInspeccionEntity.setIdEmpresaObservadora(asynInspeccionResponses.get(i).getIdEmpresaObservadora());
                        Integer ruc = (asynInspeccionResponses.get(i).getRuc().intValue());
                        crearInspeccionEntity.setRuc(String.valueOf(ruc));
                        crearInspeccionEntity.setIdActividadEconomica(asynInspeccionResponses.get(i).getIdActividadEconomica());
                        crearInspeccionEntity.setDomicilioLegal(asynInspeccionResponses.get(i).getDomicilioLegal());
                        crearInspeccionEntity.setNroTrabajadores(asynInspeccionResponses.get(i).getNumeroTrabajadores());
                        crearInspeccionEntity.setTipoInspeccion(asynInspeccionResponses.get(i).getTipoInspeccion());
                        crearInspeccionEntity.setFechaHoraIncidenteCreate(asynInspeccionResponses.get(i).getFechaHoraInspeccion());
                        crearInspeccionEntity.setResponsableInspeccion(asynInspeccionResponses.get(i).getResponsableInspeccion());
                        crearInspeccionEntity.setAreaInspeccionada(asynInspeccionResponses.get(i).getAreaInspeccionada());
                        crearInspeccionEntity.setResponsableAreaInspeccion(asynInspeccionResponses.get(i).getResponsableAreaInspeccion());
                        crearInspeccionEntity.setFechaHoraInspeccion(asynInspeccionResponses.get(i).getFechaHoraInspeccion());
                        crearInspeccionEntity.setEstadoInspeccion(asynInspeccionResponses.get(i).getEstadoInspeccion());
                        crearInspeccionEntity.setIdUsuario(asynInspeccionResponses.get(i).getIdUsuario());
                        crearInspeccionEntity.setNombreFormato(asynInspeccionResponses.get(i).getNombreFormato());
                        crearInspeccionEntity.setSfechaHoraInspeccion(asynInspeccionResponses.get(i).getFechaHoraInspeccion());
                        crearInspeccionEntity.setIdUsuarioRegistro(asynInspeccionResponses.get(i).getIdUsuarioRegistro());
                        crearInspeccionEntity.setNombreSede(asynInspeccionResponses.get(i).getSede());
                        crearInspeccionEntity.setNombreContratista(asynInspeccionResponses.get(i).getEmpresaContratista());
                        crearInspeccionEntity.setNombreUsuario(asynInspeccionResponses.get(i).getNombreAutor());

                        Long idInspeccionOffline = crearInspeccionRepository.insert(crearInspeccionEntity);
                        if (null != idInspeccionOffline) {
                            List<AsynInspeccionResponse.DetalleInspeccion> detalle = asynInspeccionResponses.get(i).getLstDetalleInspeccion();
                            if (detalle != null && detalle.size() > 0) {

                                CrearHallazgoEntity crearHallazgoEntity = null;
                                for (int x = 0; x < detalle.size(); x++) {
                                    crearHallazgoEntity = new CrearHallazgoEntity();
                                    crearHallazgoEntity.setIdInspeccionOffline(idInspeccionOffline);
                                    crearHallazgoEntity.setIdHallazgo(detalle.get(x).getIdInspeccionesReporteDetalle());
                                    crearHallazgoEntity.setIdInspeccion(asynInspeccionResponses.get(i).getIdInspecciones());//idInspeccion
                                    crearHallazgoEntity.setIdTipoGestion(detalle.get(x).getIdTipoGestion());
                                    crearHallazgoEntity.setIdActoSubEstandar(detalle.get(x).getIdActoSubestandar());
                                    crearHallazgoEntity.setIdCondicionSubEstandar(detalle.get(x).getIdCondicionSubestandar());
                                    crearHallazgoEntity.setActoSubestandar(detalle.get(x).getActoSubestandar());
                                    crearHallazgoEntity.setRiesgoA(detalle.get(x).getRiesgoA());
                                    crearHallazgoEntity.setRiesgoB(detalle.get(x).getRiesgoB());
                                    crearHallazgoEntity.setRiesgoM(detalle.get(x).getRiesgoM());
                                    crearHallazgoEntity.setIdTipoHallazgo(detalle.get(x).getIdHallazgo());
                                    crearHallazgoEntity.setNombreTipoHallazgo(detalle.get(x).getNombreTipoHallazgo());
                                    crearHallazgoEntity.setResposableAreaDetalle(detalle.get(x).getResposableAreaDetalle());
                                    crearHallazgoEntity.setNombreActoSubestandar(detalle.get(x).getNombreActoSubestandar());
                                    crearHallazgoEntity.setNombreTipoGestion(detalle.get(x).getNombreTipoGestion());
                                    crearHallazgoEntity.setPlazaString(detalle.get(x).getPlazo());
                                    crearHallazgoEntity.setActionMitigadora(detalle.get(x).getAccionMitigadora());
                                    crearHallazgoEntity.setIdRolUsuario(detalle.get(x).getUsuarioRol());
                                    crearHallazgoEntity.setIdUsuarioRegistro(detalle.get(x).getIdUsuarioRegistro());
                                    crearHallazgoEntity.setEstadoDetalleInspeccion(detalle.get(x).getEstadoCierreInspeccion());
                                    if (detalle.get(x).getListEvidenciaHallazgo() != null && detalle.get(x).getListEvidenciaHallazgo().size() > 0) {
                                        crearHallazgoEntity.setEvidenciaFotoFecha(detalle.get(x).getListEvidenciaHallazgo().get(0).getFechaFoto());
                                    }

                                    if (detalle.get(x).getLstEvidenciaCierre() != null && detalle.get(x).getLstEvidenciaCierre().size() > 0) {
                                        crearHallazgoEntity.setEvidenciaFotoFecha(detalle.get(x).getLstEvidenciaCierre().get(0).getFechaFoto());
                                    }

                                    Long idRow = crearHallazgoRepository.insert(crearHallazgoEntity);
                                    if (idRow != null) {
                                        if (detalle.get(x).getListEvidenciaHallazgo() != null && detalle.get(x).getListEvidenciaHallazgo().size() > 0) {
                                            downloadImageHallazgo(detalle.get(x).getListEvidenciaHallazgo().get(0).getImgB64(), idRow, true);
                                        }
                                        if (detalle.get(x).getLstEvidenciaCierre() != null && detalle.get(x).getLstEvidenciaCierre().size() > 0) {
                                            downloadImageHallazgo(detalle.get(x).getListEvidenciaHallazgo().get(0).getImgB64(), idRow, false);
                                        }
                                    }

                                }
                            }

                            List<AsynInspeccionResponse.BuenaPracticas> list = asynInspeccionResponses.get(i).getLstBuenasPracticas();
                            if (detalle != null && detalle.size() > 0) {
                                CrearBuenaPracticaEntity object = null;
                                for (int x = 0; x < list.size(); x++) {
                                    object = new CrearBuenaPracticaEntity();
                                    object.setIdInspeccionOffline(idInspeccionOffline);
                                    object.setIdBuenaPractica(list.get(x).getIdBuenasPracticas());
                                    object.setIdProyecto(list.get(x).getIdProyecto());
                                    object.setIdCategoriaBuenaPractica(list.get(x).getIdCategoriaBuenaPractica());
                                    object.setDescripcion(list.get(x).getDescripcion());
                                    object.setImgB64(list.get(x).getImgB64());
                                    object.setNombreCategoria(list.get(x).getCategoriaBuenaPractica());
                                    object.setIdProyecto(list.get(x).getIdProyecto());
                                    crearBuenaPracticaRepository.insert(object);
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<AsynInspeccionResponse>> call, Throwable t) {
                Log.e("onFailure", "dataOnlineToStorage Error" + t.getMessage());
            }
        });

    }

    public void downloadImageHallazgo(String url, Long idRow, Boolean evidencia) {
        if (!url.equals("")) {
            APIService apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
            Call<ResponseBody> listCall = apiService.downloadImageUrlSync(url);
            listCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        Bitmap scaledBitmap = BitmapFactory.decodeStream(response.body().byteStream());
                        String imgBase64 = "data:image/jpeg;base64," + UtilMDP.converteToBase64(scaledBitmap);
                        if (evidencia) {
                            crearHallazgoRepository.updateImageHallazgo(imgBase64, idRow);
                        } else {
                            crearHallazgoRepository.updateImageCierre(imgBase64, idRow);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }
    }
}
