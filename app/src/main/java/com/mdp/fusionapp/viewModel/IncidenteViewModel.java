package com.mdp.fusionapp.viewModel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.mdp.fusionapp.database.entity.incidente.CrearIncidenteEntity;
import com.mdp.fusionapp.database.entity.incidente.EquipoEntity;
import com.mdp.fusionapp.database.entity.incidente.ParteCuerpoEntity;
import com.mdp.fusionapp.database.entity.sbc.NotificarEntity;
import com.mdp.fusionapp.database.repository.NotificarRepository;
import com.mdp.fusionapp.database.repository.ProyectoRepository;
import com.mdp.fusionapp.database.repository.incidente.ComboMaestroRepository;
import com.mdp.fusionapp.database.repository.incidente.CrearIncidentesRepository;
import com.mdp.fusionapp.database.repository.incidente.EquipoRepository;
import com.mdp.fusionapp.database.repository.incidente.IncidenteEmpresaRepository;
import com.mdp.fusionapp.database.repository.incidente.ParteCuerpoRepository;
import com.mdp.fusionapp.network.reposiroty.IncidenteRepository;
import com.mdp.fusionapp.network.response.ComboMaestroEntity;
import com.mdp.fusionapp.network.response.IncidenteDetalleResponse;
import com.mdp.fusionapp.network.response.IncidenteResponse;
import com.mdp.fusionapp.network.response.Respuesta;

import java.util.HashMap;
import java.util.List;

public class IncidenteViewModel extends AndroidViewModel {

    private ComboMaestroRepository comboMaestroRepository;
    private IncidenteEmpresaRepository incidenteEmpresaRepository;
    private EquipoRepository equipoRepository;
    private ParteCuerpoRepository parteCuerpoRepository;
    private CrearIncidentesRepository crearIncidenteRepository;
    private NotificarRepository notificarRepository;

    public IncidenteViewModel(@NonNull Application application) {
        super(application);
        incidenteEmpresaRepository = new IncidenteEmpresaRepository(application);
        comboMaestroRepository = new ComboMaestroRepository(application);
        equipoRepository = new EquipoRepository(application);
        parteCuerpoRepository = new ParteCuerpoRepository(application);
        crearIncidenteRepository = new CrearIncidentesRepository(application);
        notificarRepository = new NotificarRepository(application);
    }

    public MutableLiveData<IncidenteResponse> listarIncidente(HashMap hashMap, Context context) {
        return IncidenteRepository.getInstance().listarIncidente(hashMap, context);
    }

    public MutableLiveData<List<ComboMaestroEntity>> obtenerComboMaestroGenerico(String[] array) {
        return IncidenteRepository.getInstance().obtenerComboMaestroGenerico(array);
    }

    public MutableLiveData<List<EquipoEntity>> equipoInvolucrado(String[] array) {
        return IncidenteRepository.getInstance().equipoInvolucrado(array, equipoRepository);
    }

    public List<EquipoEntity> equipoInvolucradoOffline() {
        return equipoRepository.findAll();
    }


    public MutableLiveData<List<ParteCuerpoEntity>> parteCuerpoInvolucrado(String[] array) {
        return IncidenteRepository.getInstance().parteCuerpoInvolucrado(array, parteCuerpoRepository);
    }

    public List<ParteCuerpoEntity> parteCuerpoInvolucradoOffline() {
        return parteCuerpoRepository.findAll();
    }

    public MutableLiveData<Respuesta> insertaIncidente(HashMap hashMap, Context context) {
        return IncidenteRepository.getInstance().insertaIncidente(hashMap, context);
    }

    public Long insertaIncidenteOffline(CrearIncidenteEntity crearIncidenteEntity) {
        return crearIncidenteRepository.insert(crearIncidenteEntity);
    }
    public void insertNotificarOffline(NotificarEntity notificarEntity) {
        notificarRepository.insert(notificarEntity);
    }

    public List<CrearIncidenteEntity> findAllIncidente() {
        return crearIncidenteRepository.findAll();
    }
    public CrearIncidenteEntity findById(Integer id) {
        return crearIncidenteRepository.findById(id);
    }


    public MutableLiveData<IncidenteDetalleResponse> obtenerRegistoIncidente(String[] array) {
        return IncidenteRepository.getInstance().obtenerRegistoIncidente(array);
    }

    public MutableLiveData<Respuesta> editarIncidente(HashMap hashMap, Context context) {
        return IncidenteRepository.getInstance().editarIncidente(hashMap, context);
    }

}


