package com.mdp.fusionapp.viewModel;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.mdp.fusionapp.database.repository.UsuarioNotificadorRepository;
import com.mdp.fusionapp.network.reposiroty.LoginRespository;
import com.mdp.fusionapp.network.response.LoginResponse;
import com.mdp.fusionapp.database.entity.UsuarioNotificarEntity;
import com.mdp.fusionapp.utilitary.NetworkUtil;

import java.util.HashMap;
import java.util.List;


public class LoginViewModel extends AndroidViewModel {

    private UsuarioNotificadorRepository usuarioNotificadorRepository;

    public LoginViewModel(@NonNull Application application) {
        super(application);
        usuarioNotificadorRepository =  new UsuarioNotificadorRepository(application);
    }

    public MutableLiveData<LoginResponse> loginAcceso(HashMap hashMap, Context context) {

        return LoginRespository.getInstance().loginAcceso(hashMap, context);
    }

    public MutableLiveData<List<UsuarioNotificarEntity>> obtenerUsuarioNotificar() {
        return LoginRespository.getInstance().obtenerUsuarioNotificar(usuarioNotificadorRepository);
    }


}
