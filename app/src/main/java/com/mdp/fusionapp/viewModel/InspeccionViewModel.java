package com.mdp.fusionapp.viewModel;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.mdp.fusionapp.database.entity.CategoriaBuenaPracticaEntity;
import com.mdp.fusionapp.database.entity.ContratistaEntity;
import com.mdp.fusionapp.database.entity.CrearBuenaPracticaEntity;
import com.mdp.fusionapp.database.entity.CrearInspeccionEntity;
import com.mdp.fusionapp.database.entity.EmpresaMaestraEntity;
import com.mdp.fusionapp.database.entity.LugarEntity;
import com.mdp.fusionapp.database.entity.ProyectoEntity;
import com.mdp.fusionapp.database.entity.TipoUbicacionEntity;
import com.mdp.fusionapp.database.entity.UsuarioResponsableEntity;
import com.mdp.fusionapp.database.entity.incidente.TipoEventoEntity;
import com.mdp.fusionapp.database.entity.sbc.NotificarEntity;
import com.mdp.fusionapp.database.repository.AreaTrabajoRepository;
import com.mdp.fusionapp.database.repository.CategoriaBuenaPracticaRepository;
import com.mdp.fusionapp.database.repository.ContratistaRepository;
import com.mdp.fusionapp.database.repository.CrearBuenaPracticaRepository;
import com.mdp.fusionapp.database.repository.CrearHallazgoRepository;
import com.mdp.fusionapp.database.repository.CrearInspeccionRepository;
import com.mdp.fusionapp.database.repository.EmpresaMaestraRepository;
import com.mdp.fusionapp.database.repository.LugarRepository;
import com.mdp.fusionapp.database.repository.NotificarRepository;
import com.mdp.fusionapp.database.repository.ProyectoRepository;
import com.mdp.fusionapp.database.repository.TipoUbicacionRepository;
import com.mdp.fusionapp.database.repository.UsuarioNotificadorRepository;
import com.mdp.fusionapp.database.repository.UsuarioResponsableRepository;
import com.mdp.fusionapp.database.repository.incidente.ActividadRepository;
import com.mdp.fusionapp.database.repository.incidente.ComboMaestroRepository;
import com.mdp.fusionapp.database.repository.incidente.IncidenteEmpresaRepository;
import com.mdp.fusionapp.database.repository.incidente.TipoEventoRepository;
import com.mdp.fusionapp.network.reposiroty.HallazgoRepository;
import com.mdp.fusionapp.network.reposiroty.InspeccionRepository;
import com.mdp.fusionapp.network.response.ActividadEntity;
import com.mdp.fusionapp.database.entity.AreaTrabajoEntity;
import com.mdp.fusionapp.network.response.ComboMaestroEntity;
import com.mdp.fusionapp.network.response.CreateInspeccionResponse;
import com.mdp.fusionapp.network.response.InspeccionDetalleResponse;
import com.mdp.fusionapp.network.response.InspeccionEmpresaEntity;
import com.mdp.fusionapp.network.response.InspeccionResponse;
import com.mdp.fusionapp.network.response.TorreLugarResponse;
import com.mdp.fusionapp.utilitary.UtilMDP;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InspeccionViewModel extends AndroidViewModel {

    private ProyectoRepository proyectoRepository;
    private ContratistaRepository contratistaRepository;
    private TipoUbicacionRepository tipoUbicacionRepository;
    private LugarRepository lugarRepository;
    private EmpresaMaestraRepository empresaMaestraRepository;
    private UsuarioResponsableRepository usuarioResponsableRepository;
    private CrearInspeccionRepository crearInspeccionRepository;
    private CategoriaBuenaPracticaRepository categoriaBuenaPracticaRepository;
    private CrearBuenaPracticaRepository crearBuenaPracticaRepository;
    private AreaTrabajoRepository areaTrabajoRepository;
    private CrearHallazgoRepository crearHallazgoRepository;
    private SharedPreferences sharedPreferences;
    private UsuarioNotificadorRepository usuarioNotificadorRepository;
    private NotificarRepository notificarRepository;
    private IncidenteEmpresaRepository incidenteEmpresaRepository;
    private ComboMaestroRepository comboMaestroRepository;
    private ActividadRepository actividadRepository;
    private TipoEventoRepository tipoEventoRepository;

    public InspeccionViewModel(@NonNull Application application) {
        super(application);
        sharedPreferences = application.getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        proyectoRepository = new ProyectoRepository(application);
        contratistaRepository = new ContratistaRepository(application);
        tipoUbicacionRepository = new TipoUbicacionRepository(application);
        lugarRepository = new LugarRepository(application);
        empresaMaestraRepository = new EmpresaMaestraRepository(application);
        usuarioResponsableRepository = new UsuarioResponsableRepository(application);
        crearInspeccionRepository = new CrearInspeccionRepository(application);
        categoriaBuenaPracticaRepository = new CategoriaBuenaPracticaRepository(application);
        crearBuenaPracticaRepository = new CrearBuenaPracticaRepository(application);
        areaTrabajoRepository = new AreaTrabajoRepository(application);
        crearHallazgoRepository = new CrearHallazgoRepository(application);
        usuarioNotificadorRepository = new UsuarioNotificadorRepository(application);
        notificarRepository = new NotificarRepository(application);
        incidenteEmpresaRepository = new IncidenteEmpresaRepository(application);
        comboMaestroRepository = new ComboMaestroRepository(application);
        actividadRepository = new ActividadRepository(application);
        tipoEventoRepository = new TipoEventoRepository(application);
    }

    public MutableLiveData<List<InspeccionResponse>> listaInpecciones(HashMap hashMap, Context context) {
        return InspeccionRepository.getInstance().listaInpecciones(hashMap, context);
    }

    public MutableLiveData<InspeccionDetalleResponse> detalleInspeccion(HashMap hashMap, Context context) {
        return InspeccionRepository.getInstance().detalleInspeccion(hashMap, context);
    }

    public LiveData<List<ProyectoEntity>> obtenerProyectos(HashMap hashMap) {
        return InspeccionRepository.getInstance().obtenerProyectos(hashMap, proyectoRepository);
    }

    public MutableLiveData<List<ContratistaEntity>> obtenerContratista(HashMap hashMap, Context context) {
        return InspeccionRepository.getInstance().obtenerContratista(hashMap, context);
    }

    public MutableLiveData<List<TipoUbicacionEntity>> tipoUbicacion(HashMap hashMap, Context context) {
        return InspeccionRepository.getInstance().tipoUbicacion(hashMap, context);
    }

    public MutableLiveData<List<LugarEntity>> obtenerLugar(HashMap hashMap, Context context) {
        return InspeccionRepository.getInstance().obtenerLugar(hashMap, context);
    }

    public MutableLiveData<List<CategoriaBuenaPracticaEntity>> categoriaBuenaPractica() {
        return InspeccionRepository.getInstance().categoriaBuenaPractica(categoriaBuenaPracticaRepository);
    }

    public MutableLiveData<EmpresaMaestraEntity> empresaProyecto(HashMap hashMap, Context context) {
        return InspeccionRepository.getInstance().empresaProyecto(hashMap, context);
    }

    public MutableLiveData<CreateInspeccionResponse> insertarInspeccion(HashMap hashMap) {
        return InspeccionRepository.getInstance().insertarInspeccion(hashMap);
    }

    public MutableLiveData<Boolean> eliminarInspeccion(HashMap hashMap) {
        return InspeccionRepository.getInstance().eliminarInspeccion(hashMap);
    }

    public void eliminarInspeccionOffline(Integer idRow) {
       crearInspeccionRepository.deleteInspeccion(idRow);
    }

    public void updateEstadoEliminar(Integer idRow) {
        crearInspeccionRepository.updateEstadoEliminar(idRow);
    }

    public MutableLiveData<String> insertarBuenaPractica(List<Map<String, Object>> hashMap) {
        return InspeccionRepository.getInstance().insertarBuenaPractica(hashMap);
    }

    public LiveData<List<UsuarioResponsableEntity>> obtenerUsuarios() {
        HashMap<String, Integer> hashMap = new HashMap<>();
        hashMap.put("Id_Usuario", sharedPreferences.getInt("IdUsers", 0));
        hashMap.put("Id_Rol_Usuario", sharedPreferences.getInt("Id_Rol_Usuario", 0));
        hashMap.put("Id_Rol_General", sharedPreferences.getInt("Id_Rol_General", 0));
        return InspeccionRepository.getInstance().obtenerUsuarios(hashMap, usuarioResponsableRepository);
    }

    public MutableLiveData<List<InspeccionEmpresaEntity>> obtenerEmpresa() {
        return InspeccionRepository.getInstance().obtenerEmpresa(incidenteEmpresaRepository);
    }

    public List<InspeccionEmpresaEntity> obtenerEmpresaOffline() {
        return incidenteEmpresaRepository.findAll();
    }

    public MutableLiveData<List<ActividadEntity>> obtenerActividad() {
        return InspeccionRepository.getInstance().obtenerActividad(actividadRepository);
    }

    public List<ActividadEntity> obtenerActividadOffline() {
        return actividadRepository.findAll();
    }

    public MutableLiveData<List<ComboMaestroEntity>> obtenerComboMaestro(String[] array) {
        return InspeccionRepository.getInstance().obtenerComboMaestro(array, comboMaestroRepository);
    }

    public MutableLiveData<List<TipoEventoEntity>> tipoEvento(String[] array) {
        return InspeccionRepository.getInstance().tipoEvento(array, tipoEventoRepository);
    }

    public List<TipoEventoEntity> tipoEventoOffline() {
        return tipoEventoRepository.findAll();
    }


    public List<ComboMaestroEntity> obtenerComboMaestroOffline() {
        return comboMaestroRepository.findAll();
    }

    public MutableLiveData<List<AreaTrabajoEntity>> obtenerAreaInspeccion() {
        return InspeccionRepository.getInstance().obtenerAreaInspeccion(areaTrabajoRepository);
    }

    public MutableLiveData<List<TorreLugarResponse>> obtenerTorresPorLugar(HashMap hashMap, Context context) {
        return InspeccionRepository.getInstance().obtenerTorresPorLugar(hashMap, context);
    }

    public MutableLiveData<String> sendNotification(HashMap hashMap) {
        return InspeccionRepository.getInstance().sendNotification(hashMap);
    }

    public void interNotificarOffline(NotificarEntity notificarEntity) {
        notificarRepository.insert(notificarEntity);
    }

    //TODO METODOS OFFLINE ----- Start -----
    public void obtenerProyectoOffline(HashMap hashMap) {
        InspeccionRepository.getInstance().obtenerProyectoOffline(hashMap, proyectoRepository);
    }

    public void obtenerContratistaOffline() {
        InspeccionRepository.getInstance().obtenerContratistaOffline(contratistaRepository);
    }

    public void obtenerTipoUbicacionOffline() {
        InspeccionRepository.getInstance().obtenerTipoUbicacionOffline(tipoUbicacionRepository);
    }

    public void obtenerLugarOffline() {
        InspeccionRepository.getInstance().obtenerLugarOffline(lugarRepository);
    }

    public void obtenerEmpresaMaestraOffline() {
        InspeccionRepository.getInstance().obtenerEmpresaMaestraOffline(empresaMaestraRepository);
    }

    public MutableLiveData<Boolean> insertarHallazgo(HashMap hashMap) {
        return HallazgoRepository.getInstance().insertarHallazgo(hashMap);
    }

    public List<ProyectoEntity> findAllProjectOffline(HashMap hashMap) {
        return proyectoRepository.findAllProject(hashMap);
    }

    public List<ContratistaEntity> findContratistaByProyecto(Integer idProyecto) {
        return contratistaRepository.findContratistaByProyecto(idProyecto);
    }

    public List<TipoUbicacionEntity> findTipoUbicacionByProyecto(Integer idProyecto) {
        return tipoUbicacionRepository.findTipoUbicacionByProyecto(idProyecto);
    }

    public List<LugarEntity> findLugarByUbicacion(Integer idTipoUbicacion) {
        return lugarRepository.findLugarByUbicacion(idTipoUbicacion);
    }

    public EmpresaMaestraEntity findEmpresaMaestraByProyectoDB(Integer idProyecto) {
        return empresaMaestraRepository.findEmpresaMaestraByProyecto(idProyecto);
    }

    //CREAR INSPECCION
    public CrearInspeccionEntity findInspeccionByIdOffline(Integer id) {
        return crearInspeccionRepository.findInspeccionById(id);
    }

    public Long insertInspeccionOffline(CrearInspeccionEntity crearInspeccionEntity) {
        return crearInspeccionRepository.insert(crearInspeccionEntity);
    }

    public void deleteInspeccionOffline(Integer id) {
        crearInspeccionRepository.deleteInspeccion(id);
    }

    public List<CrearInspeccionEntity> findInspeccionesOffline() {
        return crearInspeccionRepository.findInspecciones();
    }

    //CREAR BUENA PRACTICA
    public void insertBuenaPracticaOffline(CrearBuenaPracticaEntity crearBuenaPracticaEntity) {
        crearBuenaPracticaRepository.insert(crearBuenaPracticaEntity);
    }

    public void deleteBuenaPracticaffline(Long idInspeccion) {
        crearBuenaPracticaRepository.deleteAllByInspeccion(idInspeccion);
    }

    public List<CrearBuenaPracticaEntity> findBuenaPracticaEntityOffline(Long idInspeccionOffline) {
        return crearBuenaPracticaRepository.findBuenasPracticasByInspeccion(idInspeccionOffline);
    }

}
