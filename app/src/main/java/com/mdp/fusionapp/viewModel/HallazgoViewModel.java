package com.mdp.fusionapp.viewModel;

import android.app.Application;
import android.content.Context;


import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.mdp.fusionapp.database.entity.CrearHallazgoEntity;
import com.mdp.fusionapp.database.repository.CrearHallazgoRepository;
import com.mdp.fusionapp.network.reposiroty.HallazgoRepository;

import java.util.HashMap;
import java.util.List;

public class HallazgoViewModel extends AndroidViewModel {

    private CrearHallazgoRepository crearHallazgoRepository;

    public HallazgoViewModel(@NonNull Application application) {
        super(application);
        crearHallazgoRepository = new CrearHallazgoRepository(application);
    }

    public MutableLiveData<Boolean> insertarHallazgo(HashMap hashMap) {
        return HallazgoRepository.getInstance().insertarHallazgo(hashMap);
    }

    //CREAR HALLAZGO
    public void insertHallazgo(CrearHallazgoEntity crearHallazgoEntity) {
        crearHallazgoRepository.insert(crearHallazgoEntity);
    }

    public CrearHallazgoEntity findHallazgoById(Integer id) {
        return crearHallazgoRepository.findHallazgoById(id);
    }

    public List<CrearHallazgoEntity> findHallazgosByInspeccion(Long idInspeccionOffline) {
        return crearHallazgoRepository.findHallazgosByInspeccionOffline(idInspeccionOffline);
    }
}
