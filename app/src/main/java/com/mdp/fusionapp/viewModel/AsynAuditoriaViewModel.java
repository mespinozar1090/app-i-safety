package com.mdp.fusionapp.viewModel;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.mdp.fusionapp.database.entity.auditoria.CrearAuditoriaEntity;
import com.mdp.fusionapp.database.entity.auditoria.CrearDetalleAudotiraEntity;
import com.mdp.fusionapp.database.entity.sbc.NotificarEntity;
import com.mdp.fusionapp.database.repository.CrearAuditoriaRepository;
import com.mdp.fusionapp.database.repository.CrearDetalleAuditoriaRepository;
import com.mdp.fusionapp.database.repository.NotificarRepository;
import com.mdp.fusionapp.network.response.AsynAutoriaResponse;
import com.mdp.fusionapp.network.response.Respuesta;
import com.mdp.fusionapp.network.response.detalle.DetalleAuditoriaResponse;
import com.mdp.fusionapp.network.service.APIService;
import com.mdp.fusionapp.utilitary.Constante;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AsynAuditoriaViewModel extends AndroidViewModel {

    private CrearDetalleAuditoriaRepository crearDetalleAuditoriaRepository;
    private CrearAuditoriaRepository crearAuditoriaRepository;
    private NotificarRepository notificarRepository;
    private SharedPreferences sharedPreferences;

    public AsynAuditoriaViewModel(@NonNull Application application) {
        super(application);
        crearDetalleAuditoriaRepository = new CrearDetalleAuditoriaRepository(application);
        crearAuditoriaRepository = new CrearAuditoriaRepository(application);
        notificarRepository = new NotificarRepository(application);
        sharedPreferences = application.getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
    }

    public void sendDataToOnlineAuditoria(Activity activity) {
        List<CrearAuditoriaEntity> auditoria = crearAuditoriaRepository.findByIdEstado();
        if (auditoria != null && auditoria.size() > 0) {
            HashMap<String, Object> hashMap = null;
            List<HashMap<String, Object>> lstNotificacions = null;
            HashMap<String, Object> notificacion = null;
            List<HashMap<String, Object>> linemientoMapLis = null;
            List<HashMap<String, Object>> lstDetallePlan = null;
            HashMap<String, Object> detallePlan = new HashMap<>();
            for (int i = 0; i < auditoria.size(); i++) {
                if (auditoria.get(i).getEstadoRegistroDB() == Constante.ESTADO_CREADO || auditoria.get(i).getEstadoRegistroDB() == Constante.ESTADO_EDITADO) {
                    lstNotificacions = new ArrayList<>();
                    linemientoMapLis = new ArrayList<>();
                    lstDetallePlan = new ArrayList<>();
                    lstDetallePlan.add(detallePlan);
                    List<NotificarEntity> lstNotificacion = notificarRepository.findAllByModulo(Constante.MODULO_SBC, Long.valueOf(auditoria.get(i).getId()));

                    //TODO NOTIFICCACIONES
                    for (int x = 0; x < lstNotificacion.size(); x++) {
                        notificacion = new HashMap<>();
                        notificacion.put("IdUser", lstNotificacion.get(x).getIdUser());
                        notificacion.put("Nombre", lstNotificacion.get(x).getNombre());
                        notificacion.put("Email", lstNotificacion.get(x).getEmail());
                        lstNotificacions.add(notificacion);
                    }

                    HashMap<String, Object> liniamientos = null;
                    List<String> evidenciaPicture;
                    List<CrearDetalleAudotiraEntity> lstLineamientos = crearDetalleAuditoriaRepository.findByIdOffline(Long.valueOf(auditoria.get(i).getId()));
                    for (int x = 0; x < lstLineamientos.size(); x++) {
                        evidenciaPicture = new ArrayList<>();
                        liniamientos = new HashMap<>();
                        liniamientos.put("Id_Audit_Detalle", lstLineamientos.get(x).getIdAuditDetalle());
                        liniamientos.put("Id_Audit_Items", lstLineamientos.get(x).getIdAuditItems());
                        liniamientos.put("Id_Audit_Lin_SubItems", lstLineamientos.get(x).getIdAuditLinSubItems());
                        liniamientos.put("Calificacion", lstLineamientos.get(x).getCalificacion());
                        if (!lstLineamientos.get(x).getImagenEvidencia().equals("")) {
                            evidenciaPicture.add(lstLineamientos.get(x).getImagenEvidencia());
                        }

                        liniamientos.put("LstFoto", evidenciaPicture);
                        liniamientos.put("Evidencia", "cas");
                        liniamientos.put("Lugar", lstLineamientos.get(x).getLugar());
                        liniamientos.put("Notas", lstLineamientos.get(x).getNotas());
                        liniamientos.put("TextEvidencia", lstLineamientos.get(x).getNotas());
                        linemientoMapLis.add(liniamientos);
                    }

                    hashMap = new HashMap<>();
                    hashMap.put("Id_Audit", auditoria.get(i).getIdAuditoria());
                    hashMap.put("Estado_Audit", auditoria.get(i).getEstadoAuditoria());
                    hashMap.put("Fecha_Audit", auditoria.get(i).getFechaAuditoria());
                    hashMap.put("Id_Empresa_contratista", auditoria.get(i).getIdEmpresaContratista());
                    hashMap.put("IdUsers", auditoria.get(i).getIdUser());

                    hashMap.put("LstDetalleAudit", linemientoMapLis);
                    hashMap.put("LstDetallePlan", lstDetallePlan);

                    hashMap.put("Nombre_Formato", auditoria.get(i).getNombreFormato());
                    hashMap.put("Nro_Contrato", auditoria.get(i).getNroContrato());
                    hashMap.put("Sede", auditoria.get(i).getSede());
                    hashMap.put("Supervisor_Contrato", auditoria.get(i).getSupervisorContrato());
                    hashMap.put("Responsable_Contratista", auditoria.get(i).getResponsableContratista());
                    hashMap.put("Origen_Registro", "App");
                    hashMap.put("UsuariosNotificados", lstNotificacions);

                    if (null == auditoria.get(i).getIdAuditoria()) {
                        sendDataToServerCreate(hashMap, auditoria.get(i).getId(), activity);
                    } else {
                        sendDataToServerEdit(hashMap, auditoria.get(i).getId(), activity);
                    }
                }
            }
        } else {
            dataOnlineToStorage(activity);
        }
    }

    private void sendDataToServerCreate(HashMap hashMap, Integer id, Activity activity) {

        APIService apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<Respuesta> listCall = apiService.insertaAuditoria(hashMap);

        listCall.enqueue(new Callback<Respuesta>() {
            @Override
            public void onResponse(Call<Respuesta> call, Response<Respuesta> response) {
                if (response.body() != null) {
                    crearAuditoriaRepository.deleteById(id);
                    crearDetalleAuditoriaRepository.deleteByIdAuditoria(Long.valueOf(id));
                    Log.e("Entro", "ENVIO AUDITORIA Y ELIMINO " + id);

                    List<CrearAuditoriaEntity> auditoria = crearAuditoriaRepository.findByIdEstado();

                    if (auditoria != null & auditoria.size() == 0) {
                        Log.e("Entro", "ENVIO AUDITORIA  ELIMINO Y DEBE OBTENER LOS 15 ULTIMOS");
                        dataOnlineToStorage(activity);
                    }
                }
            }

            @Override
            public void onFailure(Call<Respuesta> call, Throwable t) {
                Log.e("onFailure", " sendDataToServerCreate : " + t.getMessage());
            }
        });
    }


    private void sendDataToServerEdit(HashMap hashMap, Integer id, Activity activity) {
        APIService apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<Respuesta> listCall = apiService.editarAuditoria(hashMap);

        listCall.enqueue(new Callback<Respuesta>() {
            @Override
            public void onResponse(Call<Respuesta> call, Response<Respuesta> response) {
                if (response.body() != null) {
                    crearAuditoriaRepository.deleteById(id);
                    Log.e("Entro", "ENVIO AUDITORIA Y ELIMINO " + id);
                }
            }

            @Override
            public void onFailure(Call<Respuesta> call, Throwable t) {
                Log.e("onFailure", " sendDataToServerEdit : " + t.getMessage());
            }
        });
    }


    public void dataOnlineToStorage(Activity activity) {
        HashMap<String, Object> hashMapShared = new HashMap<>();
        hashMapShared.put("Id_Usuario", sharedPreferences.getInt("IdUsers", 0));
        hashMapShared.put("Id_Rol_Usuario", sharedPreferences.getInt("Id_Rol_Usuario", 0));
        hashMapShared.put("Id_Rol_General", sharedPreferences.getInt("Id_Rol_General", 0));

        APIService apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<AsynAutoriaResponse>> listCall = apiService.listaAuditoriaDetalles(hashMapShared);

        listCall.enqueue(new Callback<List<AsynAutoriaResponse>>() {
            @Override
            public void onResponse(Call<List<AsynAutoriaResponse>> call, Response<List<AsynAutoriaResponse>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    initSaveDataToDB(response.body(), activity);
                }
            }

            @Override
            public void onFailure(Call<List<AsynAutoriaResponse>> call, Throwable t) {

            }
        });
    }

    private void initSaveDataToDB(List<AsynAutoriaResponse> response, Activity activity) {
        crearAuditoriaRepository.deleteAll();

        CrearAuditoriaEntity object = null;
        for (int i = 0; i < response.size(); i++) {
            object = new CrearAuditoriaEntity();
            object.setIdAuditoria(response.get(i).getIdAudit());
            object.setIdEmpresaContratista(response.get(i).getIdEmpresaContratista());
            object.setSede(response.get(i).getSede());
            object.setNroContrato(response.get(i).getNroContrato());
            object.setSupervisorContrato(response.get(i).getSupervisorContrato());
            object.setResponsableContratista(response.get(i).getResponsableContratista());
            object.setFechaAuditoria(response.get(i).getFechaAudit());
            object.setCodigoAuditoria(response.get(i).getCodigoAudit());
            object.setEstadoAuditoria(response.get(i).getEstadoAudit());
            object.setNombreAuditor(response.get(i).getNombreAutor());
            object.setFechaRegistro(response.get(i).getFechaReg());
            object.setNombreFormato(response.get(i).getNombreFormato());
            Long idRow = crearAuditoriaRepository.insert(object);

            if (idRow != null) {
                crearDetalleAuditoriaRepository.deleteAll();
                List<DetalleAuditoriaResponse.DetalleAuditoria> evidencia = response.get(i).getLstDetalleAudit();
                if (evidencia != null && evidencia.size() > 0) {

                    CrearDetalleAudotiraEntity detalle = null;
                    for (int x = 0; x < evidencia.size(); x++) {
                        detalle = new CrearDetalleAudotiraEntity();
                        detalle.setIdOffline(idRow);
                        detalle.setIdAudit(evidencia.get(x).getIdAudit());
                        detalle.setIdAuditDetalle(evidencia.get(x).getIdAuditDetalle());
                        detalle.setIdAuditLinSubItems(evidencia.get(x).getIdAuditLinSubItems());
                        detalle.setIdAuditItems(evidencia.get(x).getIdAuditItems());
                        detalle.setCalificacion(evidencia.get(x).getCalificacion());
                        detalle.setLugar(evidencia.get(x).getLugar());
                        detalle.setNotas(evidencia.get(x).getNotas());
                        detalle.setTextEvidencia(evidencia.get(x).getTextEvidencia());
                        detalle.setDescripcionItem(evidencia.get(x).getDescripcionItem());

                        Long idRowDetail = crearDetalleAuditoriaRepository.insert(detalle);

                        List<DetalleAuditoriaResponse.EnvidenciaAuditoria> images = evidencia.get(x).getLstEvidencia();
                        if (images != null && images.size() > 0) {
                            downloadImage(images.get(0).getImagen(), activity.getApplicationContext(), idRowDetail);
                        }
                    }
                }
            }
        }
    }

    public void downloadImage(String url, Context context, Long idRow) {
        Picasso.get().load(url).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, 250, 250, true);
                String imgBase64 = "data:image/jpeg;base64," + UtilMDP.converteToBase64(scaledBitmap);
                crearDetalleAuditoriaRepository.updateImage(imgBase64, idRow);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                Log.e("onBitmapFailed", "" + errorDrawable.toString());
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                Log.e("onPrepareLoad", "");
            }
        });
    }

}
