package com.mdp.fusionapp.viewModel;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.mdp.fusionapp.database.entity.incidente.CrearIncidenteEntity;
import com.mdp.fusionapp.database.entity.sbc.CrearSbcEntity;
import com.mdp.fusionapp.database.entity.sbc.NotificarEntity;
import com.mdp.fusionapp.database.repository.NotificarRepository;
import com.mdp.fusionapp.database.repository.incidente.CrearIncidentesRepository;
import com.mdp.fusionapp.network.reposiroty.IncidenteRepository;
import com.mdp.fusionapp.network.response.IncidenteDetalleResponse;
import com.mdp.fusionapp.network.response.Respuesta;
import com.mdp.fusionapp.network.service.APIService;
import com.mdp.fusionapp.utilitary.Constante;
import com.mdp.fusionapp.utilitary.UtilMDP;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AsynIncidenteViewModel extends AndroidViewModel {

    private SharedPreferences sharedPreferences;
    private CrearIncidentesRepository crearIncidentesRepository;
    private NotificarRepository notificarRepository;

    public AsynIncidenteViewModel(@NonNull Application application) {
        super(application);
        sharedPreferences = application.getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        crearIncidentesRepository = new CrearIncidentesRepository(application);
        notificarRepository = new NotificarRepository(application);
    }

    public void sendDataToOnlineSBC(Activity activity) {
        List<CrearIncidenteEntity> incidente = crearIncidentesRepository.findByIdEstado();
        if (incidente != null && incidente.size() > 0) {
            List<HashMap<String, Object>> lstNotificacions = null;
            HashMap<String, Object> notificacion = null;
            for (int i = 0; i < incidente.size(); i++) {
                if (incidente.get(i).getEstadoRegistroDB() == Constante.ESTADO_CREADO || incidente.get(i).getEstadoRegistroDB() == Constante.ESTADO_EDITADO) {
                    lstNotificacions = new ArrayList<>();

                    List<NotificarEntity> lstNotificacion = notificarRepository.findAllByModulo(Constante.MODULO_INCIDENTE, Long.valueOf(incidente.get(i).getId()));

                    //TODO NOTIFICCACIONES
                    for (int x = 0; x < lstNotificacion.size(); x++) {
                        notificacion = new HashMap<>();
                        notificacion.put("IdUser", lstNotificacion.get(x).getIdUser());
                        notificacion.put("Nombre", lstNotificacion.get(x).getNombre());
                        notificacion.put("Email", lstNotificacion.get(x).getEmail());
                        lstNotificacions.add(notificacion);
                    }

                    List<HashMap<String, Object>> fListaPlanesAccion = new ArrayList<>();
                    HashMap<String, Object> planesAccion = new HashMap<>();
                    fListaPlanesAccion.add(planesAccion);

                    List<HashMap<String, Object>> fListaTestigo = new ArrayList<>();
                    HashMap<String, Object> testigo = new HashMap<>();
                    fListaTestigo.add(testigo);

                    HashMap<String, Object> hashMap = new HashMap<>();
                    hashMap.put("Id_Incidente", incidente.get(i).getIdIncidente());
                    hashMap.put("Empleador_Tipo", incidente.get(i).getEmpleadorTipo());
                    hashMap.put("F_DESCRIPCION_Incidente", incidente.get(i).getFdescripcionIncidente());
                    hashMap.put("F_Empleador_IdActividadEconomica", incidente.get(i).getFempleadorIdActividadEconomica());
                    hashMap.put("F_Empleador_IdEmpresa", incidente.get(i).getFempleadorIdEmpresa());
                    hashMap.put("F_Empleador_IdTamanioEmpresa", incidente.get(i).getFempleadorIdTamanioEmpresa());
                    hashMap.put("F_EmpleadorI_IdActividadEconomica", incidente.get(i).getFempleadorIIdActividadEconomica());
                    hashMap.put("F_EmpleadorI_RazonSocial", incidente.get(i).getFempleadorIRazonSocial());
                    hashMap.put("F_EmpleadorI_RUC", incidente.get(i).getFempleadorIRuc());
                    hashMap.put("F_Empleador_RazonSocial", incidente.get(i).getFempleadorRazonSocial());
                    hashMap.put("F_Empleador_RUC", incidente.get(i).getFempleadorRuc());
                    hashMap.put("F_EVENTO_FechaAccidente", incidente.get(i).getFeventoFechaAccidente()); //getfEventoFechaAccidente
                    hashMap.put("F_EVENTO_FechaInicioInvestigacion", incidente.get(i).getFeventoFechaInicioInvestigacion());
                    hashMap.put("F_EVENTO_HoraAccidente", incidente.get(i).getFeventoHoraAccidente());
                    hashMap.put("F_EVENTO_HuboDanioMaterial", incidente.get(i).getFeventoHuboDanioMaterial());
                    hashMap.put("F_EVENTO_IdEquipoAfectado", incidente.get(i).getFeventoIdEquipoAfectado());
                    hashMap.put("F_EVENTO_IdParteAfectada", incidente.get(i).getFeventoIdParteAfectada());
                    hashMap.put("F_EVENTO_IdTipoEvento", incidente.get(i).getFeventoIdTipoEvento());
                    hashMap.put("F_EVENTO_LugarExacto", incidente.get(i).getFeventoLugarExacto());
                    hashMap.put("F_EVENTO_NumTrabajadoresAfectadas", incidente.get(i).getFeventoNumTrabajadoresAfectadas());
                    hashMap.put("F_ListaPlanesAccion", fListaPlanesAccion);
                    hashMap.put("F_ListaTestigos", fListaTestigo);

                    hashMap.put("P_Empleador_IdEmpresa", incidente.get(i).getPempleadorIdEmpresa());
                    hashMap.put("F_EmpleadorI_IdEmpresa", incidente.get(i).getFempleadorIIdEmpresa());

                    hashMap.put("F_Trabajador_DetalleCategoriaOcupacional", incidente.get(i).getFtrabajadorDetalleCategoriaOcupacional());
                    hashMap.put("F_SUPERVISOR_IdRelacionEmpresa", null);
                    hashMap.put("F_SUPERVISOR_NombreContratista", null);
                    hashMap.put("F_SUPERVISOR_Nombre", null);
                    hashMap.put("F_SUPERVISOR_DNI", null);
                    hashMap.put("F_SUPERVISOR_Antiguedad", null);
                    hashMap.put("F_SUPERVISOR_IdCategoriaOcupacional", null);
                    hashMap.put("F_SUPERVISOR_NroPersonasSuper", null);
                    hashMap.put("F_Trabajador_NombresApellidos", incidente.get(i).getFtrabajadorNombresApellidos());
                    hashMap.put("G_Fecha_Creado", incidente.get(i).getGfechaCreado());
                    hashMap.put("G_Fecha_Modifica", incidente.get(i).getGidFechaModifica());
                    hashMap.put("G_IdProyecto_Sede", incidente.get(i).getGidProyectoSede());
                    hashMap.put("G_IdUsuario_Creado", incidente.get(i).getGidUsuarioCreado());
                    hashMap.put("G_IdUsuario_Modifica", incidente.get(i).getGidUsuarioModifica());
                    hashMap.put("P_EVENTO_IdTipoEvento", 1);
                    hashMap.put("F_IdEstato_Formato", 3);
                    hashMap.put("Tipo_InformeP_F", incidente.get(i).getTipoInformePF());
                    hashMap.put("Origen_Registro", "App");
                    hashMap.put("UsuariosNotificados", lstNotificacions);
                    hashMap.put("G_DescripcionFormato", incidente.get(i).getGdescripcionFormato());

                    if (incidente.get(i).getIdIncidente() != null && incidente.get(i).getIdIncidente() != 0) {
                        sendDataToServerWSEditar(hashMap, incidente.get(i).getId(), activity);
                    } else {
                        sendDataToServerWSInsert(hashMap, incidente.get(i).getId(), activity);
                    }

                }
            }
        } else {
            dataOnlineToStorage(activity);
        }
    }

    private void sendDataToServerWSInsert(HashMap hashMap, Integer idInspeccionOffline, Activity activity) {
        APIService apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<Respuesta> listCall = apiService.insertaIncidente(hashMap);

        listCall.enqueue(new Callback<Respuesta>() {
            @Override
            public void onResponse(Call<Respuesta> call, Response<Respuesta> response) {
                if (response.body() != null) {
                    Log.e("sendDataToServerWSInsert", "INSERTA INCIDENTE Y ELIMINARA");
                    crearIncidentesRepository.deleteById(idInspeccionOffline);
                    notificarRepository.deleteById(Constante.MODULO_INCIDENTE, Long.valueOf(idInspeccionOffline));

                    List<CrearIncidenteEntity> incidente = crearIncidentesRepository.findAll();
                    if (incidente != null && incidente.size() == 0) {
                        dataOnlineToStorage(activity);
                    }
                }
            }

            @Override
            public void onFailure(Call<Respuesta> call, Throwable t) {
                Log.e("error", "sendDataToServerWSInsert" + t.getMessage());
            }
        });
    }


    private void sendDataToServerWSEditar(HashMap hashMap, Integer idInspeccionOffline, Activity activity) {
        APIService apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<Respuesta> listCall = apiService.editarIncidente(hashMap);

        listCall.enqueue(new Callback<Respuesta>() {
            @Override
            public void onResponse(Call<Respuesta> call, Response<Respuesta> response) {
                if (response.body() != null) {
                    Log.e("sendDataToServerWSEditar", "EDITO INCIDENTE Y ELIMINARA");
                    crearIncidentesRepository.deleteById(idInspeccionOffline);
                    notificarRepository.deleteById(Constante.MODULO_INCIDENTE, Long.valueOf(idInspeccionOffline));
                    List<CrearIncidenteEntity> incidente = crearIncidentesRepository.findAll();
                    if (incidente != null && incidente.size() == 0) {
                        dataOnlineToStorage(activity);
                    }
                }
            }

            @Override
            public void onFailure(Call<Respuesta> call, Throwable t) {
                Log.e("error", "sendDataToServerWSEditar" + t.getMessage());
            }
        });
    }

    //TODO OBTENER REGISTRO DESDE ONLINE Y ALMACENAR
    public void dataOnlineToStorage(Activity activity) {
        HashMap<String, Object> hashMapShared = new HashMap<>();
        hashMapShared.put("Id_Usuario", sharedPreferences.getInt("IdUsers", 0));
        hashMapShared.put("Id_Rol_Usuario", sharedPreferences.getInt("Id_Rol_Usuario", 0));
        hashMapShared.put("Id_Rol_General", sharedPreferences.getInt("Id_Rol_General", 0));
        hashMapShared.put("Tipo_Formato", "PRELIMINAR");

        IncidenteRepository.getInstance().detalleIncidenteOffline(hashMapShared).observe((LifecycleOwner) activity, new Observer<List<IncidenteDetalleResponse>>() {
            @Override
            public void onChanged(List<IncidenteDetalleResponse> list) {
                if (list != null && list.size() > 0) {
                    crearIncidentesRepository.deleteAll();
                    CrearIncidenteEntity object = null;
                    for (int i = 0; i < list.size(); i++) {
                        object = new CrearIncidenteEntity();
                        object.setIdIncidente(list.get(i).getIdIncidente());
                        object.setEmpleadorTipo(list.get(i).getEmpleadorTipo());
                        object.setFempleadorIdActividadEconomica(list.get(i).getEmpleadorIdActividadEconomica().getIdElemento());
                        object.setFempleadorIdEmpresa(list.get(i).getEmpleadoridempresa() != null ? list.get(i).getEmpleadoridempresa().getIdElemento() : 0);
                        object.setFempleadorIdTamanioEmpresa(list.get(i).getEmpleadorIdTamanioEmpresa().getIdElemento());
                        object.setFempleadorIIdActividadEconomica(list.get(i).getEmpleadorIIdActividadeconomica().getIdElemento());
                        object.setFempleadorIRazonSocial(list.get(i).getEmpleadorIRazonSocial());
                        object.setFempleadorIRuc(list.get(i).getEmpleadorIRuc());
                        object.setFempleadorRazonSocial(list.get(i).getEmpleadorRazonSocial());
                        object.setFempleadorRuc(list.get(i).getEmpleadorRuc());
                        object.setFeventoFechaAccidente(list.get(i).getEventoFechaAccidente());
                        object.setFeventoFechaInicioInvestigacion(list.get(i).getEventoFechaInicioInvestagacion());
                        object.setFeventoHoraAccidente(list.get(i).getEventoHoraAccidente());
                        object.setFeventoHuboDanioMaterial(list.get(i).getEventoHuboDanioMaterial());
                        object.setFeventoIdEquipoAfectado(list.get(i).getEventoIdEquipoAfectado().getIdElemento());
                    //    object.setFeventoIdParteAfectada(list.get(i).getEventoIdParteAfectada().getIdElemento() != null ? list.get(i).getEventoIdParteAfectada().getIdElemento() : 0);
                        object.setFeventoIdTipoEvento(list.get(i).getEventoIdTipoEvento().getIdElemento());
                        object.setFeventoLugarExacto(list.get(i).getEventoLugarExacto());
                        object.setFeventoNumTrabajadoresAfectadas(list.get(i).getEventoNumeroTrabajadoresAfectados());
                        object.setPempleadorIdEmpresa(list.get(i).getEmpleadoridempresa() != null ? list.get(i).getEmpleadoridempresa().getIdElemento() : 0);
                        object.setFempleadorIIdEmpresa(list.get(i).getFempleadorIIdEmpresa() != null ? list.get(i).getFempleadorIIdEmpresa().getIdElemento() : 0);
                        object.setFtrabajadorNombresApellidos(list.get(i).getTrabajadorNombreApellido());
                        object.setGidProyectoSede(list.get(i).getIdProyectoSede().getIdElemento());
                        object.setGfechaCreado(list.get(i).getG_Fecha_Creado());
                        object.setGidFechaModifica(list.get(i).getG_Fecha_Modifica());
                        object.setGidUsuarioCreado(list.get(i).getG_IdUsuario_Creado().getIdElemento());
                        object.setGidUsuarioModifica(list.get(i).getG_IdUsuario_Modifica().getIdElemento());
                        object.setFdescripcionIncidente(list.get(i).getDescriptionIncidente());
                        object.setGdescripcionFormato(list.get(i).getG_DescripcionFormato());
                        object.setEstadoRegistroDB(Constante.ESTADO_DOWNLOAD);
                        object.setTipoInformePF(list.get(i).getTipoInforme());
                        crearIncidentesRepository.insert(object);
                    }
                }
            }
        });

        /*APIService apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<IncidenteDetalleResponse>> listCall = apiService.detalleIncidenteOffline(hashMapShared);

        listCall.enqueue(new Callback<List<IncidenteDetalleResponse>>() {
            @Override
            public void onResponse(Call<List<IncidenteDetalleResponse>> call, Response<List<IncidenteDetalleResponse>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    crearIncidentesRepository.deleteAll();
                    List<IncidenteDetalleResponse> list = response.body();
                    CrearIncidenteEntity object = null;
                    for (int i = 0; i < list.size(); i++) {
                        object = new CrearIncidenteEntity();
                        object.setIdIncidente(list.get(i).getIdIncidente());
                        object.setEmpleadorTipo(list.get(i).getEmpleadorTipo());
                        object.setFempleadorIdActividadEconomica(list.get(i).getEmpleadorIdActividadEconomica().getIdElemento());
                        object.setFempleadorIdEmpresa(list.get(i).getEmpleadoridempresa() != null ? list.get(i).getEmpleadoridempresa().getIdElemento() : 0);
                        object.setFempleadorIdTamanioEmpresa(list.get(i).getEmpleadorIdTamanioEmpresa().getIdElemento());
                        object.setFempleadorIIdActividadEconomica(list.get(i).getEmpleadorIIdActividadeconomica().getIdElemento());
                        object.setFempleadorIRazonSocial(list.get(i).getEmpleadorIRazonSocial());
                        object.setFempleadorIRuc(list.get(i).getEmpleadorIRuc());
                        object.setFempleadorRazonSocial(list.get(i).getEmpleadorRazonSocial());
                        object.setFempleadorRuc(list.get(i).getEmpleadorRuc());
                        object.setFeventoFechaAccidente(list.get(i).getEventoFechaAccidente());
                        object.setFeventoFechaInicioInvestigacion(list.get(i).getEventoFechaInicioInvestagacion());
                        object.setFeventoHoraAccidente(list.get(i).getEventoHoraAccidente());
                        object.setFeventoHuboDanioMaterial(list.get(i).getEventoHuboDanioMaterial());
                        object.setFeventoIdEquipoAfectado(list.get(i).getEventoIdEquipoAfectado().getIdElemento());
                        object.setFeventoIdParteAfectada(list.get(i).getEventoIdParteAfectada().getIdElemento());
                        object.setFeventoIdTipoEvento(list.get(i).getEventoIdTipoEvento().getIdElemento());
                        object.setFeventoLugarExacto(list.get(i).getEventoLugarExacto());
                        object.setFeventoNumTrabajadoresAfectadas(list.get(i).getEventoNumeroTrabajadoresAfectados());
                        object.setPempleadorIdEmpresa(list.get(i).getEmpleadoridempresa() != null ? list.get(i).getEmpleadoridempresa().getIdElemento() : 0);
                        object.setFempleadorIIdEmpresa(list.get(i).getFempleadorIIdEmpresa() != null ? list.get(i).getFempleadorIIdEmpresa().getIdElemento() : 0);
                        object.setFtrabajadorNombresApellidos(list.get(i).getTrabajadorNombreApellido());
                        object.setGidProyectoSede(list.get(i).getIdProyectoSede().getIdElemento());
                        object.setGfechaCreado(list.get(i).getG_Fecha_Creado());
                        object.setGidFechaModifica(list.get(i).getG_Fecha_Modifica());
                        object.setGidUsuarioCreado(list.get(i).getG_IdUsuario_Creado().getIdElemento());
                        object.setGidUsuarioModifica(list.get(i).getG_IdUsuario_Modifica().getIdElemento());
                        object.setFdescripcionIncidente(list.get(i).getDescriptionIncidente());
                        object.setGdescripcionFormato(list.get(i).getG_DescripcionFormato());
                        object.setEstadoRegistroDB(Constante.ESTADO_DOWNLOAD);
                        object.setTipoInformePF(list.get(i).getTipoInforme());
                        crearIncidentesRepository.insert(object);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<IncidenteDetalleResponse>> call, Throwable t) {

            }
        });*/


    }
}
