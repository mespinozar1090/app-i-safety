package com.mdp.fusionapp.viewModel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.mdp.fusionapp.database.entity.interventoria.CrearControlEntity;
import com.mdp.fusionapp.database.entity.interventoria.CrearEvidenciaInterEntity;
import com.mdp.fusionapp.database.entity.interventoria.CrearInterventoriaEntiy;
import com.mdp.fusionapp.database.entity.interventoria.CrearVerificacionEntity;
import com.mdp.fusionapp.database.entity.sbc.NotificarEntity;
import com.mdp.fusionapp.database.repository.ControlSubItemRepository;
import com.mdp.fusionapp.database.repository.NotificarRepository;
import com.mdp.fusionapp.database.repository.VerificacionSubItemRepository;
import com.mdp.fusionapp.database.repository.interventoria.CrearControlRepository;
import com.mdp.fusionapp.database.repository.interventoria.CrearEvidenciaInterRepository;
import com.mdp.fusionapp.database.repository.interventoria.CrearInterventoriaRepository;
import com.mdp.fusionapp.database.repository.interventoria.CrearVerificacionRepository;
import com.mdp.fusionapp.network.reposiroty.InterventoriaRespository;
import com.mdp.fusionapp.database.entity.interventoria.ControlSubItemEntity;
import com.mdp.fusionapp.network.response.InterventoriaResponse;
import com.mdp.fusionapp.network.response.Respuesta;
import com.mdp.fusionapp.network.response.SubEstacionResponse;
import com.mdp.fusionapp.database.entity.interventoria.VerificacionSubItemEntity;
import com.mdp.fusionapp.network.response.interventoria.EvidenciaInterResponse;
import com.mdp.fusionapp.network.response.interventoria.InterventoriaDetalleOffineResponse;
import com.mdp.fusionapp.network.response.interventoria.InterventoriaDetalleResponse;

import java.util.HashMap;
import java.util.List;

public class InterventoriaViewModel extends AndroidViewModel {

    private VerificacionSubItemRepository verificacionSubItemRepository;
    private ControlSubItemRepository controlSubItemRepository;

    private CrearInterventoriaRepository crearInterventoriaRepository;
    private CrearVerificacionRepository crearVerificacionRepository;
    private CrearControlRepository crearControlRepository;
    private CrearEvidenciaInterRepository crearEvidenciaInterRepository;
    private NotificarRepository notificarRepository;

    public InterventoriaViewModel(@NonNull Application application) {
        super(application);
        verificacionSubItemRepository = new VerificacionSubItemRepository(application);
        controlSubItemRepository = new ControlSubItemRepository(application);
        crearInterventoriaRepository = new CrearInterventoriaRepository(application);
        crearVerificacionRepository = new CrearVerificacionRepository(application);
        crearControlRepository = new CrearControlRepository(application);
        notificarRepository = new NotificarRepository(application);
        crearEvidenciaInterRepository = new CrearEvidenciaInterRepository(application);
    }

    public MutableLiveData<InterventoriaResponse> listarInterventoria(HashMap hashMap, Context context) {
        return InterventoriaRespository.getInstance().listarInterventoria(hashMap, context);
    }

    public MutableLiveData<List<SubEstacionResponse>> obtenerSubEstacion(Context context) {
        return InterventoriaRespository.getInstance().obtenerSubEstacion(context);
    }

    public MutableLiveData<List<VerificacionSubItemEntity>> obtenerVerificacionSubItem(Context context) {
        return InterventoriaRespository.getInstance().obtenerVerificacionSubItem(context);
    }

    public MutableLiveData<List<ControlSubItemEntity>> obtenerControlSubItem() {
        return InterventoriaRespository.getInstance().obtenerControlSubItem(controlSubItemRepository);
    }

    public List<ControlSubItemEntity> obtenerControlSubItemOffline() {
        return controlSubItemRepository.findAll();
    }

    public MutableLiveData<Respuesta> insertarInterventoria(HashMap hashMap, Context context) {
        return InterventoriaRespository.getInstance().insertarInterventoria(hashMap, context);
    }

    public MutableLiveData<InterventoriaDetalleResponse> interventoriaDetalle(Integer id) {
        return InterventoriaRespository.getInstance().interventoriaDetalle(id);
    }

    public MutableLiveData<Respuesta> editarInterventoria(HashMap hashMap) {
        return InterventoriaRespository.getInstance().editarInterventoria(hashMap);
    }

    public MutableLiveData<Boolean> borrarInterventoria(HashMap hashMap, Context context) {
        return InterventoriaRespository.getInstance().borrarInterventoria(hashMap);
    }

    public MutableLiveData<EvidenciaInterResponse> evidenciaInterventoria(Integer idInteventoria) {
        return InterventoriaRespository.getInstance().evidenciaInterventoria(idInteventoria);
    }

    public MutableLiveData<List<VerificacionSubItemEntity>> listaVerificacionesSubitems() {
        return InterventoriaRespository.getInstance().listaVerificacionesSubitems(verificacionSubItemRepository);
    }

    public MutableLiveData<List<InterventoriaDetalleOffineResponse>> detalleInterventoriaOffline(HashMap hashMap) {
        return InterventoriaRespository.getInstance().detalleInterventoriaOffline(hashMap);
    }

    public List<VerificacionSubItemEntity> listaVerificacionesSubitemsOffline() {
        return verificacionSubItemRepository.findVerificacionSubItem();
    }

    public Long crearInterventoriaOffline(CrearInterventoriaEntiy crearInterventoriaEntiy) {
        return crearInterventoriaRepository.insert(crearInterventoriaEntiy);
    }

    public List<CrearInterventoriaEntiy> findAllInterventoriaOffline() {
        return crearInterventoriaRepository.findAll();
    }

    public CrearInterventoriaEntiy findByIdInterventoriaOffline(Integer idRow) {
        return crearInterventoriaRepository.findById(idRow);
    }

    public List<CrearVerificacionEntity> findVerificacionesByIdOffline(Long idRow) {
        return crearVerificacionRepository.findAll(idRow);
    }

    public List<CrearControlEntity> findControlByIdOffline(Long idRow) {
        return crearControlRepository.findAll(idRow);
    }

    public List<CrearEvidenciaInterEntity> findEvidenciaByIdOffline(Long idRow) {
        return crearEvidenciaInterRepository.findAll(idRow);
    }


    public void crearVerificacionOffline(CrearVerificacionEntity crearInterventoriaEntiy) {
        crearVerificacionRepository.insert(crearInterventoriaEntiy);
    }

    public void crearControlOffline(CrearControlEntity crearControlEntity) {
        crearControlRepository.insert(crearControlEntity);
    }

    public void insertNotificarOffline(NotificarEntity notificarEntity) {
        notificarRepository.insert(notificarEntity);
    }

    public void crearEvidenciaOffline(CrearEvidenciaInterEntity crearEvidenciaInterEntity) {
        crearEvidenciaInterRepository.insert(crearEvidenciaInterEntity);
    }

    public void deleteById(Integer idInterventoria) {
        crearInterventoriaRepository.deleteById(idInterventoria);
    }

    public void updateEstadoEliminar(Integer idInterventoria) {
        crearInterventoriaRepository.updateEstadoEliminar(idInterventoria);
    }
}
