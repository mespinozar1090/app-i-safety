package com.mdp.fusionapp.model;

public class BarrerasModel {

    private Integer idBarrera;
    private String descripcion;
    private String codigoBarrera;
    private Boolean estado;

    public BarrerasModel(Integer idBarrera, String descripcion, String codigoBarrera, Boolean estado) {
        this.idBarrera = idBarrera;
        this.descripcion = descripcion;
        this.codigoBarrera = codigoBarrera;
        this.estado = estado;
    }

    public Integer getIdBarrera() {
        return idBarrera;
    }

    public void setIdBarrera(Integer idBarrera) {
        this.idBarrera = idBarrera;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigoBarrera() {
        return codigoBarrera;
    }

    public void setCodigoBarrera(String codigoBarrera) {
        this.codigoBarrera = codigoBarrera;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return  codigoBarrera+" - "+descripcion;
    }
}
