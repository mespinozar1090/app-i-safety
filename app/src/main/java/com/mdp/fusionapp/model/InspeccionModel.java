package com.mdp.fusionapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class InspeccionModel implements Serializable {

    private Integer idRowInspeccion;
    private Integer idInspeccion;
    private Integer estado;
    private String fechaRegistro;
    private String nombreFormato;
    private String nombreUsuario;
    private String codigo;
    private Integer idUsuario;
    private Integer idEmpresaInspeccionada;
    private Integer idEmpresaInspector;
    private String idUsuarioNotificado;
    private String sede;
    private String empresaContratista;
    private Integer idProyecto;


    public InspeccionModel( Integer idRowInspeccion,Integer idInspeccion, Integer estado, String fechaRegistro, String nombreUsuario, String codigo, Integer idEmpresaInspeccionada, String sede, String empresaContratista, Integer idProyecto) {
        this.idInspeccion = idInspeccion;
        this.idRowInspeccion = idRowInspeccion;
        this.estado = estado;
        this.fechaRegistro = fechaRegistro;
        this.nombreUsuario = nombreUsuario;
        this.codigo = codigo;
        this.idEmpresaInspeccionada = idEmpresaInspeccionada;
        this.sede = sede;
        this.empresaContratista = empresaContratista;
        this.idProyecto = idProyecto;
    }

    public Integer getIdInspeccion() {
        return idInspeccion;
    }

    public void setIdInspeccion(Integer idInspeccion) {
        this.idInspeccion = idInspeccion;
    }

    public String getSede() {
        return sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }

    public String getEmpresaContratista() {
        return empresaContratista;
    }

    public void setEmpresaContratista(String empresaContratista) {
        this.empresaContratista = empresaContratista;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public Integer getIdRowInspeccion() {
        return idRowInspeccion;
    }

    public void setIdRowInspeccion(Integer idRowInspeccion) {
        this.idRowInspeccion = idRowInspeccion;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getNombreFormato() {
        return nombreFormato;
    }

    public void setNombreFormato(String nombreFormato) {
        this.nombreFormato = nombreFormato;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdEmpresaInspeccionada() {
        return idEmpresaInspeccionada;
    }

    public void setIdEmpresaInspeccionada(Integer idEmpresaInspeccionada) {
        this.idEmpresaInspeccionada = idEmpresaInspeccionada;
    }

    public Integer getIdEmpresaInspector() {
        return idEmpresaInspector;
    }

    public void setIdEmpresaInspector(Integer idEmpresaInspector) {
        this.idEmpresaInspector = idEmpresaInspector;
    }

    public String getIdUsuarioNotificado() {
        return idUsuarioNotificado;
    }

    public void setIdUsuarioNotificado(String idUsuarioNotificado) {
        this.idUsuarioNotificado = idUsuarioNotificado;
    }
}
