package com.mdp.fusionapp.model;

import com.google.gson.annotations.SerializedName;

public class NotificacionModel {
    //TODO las varibles no deben iniciar mayusculas, pero el servicio esta un desastre.
    private Integer IdUser;
    private String Nombre;
    private String Email;

    public Integer getIdUser() {
        return IdUser;
    }

    public void setIdUser(Integer idUser) {
        IdUser = idUser;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }
}
