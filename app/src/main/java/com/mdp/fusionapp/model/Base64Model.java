package com.mdp.fusionapp.model;

public class
Base64Model {

    private String imgB64;
    private String fecha;

    public Base64Model(String imgB64, String fecha) {
        this.imgB64 = imgB64;
        this.fecha = fecha;
    }

    public String getImgB64() {
        return imgB64;
    }

    public void setImgB64(String imgB64) {
        this.imgB64 = imgB64;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
