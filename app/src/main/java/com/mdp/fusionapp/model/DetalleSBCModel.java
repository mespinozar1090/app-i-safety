package com.mdp.fusionapp.model;

public class DetalleSBCModel {

    private Integer IdSbcDetalle;
    private Integer IdSbcCategoria;
    private Integer Idbarrera;
    private Integer IdSbcCategoriaItems;
    private Integer IdSbc;
    private String ObservacionSbcDetalle;
    private Boolean Riesgoso;
    private Boolean Seguro;
    private String descripcionItem;

    public DetalleSBCModel(Integer idSbcDetalle, Integer idSbcCategoria, Integer idbarrera, Integer idSbcCategoriaItems, Integer idSbc, String observacionSbcDetalle, Boolean riesgoso, Boolean seguro, String descripcionItems) {
        descripcionItem = descripcionItems;
        IdSbcDetalle = idSbcDetalle;
        IdSbcCategoria = idSbcCategoria;
        Idbarrera = idbarrera;
        IdSbcCategoriaItems = idSbcCategoriaItems;
        IdSbc = idSbc;
        ObservacionSbcDetalle = observacionSbcDetalle;
        Riesgoso = riesgoso;
        Seguro = seguro;

    }

    public String getDescripcionItem() {
        return descripcionItem;
    }

    public void setDescripcionItem(String descripcionItem) {
        this.descripcionItem = descripcionItem;
    }

    public Integer getIdSbcDetalle() {
        return IdSbcDetalle;
    }

    public void setIdSbcDetalle(Integer idSbcDetalle) {
        IdSbcDetalle = idSbcDetalle;
    }

    public Integer getIdSbcCategoria() {
        return IdSbcCategoria;
    }

    public void setIdSbcCategoria(Integer idSbcCategoria) {
        IdSbcCategoria = idSbcCategoria;
    }

    public Integer getIdbarrera() {
        return Idbarrera;
    }

    public void setIdbarrera(Integer idbarrera) {
        Idbarrera = idbarrera;
    }

    public Integer getIdSbcCategoriaItems() {
        return IdSbcCategoriaItems;
    }

    public void setIdSbcCategoriaItems(Integer idSbcCategoriaItems) {
        IdSbcCategoriaItems = idSbcCategoriaItems;
    }

    public Integer getIdSbc() {
        return IdSbc;
    }

    public void setIdSbc(Integer idSbc) {
        IdSbc = idSbc;
    }

    public String getObservacionSbcDetalle() {
        return ObservacionSbcDetalle;
    }

    public void setObservacionSbcDetalle(String observacionSbcDetalle) {
        ObservacionSbcDetalle = observacionSbcDetalle;
    }

    public Boolean getRiesgoso() {
        return Riesgoso;
    }

    public void setRiesgoso(Boolean riesgoso) {
        Riesgoso = riesgoso;
    }

    public Boolean getSeguro() {
        return Seguro;
    }

    public void setSeguro(Boolean seguro) {
        Seguro = seguro;
    }
}
