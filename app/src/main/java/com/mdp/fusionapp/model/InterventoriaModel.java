package com.mdp.fusionapp.model;

public class InterventoriaModel {

    private Integer idInterventoria;
    private Integer estado;
    private String fechaRegistro;
    private String nombreFormato;
    private String nombreUsuario;
    private String codigo;
    private Integer idUsuario;
    private Integer idEmpresaIntervenida;
    private Integer idEmpresaInterventor;
    private String empresaIntervenida;
    private String idUsuarioNotificado;

    public InterventoriaModel(Integer idInterventoria, String nombreUsuario, String codigo) {
        this.idInterventoria = idInterventoria;
        this.nombreUsuario = nombreUsuario;
        this.codigo = codigo;
    }

    public Integer getIdInterventoria() {
        return idInterventoria;
    }

    public void setIdInterventoria(Integer idInterventoria) {
        this.idInterventoria = idInterventoria;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getNombreFormato() {
        return nombreFormato;
    }

    public void setNombreFormato(String nombreFormato) {
        this.nombreFormato = nombreFormato;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdEmpresaIntervenida() {
        return idEmpresaIntervenida;
    }

    public void setIdEmpresaIntervenida(Integer idEmpresaIntervenida) {
        this.idEmpresaIntervenida = idEmpresaIntervenida;
    }

    public Integer getIdEmpresaInterventor() {
        return idEmpresaInterventor;
    }

    public void setIdEmpresaInterventor(Integer idEmpresaInterventor) {
        this.idEmpresaInterventor = idEmpresaInterventor;
    }

    public String getEmpresaIntervenida() {
        return empresaIntervenida;
    }

    public void setEmpresaIntervenida(String empresaIntervenida) {
        this.empresaIntervenida = empresaIntervenida;
    }

    public String getIdUsuarioNotificado() {
        return idUsuarioNotificado;
    }

    public void setIdUsuarioNotificado(String idUsuarioNotificado) {
        this.idUsuarioNotificado = idUsuarioNotificado;
    }
}
