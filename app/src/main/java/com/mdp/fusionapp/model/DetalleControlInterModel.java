package com.mdp.fusionapp.model;

public class DetalleControlInterModel {

    private Double Cantidad;
    private Integer IdControl;
    private Integer IdControlItem;
    private Integer idControlDetalle;

    public DetalleControlInterModel(Double cantidad, Integer idControl, Integer idControlItem,Integer idControlDetalle) {
        this.Cantidad = cantidad;
        this.IdControl = idControl;
        this.IdControlItem = idControlItem;
        this.idControlDetalle =idControlDetalle;
    }

    public Integer getIdControlDetalle() {
        return idControlDetalle;
    }

    public void setIdControlDetalle(Integer idControlDetalle) {
        this.idControlDetalle = idControlDetalle;
    }

    public Double getCantidad() {
        return Cantidad;
    }

    public void setCantidad(Double cantidad) {
        Cantidad = cantidad;
    }

    public Integer getIdControl() {
        return IdControl;
    }

    public void setIdControl(Integer idControl) {
        IdControl = idControl;
    }

    public Integer getIdControlItem() {
        return IdControlItem;
    }

    public void setIdControlItem(Integer idControlItem) {
        IdControlItem = idControlItem;
    }
}
