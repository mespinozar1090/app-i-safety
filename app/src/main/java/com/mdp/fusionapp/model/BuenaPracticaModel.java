package com.mdp.fusionapp.model;

import java.io.Serializable;

public class BuenaPracticaModel implements Serializable {

    private Integer idBuenasPracticas;
    private Integer idCategoriaBuenaPractica;
    private String nombre;
    private String descripcion ="";
    private String imgBase64 = "";
    private Boolean isEditable;
    private Integer isImage = 0;
    private Integer valor = 0;

    public BuenaPracticaModel() {
    }

    public BuenaPracticaModel(Integer idBuenasPracticas,Integer idCategoriaBuenaPractica, String categoriaBuenaPractica, String descripcion, Integer valor, String imgBase64) {
        this.idCategoriaBuenaPractica = idCategoriaBuenaPractica;
        this.nombre = categoriaBuenaPractica;
        this.descripcion = descripcion;
        this.valor = valor;
        this.imgBase64 = imgBase64;
        this.idBuenasPracticas = idBuenasPracticas;
    }

    public Boolean getEditable() {
        return isEditable;
    }

    public void setEditable(Boolean editable) {
        isEditable = editable;
    }

    public Integer getIdBuenasPracticas() {
        return idBuenasPracticas;
    }

    public void setIdBuenasPracticas(Integer idBuenasPracticas) {
        this.idBuenasPracticas = idBuenasPracticas;
    }

    public Integer getIdCategoriaBuenaPractica() {
        return idCategoriaBuenaPractica;
    }

    public void setIdCategoriaBuenaPractica(Integer idCategoriaBuenaPractica) {
        this.idCategoriaBuenaPractica = idCategoriaBuenaPractica;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public String getImgBase64() {
        return imgBase64;
    }

    public void setImgBase64(String imgBase64) {
        this.imgBase64 = imgBase64;
    }

    public Integer getIsImage() {
        return isImage;
    }

    public void setIsImage(Integer isImage) {
        this.isImage = isImage;
    }
}
