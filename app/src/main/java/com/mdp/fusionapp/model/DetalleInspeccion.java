package com.mdp.fusionapp.model;

import java.io.Serializable;
import java.util.List;

public class DetalleInspeccion implements Serializable {


    private Integer idRow;
    private Integer idTipoUbicacion;
    private Integer idProyecto;
    private Integer idLugar;
    private Integer idEmpresaObservadora;
    private Integer idInspecciones;
    private Integer idEmpresaContratista;
    private Integer idUsuario;
    private String responsableAreaInspeccion;
    private Integer tipoInspeccion;
    private String torre;
    private List<BuenaPracticaModel> lstBuenasPracticas;
    private String areaInspeccionada;
    private Integer estadoInspeccion;
    private String responsableInspeccion;
    private String fechaHoraInspeccion;

    public Integer getIdRow() {
        return idRow;
    }

    public void setIdRow(Integer idRow) {
        this.idRow = idRow;
    }

    public String getFechaHoraInspeccion() {
        return fechaHoraInspeccion;
    }

    public void setFechaHoraInspeccion(String fechaHoraInspeccion) {
        this.fechaHoraInspeccion = fechaHoraInspeccion;
    }

    public String getResponsableInspeccion() {
        return responsableInspeccion;
    }

    public void setResponsableInspeccion(String responsableInspeccion) {
        this.responsableInspeccion = responsableInspeccion;
    }

    public Integer getEstadoInspeccion() {
        return estadoInspeccion;
    }

    public void setEstadoInspeccion(Integer estadoInspeccion) {
        this.estadoInspeccion = estadoInspeccion;
    }

    public String getAreaInspeccionada() {
        return areaInspeccionada;
    }

    public void setAreaInspeccionada(String areaInspeccionada) {
        this.areaInspeccionada = areaInspeccionada;
    }

    public Integer getIdTipoUbicacion() {
        return idTipoUbicacion;
    }

    public void setIdTipoUbicacion(Integer idTipoUbicacion) {
        this.idTipoUbicacion = idTipoUbicacion;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public Integer getIdLugar() {
        return idLugar;
    }

    public void setIdLugar(Integer idLugar) {
        this.idLugar = idLugar;
    }

    public Integer getIdEmpresaObservadora() {
        return idEmpresaObservadora;
    }

    public void setIdEmpresaObservadora(Integer idEmpresaObservadora) {
        this.idEmpresaObservadora = idEmpresaObservadora;
    }

    public Integer getIdInspecciones() {
        return idInspecciones;
    }

    public void setIdInspecciones(Integer idInspecciones) {
        this.idInspecciones = idInspecciones;
    }

    public Integer getIdEmpresaContratista() {
        return idEmpresaContratista;
    }

    public void setIdEmpresaContratista(Integer idEmpresaContratista) {
        this.idEmpresaContratista = idEmpresaContratista;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getResponsableAreaInspeccion() {
        return responsableAreaInspeccion;
    }

    public void setResponsableAreaInspeccion(String responsableAreaInspeccion) {
        this.responsableAreaInspeccion = responsableAreaInspeccion;
    }

    public Integer getTipoInspeccion() {
        return tipoInspeccion;
    }

    public void setTipoInspeccion(Integer tipoInspeccion) {
        this.tipoInspeccion = tipoInspeccion;
    }

    public String getTorre() {
        return torre;
    }

    public void setTorre(String torre) {
        this.torre = torre;
    }

    public List<BuenaPracticaModel> getLstBuenasPracticas() {
        return lstBuenasPracticas;
    }

    public void setLstBuenasPracticas(List<BuenaPracticaModel> lstBuenasPracticas) {
        this.lstBuenasPracticas = lstBuenasPracticas;
    }


}
