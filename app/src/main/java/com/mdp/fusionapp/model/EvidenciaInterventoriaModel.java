package com.mdp.fusionapp.model;

import java.io.Serializable;

public class EvidenciaInterventoriaModel implements Serializable {

    private Integer idInterventoriaImagen;
    private String imgBase64;
    private String descripcion;

    public String getImgBase64() {
        return imgBase64;
    }

    public Integer getIdInterventoriaImagen() {
        return idInterventoriaImagen;
    }

    public void setIdInterventoriaImagen(Integer idInterventoriaImagen) {
        this.idInterventoriaImagen = idInterventoriaImagen;
    }

    public void setImgBase64(String imgBase64) {
        this.imgBase64 = imgBase64;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
