package com.mdp.fusionapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.net.Inet4Address;
import java.util.List;

public class HallazgoModel implements Serializable {

    private Integer idRow;
    private Integer id;
    private Integer idInspeccion;
    private String actoSubestandar;
    private Integer riesgo;
    private String resposableAreadetalle;
    private String plazo;
    private Integer idSubFamiliaAmbiental;
    private Integer idActoSubestandar;
    private Integer idTipoGestion;
    private Integer idCondicionSubestandar;
    private String nombreUsuario;
    private Integer idTipoUbicacion;
    private Integer idHallazgo;
    private Integer idTipoHallazgo;
    private String nombreTipoGestion;
    private String nombreTipoHallazgo;
    private String nombreActoSubestandar;
    private Boolean estadoDetalleInspeccion;
    private String accionMitigadora;
    private Integer usuarioRol;

    public Integer getIdRow() {
        return idRow;
    }

    public void setIdRow(Integer idRow) {
        this.idRow = idRow;
    }

    public Integer getIdTipoHallazgo() {
        return idTipoHallazgo;
    }

    public void setIdTipoHallazgo(Integer idTipoHallazgo) {
        this.idTipoHallazgo = idTipoHallazgo;
    }

    public Integer getUsuarioRol() {
        return usuarioRol;
    }

    public void setUsuarioRol(Integer usuarioRol) {
        this.usuarioRol = usuarioRol;
    }

    public Boolean getEstadoDetalleInspeccion() {
        return estadoDetalleInspeccion;
    }

    public void setEstadoDetalleInspeccion(Boolean estadoDetalleInspeccion) {
        this.estadoDetalleInspeccion = estadoDetalleInspeccion;
    }

    private List<EvidenciaFotograficaHallazgo> lstEvidenciaFotoGrafica;
    private List<EnvidenciaCierreHallazgo> lstEvidenciaCierre;

    public HallazgoModel() {
    }

    public String getAccionMitigadora() {
        return accionMitigadora;
    }

    public void setAccionMitigadora(String accionMitigadora) {
        this.accionMitigadora = accionMitigadora;
    }

    public Integer getIdHallazgo() {
        return idHallazgo;
    }

    public void setIdHallazgo(Integer idHallazgo) {
        this.idHallazgo = idHallazgo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdInspeccion() {
        return idInspeccion;
    }

    public void setIdInspeccion(Integer idInspeccion) {
        this.idInspeccion = idInspeccion;
    }

    public String getActoSubestandar() {
        return actoSubestandar;
    }

    public void setActoSubestandar(String actoSubestandar) {
        this.actoSubestandar = actoSubestandar;
    }

    public Integer getRiesgo() {
        return riesgo;
    }

    public void setRiesgo(Integer riesgo) {
        this.riesgo = riesgo;
    }

    public String getResposableAreadetalle() {
        return resposableAreadetalle;
    }

    public void setResposableAreadetalle(String resposableAreadetalle) {
        this.resposableAreadetalle = resposableAreadetalle;
    }

    public String getPlazo() {
        return plazo;
    }

    public void setPlazo(String plazo) {
        this.plazo = plazo;
    }

    public Integer getIdSubFamiliaAmbiental() {
        return idSubFamiliaAmbiental;
    }

    public void setIdSubFamiliaAmbiental(Integer idSubFamiliaAmbiental) {
        this.idSubFamiliaAmbiental = idSubFamiliaAmbiental;
    }

    public Integer getIdActoSubestandar() {
        return idActoSubestandar;
    }

    public void setIdActoSubestandar(Integer idActoSubestandar) {
        this.idActoSubestandar = idActoSubestandar;
    }

    public Integer getIdTipoGestion() {
        return idTipoGestion;
    }

    public void setIdTipoGestion(Integer idTipoGestion) {
        this.idTipoGestion = idTipoGestion;
    }

    public Integer getIdCondicionSubestandar() {
        return idCondicionSubestandar;
    }

    public void setIdCondicionSubestandar(Integer idCondicionSubestandar) {
        this.idCondicionSubestandar = idCondicionSubestandar;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public Integer getIdTipoUbicacion() {
        return idTipoUbicacion;
    }

    public void setIdTipoUbicacion(Integer idTipoUbicacion) {
        this.idTipoUbicacion = idTipoUbicacion;
    }

    public List<EvidenciaFotograficaHallazgo> getLstEvidenciaFotoGrafica() {
        return lstEvidenciaFotoGrafica;
    }

    public void setLstEvidenciaFotoGrafica(List<EvidenciaFotograficaHallazgo> lstEvidenciaFotoGrafica) {
        this.lstEvidenciaFotoGrafica = lstEvidenciaFotoGrafica;
    }

    public List<EnvidenciaCierreHallazgo> getLstEvidenciaCierre() {
        return lstEvidenciaCierre;
    }

    public void setLstEvidenciaCierre(List<EnvidenciaCierreHallazgo> lstEvidenciaCierre) {
        this.lstEvidenciaCierre = lstEvidenciaCierre;
    }

    public String getNombreTipoGestion() {
        return nombreTipoGestion;
    }

    public void setNombreTipoGestion(String nombreTipoGestion) {
        this.nombreTipoGestion = nombreTipoGestion;
    }

    public String getNombreTipoHallazgo() {
        return nombreTipoHallazgo;
    }

    public void setNombreTipoHallazgo(String nombreTipoHallazgo) {
        this.nombreTipoHallazgo = nombreTipoHallazgo;
    }

    public String getNombreActoSubestandar() {
        return nombreActoSubestandar;
    }

    public void setNombreActoSubestandar(String nombreActoSubestandar) {
        this.nombreActoSubestandar = nombreActoSubestandar;
    }
}
