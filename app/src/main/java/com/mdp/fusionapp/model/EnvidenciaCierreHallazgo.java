package com.mdp.fusionapp.model;

import java.io.Serializable;

public class EnvidenciaCierreHallazgo implements Serializable {
    private String imgB64;
    private String fechaFoto;

    public EnvidenciaCierreHallazgo() {
    }

    public EnvidenciaCierreHallazgo(String imgB64, String fechaFoto) {
        this.imgB64 = imgB64;
        this.fechaFoto = fechaFoto;
    }

    public String getFechaFoto() {
        return fechaFoto;
    }

    public void setFechaFoto(String fechaFoto) {
        this.fechaFoto = fechaFoto;
    }

    public String getImgB64() {
        return imgB64;
    }

    public void setImgB64(String imgB64) {
        this.imgB64 = imgB64;
    }
}
