package com.mdp.fusionapp.model;

public class LineamientoModel {

    private Integer id;
    private String description;
    private Boolean estado;
    private Integer tipo;

    public LineamientoModel(Integer id, String description, Boolean estado, Integer tipo) {
        this.id = id;
        this.description = description;
        this.estado = estado;
        this.tipo = tipo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }
}
