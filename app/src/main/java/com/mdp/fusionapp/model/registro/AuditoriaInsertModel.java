package com.mdp.fusionapp.model.registro;

public class AuditoriaInsertModel {

    private Integer estadoAudit;
    private String fechaAudit;
    private Integer idEmpresaContratista;
    private String nroContrato;
    private String responsable;
    private Integer idSedeProyecto;
    private String supervisorContrato;
    private String nombreProyecto;

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public Integer getEstadoAudit() {
        return estadoAudit;
    }

    public void setEstadoAudit(Integer estadoAudit) {
        this.estadoAudit = estadoAudit;
    }

    public String getFechaAudit() {
        return fechaAudit;
    }

    public void setFechaAudit(String fechaAudit) {
        this.fechaAudit = fechaAudit;
    }

    public Integer getIdEmpresaContratista() {
        return idEmpresaContratista;
    }

    public void setIdEmpresaContratista(Integer idEmpresaContratista) {
        this.idEmpresaContratista = idEmpresaContratista;
    }

    public String getNroContrato() {
        return nroContrato;
    }

    public void setNroContrato(String nroContrato) {
        this.nroContrato = nroContrato;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public Integer getIdSedeProyecto() {
        return idSedeProyecto;
    }

    public void setIdSedeProyecto(Integer idSedeProyecto) {
        this.idSedeProyecto = idSedeProyecto;
    }

    public String getSupervisorContrato() {
        return supervisorContrato;
    }

    public void setSupervisorContrato(String supervisorContrato) {
        this.supervisorContrato = supervisorContrato;
    }
}
