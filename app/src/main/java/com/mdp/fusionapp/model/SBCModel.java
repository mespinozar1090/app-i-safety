package com.mdp.fusionapp.model;

public class SBCModel {

    private Integer idSbc;
    private Integer estadoSBC;
    private String fechaRegistro;
    private String nombreFormato;
    private String nombreObservador;
    private String codigoSBC;
    private String observacion;
    private String idUsuario;
    private String idEmpresaObservada;

    public SBCModel(Integer idSbc, String nombreFormato, String nombreObservador, String codigoSBC,String observacion) {
        this.idSbc = idSbc;
        this.nombreFormato = nombreFormato;
        this.nombreObservador = nombreObservador;
        this.codigoSBC = codigoSBC;
        this.observacion = observacion;
    }

    public Integer getIdSbc() {
        return idSbc;
    }

    public void setIdSbc(Integer idSbc) {
        this.idSbc = idSbc;
    }

    public Integer getEstadoSBC() {
        return estadoSBC;
    }

    public void setEstadoSBC(Integer estadoSBC) {
        this.estadoSBC = estadoSBC;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getNombreFormato() {
        return nombreFormato;
    }

    public void setNombreFormato(String nombreFormato) {
        this.nombreFormato = nombreFormato;
    }

    public String getNombreObservador() {
        return nombreObservador;
    }

    public void setNombreObservador(String nombreObservador) {
        this.nombreObservador = nombreObservador;
    }

    public String getCodigoSBC() {
        return codigoSBC;
    }

    public void setCodigoSBC(String codigoSBC) {
        this.codigoSBC = codigoSBC;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getIdEmpresaObservada() {
        return idEmpresaObservada;
    }

    public void setIdEmpresaObservada(String idEmpresaObservada) {
        this.idEmpresaObservada = idEmpresaObservada;
    }
}
