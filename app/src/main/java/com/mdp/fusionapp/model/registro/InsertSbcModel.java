package com.mdp.fusionapp.model.registro;

public class InsertSbcModel {

    private String nombreObservador;
    private Integer idSedeProyecto;
    private String cargoObservador;
    private String lugarTrabajo;
    private String fechaRegistro;
    private Integer idEmpresaObservadora;
    private Integer horarioObservacion;
    private Integer tiempoExpObservada;
    private Integer especialidadObservado;
    private String actividad;
    private Integer idAreaTrabajo;
    private String descripcionAreaObservada;
   // private List<>
    private Integer getIdSedeProyectoObservado;


    public String getNombreObservador() {
        return nombreObservador;
    }

    public void setNombreObservador(String nombreObservador) {
        this.nombreObservador = nombreObservador;
    }

    public Integer getIdSedeProyecto() {
        return idSedeProyecto;
    }

    public void setIdSedeProyecto(Integer idSedeProyecto) {
        this.idSedeProyecto = idSedeProyecto;
    }

    public String getCargoObservador() {
        return cargoObservador;
    }

    public void setCargoObservador(String cargoObservador) {
        this.cargoObservador = cargoObservador;
    }

    public String getLugarTrabajo() {
        return lugarTrabajo;
    }

    public void setLugarTrabajo(String lugarTrabajo) {
        this.lugarTrabajo = lugarTrabajo;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getIdEmpresaObservadora() {
        return idEmpresaObservadora;
    }

    public void setIdEmpresaObservadora(Integer idEmpresaObservadora) {
        this.idEmpresaObservadora = idEmpresaObservadora;
    }

    public Integer getHorarioObservacion() {
        return horarioObservacion;
    }

    public void setHorarioObservacion(Integer horarioObservacion) {
        this.horarioObservacion = horarioObservacion;
    }

    public Integer getTiempoExpObservada() {
        return tiempoExpObservada;
    }

    public void setTiempoExpObservada(Integer tiempoExpObservada) {
        this.tiempoExpObservada = tiempoExpObservada;
    }

    public Integer getEspecialidadObservado() {
        return especialidadObservado;
    }

    public void setEspecialidadObservado(Integer especialidadObservado) {
        this.especialidadObservado = especialidadObservado;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public Integer getIdAreaTrabajo() {
        return idAreaTrabajo;
    }

    public void setIdAreaTrabajo(Integer idAreaTrabajo) {
        this.idAreaTrabajo = idAreaTrabajo;
    }

    public String getDescripcionAreaObservada() {
        return descripcionAreaObservada;
    }

    public void setDescripcionAreaObservada(String descripcionAreaObservada) {
        this.descripcionAreaObservada = descripcionAreaObservada;
    }

    public Integer getIdSedeProyectoObservado() {
        return getIdSedeProyectoObservado;
    }

    public void setIdSedeProyectoObservado(Integer getIdSedeProyectoObservado) {
        this.getIdSedeProyectoObservado = getIdSedeProyectoObservado;
    }

    @Override
    public String toString() {
        return "InsertSbcModel{" +
                "nombreObservador='" + nombreObservador + '\'' +
                ", idSedeProyecto=" + idSedeProyecto +
                ", cargoObservador='" + cargoObservador + '\'' +
                ", lugarTrabajo='" + lugarTrabajo + '\'' +
                ", fechaRegistro='" + fechaRegistro + '\'' +
                ", idEmpresaObservadora=" + idEmpresaObservadora +
                ", horarioObservacion=" + horarioObservacion +
                ", tiempoExpObservada=" + tiempoExpObservada +
                ", especialidadObservado=" + especialidadObservado +
                ", actividad='" + actividad + '\'' +
                ", idAreaTrabajo=" + idAreaTrabajo +
                ", descripcionAreaObservada='" + descripcionAreaObservada + '\'' +
                ", getIdSedeProyectoObservado=" + getIdSedeProyectoObservado +
                '}';
    }
}
