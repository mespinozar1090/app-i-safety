package com.mdp.fusionapp.model;

public class AuditoriaModel {

    private Integer idRow;
    private Integer idAudit;
    private Integer idEmpresacontratista;
    private Integer sede;
    private Integer estadoAudit;
    private String fechaReg;
    private Integer idUsuario;
    private String codigoAudit;
    private String nombreFormato;
    private String idUsuarioNotificado;
    private String nombreAutor;
    private Integer idRolUsuario;
    private Integer idEmpresa;


    public AuditoriaModel(Integer idRows,Integer idAudit, String fechaReg, String codigoAudit, String nombreFormato) {
        this.idRow = idRows;
        this.idAudit = idAudit;
        this.fechaReg = fechaReg;
        this.codigoAudit = codigoAudit;
        this.nombreFormato = nombreFormato;

    }

    public Integer getIdRow() {
        return idRow;
    }

    public void setIdRow(Integer idRow) {
        this.idRow = idRow;
    }

    public Integer getIdAudit() {
        return idAudit;
    }

    public void setIdAudit(Integer idAudit) {
        this.idAudit = idAudit;
    }

    public Integer getIdEmpresacontratista() {
        return idEmpresacontratista;
    }

    public void setIdEmpresacontratista(Integer idEmpresacontratista) {
        this.idEmpresacontratista = idEmpresacontratista;
    }

    public Integer getSede() {
        return sede;
    }

    public void setSede(Integer sede) {
        this.sede = sede;
    }

    public Integer getEstadoAudit() {
        return estadoAudit;
    }

    public void setEstadoAudit(Integer estadoAudit) {
        this.estadoAudit = estadoAudit;
    }

    public String getFechaReg() {
        return fechaReg;
    }

    public void setFechaReg(String fechaReg) {
        this.fechaReg = fechaReg;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getCodigoAudit() {
        return codigoAudit;
    }

    public void setCodigoAudit(String codigoAudit) {
        this.codigoAudit = codigoAudit;
    }

    public String getNombreFormato() {
        return nombreFormato;
    }

    public void setNombreFormato(String nombreFormato) {
        this.nombreFormato = nombreFormato;
    }

    public String getIdUsuarioNotificado() {
        return idUsuarioNotificado;
    }

    public void setIdUsuarioNotificado(String idUsuarioNotificado) {
        this.idUsuarioNotificado = idUsuarioNotificado;
    }

    public String getNombreAutor() {
        return nombreAutor;
    }

    public void setNombreAutor(String nombreAutor) {
        this.nombreAutor = nombreAutor;
    }

    public Integer getIdRolUsuario() {
        return idRolUsuario;
    }

    public void setIdRolUsuario(Integer idRolUsuario) {
        this.idRolUsuario = idRolUsuario;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }
}
