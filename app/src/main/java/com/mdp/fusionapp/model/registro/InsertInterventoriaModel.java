package com.mdp.fusionapp.model.registro;

import com.mdp.fusionapp.model.EvidenciaInterventoriaModel;

import java.util.List;

public class InsertInterventoriaModel {

    private String actividad;
    private String conclucion;
    private Integer empresaIntervenida;
    private Integer empresaInterventor;
    private Boolean estado;
    private String fechaHora;
    private String fechaRegistro;
    private Integer idPlanTrabajo;
    private Integer planTrabajo;
    private Integer idUsuario;
    private String controller;
    private Integer lineaSubEstacion;
    private Integer lugarZona;
    private String nroPlanTrabajo;
    private String supervisor;
    private String supervisorSustituto;
    private Integer tipoUbicacion;
    private List<EvidenciaInterventoriaModel> listEvidencia;

    public InsertInterventoriaModel() {
    }

    public InsertInterventoriaModel(String actividad, String conclucion, Integer empresaIntervenida, Integer empresaInterventor, Boolean estado, String fechaHora, String fechaRegistro, Integer idPlanTrabajo, Integer planTrabajo, Integer idUsuario, String controller, Integer lineaSubEstacion, Integer lugarZona, String nroPlanTrabajo, String supervisor, String supervisorSustituto) {
        this.actividad = actividad;
        this.conclucion = conclucion;
        this.empresaIntervenida = empresaIntervenida;
        this.empresaInterventor = empresaInterventor;
        this.estado = estado;
        this.fechaHora = fechaHora;
        this.fechaRegistro = fechaRegistro;
        this.idPlanTrabajo = idPlanTrabajo;
        this.planTrabajo = planTrabajo;
        this.idUsuario = idUsuario;
        this.controller = controller;
        this.lineaSubEstacion = lineaSubEstacion;
        this.lugarZona = lugarZona;
        this.nroPlanTrabajo = nroPlanTrabajo;
        this.supervisor = supervisor;
        this.supervisorSustituto = supervisorSustituto;
    }

    public List<EvidenciaInterventoriaModel> getListEvidencia() {
        return listEvidencia;
    }

    public void setListEvidencia(List<EvidenciaInterventoriaModel> listEvidencia) {
        this.listEvidencia = listEvidencia;
    }

    public Integer getTipoUbicacion() {
        return tipoUbicacion;
    }

    public void setTipoUbicacion(Integer tipoUbicacion) {
        this.tipoUbicacion = tipoUbicacion;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getConclucion() {
        return conclucion;
    }

    public void setConclucion(String conclucion) {
        this.conclucion = conclucion;
    }

    public Integer getEmpresaIntervenida() {
        return empresaIntervenida;
    }

    public void setEmpresaIntervenida(Integer empresaIntervenida) {
        this.empresaIntervenida = empresaIntervenida;
    }

    public Integer getEmpresaInterventor() {
        return empresaInterventor;
    }

    public void setEmpresaInterventor(Integer empresaInterventor) {
        this.empresaInterventor = empresaInterventor;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getIdPlanTrabajo() {
        return idPlanTrabajo;
    }

    public void setIdPlanTrabajo(Integer idPlanTrabajo) {
        this.idPlanTrabajo = idPlanTrabajo;
    }

    public Integer getPlanTrabajo() {
        return planTrabajo;
    }

    public void setPlanTrabajo(Integer planTrabajo) {
        this.planTrabajo = planTrabajo;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getController() {
        return controller;
    }

    public void setController(String controller) {
        this.controller = controller;
    }

    public Integer getLineaSubEstacion() {
        return lineaSubEstacion;
    }

    public void setLineaSubEstacion(Integer lineaSubEstacion) {
        this.lineaSubEstacion = lineaSubEstacion;
    }

    public Integer getLugarZona() {
        return lugarZona;
    }

    public void setLugarZona(Integer lugarZona) {
        this.lugarZona = lugarZona;
    }

    public String getNroPlanTrabajo() {
        return nroPlanTrabajo;
    }

    public void setNroPlanTrabajo(String nroPlanTrabajo) {
        this.nroPlanTrabajo = nroPlanTrabajo;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    public String getSupervisorSustituto() {
        return supervisorSustituto;
    }

    public void setSupervisorSustituto(String supervisorSustituto) {
        this.supervisorSustituto = supervisorSustituto;
    }

    @Override
    public String toString() {
        return "InsertInterventoriaModel{" +
                "actividad='" + actividad + '\'' +
                ", conclucion='" + conclucion + '\'' +
                ", empresaIntervenida=" + empresaIntervenida +
                ", empresaInterventor=" + empresaInterventor +
                ", estado=" + estado +
                ", fechaHora='" + fechaHora + '\'' +
                ", fechaRegistro='" + fechaRegistro + '\'' +
                ", idPlanTrabajo=" + idPlanTrabajo +
                ", planTrabajo=" + planTrabajo +
                ", idUsuario=" + idUsuario +
                ", controller='" + controller + '\'' +
                ", lineaSubEstacion=" + lineaSubEstacion +
                ", lugarZona=" + lugarZona +
                ", nroPlanTrabajo='" + nroPlanTrabajo + '\'' +
                ", supervisor='" + supervisor + '\'' +
                ", supervisorSustituto='" + supervisorSustituto + '\'' +
                '}';
    }
}
