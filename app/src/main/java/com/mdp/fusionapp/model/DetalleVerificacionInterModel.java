package com.mdp.fusionapp.model;

public class DetalleVerificacionInterModel {

   private Integer Cumple;
   private Integer IdInterventoriaDetalle;
   private Integer IdVerificacionSubitem;
   private Integer idVerificacionItems;
   private Integer IdInterventoria;
   private Integer IdVerificacion;
   private String descripcion;

    public DetalleVerificacionInterModel(Integer cumple, Integer idInterventoriaDetalle, Integer idVerificacionSubitem, Integer idInterventoria, Integer idVerificacion,String descripcionx,Integer idVerificacionItemsx) {
        Cumple = cumple;
        IdInterventoriaDetalle = idInterventoriaDetalle;
        IdVerificacionSubitem = idVerificacionSubitem;
        IdInterventoria = idInterventoria;
        IdVerificacion = idVerificacion;
        descripcion = descripcionx;
        idVerificacionItems = idVerificacionItemsx;
    }

    public Integer getIdVerificacionItems() {
        return idVerificacionItems;
    }

    public void setIdVerificacionItems(Integer idVerificacionItems) {
        this.idVerificacionItems = idVerificacionItems;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getCumple() {
        return Cumple;
    }

    public void setCumple(Integer cumple) {
        Cumple = cumple;
    }

    public Integer getIdInterventoriaDetalle() {
        return IdInterventoriaDetalle;
    }

    public void setIdInterventoriaDetalle(Integer idInterventoriaDetalle) {
        IdInterventoriaDetalle = idInterventoriaDetalle;
    }

    public Integer getIdVerificacionSubitem() {
        return IdVerificacionSubitem;
    }

    public void setIdVerificacionSubitem(Integer idVerificacionSubitem) {
        IdVerificacionSubitem = idVerificacionSubitem;
    }

    public Integer getIdInterventoria() {
        return IdInterventoria;
    }

    public void setIdInterventoria(Integer idInterventoria) {
        IdInterventoria = idInterventoria;
    }

    public Integer getIdVerificacion() {
        return IdVerificacion;
    }

    public void setIdVerificacion(Integer idVerificacion) {
        IdVerificacion = idVerificacion;
    }
}
