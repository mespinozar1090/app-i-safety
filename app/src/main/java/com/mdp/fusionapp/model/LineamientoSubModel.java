package com.mdp.fusionapp.model;

import java.io.Serializable;
import java.util.List;

public class LineamientoSubModel implements Serializable {

    private Integer idAuditLinSubItems;
    private String descripcionItems;
    private Integer idAuditItems;
    private Integer idCalificacion;
    private Integer idLugar;
    private String imgbase64 = "";
    private String mensage;
    private Integer idAuditDetalle;
    private Integer isImage = 0;
    private Integer valor = 0;

    public LineamientoSubModel() {
    }

    public LineamientoSubModel(Integer idAuditLinSubItems, String descripcionItems, Integer idAuditItems) {
        this.idAuditLinSubItems = idAuditLinSubItems;
        this.descripcionItems = descripcionItems;
        this.idAuditItems = idAuditItems;
    }


    public LineamientoSubModel(Integer idAuditLinSubItems, String descripcionItems, Integer idAuditItems, Integer idCalificacion, Integer idLugar, String imgbase64, String mensage) {
        this.idAuditLinSubItems = idAuditLinSubItems;
        this.descripcionItems = descripcionItems;
        this.idAuditItems = idAuditItems;
        this.idCalificacion = idCalificacion;
        this.idLugar = idLugar;
        this.imgbase64 = imgbase64;
        this.mensage = mensage;
    }

    public Integer getIdAuditDetalle() {
        return idAuditDetalle;
    }

    public void setIdAuditDetalle(Integer idAuditDetalle) {
        this.idAuditDetalle = idAuditDetalle;
    }

    public Integer getIdAuditLinSubItems() {
        return idAuditLinSubItems;
    }

    public void setIdAuditLinSubItems(Integer idAuditLinSubItems) {
        this.idAuditLinSubItems = idAuditLinSubItems;
    }

    public String getDescripcionItems() {
        return descripcionItems;
    }

    public void setDescripcionItems(String descripcionItems) {
        this.descripcionItems = descripcionItems;
    }

    public Integer getIdAuditItems() {
        return idAuditItems;
    }

    public void setIdAuditItems(Integer idAuditItems) {
        this.idAuditItems = idAuditItems;
    }

    public Integer getIdCalificacion() {
        return idCalificacion;
    }

    public void setIdCalificacion(Integer idCalificacion) {
        this.idCalificacion = idCalificacion;
    }

    public Integer getIdLugar() {
        return idLugar;
    }

    public void setIdLugar(Integer idLugar) {
        this.idLugar = idLugar;
    }

    public String getImgbase64() {
        return imgbase64;
    }

    public void setImgbase64(String imgbase64) {
        this.imgbase64 = imgbase64;
    }

    public String getMensage() {
        return mensage;
    }

    public void setMensage(String mensage) {
        this.mensage = mensage;
    }

    public Integer getIsImage() {
        return isImage;
    }

    public void setIsImage(Integer isImage) {
        this.isImage = isImage;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }
}
