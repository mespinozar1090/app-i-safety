package com.mdp.fusionapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ElementoObject  implements Serializable {
    @SerializedName("Id_Elemento")
    private Integer idElemento;
    @SerializedName("Descripcion_Elemento")
    private String descripcionElemento;

    public Integer getIdElemento() {
        return idElemento;
    }

    public void setIdElemento(Integer idElemento) {
        this.idElemento = idElemento;
    }

    public String getDescripcionElemento() {
        return descripcionElemento;
    }

    public void setDescripcionElemento(String descripcionElemento) {
        this.descripcionElemento = descripcionElemento;
    }
}
