package com.mdp.fusionapp.model;

public class IncidenteInsertModel {

    private String empleadorTipo;
    private String fDescripcionIncidente;
    private Integer fEmpleadorIdActividadEconomica;
    private Integer fEmpleadorIdEmpresa;
    private Integer fEmpleadorIdTamanioEmpresa;
    private Integer fEmpleadorIIdActividadEconomica;
    private String fEmpleadorIRazonSocial;
    private String fEmpleadorIRuc;
    private String fEmpleadorRazonSocial;
    private String fEmpleadorRuc;
    private String fEventoFechaAccidente;
    private String fEventoFechaInicioInvestigacion;
    private String fEventoHoraAccidente;
    private String fEventoHuboDanioMaterial;
    private Integer fEventoIdEquipoAfectado;
    private Integer fEventoIdParteAfectada;
    private Integer fEventoIdTipoEvento;
    private String fEventoLugarExacto;
    private String fEventoNumTrabajadoresAfectadas;
    private String fTrabajadorDetalleCategoriaOcupacional;
    private String gDescripcionFormato;
    private String fTrabajadorNombresApellidos;
    private Integer gIdProyectoSede;
    private String descripcionSede;

    private Integer pEmpleadorIdEmpresa;
    private Integer fEmpleadorIIdEmpresa;

    public Integer getpEmpleadorIdEmpresa() {
        return pEmpleadorIdEmpresa;
    }

    public void setpEmpleadorIdEmpresa(Integer pEmpleadorIdEmpresa) {
        this.pEmpleadorIdEmpresa = pEmpleadorIdEmpresa;
    }

    public Integer getfEmpleadorIIdEmpresa() {
        return fEmpleadorIIdEmpresa;
    }

    public void setfEmpleadorIIdEmpresa(Integer fEmpleadorIIdEmpresa) {
        this.fEmpleadorIIdEmpresa = fEmpleadorIIdEmpresa;
    }

    public String getDescripcionSede() {
        return descripcionSede;
    }

    public void setDescripcionSede(String descripcionSede) {
        this.descripcionSede = descripcionSede;
    }

    public Integer getgIdProyectoSede() {
        return gIdProyectoSede;
    }

    public void setgIdProyectoSede(Integer gIdProyectoSede) {
        this.gIdProyectoSede = gIdProyectoSede;
    }

    public String getfTrabajadorNombresApellidos() {
        return fTrabajadorNombresApellidos;
    }

    public void setfTrabajadorNombresApellidos(String fTrabajadorNombresApellidos) {
        this.fTrabajadorNombresApellidos = fTrabajadorNombresApellidos;
    }

    public String getEmpleadorTipo() {
        return empleadorTipo;
    }

    public void setEmpleadorTipo(String empleadorTipo) {
        this.empleadorTipo = empleadorTipo;
    }

    public String getfDescripcionIncidente() {
        return fDescripcionIncidente;
    }

    public void setfDescripcionIncidente(String fDescripcionIncidente) {
        this.fDescripcionIncidente = fDescripcionIncidente;
    }

    public Integer getfEmpleadorIdActividadEconomica() {
        return fEmpleadorIdActividadEconomica;
    }

    public void setfEmpleadorIdActividadEconomica(Integer fEmpleadorIdActividadEconomica) {
        this.fEmpleadorIdActividadEconomica = fEmpleadorIdActividadEconomica;
    }

    public Integer getfEmpleadorIdEmpresa() {
        return fEmpleadorIdEmpresa;
    }

    public void setfEmpleadorIdEmpresa(Integer fEmpleadorIdEmpresa) {
        this.fEmpleadorIdEmpresa = fEmpleadorIdEmpresa;
    }

    public Integer getfEmpleadorIdTamanioEmpresa() {
        return fEmpleadorIdTamanioEmpresa;
    }

    public void setfEmpleadorIdTamanioEmpresa(Integer fEmpleadorIdTamanioEmpresa) {
        this.fEmpleadorIdTamanioEmpresa = fEmpleadorIdTamanioEmpresa;
    }

    public Integer getfEmpleadorIIdActividadEconomica() {
        return fEmpleadorIIdActividadEconomica;
    }

    public void setfEmpleadorIIdActividadEconomica(Integer fEmpleadorIIdActividadEconomica) {
        this.fEmpleadorIIdActividadEconomica = fEmpleadorIIdActividadEconomica;
    }

    public String getfEmpleadorIRazonSocial() {
        return fEmpleadorIRazonSocial;
    }

    public void setfEmpleadorIRazonSocial(String fEmpleadorIRazonSocial) {
        this.fEmpleadorIRazonSocial = fEmpleadorIRazonSocial;
    }

    public String getfEmpleadorIRuc() {
        return fEmpleadorIRuc;
    }

    public void setfEmpleadorIRuc(String fEmpleadorIRuc) {
        this.fEmpleadorIRuc = fEmpleadorIRuc;
    }

    public String getfEmpleadorRazonSocial() {
        return fEmpleadorRazonSocial;
    }

    public void setfEmpleadorRazonSocial(String fEmpleadorRazonSocial) {
        this.fEmpleadorRazonSocial = fEmpleadorRazonSocial;
    }

    public String getfEmpleadorRuc() {
        return fEmpleadorRuc;
    }

    public void setfEmpleadorRuc(String fEmpleadorRuc) {
        this.fEmpleadorRuc = fEmpleadorRuc;
    }

    public String getfEventoFechaAccidente() {
        return fEventoFechaAccidente;
    }

    public void setfEventoFechaAccidente(String fEventoFechaAccidente) {
        this.fEventoFechaAccidente = fEventoFechaAccidente;
    }

    public String getfEventoFechaInicioInvestigacion() {
        return fEventoFechaInicioInvestigacion;
    }

    public void setfEventoFechaInicioInvestigacion(String fEventoFechaInicioInvestigacion) {
        this.fEventoFechaInicioInvestigacion = fEventoFechaInicioInvestigacion;
    }

    public String getfEventoHoraAccidente() {
        return fEventoHoraAccidente;
    }

    public void setfEventoHoraAccidente(String fEventoHoraAccidente) {
        this.fEventoHoraAccidente = fEventoHoraAccidente;
    }

    public String getfEventoHuboDanioMaterial() {
        return fEventoHuboDanioMaterial;
    }

    public void setfEventoHuboDanioMaterial(String fEventoHuboDanioMaterial) {
        this.fEventoHuboDanioMaterial = fEventoHuboDanioMaterial;
    }

    public Integer getfEventoIdEquipoAfectado() {
        return fEventoIdEquipoAfectado;
    }

    public void setfEventoIdEquipoAfectado(Integer fEventoIdEquipoAfectado) {
        this.fEventoIdEquipoAfectado = fEventoIdEquipoAfectado;
    }

    public Integer getfEventoIdParteAfectada() {
        return fEventoIdParteAfectada;
    }

    public void setfEventoIdParteAfectada(Integer fEventoIdParteAfectada) {
        this.fEventoIdParteAfectada = fEventoIdParteAfectada;
    }

    public Integer getfEventoIdTipoEvento() {
        return fEventoIdTipoEvento;
    }

    public void setfEventoIdTipoEvento(Integer fEventoIdTipoEvento) {
        this.fEventoIdTipoEvento = fEventoIdTipoEvento;
    }

    public String getfEventoLugarExacto() {
        return fEventoLugarExacto;
    }

    public void setfEventoLugarExacto(String fEventoLugarExacto) {
        this.fEventoLugarExacto = fEventoLugarExacto;
    }

    public String getfEventoNumTrabajadoresAfectadas() {
        return fEventoNumTrabajadoresAfectadas;
    }

    public void setfEventoNumTrabajadoresAfectadas(String fEventoNumTrabajadoresAfectadas) {
        this.fEventoNumTrabajadoresAfectadas = fEventoNumTrabajadoresAfectadas;
    }

    public String getfTrabajadorDetalleCategoriaOcupacional() {
        return fTrabajadorDetalleCategoriaOcupacional;
    }

    public void setfTrabajadorDetalleCategoriaOcupacional(String fTrabajadorDetalleCategoriaOcupacional) {
        this.fTrabajadorDetalleCategoriaOcupacional = fTrabajadorDetalleCategoriaOcupacional;
    }

    public String getgDescripcionFormato() {
        return gDescripcionFormato;
    }

    public void setgDescripcionFormato(String gDescripcionFormato) {
        this.gDescripcionFormato = gDescripcionFormato;
    }
}
