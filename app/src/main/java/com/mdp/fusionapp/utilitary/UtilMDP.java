package com.mdp.fusionapp.utilitary;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.mdp.fusionapp.BuildConfig;
import com.mdp.fusionapp.R;
import com.mdp.fusionapp.model.BarrerasModel;
import com.mdp.fusionapp.model.CalificacionModel;
import com.mdp.fusionapp.model.GestionModel;
import com.mdp.fusionapp.model.ItemModel;
import com.mdp.fusionapp.model.LugarModel;
import com.mdp.fusionapp.model.TipoGestionModel;
import com.mdp.fusionapp.database.entity.interventoria.VerificacionSubItemEntity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UtilMDP {

    /*public static final String BASE_URL = "http://ec2-54-189-152-83.us-west-2.compute.amazonaws.com/api_isafety/";
    public static final String BASE_URL_WEB = "http://ec2-54-189-152-83.us-west-2.compute.amazonaws.com/repienso/";*/


    // public static final String BASE_URL = "http://ec2-54-189-152-83.us-west-2.compute.amazonaws.com/api_isafety2/";
    // public static final String BASE_URL_WEB = "http://ec2-54-189-152-83.us-west-2.compute.amazonaws.com/repienso2/";
    public static final String BASE_URL = "https://extranet.rep.com.pe/WcfFusionRepiensoPDI/"; //produccion
    public static final String BASE_URL_WEB = "https://extranet.rep.com.pe/I-SAfety/"; //produccion

    // public static final String BASE_URL = "http://extranetprb.rep.com.pe/wcffusionrepienso_pdi/";
    // public static final String BASE_URL_WEB = "https://extranetprb.rep.com.pe/fusionrepienso_pdi/";

    public static final String REQUIRED_FIELD = "Campo requerido.";
    public static final String CARACTER_ESPECIAL = "No se permite caracter especial";
    public static final int REQUEST_CAMERA_SEND_ALERT = 1000;

    public static final int PERMISION_CAMERA = 17256;
    public static final String APP_DIRECTORY = "FusionApp";
    public static final String IMAGES_DIRECTORY = "/Media/Images";
    public static final String ID_APP = "APPFUSION";
    public static final String INVALID_DOCUMENT = "Credencial incorrecta.";

    public static String nameImage = "";
    public static String prefijo = "";
    static String pathDCIM = "";
    static File imgFile;
    static Bitmap rotatedBitmap;
    private static ProgressDialog progressDialog;

  /*public static Retrofit getRetrofitBuilder(String url, Context context) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                *//* .readTimeout(60, TimeUnit.SECONDS)
                 .connectTimeout(60, TimeUnit.SECONDS)*//*
                .addInterceptor(new ConnectivityInterceptor(context))
                .addInterceptor(interceptor)
                .build();

        return new Retrofit.Builder()
                .baseUrl(url)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }*/

    public static Retrofit getRetrofitBuilder(String url) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build();

        return new Retrofit.Builder()
                .baseUrl(url)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static void showProgressDialog(Activity activity, String message) {
        View view = activity.getLayoutInflater().inflate(R.layout.dialog_autenticando, null);
        TextView showMessage = (TextView) view.findViewById(R.id.tvAccion);
        showMessage.setText(message);

        progressDialog = getProgressDialog(activity, message);
        progressDialog.show();
        progressDialog.setContentView(view);
        progressDialog.setCancelable(false);
    }

    public static ProgressDialog getProgressDialog(Activity activity, String message) {
        ProgressDialog progressDialog = new ProgressDialog(activity, R.style.screenDialog);
        progressDialog.setMessage(message);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        return progressDialog;
    }

    public static void hideProgreesDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public static String getCustomDate(String format) {
        Date d = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(d);
    }

    public static String getImagesDirectory() {
        return checkDirectory(getDirApp() + IMAGES_DIRECTORY);
    }

    public static String checkDirectory(String directory) {
        File dirDB = new File(directory);
        if (!dirDB.exists()) {
            dirDB.mkdirs();
        }
        return directory;
    }

    public static String getDirApp() {
        return Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + APP_DIRECTORY;
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static List<ItemModel> huboDanio() {
        List<ItemModel> list = new ArrayList<>();
        list.add(new ItemModel(0, "Seleccionar"));
        list.add(new ItemModel(1, "SI"));
        list.add(new ItemModel(2, "NO"));
        return list;
    }

    public final static boolean validarCaracterEspecial(String target) {
        return Pattern.compile(".*[^A-Za-z0-9\\s].*").matcher(target).matches();
    }

    public static List<VerificacionSubItemEntity> lstVerificaciones() {
        List<VerificacionSubItemEntity> list = new ArrayList<>();
        list.add(new VerificacionSubItemEntity(1, "Llenado correcto de Plan de trabajo.", 1, 1));
        list.add(new VerificacionSubItemEntity(2, "Se realizó un pre-ensamble de seguridad.", 1, 1));
        list.add(new VerificacionSubItemEntity(3, "Puntualidad al inicio de la reunión conforme al plan de trabajos.", 2, 1));
        list.add(new VerificacionSubItemEntity(4, "Se realizó la reunion de inicio según MANOMAS.", 2, 1));
        list.add(new VerificacionSubItemEntity(5, "Se realizó la inspección de los EPP, EPC y RT antes de iniciar la actividad.", 2, 1));
        list.add(new VerificacionSubItemEntity(6, "Elaboracion del AST de acuerdo a Listado de Eventos Peligrosos y con la indicación de los controles a implementar.", 2, 1));
        list.add(new VerificacionSubItemEntity(7, "Se cuenta con certificados de calibración vigentes de los equipos de medición a utilizar en la actividad.", 2, 1));
        list.add(new VerificacionSubItemEntity(8, "l Supervisor de trabajos  cuenta con las matrices (IPERC, IEAA's), procedimientos, guía de mantenimiento y/o instructivo de mantenimiento y/o REM.", 2, 1));
        list.add(new VerificacionSubItemEntity(9, "Verificación en el diagrama unifilar que contenga la información de las condenas, demarcaciones y puestas a tierra.", 2, 1));
        list.add(new VerificacionSubItemEntity(10, "Se tienen vigentes las pólizas del SCTR para atención médica.", 2, 1));
        list.add(new VerificacionSubItemEntity(11, "Se dieron a conocer los aspectos relevantes del plan de emergencia (Rutas de evacuación, hospitales cercanos, medios de comunicación y  zonas seguras frente a sismos).", 2, 1));
        list.add(new VerificacionSubItemEntity(12, "Se tiene personal designado para primeros auxilios.", 2, 1));
        list.add(new VerificacionSubItemEntity(13, "El asistente de la subestación estuvo presente durante la reunión de inicio.", 2, 1));
        list.add(new VerificacionSubItemEntity(14, "Se cuenta con los equipos y personal designado para rescate en altura.", 2, 1));
        list.add(new VerificacionSubItemEntity(15, "Solicitó permiso de trabajo.", 2, 1));

        list.add(new VerificacionSubItemEntity(16, "Cumplimiento del ACATESE SO ATESE (Reglas de Oro), en el orden descrito", 3, 2));
        list.add(new VerificacionSubItemEntity(17, "Verificación de Herramientas según el codigo de colores.", 3, 2));
        list.add(new VerificacionSubItemEntity(18, "El personal utiliza, en todo momento, los equipos de protección personal necesario para la actividad.", 4, 2));
        list.add(new VerificacionSubItemEntity(19, "Botiquín de primeros auxilios completo con insumos vigentes.", 5, 2));
        list.add(new VerificacionSubItemEntity(20, "Vehículo de emergencia disponible; cuenta con camilla y extintor operativo.", 5, 2));
        list.add(new VerificacionSubItemEntity(21, "Utilización de escaleras, andamios cumpliendo los estándares de seguridad vigentes.", 6, 2));
        list.add(new VerificacionSubItemEntity(22, "Uso del sistema para detención de caídas (arnés, ganchos de escalamiento, línea de vida, entre otros) en trabajos en altura.", 6, 2));
        list.add(new VerificacionSubItemEntity(23, "Recipientes de productos químicos etiquetados y nominados de acuerdo a hoja de seguridad.", 7, 2));
        list.add(new VerificacionSubItemEntity(24, "Se cuenta con hojas  de seguridad de productos quimicos a utilizar (en español).", 7, 2));
        list.add(new VerificacionSubItemEntity(25, "Los insumos químicos se encuentran sobre bandejas antiderrame y cuentan con su kit contra derrames.", 7, 2));
        list.add(new VerificacionSubItemEntity(26, "El personal almacena sus residuos de acuerdo al código de colores.", 8, 2));
        list.add(new VerificacionSubItemEntity(27, "Al finalizar la actividad, el área de trabajo queda libre de residuos y derrames.", 8, 2));

        list.add(new VerificacionSubItemEntity(28, "Se realizó la reunión de cierre al finalizar la actividad", 9, 3));
        list.add(new VerificacionSubItemEntity(29, "Los trabajos se realizaron con puntualidad, tanto en el tiempo que deben de durar como en la cantidad de actividades programadas.", 9, 3));
        list.add(new VerificacionSubItemEntity(30, "Cumplimiento de las actividades de acuerdo al plan de trabajo.", 9, 3));
        list.add(new VerificacionSubItemEntity(31, "Se toman acuerdos y se registran las observaciones, designando la persona que los actualiza", 9, 3));
        list.add(new VerificacionSubItemEntity(32, "Se registran los pendientes y el responsable de su gestión.", 9, 3));
        list.add(new VerificacionSubItemEntity(33, "Cumplimiento de las medidas de seguridad establecidos en el plan de trabajo.", 9, 3));
        return list;
    }

    public static List<GestionModel> lstGestion() {
        List<GestionModel> list = new ArrayList<>();
        list.add(new GestionModel(0, "Seleccionar"));
        list.add(new GestionModel(1, "SST"));
        list.add(new GestionModel(2, "MA"));
        return list;
    }

    public static List<ItemModel> tipoHallazgo(Integer idGestion) {
        List<ItemModel> list = new ArrayList<>();
        if (idGestion == Constante.SST) {
            list.add(new ItemModel(0, "Seleccionar"));
            list.add(new ItemModel(1, "Accesos"));
            list.add(new ItemModel(2, "Almacenamiento / Apilamiento"));
            list.add(new ItemModel(3, "Competencias del personal"));
            list.add(new ItemModel(4, "Condiciones de Obra - Bienestar"));
            list.add(new ItemModel(5, "EPP"));
            list.add(new ItemModel(6, "Equipos estacionarios"));
            list.add(new ItemModel(7, "Equipos móviles/(Liviano - pesado)"));
            list.add(new ItemModel(8, "Ergonomía"));
            list.add(new ItemModel(9, "Espacios confinados"));
            list.add(new ItemModel(10, "Gestión Documentaria"));
            list.add(new ItemModel(11, "Gestión Vial y Transporte"));
            list.add(new ItemModel(12, "Herramientas manuales y de poder"));
            list.add(new ItemModel(13, "Manipulación de cargas - Izaje"));
            list.add(new ItemModel(14, "Materiales Peligrosos"));
            list.add(new ItemModel(15, "Señalización"));
            list.add(new ItemModel(16, "Sistema de Protección Colectiva"));
            list.add(new ItemModel(17, "Trabajos en altura"));
            list.add(new ItemModel(18, "Trabajos en caliente"));
            list.add(new ItemModel(19, "Trabajos de excavación"));
            list.add(new ItemModel(20, "Trabajos con Energía eléctrica"));
        } else {
            list.add(new ItemModel(0, "Seleccionar"));
            list.add(new ItemModel(21, "Manejo y disposición de residuos"));
            list.add(new ItemModel(22, "Requisitos para manejo o almacenamiento de MATPEL"));
            list.add(new ItemModel(23, "Baños portátiles"));
            list.add(new ItemModel(24, "Presencia de derrames o fugas (líquidos o gases)"));
            list.add(new ItemModel(25, "Almacenamiento de residuos peligrosos"));
            list.add(new ItemModel(26, "Control y mitigación de polvo"));
            list.add(new ItemModel(27, "Presencia de escombros o desmontes"));
            list.add(new ItemModel(28, "Excavaciones o desbroce"));
            list.add(new ItemModel(29, "Revegetación"));
            list.add(new ItemModel(30, "Control de ruido"));
            list.add(new ItemModel(31, "Fugas de gases (aire acondicionado, SF6)"));
            list.add(new ItemModel(32, "Controles para la colisión de aves"));
            list.add(new ItemModel(33, "Generación de efluentes líquidos"));
            list.add(new ItemModel(34, "Uso de recursos hídricos"));
            list.add(new ItemModel(35, "Accesos"));
            list.add(new ItemModel(36, "Uso de energía eléctrica"));
            list.add(new ItemModel(37, "Reciclaje"));
            list.add(new ItemModel(38, "Fauna silvestre en instalaciones"));
        }
        return list;
    }

    public static List<TipoGestionModel> actoSubEstandarSST(Integer idASCS) {
        List<TipoGestionModel> list = new ArrayList<>();
        if (idASCS == Constante.ACTO) {
            list.add(new TipoGestionModel(0, "Seleccionar"));
            list.add(new TipoGestionModel(1, "Operar equipo sin autorización"));
            list.add(new TipoGestionModel(2, "Omisión de advertir"));
            list.add(new TipoGestionModel(3, "Omisión de asegurar"));
            list.add(new TipoGestionModel(4, "Operar a velocidad no permitida"));
            list.add(new TipoGestionModel(5, "Desactivar dispositivos de seguridad"));
            list.add(new TipoGestionModel(6, "Usar equipos defectuosos"));
            list.add(new TipoGestionModel(7, "Usar equipos/herramientas sin inspección"));
            list.add(new TipoGestionModel(8, "No uso de EPP correctamente"));
            list.add(new TipoGestionModel(9, "Carga incorrecta"));
            list.add(new TipoGestionModel(10, "Ubicación incorrecta"));
            list.add(new TipoGestionModel(11, "Levantar incorrectamente"));
            list.add(new TipoGestionModel(12, "Posición inadecuada"));
            list.add(new TipoGestionModel(13, "Dar mantenimiento a equipo en operación"));
            list.add(new TipoGestionModel(14, "Jugueteo"));
            list.add(new TipoGestionModel(15, "Trabajar bajo influencia Alcohol y drogas"));
            list.add(new TipoGestionModel(16, "Uso inapropiado de equipos o herramientas"));
            list.add(new TipoGestionModel(17, "No cumplió procedimiento / estándar establecido"));
            list.add(new TipoGestionModel(18, "Inadecuada inspección preoperativa"));
            list.add(new TipoGestionModel(19, "Incorrecta evaluación del riesgo"));
            list.add(new TipoGestionModel(20, "Operar sin bloquear energía(LoTo, Reglas de oro, ACATESE)"));
            list.add(new TipoGestionModel(21, "Otros actos subestándar"));
        } else {
            list.add(new TipoGestionModel(0, "Seleccionar"));
            list.add(new TipoGestionModel(1, "Guardas o Barreras Inadecuadas"));
            list.add(new TipoGestionModel(2, "Equipo de protección incorrecto o Inadecuado"));
            list.add(new TipoGestionModel(3, "Herramientas, Equipo o Materiales defectuosos o hechizos"));
            list.add(new TipoGestionModel(4, "Congestión o Acción Restringida"));
            list.add(new TipoGestionModel(5, "Sistema de Advertencia Inadecuado"));
            list.add(new TipoGestionModel(6, "Peligros de Incendio y Explosión"));
            list.add(new TipoGestionModel(7, "Orden y Limpieza deficientes / Desorden"));
            list.add(new TipoGestionModel(8, "Exposición al Ruido"));
            list.add(new TipoGestionModel(9, "Exposición a la Radiación"));
            list.add(new TipoGestionModel(10, "Temperaturas Extremas"));
            list.add(new TipoGestionModel(11, "Iluminación Deficiente o Excesiva"));
            list.add(new TipoGestionModel(12, "Ventilación Inadecuada"));
            list.add(new TipoGestionModel(13, "Condiciones Ambientales Peligrosas"));
            list.add(new TipoGestionModel(14, "Accesos inadecuados"));
            list.add(new TipoGestionModel(15, "Escaleras portátiles o rampas sub estándares"));
            list.add(new TipoGestionModel(16, "Andamios y plataformas sub estándares"));
            list.add(new TipoGestionModel(17, "Instalaciones eléctricas en mal estado, sin protección"));
            list.add(new TipoGestionModel(18, "Vehículos y maquinaria rodante sub estándares"));
            list.add(new TipoGestionModel(19, "Equipos/herramientas sub estándares o inadecuados/ sin inspeccion"));
            list.add(new TipoGestionModel(20, "Falta de señalización / señalización inadecuada"));
            list.add(new TipoGestionModel(21, "Documentación inexistente/ inadecuada/ inviable"));
            list.add(new TipoGestionModel(22, "Peligros ergonómicos"));
            list.add(new TipoGestionModel(23, "Baños sucios"));
            list.add(new TipoGestionModel(24, "Equipos de emergencia: incompletos, malas condiciones."));
        }
        return list;
    }

    public static List<TipoGestionModel> actoSubEstandarMA(Integer idASCS) {
        List<TipoGestionModel> list = new ArrayList<>();
        if (idASCS == Constante.ACTO) {
            list.add(new TipoGestionModel(0, "Seleccionar"));
            list.add(new TipoGestionModel(22, "Inadecuada segregación de residuos"));
            list.add(new TipoGestionModel(23, "No completa el reporte mensual interno de cantidad de residuos"));
            list.add(new TipoGestionModel(24, "Almacenamiento de materiales incompatibles"));
            list.add(new TipoGestionModel(25, "Fugas o derrames no reportados o mal reportados"));
            list.add(new TipoGestionModel(26, "Incumplimiento al plan de acción estipulado en el reporte de fuga o derrame"));
            list.add(new TipoGestionModel(27, "Inadecuada segregación de residuos"));
            list.add(new TipoGestionModel(28, "No cuenta con manifiestos de disposición de residuos peligrosos"));
            list.add(new TipoGestionModel(29, "Falta de evidencia de cumplimiento"));
            list.add(new TipoGestionModel(30, "Agua proveniente de fuentes no autorizadas"));
            list.add(new TipoGestionModel(31, "Conductores no respetan la velocidad máxima"));
            list.add(new TipoGestionModel(32, "Desmonte dispuesto en lugares no autorizados"));
            list.add(new TipoGestionModel(33, "Extracción de suelo o material de préstamo sin autorización"));
            list.add(new TipoGestionModel(34, "Efluentes vertidos sin tratamiento"));
            list.add(new TipoGestionModel(35, "Uso de recursos hídricos sin autorización"));
            list.add(new TipoGestionModel(36, "Uso o apertura de accesos sin autorización"));
            list.add(new TipoGestionModel(37, "Equipos conectados fuera de horario laboral"));
            list.add(new TipoGestionModel(38, "Luces encendidas fuera de horario laboral"));
            list.add(new TipoGestionModel(39, "Falta de mecanismos de reciclaje"));
            list.add(new TipoGestionModel(40, "Daño a la fauna silvestres"));
            list.add(new TipoGestionModel(41, "No reportar presencia o interacción con fauna silvestre"));
        } else {
            list.add(new TipoGestionModel(0, "Seleccionar"));
            list.add(new TipoGestionModel(25, "Contenedores en rotos o en mal estado"));
            list.add(new TipoGestionModel(26, "Contenedores repletos (>80% de su capacidad)"));
            list.add(new TipoGestionModel(27, "Rótulos inadecuados o en mal estado"));
            list.add(new TipoGestionModel(28, "Kit antiderrames ausente o incompleto"));
            list.add(new TipoGestionModel(29, "Líquidos sin contención ≥ al 110% de su volumen"));
            list.add(new TipoGestionModel(30, "Falta de impermeabilización"));
            list.add(new TipoGestionModel(31, "Rótulos inadecuados o en mal estado"));
            list.add(new TipoGestionModel(32, "No hay un baño por cada 10 personas"));
            list.add(new TipoGestionModel(33, "Baños sin limpieza periódica"));
            list.add(new TipoGestionModel(34, "Baños con fugas o en mal estado"));
            list.add(new TipoGestionModel(35, "Fugas sin contención"));
            list.add(new TipoGestionModel(36, "Contenedores repletos (>80% de su capacidad)"));
            list.add(new TipoGestionModel(37, "Almacenamiento superior a un año"));
            list.add(new TipoGestionModel(38, "Rótulos inadecuados o en mal estado"));
            list.add(new TipoGestionModel(39, "Zona sin regar, en caso el estudio ambiental lo exija"));
            list.add(new TipoGestionModel(40, "Zona con evidente polución"));
            list.add(new TipoGestionModel(41, "Desmonte sin disponer al final de la etapa constructiva"));
            list.add(new TipoGestionModel(42, "Zona de desmonte sin delimitación y señalización"));
            list.add(new TipoGestionModel(43, "Zona disturbada sin revegetación"));
            list.add(new TipoGestionModel(44, "Actividades de Revegetación sin culminar"));
            list.add(new TipoGestionModel(45, "Ruido superior al ECA"));
            list.add(new TipoGestionModel(46, "Ruido de equipos y maquinarias por falta de mantenimiento"));
            list.add(new TipoGestionModel(47, "Falta de desviadores de vuelo en zonas donde son obligatorias"));
            list.add(new TipoGestionModel(48, "Falta o inadecuado sistema de tratamiento de efluentes"));
            list.add(new TipoGestionModel(49, "Fugas de agua"));
            list.add(new TipoGestionModel(50, "Accesos no cerrados al final su uso, en caso sea obligatorio"));
        }
        return list;
    }


    public static List<BarrerasModel> obtenerBarrera() {
        List<BarrerasModel> list = new ArrayList<>();
        list.add(new BarrerasModel(0, "Seleccionar", "A", true));
        list.add(new BarrerasModel(1, "No lo considera riesgoso", "A", true));
        list.add(new BarrerasModel(2, "Cansancio/Fatiga", "B", true));
        list.add(new BarrerasModel(3, "Distracción", "C", true));
        list.add(new BarrerasModel(4, "Falta de entrenamiento/capacitación", "D", true));
        list.add(new BarrerasModel(5, "Presión del tiempo y/o supervisión", "E", true));
        list.add(new BarrerasModel(6, "No quiere", "E", true));
        list.add(new BarrerasModel(7, "Procedimiento", "F", true));
        list.add(new BarrerasModel(8, "Instrucción recibid", "H", true));
        list.add(new BarrerasModel(9, "No es cómodo", "I", true));
        list.add(new BarrerasModel(10, "Falta de motivación", "J", true));
        list.add(new BarrerasModel(11, "Falta experiencia", "K", true));
        list.add(new BarrerasModel(12, "Falta de control y/o supervisión", "L", true));
        list.add(new BarrerasModel(13, "Condición del equipo/ Instalación", "M", true));
        list.add(new BarrerasModel(14, "Otros (especificar)", "N", true));
        return list;
    }


    public static List<CalificacionModel> calificacionModels() {
        List<CalificacionModel> lista = new ArrayList<>();
        lista.add(new CalificacionModel(1, "CONFORME"));
        lista.add(new CalificacionModel(2, "OBSERVACIÓN"));
        lista.add(new CalificacionModel(3, "NO CONFORME"));
        lista.add(new CalificacionModel(4, "NO APLICA"));
        return lista;
    }

    public static List<LugarModel> lugarModels() {
        List<LugarModel> lista = new ArrayList<>();
        lista.add(new LugarModel(1, "CAMPO"));
        lista.add(new LugarModel(2, "GABINETE"));
        return lista;
    }

    public static Uri getImageFileCompat(Context context) {
        nameImage = prefijo + "_" + UtilMDP.getCustomDate("yyyyMMddHHmmss");
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        try {
            String extension = ".jpg";
            File image = File.createTempFile(nameImage, extension, storageDir);
            nameImage += extension;
            pathDCIM = image.getAbsolutePath();
            return FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", image);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Bitmap onCaptureImageResult(Intent data) {

        File alcomapp = new File(UtilMDP.getImagesDirectory());
        if (!alcomapp.exists()) {
            boolean aa = alcomapp.mkdirs();
        }

        imgFile = new File(pathDCIM);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        Bitmap thumbnail = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);

        Bitmap bitmapx = rotarImagenAndSave(thumbnail);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmapx = Bitmap.createScaledBitmap(bitmapx, 250, 250, false);
        bitmapx.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        File destination = new File(alcomapp, nameImage);

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmapx;
    }

    public static Bitmap rotarImagenAndSave(Bitmap bitmap) {
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(imgFile.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientacion = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        Log.e("Orientacion", " : " + orientacion);
        Matrix matrix = new Matrix();
        switch (orientacion) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(270);
                break;
            default:
        }
        rotatedBitmap = bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return rotatedBitmap;
    }

    public static DisplayMetrics getDeviceMetrics(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        display.getMetrics(metrics);
        return metrics;
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    public static String converteToBase64(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    public static Boolean isOnlineNet() {
        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");
            int val = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public static Boolean isInternetAvailable() {
        try {
            InetAddress address = InetAddress.getByName("www.google.com");
            return !address.equals("");
        } catch (UnknownHostException e) {
            // Log error
        }
        return false;
    }
}
