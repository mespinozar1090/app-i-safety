package com.mdp.fusionapp.utilitary.helper;

import androidx.recyclerview.widget.RecyclerView;

public interface RecycleItemTouchHelperListenerInspeccion {
    void onSwiped(RecyclerView.ViewHolder viewHolder, int direcion, int position);
}
