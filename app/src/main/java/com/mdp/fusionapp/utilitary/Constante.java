package com.mdp.fusionapp.utilitary;

public class Constante {

    public static Integer SST = 1;
    public static Integer MA = 2;
    public static Integer ACTO = 1;
    public static Integer CONDICION = 2;

    public static Integer MODULO_INSPECCION = 1;
    public static Integer MODULO_SBC = 2;
    public static Integer MODULO_AUDITORIA = 3;
    public static Integer MODULO_INTERVENTORIA = 4;
    public static Integer MODULO_INCIDENTE = 5;


    public static Integer ADMINISTRADOR = 1;
    public static Integer SEGURIDAD = 2;
    public static Integer CONTRATISTA = 3;
    public static Integer GENERAL = 4;

    public static Integer ESTADO_DOWNLOAD = 0;
    public static Integer ESTADO_CREADO = 1;
    public static Integer ESTADO_EDITADO = 2;
    public static Integer ESTADO_ELIMINADO = 3;

    public static String OBJECTICO_INSPECION_INTERNA = "Registrar los Actos y Condiciones Subestándar para analizar las acciones a implementar.- Verificar que las actividades a realizar cuenten con los lineamientos de Seguridad, Salud y Medio Ambiente.- Identificar y controlar los factores de riesgos, impactos ambientales o desviaciones que puedan generar accidentes de trabajo, enfermedades profesionales, daños a la propiedad o contaminación al medio ambiente.";
    public static String RESULTADO_INSPECCION = "Durante la inspección se logro involucrar a la linea de mando y/o responsable del area inspeccionada en los hallazgos detectados.- Fomentar el auto cuidado y mejorar las condiciones en el proyecto.- La línea de mando y / o responsable del La línea de mando y / o responsable del área inspeccionada enriqueció más sus conocimientos respecto al cumplimiento de Estandares y Manual corporativo de  seguridad, salud en el trabajo, medio ambiente, social y calidad para contratistas.";
    public static String CONCLUSIONES_RECOMENTACIONES = "Cumplimiento de los objetivos de la inspeccion de seguridad, salud en el trabajo y medio ambiente.- Concientizar a los colaboradores en la necesidad de mantener una cultura de prevencion de seguridad, salud en el trabajo y medio ambiente.";
}
