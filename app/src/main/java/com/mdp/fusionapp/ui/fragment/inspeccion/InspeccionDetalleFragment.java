package com.mdp.fusionapp.ui.fragment.inspeccion;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.CrearBuenaPracticaEntity;
import com.mdp.fusionapp.database.entity.CrearHallazgoEntity;
import com.mdp.fusionapp.database.entity.CrearInspeccionEntity;
import com.mdp.fusionapp.model.BuenaPracticaModel;
import com.mdp.fusionapp.model.DetalleInspeccion;
import com.mdp.fusionapp.model.EnvidenciaCierreHallazgo;
import com.mdp.fusionapp.model.EvidenciaFotograficaHallazgo;
import com.mdp.fusionapp.model.HallazgoModel;
import com.mdp.fusionapp.model.InspeccionModel;
import com.mdp.fusionapp.network.response.ContratistaResponse;
import com.mdp.fusionapp.network.response.InspeccionDetalleResponse;
import com.mdp.fusionapp.network.response.ProyectoResponse;
import com.mdp.fusionapp.ui.activity.inspecciones.CrearHallazgoActivity;
import com.mdp.fusionapp.ui.activity.inspecciones.CrearInspeccionActivity;
import com.mdp.fusionapp.ui.activity.inspecciones.InspecNotificacionActivity;
import com.mdp.fusionapp.ui.activity.inspecciones.VisorPDFActivity;
import com.mdp.fusionapp.ui.adapter.HallazgoAdapter;
import com.mdp.fusionapp.utilitary.NetworkUtil;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.viewModel.HallazgoViewModel;
import com.mdp.fusionapp.viewModel.InspeccionViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class InspeccionDetalleFragment extends Fragment implements View.OnClickListener {

    private static final String ARG_OBJECT = "object";

    private InspeccionViewModel inspeccionViewModel;
    private HallazgoViewModel hallazgoViewModel;

    private Long idRowInspeccion;
    private LinearLayoutManager linearLayout;
    private RecyclerView recyclerView;
    private HallazgoAdapter adaptador;
    private List<HallazgoModel> lista;
    private Button idPDF, btnDetalle;
    private Integer idInspeccion;
    private String codigo;
    private String usuario;
    private TextView txtProyecto, txtUsuario, txtSede;
    private TextView txtEmpresaInspecionada;
    private String responsableArea;
    private InspeccionModel inspeccionModel;
    private Integer estadoInspeccion;
    //Detalle Inspecccion
    DetalleInspeccion detalleInspeccion;
    HallazgoModel hallazgoModel;
    List<EvidenciaFotograficaHallazgo> evidenciaFotograficaHallazgoList = null;
    List<EnvidenciaCierreHallazgo> envidenciaCierreHallazgos = null;

    public static InspeccionDetalleFragment newInstance(InspeccionModel object) {
        InspeccionDetalleFragment fragment = new InspeccionDetalleFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_OBJECT, object);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            inspeccionModel = (InspeccionModel) getArguments().getSerializable(ARG_OBJECT);
            if (null != inspeccionModel.getIdRowInspeccion()) {
                idRowInspeccion = Long.valueOf(inspeccionModel.getIdRowInspeccion());
            }
            idInspeccion = inspeccionModel.getIdInspeccion();
            codigo = inspeccionModel.getCodigo();
            usuario = inspeccionModel.getNombreUsuario();
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.inspeccion_detalle_fragment, container, false);
        inspeccionViewModel = new ViewModelProvider(this).get(InspeccionViewModel.class);
        hallazgoViewModel = new ViewModelProvider(this).get(HallazgoViewModel.class);
        setToolbar(view);
        setupView(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        obtenerHallazgos();
    }

    private void obtenerHallazgos() {
        lista.clear();
        UtilMDP.showProgressDialog(getActivity(), "Obtener hallazgos...");
        HashMap<String, Integer> hashMap = new HashMap<>();
        hashMap.put("Id_Inspecciones", idInspeccion);
        List<BuenaPracticaModel> buenaPracticaModels = new ArrayList<>();
        evidenciaFotograficaHallazgoList = new ArrayList<>();
        envidenciaCierreHallazgos = new ArrayList<>();
        if (NetworkUtil.isOnline(getContext())) {

            inspeccionViewModel.detalleInspeccion(hashMap, getContext()).observe(getActivity(), new Observer<InspeccionDetalleResponse>() {
                @Override
                public void onChanged(InspeccionDetalleResponse inspeccionDetalleResponse) {


                    if (null != inspeccionDetalleResponse) {
                        String responsableInspeccion = inspeccionDetalleResponse.getResponsableAreaInspeccion();
                        for (InspeccionDetalleResponse.InspeccionDetalle data : inspeccionDetalleResponse.getLstDetalleInspeccion()) {
                            hallazgoModel = new HallazgoModel();
                            hallazgoModel.setId(data.getIdInspeccionesReporteDetalle());
                            hallazgoModel.setRiesgo(obtenerRiego(data));
                            hallazgoModel.setResposableAreadetalle(data.getResposableAreaDetalle());
                            hallazgoModel.setPlazo(data.getPlazo());
                            hallazgoModel.setIdSubFamiliaAmbiental(data.getIdSubFamiliaAmbiental());
                            hallazgoModel.setActoSubestandar(data.getActoSubestandar());
                            hallazgoModel.setIdActoSubestandar(data.getIdActoSubestandar());
                            hallazgoModel.setIdTipoGestion(data.getTipoRiesgo());
                            hallazgoModel.setIdCondicionSubestandar(data.getIdCondicionSubestandar());
                            hallazgoModel.setNombreUsuario(responsableInspeccion);
                            hallazgoModel.setIdTipoGestion(data.getIdTipoGestion());
                            hallazgoModel.setIdTipoHallazgo(data.getIdHallazgo());
                            hallazgoModel.setNombreActoSubestandar(data.getNombreActoSubestandar());
                            hallazgoModel.setNombreTipoGestion(data.getNombreTipoGestion());
                            hallazgoModel.setNombreTipoHallazgo(data.getNombreTipoHallazgo());

                            hallazgoModel.setEstadoDetalleInspeccion(data.getEstadoDetalleInspeccion()); //no esta en el offline
                            hallazgoModel.setAccionMitigadora(data.getAccionMitigadora());
                            hallazgoModel.setUsuarioRol(data.getUsuarioRol());
                            evidenciaFotograficaHallazgoList.clear();
                            for (int x = 0; x < data.getLstEvidenciaFotoGrafica().size(); x++) {
                                evidenciaFotograficaHallazgoList.add(new EvidenciaFotograficaHallazgo(data.getLstEvidenciaFotoGrafica().get(x).getImgB64(), data.getLstEvidenciaFotoGrafica().get(x).getFechaFoto()));
                            }

                            envidenciaCierreHallazgos.clear();
                            for (int x = 0; x < data.getLstEvidenciaCierre().size(); x++) {
                                envidenciaCierreHallazgos.add(new EnvidenciaCierreHallazgo(data.getLstEvidenciaCierre().get(x).getImgB64(), data.getLstEvidenciaCierre().get(x).getFechaFoto()));
                            }

                            hallazgoModel.setLstEvidenciaCierre(envidenciaCierreHallazgos);
                            hallazgoModel.setLstEvidenciaFotoGrafica(evidenciaFotograficaHallazgoList);
                            lista.add(hallazgoModel);
                        }
                        adaptador.setHallazgo(lista);


                        detalleInspeccion = new DetalleInspeccion();
                        detalleInspeccion.setIdLugar(inspeccionDetalleResponse.getIdLugar());
                        detalleInspeccion.setIdTipoUbicacion(inspeccionDetalleResponse.getIdTipoUbicacion());
                        detalleInspeccion.setIdProyecto(inspeccionDetalleResponse.getIdProyecto());
                        detalleInspeccion.setIdEmpresaObservadora(inspeccionDetalleResponse.getIdEmpresaObservadora());
                        detalleInspeccion.setIdInspecciones(inspeccionDetalleResponse.getIdInspecciones());
                        detalleInspeccion.setIdEmpresaContratista(inspeccionDetalleResponse.getIdEmpresaContratista());
                        detalleInspeccion.setIdUsuario(inspeccionDetalleResponse.getIdUsuario());
                        detalleInspeccion.setResponsableAreaInspeccion(inspeccionDetalleResponse.getResponsableAreaInspeccion());
                        detalleInspeccion.setTipoInspeccion(inspeccionDetalleResponse.getTipoInspeccion());
                        detalleInspeccion.setTorre(inspeccionDetalleResponse.getTorre());
                        detalleInspeccion.setAreaInspeccionada(inspeccionDetalleResponse.getAreaInspeccionada());
                        detalleInspeccion.setEstadoInspeccion(inspeccionDetalleResponse.getEstadoInspeccion());
                        detalleInspeccion.setFechaHoraInspeccion(inspeccionDetalleResponse.getFechaHoraInspeccion());
                        detalleInspeccion.setResponsableInspeccion(inspeccionDetalleResponse.getResponsableInspeccion());
                        estadoInspeccion = inspeccionDetalleResponse.getEstadoInspeccion();
                        responsableArea = inspeccionDetalleResponse.getResponsableAreaInspeccion();


                        BuenaPracticaModel buenaPracticaModel = null;

                        for (InspeccionDetalleResponse.BuenaPractica practica : inspeccionDetalleResponse.getLstBuenasPracticas()) {
                            buenaPracticaModel = new BuenaPracticaModel();
                            buenaPracticaModel.setIdBuenasPracticas(practica.getIdBuenasPracticas());
                            buenaPracticaModel.setIdCategoriaBuenaPractica(practica.getIdCategoriaBuenaPractica());
                            buenaPracticaModel.setDescripcion(practica.getDescripcion());
                            buenaPracticaModel.setImgBase64(practica.getImgB64());
                            buenaPracticaModel.setNombre(practica.getCategoriaBuenaPractica());
                            if (null != practica.getImgB64() && null != practica.getDescripcion()) {
                                buenaPracticaModel.setValor(1);
                            }

                            if (null != practica.getImgB64()) {
                                buenaPracticaModel.setIsImage(1);
                            }
                            buenaPracticaModels.add(buenaPracticaModel);
                        }
                        detalleInspeccion.setLstBuenasPracticas(buenaPracticaModels);

                    }

                }
            });

        } else {
            //TODO OFFLINE
            CrearInspeccionEntity object = inspeccionViewModel.findInspeccionByIdOffline(Long.valueOf(idRowInspeccion).intValue());
            List<CrearHallazgoEntity> crearHallazgoEntities = hallazgoViewModel.findHallazgosByInspeccion(Long.valueOf(object.getId()));

            if (crearHallazgoEntities != null && crearHallazgoEntities.size() > 0) {
                for (CrearHallazgoEntity data : crearHallazgoEntities) {
                    hallazgoModel = new HallazgoModel();
                    hallazgoModel.setIdRow(data.getId());
                    hallazgoModel.setId(data.getId());
                    hallazgoModel.setRiesgo(obtenerRiegoOffline(data));
                    hallazgoModel.setResposableAreadetalle(data.getResposableAreaDetalle());
                    hallazgoModel.setPlazo(data.getPlazaString());
                    hallazgoModel.setIdSubFamiliaAmbiental(0);
                    hallazgoModel.setIdActoSubestandar(data.getIdActoSubEstandar());
                    hallazgoModel.setActoSubestandar(data.getActoSubestandar());
                    hallazgoModel.setIdTipoGestion(data.getIdTipoGestion());
                    hallazgoModel.setIdCondicionSubestandar(data.getIdCondicionSubEstandar());
                    hallazgoModel.setNombreUsuario(object.getNombreUsuario());
                    hallazgoModel.setNombreActoSubestandar(data.getNombreActoSubestandar());
                    hallazgoModel.setNombreTipoGestion(data.getNombreTipoGestion());
                    hallazgoModel.setNombreTipoHallazgo(data.getNombreTipoHallazgo());
                    hallazgoModel.setAccionMitigadora(data.getActionMitigadora());
                    hallazgoModel.setUsuarioRol(data.getIdRolUsuario());
                    hallazgoModel.setIdTipoHallazgo(data.getIdTipoHallazgo());

                    hallazgoModel.setEstadoDetalleInspeccion(data.getEstadoDetalleInspeccion()); //no esta en el offline

                    evidenciaFotograficaHallazgoList.add(new EvidenciaFotograficaHallazgo(data.getEvidenciaFotoImg64(), data.getEvidenciaFotoFecha()));
                    envidenciaCierreHallazgos.add(new EnvidenciaCierreHallazgo(data.getEvidenciaCierreFotoImg64(), data.getEvidenciaCierreFotoFecha()));
                    hallazgoModel.setLstEvidenciaCierre(envidenciaCierreHallazgos);
                    hallazgoModel.setLstEvidenciaFotoGrafica(evidenciaFotograficaHallazgoList);
                    lista.add(hallazgoModel);

                }
                adaptador.setHallazgo(lista);
            }
            detalleInspeccion = new DetalleInspeccion();
            detalleInspeccion.setIdRow(object.getId());
            detalleInspeccion.setIdLugar(object.getIdLugar());
            detalleInspeccion.setIdTipoUbicacion(object.getIdTipoUbicacion());
            detalleInspeccion.setIdProyecto(object.getIdProyecto());
            detalleInspeccion.setIdEmpresaObservadora(object.getIdEmpresaObservadora());
            detalleInspeccion.setIdInspecciones(object.getId());
            detalleInspeccion.setIdEmpresaContratista(object.getIdEmpresacontratista());
            detalleInspeccion.setIdUsuario(object.getIdUsuario());
            detalleInspeccion.setResponsableAreaInspeccion(object.getResponsableAreaInspeccion());
            detalleInspeccion.setTipoInspeccion(object.getTipoInspeccion());
            detalleInspeccion.setTorre(object.getTorre());
            detalleInspeccion.setAreaInspeccionada(object.getAreaInspeccionada());
            detalleInspeccion.setEstadoInspeccion(object.getEstadoInspeccion());
            detalleInspeccion.setResponsableInspeccion(object.getResponsableInspeccion());
            detalleInspeccion.setFechaHoraInspeccion(object.getFechaHoraInspeccion());
            estadoInspeccion = object.getEstadoInspeccion();
            responsableArea = object.getResponsableAreaInspeccion();


            List<CrearBuenaPracticaEntity> buenaPracticaEntityList = inspeccionViewModel.findBuenaPracticaEntityOffline(Long.valueOf(object.getId()));
            if (buenaPracticaEntityList != null && buenaPracticaEntityList.size() > 0) {
                BuenaPracticaModel buenaPracticaModel = null;
                for (CrearBuenaPracticaEntity practica : buenaPracticaEntityList) {
                    buenaPracticaModel = new BuenaPracticaModel();
                    buenaPracticaModel.setIdBuenasPracticas(practica.getIdBuenaPractica());
                    buenaPracticaModel.setIdCategoriaBuenaPractica(practica.getIdCategoriaBuenaPractica());
                    buenaPracticaModel.setDescripcion(practica.getDescripcion());
                    buenaPracticaModel.setImgBase64(practica.getImgB64());
                    buenaPracticaModel.setNombre(practica.getNombreCategoria());
                    if (null != practica.getImgB64() && null != practica.getDescripcion()) {
                        buenaPracticaModel.setValor(1);
                    }

                    if (null != practica.getImgB64()) {
                        buenaPracticaModel.setIsImage(1);
                    }
                    buenaPracticaModels.add(buenaPracticaModel);
                }
            }
            detalleInspeccion.setLstBuenasPracticas(buenaPracticaModels);

        }
        onclickTable();
        UtilMDP.hideProgreesDialog();

    }

    private Integer obtenerRiegoOffline(CrearHallazgoEntity riesgo) {
        if (riesgo.getRiesgoA()) {
            return 3;
        }
        if (riesgo.getRiesgoM()) {
            return 2;
        }

        if (riesgo.getRiesgoB()) {
            return 1;
        }
        return 0;
    }


    private Integer obtenerRiego(InspeccionDetalleResponse.InspeccionDetalle riesgo) {
        if (riesgo.getRiesgoA()) {
            return 3;
        }
        if (riesgo.getRiesgoM()) {
            return 2;
        }

        if (riesgo.getRiesgoB()) {
            return 1;
        }
        return 0;
    }

    private void setupView(View view) {
        txtSede = view.findViewById(R.id.txtSede);
        txtEmpresaInspecionada = view.findViewById(R.id.txtEmpresaInspecionada);
        txtProyecto = view.findViewById(R.id.txtProyecto);
        txtUsuario = view.findViewById(R.id.txtUsuario);
        btnDetalle = view.findViewById(R.id.btnDetalle);
        idPDF = view.findViewById(R.id.idPDF);
        recyclerView = view.findViewById(R.id.recyclerview);
        linearLayout = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayout);
        lista = new ArrayList<>();
        adaptador = new HallazgoAdapter(getContext(), lista);
        recyclerView.setAdapter(adaptador);

        idPDF.setOnClickListener(this);
        btnDetalle.setOnClickListener(this);
        txtProyecto.setText(codigo);
        txtUsuario.setText(usuario);

        txtEmpresaInspecionada.setText(inspeccionModel.getEmpresaContratista());
        txtSede.setText(inspeccionModel.getSede());
    }

    private void onclickTable() {
        adaptador.setOnItemClickListener((int positionL, View vL) -> {
            if (vL.getId() == R.id.cardRoot) {
                Intent intent = new Intent(getActivity(), CrearHallazgoActivity.class);
                intent.putExtra("hallazgoDetalle", adaptador.getHallazgo(positionL));
                intent.putExtra("idInspeccion", idInspeccion);
                intent.putExtra("estadoInspeccion", estadoInspeccion);
                intent.putExtra("idRowInspeccion", idRowInspeccion);
                startActivity(intent);
            }
        });
    }

    private void setToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        final ActionBar ab = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_back, null);
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorBlanco));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_detalle_inspec, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.send_email:
                Intent intentx = new Intent(getActivity(), InspecNotificacionActivity.class);
                intentx.putExtra("codigoInspeccion", codigo);
                intentx.putExtra("nombreproyecto", inspeccionModel.getSede());
                intentx.putExtra("idInspeccion", idInspeccion);
                if (null != idRowInspeccion) {
                    intentx.putExtra("idRowInspeccion", idRowInspeccion);
                }
                startActivity(intentx);
                break;
            case R.id.action_add:
                Intent intent = new Intent(getActivity(), CrearHallazgoActivity.class);
                intent.putExtra("idInspeccion", idInspeccion);
                intent.putExtra("responsableArea", responsableArea);
                if (null != idRowInspeccion) {
                    intent.putExtra("idRowInspeccion", idRowInspeccion);
                }
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.idPDF:
                String urlDocumento = UtilMDP.BASE_URL_WEB + "Inspeccion/VisualizarPDF?idInspeccion=" + idInspeccion;
                Intent intent = new Intent(getActivity(), VisorPDFActivity.class);
                intent.putExtra("urlDocumento", urlDocumento);
                intent.putExtra("nombreDocumento", codigo);
                startActivity(intent);
                break;
            case R.id.btnDetalle:
                Intent intentx = new Intent(getActivity(), CrearInspeccionActivity.class);
                intentx.putExtra("detalleInspeccion", detalleInspeccion);
                startActivity(intentx);
                break;
        }
    }
}
