package com.mdp.fusionapp.ui.fragment.auditoria;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.model.LineamientoModel;
import com.mdp.fusionapp.model.LineamientoSubModel;
import com.mdp.fusionapp.database.entity.LineamientoEntity;
import com.mdp.fusionapp.database.entity.LineamientoSubEntity;
import com.mdp.fusionapp.network.response.detalle.DetalleAuditoriaResponse;
import com.mdp.fusionapp.database.entity.UsuarioNotificarEntity;
import com.mdp.fusionapp.ui.activity.NotificacionActivity;
import com.mdp.fusionapp.ui.activity.auditoria.AuditoriaActivity;
import com.mdp.fusionapp.ui.activity.auditoria.LineamientoSubItemActivity;
import com.mdp.fusionapp.ui.adapter.auditoria.LineamientoAdapter;
import com.mdp.fusionapp.utilitary.NetworkUtil;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.viewModel.ListaAuditoriaViewModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class EvaluacionesFragment extends Fragment {

    private static final int NOTIFICACION_ACTIVITY_REQUEST_CODE = 999;

    private ListaAuditoriaViewModel mViewModel;
    private LinearLayoutManager linearLayout;
    private RecyclerView recyclerView;
    private LineamientoAdapter adaptador;
    private List<LineamientoModel> lista;
    private List<LineamientoSubModel> listaData;
    private List<LineamientoSubModel> listaDataAdd;
    private List<LineamientoSubModel> lstLineamientoInsert;
    private DetalleAuditoriaResponse auditoria;
    private List<UsuarioNotificarEntity> lstNotificacion;
    private List<DetalleAuditoriaResponse.DetalleAuditoria> detalleAuditorias;
    private Button btnGuardar;
    private static long mLastClickTime = 0;
    List<LineamientoSubModel> lstLineamiento;
    private FragmentManager fragmentManager;
    private SharedPreferences sharedPreferences;
    private static final int SECOND_ACTIVITY_REQUEST_CODE = 0;

    public static EvaluacionesFragment newInstance(DetalleAuditoriaResponse detalle) {
        EvaluacionesFragment fragment = new EvaluacionesFragment();
        Bundle args = new Bundle();
        args.putSerializable("detalle", detalle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            auditoria = (DetalleAuditoriaResponse) getArguments().getSerializable("detalle");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.evaluaciones_fragment, container, false);
        mViewModel = new ViewModelProvider(this).get(ListaAuditoriaViewModel.class);
        initView(view);
        return view;
    }

    private void initView(View view) {
        sharedPreferences = getActivity().getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        btnGuardar = view.findViewById(R.id.btnGuardar);
        recyclerView = view.findViewById(R.id.recyclerview);
        linearLayout = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayout);
        lista = new ArrayList<>();
        listaData = new ArrayList<>();
        lstLineamiento = new ArrayList<>();
        lstNotificacion = new ArrayList<>();
        lstLineamientoInsert = new ArrayList<>();
        detalleAuditorias = new ArrayList<>();
        listaDataAdd = new ArrayList<>();
        adaptador = new LineamientoAdapter(getContext(), lista);
        recyclerView.setAdapter(adaptador);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                obtenetItemEvaluacion();
            }
        });
        onclickTable();
    }

    private void obtenetItemEvaluacion() {
        LineamientoSubModel lineamientoSubModel;
        if (null != auditoria) {
            for (int x = 0; x < auditoria.getLstDetalleAudit().size(); x++) {
                lineamientoSubModel = new LineamientoSubModel();
                lineamientoSubModel.setIdAuditDetalle(auditoria.getLstDetalleAudit().get(x).getIdAuditDetalle());
                lineamientoSubModel.setIdAuditItems(auditoria.getLstDetalleAudit().get(x).getIdAuditItems());
                lineamientoSubModel.setIdAuditLinSubItems(auditoria.getLstDetalleAudit().get(x).getIdAuditLinSubItems());
                lineamientoSubModel.setIdCalificacion(auditoria.getLstDetalleAudit().get(x).getCalificacion());
                lineamientoSubModel.setIdLugar(auditoria.getLstDetalleAudit().get(x).getLugar());
                lineamientoSubModel.setMensage(auditoria.getLstDetalleAudit().get(x).getNotas());
                if (null != auditoria.getLstDetalleAudit().get(x).getLstEvidencia()) {
                    lineamientoSubModel.setImgbase64(auditoria.getLstDetalleAudit().get(x).getLstEvidencia().get(0).getImagen());
                } else {
                    lineamientoSubModel.setImgbase64("");
                }
                lstLineamientoInsert.add(lineamientoSubModel);
            }
        }

        int contador = 0;
        for (int x = 0; x < lstNotificacion.size(); x++) {
            if (lstNotificacion.get(x).getSelectEmail()) {
                contador = contador + 1;
            }
        }
        if (contador > 0) {
            ((AuditoriaActivity) getActivity()).inserAuditoria(lstLineamientoInsert, lstNotificacion);
        } else {
            showValidarNotificacion();
        }
    }

    public void showValidarNotificacion() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setTitle("Notificación");
        alertDialogBuilder.setMessage("Debe seleccionar a quien notificar");
        alertDialogBuilder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int arg1) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void onclickTable() {
        adaptador.setOnItemClickListener((int positionL, View vL) -> {
            if (vL.getId() == R.id.rootLayout) {

                Intent intent = new Intent(getActivity(), LineamientoSubItemActivity.class);
                intent.putExtra("idLineamiento", adaptador.getLineamiento(positionL).getId());
                intent.putExtra("auditoria", auditoria);
                intent.putExtra("listaData", (Serializable) lstLineamientoInsert);
                startActivityForResult(intent, SECOND_ACTIVITY_REQUEST_CODE);
            }
        });
    }

    private void loadLineamientoSub(List<LineamientoEntity> lineamientoRespons) {

        if (NetworkUtil.isOnline(getContext())) {
            mViewModel.obtenerLineaminetosSub().observe(getActivity(), new Observer<List<LineamientoSubEntity>>() {
                @Override
                public void onChanged(List<LineamientoSubEntity> lineamientoSubRespons) {
                    if (null != lineamientoSubRespons) {
                        LineamientoSubModel lineamientoSubModel;
                        for (LineamientoEntity object : lineamientoRespons) {
                            for (LineamientoSubEntity data : lineamientoSubRespons) {
                                if (object.getIdAuditItems() == data.getIdAuditItems()) {
                                    lineamientoSubModel = new LineamientoSubModel();
                                    lineamientoSubModel.setIdAuditItems(object.getIdAuditItems());
                                    lineamientoSubModel.setIdAuditLinSubItems(data.getIdAuditLinSubItems());
                                    lineamientoSubModel.setDescripcionItems(data.getDescripcionItems());
                                    lstLineamientoInsert.add(lineamientoSubModel);
                                }
                            }
                        }
                    }
                }
            });
        } else {
            List<LineamientoSubEntity> list = mViewModel.lineamientoSubOffline();
            if (null != list) {
                LineamientoSubModel lineamientoSubModel;
                for (LineamientoEntity object : lineamientoRespons) {
                    for (LineamientoSubEntity data : list) {
                        if (object.getIdAuditItems() == data.getIdAuditItems()) {
                            lineamientoSubModel = new LineamientoSubModel();
                            lineamientoSubModel.setIdAuditItems(object.getIdAuditItems());
                            lineamientoSubModel.setIdAuditLinSubItems(data.getIdAuditLinSubItems());
                            lineamientoSubModel.setDescripcionItems(data.getDescripcionItems());
                            lstLineamientoInsert.add(lineamientoSubModel);
                        }
                    }
                }
            }
        }

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (NetworkUtil.isOnline(getContext())) {
            mViewModel.obtenerLineaminetos().observe(getActivity(), new Observer<List<LineamientoEntity>>() {
                @Override
                public void onChanged(List<LineamientoEntity> lineamientoRespons) {
                    if (null != lineamientoRespons) {
                        for (LineamientoEntity data : lineamientoRespons) {
                            lista.add(new LineamientoModel(data.getIdAuditItems(), data.getDescripcion(), data.getEstado(), data.getTipo()));
                            adaptador.setLista(lista);
                        }
                        loadLineamientoSub(lineamientoRespons);
                    }
                }
            });
        } else {
            List<LineamientoEntity> list = mViewModel.lineamientoOffline();
            if (null != list) {
                for (LineamientoEntity data : list) {
                    lista.add(new LineamientoModel(data.getIdAuditItems(), data.getDescripcion(), data.getEstado(), data.getTipo()));
                    adaptador.setLista(lista);
                }
                loadLineamientoSub(list);
            }
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SECOND_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                assert data != null;
                listaData = (List<LineamientoSubModel>) data.getSerializableExtra("oLista");
                Log.e("listaData", " size " + lista.size());
                if (null != auditoria && auditoria.getIdAudit() != null) {
                    setDataListaAuditoria(listaData);
                } else {
                    setDataLoad(listaData);
                }
            }
        } else if (requestCode == NOTIFICACION_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                lstNotificacion = (List<UsuarioNotificarEntity>) data.getSerializableExtra("lstNotificacion");
            }
        }
    }

    private void setDataLoad(List<LineamientoSubModel> listaData) {
        Log.e("TAG", "setDataLoad: entroo aqui");
        for (int x = 0; x < lstLineamientoInsert.size(); x++) {
            for (int y = 0; y < listaData.size(); y++) {
                if (lstLineamientoInsert.get(x).getIdAuditItems().intValue() == listaData.get(y).getIdAuditItems().intValue()) {
                    if (lstLineamientoInsert.get(x).getIdAuditLinSubItems().intValue() == listaData.get(y).getIdAuditLinSubItems().intValue()) {
                        lstLineamientoInsert.get(x).setIdCalificacion(listaData.get(y).getIdCalificacion());
                        lstLineamientoInsert.get(x).setIdLugar(listaData.get(y).getIdLugar());
                        lstLineamientoInsert.get(x).setMensage(listaData.get(y).getMensage());
                        Log.e("TAG", "upload ");
                        if (!listaData.get(y).getImgbase64().equals("")) {
                            lstLineamientoInsert.get(x).setImgbase64(listaData.get(x).getImgbase64());
                        }
                    }
                }
            }
        }
        Log.e("TAG", "setDataLoad: lstLineamientoInsert siz02e : " + lstLineamientoInsert.size());
    }

    private void setDataListaAuditoria(List<LineamientoSubModel> listaData) {
        for (int x = 0; x < auditoria.getLstDetalleAudit().size(); x++) {
            for (int y = 0; y < listaData.size(); y++) {
                if (auditoria.getLstDetalleAudit().get(x).getIdAuditItems().intValue() == listaData.get(y).getIdAuditItems().intValue()) {
                    if (auditoria.getLstDetalleAudit().get(x).getIdAuditLinSubItems().intValue() == listaData.get(y).getIdAuditLinSubItems().intValue()) {
                        auditoria.getLstDetalleAudit().get(x).setCalificacion(listaData.get(y).getIdCalificacion());
                        auditoria.getLstDetalleAudit().get(x).setLugar(listaData.get(y).getIdLugar());
                        auditoria.getLstDetalleAudit().get(x).setNotas(listaData.get(y).getMensage());
                        if (!listaData.get(y).getImgbase64().equals("")) {
                            if (null != auditoria.getLstDetalleAudit().get(x).getLstEvidencia()) {
                                auditoria.getLstDetalleAudit().get(x).getLstEvidencia().get(0).setImagen(listaData.get(y).getImgbase64());
                            } else {
                                List<DetalleAuditoriaResponse.EnvidenciaAuditoria> lst = new ArrayList<>();
                                lst.add(new DetalleAuditoriaResponse.EnvidenciaAuditoria(listaData.get(y).getImgbase64()));
                                auditoria.getLstDetalleAudit().get(x).setLstEvidencia(lst);
                            }
                        }

                    }
                }
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_email, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.email_add:
                Intent intent = new Intent(getActivity(), NotificacionActivity.class);
                intent.putExtra("lstNotificacion", (Serializable) lstNotificacion);
                startActivityForResult(intent, NOTIFICACION_ACTIVITY_REQUEST_CODE);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
