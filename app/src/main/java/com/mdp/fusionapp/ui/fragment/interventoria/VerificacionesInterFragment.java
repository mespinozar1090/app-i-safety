package com.mdp.fusionapp.ui.fragment.interventoria;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.model.DetalleVerificacionInterModel;
import com.mdp.fusionapp.model.ItemModel;
import com.mdp.fusionapp.database.entity.interventoria.VerificacionSubItemEntity;
import com.mdp.fusionapp.network.response.interventoria.InterventoriaDetalleResponse;
import com.mdp.fusionapp.ui.activity.interventoria.InterventoriaActivity;
import com.mdp.fusionapp.ui.activity.interventoria.VeritifcacionItemActivity;
import com.mdp.fusionapp.ui.adapter.interventoria.VerificacionesInterAdapter;
import com.mdp.fusionapp.utilitary.NetworkUtil;
import com.mdp.fusionapp.viewModel.InterventoriaViewModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;


public class VerificacionesInterFragment extends Fragment implements View.OnClickListener {

    // CONEXION A SERIVICO WEB
    private InterventoriaViewModel interventoriaViewModel;
    private InterventoriaDetalleResponse interventoria;
    List<InterventoriaDetalleResponse.VerificacionDetalle> lstDetalle;

    // VARIABLE DE FUNCIONALIDAD
    private LinearLayoutManager linearLayout;
    private RecyclerView recyclerView;
    private Button btnSiguiente;
    private VerificacionesInterAdapter adaptador;
    private List<ItemModel> lista;
    private FragmentManager fragmentManager;
    private List<VerificacionSubItemEntity> verificacionSubItemEntityList;
    private List<VerificacionSubItemEntity> listaItemData;
    private List<DetalleVerificacionInterModel> listaItemInsert;

    private static final int SECOND_ACTIVITY_REQUEST_CODE = 0;

    public static VerificacionesInterFragment newInstance(InterventoriaDetalleResponse detalle) {
        VerificacionesInterFragment fragment = new VerificacionesInterFragment();
        Bundle args = new Bundle();
        args.putSerializable("detalle", detalle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            interventoria = (InterventoriaDetalleResponse) getArguments().getSerializable("detalle");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_verificaciones_inter, container, false);
        initView(view);
        loadVerificaciones();
        return view;
    }

    private void initView(View view) {
        interventoriaViewModel = new ViewModelProvider(this).get(InterventoriaViewModel.class);
        recyclerView = view.findViewById(R.id.recyclerview);
        btnSiguiente = view.findViewById(R.id.btnSiguiente);
        btnSiguiente.setOnClickListener(this);
        linearLayout = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayout);
        lista = new ArrayList<>();
        listaItemData = new ArrayList<>();
        listaItemInsert = new ArrayList<>();
        verificacionSubItemEntityList = new ArrayList<>();
        adaptador = new VerificacionesInterAdapter(getContext(), lista);
        recyclerView.setAdapter(adaptador);

        loadDetalle();
    }

    private void loadDetalle() {
        if (validarObjectoDetalle()) {
            lstDetalle = new ArrayList<>();
            lstDetalle = interventoria.getData().getLstVerificacionesDet();

            Log.e(TAG, "loadDetalle: ." + lstDetalle.size());
            VerificacionSubItemEntity object;
            for (int x = 0; x < lstDetalle.size(); x++) {
                object = new VerificacionSubItemEntity();
                object.setIdVerificacion(lstDetalle.get(x).getIdVerificacion());
                Log.e(TAG, "IdVerificacionx: ." + lstDetalle.get(x).getIdVerificacion());
                object.setIdVerificacionItems(lstDetalle.get(x).getIdVerificacionItem());
                object.setDescripcion(lstDetalle.get(x).getDescripcion());
                object.setCumple(lstDetalle.get(x).getCumple());
                object.setIdVerificacionSubitem(lstDetalle.get(x).getIdVerificacionSubitem());
                object.setIdInterventoriaDetalle(lstDetalle.get(x).getIdInterventoriaDetalle());
                verificacionSubItemEntityList.add(object);
            }
        } else {
            loadEvaluacioneSubItem();
        }
    }

    private Boolean validarObjectoDetalle() {
        return interventoria != null ? true : false;
    }

    private void loadEvaluacioneSubItem() {
        if (NetworkUtil.isOnline(getContext())) {
            interventoriaViewModel.listaVerificacionesSubitems().observe(getActivity(), new Observer<List<VerificacionSubItemEntity>>() {
                @Override
                public void onChanged(List<VerificacionSubItemEntity> verificacionSubItemRespons) {
                    if (null != verificacionSubItemRespons) {
                        verificacionSubItemEntityList = verificacionSubItemRespons;
                    }
                }
            });
        } else {
            verificacionSubItemEntityList = interventoriaViewModel.listaVerificacionesSubitemsOffline();
        }
    }

    private void loadVerificaciones() {
        List<ItemModel> itemModels = new ArrayList<>();
        itemModels.add(new ItemModel(1, "PLANIFICACIÓN Y REUNIÓN DE INICIO"));
        itemModels.add(new ItemModel(2, "EJECUCIÓN DE LA ACTIVIDAD"));
        itemModels.add(new ItemModel(3, "REUNIÓN DE CIERRE"));
        adaptador.setLista(itemModels);
        onclickTable();
    }

    private void onclickTable() {
        adaptador.setOnItemClickListener((int positionL, View vL) -> {
            if (vL.getId() == R.id.rootLayout) {
                Intent intent = new Intent(getActivity(), VeritifcacionItemActivity.class);
                intent.putExtra("verificacionSubItem", (Serializable) verificacionSubItemEntityList);
                intent.putExtra("idVerificacion", adaptador.getItemModelo(positionL).getId());
                startActivityForResult(intent, SECOND_ACTIVITY_REQUEST_CODE);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SECOND_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                listaItemData = (List<VerificacionSubItemEntity>) data.getSerializableExtra("oLista");

                setDataLista(listaItemData);
            }
        }
    }

    private void setDataLista(List<VerificacionSubItemEntity> listaItemData) {
        for (int y = 0; y < listaItemData.size(); y++) {
            for (int x = 0; x < verificacionSubItemEntityList.size(); x++) {
                if (verificacionSubItemEntityList.get(x).getIdVerificacionItems().intValue() == listaItemData.get(y).getIdVerificacionItems().intValue()) {
                    if (verificacionSubItemEntityList.get(x).getIdVerificacionSubitem().intValue() == listaItemData.get(y).getIdVerificacionSubitem().intValue()) {
                        verificacionSubItemEntityList.get(x).setCumple(listaItemData.get(y).getCumple());
                        verificacionSubItemEntityList.get(x).setIdVerificacion(listaItemData.get(y).getIdVerificacion());
                        verificacionSubItemEntityList.get(x).setDescripcion(listaItemData.get(y).getDescripcion());
                        verificacionSubItemEntityList.get(x).setIdVerificacionItems(listaItemData.get(y).getIdVerificacionItems());
                    }
                }
            }
        }
    }

/*
    private void obtenerItemVerificaciones(List<VerificacionSubItemResponse> oLista) {
        for (VerificacionSubItemResponse d : oLista) {
            listaItemInsert.add(new DetalleVerificacionInterModel(d.getCumple(), 1, d.getIdVerificacionSubitem(),
                    d.getIdVerificacionItems(), d.getIdVerificacionItems()));
        }
    }*/

    public List<DetalleVerificacionInterModel> listAllVerficaciones() {
        for (VerificacionSubItemEntity d : verificacionSubItemEntityList) {
            listaItemInsert.add(new DetalleVerificacionInterModel(d.getCumple(), d.getIdInterventoriaDetalle(), d.getIdVerificacionSubitem(),
                    d.getIdVerificacionItems(), d.getIdVerificacion(),d.getDescripcion(),d.getIdVerificacionItems()));
        }
        return listaItemInsert;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSiguiente:
                ((InterventoriaActivity) getActivity()).disableScroll(false);
                ((InterventoriaActivity) getActivity()).setViewPager(2);
                break;
        }
    }
}
