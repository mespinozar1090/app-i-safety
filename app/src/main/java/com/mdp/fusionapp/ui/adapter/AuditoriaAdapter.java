package com.mdp.fusionapp.ui.adapter;

import android.content.Context;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.model.AuditoriaModel;

import java.util.ArrayList;
import java.util.List;

public class AuditoriaAdapter extends RecyclerView.Adapter<AuditoriaAdapter.ViewHolder> {

    private Context context;
    private List<AuditoriaModel> lista = new ArrayList<>();
    private static ClickListener clickListener;
    private static long mLastClickTime = 0;
    public AuditoriaAdapter(Context context, List<AuditoriaModel> lista) {
        this.context = context;
        this.lista = lista;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_auditoria, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.bind(lista.get(position));
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public void setAuditoria(List<AuditoriaModel> auditorias){
        this.lista = auditorias;
        notifyDataSetChanged();
    }

    public AuditoriaModel getAuditoria(int position){
        return  lista.get(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView txtFormato, txtCodigo, txtFecha;
        private ConstraintLayout cardRoot;

        public ViewHolder(@NonNull View view) {
            super(view);
            txtCodigo = view.findViewById(R.id.txtCodigo);
            txtFormato = view.findViewById(R.id.txtFormato);
            txtFecha = view.findViewById(R.id.txtFecha);
            cardRoot = view.findViewById(R.id.cardRoot);
        }

        public void bind(AuditoriaModel item) {
            txtCodigo.setText(item.getCodigoAudit());
            txtFormato.setText(item.getNombreFormato());
            txtFecha.setText(item.getFechaReg());
            cardRoot.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            clickListener.onItemClick(getAdapterPosition(),view);
        }
    }

    public interface ClickListener {
        void onItemClick(int postion, View v);
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        AuditoriaAdapter.clickListener = clickListener;
    }
}
