package com.mdp.fusionapp.ui.fragment.interventoria;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.os.SystemClock;
import android.renderscript.Sampler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.interventoria.CrearControlEntity;
import com.mdp.fusionapp.database.entity.interventoria.CrearEvidenciaInterEntity;
import com.mdp.fusionapp.database.entity.interventoria.CrearInterventoriaEntiy;
import com.mdp.fusionapp.database.entity.interventoria.CrearVerificacionEntity;
import com.mdp.fusionapp.model.InterventoriaModel;
import com.mdp.fusionapp.network.response.InterventoriaResponse;
import com.mdp.fusionapp.network.response.detalle.DetalleSbcResponse;
import com.mdp.fusionapp.network.response.interventoria.InterventoriaDetalleResponse;
import com.mdp.fusionapp.ui.activity.interventoria.InterventoriaActivity;
import com.mdp.fusionapp.ui.activity.sbc.CrearsbcActivity;
import com.mdp.fusionapp.ui.adapter.InterventoriaAdapter;
import com.mdp.fusionapp.utilitary.NetworkUtil;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.viewModel.InterventoriaViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class InterventoriaFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private InterventoriaViewModel mViewModel;
    private LinearLayoutManager linearLayout;
    private RecyclerView recyclerView;
    private InterventoriaAdapter adaptador;
    private List<InterventoriaResponse.Data> lista;
    private SwipeRefreshLayout swipeRefreshLayout;
    private SharedPreferences sharedPreferences;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.interventoria_fragment, container, false);
        setupView(view);
        setToolbar(view);
        return view;
    }

    private void setupView(View view) {
        sharedPreferences = getActivity().getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        mViewModel = new ViewModelProvider(this).get(InterventoriaViewModel.class);
        recyclerView = view.findViewById(R.id.recyclerview);
        linearLayout = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayout);
        lista = new ArrayList<>();
        adaptador = new InterventoriaAdapter(getContext(), lista);
        recyclerView.setAdapter(adaptador);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(this);
        onclickTable();
    }

    @Override
    public void onResume() {
        super.onResume();
        listarInterventoria();
    }

    private void listarInterventoria() {
        HashMap<String, Integer> hashMap = new HashMap<>();
        hashMap.put("Id_Usuario", sharedPreferences.getInt("IdUsers", 0));
        hashMap.put("Id_Rol_Usuario", sharedPreferences.getInt("Id_Rol_Usuario", 0));
        hashMap.put("Id_Rol_General", sharedPreferences.getInt("Id_Rol_General", 0));

        if (NetworkUtil.isOnline(getContext())) {
            mViewModel.listarInterventoria(hashMap, getContext()).observe(getActivity(), new Observer<InterventoriaResponse>() {
                @Override
                public void onChanged(InterventoriaResponse interventoriaResponses) {
                    if (null != interventoriaResponses) {
                        Log.e("LOGGER", "onChanged: " + interventoriaResponses.getData().size());
                        adaptador.setInterventoria(interventoriaResponses.getData());
                    }
                }
            });
        } else {
            List<CrearInterventoriaEntiy> lista = mViewModel.findAllInterventoriaOffline();
            List<InterventoriaResponse.Data> listInterventoria = new ArrayList<>();
            InterventoriaResponse.Data crearInterventoriaEntiy = null;
            for (int i = 0; i < lista.size(); i++) {
                crearInterventoriaEntiy = new InterventoriaResponse.Data();
                crearInterventoriaEntiy.setIdRow(lista.get(i).getId());
                crearInterventoriaEntiy.setIdInterventoria(lista.get(i).getIdInterventoria());
                crearInterventoriaEntiy.setCodigo(lista.get(i).getCodigoInterventoria());
                crearInterventoriaEntiy.setNombreFormato(lista.get(i).getNombreFormato());
                crearInterventoriaEntiy.setNombreUsuario(lista.get(i).getInterventor());
                listInterventoria.add(crearInterventoriaEntiy);
            }
            adaptador.setInterventoria(listInterventoria);
        }
       /* txtCodigo.setText(item.getCodigo());
        txtFormato.setText(item.getNombreFormato());
        txtUsuario.setText(item.getNombreUsuario());*/

    }

    private void onclickTable() {
        adaptador.setOnItemClickListener((int positionL, View vL) -> {
            if (vL.getId() == R.id.cardRoot) {
                loadDetalle(adaptador.getInterventoria(positionL).getIdInterventoria(), adaptador.getInterventoria(positionL).getIdRow());
            } else if (vL.getId() == R.id.btnBorrar) {
                showDialogEliminar(adaptador.getInterventoria(positionL).getIdInterventoria(), positionL);
            }
        });
    }

    private void loadDetalle(Integer idInteventoria, Integer idRow) {

        UtilMDP.showProgressDialog(getActivity(), "Obteniendo detalle...");
        if (NetworkUtil.isOnline(getContext())) {
            mViewModel.interventoriaDetalle(idInteventoria).observe(getActivity(), new Observer<InterventoriaDetalleResponse>() {
                @Override
                public void onChanged(InterventoriaDetalleResponse interventoriaDetalleResponse) {
                    UtilMDP.hideProgreesDialog();
                    if (null != interventoriaDetalleResponse) {
                        Intent intent = new Intent(getActivity(), InterventoriaActivity.class);
                        intent.putExtra("interventoria", interventoriaDetalleResponse);
                        startActivity(intent);
                    }
                }
            });
        } else {
            CrearInterventoriaEntiy entity = mViewModel.findByIdInterventoriaOffline(idRow);
            InterventoriaDetalleResponse.InterventoriaDetalle interventoria = new InterventoriaDetalleResponse.InterventoriaDetalle();
            interventoria.setIdInterventoria(entity.getIdInterventoria() != null ? entity.getIdInterventoria() : 0);
            interventoria.setIdUsuario(entity.getIdUsuario());
            interventoria.setEstado(1);
            interventoria.setEmpresaInterventor(entity.getEmpresaInterventor());
            interventoria.setIdEmpresaIntervenida(entity.getIdEmpresaIntervenida());
            interventoria.setFechaRegistro(entity.getFechaRegistro());
            interventoria.setInterventor(entity.getInterventor());
            interventoria.setActividad(entity.getActividad());
            interventoria.setIdPlanTrabajo(entity.getIdPlanTrabajo());
            interventoria.setEmpresaIntervenida(String.valueOf(entity.getEmpresaIntervenida()));
            interventoria.setSupervisor(entity.getSupervisor());
            interventoria.setSupervisorSustituto(entity.getSupervisorSustituto());
            interventoria.setLineaSubestacion(String.valueOf(entity.getLineaSubEstacion()));
            interventoria.setNroPlanTrabajo(entity.getNroPlanTrabajo());
            interventoria.setPlanTrabajo(String.valueOf(entity.getPlanTrabajo()));
            interventoria.setLugarZona(String.valueOf(entity.getLugarZona()));
            interventoria.setTipoUbicacion(String.valueOf(entity.getTipoUbicacion()));
            interventoria.setConcluciones(entity.getConclucion());

            List<CrearVerificacionEntity> verificacion = mViewModel.findVerificacionesByIdOffline(Long.valueOf(entity.getId()));
            List<InterventoriaDetalleResponse.VerificacionDetalle> listVerificacion = new ArrayList<>();
            InterventoriaDetalleResponse.VerificacionDetalle verificacionDetalle = null;
            for (int x = 0; x < verificacion.size(); x++) {
                verificacionDetalle = new InterventoriaDetalleResponse.VerificacionDetalle();
                verificacionDetalle.setIdInterventoriaDetalle(verificacion.get(x).getIdInterventoriaDetalle());
                verificacionDetalle.setCumple(verificacion.get(x).getCumple());
                verificacionDetalle.setIdVerificacionSubitem(verificacion.get(x).getIdVerificacionSubitem());
                verificacionDetalle.setIdVerificacion(verificacion.get(x).getIdVerificacion());
                verificacionDetalle.setIdInterventoria(verificacion.get(x).getIdInterventoria());
                verificacionDetalle.setDescripcion(verificacion.get(x).getDescripcion());
                verificacionDetalle.setIdVerificacionPadre(verificacion.get(x).getIdVerificacionPadre());
                verificacionDetalle.setIdVerificacionItem(verificacion.get(x).getIdVerificacionItem());
                listVerificacion.add(verificacionDetalle);
            }
            interventoria.setLstVerificacionesDet(listVerificacion);

            List<CrearControlEntity> control = mViewModel.findControlByIdOffline(Long.valueOf(entity.getId()));
            List<InterventoriaDetalleResponse.ControlDetalle> listControl = new ArrayList<>();
            InterventoriaDetalleResponse.ControlDetalle controlDetalle = null;
            for (int i = 0; i < control.size(); i++) {
                controlDetalle = new InterventoriaDetalleResponse.ControlDetalle();
                controlDetalle.setIdControlItem(control.get(i).getIdControlItem());
                controlDetalle.setCantidad(control.get(i).getCantidad());
                controlDetalle.setIdControlDetalle(control.get(i).getIdControlDetalle());
                controlDetalle.setIdControl(control.get(i).getIdControl());
                controlDetalle.setIdInterventoria(control.get(i).getIdInteventoria());
                controlDetalle.setDescripcion(control.get(i).getDescripcion());
                listControl.add(controlDetalle);
            }
            interventoria.setLstControlesDet(listControl);

            InterventoriaDetalleResponse objec = new InterventoriaDetalleResponse();
            objec.setData(interventoria);

            Intent intent = new Intent(getActivity(), InterventoriaActivity.class);
            intent.putExtra("interventoria", objec);
            startActivity(intent);
            UtilMDP.hideProgreesDialog();
        }
    }

    private void setToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        final ActionBar ab = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_menu, null);
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorBlanco));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_inspeccion, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                DrawerLayout drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
                drawer.openDrawer(Gravity.LEFT);
                break;
            case R.id.action_add:
                Intent intent = new Intent(getActivity(), InterventoriaActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                listarInterventoria();
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 2000);
    }

    private void showDialogEliminar(Integer idInterventoria, Integer position) {
        View viewAddProject = getActivity().getLayoutInflater().inflate(R.layout.delete_inspeccion, null);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(false);
        alertDialog.setView(viewAddProject);

        TextView textMessage = viewAddProject.findViewById(R.id.textMessage);
        textMessage.setText("Esta seguro de eliminar el registro de Interventoria ?");

        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                //adaptador.notifyDataSetChanged();
            }
        });

        alertDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                eliminarInterventioria(idInterventoria, position);
                dialog.dismiss();
            }
        });

        final AlertDialog dialog = alertDialog.create();
        dialog.show();
    }

    private void eliminarInterventioria(Integer idInterventoria, Integer position) {
        if (NetworkUtil.isOnline(requireContext())) {
            HashMap<String, Integer> hashMap = new HashMap<>();
            hashMap.put("Id_Interventoria", idInterventoria);
            mViewModel.borrarInterventoria(hashMap, getContext()).observe(getActivity(), new Observer<Boolean>() {
                @Override
                public void onChanged(Boolean aBoolean) {
                    if (aBoolean) {
                        adaptador.removeItem(position);
                        Toast.makeText(getActivity(), "Eliminado con éxito!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), "Ocurrio un error", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            if (adaptador.getInterventoria(position).getIdInterventoria() != null &&
                    adaptador.getInterventoria(position).getIdInterventoria() != 0) {
                mViewModel.updateEstadoEliminar(adaptador.getInterventoria(position).getIdRow());
            } else {
                mViewModel.deleteById(adaptador.getInterventoria(position).getIdRow());
            }
            adaptador.removeItem(position);
        }

    }
}
