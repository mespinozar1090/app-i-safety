package com.mdp.fusionapp.ui.activity.auditoria;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.model.LineamientoSubModel;
import com.mdp.fusionapp.network.response.detalle.DetalleAuditoriaResponse;
import com.mdp.fusionapp.ui.adapter.auditoria.LineamientoSubAdapter;
import com.mdp.fusionapp.utilitary.FileUtils;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.viewModel.ListaAuditoriaViewModel;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LineamientoSubItemActivity extends AppCompatActivity implements View.OnClickListener {

    private ListaAuditoriaViewModel mViewModel;
    private LineamientoSubAdapter adaptador;
    private RecyclerView recyclerview;
    private LinearLayoutManager linearLayout;
    private List<LineamientoSubModel> lista;
    public List<LineamientoSubModel> listaData;
    private Button btnGuardar;

    //capturar imagen
    private String encodeImage;
    private static String nameImage = "";
    private static String prefijo = "";
    private static String pathDCIM = "";
    private Integer indextSelect;
    private DetalleAuditoriaResponse auditoria;
    private static final int SELECT_IMAGE = 3;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lineamiento_sub_item);
        setToolbar();
        initView();
        sharedPreferences = getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        int idLineamiento = getIntent().getIntExtra("idLineamiento", 0);
        auditoria = (DetalleAuditoriaResponse) getIntent().getSerializableExtra("auditoria");
        listaData = (List<LineamientoSubModel>) getIntent().getSerializableExtra("listaData");

        if (auditoria != null) {
            loadLineamientoSub(auditoria.getLstDetalleAudit(), idLineamiento);
        } else {
            loadLineamientoSub(idLineamiento);
        }
    }

    private void loadLineamientoSub(List<DetalleAuditoriaResponse.DetalleAuditoria> listObject, Integer idLineamiento) {
        LineamientoSubModel lineamientoSubModel;
        for (int x = 0; x < listObject.size(); x++) {
            lineamientoSubModel = new LineamientoSubModel();
            if (listObject.get(x).getIdAuditItems().intValue() == idLineamiento) {
                lineamientoSubModel.setIdAuditItems(listObject.get(x).getIdAuditItems());
                lineamientoSubModel.setIdLugar(listObject.get(x).getLugar());
                lineamientoSubModel.setIdAuditLinSubItems(listObject.get(x).getIdAuditLinSubItems());
                lineamientoSubModel.setIdAuditDetalle(listObject.get(x).getIdAuditDetalle());
                lineamientoSubModel.setIdLugar(listObject.get(x).getLugar());
                lineamientoSubModel.setDescripcionItems(listObject.get(x).getDescripcionItem());
                lineamientoSubModel.setMensage(listObject.get(x).getNotas());
                lineamientoSubModel.setIdCalificacion(listObject.get(x).getCalificacion());
                lineamientoSubModel.setIdAuditDetalle(listObject.get(x).getIdAuditDetalle());
                if (listObject.get(x).getLstEvidencia() != null && listObject.get(x).getLstEvidencia().size() > 0) {
                    lineamientoSubModel.setImgbase64(listObject.get(x).getLstEvidencia().get(0).getImagen());
                }
                lista.add(lineamientoSubModel);
            }
        }
        adaptador.setLista(lista);
    }

    private void loadLineamientoSub(Integer idLineamiento) {
        LineamientoSubModel lineamientoSubModel;
        for (LineamientoSubModel data : listaData) {
            lineamientoSubModel = new LineamientoSubModel();
            if (data.getIdAuditItems().intValue() == idLineamiento) {
                lineamientoSubModel.setIdAuditItems(data.getIdAuditItems());
                lineamientoSubModel.setIdAuditLinSubItems(data.getIdAuditLinSubItems());
                lineamientoSubModel.setDescripcionItems(data.getDescripcionItems());
                lineamientoSubModel.setIdCalificacion(data.getIdCalificacion());
                lineamientoSubModel.setMensage(data.getMensage());
                lineamientoSubModel.setIdLugar(data.getIdLugar());
                lineamientoSubModel.setIdAuditDetalle(data.getIdAuditDetalle());
                lineamientoSubModel.setImgbase64(data.getImgbase64());
                lista.add(lineamientoSubModel);
            }
        }
        adaptador.setLista(lista);
    }

    private void initView() {
        mViewModel = new ViewModelProvider(this).get(ListaAuditoriaViewModel.class);
        btnGuardar = findViewById(R.id.btnGuardar);
        recyclerview = findViewById(R.id.recyclerview);
        linearLayout = new LinearLayoutManager(getApplicationContext());
        recyclerview.setLayoutManager(linearLayout);
        lista = new ArrayList<>();
        adaptador = new LineamientoSubAdapter(getApplicationContext(), lista);
        recyclerview.setAdapter(adaptador);

        btnGuardar.setOnClickListener(this);
        onclickTable();
    }

    public void utilizarCamara() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Uri mProcessingPhotoUri = UtilMDP.getImageFileCompat(getApplicationContext());
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mProcessingPhotoUri);
        } else {
            nameImage = prefijo + "_" + UtilMDP.getCustomDate("yyyyMMddHHmmss") + ".jpg";
            File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
            pathDCIM = storageDir.getAbsolutePath() + File.separator + nameImage;
            File file = new File(pathDCIM);
            Uri outputFileUri = Uri.fromFile(file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        }
        startActivityForResult(intent, UtilMDP.REQUEST_CAMERA_SEND_ALERT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == UtilMDP.REQUEST_CAMERA_SEND_ALERT) {
            if (resultCode == Activity.RESULT_OK) {
                Bitmap bitmapCapture = UtilMDP.onCaptureImageResult(data);
                setImageCapture(bitmapCapture);
            }
        } else if (requestCode == SELECT_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    if (Build.VERSION.SDK_INT < 28) {
                        File file = FileUtils.getFile(getApplicationContext(), data.getData());
                        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                        setImageCapture(bitmap);
                    } else {
                        //  Uri uri = data.getData();
                        File file = FileUtils.getFile(getApplicationContext(), data.getData());
                        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                        setImageCapture(bitmap);

                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(LineamientoSubItemActivity.this, "Canceled", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void setImageCapture(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Bitmap bitmapCapture = Bitmap.createScaledBitmap(bitmap, 250, 250, true);
        bitmapCapture.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        encodeImage = "data:image/jpeg;base64," + Base64.encodeToString(b, Base64.DEFAULT);
        adaptador.setValorImg(indextSelect, encodeImage);
    }

    private void onclickTable() {
        adaptador.setOnItemClickListener((int positionL, View vL) -> {
            if (vL.getId() == R.id.imgFoto) {
                indextSelect = positionL;
                showCaptureImagen(adaptador.getLineamientoSub(positionL).getImgbase64());
            } else if (vL.getId() == R.id.btnGallery) {
                indextSelect = positionL;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE);
            } else if (vL.getId() == R.id.btnCamera) {
                indextSelect = positionL;
                utilizarCamara();
            }
        });
    }

    private void showCaptureImagen(String imgBase64) {
        Log.e("TAG", "showCaptureImagen: " + imgBase64);
        View viewAddProject = getLayoutInflater().inflate(R.layout.show_foto, null);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(true);
        alertDialog.setView(viewAddProject);

        Button btnClose = viewAddProject.findViewById(R.id.btnClose);
        ImageView imgFoto = viewAddProject.findViewById(R.id.imgFoto);


        if (null != imgBase64 && !imgBase64.equalsIgnoreCase("")) {
            if (null != imgBase64 && imgBase64.contains("base64")) {
                String base64 = imgBase64;
                String[] parts = base64.split("base64,");
                String part1 = parts[0];
                String part2 = parts[1];
                imgFoto.setImageBitmap(UtilMDP.decodeBase64(part2));
            } else {
                Picasso.get().load(imgBase64).into(imgFoto);
            }
        }

        final AlertDialog dialog = alertDialog.create();
        dialog.show();

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
    }

    private Boolean validarObjectoDetalle() {
        return auditoria != null ? true : false;
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_back, null);
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorBlanco));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    private void obtenerDataLista() {
        Intent intent = new Intent();
        intent.putExtra("oLista", (Serializable) adaptador.getLista());
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnGuardar:
                obtenerDataLista();
                break;
        }
    }
}
