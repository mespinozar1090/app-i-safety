package com.mdp.fusionapp.ui.activity.incidentes;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.tabs.TabLayout;
import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.UsuarioNotificarEntity;
import com.mdp.fusionapp.database.entity.incidente.CrearIncidenteEntity;
import com.mdp.fusionapp.database.entity.sbc.NotificarEntity;
import com.mdp.fusionapp.model.IncidenteInsertModel;
import com.mdp.fusionapp.network.response.IncidenteDetalleResponse;
import com.mdp.fusionapp.network.response.Respuesta;
import com.mdp.fusionapp.ui.fragment.incidente.EventoFragment;
import com.mdp.fusionapp.ui.fragment.incidente.PrincipalFragment;
import com.mdp.fusionapp.utilitary.Constante;
import com.mdp.fusionapp.utilitary.CustomViewPager;
import com.mdp.fusionapp.utilitary.NetworkUtil;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.viewModel.IncidenteViewModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class IncidenteAcitvity extends AppCompatActivity {

    private IncidenteViewModel incidenteViewModel;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private CustomViewPager mViewPager;
    private TabLayout tabLayout;
    private IncidenteInsertModel objectPrincipal;
    private SharedPreferences sharedPreferences;
    private IncidenteDetalleResponse incidente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incidente_acitvity);
        initView();
        setToolbar();
    }

    private void initView() {
        incidente = (IncidenteDetalleResponse) getIntent().getSerializableExtra("incidente");

        incidenteViewModel = new ViewModelProvider(this).get(IncidenteViewModel.class);
        sharedPreferences = getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.disableScroll(true);
        mViewPager.setOffscreenPageLimit(3);
        tabLayout = findViewById(R.id.tabs);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        objectPrincipal = new IncidenteInsertModel();
        setUntouchableTab();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_back, null);
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorBlanco));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    private void setUntouchableTab() {
        LinearLayout tabStrip = ((LinearLayout) tabLayout.getChildAt(0));
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }
    }

    public void disableScroll(Boolean valor) {
        mViewPager.disableScroll(valor);
    }

    public void setViewPager(int page) {
        mViewPager.setCurrentItem(page);
    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    PrincipalFragment principalFragment = new PrincipalFragment().newInstance(incidente);
                    return principalFragment;
                case 1:
                    EventoFragment eventoFragment = new EventoFragment().newInstance(incidente);
                    return eventoFragment;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    private static String makeFragmentName(int viewId, long id) {
        return "android:switcher:" + viewId + ":" + id;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void obtenerDataPrincipal() {
        String fragmentTag = makeFragmentName(mViewPager.getId(), 0);
        FragmentManager fm = getSupportFragmentManager();
        PrincipalFragment fragment = (PrincipalFragment) fm.findFragmentByTag(fragmentTag);
        objectPrincipal = fragment.saveIndecentePrincipla();
    }

    public void insertarIncidente(IncidenteInsertModel evento, List<UsuarioNotificarEntity> lstNotificacion) {
        UtilMDP.showProgressDialog(this, "Guardando...");
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        List<HashMap<String, Object>> lstNotificacions = new ArrayList<>();
        HashMap<String, Object> notificacion = null;
        for (int x = 0; x < lstNotificacion.size(); x++) {
            if (lstNotificacion.get(x).getSelectEmail()) {
                notificacion = new HashMap<>();
                notificacion.put("IdUser", lstNotificacion.get(x).getIdUsers());
                notificacion.put("Nombre", lstNotificacion.get(x).getNombreUsuario());
                notificacion.put("Email", lstNotificacion.get(x).getEmailCorporativo());
                lstNotificacions.add(notificacion);
            }
        }

        List<HashMap<String, Object>> fListaPlanesAccion = new ArrayList<>();
        HashMap<String, Object> planesAccion = new HashMap<>();
        fListaPlanesAccion.add(planesAccion);

        List<HashMap<String, Object>> fListaTestigo = new ArrayList<>();
        HashMap<String, Object> testigo = new HashMap<>();
        fListaTestigo.add(testigo);

        obtenerDataPrincipal();

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("Id_Incidente", incidente != null ? incidente.getIdIncidente() : 0);
        hashMap.put("Empleador_Tipo", objectPrincipal.getEmpleadorTipo());
        hashMap.put("F_DESCRIPCION_Incidente", objectPrincipal.getfDescripcionIncidente());
        hashMap.put("F_Empleador_IdActividadEconomica", objectPrincipal.getfEmpleadorIdActividadEconomica());
        hashMap.put("F_Empleador_IdEmpresa", objectPrincipal.getfEmpleadorIdEmpresa());
        hashMap.put("F_Empleador_IdTamanioEmpresa", objectPrincipal.getfEmpleadorIdTamanioEmpresa());
        hashMap.put("F_EmpleadorI_IdActividadEconomica", objectPrincipal.getfEmpleadorIIdActividadEconomica());
        hashMap.put("F_EmpleadorI_RazonSocial", objectPrincipal.getfEmpleadorIRazonSocial());
        hashMap.put("F_EmpleadorI_RUC", objectPrincipal.getfEmpleadorIRuc());
        hashMap.put("F_Empleador_RazonSocial", objectPrincipal.getfEmpleadorRazonSocial());
        hashMap.put("F_Empleador_RUC", objectPrincipal.getfEmpleadorRuc());
        hashMap.put("F_EVENTO_FechaAccidente", evento.getfEventoFechaAccidente());
        hashMap.put("F_EVENTO_FechaInicioInvestigacion", evento.getfEventoFechaInicioInvestigacion());
        hashMap.put("F_EVENTO_HoraAccidente", evento.getfEventoHoraAccidente());
        hashMap.put("F_EVENTO_HuboDanioMaterial", evento.getfEventoHuboDanioMaterial());
        hashMap.put("F_EVENTO_IdEquipoAfectado", evento.getfEventoIdEquipoAfectado());
        hashMap.put("F_EVENTO_IdParteAfectada", evento.getfEventoIdParteAfectada());
        hashMap.put("F_EVENTO_IdTipoEvento", evento.getfEventoIdTipoEvento());
        hashMap.put("F_EVENTO_LugarExacto", evento.getfEventoLugarExacto());
        hashMap.put("F_EVENTO_NumTrabajadoresAfectadas", evento.getfEventoNumTrabajadoresAfectadas());
        hashMap.put("F_ListaPlanesAccion", fListaPlanesAccion);
        hashMap.put("F_ListaTestigos", fListaTestigo);

        hashMap.put("P_Empleador_IdEmpresa", objectPrincipal.getpEmpleadorIdEmpresa());
        hashMap.put("F_EmpleadorI_IdEmpresa", objectPrincipal.getfEmpleadorIIdEmpresa());

        hashMap.put("F_Trabajador_DetalleCategoriaOcupacional", evento.getfTrabajadorDetalleCategoriaOcupacional());
        hashMap.put("F_SUPERVISOR_IdRelacionEmpresa", null);
        hashMap.put("F_SUPERVISOR_NombreContratista", null);
        hashMap.put("F_SUPERVISOR_Nombre", null);
        hashMap.put("F_SUPERVISOR_DNI", null);
        hashMap.put("F_SUPERVISOR_Antiguedad", null);
        hashMap.put("F_SUPERVISOR_IdCategoriaOcupacional", null);
        hashMap.put("F_SUPERVISOR_NroPersonasSuper", null);
        hashMap.put("F_Trabajador_NombresApellidos", objectPrincipal.getfTrabajadorNombresApellidos());
        hashMap.put("G_Fecha_Creado", dateFormat.format(date));
        hashMap.put("G_Fecha_Modifica", dateFormat.format(date));
        hashMap.put("G_IdProyecto_Sede", evento.getgIdProyectoSede());
        hashMap.put("G_IdUsuario_Creado", sharedPreferences.getInt("IdUsers", 0));
        hashMap.put("G_IdUsuario_Modifica", sharedPreferences.getInt("IdUsers", 0));
        hashMap.put("P_EVENTO_IdTipoEvento", 1);
        hashMap.put("F_IdEstato_Formato", 3);
        hashMap.put("Tipo_InformeP_F", validarTipoInforme());
        hashMap.put("Origen_Registro", "App");
        hashMap.put("UsuariosNotificados", lstNotificacions);
        hashMap.put("G_DescripcionFormato", "Incidente-" + sharedPreferences.getString("nombreUsuario", "") + evento.getDescripcionSede());

        Log.e("parametros->", " : " + hashMap);

        if (NetworkUtil.isOnline(getApplicationContext())) {
            if (null != incidente) {
                incidenteViewModel.editarIncidente(hashMap, getApplicationContext()).observe(this, new Observer<Respuesta>() {
                    @Override
                    public void onChanged(Respuesta respuesta) {
                        UtilMDP.hideProgreesDialog();
                        if (null != respuesta) {
                            if (respuesta.getEstatus()) {
                                finish();
                            }

                        } else {
                            Toast.makeText(IncidenteAcitvity.this, "Ocurrio un error!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } else {
                incidenteViewModel.insertaIncidente(hashMap, getApplicationContext()).observe(this, new Observer<Respuesta>() {
                    @Override
                    public void onChanged(Respuesta respuesta) {
                        if (null != respuesta) {
                            if (respuesta.getEstatus()) {
                                finish();

                            }
                            UtilMDP.hideProgreesDialog();
                        } else {

                            Toast.makeText(IncidenteAcitvity.this, "Ocurrio un error!", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            }
        } else {
            insertarIncidenteOffline(evento, lstNotificacion);
        }

    }


    private void insertarIncidenteOffline(IncidenteInsertModel evento, List<UsuarioNotificarEntity> lstNotificacion) {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        CrearIncidenteEntity object = new CrearIncidenteEntity();
        object.setIdIncidente(incidente != null ? incidente.getIdIncidente() : 0);
        object.setEmpleadorTipo(objectPrincipal.getEmpleadorTipo());
        object.setFdescripcionIncidente(objectPrincipal.getfDescripcionIncidente());
        object.setFempleadorIdActividadEconomica(objectPrincipal.getfEmpleadorIdActividadEconomica());
        object.setFempleadorIdEmpresa(objectPrincipal.getfEmpleadorIdEmpresa());
        object.setFempleadorIdTamanioEmpresa(objectPrincipal.getfEmpleadorIdTamanioEmpresa());
        object.setFempleadorIIdActividadEconomica(objectPrincipal.getfEmpleadorIIdActividadEconomica());
        object.setFempleadorIRazonSocial(objectPrincipal.getfEmpleadorIRazonSocial());
        object.setFempleadorIRuc(objectPrincipal.getfEmpleadorIRuc());
        object.setFempleadorRazonSocial(objectPrincipal.getfEmpleadorRazonSocial());
        object.setFempleadorRuc(objectPrincipal.getfEmpleadorRuc());
        object.setFtrabajadorNombresApellidos(objectPrincipal.getfTrabajadorNombresApellidos());
        object.setPempleadorIdEmpresa(objectPrincipal.getpEmpleadorIdEmpresa());
        object.setFempleadorIIdEmpresa(objectPrincipal.getfEmpleadorIIdEmpresa());
        object.setFeventoFechaAccidente(evento.getfEventoFechaAccidente());
        object.setFeventoFechaInicioInvestigacion(evento.getfEventoFechaInicioInvestigacion());
        object.setFeventoHoraAccidente(evento.getfEventoHoraAccidente());
        object.setFeventoHuboDanioMaterial(evento.getfEventoHuboDanioMaterial());
        object.setFeventoIdEquipoAfectado(evento.getfEventoIdEquipoAfectado());
        object.setFeventoIdParteAfectada(evento.getfEventoIdParteAfectada());
        object.setFeventoIdTipoEvento(evento.getfEventoIdTipoEvento());
        object.setFeventoLugarExacto(evento.getfEventoLugarExacto());
        object.setFeventoNumTrabajadoresAfectadas(evento.getfEventoNumTrabajadoresAfectadas());
        object.setFtrabajadorDetalleCategoriaOcupacional( evento.getfTrabajadorDetalleCategoriaOcupacional());
        object.setGidProyectoSede(evento.getgIdProyectoSede());
        object.setGfechaCreado(dateFormat.format(new Date()));
        object.setGidFechaModifica(dateFormat.format(new Date()));
        object.setTipoInformePF(validarTipoInforme());
        object.setFeventoLugarExacto(dateFormat.format(new Date()));
        object.setGidUsuarioCreado(sharedPreferences.getInt("IdUsers", 0));
        object.setGidUsuarioModifica(sharedPreferences.getInt("IdUsers", 0));
        object.setGdescripcionFormato("Incidente-" + sharedPreferences.getString("nombreUsuario", "") + evento.getDescripcionSede());
        object.setEstadoRegistroDB(Constante.ESTADO_CREADO);

        if (null != incidente && incidente.getIdRow() != null && incidente.getIdRow() != 0) {
            object.setEstadoRegistroDB(Constante.ESTADO_EDITADO);
            object.setId(incidente.getIdRow());
        }
        Long idRow =  incidenteViewModel.insertaIncidenteOffline(object);
        if(idRow != null){
            for (int x = 0; x < lstNotificacion.size(); x++) {
                NotificarEntity notificarEntity = null;
                if (lstNotificacion.get(x).getSelectEmail()) {
                    notificarEntity = new NotificarEntity();
                    notificarEntity.setIdUser(lstNotificacion.get(x).getIdUsers());
                    notificarEntity.setNombre(lstNotificacion.get(x).getNombreUsuario());
                    notificarEntity.setEmail(lstNotificacion.get(x).getEmailCorporativo());
                    notificarEntity.setIdModulo(Constante.MODULO_INCIDENTE);
                    notificarEntity.setIdOffline(idRow);
                    incidenteViewModel.insertNotificarOffline(notificarEntity);
                }
            }
            Log.e("INSERT"," CORRECTAMENTE INCIDENTE");
            UtilMDP.hideProgreesDialog();
            showCreateRegistro("Estimado, tu registro se ha guardado con éxito. Cuando haya conexión a Internet se sincronizara con la web");
        }else{
            UtilMDP.hideProgreesDialog();
        }


    }

    private String validarTipoInforme() {
        String tipoInforme = "";
        if (null != incidente) {
            if (incidente.getTipoInforme().equalsIgnoreCase("PRELIMINAR")) {
                tipoInforme = "FINAL";
            } else if (incidente.getTipoInforme().equalsIgnoreCase("FINAL")) {
                tipoInforme = "FINAL";
            }
        } else {
            tipoInforme = "PRELIMINAR";
        }
        return tipoInforme;
    }


    public void showCreateRegistro(String mensaje) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Registro Guardado");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage(mensaje);
        alertDialogBuilder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                finish();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
