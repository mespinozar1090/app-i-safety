package com.mdp.fusionapp.ui.adapter.interventoria;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.interventoria.ControlSubItemEntity;

import java.util.ArrayList;
import java.util.List;

public class ControlPersonaItemAdapter extends RecyclerView.Adapter<ControlPersonaItemAdapter.ViewHolder> {
    private Context context;
    private List<ControlSubItemEntity> lista = new ArrayList<>();
    private static ClickListener clickListener;

    public ControlPersonaItemAdapter(Context context, List<ControlSubItemEntity> lista) {
        this.context = context;
        this.lista = lista;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_controlsub, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(lista.get(position));
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public void setLista(List<ControlSubItemEntity> lista) {
        this.lista = lista;
        notifyDataSetChanged();
    }

    public List<ControlSubItemEntity> getLista() {
        return lista;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextInputEditText tietcantidad;
        TextView txtNombre;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtNombre = itemView.findViewById(R.id.txtNombre);
            tietcantidad = itemView.findViewById(R.id.tietcantidad);
        }

        public void bind(ControlSubItemEntity item) {
            txtNombre.setText(item.getDescripcion());
            Log.e("Cantidad", "" + item.getCantidad());
            tietcantidad.setText(item.getCantidad() != null ? item.getCantidad().toString() : "0");

            tietcantidad.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!s.toString().equals("")) {
                        item.setCantidad(Double.parseDouble(s.toString()));
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public interface ClickListener {
        void onItemClick(int postion, View v);
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        ControlPersonaItemAdapter.clickListener = clickListener;
    }
}
