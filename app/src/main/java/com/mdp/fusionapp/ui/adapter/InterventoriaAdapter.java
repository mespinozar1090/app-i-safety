package com.mdp.fusionapp.ui.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.model.InterventoriaModel;
import com.mdp.fusionapp.network.response.InterventoriaResponse;
import com.mdp.fusionapp.utilitary.UtilMDP;

import java.util.ArrayList;
import java.util.List;

public class InterventoriaAdapter extends  RecyclerView.Adapter<InterventoriaAdapter.ViewHolder> {

    private Context context;
    private List<InterventoriaResponse.Data> lista = new ArrayList<>();
    private  static ClickListener clickListener;
    private static long mLastClickTime = 0;
    private static SharedPreferences sharedPreferences;
    public InterventoriaAdapter(Context context, List<InterventoriaResponse.Data> lista) {
        sharedPreferences = context.getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        this.context = context;
        this.lista = lista;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_interventoria,parent,false);
        return  new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.bind(lista.get(position));
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }
    public void setInterventoria(List<InterventoriaResponse.Data> hallazgos){
        this.lista = hallazgos;
        notifyDataSetChanged();
    }

    public void removeItem(int position){
        lista.remove(position);
        notifyItemRemoved(position);
    }

    public InterventoriaResponse.Data getInterventoria(int position){
        return lista.get(position);
    }



    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView txtCodigo,txtFormato,txtUsuario;
        private ConstraintLayout cardRoot;
        private ImageView btnBorrar;

        public ViewHolder(@NonNull View view) {
            super(view);
            txtCodigo = view.findViewById(R.id.txtCodigo);
            txtFormato = view.findViewById(R.id.txtFormato);
            txtUsuario = view.findViewById(R.id.txtUsuario);
            cardRoot = view.findViewById(R.id.cardRoot);
            btnBorrar = view.findViewById(R.id.btnBorrar);
        }

        public void bind(InterventoriaResponse.Data item){
            if(sharedPreferences.getInt("Id_Rol_Usuario", 0) == 1){
                btnBorrar.setVisibility(View.VISIBLE);
            }
            txtCodigo.setText(item.getCodigo());
            txtFormato.setText(item.getNombreFormato());
            txtUsuario.setText(item.getNombreUsuario());
            btnBorrar.setOnClickListener(this);
            cardRoot.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            clickListener.onItemClick(getAdapterPosition(),view);
        }
    }
    public interface ClickListener{
        void onItemClick(int postion,View v);
    }

    public void setOnItemClickListener(ClickListener clickListener){
        InterventoriaAdapter.clickListener = clickListener;
    }
}
