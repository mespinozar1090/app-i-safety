package com.mdp.fusionapp.ui.activity.inspecciones;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import android.app.DownloadManager;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.barteksc.pdfviewer.PDFView;
import com.google.android.material.snackbar.Snackbar;
import com.mdp.fusionapp.R;
import com.mdp.fusionapp.utilitary.UtilMDP;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class VisorPDFActivity extends AppCompatActivity {

    private PDFView pdfView;
    private Integer idInspeccion;
    DownloadManager downloadManager;
    private String nombreDocumento;
    private RelativeLayout rootLayout;
    String urlPDF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visor_p_d_f);
        setToolbar();
        initView();
    }
    //getIntent().getStringExtra("urlDocumento");
    private void initView(){
        pdfView = findViewById(R.id.pdfView);
        rootLayout = findViewById(R.id.rootLayout);
        String urlDocumento  = getIntent().getStringExtra("urlDocumento");
        nombreDocumento = getIntent().getStringExtra("nombreDocumento");
        urlPDF = urlDocumento;
        Log.e("urlDocumento"," : "+urlDocumento);
       // urlDocumento = "https://extranetprb.rep.com.pe/fusionrepienso_pdi/Inspeccion/VisualizarPDF?idInspeccion=3350";
        //pdfView.fromUri(myUri);
         new RetrivePDFStream().execute(urlDocumento);
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView descripcion = (TextView)toolbar.findViewById(R.id.tvDescription);
        descripcion.setText("Documento");
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_back, null);
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorBlanco));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_visor, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }else if (item.getItemId() == R.id.action_donwload){
            descargarPDF(urlPDF);
        }
        return super.onOptionsItemSelected(item);
    }

    public void descargarPDF(String rutaPdf) {

        Context context = getApplicationContext();
        downloadManager = (DownloadManager)getSystemService(DOWNLOAD_SERVICE);
        //Uri uri = Uri.parse("http://10.125.6.97/repienso/Inspeccion/InspeccionPDF?idIns=49");
        Uri uri = Uri.parse(rutaPdf);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setTitle(nombreDocumento);
        request.setAllowedOverMetered(true);
        request.setAllowedOverRoaming(true);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        Long reference = downloadManager.enqueue(request);

        success(rootLayout,"Descargando documento");
    }

    public static void success(View view, String mensaje) {
        Snackbar snackbar;
        snackbar = Snackbar.make(view, mensaje, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(Color.parseColor("#17A05E"));
        TextView textView = (TextView) snackBarView.findViewById(R.id.snackbar_text);
        //  textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_white_24dp, 0, 0, 0);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    class RetrivePDFStream extends AsyncTask<String,Void, InputStream> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            UtilMDP.showProgressDialog(VisorPDFActivity.this,"Cargando...");
        }

        @Override
        protected InputStream doInBackground(String... params) {
            HttpURLConnection urlConnection = null;
            InputStream inputStream =null;
            try {
                URL urlServer = new URL(params [0]);
                urlConnection = (HttpURLConnection) urlServer.openConnection();
                if (urlConnection.getResponseCode() == 200) {
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                }
            } catch (Exception e) {
                e.printStackTrace();
                UtilMDP.hideProgreesDialog();
            }
            return inputStream;
        }

        @Override
        protected void onPostExecute(InputStream inputStream) {
            super.onPostExecute(inputStream);
            UtilMDP.hideProgreesDialog();

            Log.e("entro","Termino processo");
            pdfView.fromStream(inputStream).load();
        }
    }
}
