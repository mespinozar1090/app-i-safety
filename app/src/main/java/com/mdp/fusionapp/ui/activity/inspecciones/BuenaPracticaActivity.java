package com.mdp.fusionapp.ui.activity.inspecciones;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.CategoriaBuenaPracticaEntity;
import com.mdp.fusionapp.model.BuenaPracticaModel;
import com.mdp.fusionapp.ui.adapter.BuenaPracticaAdapter;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.viewModel.InspeccionViewModel;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BuenaPracticaActivity extends AppCompatActivity implements View.OnClickListener {

    Spinner spPractica;
    private InspeccionViewModel inspeccionViewModel;
    private BuenaPracticaAdapter adaptador;
    private RecyclerView recyclerview;
    private LinearLayoutManager linearLayout;
    private static List<BuenaPracticaModel> lista;
    private List<BuenaPracticaModel> listData;
    private Button btnGuardar;
    private static final int SELECT_IMAGE = 3;
    String encodeImage;
    ImageView imgCategoria;
    //capturar imagen
    private static String nameImage = "";
    private static String prefijo = "";
    private static String pathDCIM = "";
    private Integer indextSelect, isEditar;

    private static BuenaPracticaActivity instancia;
    private static String test;


    public static BuenaPracticaActivity newInstances(List<BuenaPracticaModel> lstBuenaPractica) {
        if (instancia == null) {
            instancia = new BuenaPracticaActivity();
        }
        lista = lstBuenaPractica;
        return instancia;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buena_practica);
        inspeccionViewModel = new ViewModelProvider(this).get(InspeccionViewModel.class);
        setToolbar();
        initView();
        Log.e("TAG", "onCreate: lista size : " + lista.size());
    }

    private void initView() {
        recyclerview = findViewById(R.id.recyclerview);
        btnGuardar = findViewById(R.id.btnGuardar);
        linearLayout = new LinearLayoutManager(getApplicationContext());
        recyclerview.setLayoutManager(linearLayout);
        //   lista = new ArrayList<>();
        listData = new ArrayList<>();
        adaptador = new BuenaPracticaAdapter(getApplicationContext(), lista);
        recyclerview.setAdapter(adaptador);
        btnGuardar.setOnClickListener(this);


        Intent intent = getIntent();
        //   lista = (List<BuenaPracticaModel>) intent.getSerializableExtra("lista");
        isEditar = intent.getIntExtra("editar", 0);
        if (lista != null) {
            adaptador.setBuenaPracticaModel(lista);
        } else {
            lista = new ArrayList<>();
        }
        onclickTable();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_back, null);
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorBlanco));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    public void utilizarCamara() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Uri mProcessingPhotoUri = UtilMDP.getImageFileCompat(getApplicationContext());
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mProcessingPhotoUri);
        } else {
            nameImage = prefijo + "_" + UtilMDP.getCustomDate("yyyyMMddHHmmss") + ".jpg";
            File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
            pathDCIM = storageDir.getAbsolutePath() + File.separator + nameImage;
            File file = new File(pathDCIM);
            Uri outputFileUri = Uri.fromFile(file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        }
        startActivityForResult(intent, UtilMDP.REQUEST_CAMERA_SEND_ALERT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == UtilMDP.REQUEST_CAMERA_SEND_ALERT) {
            if (resultCode == Activity.RESULT_OK) {
                Bitmap bitmapCapture = UtilMDP.onCaptureImageResult(data);
                setImageCapture(bitmapCapture);
            }
        } else if (requestCode == SELECT_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    try {
                        if (Build.VERSION.SDK_INT < 28) {
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                            setImageCapture(bitmap);
                            //setImageCapture(MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData()));
                        } else {
                            Uri uri = data.getData();
                            // ImageDecoder.Source source = ImageDecoder.createSource(this.getContentResolver(), uri);
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                            setImageCapture(bitmap);
                            // setImageCapture(ImageDecoder.decodeBitmap(source));

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(BuenaPracticaActivity.this, "Canceled", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void showModalAddBuenaPractica(BuenaPracticaModel buenaPracticaModel, Integer position) {
        BuenaPracticaModel practicaModel = new BuenaPracticaModel();
        encodeImage = "";
        View viewAddProject = getLayoutInflater().inflate(R.layout.buena_practica_add, null);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(false);
        alertDialog.setView(viewAddProject);

        spPractica = viewAddProject.findViewById(R.id.spPractica);
        EditText edtMensaje = viewAddProject.findViewById(R.id.edtMensaje);
        imgCategoria = viewAddProject.findViewById(R.id.imgCategoria);

        Button btnAgregar = viewAddProject.findViewById(R.id.btnAgregar);
        Button btnCancelar = viewAddProject.findViewById(R.id.btnCancelar);

        ImageButton btnGallery = viewAddProject.findViewById(R.id.btnGallery);
        ImageButton btnCamera = viewAddProject.findViewById(R.id.btnCamera);


        Dialog dialog = alertDialog.create();
        dialog.show();

        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utilizarCamara();
            }
        });

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE);
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (encodeImage.equals("") || edtMensaje.getText().toString().trim().equals("")) {
                    Toast.makeText(BuenaPracticaActivity.this, "Debe ingresar una descripcion y adjuntar una imagen", Toast.LENGTH_SHORT).show();
                } else {
                    CategoriaBuenaPracticaEntity object = (CategoriaBuenaPracticaEntity) spPractica.getSelectedItem();
                    practicaModel.setDescripcion(edtMensaje.getText().toString());
                    practicaModel.setIdCategoriaBuenaPractica(object.getIdCategoriaBuenaPractica());
                    practicaModel.setImgBase64(encodeImage != null ? encodeImage : "");
                    practicaModel.setNombre(object.getNombre());
                    if (position != null) {
                        lista.remove(buenaPracticaModel);
                    }
                    lista.add(practicaModel);
                    adaptador.setBuenaPracticaModel(lista);
                    adaptador.notifyDataSetChanged();
                    dialog.dismiss();
                }
            }
        });

        if (buenaPracticaModel != null && position != null) {
            practicaModel.setIdCategoriaBuenaPractica(buenaPracticaModel.getIdCategoriaBuenaPractica());
            practicaModel.setIdBuenasPracticas(buenaPracticaModel.getIdBuenasPracticas());
            edtMensaje.setText(buenaPracticaModel.getDescripcion());
            if (buenaPracticaModel.getImgBase64() != null && !buenaPracticaModel.getImgBase64().equals("")) {
                encodeImage = buenaPracticaModel.getImgBase64();
                if (buenaPracticaModel.getImgBase64().contains("base64")) {
                    String base64 = buenaPracticaModel.getImgBase64();
                    String[] parts = base64.split("base64,");
                    String part1 = parts[0];
                    String part2 = parts[1];
                    imgCategoria.setImageBitmap(UtilMDP.decodeBase64(part2));
                } else {
                    Picasso.get().load(buenaPracticaModel.getImgBase64()).into(imgCategoria);
                }
            }
        }
        if (buenaPracticaModel != null && buenaPracticaModel.getIdCategoriaBuenaPractica() != null) {
            loadCategoriaBuenaPractica(buenaPracticaModel.getIdCategoriaBuenaPractica());
        } else {
            loadCategoriaBuenaPractica(null);
        }

    }


    private void loadCategoriaBuenaPractica(Integer idCategoria) {
        inspeccionViewModel.categoriaBuenaPractica().observe(this, new Observer<List<CategoriaBuenaPracticaEntity>>() {
            @Override
            public void onChanged(List<CategoriaBuenaPracticaEntity> categoriaBuenaPracticaEntities) {
                if (null != categoriaBuenaPracticaEntities) {
                    showSpinnerBuenaPractica(categoriaBuenaPracticaEntities, idCategoria);
                }
            }
        });
    }

    private void showSpinnerBuenaPractica(List<CategoriaBuenaPracticaEntity> buenaPracticaResponses, Integer idCategoria) {
        ArrayAdapter<CategoriaBuenaPracticaEntity> adapterTipoDocumento = new ArrayAdapter<>(this, R.layout.spinner_text, buenaPracticaResponses);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPractica.setAdapter(adapterTipoDocumento);
        spPractica.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CategoriaBuenaPracticaEntity object = (CategoriaBuenaPracticaEntity) parent.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        if (idCategoria != null) {
            for (int x = 0; x < buenaPracticaResponses.size(); x++) {
                if (buenaPracticaResponses.get(x).getIdCategoriaBuenaPractica().intValue() == idCategoria) {
                    spPractica.setSelection(x);
                }
            }
        }

    }

    private void setImageCapture(Bitmap bitmapCaptures) {
        // Bitmap bitmap = ((BitmapDrawable) imgFoto.getDrawable()).getBitmap();
        Bitmap bitmapCapture = Bitmap.createScaledBitmap(bitmapCaptures, 250, 250, true);

        imgCategoria.setImageBitmap(bitmapCapture);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmapCapture.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        encodeImage = "data:image/jpeg;base64," + Base64.encodeToString(b, Base64.DEFAULT);
    }

    private void onclickTable() {
        adaptador.setOnItemClickListener((int positionL, View vL) -> {
            if (vL.getId() == R.id.imgFotoCategoria) {
                showCaptureImagen(adaptador.getBuenaPracticaModel(positionL).getImgBase64());
            } else if (vL.getId() == R.id.btnEditar) {
                showModalAddBuenaPractica(adaptador.getBuenaPracticaModel(positionL), positionL);
            }
        });
    }

    private void showCaptureImagen(String imgBase64) {
        View viewAddProject = getLayoutInflater().inflate(R.layout.show_foto, null);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(true);
        alertDialog.setView(viewAddProject);

        Button btnClose = viewAddProject.findViewById(R.id.btnClose);
        ImageView imgFoto = viewAddProject.findViewById(R.id.imgFoto);

        if (imgBase64.contains("base64")) {
            String base64 = imgBase64;
            String[] parts = base64.split("base64,");
            String part1 = parts[0];
            String part2 = parts[1];

            imgFoto.setImageBitmap(UtilMDP.decodeBase64(part2));
        } else {
            Picasso.get().load(imgBase64).into(imgFoto);
        }

        final AlertDialog dialog = alertDialog.create();
        dialog.show();

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.item_add) {
            showModalAddBuenaPractica(null, null);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnGuardar:
                obtenerData();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void obtenerData() {
        try {
            UtilMDP.showProgressDialog(BuenaPracticaActivity.this, "Guardando...");
            Intent intent = new Intent();
            intent.putExtra("oLista", (Serializable) adaptador.getLista());
            setResult(RESULT_OK, intent);
            UtilMDP.hideProgreesDialog();
            finish();
        } catch (Exception e) {
            Toast.makeText(this, "result error " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }
}
