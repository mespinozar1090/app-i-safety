package com.mdp.fusionapp.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.model.HallazgoModel;

import java.util.ArrayList;
import java.util.List;

public class HallazgoAdapter extends RecyclerView.Adapter<HallazgoAdapter.ViewHolder> {

    private Context context;
    private List<HallazgoModel> lista = new ArrayList<>();
    private  static ClickListener clickListener;

    public HallazgoAdapter(Context context, List<HallazgoModel> lista) {
        this.context = context;
        this.lista = lista;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hallazgo,parent,false);
        return  new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.bind(lista.get(position),context);
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public void setHallazgo(List<HallazgoModel> hallazgos){
        this.lista = hallazgos;
        notifyDataSetChanged();
    }

    public HallazgoModel getHallazgo(int position){
        return lista.get(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView txtHallazgo,txtUsuario,txtFecha;
        private Button btnTipoGestion,btnEstado;
        private LinearLayout cardRoot;
        private LinearLayout nivelRiesgo;

        public ViewHolder(@NonNull View view) {
            super(view);
            txtHallazgo = view.findViewById(R.id.txtHallazgo);
            txtUsuario = view.findViewById(R.id.txtUsuario);
            txtFecha = view.findViewById(R.id.txtFecha);
            btnTipoGestion = view.findViewById(R.id.btnTipoGestion);
            btnEstado = view.findViewById(R.id.btnEstado);
            cardRoot = view.findViewById(R.id.cardRoot);
            nivelRiesgo = view.findViewById(R.id.nivelRiesgo);
        }

        public void bind(HallazgoModel item, Context context){
            txtHallazgo.setText(item.getNombreTipoHallazgo().trim());
            txtUsuario.setText(item.getNombreActoSubestandar().trim());
            txtFecha.setText("Plazo : "+item.getPlazo());

            switch (item.getRiesgo()) {
                case 1:
                    nivelRiesgo.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGreen));
                    break;
                case 2:
                    nivelRiesgo.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAmbar));
                    break;
                case 3:
                    nivelRiesgo.setBackgroundColor(ContextCompat.getColor(context, R.color.cardRosa));
                    break;
            }

            btnEstado.setText(item.getEstadoDetalleInspeccion() ? "Cerrado" : "Abierto" );
            btnEstado.setTextColor(item.getEstadoDetalleInspeccion() ? ContextCompat.getColor(context, R.color.cardRosa) :  ContextCompat.getColor(context, R.color.colorNegro));
            btnTipoGestion.setText(item.getNombreTipoGestion());
            cardRoot.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            clickListener.onItemClick(getAdapterPosition(),view);
        }
    }

    public interface ClickListener{
        void onItemClick(int postion,View v);
    }

    public void setOnItemClickListener(ClickListener clickListener){
        HallazgoAdapter.clickListener = clickListener;
    }
}
