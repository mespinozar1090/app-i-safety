package com.mdp.fusionapp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.UsuarioNotificarEntity;

import java.util.ArrayList;
import java.util.List;

public class NotificacionApdarter extends RecyclerView.Adapter<NotificacionApdarter.ViewHolder> {

    private Context context;
    private List<UsuarioNotificarEntity> lista = new ArrayList<>();

    public NotificacionApdarter(Context context, List<UsuarioNotificarEntity> lista) {
        this.context = context;
        this.lista = lista;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_notificacion, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.bind(context, lista.get(position));
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }


    public List<UsuarioNotificarEntity> getLista() {
        return lista;
    }

    public void setLista(List<UsuarioNotificarEntity> lista) {
        this.lista = lista;
        notifyDataSetChanged();
    }

    public void setFilter(ArrayList<UsuarioNotificarEntity> newLista) {
        lista = new ArrayList<>();
        lista.addAll(newLista);
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtNombre, txtCorreo;
        private CheckBox cboSelect;

        public ViewHolder(@NonNull View view) {
            super(view);
            txtNombre = view.findViewById(R.id.txtNombre);
            txtCorreo = view.findViewById(R.id.txtCorreo);
            cboSelect = view.findViewById(R.id.cboSelect);
        }

        public void bind(Context context, UsuarioNotificarEntity item) {
            cboSelect.setChecked(false);
            txtNombre.setText(item.getNombreUsuario()+" "+item.getApellidoUsuario());
            txtCorreo.setText(item.getEmailCorporativo());
            cboSelect.setChecked(item.getSelectEmail());

            cboSelect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cboSelect.isChecked()) {
                        item.setSelectEmail(true);
                    } else {
                        item.setSelectEmail(false);
                    }
                }
            });
        }
    }
}
