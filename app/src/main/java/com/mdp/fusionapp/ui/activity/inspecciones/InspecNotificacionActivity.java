package com.mdp.fusionapp.ui.activity.inspecciones;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.UsuarioNotificarEntity;
import com.mdp.fusionapp.database.entity.sbc.NotificarEntity;
import com.mdp.fusionapp.ui.adapter.NotificacionApdarter;
import com.mdp.fusionapp.utilitary.Constante;
import com.mdp.fusionapp.utilitary.NetworkUtil;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.viewModel.InspeccionViewModel;
import com.mdp.fusionapp.viewModel.LoginViewModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class InspecNotificacionActivity extends AppCompatActivity implements View.OnClickListener {

    private LoginViewModel loginViewModel;
    private InspeccionViewModel inspeccionViewModel;
    private NotificacionApdarter adaptador;
    private RecyclerView recyclerview;
    private LinearLayoutManager linearLayout;
    private List<UsuarioNotificarEntity> lista;
    private Button btnAgregar;
    private EditText etdBuscar;
    private SharedPreferences sharedPreferences;
    private String codigoInspeccion, nombreproyecto;
    private Integer idInspeccion;
    private Long idRowInspeccion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inspec_notificacion);
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        inspeccionViewModel = new ViewModelProvider(this).get(InspeccionViewModel.class);
        codigoInspeccion = getIntent().getStringExtra("codigoInspeccion");
        nombreproyecto = getIntent().getStringExtra("nombreproyecto");
        idInspeccion = getIntent().getIntExtra("idInspeccion", 0);
        idRowInspeccion = getIntent().getLongExtra("idRowInspeccion", 0);

        lista = new ArrayList<>();
        setToolbar();
        initView();
    }

    private void initView() {
        sharedPreferences = getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        btnAgregar = findViewById(R.id.btnAgregar);
        recyclerview = findViewById(R.id.recyclerview);
        etdBuscar = findViewById(R.id.etdBuscar);
        linearLayout = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(linearLayout);
        adaptador = new NotificacionApdarter(this, lista);
        recyclerview.setAdapter(adaptador);

        filtrarLista();
        obtenerUsuarioNotificar();
        btnAgregar.setOnClickListener(this);
    }

    private void filtrarLista() {
        etdBuscar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence editable, int i, int i1, int i2) {
                String aux = editable.toString().trim();
                if (!aux.equals("")) {
                    ArrayList<UsuarioNotificarEntity> newLista = new ArrayList<>();
                    for (UsuarioNotificarEntity usuarioMap : lista) {
                        String nombre = usuarioMap.getNombreUsuario();
                        if (nombre.toLowerCase().contains(aux.toLowerCase())) {
                            newLista.add(usuarioMap);
                        }
                    }
                    adaptador.setFilter(newLista);
                } else {
                    adaptador.setLista(lista);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_back, null);
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorBlanco));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void obtenerUsuarioNotificar() {
        loginViewModel.obtenerUsuarioNotificar().observe(this, new Observer<List<UsuarioNotificarEntity>>() {
            @Override
            public void onChanged(List<UsuarioNotificarEntity> usuarioNotiRespons) {
                if (null != usuarioNotiRespons) {
                    lista.addAll(usuarioNotiRespons);
                    adaptador.setLista(lista);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAgregar:
                enviarCorreo();
                break;
        }
    }

    private void enviarCorreo() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        List<HashMap<String, Object>> lstNotificacions = new ArrayList<>();
        HashMap<String, Object> notificacion = null;
        for (int x = 0; x < lista.size(); x++) {
            if (lista.get(x).getSelectEmail()) {
                notificacion = new HashMap<>();
                notificacion.put("IdUser", lista.get(x).getIdUsers());
                notificacion.put("Nombre", lista.get(x).getNombreUsuario());
                notificacion.put("Email", lista.get(x).getEmailCorporativo());
                lstNotificacions.add(notificacion);
            }
        }

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("Id_Inspecciones", idInspeccion);
        hashMap.put("codigoInspeccion", codigoInspeccion);
        hashMap.put("nombreproyecto", nombreproyecto);
        hashMap.put("proyecto", nombreproyecto);
        hashMap.put("UsuariosNotificados", lstNotificacions);
        hashMap.put("Origen_Registro", "APP");
        hashMap.put("s_fecha_Hora_Inspeccion", dateFormat.format(new Date()));
        hashMap.put("Id_Usuario", sharedPreferences.getInt("IdUsers", 0));

        Log.e("params", " " + hashMap);

        if (NetworkUtil.isOnline(getApplicationContext())) {
            inspeccionViewModel.sendNotification(hashMap).observe(this, new Observer<String>() {
                @Override
                public void onChanged(String result) {
                    if (result != null) {
                        showValidarNotificacion("Se notificó a todos los usuarios seleccionados");
                    }
                }
            });
        } else {
            for (int x = 0; x < lista.size(); x++) {
                NotificarEntity notificarEntity = null;
                if (lista.get(x).getSelectEmail()) {
                    notificarEntity = new NotificarEntity();
                    notificarEntity.setIdUser(lista.get(x).getIdUsers());
                    notificarEntity.setNombre(lista.get(x).getNombreUsuario());
                    notificarEntity.setEmail(lista.get(x).getEmailCorporativo());
                    notificarEntity.setIdModulo(Constante.MODULO_INSPECCION);
                    notificarEntity.setIdOffline(idRowInspeccion);
                    inspeccionViewModel.interNotificarOffline(notificarEntity);
                }
            }
            showValidarNotificacion("Estimado, se notificará a los usuarios seleccionados cuando haya conexión a Internet");
        }


    }

    public void showValidarNotificacion(String mensaje) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Información");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage(mensaje);
        alertDialogBuilder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                finish();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


}