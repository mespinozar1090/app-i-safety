package com.mdp.fusionapp.ui.activity.interventoria;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.material.tabs.TabLayout;
import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.interventoria.CrearControlEntity;
import com.mdp.fusionapp.database.entity.interventoria.CrearEvidenciaInterEntity;
import com.mdp.fusionapp.database.entity.interventoria.CrearInterventoriaEntiy;
import com.mdp.fusionapp.database.entity.interventoria.CrearVerificacionEntity;
import com.mdp.fusionapp.database.entity.sbc.NotificarEntity;
import com.mdp.fusionapp.model.DetalleControlInterModel;
import com.mdp.fusionapp.model.DetalleVerificacionInterModel;
import com.mdp.fusionapp.model.EvidenciaInterventoriaModel;
import com.mdp.fusionapp.model.registro.InsertInterventoriaModel;
import com.mdp.fusionapp.network.response.Respuesta;
import com.mdp.fusionapp.database.entity.UsuarioNotificarEntity;
import com.mdp.fusionapp.network.response.interventoria.InterventoriaDetalleResponse;
import com.mdp.fusionapp.ui.fragment.interventoria.ControlInterFragment;
import com.mdp.fusionapp.ui.fragment.interventoria.DatosGeneralesInterFragment;
import com.mdp.fusionapp.ui.fragment.interventoria.ObservacionesFragment;
import com.mdp.fusionapp.ui.fragment.interventoria.VerificacionesInterFragment;
import com.mdp.fusionapp.utilitary.Constante;
import com.mdp.fusionapp.utilitary.CustomViewPager;
import com.mdp.fusionapp.utilitary.NetworkUtil;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.viewModel.InterventoriaViewModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class InterventoriaActivity extends AppCompatActivity {

    private InterventoriaViewModel viewModel;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private CustomViewPager mViewPager;
    private TabLayout tabLayout;
    private InterventoriaDetalleResponse interventoria;
    InsertInterventoriaModel insertObject = null;
    List<DetalleVerificacionInterModel> lstVerificaciones;
    private SharedPreferences sharedPreferences;
    private List<DetalleControlInterModel> listaControl;
    private List<UsuarioNotificarEntity> listaNotificacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interventoria);
        interventoria = (InterventoriaDetalleResponse) getIntent().getSerializableExtra("interventoria");
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        setToolbar();
        initView();
    }

    private void initView() {
        viewModel = new ViewModelProvider(this).get(InterventoriaViewModel.class);
        sharedPreferences = getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.disableScroll(true);
        mViewPager.setOffscreenPageLimit(4);

        tabLayout = findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        setUntouchableTab();

    }

    private void setUntouchableTab() {
        LinearLayout tabStrip = ((LinearLayout) tabLayout.getChildAt(0));
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }
    }

    public void disableScroll(Boolean valor) {
       // mViewPager.disableScroll(valor);
        mViewPager.disableScroll(true);
    }

    public void setViewPager(int page) {
        Log.e("page", "" + page);
        mViewPager.setCurrentItem(page);
    }

    private Boolean validarDetalle() {
        return interventoria != null ? true : false;
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_back, null);
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorBlanco));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Log.e("position", "" + position);
            switch (position) {
                case 0:
                    DatosGeneralesInterFragment datosGeneralesInterFragment = new DatosGeneralesInterFragment().newInstance(interventoria);
                    return datosGeneralesInterFragment;
                case 1:
                    VerificacionesInterFragment verificacionesInterFragment = new VerificacionesInterFragment().newInstance(interventoria);
                    return verificacionesInterFragment;
                case 2:
                    ControlInterFragment controlInterFragment = new ControlInterFragment().newInstance(interventoria);
                    return controlInterFragment;
                case 3:
                    ObservacionesFragment observacionesFragment = new ObservacionesFragment().newInstance(interventoria);
                    return observacionesFragment;
                default:
                    return null;


            }
        }

        @Override
        public int getCount() {
            return 4;
        }
    }

    private static String makeFragmentName(int viewId, long id) {
        return "android:switcher:" + viewId + ":" + id;
    }

    private void obtenerDataGeneral() {
        String fragmentTag = makeFragmentName(mViewPager.getId(), 0);
        FragmentManager fm = getSupportFragmentManager();
        DatosGeneralesInterFragment fragment = (DatosGeneralesInterFragment) fm.findFragmentByTag(fragmentTag);

        if (fragment != null) {
            insertObject = fragment.saveDatosGenerales();
        }
    }

    private List<DetalleVerificacionInterModel> obtenerVerificaciones() {
        String fragmentTag = makeFragmentName(mViewPager.getId(), 1);
        FragmentManager fm = getSupportFragmentManager();
        VerificacionesInterFragment fragment = (VerificacionesInterFragment) fm.findFragmentByTag(fragmentTag);
        if (fragment != null) {
            lstVerificaciones = fragment.listAllVerficaciones();
        }
        return lstVerificaciones;
    }

    public void insertarControl(List<DetalleControlInterModel> lstControl, List<UsuarioNotificarEntity> lstNotificacion) {
        listaNotificacion = lstNotificacion;
        listaControl = lstControl;
    }

    public void insertConcluciones(String concluciones,List<EvidenciaInterventoriaModel> lstEvidencia) {
        insertarInterventoria(listaControl, listaNotificacion, concluciones,lstEvidencia);
    }

    public void showValidarNotificacion() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getApplicationContext());
        alertDialogBuilder.setTitle("Notificación");
        alertDialogBuilder.setMessage("Debe seleccionar a quien notificar");
        alertDialogBuilder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int arg1) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void insertarInterventoria(List<DetalleControlInterModel> lstControl, List<UsuarioNotificarEntity> lstNotificacion, String concluciones,List<EvidenciaInterventoriaModel> lstEvidencia) {
        UtilMDP.showProgressDialog(this, "Guardando...");
        obtenerDataGeneral();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        List<HashMap<String, Object>> lstNotificacions = new ArrayList<>();
        HashMap<String, Object> notificacion = null;
        if (lstNotificacion !=null && lstNotificacion.size()>0) {
            for (int x = 0; x < lstNotificacion.size(); x++) {
                if (lstNotificacion.get(x).getSelectEmail()) {
                    notificacion = new HashMap<>();
                    notificacion.put("IdUser", lstNotificacion.get(x).getIdUsers());
                    notificacion.put("Nombre", lstNotificacion.get(x).getNombreUsuario());
                    notificacion.put("Email", lstNotificacion.get(x).getEmailCorporativo());
                    lstNotificacions.add(notificacion);
                }
            }
        }else{
            showValidarNotificacion();
            return;
        }


        List<HashMap<String, Object>> lstVerificacionesMap = new ArrayList<>();
        HashMap<String, Object> verificaciones = null;
        List<DetalleVerificacionInterModel> data = obtenerVerificaciones();
        for (int x = 0; x < data.size(); x++) {
            verificaciones = new HashMap<>();
            verificaciones.put("Id_Interventoria_Detalle", data.get(x).getIdInterventoriaDetalle() != null ? data.get(x).getIdInterventoriaDetalle() : null);
            verificaciones.put("Id_Verificacion_Subitem", data.get(x).getIdVerificacionSubitem());
            verificaciones.put("Id_Verificacion", data.get(x).getIdVerificacion());
            verificaciones.put("Cumple", data.get(x).getCumple());
            verificaciones.put("Id_Interventoria", validarDetalle() ? interventoria.getData().getIdInterventoria() : null);
            lstVerificacionesMap.add(verificaciones);
        }

        Log.e("lstVerificacionesMap", " : " + lstVerificacionesMap);

        List<HashMap<String, Object>> lstControlsMap = new ArrayList<>();
        HashMap<String, Object> controles = null;
        for (int x = 0; x < lstControl.size(); x++) {
            controles = new HashMap<>();
            controles.put("Id_Control_Detalle", lstControl.get(x).getIdControlDetalle() != null ? lstControl.get(x).getIdControlDetalle() : null);
            controles.put("Id_Control_item", lstControl.get(x).getIdControlItem());
            controles.put("Id_Control", lstControl.get(x).getIdControl());
            controles.put("Cantidad", lstControl.get(x).getCantidad());
            lstControlsMap.add(controles);
        }


        List<HashMap<String, Object>> lstEvidenciaMap = new ArrayList<>();
        HashMap<String, Object> evidencia = null;
        if (null != lstEvidencia && lstEvidencia.size() > 0) {
            for (int x = 0; x < lstEvidencia.size(); x++) {
                evidencia = new HashMap<>();
                evidencia.put("Id_Interventoria_Imagen", lstEvidencia.get(x).getIdInterventoriaImagen() != null ? lstEvidencia.get(x).getIdInterventoriaImagen() : null);
                evidencia.put("Id_Interventoria", validarDetalle() ? interventoria.getData().getIdInterventoria() : 0);
                evidencia.put("Imagen", lstEvidencia.get(x).getImgBase64());
                evidencia.put("Descripcion", lstEvidencia.get(x).getDescripcion());
                evidencia.put("FechaCreacion", dateFormat.format(new Date()));
                lstEvidenciaMap.add(evidencia);
            }
        }

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("Id_Interventoria", validarDetalle() ? interventoria.getData().getIdInterventoria() : 0);
        hashMap.put("Actividad", insertObject.getActividad());
        hashMap.put("Conclucion", concluciones);
        hashMap.put("Empresa_Intervenida", insertObject.getEmpresaIntervenida());
        hashMap.put("Empresa_Inteventor", insertObject.getEmpresaInterventor());
        hashMap.put("Estado", insertObject.getEstado());
        hashMap.put("Fecha_Hora", insertObject.getFechaHora());
        hashMap.put("Fecha_Registro", insertObject.getFechaRegistro());
        hashMap.put("Id_Plan_Trabajo", insertObject.getIdPlanTrabajo());
        hashMap.put("PlanTrabajo", insertObject.getIdPlanTrabajo());
        hashMap.put("IdUsuario", sharedPreferences.getInt("IdUsers", 0));
        hashMap.put("Interventor", sharedPreferences.getString("nombreUsuario", ""));


        hashMap.put("Tipo_Ubicacion", insertObject.getTipoUbicacion());  //insertObject.getLineaSubEstacion()
        hashMap.put("Linea_Subestacion", insertObject.getLineaSubEstacion());

        hashMap.put("LstControles", lstControlsMap);
        hashMap.put("LstDetalle", lstVerificacionesMap);

        hashMap.put("Lugar_Zona", insertObject.getLugarZona());
        hashMap.put("Nro_Plan_Trabajo", insertObject.getNroPlanTrabajo());
        hashMap.put("Supervisor", insertObject.getSupervisor());
        hashMap.put("Supervisor_Sustituto", insertObject.getSupervisorSustituto());


        hashMap.put("Tipo_Plan", 1);
        hashMap.put("Tipo_Registro", 2);
        hashMap.put("Origen_Registro", "App");
        hashMap.put("UsuariosNotificados", lstNotificacions);//lstNotificacions
        hashMap.put("LstImagenes", lstEvidenciaMap);
        hashMap.put("NombreFormato", "INTER-" + sharedPreferences.getString("rol", "") + sharedPreferences.getString("nombreUsuario", "") + insertObject.getFechaRegistro());


        if (NetworkUtil.isOnline(getApplicationContext())) {
            if (null != interventoria) {
                viewModel.editarInterventoria(hashMap).observe(this, new Observer<Respuesta>() {
                    @Override
                    public void onChanged(Respuesta respuesta) {
                        UtilMDP.hideProgreesDialog();
                        if (null != respuesta) {
                            if (respuesta.getEstatus()) {
                                Log.e("EDITAR INTER", " success : " + respuesta.getMessage());
                                finish();
                            }
                        }
                    }
                });
            } else {
                viewModel.insertarInterventoria(hashMap, getApplicationContext()).observe(this, new Observer<Respuesta>() {
                    @Override
                    public void onChanged(Respuesta respuesta) {
                        UtilMDP.hideProgreesDialog();
                        if (null != respuesta) {
                            if (respuesta.getEstatus()) {
                                Log.e("INSERT INT", " success : " + respuesta.getMessage());
                                finish();
                            }
                        }
                    }
                });
            }
        } else {
            crearInterventoriaOffline(lstControl, lstNotificacion);
        }
    }


    private void crearInterventoriaOffline(List<DetalleControlInterModel> lstControl, List<UsuarioNotificarEntity> lstNotificacion) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat dateFormatHour = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        CrearInterventoriaEntiy interventoriaEntiy = new CrearInterventoriaEntiy();
        interventoriaEntiy.setIdInterventoria(validarDetalle() ? interventoria.getData().getIdInterventoria() : 0);
        interventoriaEntiy.setActividad(insertObject.getActividad());
        interventoriaEntiy.setConclucion(insertObject.getConclucion());
        interventoriaEntiy.setEmpresaIntervenida(insertObject.getEmpresaIntervenida());
        interventoriaEntiy.setEmpresaInterventor(insertObject.getEmpresaInterventor());
        interventoriaEntiy.setEstado(insertObject.getEstado());
        interventoriaEntiy.setFechaHora(insertObject.getFechaHora());
        interventoriaEntiy.setFechaRegistro(insertObject.getFechaRegistro());
        interventoriaEntiy.setIdPlanTrabajo(insertObject.getIdPlanTrabajo());
        interventoriaEntiy.setPlanTrabajo(insertObject.getPlanTrabajo());
        interventoriaEntiy.setIdUsuario(sharedPreferences.getInt("IdUsers", 0));
        interventoriaEntiy.setInterventor(sharedPreferences.getString("nombreUsuario", ""));
        interventoriaEntiy.setNombreObservador(sharedPreferences.getString("nombreUsuario", ""));
        interventoriaEntiy.setLineaSubEstacion(insertObject.getLineaSubEstacion());
        interventoriaEntiy.setCodigoInterventoria("INTER - " + dateFormatHour.format(new Date()));
        interventoriaEntiy.setLugarZona(insertObject.getLugarZona());
        interventoriaEntiy.setNroPlanTrabajo(insertObject.getNroPlanTrabajo());
        interventoriaEntiy.setSupervisor(insertObject.getSupervisor());
        interventoriaEntiy.setSupervisorSustituto(insertObject.getSupervisorSustituto());
        interventoriaEntiy.setEstadoRegistroDB(Constante.ESTADO_CREADO);
        interventoriaEntiy.setTipoUbicacion(insertObject.getTipoUbicacion());
        interventoriaEntiy.setNombreFormato("INTER-" + sharedPreferences.getString("rol", "") + sharedPreferences.getString("nombreUsuario", "") + insertObject.getFechaRegistro());

        Long idRow = viewModel.crearInterventoriaOffline(interventoriaEntiy);

        if (idRow != null) {

            CrearVerificacionEntity verificaciones = null;
            List<DetalleVerificacionInterModel> data = obtenerVerificaciones();
            for (int x = 0; x < data.size(); x++) {
                verificaciones = new CrearVerificacionEntity();
                verificaciones.setIdInterventoriaDetalle(data.get(x).getIdInterventoriaDetalle() != null ? data.get(x).getIdInterventoriaDetalle() : 0);
                verificaciones.setIdVerificacionSubitem(data.get(x).getIdVerificacionSubitem());
                verificaciones.setIdVerificacionItem(data.get(x).getIdVerificacionItems());
                verificaciones.setIdVerificacion(data.get(x).getIdVerificacion());
                verificaciones.setCumple(data.get(x).getCumple());
                verificaciones.setIdInterventoria(validarDetalle() ? interventoria.getData().getIdInterventoria() : 0);
                verificaciones.setIdOffline(idRow);
                verificaciones.setDescripcion(data.get(x).getDescripcion());
                verificaciones.setDescripcionPadre(verificaciones.getDescripcionPadre());
                verificaciones.setDescripcionItem(verificaciones.getDescripcionItem());
                viewModel.crearVerificacionOffline(verificaciones);
            }

            CrearControlEntity crearControlEntity = null;
            for (int x = 0; x < lstControl.size(); x++) {
                crearControlEntity = new CrearControlEntity();
                crearControlEntity.setIdControlDetalle(lstControl.get(x).getIdControlDetalle() != null ? lstControl.get(x).getIdControlDetalle() : 0);
                crearControlEntity.setIdControlItem(lstControl.get(x).getIdControlItem());
                crearControlEntity.setIdControl(lstControl.get(x).getIdControl());
                crearControlEntity.setCantidad(lstControl.get(x).getCantidad() != null ? lstControl.get(x).getCantidad() : 0);
                crearControlEntity.setIdOffline(idRow);
                viewModel.crearControlOffline(crearControlEntity);
            }

            CrearEvidenciaInterEntity crearEvidenciaInterEntity = null;
            if (null != insertObject.getListEvidencia() && insertObject.getListEvidencia().size() > 0) {
                for (int x = 0; x < insertObject.getListEvidencia().size(); x++) {
                    crearEvidenciaInterEntity = new CrearEvidenciaInterEntity();
                    crearEvidenciaInterEntity.setIdInterventoriaImagen(insertObject.getListEvidencia().get(x).getIdInterventoriaImagen() != null ? insertObject.getListEvidencia().get(x).getIdInterventoriaImagen() : 0);
                    crearEvidenciaInterEntity.setIdInterventoria(validarDetalle() ? interventoria.getData().getIdInterventoria() : 0);
                    crearEvidenciaInterEntity.setImgBase64(insertObject.getListEvidencia().get(x).getImgBase64());
                    crearEvidenciaInterEntity.setDescripcion(insertObject.getListEvidencia().get(x).getDescripcion());
                    crearEvidenciaInterEntity.setFechaCreacion(dateFormat.format(new Date()));
                    crearEvidenciaInterEntity.setIdOffline(idRow);
                    viewModel.crearEvidenciaOffline(crearEvidenciaInterEntity);
                }
            }


            for (int x = 0; x < lstNotificacion.size(); x++) {
                NotificarEntity notificarEntity = null;
                if (lstNotificacion.get(x).getSelectEmail()) {
                    notificarEntity = new NotificarEntity();
                    notificarEntity.setIdUser(lstNotificacion.get(x).getIdUsers());
                    notificarEntity.setNombre(lstNotificacion.get(x).getNombreUsuario());
                    notificarEntity.setEmail(lstNotificacion.get(x).getEmailCorporativo());
                    notificarEntity.setIdModulo(Constante.MODULO_INTERVENTORIA);
                    notificarEntity.setIdOffline(idRow);
                    viewModel.insertNotificarOffline(notificarEntity);
                }
            }
            UtilMDP.hideProgreesDialog();
            showCreateRegistro("Estimado, tu registro se ha guardado con éxito. Cuando haya conexión a Internet se sincronizara con la web");
        } else {
            UtilMDP.hideProgreesDialog();
        }
    }

    public void showCreateRegistro(String mensaje) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Registro Guardado");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage(mensaje);
        alertDialogBuilder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                finish();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
