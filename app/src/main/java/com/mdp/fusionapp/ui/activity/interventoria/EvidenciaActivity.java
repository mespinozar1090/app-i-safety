package com.mdp.fusionapp.ui.activity.interventoria;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.model.BuenaPracticaModel;
import com.mdp.fusionapp.model.EvidenciaInterventoriaModel;
import com.mdp.fusionapp.network.response.interventoria.EvidenciaInterResponse;
import com.mdp.fusionapp.ui.activity.inspecciones.BuenaPracticaActivity;
import com.mdp.fusionapp.ui.adapter.BuenaPracticaAdapter;
import com.mdp.fusionapp.ui.adapter.interventoria.EvidenciaAdapter;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.viewModel.InterventoriaViewModel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class EvidenciaActivity extends AppCompatActivity implements View.OnClickListener {

    private EvidenciaAdapter adaptador;
    private RecyclerView recyclerview;
    private LinearLayoutManager linearLayout;
    private List<EvidenciaInterventoriaModel> lista;
    private Button btnGuardar;
    private static long mLastClickTime = 0;
    private static String nameImage = "";
    private static String prefijo = "";
    private static String pathDCIM = "";
    private Integer indextSelect;
    private static final int SELECT_IMAGE = 3;
    private InterventoriaViewModel viewModel;
    private Integer idInterventoria;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evidencia);
        setToolbar();
        initView();

    }

    private void initView() {
        viewModel = new ViewModelProvider(this).get(InterventoriaViewModel.class);
        recyclerview = findViewById(R.id.recyclerview);
        btnGuardar = findViewById(R.id.btnGuardar);
        linearLayout = new LinearLayoutManager(getApplicationContext());
        recyclerview.setLayoutManager(linearLayout);
        lista = new ArrayList<>();
        adaptador = new EvidenciaAdapter(getApplicationContext(), lista);
        recyclerview.setAdapter(adaptador);
        btnGuardar.setOnClickListener(this);

        lista = (List<EvidenciaInterventoriaModel>) getIntent().getSerializableExtra("lstEvidencia");
        idInterventoria = getIntent().getIntExtra("idInterventoria",0);
        if (lista.size() > 0) {
            adaptador.setLista(lista);
        }

        if(idInterventoria != 0){
            loadEvidencia();
        }

        onclickTable();
    }

    private void loadEvidencia() {
        viewModel.evidenciaInterventoria(idInterventoria).observe(this, new Observer<EvidenciaInterResponse>() {
            @Override
            public void onChanged(EvidenciaInterResponse evidenciaInterResponse) {
                if (null != evidenciaInterResponse) {
                    if (evidenciaInterResponse.getEstatus() && evidenciaInterResponse.getData().size() > 0) {
                        for (int x = 0; x < evidenciaInterResponse.getData().size(); x++) {
                            EvidenciaInterventoriaModel object = new EvidenciaInterventoriaModel();
                            object.setDescripcion(evidenciaInterResponse.getData().get(x).getDescripcion());
                            object.setIdInterventoriaImagen(evidenciaInterResponse.getData().get(x).getIdInterventoria());
                            object.setImgBase64(evidenciaInterResponse.getData().get(x).getImgBase64());
                            lista.add(object);
                        }
                        adaptador.setLista(lista);
                    }
                }
            }
        });
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_back, null);
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorBlanco));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_add:
                agregarObjecto();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void agregarObjecto() {
        EvidenciaInterventoriaModel model = new EvidenciaInterventoriaModel();
        model.setDescripcion("");
        model.setImgBase64("");
        adaptador.setObjecto(model);
    }

    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        switch (v.getId()) {
            case R.id.btnGuardar:
                obtenerData();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_inspeccion, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void obtenerData() {
        Intent intent = new Intent();
        intent.putExtra("lstEvidencia", (Serializable) adaptador.getLista());
        setResult(RESULT_OK, intent);
        finish();
    }


    public void utilizarCamara() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Uri mProcessingPhotoUri = UtilMDP.getImageFileCompat(getApplicationContext());
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mProcessingPhotoUri);
        } else {
            nameImage = prefijo + "_" + UtilMDP.getCustomDate("yyyyMMddHHmmss") + ".jpg";
            File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
            pathDCIM = storageDir.getAbsolutePath() + File.separator + nameImage;
            File file = new File(pathDCIM);
            Uri outputFileUri = Uri.fromFile(file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        }
        startActivityForResult(intent, UtilMDP.REQUEST_CAMERA_SEND_ALERT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == UtilMDP.REQUEST_CAMERA_SEND_ALERT) {
            if (resultCode == Activity.RESULT_OK) {
                Bitmap bitmapCapture = UtilMDP.onCaptureImageResult(data);
                setImageCapture(bitmapCapture);
            }
        } else if (requestCode == SELECT_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    try {
                        if (Build.VERSION.SDK_INT < 28) {
                            setImageCapture(MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData()));
                        } else {
                            Uri uri = data.getData();
                            ImageDecoder.Source source = ImageDecoder.createSource(this.getContentResolver(), uri);
                            setImageCapture(ImageDecoder.decodeBitmap(source));

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(EvidenciaActivity.this, "Canceled", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void setImageCapture(Bitmap bitmapCapture) {
        Bitmap bitmapCapturex = Bitmap.createScaledBitmap(bitmapCapture, 250, 250, true);
        View view = linearLayout.findViewByPosition(indextSelect);
        ImageView imgFoto = view.findViewById(R.id.imgCategoria);
        imgFoto.setImageBitmap(bitmapCapturex);

        Bitmap bitmap = ((BitmapDrawable) imgFoto.getDrawable()).getBitmap();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String encodeImage = "data:image/jpeg;base64," + Base64.encodeToString(b, Base64.DEFAULT);
        adaptador.setValorImg(indextSelect, encodeImage);
    }

    private void onclickTable() {
        adaptador.setOnItemClickListener((int positionL, View vL) -> {
            if (vL.getId() == R.id.imgCategoria) {
                indextSelect = positionL;
                showCaptureImagen();
                //utilizarCamara();
            }
        });
    }

    private void showCaptureImagen() {
        View viewAddProject = getLayoutInflater().inflate(R.layout.dialog_boottomsheet, null);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(true);
        alertDialog.setView(viewAddProject);

        LinearLayout llcamara = viewAddProject.findViewById(R.id.llcamara);
        LinearLayout llgaleria = viewAddProject.findViewById(R.id.llgaleria);

        final AlertDialog dialog = alertDialog.create();
        dialog.show();

        llcamara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utilizarCamara();
                dialog.dismiss();
            }
        });

        llgaleria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE);
                dialog.dismiss();
            }
        });
    }
}