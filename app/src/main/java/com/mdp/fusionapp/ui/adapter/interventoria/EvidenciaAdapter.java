package com.mdp.fusionapp.ui.adapter.interventoria;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.model.BuenaPracticaModel;
import com.mdp.fusionapp.model.EvidenciaInterventoriaModel;
import com.mdp.fusionapp.ui.adapter.BuenaPracticaAdapter;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class EvidenciaAdapter extends RecyclerView.Adapter<EvidenciaAdapter.ViewHolder> {

    private Context context;
    private List<EvidenciaInterventoriaModel> lista = new ArrayList<>();
    private static ClickListener clickListener;

    public EvidenciaAdapter(Context context, List<EvidenciaInterventoriaModel> lista) {
        this.context = context;
        this.lista = lista;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_evidencia, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.bind(lista.get(position), context);
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public void setObjecto(EvidenciaInterventoriaModel objecto) {
        lista.add(objecto);
        notifyDataSetChanged();
    }

    public void setLista(List<EvidenciaInterventoriaModel> lista) {
        this.lista = lista;
        notifyDataSetChanged();
    }

    public List<EvidenciaInterventoriaModel> getLista() {
        return lista;
    }

    public void setValorImg(int position, String imgBase64) {
        lista.get(position).setImgBase64(imgBase64);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        EditText edtMensaje;
        ImageView imgCategoria;

        public ViewHolder(@NonNull View v) {
            super(v);
            edtMensaje = v.findViewById(R.id.edtMensaje);
            imgCategoria = v.findViewById(R.id.imgCategoria);
        }

        public void bind(EvidenciaInterventoriaModel item, Context context) {
            edtMensaje.setText(item.getDescripcion());

            if (null != item.getImgBase64() && !item.getImgBase64().equalsIgnoreCase("")) {
                if (null != item.getImgBase64() && (item.getImgBase64().contains("base64"))) {
                    String base64 = item.getImgBase64();
                    String[] parts = base64.split("base64,");
                    String part1 = parts[0];
                    String part2 = parts[1];
                    imgCategoria.setImageBitmap(UtilMDP.decodeBase64(part2));
                } else {
                    Picasso.get().load(item.getImgBase64()).into(imgCategoria);
                }
            }


            imgCategoria.setOnClickListener(this);
            edtMensaje.addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    item.setDescripcion(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

        @Override
        public void onClick(View view) {
            clickListener.onItemClick(getAdapterPosition(), view);
        }
    }

    public interface ClickListener {
        void onItemClick(int postion, View v);
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        EvidenciaAdapter.clickListener = clickListener;
    }
}
