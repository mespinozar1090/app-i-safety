package com.mdp.fusionapp.ui.adapter.interventoria;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.interventoria.VerificacionSubItemEntity;

import java.util.ArrayList;
import java.util.List;

public class VerificacionItemInterAdapter extends RecyclerView.Adapter<VerificacionItemInterAdapter.ViewHolder> {

    private Context context;
    private List<VerificacionSubItemEntity> lista = new ArrayList<>();

    public VerificacionItemInterAdapter(Context context, List<VerificacionSubItemEntity> lista) {
        this.context = context;
        this.lista = lista;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_verificacion, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(lista.get(position),context);
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public void setLista(List<VerificacionSubItemEntity> lista) {
        this.lista = lista;
        notifyDataSetChanged();
    }

    public List<VerificacionSubItemEntity> getLista() {
        return lista;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtPregunda;
        RadioButton rbtS,rbtN,rbtNA;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtPregunda = itemView.findViewById(R.id.txtPregunda);
            rbtS = itemView.findViewById(R.id.rbtS);
            rbtN = itemView.findViewById(R.id.rbtN);
            rbtNA = itemView.findViewById(R.id.rbtNA);
            rbtNA.setChecked(false);
            rbtNA.setChecked(false);
            rbtNA.setChecked(false);
        }
        public void bind(VerificacionSubItemEntity item, Context context) {
            rbtNA.setChecked(true);
            Log.e("cumple"," - "+item.getCumple());
            switch (item.getCumple()){
                case 1:
                    item.setCumple(1);
                    rbtS.setChecked(true);
                    break;
                case 2:
                    item.setCumple(2);
                    rbtN.setChecked(true);
                    break;
                case 3:
                    item.setCumple(3);
                    rbtNA.setChecked(true);
                    break;
            }

            txtPregunda.setText(item.getDescripcion());
            rbtS.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item.setCumple(1);
                }
            });

            rbtN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item.setCumple(2);
                }
            });

            rbtNA.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item.setCumple(3);
                }
            });

        }

        @Override
        public void onClick(View v) {

        }
    }
}
