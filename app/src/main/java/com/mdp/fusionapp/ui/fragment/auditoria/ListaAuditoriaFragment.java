package com.mdp.fusionapp.ui.fragment.auditoria;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.auditoria.CrearAuditoriaEntity;
import com.mdp.fusionapp.database.entity.auditoria.CrearDetalleAudotiraEntity;
import com.mdp.fusionapp.model.AuditoriaModel;
import com.mdp.fusionapp.model.InspeccionModel;
import com.mdp.fusionapp.network.response.AuditoriaResponse;
import com.mdp.fusionapp.network.response.detalle.DetalleAuditoriaResponse;
import com.mdp.fusionapp.network.response.interventoria.InterventoriaDetalleResponse;
import com.mdp.fusionapp.ui.activity.auditoria.AuditoriaActivity;
import com.mdp.fusionapp.ui.activity.interventoria.InterventoriaActivity;
import com.mdp.fusionapp.ui.adapter.AuditoriaAdapter;
import com.mdp.fusionapp.utilitary.Constante;
import com.mdp.fusionapp.utilitary.NetworkUtil;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.viewModel.ListaAuditoriaViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ListaAuditoriaFragment extends Fragment {

    private ListaAuditoriaViewModel mViewModel;
    private LinearLayoutManager linearLayout;
    private RecyclerView recyclerView;
    private AuditoriaAdapter adaptador;
    private List<AuditoriaModel> lista;
    private SharedPreferences sharedPreferences;

    public static ListaAuditoriaFragment newInstance() {
        return new ListaAuditoriaFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lista_auditoria_fragment, container, false);
        mViewModel = new ViewModelProvider(this).get(ListaAuditoriaViewModel.class);
        setToolbar(view);
        setupView(view);
        return view;
    }

    private void setupView(View view) {
        sharedPreferences = getActivity().getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        recyclerView = view.findViewById(R.id.recyclerview);
        linearLayout = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayout);
        lista = new ArrayList<>();
        adaptador = new AuditoriaAdapter(getContext(), lista);
        recyclerView.setAdapter(adaptador);

        onclickTable();
    }

    @Override
    public void onResume() {
        super.onResume();
        obtenerLista();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    private void onclickTable() {
        adaptador.setOnItemClickListener((int positionL, View vL) -> {
            if (vL.getId() == R.id.cardRoot) {
                loadDetalle(adaptador.getAuditoria(positionL).getIdAudit(), adaptador.getAuditoria(positionL).getIdRow());
            }
        });
    }

    private void obtenerLista() {
        if (NetworkUtil.isOnline(getContext())) {
            HashMap<String, Integer> hashMap = new HashMap<>();
            hashMap.put("Id_Usuario", sharedPreferences.getInt("IdUsers", 0));
            hashMap.put("Id_Rol_Usuario", sharedPreferences.getInt("Id_Rol_Usuario", 0));
            hashMap.put("Id_Rol_General", sharedPreferences.getInt("Id_Rol_General", 0));
            mViewModel.listaAuditoria(hashMap, getContext()).observe(getActivity(), new Observer<List<AuditoriaResponse>>() {
                @Override
                public void onChanged(List<AuditoriaResponse> auditoriaResponses) {
                    lista.clear();
                    if (null != auditoriaResponses) {
                        for (AuditoriaResponse data : auditoriaResponses) {
                            lista.add(new AuditoriaModel(null, data.getIdAudit(), data.getFechaReg(), data.getCodigoAudit(), data.getNombreFormato()));
                        }
                        adaptador.setAuditoria(lista);
                    }
                }
            });
        } else {
            List<CrearAuditoriaEntity> list = mViewModel.findAllOffline();
            lista.clear();
            if (null != list) {
                for (CrearAuditoriaEntity data : list) {
                    lista.add(new AuditoriaModel(data.getId(), data.getIdAuditoria(), data.getFechaAuditoria(), data.getCodigoAuditoria(), data.getNombreFormato()));
                }
                adaptador.setAuditoria(lista);
            }
        }

    }

    private void loadDetalle(Integer idAuditoria, Integer idOffline) {
        Log.e("idAuditoria", "idOffline" + idOffline);
        UtilMDP.showProgressDialog(getActivity(), "Obteniendo detalle...");
        if (NetworkUtil.isOnline(getContext())) {
            mViewModel.obtenerRegistroAuditoria(idAuditoria).observe(getActivity(), new Observer<DetalleAuditoriaResponse>() {
                @Override
                public void onChanged(DetalleAuditoriaResponse detalleAuditoriaResponse) {
                    UtilMDP.hideProgreesDialog();
                    if (null != detalleAuditoriaResponse) {
                        UtilMDP.hideProgreesDialog();
                        Intent intent = new Intent(getActivity(), AuditoriaActivity.class);
                        intent.putExtra("auditoria", detalleAuditoriaResponse);
                        startActivity(intent);
                    }
                }
            });
        } else {
            CrearAuditoriaEntity auditoria = mViewModel.findById(idOffline);

            if (auditoria != null) {
                DetalleAuditoriaResponse response = new DetalleAuditoriaResponse();
                response.setIdRow(auditoria.getId());
                response.setIdAudit(auditoria.getIdAuditoria());
                response.setIdEmpresaContratista(auditoria.getIdEmpresaContratista());
                response.setSede(auditoria.getSede());
                response.setNroContrato(auditoria.getNroContrato());
                response.setSupervisorContrato(auditoria.getSupervisorContrato());
                response.setResponsableContratista(auditoria.getResponsableContratista());
                response.setFechaAudit(auditoria.getFechaAuditoria());
                response.setCodigoAudit(auditoria.getCodigoAuditoria());
                response.setEstadoAudit(auditoria.getEstadoAuditoria());
                response.setNombreAutor(auditoria.getNombreAuditor());
                response.setFechaReg(auditoria.getFechaRegistro());

                List<CrearDetalleAudotiraEntity> detallex = mViewModel.findDetailByAuditoria(Long.valueOf(auditoria.getId()));
                List<DetalleAuditoriaResponse.DetalleAuditoria> lstDetalleAudit = new ArrayList<>();
                if (detallex != null && detallex.size() > 0) {
                    DetalleAuditoriaResponse.DetalleAuditoria detalleAuditoria = null;

                    List<DetalleAuditoriaResponse.EnvidenciaAuditoria> detalleAuditoriaImages = new ArrayList<>();
                    DetalleAuditoriaResponse.EnvidenciaAuditoria envidenciaAuditoria = null;
                    for (int x = 0; x < detallex.size(); x++) {
                        detalleAuditoria = new DetalleAuditoriaResponse.DetalleAuditoria();
                        detalleAuditoria.setIdAudit(detallex.get(x).getIdAudit());
                        detalleAuditoria.setIdAuditDetalle(detallex.get(x).getIdAuditDetalle());
                        detalleAuditoria.setIdAuditItems(detallex.get(x).getIdAuditItems());
                        detalleAuditoria.setIdAuditLinSubItems(detallex.get(x).getIdAuditLinSubItems());
                        detalleAuditoria.setCalificacion(detallex.get(x).getCalificacion());
                        detalleAuditoria.setLugar(detallex.get(x).getLugar());
                        detalleAuditoria.setNotas(detallex.get(x).getNotas());
                        detalleAuditoria.setTextEvidencia(detallex.get(x).getTextEvidencia());
                        detalleAuditoria.setDescripcionItem(detallex.get(x).getDescripcionItem());

                        if (detallex.get(x).getImagenEvidencia() != null && !detallex.get(x).getImagenEvidencia().equals("")) {
                            envidenciaAuditoria = new DetalleAuditoriaResponse.EnvidenciaAuditoria();
                            envidenciaAuditoria.setIdElemento(detallex.get(x).getIdElementoEvidencia());
                            envidenciaAuditoria.setImagen(detallex.get(x).getImagenEvidencia());
                            envidenciaAuditoria.setNombreImagen(detallex.get(x).getNombreImagenEvidencia());
                            detalleAuditoriaImages.add(envidenciaAuditoria);
                            Log.e("idAuditoria", "img64" + detallex.get(x).getImagenEvidencia());
                            detalleAuditoria.setLstEvidencia(detalleAuditoriaImages);
                        }
                        lstDetalleAudit.add(detalleAuditoria);
                    }
                }
                response.setLstDetalleAudit(lstDetalleAudit);

                UtilMDP.hideProgreesDialog();
                Intent intent = new Intent(getActivity(), AuditoriaActivity.class);
                intent.putExtra("auditoria", response);
                startActivity(intent);
            }


        }
    }

    private void setToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        final ActionBar ab = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_menu, null);
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorBlanco));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_inspeccion, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                DrawerLayout drawer = getActivity().findViewById(R.id.drawer_layout);
                drawer.openDrawer(Gravity.LEFT);
                break;
            case R.id.action_add:
                Intent intent = new Intent(getActivity(), AuditoriaActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
