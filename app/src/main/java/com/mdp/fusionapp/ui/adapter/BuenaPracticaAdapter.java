package com.mdp.fusionapp.ui.adapter;

import android.content.Context;
import android.media.Image;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.model.BuenaPracticaModel;
import com.mdp.fusionapp.utilitary.UtilMDP;

import java.util.ArrayList;
import java.util.List;

public class BuenaPracticaAdapter extends RecyclerView.Adapter<BuenaPracticaAdapter.ViewHolder> {

    private Context context;
    private List<BuenaPracticaModel> lista = new ArrayList<>();
    private static ClickListener clickListener;

    public BuenaPracticaAdapter(Context context, List<BuenaPracticaModel> list) {
        this.context = context;
        this.lista = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_buenapractica, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.bind(lista.get(position));
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public void setBuenaPracticaModel(List<BuenaPracticaModel> hallazgos) {
        this.lista = hallazgos;
        notifyDataSetChanged();
    }

    public void setValorImg(int position, String imgBase64) {
        lista.get(position).setImgBase64(imgBase64);
    }

    public BuenaPracticaModel getBuenaPracticaModel(int position) {
        return lista.get(position);
    }

    public List<BuenaPracticaModel> getLista() {
        return lista;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView title;
        TextView edtMensaje;
        LinearLayout lnlMensaje;
        ImageView imgFotoCategoria;
        ImageView btnEditar;

        public ViewHolder(@NonNull View v) {
            super(v);
            lnlMensaje = v.findViewById(R.id.lnlMensaje);
            title = v.findViewById(R.id.title);
            edtMensaje = v.findViewById(R.id.edtMensaje);
            imgFotoCategoria = v.findViewById(R.id.imgFotoCategoria);
            btnEditar = v.findViewById(R.id.btnEditar);
        }

        public void bind(BuenaPracticaModel item) {
            title.setText(item.getNombre());
            edtMensaje.setText(item.getDescripcion() != null ? item.getDescripcion() : "");
            if (null != item.getImgBase64() && !item.getImgBase64().equals("")) {
                imgFotoCategoria.setVisibility(View.VISIBLE);
            } else {
                imgFotoCategoria.setVisibility(View.GONE);
            }
            imgFotoCategoria.setOnClickListener(this);
            btnEditar.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            clickListener.onItemClick(getAdapterPosition(), view);
        }
    }

    public interface ClickListener {
        void onItemClick(int postion, View v);
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        BuenaPracticaAdapter.clickListener = clickListener;
    }
}
