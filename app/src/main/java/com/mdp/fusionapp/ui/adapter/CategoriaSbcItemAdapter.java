package com.mdp.fusionapp.ui.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.model.BarrerasModel;
import com.mdp.fusionapp.database.entity.CategoriaSbcItemEntity;
import com.mdp.fusionapp.utilitary.UtilMDP;

import java.util.ArrayList;
import java.util.List;

public class CategoriaSbcItemAdapter extends RecyclerView.Adapter<CategoriaSbcItemAdapter.ViewHolder> {

    private Context context;
    private List<CategoriaSbcItemEntity> lista = new ArrayList<>();
    private static ClickListener clickListener;
    static  String[] escrito;

    public CategoriaSbcItemAdapter(Context context, List<CategoriaSbcItemEntity> lista) {
        this.context = context;
        this.lista = lista;
        escrito = new String[lista.size()];
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_categoria_sbc, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(lista.get(position), context);
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public void setLista(List<CategoriaSbcItemEntity> lista) {
        this.lista = lista;
        notifyDataSetChanged();
    }

    public CategoriaSbcItemEntity getCategoriaSbcItemResponse(int position) {
        return lista.get(position);
    }

    public void setMesage(int position,String message){
        lista.get(position).setDescripcionItem(message);
    }

    public List<CategoriaSbcItemEntity> getLista() {
        return lista;
    }

    public String getEscrito(int position) {
        return escrito[position];
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtPregunda;
        RadioButton rbtS, rbtR,rbtNS;
        Spinner spBarreras;
        EditText edtMensaje;
        RelativeLayout idCombo;
        LinearLayout lnlMensaje;

        public ViewHolder(@NonNull View view) {
            super(view);
            txtPregunda = view.findViewById(R.id.txtPregunda);
            rbtS = view.findViewById(R.id.rbtS);
            rbtR = view.findViewById(R.id.rbtR);
            rbtNS = view.findViewById(R.id.rbtNS);
            spBarreras = view.findViewById(R.id.spBarreras);
            edtMensaje = view.findViewById(R.id.edtMensaje);
            idCombo = view.findViewById(R.id.idCombo);
            lnlMensaje = view.findViewById(R.id.lnlMensaje);
        }

        public void bind(CategoriaSbcItemEntity item, Context context) {

            item.setRiesgoso(item.getRiesgoso());
            item.setSeguro(item.getSeguro());

           if(item.getSeguro()){
               rbtS.setChecked(true);
               lnlMensaje.setVisibility(View.GONE);
               idCombo.setVisibility(View.GONE);
           } else if(item.getRiesgoso()){
               rbtR.setChecked(true);
               lnlMensaje.setVisibility(View.VISIBLE);
               idCombo.setVisibility(View.VISIBLE);
           } else  {
               item.setRiesgoso(false);
               item.setSeguro(false);
               Log.e("es N/S", " debe entrar ");
               rbtNS.setChecked(true);
               lnlMensaje.setVisibility(View.GONE);
               idCombo.setVisibility(View.GONE);
           }

            rbtR.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item.setRiesgoso(true);
                    item.setSeguro(false);
                    edtMensaje.setEnabled(true);
                    spBarreras.setEnabled(true);
                    lnlMensaje.setVisibility(View.VISIBLE);
                    idCombo.setVisibility(View.VISIBLE);
                }
            });

            rbtS.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item.setRiesgoso(false);
                    item.setSeguro(true);
                    edtMensaje.setText("");
                    edtMensaje.setEnabled(false);
                    spBarreras.setEnabled(false);
                    lnlMensaje.setVisibility(View.GONE);
                    idCombo.setVisibility(View.GONE);
                   // spBarreras.setSelection(13);
                }
            });

            rbtNS.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item.setRiesgoso(false);
                    item.setSeguro(false);
                    edtMensaje.setText("");
                    edtMensaje.setEnabled(false);
                    spBarreras.setEnabled(false);
                    lnlMensaje.setVisibility(View.GONE);
                    idCombo.setVisibility(View.GONE);
                }
            });

            edtMensaje.setText(item.getObservacionSbcDetalle());
            txtPregunda.setText(item.getDescripcionItem());

            ArrayAdapter<BarrerasModel> adapterTipoDocumento = new ArrayAdapter<>(context, R.layout.spinner_text, UtilMDP.obtenerBarrera());
            adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spBarreras.setAdapter(adapterTipoDocumento);
            spBarreras.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    BarrerasModel selectedItem = (BarrerasModel) parent.getSelectedItem();
                    item.setIdbarrera(selectedItem.getIdBarrera());
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    Log.e("onNothingSelected","Entroo?");
                  //  BarrerasModel selectedItem = (BarrerasModel) parent.getSelectedItem();
                 //   item.setIdbarrera(0);
                }
            });


            List<BarrerasModel> barrerasModels = UtilMDP.obtenerBarrera();
            for (int x=0; x<barrerasModels.size();x++){
                Log.e("idBarrera",""+barrerasModels.get(x).getIdBarrera().intValue()+" == "+item.getIdbarrera());
                if (barrerasModels.get(x).getIdBarrera().intValue() == item.getIdbarrera().intValue()) {
                    spBarreras.setSelection(x);
                }
            }

            edtMensaje.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    item.setObservacionSbcDetalle(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

        @Override
        public void onClick(View v) {
        }
    }

    public interface ClickListener {
        void onItemClick(int postion, View v);
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        CategoriaSbcItemAdapter.clickListener = clickListener;
    }
}
