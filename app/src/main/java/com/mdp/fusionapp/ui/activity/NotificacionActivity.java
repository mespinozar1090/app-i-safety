package com.mdp.fusionapp.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.UsuarioNotificarEntity;
import com.mdp.fusionapp.ui.adapter.NotificacionApdarter;
import com.mdp.fusionapp.viewModel.LoginViewModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class NotificacionActivity extends AppCompatActivity implements View.OnClickListener {

    private LoginViewModel loginViewModel;
    private NotificacionApdarter adaptador;
    private RecyclerView recyclerview;
    private LinearLayoutManager linearLayout;
    private List<UsuarioNotificarEntity> lista;
    private Button btnAgregar;
    private EditText etdBuscar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notificacion);
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        lista = new ArrayList<>();
        setToolbar();
        initView();
    }

    private void initView(){
        btnAgregar = findViewById(R.id.btnAgregar);
        recyclerview = findViewById(R.id.recyclerview);
        etdBuscar = findViewById(R.id.etdBuscar);
        linearLayout = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(linearLayout);
        adaptador = new NotificacionApdarter(this, lista);
        recyclerview.setAdapter(adaptador);
        lista = (List<UsuarioNotificarEntity>) getIntent().getSerializableExtra("lstNotificacion");
        filtrarLista();
        if(lista.size()>0){
            adaptador.setLista(lista);
        }else {
            obtenerUsuarioNotificar();
        }
        btnAgregar.setOnClickListener(this);
    }

    private void  filtrarLista(){
        etdBuscar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence editable, int i, int i1, int i2) {
                String aux = editable.toString().trim();
                if (!aux.equals("")) {
                    ArrayList<UsuarioNotificarEntity> newLista = new ArrayList<>();
                    for (UsuarioNotificarEntity usuarioMap : lista) {
                        String nombre = usuarioMap.getNombreUsuario();
                        if (nombre.toLowerCase().contains(aux.toLowerCase())) {
                            newLista.add(usuarioMap);
                        }
                    }
                    adaptador.setFilter(newLista);
                } else {
                    adaptador.setLista(lista);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_back, null);
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorBlanco));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void obtenerUsuarioNotificar(){
        loginViewModel.obtenerUsuarioNotificar().observe(this, new Observer<List<UsuarioNotificarEntity>>() {
            @Override
            public void onChanged(List<UsuarioNotificarEntity> usuarioNotiRespons) {
                if(null != usuarioNotiRespons){
                    lista.addAll(usuarioNotiRespons);
                    adaptador.setLista(usuarioNotiRespons);
                }
            }
        });
    }

    private void obtenerData() {
        Intent intent = new Intent();
        intent.putExtra("lstNotificacion", (Serializable) adaptador.getLista());
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnAgregar:
                obtenerData();
                break;
        }
    }
}