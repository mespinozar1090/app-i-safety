package com.mdp.fusionapp.ui.activity.inspecciones;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.ContratistaEntity;
import com.mdp.fusionapp.database.entity.CrearBuenaPracticaEntity;
import com.mdp.fusionapp.database.entity.CrearInspeccionEntity;
import com.mdp.fusionapp.database.entity.EmpresaMaestraEntity;
import com.mdp.fusionapp.database.entity.LugarEntity;
import com.mdp.fusionapp.database.entity.ProyectoEntity;
import com.mdp.fusionapp.database.entity.SedeProyectoEntity;
import com.mdp.fusionapp.database.entity.TipoUbicacionEntity;
import com.mdp.fusionapp.database.entity.UsuarioResponsableEntity;
import com.mdp.fusionapp.database.serviceImpl.ProyectoServiceImpl;
import com.mdp.fusionapp.model.BuenaPracticaModel;
import com.mdp.fusionapp.model.DetalleInspeccion;
import com.mdp.fusionapp.network.response.CreateInspeccionResponse;
import com.mdp.fusionapp.database.entity.UsuarioNotificarEntity;
import com.mdp.fusionapp.ui.activity.NotificacionActivity;
import com.mdp.fusionapp.ui.activity.UsuarioResponsableActivity;
import com.mdp.fusionapp.utilitary.Constante;
import com.mdp.fusionapp.utilitary.NetworkUtil;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.viewModel.InspeccionViewModel;
import com.mdp.fusionapp.viewModel.SbcViewModel;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CrearInspeccionActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int SECOND_ACTIVITY_REQUEST_CODE = 0;
    private static final int USUARIO_REQUEST_CODE = 10;
    private static final int NOTIFICACION_ACTIVITY_REQUEST_CODE = 999;
    private Long idRowInspeccion;
    private Toolbar toolbar;
    private Spinner spProyecto, spContratista, spTipoUbicacion, spLugar, spCategoriaPractica, spAreaInspeccionada;
    private ImageButton swCategoria;
    private InspeccionViewModel inspeccionViewModel;
    private LinearLayout lblTorre;
    private Button btnAceptar, btnUsuarioResponsable, btnFechaAndHora, btnHora, btnDate;
    private RadioButton rbtPlaneado, rbtNoPlaneado;
    private SharedPreferences sharedPreferences;
    private TextInputLayout tilresponsableArea, tilTorre;
    private TextInputEditText tietresponsableArea, tietTorre;
    private List<BuenaPracticaModel> lstBuenaPractica;
    private List<UsuarioNotificarEntity> lstNotificacion;
    //Variables
    private String areaInspeccionada, responsableArea;
    private Integer isPlaneado, idProyecto, idEmpresaObservadora;
    private Integer idLugar = null;
    private Integer idTipoUbicacion = null;
    private EmpresaMaestraEntity empresaProyecto;
    private DetalleInspeccion detalleInspeccion;
    private Integer idInspeccion = null;
    private String nombreLugar, fechaIncidente, horaIncidente, fechaIncidenteCreate;
    private Integer estadoInpeccion = 1;
    private TextView tvTipoUbicacion, tvLugar;
    private RelativeLayout rltLugar, rltUbicacion;
    private Integer idUsuaroResponsable;
    private String nombreUsuaroResponsable, nombreSede, nombreContratista;


    //TODO METODOS OFFLINE
    private ProyectoServiceImpl proyectoServiceImpl;
    private SbcViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_inspeccion);
        inspeccionViewModel = new ViewModelProvider(this).get(InspeccionViewModel.class);
        proyectoServiceImpl = new ViewModelProvider(this).get(ProyectoServiceImpl.class);
        mViewModel = new ViewModelProvider(this).get(SbcViewModel.class);
        setupView();
    }

    private void setupView() {

        detalleInspeccion = (DetalleInspeccion) getIntent().getSerializableExtra("detalleInspeccion");
        sharedPreferences = getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        tilresponsableArea = findViewById(R.id.tilresponsableArea);
        tvTipoUbicacion = findViewById(R.id.tvTipoUbicacion);
        tilresponsableArea = findViewById(R.id.tilresponsableArea);
        tilTorre = findViewById(R.id.tilTorre);
        btnUsuarioResponsable = findViewById(R.id.btnUsuarioResponsable);
        btnUsuarioResponsable.setOnClickListener(this);
        lstBuenaPractica = new ArrayList<>();
        tvLugar = findViewById(R.id.tvLugar);
        tietTorre = findViewById(R.id.tietTorre);
        rltLugar = findViewById(R.id.rltLugar);
        rltUbicacion = findViewById(R.id.rltUbicacion);
        btnFechaAndHora = findViewById(R.id.btnFecha);
        btnDate = findViewById(R.id.btnDate);
        btnHora = findViewById(R.id.btnHora);

        tietresponsableArea = findViewById(R.id.tietresponsableArea);
        lblTorre = findViewById(R.id.lblTorre);
        rbtPlaneado = findViewById(R.id.rbtPlaneado);
        rbtPlaneado.setChecked(true);
        isPlaneado = 3;

        rbtNoPlaneado = findViewById(R.id.rbtNoPlaneado);
        btnAceptar = findViewById(R.id.btnAceptar);
        spProyecto = findViewById(R.id.spProyecto);
        spContratista = findViewById(R.id.spContratista);
        spTipoUbicacion = findViewById(R.id.spTipoUbicacion);
        spLugar = findViewById(R.id.spLugar);
        swCategoria = findViewById(R.id.swCategoria);
        spAreaInspeccionada = findViewById(R.id.spAreaInspeccionada);
        lstNotificacion = new ArrayList<>();

        btnDate.setOnClickListener(this);
        btnHora.setOnClickListener(this);
        btnAceptar.setOnClickListener(this);
        rbtPlaneado.setOnClickListener(this);
        rbtNoPlaneado.setOnClickListener(this);
        setHorayFecha();
        tietTorre.setText("");
        tietresponsableArea.setText("");

        if (validarObject()) {
            tietresponsableArea.setText(detalleInspeccion.getResponsableAreaInspeccion());
            rbtPlaneado.setChecked(detalleInspeccion.getTipoInspeccion() == 3);
            rbtNoPlaneado.setChecked(detalleInspeccion.getTipoInspeccion() == 1);
            isPlaneado = detalleInspeccion.getTipoInspeccion();
            lstBuenaPractica = detalleInspeccion.getLstBuenasPracticas();
            idInspeccion = detalleInspeccion.getIdInspecciones();
            tietTorre.setText(detalleInspeccion.getTorre() != null ? detalleInspeccion.getTorre() : "");
            btnFechaAndHora.setText(detalleInspeccion.getFechaHoraInspeccion());
            editable();
        }

        setToolbar(validarObject() ? "Editar Inspección" : "Crear Inspección");
        swCategoria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CrearInspeccionActivity.this, (BuenaPracticaActivity.newInstances(lstBuenaPractica)).getClass());
                //   intent.putExtra("lista", (Serializable) lstBuenaPractica);
                intent.putExtra("editar", validarObject() ? 1 : 0);
                startActivityForResult(intent, SECOND_ACTIVITY_REQUEST_CODE);
            }
        });


        loadSpinner();
        // loadAreaInspecionada();
        loadUsuario();
        // loadCategoriaBuenaPractica();
    }

    private void editable() {
        if (detalleInspeccion.getEstadoInspeccion() == 2) {
            tietresponsableArea.setEnabled(false);
            rbtPlaneado.setEnabled(false);
            rbtNoPlaneado.setEnabled(false);
            tietTorre.setEnabled(false);
            spProyecto.setEnabled(false);
            spContratista.setEnabled(false);
            spTipoUbicacion.setEnabled(false);
            spLugar.setEnabled(false);
            swCategoria.setEnabled(false);
            btnAceptar.setEnabled(false);
            btnUsuarioResponsable.setEnabled(false);
        }
    }

    private void setHorayFecha() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat dateFormatCreate = new SimpleDateFormat("MM/dd/yyyy");
        fechaIncidenteCreate = dateFormatCreate.format(new Date());
        Log.e("fechaIncidenteCreate", "" + fechaIncidenteCreate);

        SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm");
        fechaIncidente = dateFormat.format(new Date());
        horaIncidente = hourFormat.format(new Date());
        btnFechaAndHora.setText(fechaIncidente + " " + horaIncidente);
    }

    private void setFechaAndHora(String fechaIncidente, String horaIncidente) {
        btnFechaAndHora.setText(fechaIncidente + " " + horaIncidente);
    }

    private void loadSpinner() {

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("Modulo", "Inspe");

        List<SedeProyectoEntity> list = mViewModel.obtenerSedeOffline(hashMap);
        if (null != list) {
            listProyecto(list);
            if (validarObject()) {
                for (int x = 0; x < list.size(); x++) {
                    if (detalleInspeccion.getIdProyecto().intValue() == list.get(x).getIdSedeProyecto()) {
                        spProyecto.setSelection(x);
                    }
                }
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SECOND_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                List<BuenaPracticaModel> modelList = (List<BuenaPracticaModel>) data.getSerializableExtra("oLista");
                if (modelList.size() > 0) {
                    lstBuenaPractica = modelList;
                    //swCategoria.setImageResource(R.drawable.ic_playlist);
                }
            }
        } else if (requestCode == NOTIFICACION_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                lstNotificacion = (List<UsuarioNotificarEntity>) data.getSerializableExtra("lstNotificacion");
            }
        } else if (requestCode == USUARIO_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                UsuarioResponsableEntity usuario = (UsuarioResponsableEntity) data.getSerializableExtra("object");
                btnUsuarioResponsable.setText(usuario.getNombreUsuario() + " " + usuario.getApellidoUsuario());
                nombreUsuaroResponsable = usuario.getNombreUsuario() + " " + usuario.getApellidoUsuario();
                idUsuaroResponsable = usuario.getIdUsuario();
            }
        }
    }

    private void listProyecto(List<SedeProyectoEntity> proyectoResponses) {

        ArrayAdapter<SedeProyectoEntity> adapterTipoDocumento = new ArrayAdapter<>(this, R.layout.spinner_text, proyectoResponses);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spProyecto.setAdapter(adapterTipoDocumento);
        spProyecto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SedeProyectoEntity proyectoModel = (SedeProyectoEntity) parent.getSelectedItem();
                idProyecto = proyectoModel.getIdSedeProyecto();
                nombreSede = proyectoModel.getDescripcion();
                loadContratista(proyectoModel.getIdSedeProyecto());
                loadTipoUbicacion(proyectoModel.getIdSedeProyecto());
                empresaProyecto(proyectoModel.getIdSedeProyecto());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private Boolean validarObject() {
        return detalleInspeccion != null;
    }

    private void loadContratista(Integer idProyecto) {
        /*HashMap<String, Integer> hashMap = new HashMap<>();
        hashMap.put("idParam", idProyecto);
        hashMap.put("Id_Usuario", sharedPreferences.getInt("IdUsers", 0));
        hashMap.put("Id_Rol_Usuario", sharedPreferences.getInt("Id_Rol_Usuario", 0));
        hashMap.put("Id_Rol_General", sharedPreferences.getInt("Id_Rol_General", 0));*/
        List<ContratistaEntity> list = new ArrayList<>();
        list.add(new ContratistaEntity(0, "Seleccionar"));
        list.addAll(inspeccionViewModel.findContratistaByProyecto(idProyecto));
        listContratista(list);
        if (validarObject()) {
            for (int x = 0; x < list.size(); x++) {
                if (detalleInspeccion.getIdEmpresaContratista().intValue() == list.get(x).getIdEmpresaObservadora()) {
                    spContratista.setSelection(x);
                }
            }
        }


        /*inspeccionViewModel.obtenerContratista(hashMap, getApplicationContext()).observe(this, new Observer<List<ContratistaEntity>>() {
            @Override
            public void onChanged(List<ContratistaEntity> contratistaEntities) {
                List<ContratistaEntity> list = new ArrayList<>();
                list.add(new ContratistaEntity(0, "Seleccionar"));
                list.addAll(contratistaEntities == null ? inspeccionViewModel.findContratistaByProyecto(idProyecto) : contratistaEntities);
                if (null != list) {
                    listContratista(list);
                    if (validarObject()) {
                        for (int x = 0; x < list.size(); x++) {
                            if (detalleInspeccion.getIdEmpresaContratista().intValue() == list.get(x).getIdEmpresaObservadora()) {
                                spContratista.setSelection(x);
                            }
                        }
                    }
                }
            }
        });*/
    }

    private void listContratista(List<ContratistaEntity> contratistaResponses) {
        ArrayAdapter<ContratistaEntity> adapterTipoDocumento = new ArrayAdapter<>(this, R.layout.spinner_text, contratistaResponses);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spContratista.setAdapter(adapterTipoDocumento);
        spContratista.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ContratistaEntity proyectoModel = (ContratistaEntity) parent.getSelectedItem();
                idEmpresaObservadora = proyectoModel.getIdEmpresaObservadora();
                nombreContratista = proyectoModel.getDescripcion();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void loadTipoUbicacion(Integer idProyecto) {
        HashMap<String, Integer> hashMap = new HashMap<>();
        hashMap.put("idProyecto", idProyecto);

        List<TipoUbicacionEntity> list = new ArrayList<>(); //tipoUbicacionResponses
        list.add(new TipoUbicacionEntity(0, "Seleccionar"));
        list.addAll(inspeccionViewModel.findTipoUbicacionByProyecto(idProyecto));
        if (null != list) {
            if (list.size() > 0) {
                rltLugar.setVisibility(View.VISIBLE);
                rltUbicacion.setVisibility(View.VISIBLE);
                tvTipoUbicacion.setVisibility(View.VISIBLE);
                tvLugar.setVisibility(View.VISIBLE);
                listTipoUbiccion(list);
            } else {
                rltLugar.setVisibility(View.GONE);
                rltUbicacion.setVisibility(View.GONE);
                tvTipoUbicacion.setVisibility(View.GONE);
                tvLugar.setVisibility(View.GONE);
            }
        } else {
            rltLugar.setVisibility(View.GONE);
            rltUbicacion.setVisibility(View.GONE);
            tvTipoUbicacion.setVisibility(View.GONE);
            tvLugar.setVisibility(View.GONE);
            Log.e("INGRESO", " : no tiene lugar");
        }

       /* inspeccionViewModel.tipoUbicacion(hashMap, getApplicationContext()).observe(this, new Observer<List<TipoUbicacionEntity>>() {
            @Override
            public void onChanged(List<TipoUbicacionEntity> tipoUbicacionResponses) {

        });*/
    }

    private void listTipoUbiccion(List<TipoUbicacionEntity> tipoUbicacionResponse) {
        ArrayAdapter<TipoUbicacionEntity> adapterTipoDocumento = new ArrayAdapter<>(this, R.layout.spinner_text, tipoUbicacionResponse);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTipoUbicacion.setAdapter(adapterTipoDocumento);
        spTipoUbicacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TipoUbicacionEntity object = (TipoUbicacionEntity) parent.getSelectedItem();
                loadLugar(object.getIdTipoUbicacion());
                idTipoUbicacion = object.getIdTipoUbicacion();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        if (validarObject()) {
            for (int x = 0; x < tipoUbicacionResponse.size(); x++) {
                if (detalleInspeccion.getIdTipoUbicacion().intValue() == tipoUbicacionResponse.get(x).getIdTipoUbicacion()) {
                    spTipoUbicacion.setSelection(x);
                }
            }
        }
    }

    private void loadLugar(Integer idTipoUbicacion) {
        HashMap<String, Integer> hashMap = new HashMap<>();
        hashMap.put("idTipoUbicacion", idTipoUbicacion);
        if (NetworkUtil.isOnline(getApplicationContext())) {
            inspeccionViewModel.obtenerLugar(hashMap, getApplicationContext()).observe(this, new Observer<List<LugarEntity>>() {
                @Override
                public void onChanged(List<LugarEntity> lugarResponses) { //lugarResponses
                    List<LugarEntity> list = new ArrayList<>(); //tipoUbicacionResponses
                    list.add(new LugarEntity(0, "Seleccionar"));
                    list.addAll(lugarResponses);
                    if (null != list) {
                        listLugar(list);
                        if (validarObject()) {
                            for (int x = 0; x < list.size(); x++) {
                                if (detalleInspeccion.getIdLugar().intValue() == list.get(x).getIdLugar()) {
                                    spLugar.setSelection(x);
                                }
                            }
                        }
                    }
                }
            });
        } else {
            List<LugarEntity> list = new ArrayList<>(); //tipoUbicacionResponses
            list.add(new LugarEntity(0, "Seleccionar"));
            list.addAll(inspeccionViewModel.findLugarByUbicacion(idTipoUbicacion));
            if (null != list) {
                listLugar(list);
                if (validarObject()) {
                    for (int x = 0; x < list.size(); x++) {
                        if (detalleInspeccion.getIdLugar().intValue() == list.get(x).getIdLugar()) {
                            spLugar.setSelection(x);
                        }
                    }
                }
            }
        }

    }

    private void listLugar(List<LugarEntity> lugarResponses) {
        ArrayAdapter<LugarEntity> adapterTipoDocumento = new ArrayAdapter<>(this, R.layout.spinner_text, lugarResponses);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spLugar.setAdapter(adapterTipoDocumento);
        spLugar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                LugarEntity proyectoModel = (LugarEntity) parent.getSelectedItem();
                idLugar = proyectoModel.getIdLugar();
                nombreLugar = proyectoModel.getNombre();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void empresaProyecto(Integer idProyecto) {
        HashMap<String, Integer> hashMap = new HashMap<>();
        hashMap.put("idParam", idProyecto);
        if (NetworkUtil.isOnline(getApplicationContext())) {
            inspeccionViewModel.empresaProyecto(hashMap, getApplicationContext()).observe(this, new Observer<EmpresaMaestraEntity>() {
                @Override
                public void onChanged(EmpresaMaestraEntity empresaMaestraEntity) {
                    empresaProyecto = empresaMaestraEntity;
                }
            });
        } else {
            empresaProyecto = inspeccionViewModel.findEmpresaMaestraByProyectoDB(idProyecto);
        }

    }

    private void loadUsuario() {
        if (validarObject()) {
            if (detalleInspeccion.getResponsableInspeccion() != null && !detalleInspeccion.getResponsableInspeccion().isEmpty()) {
                btnUsuarioResponsable.setText(detalleInspeccion.getResponsableInspeccion());
                nombreUsuaroResponsable = detalleInspeccion.getResponsableInspeccion();
                idUsuaroResponsable = detalleInspeccion.getIdUsuario();
            }
        } else {
            btnUsuarioResponsable.setText("Seleccionar");
        }
    }

    private void insertarInspeccion() {
        Date dateInspeccion = null;
        DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/aaaa");

        if (validarCampos()) {
            tilresponsableArea.clearFocus();
            UtilMDP.showProgressDialog(CrearInspeccionActivity.this, validarObject() ? "Editar Inspección" : "Crear Inspección");
            String nombreFormato = "Inspección" + "-" + sharedPreferences.getString("rol", "") + "-" + sharedPreferences.getString("usuario", "") + "-" + btnFechaAndHora.getText().toString(); //+ "-" + lstEmpresa[0].Descripcion;
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("Id_Inspecciones", idInspeccion);
            hashMap.put("idProyecto", idProyecto);
            hashMap.put("Id_Empresa_contratista", idEmpresaObservadora);
            hashMap.put("idTipoUbicacion", idTipoUbicacion);
            hashMap.put("idLugar", idLugar);
            hashMap.put("Torre", tietTorre.getText().toString());
            hashMap.put("Id_Empresa_Observadora", empresaProyecto.getIdEmpresaMaestra()); //
            hashMap.put("RUC", empresaProyecto.getRuc());
            hashMap.put("Id_Actividad_Economica", empresaProyecto.getIdActividadEconomica());
            hashMap.put("Domicilio_Legal", empresaProyecto.getDomicilioLegal());
            hashMap.put("Nro_Trabajadores", empresaProyecto.getNroTrabajadores());
            hashMap.put("Tipo_inspeccion", isPlaneado);
            hashMap.put("Fecha_Hora_Inspeccion", fechaIncidenteCreate);
            hashMap.put("Responsable_Inspeccion", nombreUsuaroResponsable); // responsableArea
            hashMap.put("Area_Inspeccionada", nombreLugar + " - " + tietTorre.getText().toString()); // como que envio miguel
            hashMap.put("Responsable_Area_Inspeccion", tietresponsableArea.getText().toString());

            hashMap.put("Objectivo_Inspeccion_Interna", Constante.OBJECTICO_INSPECION_INTERNA);
            hashMap.put("Resultado_Inspeccion", Constante.RESULTADO_INSPECCION);
            hashMap.put("Conclusiones_Recomendaciones", Constante.CONCLUSIONES_RECOMENTACIONES);
            hashMap.put("Origen_Registro", "App");

            hashMap.put("fecha_Hora_Inspeccion", btnFechaAndHora.getText().toString());
            //hashMap.put("UsuariosNotificados", lstNotificacions);
            hashMap.put("Estado_Inspeccion", estadoInpeccion);
            hashMap.put("Id_Usuario", idUsuaroResponsable); // idUsuaroResponsable
            hashMap.put("InspeccionInterna", 0);
            hashMap.put("nombreFormato", nombreFormato);
            hashMap.put("s_fecha_Hora_Inspeccion", btnFechaAndHora.getText().toString());
            hashMap.put("Id_Usuario_Registro", sharedPreferences.getInt("IdUsers", 0));

            if (NetworkUtil.isOnline(getApplicationContext())) {
                inspeccionViewModel.insertarInspeccion(hashMap).observe(this, new Observer<CreateInspeccionResponse>() {
                    @Override
                    public void onChanged(CreateInspeccionResponse response) {
                        if (null != response) {
                            if (!response.getId_Inspecciones().equals("")) {
                                saveBuenasPracticas(response.getId_Inspecciones());
                            }
                        }
                        UtilMDP.hideProgreesDialog();
                        finish();
                    }
                });
            } else {
                saveInspeccionOffline(nombreFormato);
            }
        }
    }

    private void saveInspeccionOffline(String nombreFormato) {
        CrearInspeccionEntity object = new CrearInspeccionEntity();

        object.setEstadoRegistroDB(Constante.ESTADO_CREADO);
        object.setIdProyecto(idProyecto);
        object.setIdEmpresacontratista(idEmpresaObservadora);
        object.setIdTipoUbicacion(idTipoUbicacion);
        object.setIdLugar(idLugar);
        object.setTorre(tietTorre.getText().toString());
        object.setIdEmpresaObservadora(empresaProyecto.getIdEmpresaMaestra());
        object.setRuc(empresaProyecto.getRuc());
        object.setIdActividadEconomica(empresaProyecto.getIdActividadEconomica());
        object.setDomicilioLegal(empresaProyecto.getDomicilioLegal());
        object.setNroTrabajadores(empresaProyecto.getNroTrabajadores());
        object.setTipoInspeccion(isPlaneado);
        object.setFechaHoraIncidenteCreate(fechaIncidenteCreate);
        object.setResponsableInspeccion(nombreUsuaroResponsable);
        object.setAreaInspeccionada(nombreLugar + " - " + tietTorre.getText().toString());
        object.setResponsableAreaInspeccion(tietresponsableArea.getText().toString());
        object.setFechaHoraInspeccion(btnFechaAndHora.getText().toString());
        object.setEstadoInspeccion(estadoInpeccion);
        object.setIdUsuario(idUsuaroResponsable);
        object.setNombreFormato(nombreFormato);
        object.setSfechaHoraInspeccion(btnFechaAndHora.getText().toString());
        object.setIdUsuarioRegistro(sharedPreferences.getInt("IdUsers", 0));
        object.setNombreSede(nombreSede);
        object.setNombreContratista(nombreContratista);
        object.setNombreUsuario(sharedPreferences.getString("nombreUsuario", ""));

        if (detalleInspeccion != null && detalleInspeccion.getIdRow() != null) {
            object.setId(detalleInspeccion.getIdRow());
            object.setEstadoRegistroDB(Constante.ESTADO_EDITADO);
        }

        Long idInspeccionOffline = inspeccionViewModel.insertInspeccionOffline(object);
        if (idInspeccionOffline != null) {
            saveBuenasPracticasOffline(idInspeccionOffline);
        }
        UtilMDP.hideProgreesDialog();
        showCreateRegistro("Estimado, tu registro se ha guardado con éxito. Cuando haya conexión a Internet se sincronizara con la web");
    }

    private void saveBuenasPracticasOffline(Long idInspeccionOffline) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
        CrearBuenaPracticaEntity object = null;
        if (lstBuenaPractica != null && lstBuenaPractica.size() > 0) {
            inspeccionViewModel.deleteBuenaPracticaffline(idInspeccionOffline);
            for (int x = 0; x < lstBuenaPractica.size(); x++) {
                object = new CrearBuenaPracticaEntity();
                object.setIdBuenaPractica(lstBuenaPractica.get(x).getIdBuenasPracticas());
                object.setIdProyecto(idProyecto);
                // object.setIdInspeccion(idInspeccion);
                object.setIdCategoriaBuenaPractica(lstBuenaPractica.get(x).getIdCategoriaBuenaPractica());
                object.setIdEmpresaContratista(empresaProyecto.getIdEmpresaMaestra());
                object.setDescripcion(lstBuenaPractica.get(x).getDescripcion());
                object.setFechaImg(dateFormat.format(new Date()) + " " + hourFormat.format(new Date()));
                object.setImgB64(lstBuenaPractica.get(x).getImgBase64());
                object.setIdInspeccionOffline(idInspeccionOffline);
                object.setNombreCategoria(lstBuenaPractica.get(x).getNombre());
                inspeccionViewModel.insertBuenaPracticaOffline(object);
            }
        }
    }

    private void saveBuenasPracticas(Integer idInspeccion) {
        List<Map<String, Object>> listhashMap = new ArrayList<Map<String, Object>>();
        HashMap<String, Object> hashMap = null;

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");

        if (lstBuenaPractica != null && lstBuenaPractica.size() > 0) {
            for (int x = 0; x < lstBuenaPractica.size(); x++) {
                hashMap = new HashMap<>();
                if (null != lstBuenaPractica.get(x).getImgBase64() & null != lstBuenaPractica.get(x).getDescripcion()) {
                    if (!lstBuenaPractica.get(x).getImgBase64().equals("") || !lstBuenaPractica.get(x).getDescripcion().equals("")) {
                        hashMap.put("idBuenasPracticas", lstBuenaPractica.get(x).getIdBuenasPracticas());
                        hashMap.put("idProyecto", idProyecto);
                        hashMap.put("Id_Inspecciones", idInspeccion);
                        hashMap.put("idCategoriaBuenaPractica", lstBuenaPractica.get(x).getIdCategoriaBuenaPractica());
                        hashMap.put("Id_Empresa_contratista", empresaProyecto.getIdEmpresaMaestra());
                        hashMap.put("Descripcion", lstBuenaPractica.get(x).getDescripcion());
                        hashMap.put("nombreImg", "");
                        hashMap.put("fechaImg", dateFormat.format(new Date()) + " " + hourFormat.format(new Date()));
                        hashMap.put("ImgB64", lstBuenaPractica.get(x).getImgBase64());
                        listhashMap.add(hashMap);
                    }
                }
            }
            inspeccionViewModel.insertarBuenaPractica(listhashMap);

        } else {
            UtilMDP.hideProgreesDialog();
            finish();
        }
    }


    private Boolean validarCampos() {
        if (idProyecto != null & idProyecto != 0) {
            if (idEmpresaObservadora != null & idEmpresaObservadora != 0) {
                if (idTipoUbicacion != null & idTipoUbicacion != 0) {
                    if (idLugar != null & idLugar != 0) {
                        if (!tietTorre.getText().toString().isEmpty()) {
                            if (!tietresponsableArea.getText().toString().isEmpty()) {
                                if (nombreUsuaroResponsable != null && !nombreUsuaroResponsable.isEmpty()) {
                                    return true;
                                } else {
                                    Toast.makeText(this, "Seleccione Responsable de registro", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                tilresponsableArea.setError(UtilMDP.REQUIRED_FIELD);
                                tietresponsableArea.requestFocus();
                            }
                        } else {
                            tilTorre.setError(UtilMDP.REQUIRED_FIELD);
                            tietTorre.requestFocus();
                        }
                    } else {
                        Toast.makeText(this, "Seleccione Lugar", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(this, "Seleccione Tipo ubicación", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(this, "Seleccione Empresa Contratista", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "Seleccione Sede / Proyecto", Toast.LENGTH_LONG).show();
        }

        return false;
    }

    private void setToolbar(String title) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView titulo = (TextView) toolbar.findViewById(R.id.tvDescription);
        titulo.setText(title);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_back, null);
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorBlanco));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.email_add) {
            Intent intent = new Intent(CrearInspeccionActivity.this, NotificacionActivity.class);
            intent.putExtra("lstNotificacion", (Serializable) lstNotificacion);
            startActivityForResult(intent, NOTIFICACION_ACTIVITY_REQUEST_CODE);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAceptar:
                validarNoficacion();
                break;
            case R.id.rbtPlaneado:
                boolean checkeds = ((RadioButton) view).isChecked();
                if (checkeds)
                    isPlaneado = 3;
                break;
            case R.id.rbtNoPlaneado:
                boolean checked = ((RadioButton) view).isChecked();
                if (checked)
                    isPlaneado = 1;
                break;
            case R.id.btnUsuarioResponsable:
                Intent intent = new Intent(getApplicationContext(), UsuarioResponsableActivity.class);
                startActivityForResult(intent, USUARIO_REQUEST_CODE);
                break;
            case R.id.btnDate:
                getFechaCalendar();
                break;
            case R.id.btnHora:
                getHoraCalendar();
                break;
        }
    }

    private void getFechaCalendar() {
        final Calendar calendario = Calendar.getInstance();
        int year = calendario.get(Calendar.YEAR);
        int month = calendario.get(Calendar.MONTH);
        int day = calendario.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, monthOfYear);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                SimpleDateFormat dateFormatCreate = new SimpleDateFormat("MM/dd/yyyy");
                dateFormatCreate.setTimeZone(cal.getTimeZone());
                String currentDateString2 = dateFormatCreate.format(cal.getTime());
                fechaIncidenteCreate = currentDateString2;
                Log.e("getFechaCalendar", "" + currentDateString2);

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                dateFormat.setTimeZone(cal.getTimeZone());
                String currentDateString = dateFormat.format(cal.getTime());
                fechaIncidente = currentDateString;
                setFechaAndHora(fechaIncidente, horaIncidente);
            }
        }, year, month, day);

        Date miFecha = calendario.getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        datePicker.getDatePicker().setMaxDate(convertDateToMillis(dateFormat.format(miFecha)));
        datePicker.show();
    }

    private Long convertDateToMillis(String givenDateString) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        long timeInMilliseconds = System.currentTimeMillis() - 1000;
        try {
            Date mDate = sdf.parse(givenDateString);
            timeInMilliseconds = mDate.getTime();
            System.out.println("Date in milliseconds: " + timeInMilliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeInMilliseconds;
    }

    private void getHoraCalendar() {
        final Calendar calendario = Calendar.getInstance();
        int hora = calendario.get(Calendar.HOUR_OF_DAY);
        int minuto = calendario.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String am_pm = "";
                Calendar datetime = Calendar.getInstance();
                datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                datetime.set(Calendar.MINUTE, minute);
                String strHrsToShow = (datetime.get(Calendar.HOUR) == 0) ? "12" : datetime.get(Calendar.HOUR) + "";
                horaIncidente = hourOfDay + ":" + minute;
                setFechaAndHora(fechaIncidente, strHrsToShow + ":" + datetime.get(Calendar.MINUTE));
            }
        }, hora, minuto, true);
        timePickerDialog.show();
    }

    private void validarNoficacion() {
        insertarInspeccion();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // MenuInflater inflater = getMenuInflater();
        //inflater.inflate(R.menu.menu_email, menu);
        return super.onCreateOptionsMenu(menu);
    }


    public void showCreateRegistro(String mensaje) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Información");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage(mensaje);
        alertDialogBuilder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                finish();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
