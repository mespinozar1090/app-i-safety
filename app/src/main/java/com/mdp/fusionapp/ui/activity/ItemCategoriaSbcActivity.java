package com.mdp.fusionapp.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.CategoriaSbcItemEntity;
import com.mdp.fusionapp.ui.adapter.CategoriaSbcItemAdapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ItemCategoriaSbcActivity extends AppCompatActivity implements View.OnClickListener {

    private CategoriaSbcItemAdapter adaptador;
    private RecyclerView recyclerview;
    private LinearLayoutManager linearLayout;
    private List<CategoriaSbcItemEntity> lista;
    private List<CategoriaSbcItemEntity> listaData;
    private Integer idCategoria;
    private Button btnGuardar;
    private long mLastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_categoria_sbc);
        initView();
        setToolbar();
        lista = (List<CategoriaSbcItemEntity>) getIntent().getSerializableExtra("listCategoriaItem");
        idCategoria = getIntent().getIntExtra("idCategoria", 0);
        listaFiltro(idCategoria);
    }

    private void listaFiltro(int idCategoria) {
        listaData = new ArrayList<>();
        listaData.clear();
        for (int x = 0; x < lista.size(); x++) {
            if (idCategoria == lista.get(x).getIdsbcCategoria()) {
                listaData.add(lista.get(x));
                Log.e("TAG", "Observacion_sbc_detalle2 "+lista.get(x).getObservacionSbcDetalle());
            }
        }
        Log.e("listaDataSIZE", " : " + listaData.size());
        adaptador.setLista(listaData);
    }

    private void initView() {
        recyclerview = findViewById(R.id.recyclerview);
        linearLayout = new LinearLayoutManager(getApplicationContext());
        recyclerview.setLayoutManager(linearLayout);
        lista = new ArrayList<>();
        adaptador = new CategoriaSbcItemAdapter(getApplicationContext(), lista);
        recyclerview.setAdapter(adaptador);

        btnGuardar = findViewById(R.id.btnGuardar);
        btnGuardar.setOnClickListener(this);
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_back, null);
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorBlanco));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void obtenerDataLista() {
        Intent intent = new Intent();
        intent.putExtra("oLista", (Serializable) adaptador.getLista());
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        switch (v.getId()) {
            case R.id.btnGuardar:
                obtenerDataLista();
                break;
        }
    }
}
