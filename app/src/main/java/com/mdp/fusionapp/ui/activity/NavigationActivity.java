package com.mdp.fusionapp.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.navigation.NavigationView;
import com.infideap.drawerbehavior.AdvanceDrawerLayout;
import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.controller.LoginController;
import com.mdp.fusionapp.database.repository.NotificarRepository;
import com.mdp.fusionapp.network.response.AsynInspeccionResponse;
import com.mdp.fusionapp.ui.fragment.incidente.IncidenteFragment;
import com.mdp.fusionapp.ui.fragment.inspeccion.InspeccionFragment;
import com.mdp.fusionapp.ui.fragment.interventoria.InterventoriaFragment;
import com.mdp.fusionapp.ui.fragment.auditoria.ListaAuditoriaFragment;
import com.mdp.fusionapp.ui.fragment.sbc.ListaSbcFragment;
import com.mdp.fusionapp.utilitary.NetworkUtil;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.viewModel.AsynAuditoriaViewModel;
import com.mdp.fusionapp.viewModel.AsynIncidenteViewModel;
import com.mdp.fusionapp.viewModel.AsyncInterventoriaViewModel;
import com.mdp.fusionapp.viewModel.AsynSbcViewModel;
import com.mdp.fusionapp.viewModel.AsyncInspeccionViewModel;
import com.mdp.fusionapp.viewModel.InspeccionViewModel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.HashMap;
import java.util.List;
import java.util.PrimitiveIterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class NavigationActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private AdvanceDrawerLayout drawer;
    private NavigationView navigationView;
    private FragmentManager fragmentManager;
    private ImageView imgFoto;
    private AppBarConfiguration mAppBarConfiguration;
    // private UsuarioModel loginResponse;
    private InspeccionViewModel inspeccionViewModel;
    private AsyncInspeccionViewModel asyncInspeccionViewModel;
    private AsynSbcViewModel asynSbcViewModel;
    private AsynAuditoriaViewModel asynAuditoriaViewModel;
    private AsyncInterventoriaViewModel asynInterventoriaViewModel;
    private TextView txtUsuario, txtEmpresa;
    private AsynIncidenteViewModel asynIncidenteViewModel;
    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        inspeccionViewModel = new ViewModelProvider(this).get(InspeccionViewModel.class);
        asynAuditoriaViewModel = new ViewModelProvider(this).get(AsynAuditoriaViewModel.class);
        asyncInspeccionViewModel = new ViewModelProvider(this).get(AsyncInspeccionViewModel.class);
        asynSbcViewModel = new ViewModelProvider(this).get(AsynSbcViewModel.class);
        asynInterventoriaViewModel = new ViewModelProvider(this).get(AsyncInterventoriaViewModel.class);
        asynIncidenteViewModel = new ViewModelProvider(this).get(AsynIncidenteViewModel.class);
        setupConfig();
        asynData();
    }

    private void asynData() {
        HashMap<String, Integer> hashMap = new HashMap<>();
        hashMap.put("Id_Usuario", sharedPreferences.getInt("IdUsers", 0));
        hashMap.put("Id_Rol_Usuario", sharedPreferences.getInt("Id_Rol_Usuario", 0));
        hashMap.put("Id_Rol_General", sharedPreferences.getInt("Id_Rol_General", 0));
        if (NetworkUtil.isOnline(getApplicationContext())) {


           // asyncInspeccionViewModel.sendDataToOnlineInspeccion(NavigationActivity.this);
           // asynInterventoriaViewModel.sendDataToOnlineInterventoria(NavigationActivity.this);
           // asynSbcViewModel.sendDataToOnlineSBC();
          //  asynAuditoriaViewModel.sendDataToOnlineAuditoria(NavigationActivity.this);
            asynIncidenteViewModel.sendDataToOnlineSBC(NavigationActivity.this);

            ExecutorService executorService = Executors.newSingleThreadExecutor();

            executorService.execute(new Runnable() {
                public void run() {
                     asyncInspeccionViewModel.sendDataToOnlineInspeccion(NavigationActivity.this);
                }
            });

            executorService.execute(new Runnable() {
                public void run() {
                    asynSbcViewModel.sendDataToOnlineSBC();
                }
            });

            executorService.execute(new Runnable() {
                public void run() {
                      asynAuditoriaViewModel.sendDataToOnlineAuditoria(NavigationActivity.this);
                }
            });


            executorService.execute(new Runnable() {
                public void run() {
                   asynInterventoriaViewModel.sendDataToOnlineInterventoria(NavigationActivity.this);
                    //  subProceso1(hashMap);
                    // subProceso2(hashMap);
                    Log.e("Asynchronous", "sendDataToOnlineAuditoria");
                }
            });

            /*executorService.execute(new Runnable() {
                public void run() {
                    asynIncidenteViewModel.dataOnlineToStorage(NavigationActivity.this);
                }
            });*/

            executorService.shutdown();

            //   subProceso2(hashMap);
            // subProceso3(hashMap);
        }
    }


    private void subProceso1(HashMap hashMap) {
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        asyncInspeccionViewModel.sendDataToOnlineInspeccion(NavigationActivity.this);
                    }
                });
            }
        }).start();
    }

    private void subProceso2(HashMap hashMap) {
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        asynSbcViewModel.sendDataToOnlineSBC();
                    }
                });
            }
        }).start();
    }

    private void subProceso3(HashMap hashMap) {
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        asynAuditoriaViewModel.sendDataToOnlineAuditoria(NavigationActivity.this);
                    }
                });
            }
        }).start();
    }

    private void setupConfig() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        drawer.openDrawer(Gravity.LEFT);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        View view = navigationView.getHeaderView(0);
        imgFoto = (ImageView) navigationView.findViewById(R.id.imgFoto);
        navigationView.setNavigationItemSelectedListener(this);
        drawer.setViewScale(Gravity.START, 0.9f);
        drawer.setRadius(Gravity.START, 35);
        drawer.setViewElevation(Gravity.START, 20);

        setFragment(new InspeccionFragment());
        initViews(view);

    }

    private void initViews(View view) {
        sharedPreferences = getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        // loginResponse = (UsuarioModel) getIntent().getSerializableExtra("usuarioModel");
        txtUsuario = view.findViewById(R.id.txtUsuario);
        txtEmpresa = view.findViewById(R.id.txtEmpresa);
        txtUsuario.setText("Bienvenido, " + sharedPreferences.getString("nombreUsuario", ""));
        txtEmpresa.setText(sharedPreferences.getString("rol", ""));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_inspeccion:
                setFragment(new InspeccionFragment());
                break;
            case R.id.nav_sbc:
                setFragment(new ListaSbcFragment());
                break;
            case R.id.nav_interventoria:
                setFragment(new InterventoriaFragment());
                break;
            case R.id.nav_auditoria:
                setFragment(new ListaAuditoriaFragment());
                break;
            case R.id.nav_incidente:
                setFragment(new IncidenteFragment());
                break;
            case R.id.nav_sesion:
                cerrarSesion();
                break;
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void cerrarSesion() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }


    public void setFragment(Fragment fragment) {
        if (null != fragment) {
            fragmentManager = getSupportFragmentManager();
            boolean existFragment = false;
            List<Fragment> fragments = fragmentManager.getFragments();
            if (fragments != null) {
                for (Fragment mFragment : fragments) {
                    if (mFragment != null && mFragment.isVisible()) {
                        if (mFragment.getClass().getName().equals(fragment.getClass().getSimpleName())) {
                            existFragment = true;
                        }
                    }
                }
            }
            if (!existFragment) {
                fragmentManager
                        .beginTransaction()
                        .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                        .replace(R.id.content_main, fragment)
                        .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(null).commit();
            } else {
                fragmentManager.beginTransaction().remove(fragment)
                        .replace(R.id.content_main, fragment)
                        .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(null).commit();
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (fragmentManager.getBackStackEntryCount() > 1) {
                if (null != fragmentManager.findFragmentByTag("Inspecciones") || null != fragmentManager.findFragmentByTag("DocumentosLegajosFragment")) {
                    fragmentManager.popBackStackImmediate();
                    fragmentManager.beginTransaction().commit();
                } else {
                    if (null != navigationView) {
                        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        navigationView.getMenu().getItem(0).setChecked(true);
                        setFragment(new InspeccionFragment());
                    }
                }
            } else {
                moveTaskToBack(true);
            }
        }
    }
}
