package com.mdp.fusionapp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.model.InspeccionModel;

import java.util.ArrayList;
import java.util.List;

public class InspeccionAdapter extends RecyclerView.Adapter<InspeccionAdapter.ViewHolder> {

    private Context context;
    private List<InspeccionModel> lista = new ArrayList<>();
    private static ClickListener clickListener;

    public InspeccionAdapter(Context context, List<InspeccionModel> lista) {
        this.context = context;
        this.lista = lista;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_inspeccion,parent,false);
        return  new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.bind(context,lista.get(position));
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public void setInspeccion(List<InspeccionModel> inspecciones){
        this.lista = inspecciones;
        notifyDataSetChanged();
    }
    public void setInspeccionFiltro(List<InspeccionModel> inspecciones){
        lista = new ArrayList<>();
        lista.addAll(inspecciones);
        notifyDataSetChanged();
    }

    public InspeccionModel getInspeccion(int positon){
        return lista.get(positon);
    }

    public void removeItem(int position){
        lista.remove(position);
        notifyItemRemoved(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView txtUsuario,txtSede,txtProyecto,txtEmpresaInspeccionada,txtFecha,txtEstado;
        private LinearLayout estadoTicket;
        private ConstraintLayout cardRoot;
        public RelativeLayout viewBackground, viewForeground;
        private ImageView btnBorrar;

        public ViewHolder(@NonNull View v) {
            super(v);
            txtUsuario = v.findViewById(R.id.txtUsuario);
            txtSede = v.findViewById(R.id.txtSede);
            txtProyecto = v.findViewById(R.id.txtProyecto);
            txtEmpresaInspeccionada = v.findViewById(R.id.txtEmpresaInspeccionada);
          //  txtFecha = v.findViewById(R.id.txtFecha);
            cardRoot = v.findViewById(R.id.cardRoot);
            txtEstado = v.findViewById(R.id.txtEstado);
            btnBorrar = v.findViewById(R.id.btnBorrar);
            estadoTicket = v.findViewById(R.id.estadoTicket);
            viewBackground =  itemView.findViewById(R.id.view_background);
            viewForeground = itemView.findViewById(R.id.view_foreground);
        }

        public void bind(Context context,InspeccionModel item){
            txtUsuario.setText(item.getNombreUsuario());
            txtSede.setText(item.getSede());
            txtProyecto.setText(item.getCodigo());
            txtEmpresaInspeccionada.setText(item.getEmpresaContratista());
           // txtFecha.setText(item.getFechaRegistro());
            txtEstado.setText(validarEstado(item.getEstado()));
            txtEstado.setTextColor(item.getEstado() == 2? ContextCompat.getColor(context, R.color.colorGrisDrak) : ContextCompat.getColor(context, R.color.naranjaRep));
            estadoTicket.setBackgroundColor(item.getEstado() == 2? ContextCompat.getColor(context, R.color.colorGrisDrak) : ContextCompat.getColor(context, R.color.naranjaRep));
            cardRoot.setOnClickListener(this);
            btnBorrar.setOnClickListener(this);
        }

        private String validarEstado(Integer estado){
            switch (estado){
                case 1:
                    return "Abierto";
                case 2:
                    return "Cerrado";
                case 3:
                    return "Borrador";
                case 4:
                    return "Offline";
            }
            return "";
        }

        @Override
        public void onClick(View view) {
            clickListener.onItemClick(getAdapterPosition(),view);
        }
    }

    public interface ClickListener{
        void onItemClick(int postion,View v);
    }

    public void setOnItemClickListener(ClickListener clickListener){
        InspeccionAdapter.clickListener = clickListener;
    }

    public void setOnLongClickListener(ClickListener clickListener){
        InspeccionAdapter.clickListener = clickListener;
    }


}
