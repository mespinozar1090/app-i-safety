package com.mdp.fusionapp.ui.fragment.interventoria;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mdp.fusionapp.R;
import com.mdp.fusionapp.model.EvidenciaInterventoriaModel;
import com.mdp.fusionapp.network.response.interventoria.InterventoriaDetalleResponse;
import com.mdp.fusionapp.ui.activity.interventoria.EvidenciaActivity;
import com.mdp.fusionapp.ui.activity.interventoria.InterventoriaActivity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ObservacionesFragment extends Fragment {

    private TextInputLayout tilObservacion;
    private TextInputEditText tietObservacion;
    private Button btnGuardar;
    private InterventoriaDetalleResponse interventoria;
    private List<EvidenciaInterventoriaModel> lstEvidencia;
    private static final int SECOND_ACTIVITY_REQUEST_CODE = 999;

    public static ObservacionesFragment newInstance(InterventoriaDetalleResponse detalle) {
        ObservacionesFragment fragment = new ObservacionesFragment();
        Bundle args = new Bundle();
        args.putSerializable("detalle", detalle);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            interventoria = (InterventoriaDetalleResponse) getArguments().getSerializable("detalle");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_observaciones, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        tilObservacion = view.findViewById(R.id.tilObservacion);
        tietObservacion = view.findViewById(R.id.tietObservacion);
        btnGuardar = view.findViewById(R.id.btnGuardar);
        lstEvidencia = new ArrayList<>();
        if (interventoria != null) {
            tietObservacion.setText(interventoria.getData().getConcluciones());
        }

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((InterventoriaActivity) getActivity()).insertConcluciones(tietObservacion.getText().toString(),lstEvidencia);
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_foto, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_foto:
                Intent intent = new Intent(getActivity(), EvidenciaActivity.class);
                intent.putExtra("lstEvidencia", (Serializable) lstEvidencia);
                if (null != interventoria && null != interventoria.getData().getIdInterventoria()) {
                    intent.putExtra("idInterventoria", interventoria.getData().getIdInterventoria());
                }
                startActivityForResult(intent, SECOND_ACTIVITY_REQUEST_CODE);
                break;
        }
        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SECOND_ACTIVITY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                lstEvidencia = (List<EvidenciaInterventoriaModel>) data.getSerializableExtra("lstEvidencia");
            }
        }
    }

}