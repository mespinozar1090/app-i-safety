package com.mdp.fusionapp.ui.fragment.sbc;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.mdp.fusionapp.R;
import com.mdp.fusionapp.model.ItemModel;
import com.mdp.fusionapp.model.registro.InsertSbcModel;
import com.mdp.fusionapp.database.entity.AreaTrabajoEntity;
import com.mdp.fusionapp.database.entity.EmpresaSbcEntity;
import com.mdp.fusionapp.database.entity.EspecialidadEntity;
import com.mdp.fusionapp.database.entity.SedeProyectoEntity;
import com.mdp.fusionapp.network.response.detalle.DetalleSbcResponse;
import com.mdp.fusionapp.ui.activity.sbc.CrearsbcActivity;
import com.mdp.fusionapp.utilitary.NetworkUtil;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.viewModel.InspeccionViewModel;
import com.mdp.fusionapp.viewModel.SbcViewModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class GeneralsbcFragment extends Fragment implements View.OnClickListener {

    private SbcViewModel mViewModel;
    private InspeccionViewModel inspeccionViewModel;

    private String fechaUso;
    DetalleSbcResponse detalleSbcResponse;
    private SharedPreferences sharedPreferences;
    //Observador
    TextInputEditText tietNombreObservador, tietCargo, tietLugarTrabajo;
    Spinner spSedeProyectoObservador;

    //Datos generales
    Button btnFecha, btnSiguiente;
    Spinner spEmpresa, spHorario, spTiempoExperiencia, spEspecialidad, spAreaTrabajo, spSedeProyecto;
    TextInputEditText tietactividad;

    //variable insert
    private Integer idSedeObservador, idTiempoExperiencia,
            idSedeDatoGenerales, idAreaTrabAjo,
            idEmpresObservado, idHorario, idEspecialidad;


    public static GeneralsbcFragment newInstance(DetalleSbcResponse detalle) {
        GeneralsbcFragment fragment = new GeneralsbcFragment();
        Bundle args = new Bundle();
        args.putSerializable("detalle", detalle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            detalleSbcResponse = (DetalleSbcResponse) getArguments().getSerializable("detalle");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_generalsbc, container, false);
        sharedPreferences = getActivity().getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        initView(view);
        return view;
    }

    private void initView(View view) {
        mViewModel = new ViewModelProvider(this).get(SbcViewModel.class);
        inspeccionViewModel = new ViewModelProvider(this).get(InspeccionViewModel.class);

        tietNombreObservador = view.findViewById(R.id.tietNombreObservador);
        tietCargo = view.findViewById(R.id.tietCargo);
        tietLugarTrabajo = view.findViewById(R.id.tietLugarTrabajo);

        tietNombreObservador.setText(sharedPreferences.getString("nombreUsuario", ""));
        spSedeProyectoObservador = view.findViewById(R.id.spSedeProyectoObservador);

        btnFecha = view.findViewById(R.id.btnFecha);
        spEmpresa = view.findViewById(R.id.spEmpresa);
        spHorario = view.findViewById(R.id.spHorario);
        spTiempoExperiencia = view.findViewById(R.id.spTiempoExperiencia);
        spEspecialidad = view.findViewById(R.id.spEspecialidad);
        tietactividad = view.findViewById(R.id.tietactividad);
        spAreaTrabajo = view.findViewById(R.id.spAreaTrabajo);
        spSedeProyecto = view.findViewById(R.id.spSedeProyecto);
        btnSiguiente = view.findViewById(R.id.btnSiguiente);

        btnFecha.setOnClickListener(this);
        btnSiguiente.setOnClickListener(this);

        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        fechaUso = dateFormat.format(date);
        btnFecha.setText(fechaUso);
        btnFecha.setTextSize(12);
        btnFecha.setTextColor(Color.BLACK);

        loadDataWS();
        loadHorrio();
        loadTiempoExperienca();

        loadDetalleSBC();
    }

    private void loadDetalleSBC() {
        if (validarDetalle()) {
            Log.e("Kugar",""+detalleSbcResponse.getLugarTrabajo());
            tietNombreObservador.setText(detalleSbcResponse.getNombreobservador() != null ? detalleSbcResponse.getNombreobservador() : "");
            tietactividad.setText(detalleSbcResponse.getActividadObservada() != null ? detalleSbcResponse.getActividadObservada() : "");
            tietCargo.setText(detalleSbcResponse.getCargoObservador() != null ? detalleSbcResponse.getCargoObservador() : "");
            tietLugarTrabajo.setText(detalleSbcResponse.getLugarTrabajo() != null ? detalleSbcResponse.getLugarTrabajo() : "");
            btnFecha.setText(detalleSbcResponse.getFecharegistro());
        }
    }

    private Boolean validarDetalle() {
        return detalleSbcResponse != null;
    }

    public InsertSbcModel saveDataGeneralSBC() {
        InsertSbcModel insertSbcModel = new InsertSbcModel();
        insertSbcModel.setNombreObservador(tietNombreObservador.getText().toString() != null ? tietNombreObservador.getText().toString() : "");

        insertSbcModel.setCargoObservador(tietCargo.getText().toString().trim());
        insertSbcModel.setLugarTrabajo(tietLugarTrabajo.getText().toString().trim());
        insertSbcModel.setFechaRegistro(fechaUso);
        insertSbcModel.setIdEmpresaObservadora(idEmpresObservado);
        insertSbcModel.setHorarioObservacion(idHorario);
        insertSbcModel.setTiempoExpObservada(idTiempoExperiencia);
        insertSbcModel.setEspecialidadObservado(idEspecialidad);
        insertSbcModel.setActividad(tietactividad.getText().toString() != null ? tietactividad.getText().toString() : "");
        insertSbcModel.setIdAreaTrabajo(idAreaTrabAjo);
        insertSbcModel.setDescripcionAreaObservada("");
        insertSbcModel.setIdSedeProyectoObservado(idSedeDatoGenerales);
        insertSbcModel.setIdSedeProyecto(idSedeObservador); //idSedeDatoGenerales
        return insertSbcModel;
    }

    private Boolean valiarCampos() {
        if (idSedeObservador == 0) {
            Toast.makeText(getContext(), "Seleccione Sede / Proyecto de Observador", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (idEmpresObservado == 0) {
            Toast.makeText(getContext(), "Seleccione empresa del observado", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (idTiempoExperiencia == 0) {
            Toast.makeText(getContext(), "Seleccione tiempo de experiencia", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (idEspecialidad == 0) {
            Toast.makeText(getContext(), "Seleccione especialidad del observado", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (tietactividad.getText().toString() == null || tietactividad.getText().toString().trim().equals("")) {
            Toast.makeText(getContext(), "Ingrese actividad de persona observada", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (idAreaTrabAjo == 0) {
            Toast.makeText(getContext(), "Seleccione área de trabajo", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (idSedeDatoGenerales == 0) {
            Toast.makeText(getContext(), "Seleccione Sede / Proyecto del observado", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void enablePager() {
        if (valiarCampos()) {
            ((CrearsbcActivity) getActivity()).disableScroll(false);
            ((CrearsbcActivity) getActivity()).setViewPager(2);
        }
    }

    private void loadDataWS() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("Modulo", "SBC");
        spinnerSede(mViewModel.obtenerSedeOffline(hashMap));
      /*  if(NetworkUtil.isOnline(requireContext())){
            mViewModel.obtenerSede(hashMap).observe(getActivity(), new Observer<List<SedeProyectoEntity>>() {
                @Override
                public void onChanged(List<SedeProyectoEntity> sedeResponses) {
                    if (null != sedeResponses) {
                        spinnerSede(sedeResponses);
                    }
                }
            });
        }else{
            spinnerSede(mViewModel.obtenerSedeOffline(hashMap));
        }*/


        mViewModel.obtenerEmpresaSBSResponse().observe(getActivity(), new Observer<List<EmpresaSbcEntity>>() {
            @Override
            public void onChanged(List<EmpresaSbcEntity> empresaSBSRespons) {
                if (null != empresaSBSRespons)
                    loadEmpresas(empresaSBSRespons);
            }
        });

        mViewModel.obtenerEspecialidad().observe(getActivity(), new Observer<List<EspecialidadEntity>>() {
            @Override
            public void onChanged(List<EspecialidadEntity> especialidadRespons) {
                if (null != especialidadRespons) {
                    loadEspecialidad(especialidadRespons);
                }
            }
        });

        inspeccionViewModel.obtenerAreaInspeccion().observe(getActivity(), new Observer<List<AreaTrabajoEntity>>() {
            @Override
            public void onChanged(List<AreaTrabajoEntity> areaInspeccionRespons) {
                if (null != areaInspeccionRespons) {
                    loadAreaInspecionada(areaInspeccionRespons);
                }
            }
        });
    }


    private void spinnerSede(List<SedeProyectoEntity> sedeResponses) {
        ArrayAdapter<SedeProyectoEntity> adapterTipoDocumento = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, sedeResponses);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSedeProyectoObservador.setAdapter(adapterTipoDocumento);
        spSedeProyectoObservador.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SedeProyectoEntity selectedItem = (SedeProyectoEntity) parent.getSelectedItem();
                idSedeObservador = selectedItem.getIdSedeProyecto();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        if (validarDetalle()) {
            for (int x = 0; x < sedeResponses.size(); x++) {
                Log.e("getIdSedeProyecto", " result1 = " + detalleSbcResponse.getIdSedeProyecto().intValue());
                if (detalleSbcResponse.getIdSedeProyecto().intValue() == sedeResponses.get(x).getIdSedeProyecto()) {
                    Log.e("getIdSedeProyecto", " result2 = " + detalleSbcResponse.getIdSedeProyecto().intValue());
                    spSedeProyectoObservador.setSelection(x);
                }
            }
        }

        spSedeProyecto.setAdapter(adapterTipoDocumento);
        spSedeProyecto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SedeProyectoEntity selectedItem = (SedeProyectoEntity) parent.getSelectedItem();
                idSedeDatoGenerales = selectedItem.getIdSedeProyecto();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        if (validarDetalle()) {
            for (int x = 0; x < sedeResponses.size(); x++) {
                Log.e("IdSedeProyectoObservado", " result1 = " + detalleSbcResponse.getIdSedeProyectoObservado().intValue());
                if (detalleSbcResponse.getIdSedeProyectoObservado().intValue() == sedeResponses.get(x).getIdSedeProyecto()) {
                    Log.e("IdSedeProyectoObservado", " result2 = " + detalleSbcResponse.getIdSedeProyectoObservado().intValue());
                    spSedeProyecto.setSelection(x);
                }
            }

        }
    }

    private void loadEmpresas(List<EmpresaSbcEntity> empresaSBSRespons) {
        ArrayAdapter<EmpresaSbcEntity> adapterTipoDocumento = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, empresaSBSRespons);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spEmpresa.setAdapter(adapterTipoDocumento);
        spEmpresa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                EmpresaSbcEntity selectedItem = (EmpresaSbcEntity) parent.getSelectedItem();
                idEmpresObservado = selectedItem.getIdEmpresaObservadora();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        if (validarDetalle()) {
            for (int x = 0; x < empresaSBSRespons.size(); x++) {
                if (detalleSbcResponse.getIdEmpresaObservadora().intValue() == empresaSBSRespons.get(x).getIdEmpresaObservadora()) {
                    spEmpresa.setSelection(x);
                }
            }
        }
    }

    private void loadEspecialidad(List<EspecialidadEntity> empresaSBSResponses) {
        ArrayAdapter<EspecialidadEntity> adapterTipoDocumento = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, empresaSBSResponses);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spEspecialidad.setAdapter(adapterTipoDocumento);
        spEspecialidad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                EspecialidadEntity selectedItem = (EspecialidadEntity) parent.getSelectedItem();
                idEspecialidad = selectedItem.getIdEspecialidad();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (validarDetalle()) {
            for (int x = 0; x < empresaSBSResponses.size(); x++) {
                if (detalleSbcResponse.getEspecialidadObservado().intValue() == empresaSBSResponses.get(x).getIdEspecialidad()) {
                    spEspecialidad.setSelection(x);
                }
            }
        }
    }

    private void loadHorrio() {
        List<ItemModel> horarios = new ArrayList<>();
        horarios.add(new ItemModel(0, "MAÑANA"));
        horarios.add(new ItemModel(1, "TARDE"));

        ArrayAdapter<ItemModel> adapterTipoDocumento = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, horarios);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spHorario.setAdapter(adapterTipoDocumento);
        spHorario.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ItemModel selectedItem = (ItemModel) parent.getSelectedItem();
                idHorario = selectedItem.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (validarDetalle()) {
            for (int x = 0; x < horarios.size(); x++) {
                if (detalleSbcResponse.getHorarioObservacion() != null) {
                    if (Integer.parseInt(detalleSbcResponse.getHorarioObservacion()) == horarios.get(x).getId()) {
                        spHorario.setSelection(x);
                    }
                }
            }
        }
    }

    private void loadTiempoExperienca() {
        List<ItemModel> listaTiempoExperiencia = new ArrayList<>();
        listaTiempoExperiencia.add(new ItemModel(0, "Seleccionar"));
        listaTiempoExperiencia.add(new ItemModel(1, "Menor a 2 años"));
        listaTiempoExperiencia.add(new ItemModel(2, "Entre 2 a 5 años"));
        listaTiempoExperiencia.add(new ItemModel(3, "Entre 6 y 11 años"));
        listaTiempoExperiencia.add(new ItemModel(4, "Mayor a 12 años"));

        ArrayAdapter<ItemModel> adapterTipoDocumento = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, listaTiempoExperiencia);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTiempoExperiencia.setAdapter(adapterTipoDocumento);
        spTiempoExperiencia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ItemModel selectedItem = (ItemModel) parent.getSelectedItem();
                idTiempoExperiencia = selectedItem.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (validarDetalle()) {
            Log.e("getTiempoexpobservada", "result = " + detalleSbcResponse.getTiempoexpobservada().intValue());
            for (int x = 0; x < listaTiempoExperiencia.size(); x++) {
                if (detalleSbcResponse.getTiempoexpobservada() != null) {
                    if (detalleSbcResponse.getTiempoexpobservada().intValue() == listaTiempoExperiencia.get(x).getId().intValue()) {
                        spTiempoExperiencia.setSelection(x);
                    }
                }
            }
        }

    }

    private void loadAreaInspecionada(List<AreaTrabajoEntity> empresaSBSResponses) {
        ArrayAdapter<AreaTrabajoEntity> adapterTipoDocumento = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, empresaSBSResponses);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spAreaTrabajo.setAdapter(adapterTipoDocumento);
        spAreaTrabajo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AreaTrabajoEntity selectedItem = (AreaTrabajoEntity) parent.getSelectedItem();
                idAreaTrabAjo = selectedItem.getIdAreainspeccionada();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (validarDetalle()) {
            Log.e("idAreaTrabAjoOBx", "idAreaTrabAjo = " + detalleSbcResponse.getIdAreaTrabajo().intValue());
            for (int x = 0; x < empresaSBSResponses.size(); x++) {
                if (detalleSbcResponse.getIdAreaTrabajo() != null) {
                    Log.e("Area", "" + detalleSbcResponse.getIdAreaTrabajo().intValue() + " == " + empresaSBSResponses.get(x).getIdAreainspeccionada());
                    if (detalleSbcResponse.getIdAreaTrabajo().intValue() == empresaSBSResponses.get(x).getIdAreainspeccionada()) {
                        Log.e("entroo?", " == " + empresaSBSResponses.get(x).getIdAreainspeccionada());
                        spAreaTrabajo.setSelection(x);
                    }
                }
            }
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnFecha:
                getFechaCalendar();
                break;
            case R.id.btnSiguiente:
                enablePager();
                break;
        }
    }

    private void getFechaCalendar() {
        final Calendar calendario = Calendar.getInstance();
        int year = calendario.get(Calendar.YEAR);
        int month = calendario.get(Calendar.MONTH);
        int day = calendario.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, monthOfYear);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                fechaUso = dateFormat1.format(cal.getTime());

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                dateFormat.setTimeZone(cal.getTimeZone());
                String currentDateString = dateFormat.format(cal.getTime());
                fechaUso = currentDateString;
                btnFecha.setText(currentDateString);
                btnFecha.setTextSize(12);
                btnFecha.setTextColor(Color.BLACK);
            }
        }, year, month, day);

        Date miFecha = calendario.getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        // datePicker.getDatePicker().setMinDate(convertDateToMillis(dateFormat.format(miFecha)));
        datePicker.show();
    }
}
