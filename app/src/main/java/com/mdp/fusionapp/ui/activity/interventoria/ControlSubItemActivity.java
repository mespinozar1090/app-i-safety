package com.mdp.fusionapp.ui.activity.interventoria;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.interventoria.ControlSubItemEntity;
import com.mdp.fusionapp.ui.adapter.interventoria.ControlPersonaItemAdapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ControlSubItemActivity extends AppCompatActivity implements View.OnClickListener {

    private ControlPersonaItemAdapter adaptador;
    private RecyclerView recyclerview;
    private LinearLayoutManager linearLayout;
    private List<ControlSubItemEntity> lista;
    private List<ControlSubItemEntity> listaFilter;
    private Integer idControl;
    private Button btnGuardar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control_sub_item);
        setToolbar();
        initView();
        idControl = getIntent().getIntExtra("idControl", 0);
        lista = (List<ControlSubItemEntity>) getIntent().getSerializableExtra("listaControlSubItem");
        listaFiltro(idControl);
    }

    private void initView() {
        recyclerview = findViewById(R.id.recyclerview);
        linearLayout = new LinearLayoutManager(getApplicationContext());
        recyclerview.setLayoutManager(linearLayout);
        lista = new ArrayList<>();
        adaptador = new ControlPersonaItemAdapter(getApplicationContext(), lista);
        recyclerview.setAdapter(adaptador);

        btnGuardar = findViewById(R.id.btnGuardar);
        btnGuardar.setOnClickListener(this);
    }

    private void listaFiltro(int idControl) {
        listaFilter = new ArrayList<>();
        Log.e("lista size:"," "+lista.size());
        for (int x = 0; x < lista.size(); x++) {
            if (idControl == lista.get(x).getIdControl()) {
                Log.e("idControl"," "+idControl+" == "+lista.get(x).getIdControl());
                listaFilter.add(lista.get(x));
            }
        }
        adaptador.setLista(listaFilter);
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_back, null);
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorBlanco));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void obtenerDataLista() {
        Intent intent = new Intent();
        intent.putExtra("oLista", (Serializable) adaptador.getLista());
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnGuardar:
                obtenerDataLista();
                break;
        }
    }
}
