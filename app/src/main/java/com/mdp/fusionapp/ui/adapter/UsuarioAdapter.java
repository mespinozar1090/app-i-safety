package com.mdp.fusionapp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.UsuarioResponsableEntity;

import java.util.ArrayList;
import java.util.List;

public class UsuarioAdapter extends RecyclerView.Adapter<UsuarioAdapter.ViewHolder> {

    private Context context;
    private List<UsuarioResponsableEntity> lista = new ArrayList<>();
    private static ClickListener clickListener;


    public UsuarioAdapter(Context context, List<UsuarioResponsableEntity> lista) {
        this.context = context;
        this.lista = lista;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_usuario_responsable, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.bin(lista.get(position));
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public void setLista(List<UsuarioResponsableEntity> lista) {
        this.lista = lista;
        notifyDataSetChanged();
    }

    public void setFilter(ArrayList<UsuarioResponsableEntity> newLista) {
        lista = new ArrayList<>();
        lista.addAll(newLista);
        notifyDataSetChanged();
    }

    public UsuarioResponsableEntity getObject(int position) {
        return lista.get(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView txtNombre, txtCorreo;
        private LinearLayout rootLayout;

        public ViewHolder(@NonNull View view) {
            super(view);
            txtNombre = view.findViewById(R.id.txtNombre);
            txtCorreo = view.findViewById(R.id.txtCorreo);
            rootLayout = view.findViewById(R.id.rootLayout);
        }

        public void bin(UsuarioResponsableEntity item) {
            txtNombre.setText(item.getNombreUsuario() + " " + item.getApellidoUsuario());
            txtCorreo.setText(item.getEmail());
            rootLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            clickListener.onItemClick(getAdapterPosition(), view);
        }
    }

    public interface ClickListener {
        void onItemClick(int postion, View v);
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        UsuarioAdapter.clickListener = clickListener;
    }
}
