package com.mdp.fusionapp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.CategoriaSbcEntity;

import java.util.ArrayList;
import java.util.List;

public class CategoriaSbcAdapter  extends  RecyclerView.Adapter<CategoriaSbcAdapter.ViewHolder>{

    private Context context;
    private List<CategoriaSbcEntity> lista = new ArrayList<>();
    private static ClickListener clickListener;

    public CategoriaSbcAdapter(Context context, List<CategoriaSbcEntity> lista) {
        this.context = context;
        this.lista = lista;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lineamiento, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(lista.get(position));
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public void setLista(List<CategoriaSbcEntity> lista) {
        this.lista = lista;
        notifyDataSetChanged();
    }
    public CategoriaSbcEntity getCategoriaSbc(int position){
        return  lista.get(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        TextView txtNombre;
        LinearLayout rootLayout;

        public ViewHolder(@NonNull View v) {
            super(v);
            txtNombre = v.findViewById(R.id.txtNombre);
            rootLayout = v.findViewById(R.id.rootLayout);
            rootLayout.setOnClickListener(this);
        }

        public void bind(CategoriaSbcEntity item) {
            txtNombre.setText(item.getDescripcionCategoria());
        }

        @Override
        public void onClick(View view) {
            clickListener.onItemClick(getAdapterPosition(),view);
        }
    }

    public interface ClickListener {
        void onItemClick(int postion, View v);
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        CategoriaSbcAdapter.clickListener = clickListener;
    }
}
