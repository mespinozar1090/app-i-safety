package com.mdp.fusionapp.ui.fragment.sbc;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.sbc.CrearCategoriaSbcEntity;
import com.mdp.fusionapp.database.entity.sbc.CrearSbcEntity;
import com.mdp.fusionapp.network.response.SbcResponse;
import com.mdp.fusionapp.network.response.detalle.DetalleSbcResponse;
import com.mdp.fusionapp.ui.activity.sbc.CrearsbcActivity;
import com.mdp.fusionapp.ui.adapter.SbcAdapter;
import com.mdp.fusionapp.utilitary.NetworkUtil;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.viewModel.SbcViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ListaSbcFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private SbcViewModel mViewModel;
    private LinearLayoutManager linearLayout;
    private RecyclerView recyclerView;
    private SbcAdapter adaptador;
    private List<SbcResponse> lista;
    private FragmentManager fragmentManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private SharedPreferences sharedPreferences;
    DetalleSbcResponse oDetalleSbc;

    public static ListaSbcFragment newInstance() {
        return new ListaSbcFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lista_sbc_fragment, container, false);
        sharedPreferences = getActivity().getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        setToolbar(view);
        setupView(view);
        return view;
    }

    private void setupView(View view) {
        mViewModel = new ViewModelProvider(this).get(SbcViewModel.class);
        recyclerView = view.findViewById(R.id.recyclerview);
        linearLayout = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayout);
        lista = new ArrayList<>();
        adaptador = new SbcAdapter(getContext(), lista);
        recyclerView.setAdapter(adaptador);

        swipeRefreshLayout = view.findViewById(R.id.swipeRefresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(this);
    }


    @Override
    public void onResume() {
        super.onResume();
        obtenerListaSBC();
    }

    private void setToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        final ActionBar ab = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_menu, null);
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorBlanco));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_inspeccion, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                DrawerLayout drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
                drawer.openDrawer(Gravity.LEFT);
                break;
            case R.id.action_add:
                Intent intent = new Intent(getActivity(), CrearsbcActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onclickTable() {
        adaptador.setOnItemClickListener((int positionL, View vL) -> {
            if (vL.getId() == R.id.cardRoot) {
                loadDetalle(adaptador.getSBC(positionL).getIdSbc(), adaptador.getSBC(positionL).getIdRowSbc());
            } else if (vL.getId() == R.id.btnBorrar) {
                showDialogEliminar(adaptador.getSBC(positionL).getIdSbc(), positionL);
            }
        });
    }

    private void loadDetalle(Integer idSbc, Long idOffline) {
        UtilMDP.showProgressDialog(getActivity(), "Obteniendo Detalle...");

        if (NetworkUtil.isOnline(getContext())) {
            mViewModel.obtenerRegistroSBC(idSbc, getContext()).observe(this, new Observer<DetalleSbcResponse>() {
                @Override
                public void onChanged(DetalleSbcResponse detalleSbcResponse) {
                    UtilMDP.hideProgreesDialog();
                    if (null != detalleSbcResponse) {
                        UtilMDP.hideProgreesDialog();
                        Intent intent = new Intent(getActivity(), CrearsbcActivity.class);
                        intent.putExtra("oDetalleSbc", detalleSbcResponse);
                        startActivity(intent);
                    }
                }
            });
        } else {
            List<DetalleSbcResponse.Categoria> lstDetalleSBC = new ArrayList<>();
            CrearSbcEntity entity = mViewModel.findBySbcOffline(idOffline);

            DetalleSbcResponse response = new DetalleSbcResponse();
            response.setIdRow(entity.getId());
            response.setIdsbc(entity.getIdSbc());
            response.setNombreobservador(entity.getNombreObservador());
            response.setLugarTrabajo(entity.getLugarTrabajo());
            response.setFecharegistro(entity.getFechaRegristo());
            response.setIdEmpresaObservadora(entity.getIdEmpresaObservadora());
            response.setHorarioObservacion(String.valueOf(entity.getHorarioObservacion()));
            response.setTiempoexpobservada(entity.getTiempoExpObservada());
            response.setEspecialidadObservado(entity.getEspecialidaObservado());
            response.setActividadObservada(entity.getActividadObservado());
            response.setIdAreaTrabajo(entity.getIdAreaTrabajo());
            response.setDescripcionAreaObservada(entity.getDescripcionAreaObservada());
            response.setIdUsuario(entity.getIdUsuario());
            response.setIdSedeProyecto(entity.getIdSedeProyecto());
            response.setIdSedeProyectoObservado(entity.getIdSedeProyectoObservado());
            response.setCargoObservador(entity.getCargoObservador());


            List<CrearCategoriaSbcEntity> list = mViewModel.findByIdSbcOffline(Long.valueOf(entity.getId()));
            DetalleSbcResponse.Categoria categoria = null;
            for (int i = 0; i < list.size(); i++) {
                categoria = new DetalleSbcResponse.Categoria();
                categoria.setIdSBCDetalle(list.get(i).getIdsbcdetalle());
                categoria.setIdSBCCategoria(list.get(i).getIdsbcCategoria());
                categoria.setIdSBCCategoriaItems(list.get(i).getIdsbcCategoriaItems());
                categoria.setSeguro(list.get(i).getSeguro());
                categoria.setRiesgoso(list.get(i).getRiesgoso());
                categoria.setIdbarrera(list.get(i).getIdbarrera());
                categoria.setObservacionSBCDetalle(list.get(i).getObservacionSbcDetalle());
                categoria.setIdbarrera(list.get(i).getIdbarrera());
                categoria.setDescripcionItem(list.get(i).getDescripcionItem());
                lstDetalleSBC.add(categoria);
            }
            response.setLstDetalleSBC(lstDetalleSBC);
            UtilMDP.hideProgreesDialog();
            Intent intent = new Intent(getActivity(), CrearsbcActivity.class);
            intent.putExtra("oDetalleSbc", response);
            startActivity(intent);
        }


    }

    private void obtenerListaSBC() {
        HashMap<String, Integer> hashMap = new HashMap<>();
        hashMap.put("Id_Usuario", sharedPreferences.getInt("IdUsers", 0));
        hashMap.put("Id_Rol_Usuario", sharedPreferences.getInt("Id_Rol_Usuario", 0));
        hashMap.put("Id_Rol_General", sharedPreferences.getInt("Id_Rol_General", 0));

        if (NetworkUtil.isOnline(getContext())) {
            mViewModel.listarSBC(hashMap, getContext()).observe(getActivity(), new Observer<List<SbcResponse>>() {
                @Override
                public void onChanged(List<SbcResponse> sbcResponses) {
                    if (null != sbcResponses) {
                        adaptador.setSBC(sbcResponses);

                    }
                }
            });
        } else {
            List<CrearSbcEntity> list = mViewModel.findAllSbcOffline();
            SbcResponse object = null;
            List<SbcResponse> objectList = new ArrayList<>();
            for (CrearSbcEntity data : list) {
                object = new SbcResponse();
                object.setIdRowSbc(Long.valueOf(data.getId()));
                object.setIdSbc(data.getIdSbc());
                object.setEstadoSBC(4);
                object.setFechaRegistro(data.getFechaRegistroSbc());
                object.setNombreFormato(data.getNombreFormato());
                object.setNombreObservador(data.getNombreObservador());
                object.setCodigoSBC(data.getNombreFormato());
                object.setObservacion(data.getNombreObservador());
                objectList.add(object);
            }
            adaptador.setSBC(objectList);
        }
        onclickTable();
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                obtenerListaSBC();
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 2000);
    }

    private void eliminarSbc(Integer idSBC, Integer position) {
        if(NetworkUtil.isOnline(requireContext())){
            HashMap<String, Integer> hashMap = new HashMap<>();
            hashMap.put("idSBC", idSBC);
            mViewModel.borrarSBC(hashMap, getContext()).observe(getActivity(), new Observer<Boolean>() {
                @Override
                public void onChanged(Boolean aBoolean) {
                    if (aBoolean) {
                        adaptador.removeItem(position);
                    } else {
                        Toast.makeText(getActivity(), "Ocurrio un error", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }else{
            if(adaptador.getSBC(position).getIdSbc() != null &&
                    adaptador.getSBC(position).getIdSbc()  != 0){
                mViewModel.updateEstadoEliminar(adaptador.getSBC(position).getIdRowSbc().intValue());
            }else{
                mViewModel.deleteById( adaptador.getSBC(position).getIdRowSbc().intValue());
            }
            adaptador.removeItem(position);
        }

    }

    private void showDialogEliminar(Integer idSBC, Integer position) {
        View viewAddProject = getActivity().getLayoutInflater().inflate(R.layout.delete_inspeccion, null);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(false);
        alertDialog.setView(viewAddProject);

        TextView textMessage = viewAddProject.findViewById(R.id.textMessage);
        textMessage.setText("¿Está seguro de eliminar el registro de SBC?");

        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                //adaptador.notifyDataSetChanged();
            }
        });

        alertDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                eliminarSbc(idSBC, position);
                dialog.dismiss();
            }
        });

        final AlertDialog dialog = alertDialog.create();
        dialog.show();
    }
}
