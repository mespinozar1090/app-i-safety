package com.mdp.fusionapp.ui.activity.inspecciones;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ImageDecoder;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mdp.fusionapp.BuildConfig;
import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.CrearHallazgoEntity;
import com.mdp.fusionapp.model.Base64Model;
import com.mdp.fusionapp.model.EnvidenciaCierreHallazgo;
import com.mdp.fusionapp.model.EvidenciaFotograficaHallazgo;
import com.mdp.fusionapp.model.GestionModel;
import com.mdp.fusionapp.model.HallazgoModel;
import com.mdp.fusionapp.model.ItemModel;
import com.mdp.fusionapp.model.TipoGestionModel;
import com.mdp.fusionapp.utilitary.Constante;
import com.mdp.fusionapp.utilitary.FileUtils;
import com.mdp.fusionapp.utilitary.NetworkUtil;
import com.mdp.fusionapp.utilitary.RealPathUtil;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.viewModel.HallazgoViewModel;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


public class CrearHallazgoActivity extends AppCompatActivity implements View.OnClickListener {

    private HallazgoViewModel viewModel;

    private Spinner spGestion, spTipoHallazgo, spActoCondicion;
    private Integer selectACCS, idTipoHallazgo;
    private RadioButton rbtBajo, rbtMedio, rbtAlto;
    private RadioButton rbtActo, rbtCondicion;
    private Button btnFecha;
    private static String nameImage = "";
    private static String prefijo = "";
    private static String pathDCIM;
    private File imgFile;

    private String fechaUso = "";
    LinearLayout llTakePhoto, lnlFoto, lblconedorfoto;
    TextView tvModifyPhoto;
    ImageView ivCaptured;
    private Bitmap rotatedBitmap = null;
    private Bitmap rotatedBitmapEvidencia;

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static final int SELECT_IMAGE = 3;

    //variable para crear inspccion;
    private Integer idActoCondicion, idInspeccion, idTipoGestion;
    private Integer idBajo, idMedio, idAlto;
    private Long idRowInspeccion;
    private Integer idActoSubEstandar = 0, idCondicionSubEstandar = 0;
    private String fechaImageGallery, fechaImageMitigadora, nombreTipoGestion;
    private Integer acto, condicion;
    private Button saveHallazgo;
    List<Base64Model> base64Models = null;
    List<Base64Model> base64Evidencia = null;

    private HallazgoModel hallazgoModel;
    private TextInputLayout tilresponsableArea, tilMensaje;
    private TextInputEditText tietresponsableArea, tietmensaje, tietccionMitigadora;
    private TextView tituloToolbar;
    List<EvidenciaFotograficaHallazgo> evidenciaFotograficaHallazgoList;
    List<EnvidenciaCierreHallazgo> envidenciaCierreHallazgoList;
    private Integer idHallazgo = 0;
    private Integer estadoHallazgo = 1;
    private SharedPreferences sharedPreferences;
    private Integer estadoInspeccion;
    //cierre hallazgo
    LinearLayout lnlCierre;
    private ImageView fotoEvidencia;
    private ImageButton capturaEvidencia, showEvidenciaCierre;
    private Boolean isContratisa = false;
    private String base64Hallazgo, base64Mitigadora, nombreActoSubestandar, nombreTipoHallazgo;


    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_hallazgo);
        verifyStoragePermissions(this);
        initView();
    }

    private void setToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        tituloToolbar = (TextView) toolbar.findViewById(R.id.tvDescription);
        tituloToolbar.setText(title);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_back, null);
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorBlanco));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    private void initView() {
        viewModel = new ViewModelProvider(this).get(HallazgoViewModel.class);
        idInspeccion = getIntent().getIntExtra("idInspeccion", 0);
        estadoInspeccion = getIntent().getIntExtra("estadoInspeccion", 0);
        idRowInspeccion = getIntent().getLongExtra("idRowInspeccion", 0);
        sharedPreferences = getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        hallazgoModel = (HallazgoModel) getIntent().getSerializableExtra("hallazgoDetalle");
        setToolbar(validaObject() ? "Editar Hallazgo" : "Crear Hallazgo");

        Log.e("idRowInspeccion", " " + idRowInspeccion);
        base64Evidencia = new ArrayList<>();
        base64Models = new ArrayList<>();
        saveHallazgo = findViewById(R.id.saveHallazgo);
        tietmensaje = findViewById(R.id.tietmensaje);
        tilresponsableArea = findViewById(R.id.tilresponsableArea);
        tilMensaje = findViewById(R.id.tilMensaje);
        lblconedorfoto = findViewById(R.id.lblconedorfoto);
        btnFecha = findViewById(R.id.btnFecha);
        lnlFoto = findViewById(R.id.lnlFoto);
        tietresponsableArea = findViewById(R.id.tietresponsableArea);
        tietccionMitigadora = findViewById(R.id.tietccionMitigadora);
        lnlCierre = findViewById(R.id.lnlCierre);
        fotoEvidencia = findViewById(R.id.fotoEvidencia);
        capturaEvidencia = findViewById(R.id.capturaEvidencia);
        showEvidenciaCierre = findViewById(R.id.showEvidenciaCierre);
        showEvidenciaCierre.setOnClickListener(this);

        rbtActo = findViewById(R.id.rbtActo);
        rbtActo.setChecked(true);
        selectACCS = Constante.ACTO;
        rbtCondicion = findViewById(R.id.rbtCondicion);
        rbtBajo = findViewById(R.id.rbtBajo);
        rbtBajo.setChecked(true);
        rbtMedio = findViewById(R.id.rbtMedio);
        rbtAlto = findViewById(R.id.rbtAlto);
        spGestion = findViewById(R.id.spGestion);

        //update
        spTipoHallazgo = findViewById(R.id.spTipoHallazgo);
        spActoCondicion = findViewById(R.id.spActoCondicion);

        btnFecha.setOnClickListener(this);
        rbtActo.setOnClickListener(this);
        rbtCondicion.setOnClickListener(this);
        rbtBajo.setOnClickListener(this);
        rbtMedio.setOnClickListener(this);
        rbtAlto.setOnClickListener(this);
        lnlFoto.setOnClickListener(this);
        saveHallazgo.setOnClickListener(this);
        capturaEvidencia.setOnClickListener(this);
        lnlCierre.setVisibility(View.GONE);
        idBajo = 1;
        idMedio = 0;
        idAlto = 0;

        loadFechaActual();
        loadGestion();
        tietresponsableArea.setText(getIntent().getStringExtra("responsableArea"));
        if (validaObject()) {
            loadDataDetalle();
            editar();
            validarCreadorHallazgo();
        }
        validarRolCreacion();
    }

    private void editar() {
        if (estadoInspeccion == 2) {
            btnFecha.setEnabled(false);
            rbtActo.setEnabled(false);
            rbtCondicion.setEnabled(false);
            rbtBajo.setEnabled(false);
            rbtMedio.setEnabled(false);
            rbtAlto.setEnabled(false);
            saveHallazgo.setEnabled(false);
            capturaEvidencia.setEnabled(false);
            btnFecha.setEnabled(false);
            lnlCierre.setEnabled(false);
            lnlCierre.setVisibility(View.VISIBLE);
            spTipoHallazgo.setEnabled(false);
            spActoCondicion.setEnabled(false);
            spGestion.setEnabled(false);
            tietresponsableArea.setEnabled(false);
            tietmensaje.setEnabled(false);
            tietccionMitigadora.setEnabled(false);
        }
    }

    private void validarRolCreacion() {
        Integer rolUsuario = sharedPreferences.getInt("Id_Rol_Usuario", 0);
        if (rolUsuario == 2 || rolUsuario == 4) {
            lnlCierre.setVisibility(View.GONE);
        } else {
            lnlCierre.setEnabled(true);
            lnlCierre.setVisibility(View.VISIBLE);
        }
        if (validaObject()) {
            if (hallazgoModel.getEstadoDetalleInspeccion()) {
                Log.e("estado", " result " + hallazgoModel.getEstadoDetalleInspeccion());
                lnlCierre.setVisibility(View.VISIBLE);
                tietccionMitigadora.setEnabled(false);
                capturaEvidencia.setEnabled(false);
            }
        }
    }

    private void validarCreadorHallazgo() {

        switch (hallazgoModel.getUsuarioRol()) {
            case 1:
                if (hallazgoModel.getUsuarioRol() != sharedPreferences.getInt("Id_Rol_Usuario", 0)) {
                    btnFecha.setEnabled(false);
                    rbtActo.setEnabled(false);
                    rbtCondicion.setEnabled(false);
                    rbtBajo.setEnabled(false);
                    rbtMedio.setEnabled(false);
                    rbtAlto.setEnabled(false);
                    saveHallazgo.setEnabled(false);
                    capturaEvidencia.setEnabled(false);
                    btnFecha.setEnabled(false);
                    spTipoHallazgo.setEnabled(false);
                    spActoCondicion.setEnabled(false);
                    spGestion.setEnabled(false);
                    tietresponsableArea.setEnabled(false);
                    tietmensaje.setEnabled(false);
                    tietccionMitigadora.setEnabled(false);

                    if (sharedPreferences.getInt("Id_Rol_Usuario", 0) == 3 || sharedPreferences.getInt("Id_Rol_Usuario", 0) == 1) {
                        lnlCierre.setEnabled(true);
                        tietccionMitigadora.setEnabled(true);
                        capturaEvidencia.setEnabled(true);
                        lnlCierre.setVisibility(View.VISIBLE);
                        saveHallazgo.setEnabled(true);
                    }
                } else if (hallazgoModel.getUsuarioRol() == 1) {
                    lnlCierre.setEnabled(true);
                    tietccionMitigadora.setEnabled(true);
                    capturaEvidencia.setEnabled(true);
                    lnlCierre.setVisibility(View.VISIBLE);
                    saveHallazgo.setEnabled(true);
                }
                break;
            case 4:
            case 2:
                if (hallazgoModel.getUsuarioRol() != sharedPreferences.getInt("Id_Rol_Usuario", 0) && sharedPreferences.getInt("Id_Rol_Usuario", 0) != 1) {
                    btnFecha.setEnabled(false);
                    rbtActo.setEnabled(false);
                    rbtCondicion.setEnabled(false);
                    rbtBajo.setEnabled(false);
                    rbtMedio.setEnabled(false);
                    rbtAlto.setEnabled(false);
                    saveHallazgo.setEnabled(false);
                    capturaEvidencia.setEnabled(false);
                    btnFecha.setEnabled(false);
                    lnlCierre.setEnabled(false);
                    spTipoHallazgo.setEnabled(false);
                    spActoCondicion.setEnabled(false);
                    spGestion.setEnabled(false);
                    tietresponsableArea.setEnabled(false);
                    tietmensaje.setEnabled(false);
                    tietccionMitigadora.setEnabled(false);
                    if (sharedPreferences.getInt("Id_Rol_Usuario", 0) == 3 || sharedPreferences.getInt("Id_Rol_Usuario", 0) == 1) {
                        lnlCierre.setEnabled(true);
                        tietccionMitigadora.setEnabled(true);
                        capturaEvidencia.setEnabled(true);
                        lnlCierre.setVisibility(View.VISIBLE);
                        saveHallazgo.setEnabled(true);
                    }
                }
                break;
            case 3:
                if (hallazgoModel.getUsuarioRol() != sharedPreferences.getInt("Id_Rol_Usuario", 0) && sharedPreferences.getInt("Id_Rol_Usuario", 0) != 1) {
                    btnFecha.setEnabled(false);
                    rbtActo.setEnabled(false);
                    rbtCondicion.setEnabled(false);
                    rbtBajo.setEnabled(false);
                    rbtMedio.setEnabled(false);
                    rbtAlto.setEnabled(false);
                    saveHallazgo.setEnabled(false);
                    capturaEvidencia.setEnabled(false);
                    btnFecha.setEnabled(false);
                    lnlCierre.setEnabled(false);
                    spTipoHallazgo.setEnabled(false);
                    spActoCondicion.setEnabled(false);
                    spGestion.setEnabled(false);
                    tietresponsableArea.setEnabled(false);
                    tietmensaje.setEnabled(false);
                    tietccionMitigadora.setEnabled(false);
                } else {
                    saveHallazgo.setEnabled(true);
                    lnlCierre.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    private void loadFechaActual() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        fechaUso = dateFormat.format(new Date());
        btnFecha.setText(dateFormat.format(new Date()));
        btnFecha.setTextSize(12);
        btnFecha.setTextColor(Color.BLACK);
    }

    private Boolean validaObject() {
        return null != hallazgoModel;
    }

    private void loadDataDetalle() {
        switch (hallazgoModel.getRiesgo()) {
            case 1:
                rbtBajo.setChecked(true);
                idBajo = 1;
                idMedio = 0;
                idAlto = 0;
                break;
            case 2:
                rbtMedio.setChecked(true);
                idBajo = 0;
                idMedio = 1;
                idAlto = 0;
                break;
            case 3:
                rbtAlto.setChecked(true);
                idBajo = 0;
                idMedio = 0;
                idAlto = 1;
                break;
        }

        idHallazgo = hallazgoModel.getId();
        idActoCondicion = hallazgoModel.getIdActoSubestandar();
        idCondicionSubEstandar = hallazgoModel.getIdCondicionSubestandar();
        tietresponsableArea.setText(hallazgoModel.getResposableAreadetalle());
        tietmensaje.setText(hallazgoModel.getActoSubestandar());
        fechaUso = hallazgoModel.getPlazo();


        btnFecha.setText(hallazgoModel.getPlazo());
        btnFecha.setTextSize(12);
        btnFecha.setTextColor(Color.BLACK);

        evidenciaFotograficaHallazgoList = hallazgoModel.getLstEvidenciaFotoGrafica();
        if (evidenciaFotograficaHallazgoList != null && evidenciaFotograficaHallazgoList.size() > 0) {
            lblconedorfoto.setVisibility(View.VISIBLE);
            for (int x = 0; x < evidenciaFotograficaHallazgoList.size(); x++) {
                base64Hallazgo = evidenciaFotograficaHallazgoList.get(x).getImgB64();
                fechaImageGallery = evidenciaFotograficaHallazgoList.get(x).getFechaFoto();
            }
        }

        envidenciaCierreHallazgoList = hallazgoModel.getLstEvidenciaCierre();
        if (envidenciaCierreHallazgoList != null && envidenciaCierreHallazgoList.size() > 0) {
            showEvidenciaCierre.setVisibility(View.VISIBLE);

            for (int x = 0; x < envidenciaCierreHallazgoList.size(); x++) {
                base64Mitigadora = envidenciaCierreHallazgoList.get(x).getImgB64();
                fechaImageMitigadora = envidenciaCierreHallazgoList.get(x).getFechaFoto();
            }
        }

        if (hallazgoModel.getAccionMitigadora() != null && hallazgoModel.getAccionMitigadora() != "") {
            tietccionMitigadora.setText(hallazgoModel.getAccionMitigadora());
        }

        if (hallazgoModel.getIdActoSubestandar() != 0) {
            rbtActo.setChecked(true);
            selectACCS = 1;
        }

        if (hallazgoModel.getIdCondicionSubestandar() != 0) {
            rbtCondicion.setChecked(true);
            selectACCS = 2;
        }

        if (validaObject()) {
            List<GestionModel> oLista = UtilMDP.lstGestion();
            for (int x = 0; x < oLista.size(); x++) {
                if (hallazgoModel.getIdTipoGestion().intValue() == oLista.get(x).getId()) {
                    spGestion.setSelection(x);
                }
            }
        }
    }

    private Boolean validarCampos() {
        if (idTipoGestion != null && idTipoGestion != 0) {
            if (idTipoHallazgo != null && idTipoGestion != 0) {
                if (idCondicionSubEstandar != 0 || idActoSubEstandar != 0) {
                    if (!tietmensaje.getText().toString().trim().isEmpty()) {
                        if (!tietresponsableArea.getText().toString().trim().isEmpty()) {
                            if (rotatedBitmap != null) {
                                return true;
                            } else {
                                if (base64Hallazgo != null && !base64Hallazgo.equals("")) {
                                    return true;
                                } else {
                                    showValidarNFoto("Evidencia fotografica", "Debe adjuntar una evidencia fotografica");
                                }
                            }
                        } else {
                            tilresponsableArea.setError(UtilMDP.REQUIRED_FIELD);
                            tietresponsableArea.requestFocus();
                        }
                    } else {
                        tilMensaje.setError(UtilMDP.REQUIRED_FIELD);
                        tietmensaje.requestFocus();
                    }
                } else {
                    Toast.makeText(this, "Seleccione Tipo de Acto o Condición Subestándar", Toast.LENGTH_LONG).show();

                }
            } else {
                Toast.makeText(this, "Seleccione Tipo de Hallazgo", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "Seleccione Tipo Gestión", Toast.LENGTH_LONG).show();
        }

        return false;
    }

    public void showValidarNFoto(String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int arg1) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void saveHallazgo() {
        List<HashMap<String, Object>> hashMapList = new ArrayList<>();
        List<HashMap<String, Object>> hashMapListEnvidencia = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String fechaImagen = dateFormat.format(new Date());

        if (!tietccionMitigadora.getText().toString().trim().isEmpty() || rotatedBitmapEvidencia != null) {
            Log.e("entra", "data1" + tietccionMitigadora.getText().toString() + "3");

            if (rotatedBitmapEvidencia == null) {
                Toast.makeText(this, "Agrege una foto en Accion mitigadora", Toast.LENGTH_SHORT).show();
                return;
            }

            if (tietccionMitigadora.getText().toString().trim().isEmpty()) {
                Toast.makeText(this, "Agrege Accion mitigadora", Toast.LENGTH_SHORT).show();
                return;
            }
            // estadoHallazgo = 0;
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("imgB64", rotatedBitmapEvidencia != null ? "data:image/jpeg;base64," + UtilMDP.converteToBase64(rotatedBitmapEvidencia) : base64Mitigadora);
            hashMap.put("fecha", fechaImageMitigadora != null ? fechaImageMitigadora : fechaImagen);
            hashMapListEnvidencia.add(hashMap);
        }

        if (validarCampos()) {
            HashMap<String, Object> hashMapFoto = new HashMap<>();
            hashMapFoto.put("fecha", fechaImageGallery != null ? fechaImageGallery : fechaImagen);
            hashMapFoto.put("imgB64", rotatedBitmap != null ? "data:image/jpeg;base64," + UtilMDP.converteToBase64(rotatedBitmap) : base64Hallazgo);
            hashMapList.add(hashMapFoto);

            UtilMDP.showProgressDialog(this, "Guardando hallazgo...");
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("Id_Inspecciones_reporte_detalle", idHallazgo);

            hashMap.put("Id_Inspeccion", idInspeccion);
            hashMap.put("idTipoGestion", idTipoGestion);
            hashMap.put("id_ActoSubestandar", idActoSubEstandar);
            hashMap.put("id_CondicionSubestandar", idCondicionSubEstandar);
            hashMap.put("Acto_Subestandar", tietmensaje.getText().toString()); // descripcion
            hashMap.put("Riesgo_A", idAlto);
            hashMap.put("Riesgo_M", idMedio);
            hashMap.put("Riesgo_B", idBajo);
            hashMap.put("idHallazgo", idTipoHallazgo);
            hashMap.put("id_NoConformidad", 0);
            hashMap.put("id_SubFamiliaAmbiental", 0);
            hashMap.put("Resposable_Area_detalle", tietresponsableArea.getText().toString());
            hashMap.put("EvidenciaFotograficaObservacion", hashMapList);
            hashMap.put("PlazoString", fechaUso);
            hashMap.put("Origen_Registro", "App");
            hashMap.put("Estado_DetalleInspeccion", estadoHallazgo);
            hashMap.put("Accion_Mitigadora", tietccionMitigadora.getText().toString());
            hashMap.put("Evidencia_Cierre", hashMapListEnvidencia.size() > 0 ? hashMapListEnvidencia : null);
            hashMap.put("Id_Usuario_Registro", sharedPreferences.getInt("IdUsers", 0));

            if (NetworkUtil.isOnline(getApplicationContext())) {
                viewModel.insertarHallazgo(hashMap).observe(this, new Observer<Boolean>() {
                    @Override
                    public void onChanged(Boolean result) {
                        if (result != null) {
                            UtilMDP.hideProgreesDialog();
                            finish();
                        }
                    }
                });
            } else {
                insertHallazgoOffline();
            }

        }
    }


    private void insertHallazgoOffline() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String fechaImagen = dateFormat.format(new Date());

        CrearHallazgoEntity object = new CrearHallazgoEntity();
        object.setEstadoRegistroDB(Constante.ESTADO_CREADO);
        object.setIdInspeccionesReporteDetalle(idHallazgo);
        object.setIdInspeccionOffline(idRowInspeccion);
        object.setIdTipoGestion(idTipoGestion);
        object.setIdActoSubEstandar(idActoSubEstandar);
        object.setIdCondicionSubEstandar(idCondicionSubEstandar);
        object.setActoSubestandar(tietmensaje.getText().toString());
        object.setRiesgoA(idAlto == 0 ? false : true);
        object.setRiesgoM(idMedio == 0 ? false : true);
        object.setRiesgoB(idBajo == 0 ? false : true);
        object.setIdTipoHallazgo(idTipoHallazgo);
        object.setNombreTipoHallazgo(nombreTipoHallazgo);
        object.setResposableAreaDetalle(tietresponsableArea.getText().toString());
        object.setNombreActoSubestandar(nombreActoSubestandar);
        object.setEvidenciaFotoFecha(fechaImageGallery != null ? fechaImageGallery : fechaImagen);
        object.setEvidenciaFotoImg64(rotatedBitmap != null ? "data:image/jpeg;base64," + UtilMDP.converteToBase64(rotatedBitmap) : base64Hallazgo);
        object.setNombreTipoGestion(nombreTipoGestion);
        object.setPlazaString(fechaUso);
        object.setActionMitigadora(tietccionMitigadora.getText().toString());
        object.setIdRolUsuario(sharedPreferences.getInt("Id_Rol_Usuario", 0));
        object.setIdUsuarioRegistro(sharedPreferences.getInt("IdUsers", 0));

        if (!tietccionMitigadora.getText().toString().trim().isEmpty() && rotatedBitmapEvidencia != null) {
            object.setEstadoDetalleInspeccion(true);
            object.setEvidenciaCierreFotoFecha(fechaImageMitigadora != null ? fechaImageMitigadora : fechaImagen);
            object.setEvidenciaCierreFotoImg64(rotatedBitmapEvidencia != null ? "data:image/jpeg;base64," + UtilMDP.converteToBase64(rotatedBitmapEvidencia) : base64Mitigadora);
        } else {
            object.setEstadoDetalleInspeccion(false);
        }

        if (hallazgoModel != null && hallazgoModel.getIdRow() != null) {
            object.setId(hallazgoModel.getIdRow());
            object.setEstadoRegistroDB(Constante.ESTADO_EDITADO);
        }

        viewModel.insertHallazgo(object);
        UtilMDP.hideProgreesDialog();
        showCreateRegistro("Estimado, tu registro se ha guardado con éxito. Cuando haya conexión a Internet se sincronizara con la web");
    }

    public void showCreateRegistro(String mensaje) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Registro Guardado");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage(mensaje);
        alertDialogBuilder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                finish();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void mostrarDialogFoto() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String fechaImagen = dateFormat.format(new Date());

        View view = getLayoutInflater().inflate(R.layout.dialo_mostrarfoto, null);
        llTakePhoto = view.findViewById(R.id.llTakePhoto);
        llTakePhoto.setVisibility(View.GONE);
        tvModifyPhoto = view.findViewById(R.id.tvModifyPhoto);
        ivCaptured = view.findViewById(R.id.ivCaptured);
        TextView txtFecha = view.findViewById(R.id.txtFecha);
        llTakePhoto.setOnClickListener(this);
        tvModifyPhoto.setOnClickListener(this);
        txtFecha.setText(fechaImageGallery != null ? fechaImageGallery : fechaImagen);
        ivCaptured.setVisibility(View.VISIBLE);
        llTakePhoto.setVisibility(View.GONE);
        tvModifyPhoto.setVisibility(View.VISIBLE);

        if (rotatedBitmap != null) {
            ivCaptured.setImageBitmap(rotatedBitmap);
        } else if (base64Hallazgo != null && !base64Hallazgo.isEmpty()) {
            if (base64Hallazgo.contains("base64")) {
                String base64 = base64Hallazgo;
                String[] parts = base64.split("base64,");
                String part1 = parts[0];
                String part2 = parts[1];
                ivCaptured.setImageBitmap(UtilMDP.decodeBase64(part2));
            } else {
                Picasso.get().load(base64Hallazgo).into(ivCaptured);
            }
            // Log.e("base64Hallazgo2", "" + base64Hallazgo);
            //  Picasso.with(getApplicationContext()).load(base64Hallazgo).into(ivCaptured);
        }

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(CrearHallazgoActivity.this);
        alertDialog.setCancelable(true);
        alertDialog.setView(view);

        final AlertDialog dialog = alertDialog.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().height =
                (int) (UtilMDP.getDeviceMetrics(getApplicationContext()).heightPixels * 0.8);
        dialog.show();
    }


    private void mostrarDialogFotoCierre() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String fechaImagen = dateFormat.format(new Date());

        View view = getLayoutInflater().inflate(R.layout.dialo_mostrarfoto, null);
        llTakePhoto = view.findViewById(R.id.llTakePhoto);
        tvModifyPhoto = view.findViewById(R.id.tvModifyPhoto);
        ivCaptured = view.findViewById(R.id.ivCaptured);
        TextView txtFecha = view.findViewById(R.id.txtFecha);

        llTakePhoto.setOnClickListener(this);
        tvModifyPhoto.setOnClickListener(this);
        txtFecha.setText(fechaImageMitigadora != null ? fechaImageMitigadora : fechaImagen);
        ivCaptured.setVisibility(View.VISIBLE);
        llTakePhoto.setVisibility(View.GONE);
        tvModifyPhoto.setVisibility(View.VISIBLE);

        if (rotatedBitmapEvidencia != null) {
            ivCaptured.setImageBitmap(rotatedBitmapEvidencia);
        } else if (base64Mitigadora != null && !base64Mitigadora.isEmpty()) {
            if (base64Mitigadora.contains("base64")) {
                String base64 = base64Hallazgo;
                String[] parts = base64.split("base64,");
                String part1 = parts[0];
                String part2 = parts[1];
                ivCaptured.setImageBitmap(UtilMDP.decodeBase64(part2));
            } else {
                Picasso.get().load(base64Mitigadora).into(ivCaptured);
            }
        }

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(CrearHallazgoActivity.this);
        alertDialog.setCancelable(true);
        alertDialog.setView(view);

        final AlertDialog dialog = alertDialog.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().height =
                (int) (UtilMDP.getDeviceMetrics(getApplicationContext()).heightPixels * 0.8);
        dialog.show();
    }

    private void loadGestion() {
        ArrayAdapter<GestionModel> adapterTipoDocumento = new ArrayAdapter<>(this, R.layout.spinner_text, UtilMDP.lstGestion());
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spGestion.setAdapter(adapterTipoDocumento);
        spGestion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                GestionModel gestionModel = (GestionModel) parent.getSelectedItem();
                idTipoGestion = gestionModel.getId();
                nombreTipoGestion = gestionModel.getNombre();
                loadTipoHallazgo(idTipoGestion);
                loadActoCondicion(idTipoGestion);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void loadTipoHallazgo(Integer idGestion) {
        ArrayAdapter<ItemModel> adapterTipoDocumento = new ArrayAdapter<>(this, R.layout.spinner_text, UtilMDP.tipoHallazgo(idGestion));
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTipoHallazgo.setAdapter(adapterTipoDocumento);
        spTipoHallazgo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ItemModel itemModel = (ItemModel) parent.getSelectedItem();
                idTipoHallazgo = itemModel.getId();
                nombreTipoHallazgo = itemModel.getNombre();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        if (validaObject()) {
            List<ItemModel> lista = UtilMDP.tipoHallazgo(idGestion);
            for (int x = 0; x < lista.size(); x++) {
                if (hallazgoModel.getIdTipoHallazgo().intValue() == lista.get(x).getId()) {
                    Log.e("entroo", "debe entrar ");
                    spTipoHallazgo.setSelection(x);
                }
            }
        }
    }

    private void loadActoCondicion(Integer idGestion) {
        List<TipoGestionModel> lista = (idGestion == Constante.SST ? UtilMDP.actoSubEstandarSST(selectACCS) : UtilMDP.actoSubEstandarMA(selectACCS));
        ArrayAdapter<TipoGestionModel> adapterTipoDocumento = new ArrayAdapter<>(this, R.layout.spinner_text, lista);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spActoCondicion.setAdapter(adapterTipoDocumento);
        spActoCondicion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TipoGestionModel tipoGestionModel = (TipoGestionModel) parent.getSelectedItem();
                if (selectACCS == 1) {
                    idCondicionSubEstandar = 0;
                    idActoSubEstandar = tipoGestionModel.getId();
                    nombreActoSubestandar = tipoGestionModel.getNombre();
                } else if (selectACCS == 2) {
                    idActoSubEstandar = 0;
                    nombreActoSubestandar = tipoGestionModel.getNombre();
                    idCondicionSubEstandar = tipoGestionModel.getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (validaObject()) {
            for (int x = 0; x < lista.size(); x++) {
                if (hallazgoModel.getIdActoSubestandar() != 0) {
                    if (hallazgoModel.getIdActoSubestandar().intValue() == lista.get(x).getId()) {
                        spActoCondicion.setSelection(x);
                    }
                }
                if (hallazgoModel.getIdCondicionSubestandar() != 0) {
                    if (hallazgoModel.getIdCondicionSubestandar().intValue() == lista.get(x).getId()) {
                        spActoCondicion.setSelection(x);
                    }
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_foto:
                if (validaObject()) {
                    if (hallazgoModel.getUsuarioRol() == sharedPreferences.getInt("Id_Rol_Usuario", 0) || sharedPreferences.getInt("Id_Rol_Usuario", 0) == 1) {
                        showCaptureImagen();
                    }
                } else {
                    showCaptureImagen();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showCaptureImagen() {
        View viewAddProject = getLayoutInflater().inflate(R.layout.dialog_boottomsheet, null);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(true);
        alertDialog.setView(viewAddProject);

        LinearLayout llcamara = viewAddProject.findViewById(R.id.llcamara);
        LinearLayout llgaleria = viewAddProject.findViewById(R.id.llgaleria);

        final AlertDialog dialog = alertDialog.create();
        dialog.show();

        llcamara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accionCamara();
                dialog.dismiss();
            }
        });

        llgaleria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE);
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == UtilMDP.REQUEST_CAMERA_SEND_ALERT) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            fechaImageGallery = dateFormat.format(new Date());
            if (resultCode == Activity.RESULT_OK) {
                onCaptureImageResult(data);
            } else {
                deleteImage();
            }
        } else if (requestCode == SELECT_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    File file = FileUtils.getFile(getApplicationContext(), data.getData());
                    if (Build.VERSION.SDK_INT < 28) {
                        if (isContratisa) {
                            if (file.exists()) {
                                fechaImageMitigadora = getDateImage(file.getAbsolutePath());
                                Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                                Bitmap bitmapx = rotarImagen(bitmap, file.getAbsolutePath());
                                rotatedBitmapEvidencia = Bitmap.createScaledBitmap(bitmapx, 250, 250, true);
                                showEvidenciaCierre.setVisibility(View.VISIBLE);
                            }
                        } else {
                            if (file.exists()) {
                                fechaImageMitigadora = getDateImage(file.getAbsolutePath());
                                Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                                Bitmap bitmapx = rotarImagen(bitmap, file.getAbsolutePath());
                                rotatedBitmap = Bitmap.createScaledBitmap(bitmapx, 250, 250, true);
                                lblconedorfoto.setVisibility(View.VISIBLE);
                            }
                        }
                    } else {
                        if (isContratisa) {
                            if (file.exists()) {
                                fechaImageMitigadora = getDateImage(file.getAbsolutePath());
                                Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                                Bitmap bitmapx = rotarImagen(bitmap, file.getAbsolutePath());
                                rotatedBitmapEvidencia = Bitmap.createScaledBitmap(bitmapx, 250, 250, true);
                                showEvidenciaCierre.setVisibility(View.VISIBLE);
                            }
                        } else {
                            if (file.exists()) {
                                fechaImageMitigadora = getDateImage(file.getAbsolutePath());
                                lblconedorfoto.setVisibility(View.VISIBLE);
                                Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                                Bitmap bitmapx = rotarImagen(bitmap, file.getAbsolutePath());
                                rotatedBitmap = Bitmap.createScaledBitmap(bitmapx, 250, 250, true);
                            }
                        }
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(CrearHallazgoActivity.this, "Canceled", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String getDateImage(String pathToTheImage) {
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(pathToTheImage);
            if (exif != null) {
                return exif.getAttribute(ExifInterface.TAG_DATETIME);
            }
        } catch (IOException exception) {
            Log.e("ERROR", " : " + exception.getMessage());
            exception.printStackTrace();
        }

        return null;
    }

    public void deleteImage() {
        File fileImage = new File(UtilMDP.getImagesDirectory() + File.separator + nameImage);
        if (fileImage.exists()) {
            fileImage.delete();
            lblconedorfoto.setVisibility(View.GONE);
        }
        nameImage = "";
    }

    private Bitmap onCaptureImageResult(Intent data) {
        File alcomapp = new File(UtilMDP.getImagesDirectory());
        if (!alcomapp.exists()) {
            boolean aa = alcomapp.mkdirs();
        }

        imgFile = new File(pathDCIM);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
        Bitmap thumbnail = Bitmap.createScaledBitmap(bitmap, 512, 512, true);
        Bitmap bitmapx = rotarImagenAndSave(thumbnail);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmapx.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File destination = new File(alcomapp, nameImage);
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (isContratisa) {
            rotatedBitmapEvidencia = bitmapx;
            fotoEvidencia.setImageBitmap(rotatedBitmapEvidencia);
            fotoEvidencia.setVisibility(View.GONE);
            showEvidenciaCierre.setVisibility(View.VISIBLE);
        } else {
            rotatedBitmap = bitmapx;
            lblconedorfoto.setVisibility(View.VISIBLE);
        }
        return bitmapx; //thumbnail
    }

    private Bitmap rotarImagenAndSave(Bitmap bitmap) {
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(imgFile.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientacion = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        Matrix matrix = new Matrix();
        switch (orientacion) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                Log.e("ENTRO", "ORIENTATION_ROTATE_90");
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                Log.e("ENTRO", "ORIENTATION_ROTATE_90");
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                Log.e("ENTRO", "ORIENTATION_ROTATE_90");
                matrix.setRotate(270);
                break;
            default:
        }
        return bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    private Bitmap rotarImagen(Bitmap bitmap, String path) {
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientacion = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        Matrix matrix = new Matrix();
        switch (orientacion) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                Log.e("ENTRO", "ORIENTATION_ROTATE_90");
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                Log.e("ENTRO", "ORIENTATION_ROTATE_90");
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                Log.e("ENTRO", "ORIENTATION_ROTATE_90");
                matrix.setRotate(270);
                break;
            default:
        }
        return bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static void verifyStoragePermissions(Activity activity) {
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }


    public void utilizarCamara() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Uri mProcessingPhotoUri = getImageFileCompat();
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mProcessingPhotoUri);
        } else {
            nameImage = prefijo + "_" + UtilMDP.getCustomDate("yyyyMMddHHmmss") + ".jpg";
            File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
            pathDCIM = storageDir.getAbsolutePath() + File.separator + nameImage;
            File file = new File(pathDCIM);
            Uri outputFileUri = Uri.fromFile(file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        }
        startActivityForResult(intent, UtilMDP.REQUEST_CAMERA_SEND_ALERT);
    }

    private Uri getImageFileCompat() {
        nameImage = prefijo + "_" + UtilMDP.getCustomDate("yyyyMMddHHmmss");
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        try {
            String extension = ".jpg";
            File image = File.createTempFile(nameImage, extension, storageDir);
            nameImage += extension;
            pathDCIM = image.getAbsolutePath();
            return FileProvider.getUriForFile(getApplicationContext(), BuildConfig.APPLICATION_ID + ".provider", image);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.capturaEvidencia:
                isContratisa = true;
                //accionCamara();
                showCaptureImagen();
                break;
            case R.id.saveHallazgo:
                saveHallazgo();
                break;
            case R.id.showEvidenciaCierre:
                mostrarDialogFotoCierre();
                break;
            case R.id.btnFecha:
                getFechaCalendar();
                break;
            case R.id.lnlFoto:
                mostrarDialogFoto();
                break;
            case R.id.llTakePhoto:
                accionCamara();
                break;
            case R.id.tvModifyPhoto:
                accionCamara();
                break;
            case R.id.rbtBajo:
                boolean checked1 = ((RadioButton) view).isChecked();
                if (checked1)
                    idBajo = 1;
                idMedio = 0;
                idAlto = 0;
                break;
            case R.id.rbtMedio:
                boolean checked2 = ((RadioButton) view).isChecked();
                if (checked2)
                    idBajo = 0;
                idMedio = 1;
                idAlto = 0;
                break;
            case R.id.rbtAlto:
                boolean checked3 = ((RadioButton) view).isChecked();
                if (checked3)
                    idBajo = 0;
                idMedio = 0;
                idAlto = 1;
                break;
            case R.id.rbtActo:
                boolean checkedActo = ((RadioButton) view).isChecked();
                if (checkedActo)
                    selectACCS = 1;
                loadActoCondicion(idTipoGestion);
                break;
            case R.id.rbtCondicion:
                boolean checkedCondicion = ((RadioButton) view).isChecked();
                if (checkedCondicion)
                    selectACCS = 2;
                loadActoCondicion(idTipoGestion);
                break;
        }
    }

    private void getFechaCalendar() {
        final Calendar calendario = Calendar.getInstance();
        int year = calendario.get(Calendar.YEAR);
        int month = calendario.get(Calendar.MONTH);
        int day = calendario.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, monthOfYear);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                //SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yyyy'T'HH:mm:ss");
                // fechaUso = dateFormat1.format(cal.getTime());

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"); //
                dateFormat.setTimeZone(cal.getTimeZone());
                String currentDateString = dateFormat.format(cal.getTime());
                fechaUso = currentDateString;
                btnFecha.setText(currentDateString);
                btnFecha.setTextSize(12);
                btnFecha.setTextColor(Color.BLACK);
            }
        }, year, month, day);

        Date miFecha = calendario.getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        datePicker.getDatePicker().setMinDate(convertDateToMillis(dateFormat.format(miFecha)));
        datePicker.show();
    }

    private Long convertDateToMillis(String givenDateString) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        long timeInMilliseconds = System.currentTimeMillis() - 1000;
        try {
            Date mDate = sdf.parse(givenDateString);
            timeInMilliseconds = mDate.getTime();
            System.out.println("Date in milliseconds: " + timeInMilliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeInMilliseconds;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_foto, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void accionCamara() {
        utilizarCamara();
    }
}
