package com.mdp.fusionapp.ui.fragment.incidente;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.incidente.CrearIncidenteEntity;
import com.mdp.fusionapp.network.response.IncidenteDetalleResponse;
import com.mdp.fusionapp.network.response.IncidenteResponse;
import com.mdp.fusionapp.ui.activity.incidentes.IncidenteAcitvity;
import com.mdp.fusionapp.ui.adapter.IncidenteAdapter;
import com.mdp.fusionapp.ui.fragment.inspeccion.InspeccionDetalleFragment;
import com.mdp.fusionapp.utilitary.Constante;
import com.mdp.fusionapp.utilitary.NetworkUtil;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.viewModel.IncidenteViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class IncidenteFragment extends Fragment {

    private IncidenteViewModel mViewModel;
    private LinearLayoutManager linearLayout;
    private RecyclerView recyclerView;
    private IncidenteAdapter adaptador;
    private List<IncidenteResponse.Data> lista;
    private SharedPreferences sharedPreferences;

    public static IncidenteFragment newInstance() {
        return new IncidenteFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.incidente_fragment, container, false);
        mViewModel = new ViewModelProvider(this).get(IncidenteViewModel.class);
        setToolbar(view);
        setupView(view);
        return view;
    }

    private void setupView(View view) {
        sharedPreferences = getActivity().getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        recyclerView = view.findViewById(R.id.recyclerview);
        linearLayout = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayout);
        lista = new ArrayList<>();
        adaptador = new IncidenteAdapter(getContext(), lista);
        recyclerView.setAdapter(adaptador);
        onclickTable();
    }

    @Override
    public void onResume() {
        super.onResume();
        obtenerLista();
    }

    private void obtenerLista() {

        if (NetworkUtil.isOnline(requireContext())) {
            HashMap<String, Integer> hashMap = new HashMap<>();
            hashMap.put("Id_Usuario", sharedPreferences.getInt("IdUsers", 0));
            hashMap.put("Id_Rol_Usuario", sharedPreferences.getInt("Id_Rol_Usuario", 0));
            hashMap.put("Id_Rol_General", sharedPreferences.getInt("Id_Rol_General", 0));
            mViewModel.listarIncidente(hashMap, getContext()).observe(getActivity(), new Observer<IncidenteResponse>() {
                @Override
                public void onChanged(IncidenteResponse incidenteResponses) {
                    if (null != incidenteResponses) {
                        adaptador.setLista(incidenteResponses.getData());
                    }
                }
            });
        } else {
            List<CrearIncidenteEntity> incidentes = mViewModel.findAllIncidente();
            List<IncidenteResponse.Data> listData = new ArrayList<>();
            IncidenteResponse.Data data = null;
            for (int i = 0; i < incidentes.size(); i++) {
                data = new IncidenteResponse.Data();
                data.setIdRow(incidentes.get(i).getId());
                data.setIdIncidente(incidentes.get(i).getIdIncidente());
                data.setTipoInformePF(incidentes.get(i).getTipoInformePF());
                data.setgIdProyectoSede(incidentes.get(i).getGidProyectoSede());
                data.setgIdUsuarioModifica(String.valueOf(incidentes.get(i).getGidUsuarioModifica()));
                data.setgFechaCreado(incidentes.get(i).getGfechaCreado());
                data.setgDescripcionFormato(incidentes.get(i).getGdescripcionFormato());

                listData.add(data);
            }
            adaptador.setLista(listData);
        }

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_inspeccion, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                DrawerLayout drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
                drawer.openDrawer(Gravity.LEFT);
                break;
            case R.id.action_add:
                Intent intent = new Intent(getActivity(), IncidenteAcitvity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadDetalle(Integer idIncidente, String tipoInforme) {
        if (NetworkUtil.isOnline(requireContext())) {
            UtilMDP.showProgressDialog(getActivity(), "Obteniendo detalle...");
            String[] array = {idIncidente.toString(), tipoInforme};
            mViewModel.obtenerRegistoIncidente(array).observe(getActivity(), new Observer<IncidenteDetalleResponse>() {
                @Override
                public void onChanged(IncidenteDetalleResponse incidenteDetalleResponse) {
                    UtilMDP.hideProgreesDialog();
                    if (null != incidenteDetalleResponse) {
                        Intent intent = new Intent(getActivity(), IncidenteAcitvity.class);
                        intent.putExtra("incidente", incidenteDetalleResponse);
                        startActivity(intent);
                    }
                }
            });
        } else {
            CrearIncidenteEntity incidentes = mViewModel.findById(idIncidente);

            IncidenteDetalleResponse object = new IncidenteDetalleResponse();
            object.setIdIncidente(incidentes.getIdIncidente());
            object.setEmpleadorTipo(incidentes.getEmpleadorTipo());

            IncidenteDetalleResponse.EmpleadoridActividadEconomica empleadoridActividadEconomica = new IncidenteDetalleResponse.EmpleadoridActividadEconomica();
            empleadoridActividadEconomica.setIdElemento(incidentes.getFempleadorIdActividadEconomica());
            object.setEmpleadorIdActividadEconomica(empleadoridActividadEconomica);

            IncidenteDetalleResponse.Empleadoridempresa empleadoridempresa = new IncidenteDetalleResponse.Empleadoridempresa();
            empleadoridempresa.setIdElemento(incidentes.getFempleadorIdEmpresa());
            object.setEmpleadoridempresa(empleadoridempresa);

            IncidenteDetalleResponse.EmpleadoidtamanioEmpresa empleadoidtamanioEmpresa = new IncidenteDetalleResponse.EmpleadoidtamanioEmpresa();
            empleadoidtamanioEmpresa.setIdElemento(incidentes.getFempleadorIdTamanioEmpresa());
            object.setEmpleadorIdTamanioEmpresa(empleadoidtamanioEmpresa);

            IncidenteDetalleResponse.EmpleadorIIdActividadeconomica empleadorIIdActividadeconomica = new IncidenteDetalleResponse.EmpleadorIIdActividadeconomica();
            empleadorIIdActividadeconomica.setIdElemento(incidentes.getFempleadorIIdActividadEconomica());
            object.setEmpleadorIIdActividadeconomica(empleadorIIdActividadeconomica);

            object.setEmpleadorIRazonSocial(incidentes.getFempleadorIRazonSocial());
            object.setEmpleadorIRuc(incidentes.getFempleadorIRuc());
            object.setEmpleadorRazonSocial(incidentes.getFempleadorRazonSocial());
            object.setEmpleadorRuc(incidentes.getFempleadorRuc());
            object.setEventoFechaAccidente(incidentes.getFeventoFechaAccidente());
            object.setEventoFechaInicioInvestagacion(incidentes.getFeventoFechaInicioInvestigacion());
            object.setEventoHoraAccidente(incidentes.getFeventoHoraAccidente());
            object.setEventoHuboDanioMaterial(incidentes.getFeventoHuboDanioMaterial());

            IncidenteDetalleResponse.EventoIdEquipoAfectado eventoIdEquipoAfectado = new IncidenteDetalleResponse.EventoIdEquipoAfectado();
            eventoIdEquipoAfectado.setIdElemento(incidentes.getFeventoIdEquipoAfectado());
            object.setEventoIdEquipoAfectado(eventoIdEquipoAfectado);


            IncidenteDetalleResponse.EventoIdParteAfectada eventoIdParteAfectada = new IncidenteDetalleResponse.EventoIdParteAfectada();
            eventoIdParteAfectada.setIdElemento(incidentes.getFeventoIdParteAfectada());
            object.setEventoIdParteAfectada(eventoIdParteAfectada);

            IncidenteDetalleResponse.EventoIdTipoEvento tipoEvento = new IncidenteDetalleResponse.EventoIdTipoEvento();
            tipoEvento.setIdElemento(incidentes.getFeventoIdTipoEvento());
            object.setEventoIdTipoEvento(tipoEvento);

            object.setEventoLugarExacto(incidentes.getFeventoLugarExacto());
            object.setEventoNumeroTrabajadoresAfectados(incidentes.getFeventoNumTrabajadoresAfectadas());

            IncidenteDetalleResponse.Empleadoridempresa empleadoridempresa1 = new IncidenteDetalleResponse.Empleadoridempresa();
            empleadoridempresa1.setIdElemento(incidentes.getPempleadorIdEmpresa());
            object.setEmpleadoridempresa(empleadoridempresa1);

            IncidenteDetalleResponse.FempleadorIIdEmpresa fempleadorIIdEmpresa = new IncidenteDetalleResponse.FempleadorIIdEmpresa();
            fempleadorIIdEmpresa.setIdElemento(incidentes.getFempleadorIIdEmpresa());
            object.setFempleadorIIdEmpresa(fempleadorIIdEmpresa);

            object.setTrabajadorNombreApellido(incidentes.getFtrabajadorNombresApellidos());

            IncidenteDetalleResponse.GProyectoSede gProyectoSede = new IncidenteDetalleResponse.GProyectoSede();
            gProyectoSede.setIdElemento(incidentes.getGidProyectoSede());
            object.setIdProyectoSede(gProyectoSede);

            object.setTrabajadorNombreApellido(incidentes.getFtrabajadorNombresApellidos());
            object.setG_Fecha_Creado(incidentes.getGfechaCreado());
            object.setG_Fecha_Modifica(incidentes.getGidFechaModifica());

            IncidenteDetalleResponse.GUsuarioCreado gUsuarioCreado = new IncidenteDetalleResponse.GUsuarioCreado();
            gUsuarioCreado.setIdElemento(incidentes.getGidUsuarioCreado());
            object.setG_IdUsuario_Creado(gUsuarioCreado);

            IncidenteDetalleResponse.GUsarioModifica gUsarioModifica = new IncidenteDetalleResponse.GUsarioModifica();
            gUsarioModifica.setIdElemento(incidentes.getGidUsuarioModifica());
            object.setG_IdUsuario_Modifica(gUsarioModifica);

            object.setDescriptionIncidente(incidentes.getFdescripcionIncidente());
            object.setG_DescripcionFormato(incidentes.getGdescripcionFormato());
            object.setTipoInforme(incidentes.getTipoInformePF());

            Intent intent = new Intent(getActivity(), IncidenteAcitvity.class);
            intent.putExtra("incidente", object);
            startActivity(intent);

        }
    }

    private void onclickTable() {
        adaptador.setOnItemClickListener((int positionL, View vL) -> {
            if (vL.getId() == R.id.cardRoot) {
                if (NetworkUtil.isOnline(requireContext())) {
                    loadDetalle(adaptador.getIncidenteResponse(positionL).getIdIncidente(), adaptador.getIncidenteResponse(positionL).getTipoInformePF());
                } else {
                    loadDetalle(adaptador.getIncidenteResponse(positionL).getIdRow(), adaptador.getIncidenteResponse(positionL).getTipoInformePF());
                }
            }
        });
    }

    private void setToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        final ActionBar ab = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_menu, null);
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorBlanco));
            ab.setHomeAsUpIndicator(drawable);
        }
    }
}
