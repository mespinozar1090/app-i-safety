package com.mdp.fusionapp.ui.fragment.auditoria;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.ProyectoEntity;
import com.mdp.fusionapp.model.registro.AuditoriaInsertModel;
import com.mdp.fusionapp.database.entity.EmpresaSbcEntity;
import com.mdp.fusionapp.database.entity.SedeProyectoEntity;
import com.mdp.fusionapp.network.response.detalle.DetalleAuditoriaResponse;
import com.mdp.fusionapp.ui.activity.auditoria.AuditoriaActivity;
import com.mdp.fusionapp.utilitary.NetworkUtil;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.viewModel.InspeccionViewModel;
import com.mdp.fusionapp.viewModel.SbcViewModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class AuditoriaGeneralFragment extends Fragment implements View.OnClickListener {

    private SbcViewModel mViewModel;
    private InspeccionViewModel inspeccionViewModel;
    private Spinner spEmpresas, spSedeProyecto;
    private Integer idEmpresaContratista, idSedeProyecto;
    private String nombreProyecto;
    private Button btnSiguiente;
    private TextInputEditText tietNroContrato, tietsupervisor, tietresponsable;
    private DetalleAuditoriaResponse auditoria;
    private SharedPreferences sharedPreferences;

    Button btnFecha;
    private String fechaAuditoria;

    public static AuditoriaGeneralFragment newInstance(DetalleAuditoriaResponse detalle) {
        AuditoriaGeneralFragment fragment = new AuditoriaGeneralFragment();
        Bundle args = new Bundle();
        args.putSerializable("detalle", detalle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            auditoria = (DetalleAuditoriaResponse) getArguments().getSerializable("detalle");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.auditoria_general_fragment, container, false);
        inspeccionViewModel = new ViewModelProvider(this).get(InspeccionViewModel.class);
        mViewModel = new ViewModelProvider(this).get(SbcViewModel.class);
        initView(view);
        return view;
    }

    private Boolean validarObjectoDetalle() {
        return auditoria != null ? true : false;
    }

    private void initView(View view) {
        sharedPreferences = getActivity().getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        btnSiguiente = view.findViewById(R.id.btnSiguiente);
        spEmpresas = view.findViewById(R.id.spEmpresas);
        spSedeProyecto = view.findViewById(R.id.spSedeProyecto);
        tietNroContrato = view.findViewById(R.id.tietNroContrato);
        tietsupervisor = view.findViewById(R.id.tietsupervisor);
        tietresponsable = view.findViewById(R.id.tietresponsable);
        btnFecha = view.findViewById(R.id.btnFecha);
        btnFecha.setOnClickListener(this);
        btnSiguiente.setOnClickListener(this);

        tietsupervisor.setText("");
        tietNroContrato.setText("");
        tietresponsable.setText("");
        setHorayFecha();

        if (validarObjectoDetalle()) {
            tietsupervisor.setText(auditoria.getSupervisorContrato());
            tietNroContrato.setText(auditoria.getNroContrato());
            tietresponsable.setText(auditoria.getResponsableContratista());
            btnFecha.setText(auditoria.getFechaAudit());
            fechaAuditoria = auditoria.getFechaAudit();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (NetworkUtil.isOnline(getContext())) {
            mViewModel.obtenerEmpresaSBSResponse().observe(getActivity(), new Observer<List<EmpresaSbcEntity>>() {
                @Override
                public void onChanged(List<EmpresaSbcEntity> empresaSBSRespons) {
                    if (null != empresaSBSRespons)
                        loadEmpresas(empresaSBSRespons);
                }
            });
        } else {
            loadEmpresas(mViewModel.empresaContratistaOffline());
        }


        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("Id_Usuario", sharedPreferences.getInt("IdUsers", 0));
        hashMap.put("Id_Rol_Usuario", sharedPreferences.getInt("Id_Rol_Usuario", 0));
        hashMap.put("Id_Rol_General", sharedPreferences.getInt("Id_Rol_General", 0));
        hashMap.put("Modulo", "Inspe");

        List<ProyectoEntity> proyectoResponses = new ArrayList<>();
        proyectoResponses.add(new ProyectoEntity(0, "Seleccionar"));
        proyectoResponses.addAll(inspeccionViewModel.findAllProjectOffline(hashMap));
        spinnerSede(proyectoResponses);

       /* if (NetworkUtil.isOnline(getContext())) {
            inspeccionViewModel.obtenerProyectos(hashMap).observe(getActivity(), new Observer<List<ProyectoEntity>>() {
                @Override
                public void onChanged(List<ProyectoEntity> proyectoResponses) {

                    if (null != proyectoResponses) {
                        spinnerSede(proyectoResponses);
                    }
                }
            });
        } else {
            List<ProyectoEntity> proyectoResponses = new ArrayList<>();
            proyectoResponses.add(new ProyectoEntity(0, "Seleccionar"));
            proyectoResponses.addAll(inspeccionViewModel.findAllProjectOffline(hashMap));
            spinnerSede(proyectoResponses);
        }*/
    }

    public AuditoriaInsertModel saveAuditoriaGeneral() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        AuditoriaInsertModel object = new AuditoriaInsertModel();
        object.setEstadoAudit(1);
        object.setFechaAudit(dateFormat.format(new Date()));
        object.setIdEmpresaContratista(idEmpresaContratista);
        object.setNroContrato(tietNroContrato.getText().toString());
        object.setResponsable(tietresponsable.getText().toString());
        object.setSupervisorContrato(tietsupervisor.getText().toString());
        object.setIdSedeProyecto(idSedeProyecto);
        object.setNombreProyecto(nombreProyecto);
        object.setFechaAudit(fechaAuditoria);
        return object;
    }

    private void loadEmpresas(List<EmpresaSbcEntity> empresaSBSRespons) {
        ArrayAdapter<EmpresaSbcEntity> adapterTipoDocumento = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, empresaSBSRespons);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spEmpresas.setAdapter(adapterTipoDocumento);
        spEmpresas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                EmpresaSbcEntity selectedItem = (EmpresaSbcEntity) parent.getSelectedItem();
                idEmpresaContratista = selectedItem.getIdEmpresaObservadora();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (validarObjectoDetalle()) {
          /*  List<EmpresaSBSResponse> listEmpresa = empresaSBSResponses.stream().filter(data ->
                    data.getIdEmpresaObservadora() == auditoria.getIdEmpresaContratista())
                    .collect(Collectors.toList());

            Log.e("TAG", "loadEmpresas: size "+listEmpresa.size() +" "+listEmpresa.get(0).getDescripcion());*/
            for (int x = 0; x < empresaSBSRespons.size(); x++) {
                if (empresaSBSRespons.get(x).getIdEmpresaObservadora().intValue() == auditoria.getIdEmpresaContratista()) {
                    spEmpresas.setSelection(x);
                }
            }
        }
    }

    private void setHorayFecha() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        btnFecha.setText(dateFormat.format(new Date()));
        fechaAuditoria = dateFormat.format(new Date());
    }

    private void getFechaCalendar() {
        final Calendar calendario = Calendar.getInstance();
        int year = calendario.get(Calendar.YEAR);
        int month = calendario.get(Calendar.MONTH);
        int day = calendario.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, monthOfYear);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm:ss");
                fechaAuditoria = dateFormat1.format(cal.getTime());

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                dateFormat.setTimeZone(cal.getTimeZone());
                String currentDateString = dateFormat.format(cal.getTime());
                fechaAuditoria = currentDateString;
                btnFecha.setText(currentDateString);
                btnFecha.setTextSize(12);
                btnFecha.setTextColor(Color.BLACK);
            }
        }, year, month, day);

        Date miFecha = calendario.getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        // datePicker.getDatePicker().setMinDate(convertDateToMillis(dateFormat.format(miFecha)));
        datePicker.show();
    }

    private void spinnerSede(List<ProyectoEntity> sedeResponses) {
        ArrayAdapter<ProyectoEntity> adapterTipoDocumento = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, sedeResponses);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSedeProyecto.setAdapter(adapterTipoDocumento);
        spSedeProyecto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ProyectoEntity selectedItem = (ProyectoEntity) parent.getSelectedItem();
                idSedeProyecto = selectedItem.getIdProyecto();
                nombreProyecto = selectedItem.getNombre();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (validarObjectoDetalle()) {
            for (int x = 0; x < sedeResponses.size(); x++) {
                if (sedeResponses.get(x).getIdProyecto().intValue() == auditoria.getSede()) {
                    spSedeProyecto.setSelection(x);
                }
            }
        }
    }

    private Boolean valiarCampos() {

        if (idEmpresaContratista == 0) {
            Toast.makeText(getContext(), "Seleccione empresa contratista", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (idSedeProyecto == 0) {
            Toast.makeText(getContext(), "Seleccione nombre del proyecto", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (tietNroContrato.getText().toString() == null || tietNroContrato.getText().toString().trim().equals("")) {
            Toast.makeText(getContext(), "Ingrese número del contrato", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (tietsupervisor.getText().toString() == null || tietsupervisor.getText().toString().trim().equals("")) {
            Toast.makeText(getContext(), "Ingrese supervisor del contrato", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (tietresponsable.getText().toString() == null || tietresponsable.getText().toString().trim().equals("")) {
            Toast.makeText(getContext(), "Ingrese responsable STSOMARS", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void enablePager() {
        if (valiarCampos()) {
            ((AuditoriaActivity) getActivity()).disableScroll(false);
            ((AuditoriaActivity) getActivity()).setViewPager(1);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnFecha:
                getFechaCalendar();
                break;
            case R.id.btnSiguiente:
                enablePager();
                break;
        }
    }
}
