package com.mdp.fusionapp.ui.fragment.sbc;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.model.DetalleSBCModel;
import com.mdp.fusionapp.database.entity.CategoriaSbcItemEntity;
import com.mdp.fusionapp.database.entity.CategoriaSbcEntity;
import com.mdp.fusionapp.network.response.detalle.DetalleSbcResponse;
import com.mdp.fusionapp.database.entity.UsuarioNotificarEntity;
import com.mdp.fusionapp.ui.activity.ItemCategoriaSbcActivity;
import com.mdp.fusionapp.ui.activity.NotificacionActivity;
import com.mdp.fusionapp.ui.activity.sbc.CrearsbcActivity;
import com.mdp.fusionapp.ui.adapter.CategoriaSbcAdapter;
import com.mdp.fusionapp.viewModel.SbcViewModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class CategoriasbcFragment extends Fragment {

    private SbcViewModel mViewModel;
    private LinearLayoutManager linearLayout;
    private RecyclerView recyclerView;
    private CategoriaSbcAdapter adaptador;
    private List<CategoriaSbcEntity> lista;
    private FragmentManager fragmentManager;

    private List<CategoriaSbcItemEntity> listaItem;
    //  private List<CategoriaSbcItemResponse> listaDetalle;
    private List<CategoriaSbcItemEntity> listaItemConData;
    private List<DetalleSBCModel> listaItemInsert;
    private DetalleSbcResponse detalleSbcResponse;
    private List<UsuarioNotificarEntity> lstNotificacion;
    private Button btnGuardar;
    private static long mLastClickTime = 0;
    private static final int NOTIFICACION_ACTIVITY_REQUEST_CODE = 999;
    private static final int SECOND_ACTIVITY_REQUEST_CODE = 0;

    public static CategoriasbcFragment
    newInstance(DetalleSbcResponse detalle) {
        CategoriasbcFragment fragment = new CategoriasbcFragment();
        Bundle args = new Bundle();
        args.putSerializable("detalle", detalle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            detalleSbcResponse = (DetalleSbcResponse) getArguments().getSerializable("detalle");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_categoriasbc, container, false);
        mViewModel = new ViewModelProvider(this).get(SbcViewModel.class);
        initView(view);
        return view;
    }

    private void initView(View view) {
        btnGuardar = view.findViewById(R.id.btnGuardar);
        recyclerView = view.findViewById(R.id.recyclerview);
        linearLayout = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayout);
        lista = new ArrayList<>();
        lstNotificacion = new ArrayList<>();
        //    listaDetalle = new ArrayList<>();
        listaItem = new ArrayList<>();
        adaptador = new CategoriaSbcAdapter(getContext(), lista);
        recyclerView.setAdapter(adaptador);
        listaItemInsert = new ArrayList<>();

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                obtenerData();
            }
        });
        loadDetalle();
    }

    private void obtenerData() {
        for (int x = 0; x < listaItem.size(); x++) {
            listaItemInsert.add(new DetalleSBCModel(listaItem.get(x).getIdsbcCategoria(), listaItem.get(x).getIdsbcCategoria(),
                    listaItem.get(x).getIdbarrera(), listaItem.get(x).getIdsbcCategoriaItems(), 1, listaItem.get(x).getObservacionSbcDetalle(),
                    listaItem.get(x).getRiesgoso(), listaItem.get(x).getSeguro(),listaItem.get(x).getDescripcionItem()));
        }

        int contador = 0;
        for (int x = 0; x < lstNotificacion.size(); x++) {
            if (lstNotificacion.get(x).getSelectEmail()) {
                contador = contador + 1;
            }
        }
        if (contador > 0) {
            ((CrearsbcActivity) getActivity()).insertarSBC(listaItemInsert, lstNotificacion);
        } else {
            showValidarNotificacion();
        }
    }

    public void showValidarNotificacion() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setTitle("Notificación");
        alertDialogBuilder.setMessage("Debe seleccionar a quien notificar");
        alertDialogBuilder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int arg1) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private Boolean validarDetalle() {
        return detalleSbcResponse != null ? true : false;
    }

    private void loadDetalle() {
        if (validarDetalle()) {
            List<DetalleSbcResponse.Categoria> categorias = detalleSbcResponse.getLstDetalleSBC();
            CategoriaSbcItemEntity objecto;
            for (int x = 0; x < categorias.size(); x++) {
                objecto = new CategoriaSbcItemEntity();
                objecto.setIdsbcCategoria(categorias.get(x).getIdSBCCategoria());
                objecto.setIdsbcCategoriaItems(categorias.get(x).getIdSBCCategoriaItems());
                objecto.setIdbarrera(categorias.get(x).getIdbarrera());
                objecto.setRiesgoso(categorias.get(x).getRiesgoso());
                objecto.setSeguro(categorias.get(x).getSeguro());
                objecto.setDescripcionItem(categorias.get(x).getDescripcionItem());
                objecto.setObservacionSbcDetalle(categorias.get(x).getObservacionSBCDetalle());
                Log.e("TAG", "getDescripcionItem " + categorias.get(x).getDescripcionItem());
                listaItem.add(objecto);
            }
            // obtenerItemDeCategoriaDatelle(listaDetalle);
        } else {
            obtenerCategoriaItemSBC();
        }
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel.obtenerCategoriaSBC().observe(getActivity(), new Observer<List<CategoriaSbcEntity>>() {
            @Override
            public void onChanged(List<CategoriaSbcEntity> categoriaSbcRespons) {
                if (categoriaSbcRespons != null) {
                    lista = categoriaSbcRespons;
                    adaptador.setLista(lista);
                }
            }
        });
        onclickTable();
    }

    private void obtenerCategoriaItemSBC() {
        mViewModel.obtenerCategoriaItemSBC().observe(getActivity(), new Observer<List<CategoriaSbcItemEntity>>() {
            @Override
            public void onChanged(List<CategoriaSbcItemEntity> categoriaSbcItemRespons) {
                if (null != categoriaSbcItemRespons) {
                    listaItem = categoriaSbcItemRespons;
                }
            }
        });
    }

    private void onclickTable() {
        adaptador.setOnItemClickListener((int positionL, View vL) -> {
            if (vL.getId() == R.id.rootLayout) {
                Intent intent = new Intent(getActivity(), ItemCategoriaSbcActivity.class);
                intent.putExtra("listCategoriaItem", (Serializable) listaItem);
                intent.putExtra("idCategoria", adaptador.getCategoriaSbc(positionL).getIdsbcCategoria());
                startActivityForResult(intent, SECOND_ACTIVITY_REQUEST_CODE);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SECOND_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                assert data != null;
                listaItemConData = (List<CategoriaSbcItemEntity>) data.getSerializableExtra("oLista");
                setDataLista(listaItemConData);
                //    obtenerItemDeCategoria(listaItemConData);
            }
        } else if (requestCode == NOTIFICACION_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                lstNotificacion = (List<UsuarioNotificarEntity>) data.getSerializableExtra("lstNotificacion");
            }
        }
    }

    private void setDataLista(List<CategoriaSbcItemEntity> listaItemConData) {
        for (int y = 0; y < listaItemConData.size(); y++) {
            for (int x = 0; x < listaItem.size(); x++) {
                if (listaItem.get(x).getIdsbcCategoria().intValue() == listaItemConData.get(y).getIdsbcCategoria().intValue()) {
                    if (listaItem.get(x).getIdsbcCategoriaItems().intValue() == listaItemConData.get(y).getIdsbcCategoriaItems().intValue()) {
                        listaItem.get(x).setObservacionSbcDetalle(listaItemConData.get(y).getObservacionSbcDetalle());
                        listaItem.get(x).setIdbarrera(listaItemConData.get(y).getIdbarrera());
                        listaItem.get(x).setSeguro(listaItemConData.get(y).getSeguro());
                        listaItem.get(x).setRiesgoso(listaItemConData.get(y).getRiesgoso());
                        listaItem.get(x).setIdsbcCategoriaItems(listaItemConData.get(y).getIdsbcCategoriaItems());
                        listaItem.get(x).setIdsbcCategoria(listaItemConData.get(y).getIdsbcCategoria());
                        listaItem.get(x).setDescripcionItem(listaItemConData.get(y).getDescripcionItem());
                    }
                }
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_email, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.email_add:
                Intent intent = new Intent(getActivity(), NotificacionActivity.class);
                intent.putExtra("lstNotificacion", (Serializable) lstNotificacion);
                startActivityForResult(intent, NOTIFICACION_ACTIVITY_REQUEST_CODE);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
