package com.mdp.fusionapp.ui.fragment.incidente;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.incidente.EquipoEntity;
import com.mdp.fusionapp.database.entity.incidente.ParteCuerpoEntity;
import com.mdp.fusionapp.database.entity.incidente.TipoEventoEntity;
import com.mdp.fusionapp.model.IncidenteInsertModel;
import com.mdp.fusionapp.model.ItemModel;
import com.mdp.fusionapp.network.response.ComboMaestroEntity;
import com.mdp.fusionapp.network.response.IncidenteDetalleResponse;
import com.mdp.fusionapp.database.entity.SedeProyectoEntity;
import com.mdp.fusionapp.database.entity.UsuarioNotificarEntity;
import com.mdp.fusionapp.ui.activity.NotificacionActivity;
import com.mdp.fusionapp.ui.activity.incidentes.IncidenteAcitvity;
import com.mdp.fusionapp.utilitary.NetworkUtil;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.viewModel.IncidenteViewModel;
import com.mdp.fusionapp.viewModel.InspeccionViewModel;
import com.mdp.fusionapp.viewModel.SbcViewModel;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static android.app.Activity.RESULT_OK;


public class EventoFragment extends Fragment implements View.OnClickListener {
    private static final int NOTIFICACION_ACTIVITY_REQUEST_CODE = 999;
    private SbcViewModel mViewModel;
    private IncidenteViewModel incidenteViewModel;
    private InspeccionViewModel inspeccionViewModel;
    private IncidenteDetalleResponse incidente;
    private List<UsuarioNotificarEntity> lstNotificacion;
    private static long mLastClickTime = 0;
    TextInputEditText tietLugarExacto, tietNrotrabajadores;

    Button btnFecha, btnHora, btnGuardar;
    LinearLayout lnlCuerpo;
    Spinner spSedeProyecto, spTipoEvento, spDanio, spCuerpoAfectado, spEquiposInvolucrados;

    private String fechaIncidente, horaIncidente, fEventoHuboDanioMaterial, descripcionSede;
    private Integer fEventoIdEquipoAfectado, fEventoIdParteAfectada, fEventoIdTipoEvento, idSedeProyecto;

    public static EventoFragment newInstance(IncidenteDetalleResponse detalle) {
        EventoFragment fragment = new EventoFragment();
        Bundle args = new Bundle();
        args.putSerializable("detalle", detalle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            incidente = (IncidenteDetalleResponse) getArguments().getSerializable("detalle");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_evento, container, false);
        mViewModel = new ViewModelProvider(this).get(SbcViewModel.class);
        incidenteViewModel = new ViewModelProvider(this).get(IncidenteViewModel.class);
        inspeccionViewModel = new ViewModelProvider(this).get(InspeccionViewModel.class);
        initView(view);
        return view;
    }

    private void initView(View view) {
        btnGuardar = view.findViewById(R.id.btnGuardar);
        spSedeProyecto = view.findViewById(R.id.spSedeProyecto);
        spTipoEvento = view.findViewById(R.id.spTipoEvento);
        spDanio = view.findViewById(R.id.spDanio);
        spCuerpoAfectado = view.findViewById(R.id.spCuerpoAfectado);
        spEquiposInvolucrados = view.findViewById(R.id.spEquiposInvolucrados);
        lnlCuerpo = view.findViewById(R.id.lnlCuerpo);
        btnFecha = view.findViewById(R.id.btnFecha);
        btnHora = view.findViewById(R.id.btnHora);
        tietLugarExacto = view.findViewById(R.id.tietLugarExacto);
        tietNrotrabajadores = view.findViewById(R.id.tietNrotrabajadores);
        lstNotificacion = new ArrayList<>();
        btnFecha.setOnClickListener(this);
        btnHora.setOnClickListener(this);
        setHorayFecha();

        loadSede();
        loadtipoEvento();
        loadEquipoInvolucrados();
        spinnerdanio();
        loadLeccionCuerpo();
        tietLugarExacto.setText("");
        tietNrotrabajadores.setText("");

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                saveIncidenteEvento();
            }
        });
        loadDetalle();
    }

    private Boolean validarObject() {
        return incidente != null ? true : false;
    }

    private void loadDetalle() {
        if (validarObject()) {
            tietLugarExacto.setText(incidente.getEventoLugarExacto());
            tietNrotrabajadores.setText(incidente.getEventoNumeroTrabajadoresAfectados());
            btnFecha.setText(incidente.getEventoFechaAccidente());
            btnHora.setText(incidente.getEventoHoraAccidente());
        }
    }

    private void setHorayFecha() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
        btnFecha.setText(dateFormat.format(new Date()));
        fechaIncidente = dateFormat.format(new Date());
        btnHora.setText(hourFormat.format(new Date()));
        horaIncidente = hourFormat.format(new Date());
    }

    private void saveIncidenteEvento() {
        if (valiarCampos()) {
            int contador = 0;
            for (int x = 0; x < lstNotificacion.size(); x++) {
                if (lstNotificacion.get(x).getSelectEmail()) {
                    contador = contador + 1;
                }
            }
            if (contador > 0) {
                ((IncidenteAcitvity) getActivity()).insertarIncidente(saveIncidente(), lstNotificacion);
            } else {
                showValidarNotificacion();
            }
        }
    }

    private Boolean valiarCampos() {
        if (idSedeProyecto == 0) {
            Toast.makeText(getContext(), "Seleccione Sede / Proyecto", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (fEventoIdTipoEvento == 0) {
            Toast.makeText(getContext(), "Seleccione Tipo de Evento", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (fEventoHuboDanioMaterial.equals("Seleccionar")) {
            Toast.makeText(getContext(), "Seleccione si hubo daño material", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (fEventoIdEquipoAfectado == 0) {
            Toast.makeText(getContext(), "Seleccione Equipos involucrados", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void showValidarNotificacion() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setTitle("Notificación");
        alertDialogBuilder.setMessage("Debe seleccionar a quien notificar");
        alertDialogBuilder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int arg1) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private IncidenteInsertModel saveIncidente() {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        IncidenteInsertModel object = new IncidenteInsertModel();
        object.setfEventoFechaAccidente(fechaIncidente);
        object.setfDescripcionIncidente(dateFormat.format(date));
        object.setfEventoHoraAccidente(horaIncidente);
        object.setfEventoHuboDanioMaterial(fEventoHuboDanioMaterial);
        object.setfEventoIdEquipoAfectado(fEventoIdEquipoAfectado);
        object.setfEventoIdParteAfectada(fEventoIdParteAfectada);
        object.setfEventoIdTipoEvento(fEventoIdTipoEvento);
        object.setfEventoLugarExacto(tietLugarExacto.getText().toString());
        object.setfEventoNumTrabajadoresAfectadas(tietNrotrabajadores.getText().toString());
        object.setfTrabajadorDetalleCategoriaOcupacional("");
        object.setgIdProyectoSede(idSedeProyecto);
        object.setDescripcionSede(descripcionSede);
        object.setfEventoFechaInicioInvestigacion(fechaIncidente);
        return object;
    }

    private void loadSede() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("Modulo", "Incidente");
        if (NetworkUtil.isOnline(requireContext())) {
            mViewModel.obtenerSede(hashMap).observe(getActivity(), new Observer<List<SedeProyectoEntity>>() {
                @Override
                public void onChanged(List<SedeProyectoEntity> sedeResponses) {
                    if (null != sedeResponses) {
                        spinnerSede(sedeResponses);
                    }
                }
            });
        } else {
            spinnerSede(mViewModel.obtenerSedeOffline(hashMap));
        }

    }

    private void getFechaCalendar() {
        final Calendar calendario = Calendar.getInstance();
        int year = calendario.get(Calendar.YEAR);
        int month = calendario.get(Calendar.MONTH);
        int day = calendario.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, monthOfYear);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm:ss");
                fechaIncidente = dateFormat1.format(cal.getTime());

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                dateFormat.setTimeZone(cal.getTimeZone());
                String currentDateString = dateFormat.format(cal.getTime());
                fechaIncidente = currentDateString;
                btnFecha.setText(currentDateString);
                btnFecha.setTextSize(12);
                btnFecha.setTextColor(Color.BLACK);
            }
        }, year, month, day);

        Date miFecha = calendario.getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        // datePicker.getDatePicker().setMinDate(convertDateToMillis(dateFormat.format(miFecha)));
        datePicker.show();
    }

    private void getHoraCalendar() {
        final Calendar calendario = Calendar.getInstance();
        int hora = calendario.get(Calendar.HOUR_OF_DAY);
        int minuto = calendario.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                horaIncidente = hourOfDay + ":" + minute;
                btnHora.setText(hourOfDay + ":" + minute);
                btnHora.setTextSize(12);
                btnHora.setTextColor(Color.BLACK);
            }
        }, hora, minuto, true);

        timePickerDialog.show();
    }

    private void spinnerSede(List<SedeProyectoEntity> sedeResponses) {
        ArrayAdapter<SedeProyectoEntity> adapterTipoDocumento = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, sedeResponses);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSedeProyecto.setAdapter(adapterTipoDocumento);
        spSedeProyecto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SedeProyectoEntity selectedItem = (SedeProyectoEntity) parent.getSelectedItem();
                idSedeProyecto = selectedItem.getIdSedeProyecto();
                descripcionSede = selectedItem.getDescripcion();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (validarObject()) {
            for (int x = 0; x < sedeResponses.size(); x++) {
                if (sedeResponses.get(x).getIdSedeProyecto().intValue() == incidente.getIdProyectoSede().getIdElemento().intValue()) {
                    spSedeProyecto.setSelection(x);
                }
            }
        }
    }

    private void loadtipoEvento() {
        if (NetworkUtil.isOnline(requireContext())) {
            String[] array = {"4", "1"};
            inspeccionViewModel.tipoEvento(array).observe(getActivity(), new Observer<List<TipoEventoEntity>>() {
                @Override
                public void onChanged(List<TipoEventoEntity> comboMaestroRespons) {
                    if (null != comboMaestroRespons) {
                        spinnerEvento(comboMaestroRespons);
                    }
                }
            });
        } else {
            spinnerEvento(inspeccionViewModel.tipoEventoOffline());
        }

    }

    private void spinnerEvento(List<TipoEventoEntity> comboMaestroRespons) {
        ArrayAdapter<TipoEventoEntity> adapterTipoDocumento = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, comboMaestroRespons);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTipoEvento.setAdapter(adapterTipoDocumento);
        spTipoEvento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TipoEventoEntity selectedItem = (TipoEventoEntity) parent.getSelectedItem();
                lnlCuerpo.setVisibility(selectedItem.getIdElemento() == 23 ? View.VISIBLE : View.GONE);
                fEventoIdTipoEvento = selectedItem.getIdElemento();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (validarObject()) {
            for (int x = 0; x < comboMaestroRespons.size(); x++) {
                if (null != incidente.getEventoIdTipoEvento()) {
                    if (comboMaestroRespons.get(x).getIdElemento().intValue() == incidente.getEventoIdTipoEvento().getIdElemento()) {
                        spTipoEvento.setSelection(x);
                    }
                }
            }
        }
    }

    private void spinnerdanio() {
        List<ItemModel> danio = UtilMDP.huboDanio();
        ArrayAdapter<ItemModel> adapterTipoDocumento = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, danio);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDanio.setAdapter(adapterTipoDocumento);
        spDanio.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ItemModel selectedItem = (ItemModel) parent.getSelectedItem();
                fEventoHuboDanioMaterial = selectedItem.getNombre();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (validarObject()) {
            for (int x = 0; x < danio.size(); x++) {
                if (danio.get(x).getNombre().equals(incidente.getEventoHuboDanioMaterial())) {
                    spDanio.setSelection(x);
                }
            }
        }
    }


    private void loadEquipoInvolucrados() {
        if (NetworkUtil.isOnline(requireContext())) {
            String[] array = {"tb_MAESTRA_Equipos", "1"};
            incidenteViewModel.equipoInvolucrado(array).observe(getActivity(), new Observer<List<EquipoEntity>>() {
                @Override
                public void onChanged(List<EquipoEntity> comboMaestroRespons) {
                    if (null != comboMaestroRespons) {
                        spinnerEquipoInvolucrado(comboMaestroRespons);
                    }
                }
            });
        } else {
            spinnerEquipoInvolucrado(incidenteViewModel.

                    equipoInvolucradoOffline());
        }

    }

    private void spinnerEquipoInvolucrado(List<EquipoEntity> comboMaestroRespons) {
        ArrayAdapter<EquipoEntity> adapterTipoDocumento = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, comboMaestroRespons);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spEquiposInvolucrados.setAdapter(adapterTipoDocumento);
        spEquiposInvolucrados.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                EquipoEntity selectedItem = (EquipoEntity) parent.getSelectedItem();
                fEventoIdEquipoAfectado = selectedItem.getIdElemento();
                Log.e("idEquipoAfectado", " : " + fEventoIdEquipoAfectado);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }

        });

        if (validarObject()) {
            for (int x = 0; x < comboMaestroRespons.size(); x++) {
                if (null != incidente.getEventoIdEquipoAfectado().getIdElemento()) {
                    if (comboMaestroRespons.get(x).getIdElemento().intValue() == incidente.getEventoIdEquipoAfectado().getIdElemento()) {
                        spEquiposInvolucrados.setSelection(x);
                    }
                }
            }
        }
    }

    private void loadLeccionCuerpo() {
        if (NetworkUtil.isOnline(requireContext())) {
            String[] array = {"tb_MAESTRA_ParteCuerpo", "1"};
            incidenteViewModel.parteCuerpoInvolucrado(array).observe(getActivity(), new Observer<List<ParteCuerpoEntity>>() {
                @Override
                public void onChanged(List<ParteCuerpoEntity> comboMaestroRespons) {
                    if (null != comboMaestroRespons) {
                        spinnerLeccionCuerpo(comboMaestroRespons);
                    }
                }
            });
        } else {
            List<ParteCuerpoEntity> parteCuerpoEntities = incidenteViewModel.parteCuerpoInvolucradoOffline();
            spinnerLeccionCuerpo(parteCuerpoEntities);
        }
    }

    private void spinnerLeccionCuerpo(List<ParteCuerpoEntity> comboMaestroRespons) {
        ArrayAdapter<ParteCuerpoEntity> adapterTipoDocumento = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, comboMaestroRespons);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCuerpoAfectado.setAdapter(adapterTipoDocumento);
        spCuerpoAfectado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ParteCuerpoEntity selectedItem = (ParteCuerpoEntity) parent.getSelectedItem();
                fEventoIdParteAfectada = selectedItem.getIdElemento();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (validarObject()) {
            if (null != incidente.getEventoIdParteAfectada()) {
                for (int x = 0; x < comboMaestroRespons.size(); x++) {
                    if (comboMaestroRespons.get(x).getIdElemento().intValue() == incidente.getEventoIdParteAfectada().getIdElemento()) {
                        spCuerpoAfectado.setSelection(x);
                    }
                }
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_email, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.email_add:
                Intent intent = new Intent(getActivity(), NotificacionActivity.class);
                intent.putExtra("lstNotificacion", (Serializable) lstNotificacion);
                startActivityForResult(intent, NOTIFICACION_ACTIVITY_REQUEST_CODE);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == NOTIFICACION_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                lstNotificacion = (List<UsuarioNotificarEntity>) data.getSerializableExtra("lstNotificacion");
            }
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnFecha:
                getFechaCalendar();
                break;
            case R.id.btnHora:
                getHoraCalendar();
                break;
        }
    }
}
