package com.mdp.fusionapp.ui.fragment.interventoria;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.model.DetalleControlInterModel;
import com.mdp.fusionapp.model.ItemModel;
import com.mdp.fusionapp.database.entity.interventoria.ControlSubItemEntity;
import com.mdp.fusionapp.database.entity.UsuarioNotificarEntity;
import com.mdp.fusionapp.network.response.interventoria.InterventoriaDetalleResponse;
import com.mdp.fusionapp.ui.activity.NotificacionActivity;
import com.mdp.fusionapp.ui.activity.interventoria.ControlSubItemActivity;
import com.mdp.fusionapp.ui.activity.interventoria.InterventoriaActivity;
import com.mdp.fusionapp.ui.adapter.interventoria.ControlPersonalAdapter;
import com.mdp.fusionapp.utilitary.NetworkUtil;
import com.mdp.fusionapp.viewModel.InterventoriaViewModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;


public class ControlInterFragment extends Fragment {

    // CONEXION A SERIVICO WEB
    private InterventoriaViewModel interventoriaViewModel;
    private InterventoriaDetalleResponse interventoria;
    List<InterventoriaDetalleResponse.ControlDetalle> lstDetalle;
    private List<UsuarioNotificarEntity> lstNotificacion;

    // VARIABLE DE FUNCIONALIDAD
    private LinearLayoutManager linearLayout;
    private RecyclerView recyclerView;
    private ControlPersonalAdapter adaptador;
    private List<ItemModel> lista;
    private FragmentManager fragmentManager;
    private List<ControlSubItemEntity> listaControlSubItem;
    private List<ControlSubItemEntity> listaItemData;
    private List<DetalleControlInterModel> listaItemInsert;
    private Button btnGuardar;
    private static final int SECOND_ACTIVITY_REQUEST_CODE = 0;
    private static final int NOTIFICACION_ACTIVITY_REQUEST_CODE = 999;
    private static long mLastClickTime = 0;

    public static ControlInterFragment newInstance(InterventoriaDetalleResponse detalle) {
        ControlInterFragment fragment = new ControlInterFragment();
        Bundle args = new Bundle();
        args.putSerializable("detalle", detalle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            interventoria = (InterventoriaDetalleResponse) getArguments().getSerializable("detalle");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_control_inter, container, false);

        initView(view);
        loadControlPersonal();
        return view;
    }

    private void initView(View view) {
        interventoriaViewModel = new ViewModelProvider(this).get(InterventoriaViewModel.class);
        btnGuardar = view.findViewById(R.id.btnGuardar);
        recyclerView = view.findViewById(R.id.recyclerview);
        linearLayout = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayout);
        lista = new ArrayList<>();
        listaItemData = new ArrayList<>();
        listaItemInsert = new ArrayList<>();
        listaControlSubItem = new ArrayList<>();
        lstNotificacion = new ArrayList<>();
        adaptador = new ControlPersonalAdapter(getContext(), lista);
        recyclerView.setAdapter(adaptador);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                obtenerItemControl(listaControlSubItem);
            }
        });
        loadDetalle();
    }

    private void obtenerItemControl(List<ControlSubItemEntity> oLista) {
        for (int x = 0; x < oLista.size(); x++) {
            listaItemInsert.add(new DetalleControlInterModel(oLista.get(x).getCantidad(), oLista.get(x).getIdControl(), oLista.get(x).getIdControlitem(), oLista.get(x).getIdControlDetalle()));
        }
        int contador = 0;
        for (int x = 0; x < lstNotificacion.size(); x++) {
            if (lstNotificacion.get(x).getSelectEmail()) {
                contador = contador + 1;
            }
        }

        if (contador > 0) {
            Log.e("obtenerItemControl", "ewtrooooo");
            ((InterventoriaActivity) getActivity()).insertarControl(listaItemInsert, lstNotificacion);
            ((InterventoriaActivity) getActivity()).disableScroll(false);
            ((InterventoriaActivity) getActivity()).setViewPager(3);
        } else {
            // SE VA MOSTRAR UN MODAL
            showValidarNotificacion();
        }
    }


    public void showValidarNotificacion() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setTitle("Notificación");
        alertDialogBuilder.setMessage("Debe seleccionar a quien notificar");
        alertDialogBuilder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int arg1) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void loadDetalle() {
        if (validarObjectoDetalle()) {
            lstDetalle = new ArrayList<>();
            lstDetalle = interventoria.getData().getLstControlesDet();
            ControlSubItemEntity object;
            for (int x = 0; x < lstDetalle.size(); x++) {
                object = new ControlSubItemEntity();
                object.setCantidad(lstDetalle.get(x).getCantidad());
                object.setDescripcion(lstDetalle.get(x).getDescripcion());
                object.setIdControl(lstDetalle.get(x).getIdControl());
                object.setIdControlitem(lstDetalle.get(x).getIdControlItem());
                object.setIdControlDetalle(lstDetalle.get(x).getIdControlDetalle());
                listaControlSubItem.add(object);
            }
        } else {
            loadControlSubItem();
        }

    }

    private Boolean validarObjectoDetalle() {
        return interventoria != null ? true : false;
    }

    private void loadControlSubItem() {
        if (NetworkUtil.isOnline(getContext())) {
            interventoriaViewModel.obtenerControlSubItem().observe(getActivity(), new Observer<List<ControlSubItemEntity>>() {
                @Override
                public void onChanged(List<ControlSubItemEntity> controlSubItemRespons) {
                    if (null != controlSubItemRespons) {
                        listaControlSubItem = controlSubItemRespons;
                    }
                }
            });
        } else {
            listaControlSubItem = interventoriaViewModel.obtenerControlSubItemOffline();
        }
    }

    private void loadControlPersonal() {
        List<ItemModel> itemModels = new ArrayList<>();
        itemModels.add(new ItemModel(1, "MANO DE OBRA"));
        itemModels.add(new ItemModel(2, "TRABAJOS PREVIOS"));
        itemModels.add(new ItemModel(3, "VEHICULO"));
        itemModels.add(new ItemModel(4, "OTROS VEHICULOS"));
        itemModels.add(new ItemModel(5, "MATERIALES E INSUMOS"));
        itemModels.add(new ItemModel(6, "EQUIPOS Y HERRAMIENTAS"));
        itemModels.add(new ItemModel(7, "EQUIPOS Y HERRAMIENTAS ESPECIALES"));

        adaptador.setLista(itemModels);
        onclickTable();
    }

    private void onclickTable() {
        adaptador.setOnItemClickListener((int positionL, View vL) -> {
            if (vL.getId() == R.id.rootLayout) {
                Intent intent = new Intent(getActivity(), ControlSubItemActivity.class);
                intent.putExtra("listaControlSubItem", (Serializable) listaControlSubItem);
                intent.putExtra("idControl", adaptador.getControlPersona(positionL).getId());
                startActivityForResult(intent, SECOND_ACTIVITY_REQUEST_CODE);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SECOND_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                listaItemData = (List<ControlSubItemEntity>) data.getSerializableExtra("oLista");
                setDataLista(listaItemData);
            }
        } else if (requestCode == NOTIFICACION_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                lstNotificacion = (List<UsuarioNotificarEntity>) data.getSerializableExtra("lstNotificacion");
            }
        }
    }

    private void setDataLista(List<ControlSubItemEntity> listaItemData) {
        Log.e("listaControlSubItem 1", " : " + listaControlSubItem.size());
        for (int x = 0; x < listaControlSubItem.size(); x++) {
            for (int y = 0; y < listaItemData.size(); y++) {
                if (listaControlSubItem.get(x).getIdControl().intValue() == listaItemData.get(y).getIdControl().intValue()) {
                    if (listaControlSubItem.get(x).getIdControlitem().intValue() == listaItemData.get(y).getIdControlitem().intValue()) {
                        listaControlSubItem.get(x).setCantidad(listaItemData.get(y).getCantidad());
                    }
                }
            }
        }
        Log.e("listaControlSubItem 2", " : " + listaControlSubItem.size());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_email, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.email_add:
                Intent intent = new Intent(getActivity(), NotificacionActivity.class);
                intent.putExtra("lstNotificacion", (Serializable) lstNotificacion);
                startActivityForResult(intent, NOTIFICACION_ACTIVITY_REQUEST_CODE);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
