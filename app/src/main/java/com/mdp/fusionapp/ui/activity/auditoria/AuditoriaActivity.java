package com.mdp.fusionapp.ui.activity.auditoria;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.material.tabs.TabLayout;
import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.auditoria.CrearAuditoriaEntity;
import com.mdp.fusionapp.database.entity.auditoria.CrearDetalleAudotiraEntity;
import com.mdp.fusionapp.database.entity.auditoria.CrearLineamientoEntity;
import com.mdp.fusionapp.database.entity.sbc.NotificarEntity;
import com.mdp.fusionapp.model.LineamientoSubModel;
import com.mdp.fusionapp.model.registro.AuditoriaInsertModel;
import com.mdp.fusionapp.network.response.Respuesta;
import com.mdp.fusionapp.network.response.detalle.DetalleAuditoriaResponse;
import com.mdp.fusionapp.database.entity.UsuarioNotificarEntity;
import com.mdp.fusionapp.ui.fragment.auditoria.AuditoriaGeneralFragment;
import com.mdp.fusionapp.ui.fragment.auditoria.EvaluacionesFragment;
import com.mdp.fusionapp.utilitary.Constante;
import com.mdp.fusionapp.utilitary.CustomViewPager;
import com.mdp.fusionapp.utilitary.NetworkUtil;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.viewModel.ListaAuditoriaViewModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class AuditoriaActivity extends AppCompatActivity {

    private ListaAuditoriaViewModel viewModel;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private CustomViewPager mViewPager;
    private TabLayout tabLayout;
    private DetalleAuditoriaResponse auditoria;
    private AuditoriaInsertModel auditoriaInsertModel;
    private SharedPreferences sharedPreferences;

    private int[] tabIcons = {
            R.drawable.ic_add,
            R.drawable.ic_back
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auditoria);
        viewModel = new ViewModelProvider(this).get(ListaAuditoriaViewModel.class);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        auditoriaInsertModel = new AuditoriaInsertModel();
        initView();
        setToolbar();

    }

    private void initView() {
        auditoria = (DetalleAuditoriaResponse) getIntent().getSerializableExtra("auditoria");
        sharedPreferences = getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.disableScroll(true);
        mViewPager.setOffscreenPageLimit(3);

        tabLayout = findViewById(R.id.tabs);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        setUntouchableTab();
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    AuditoriaGeneralFragment auditoriaGeneralFragment = new AuditoriaGeneralFragment().newInstance(auditoria);
                    return auditoriaGeneralFragment;
                case 1:
                    EvaluacionesFragment evaluacionesFragment = new EvaluacionesFragment().newInstance(auditoria);
                    return evaluacionesFragment;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    private void setUntouchableTab() {
        LinearLayout tabStrip = ((LinearLayout) tabLayout.getChildAt(0));
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }
    }

    public void disableScroll(Boolean valor) {
        mViewPager.disableScroll(valor);
    }

    public void setViewPager(int page) {
        mViewPager.setCurrentItem(page);
    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_back, null);
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorBlanco));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private static String makeFragmentName(int viewId, long id) {
        return "android:switcher:" + viewId + ":" + id;
    }

    private Boolean validarObject() {
        return auditoria != null ? true : false;
    }

    private void obtenerDataGeneral() {
        String fragmentTag = makeFragmentName(mViewPager.getId(), 0);
        FragmentManager fm = getSupportFragmentManager();
        AuditoriaGeneralFragment fragment = (AuditoriaGeneralFragment) fm.findFragmentByTag(fragmentTag);
        if (null != fragment) {
            auditoriaInsertModel = fragment.saveAuditoriaGeneral();
        }
    }

    public void inserAuditoria(List<LineamientoSubModel> lstLineamientos, List<UsuarioNotificarEntity> lstNotificacion) {
        UtilMDP.showProgressDialog(this, "Guardando...");
        obtenerDataGeneral();
        List<HashMap<String, Object>> lstNotificacions = new ArrayList<>();
        HashMap<String, Object> notificacion = null;
        for (int x = 0; x < lstNotificacion.size(); x++) {
            if (lstNotificacion.get(x).getSelectEmail()) {
                notificacion = new HashMap<>();
                notificacion.put("IdUser", lstNotificacion.get(x).getIdUsers());
                notificacion.put("Nombre", lstNotificacion.get(x).getNombreUsuario());
                notificacion.put("Email", lstNotificacion.get(x).getEmailCorporativo());
                lstNotificacions.add(notificacion);
            }
        }


        List<HashMap<String, Object>> linemientoMapLis = new ArrayList<>();
        HashMap<String, Object> liniamientos = null;
        List<String> evidenciaPicture;

        for (int x = 0; x < lstLineamientos.size(); x++) {
            evidenciaPicture = new ArrayList<>();
            liniamientos = new HashMap<>();
            liniamientos.put("Id_Audit_Detalle", lstLineamientos.get(x).getIdAuditDetalle());
            liniamientos.put("Id_Audit_Items", lstLineamientos.get(x).getIdAuditItems());
            liniamientos.put("Id_Audit_Lin_SubItems", lstLineamientos.get(x).getIdAuditLinSubItems());
            liniamientos.put("Calificacion", lstLineamientos.get(x).getIdCalificacion());
            if (!lstLineamientos.get(x).getImgbase64().equals("")) {
                evidenciaPicture.add(lstLineamientos.get(x).getImgbase64());
            }

            liniamientos.put("LstFoto", evidenciaPicture);
            liniamientos.put("Evidencia", "cas");
            liniamientos.put("Lugar", lstLineamientos.get(x).getIdLugar());
            liniamientos.put("Notas", lstLineamientos.get(x).getMensage());
            liniamientos.put("TextEvidencia", lstLineamientos.get(x).getMensage());
            linemientoMapLis.add(liniamientos);
        }

        List<HashMap<String, Object>> lstDetallePlan = new ArrayList<>();
        HashMap<String, Object> detallePlan = new HashMap<>();
        lstDetallePlan.add(detallePlan);

        String nombreFormato = "Auditoria" + sharedPreferences.getString("rol", "")
                + sharedPreferences.getString("nombreUsuario", "") + auditoriaInsertModel.getFechaAudit() + auditoriaInsertModel.getNombreProyecto();


        HashMap<String, Object> hashMap = new HashMap<>();
        if (null != auditoria && auditoria.getIdAudit() != null) {
            hashMap.put("Id_Audit", auditoria.getIdAudit());
        } else {
            hashMap.put("Id_Audit", 0);
        }
        hashMap.put("Estado_Audit", auditoriaInsertModel.getEstadoAudit());
        hashMap.put("Fecha_Audit", auditoriaInsertModel.getFechaAudit());
        hashMap.put("Id_Empresa_contratista", auditoriaInsertModel.getIdEmpresaContratista());
        hashMap.put("IdUsers", sharedPreferences.getInt("IdUsers", 0));
        hashMap.put("LstDetalleAudit", linemientoMapLis);
        hashMap.put("LstDetallePlan", lstDetallePlan);
        hashMap.put("Nombre_Formato", nombreFormato);
        hashMap.put("Nro_Contrato", auditoriaInsertModel.getNroContrato());
        hashMap.put("Sede", auditoriaInsertModel.getIdSedeProyecto());
        hashMap.put("Supervisor_Contrato", auditoriaInsertModel.getSupervisorContrato());
        hashMap.put("Responsable_Contratista", auditoriaInsertModel.getResponsable());
        hashMap.put("Origen_Registro", "App");
        hashMap.put("UsuariosNotificados", lstNotificacions);


        if (NetworkUtil.isOnline(getApplicationContext())) {
            if (null != auditoria) {
                viewModel.editarAuditoria(hashMap).observe(this, new Observer<Respuesta>() {
                    @Override
                    public void onChanged(Respuesta respuesta) {
                        UtilMDP.hideProgreesDialog();
                        if (null != respuesta) {
                            if (respuesta.getEstatus()) {
                                finish();
                            }
                        }

                    }
                });
            } else {
                viewModel.insertaAuditoria(hashMap, getApplicationContext()).observe(this, new Observer<Respuesta>() {
                    @Override
                    public void onChanged(Respuesta respuesta) {
                        UtilMDP.hideProgreesDialog();
                        if (null != respuesta) {
                            if (respuesta.getEstatus()) {
                                finish();
                            }
                        }

                    }
                });
            }
        } else {
            guardarRegistroOffline(lstLineamientos, lstNotificacion);
        }
    }

    private void guardarRegistroOffline(List<LineamientoSubModel> lstLineamientos, List<UsuarioNotificarEntity> lstNotificacion) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String nombreFormato = "Auditoria" + sharedPreferences.getString("rol", "")
                + sharedPreferences.getString("nombreUsuario", "") + auditoriaInsertModel.getFechaAudit() + auditoriaInsertModel.getNombreProyecto();

        CrearAuditoriaEntity object = new CrearAuditoriaEntity();
        //  object.setIdAuditoria(auditoria.getIdAudit());
        object.setEstadoRegistroDB(Constante.ESTADO_CREADO);
        object.setEstadoAuditoria(auditoriaInsertModel.getEstadoAudit());
        object.setFechaAuditoria(auditoriaInsertModel.getFechaAudit());
        object.setIdEmpresaContratista(auditoriaInsertModel.getIdEmpresaContratista());
        object.setIdUser(sharedPreferences.getInt("IdUsers", 0));
        object.setNombreFormato(nombreFormato);
        object.setCodigoAuditoria("Cod - " + dateFormat.format(new Date()));
        object.setSede(auditoriaInsertModel.getIdSedeProyecto());
        object.setNroContrato(auditoriaInsertModel.getNroContrato());
        object.setResponsableContratista(auditoriaInsertModel.getResponsable());
        object.setSupervisorContrato(auditoriaInsertModel.getSupervisorContrato());
        object.setNombreAuditor(sharedPreferences.getString("nombreUsuario", ""));
        object.setFechaRegistro(dateFormat.format(new Date()));

        if (null != auditoria && auditoria.getIdRow() != null) {
            object.setEstadoRegistroDB(Constante.ESTADO_EDITADO);
            object.setId(auditoria.getIdRow());
        }

        Long idRow = viewModel.insertAuditoriaOffline(object);
        if (idRow != null) {
            CrearDetalleAudotiraEntity lineamientoEntity = null;
            viewModel.deleteByIdAuditoria(idRow);
            for (int x = 0; x < 38; x++) {
                lineamientoEntity = new CrearDetalleAudotiraEntity();
                lineamientoEntity.setIdAuditDetalle(lstLineamientos.get(x).getIdAuditDetalle());
                lineamientoEntity.setIdAuditItems(lstLineamientos.get(x).getIdAuditItems());
                lineamientoEntity.setIdAuditLinSubItems(lstLineamientos.get(x).getIdAuditLinSubItems());
                lineamientoEntity.setCalificacion(lstLineamientos.get(x).getIdCalificacion());
                lineamientoEntity.setLugar(lstLineamientos.get(x).getIdLugar());
                lineamientoEntity.setNotas(lstLineamientos.get(x).getMensage());
                lineamientoEntity.setDescripcionItem(lstLineamientos.get(x).getDescripcionItems());
                lineamientoEntity.setImagenEvidencia(lstLineamientos.get(x).getImgbase64());
                lineamientoEntity.setIdOffline(idRow);
                viewModel.insetDetalleAudtoriaOffline(lineamientoEntity);
            }

            for (int x = 0; x < lstNotificacion.size(); x++) {
                NotificarEntity notificarEntity = null;
                if (lstNotificacion.get(x).getSelectEmail()) {
                    notificarEntity = new NotificarEntity();
                    notificarEntity.setIdUser(lstNotificacion.get(x).getIdUsers());
                    notificarEntity.setNombre(lstNotificacion.get(x).getNombreUsuario());
                    notificarEntity.setEmail(lstNotificacion.get(x).getEmailCorporativo());
                    notificarEntity.setIdModulo(Constante.MODULO_AUDITORIA);
                    notificarEntity.setIdOffline(idRow);
                    viewModel.insertNotificarOffline(notificarEntity);
                }
            }
            UtilMDP.hideProgreesDialog();
            showCreateRegistro("Estimado, tu registro se ha guardado con éxito. Cuando haya conexión a Internet se sincronizara con la web");
        } else {
            UtilMDP.hideProgreesDialog();
        }
    }

    public void showCreateRegistro(String mensaje) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Registro Guardado");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage(mensaje);
        alertDialogBuilder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                finish();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}

