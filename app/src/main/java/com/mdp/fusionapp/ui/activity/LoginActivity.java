package com.mdp.fusionapp.ui.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.controller.LoginController;
import com.mdp.fusionapp.database.entity.UsuarioEntity;
import com.mdp.fusionapp.database.serviceImpl.UsuarioServiceImpl;
import com.mdp.fusionapp.model.UsuarioModel;
import com.mdp.fusionapp.network.response.LoginResponse;
import com.mdp.fusionapp.network.service.APIService;
import com.mdp.fusionapp.utilitary.NetworkUtil;
import com.mdp.fusionapp.utilitary.NoConnectivityException;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.viewModel.LoginViewModel;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends LoginController implements View.OnClickListener {

    private UsuarioServiceImpl usuarioServiceImpl;
    private LoginViewModel loginViewModel;
    private TextInputLayout tilUsuario, tilPassword;
    private TextInputEditText tietUsuario, tietPassword;
    private Button btnLogin;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    private static final int REQUEST_EXTERNAL_STORAGE = 1;

    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  setContentView(R.layout.activity_login);
        setupView();
        verifyStoragePermissions(this);
        usuarioServiceImpl = new ViewModelProvider(this).get(UsuarioServiceImpl.class);
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
    }


    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_login;
    }

    private void setupView() {
        sharedPreferences = getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        tilUsuario = findViewById(R.id.tilUsuario);
        tietUsuario = findViewById(R.id.tietUsuario);
        tilPassword = findViewById(R.id.tilPassword);
        tietPassword = findViewById(R.id.tietPassword);
        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);

        Integer idUser = sharedPreferences.getInt("IdUsers", 0);
        if (idUser > 0) {
            Intent intent = new Intent(LoginActivity.this, NavigationActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    private void loginAcceso(String usuario, String clave) {
        UtilMDP.showProgressDialog(this, "Iniciando...");
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("User", usuario);
        hashMap.put("Pass", clave);

        if (NetworkUtil.isOnline(getApplicationContext())) {
            APIService apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
            Call<LoginResponse> loginResponseCall = apiService.loginAcceso(hashMap);

            loginResponseCall.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    if (response.body() != null) {
                        if (null != response.body().getData()) {
                            insertUser(response.body());
                        } else {
                            UtilMDP.hideProgreesDialog();
                            if(usuario.toLowerCase().contains("@rep.com.pe")){
                                showModal("Ahora puedes ingresar con tu contraseña de tu cuenta de REP, en caso el error persista significa que tu correo no se encuentra en el AD.");
                            }else{
                                showModal(" Ingresar correctamente la contraseña. En caso el error persista por favor, contactarse con el administrador del sistema.");
                            }
                            /*tilPassword.setError(UtilMDP.INVALID_DOCUMENT);
                            tietPassword.requestFocus();*/
                        }
                    } else {
                        validarUsuarioDB(usuario, clave);
                    }
                }
                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Log.e("loginAcceso", "onFailure" + t.getMessage());
                }
            });
        } else {
            UtilMDP.hideProgreesDialog();
            validarUsuarioDB(usuario, clave);
            Log.e("One", "TIENE CONEXION A INTERNET");
        }

    }

    public void showModal(String mensaje) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Contraseña Incorrecta");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage(mensaje);
        alertDialogBuilder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }



/*    private void validarUsuarioDB(String userName, String password){

        UsuarioEntity usuario =  usuarioServiceImpl.getByUserName(userName);
        if(null != usuario){
            validarLoginAndPasswordDB(userName,password);
        }else{
            Toast.makeText(LoginActivity.this, "Usuario no registrado en el aplicativo", Toast.LENGTH_SHORT).show();
        }
    }*/

   /* private void validarLoginAndPasswordDB(String usuario, String clave){
        usuarioServiceImpl.getByUserAndPassword(usuario,clave).observe(this, new Observer<UsuarioEntity>() {
            @Override
            public void onChanged(UsuarioEntity usuarioEntity) {
                if(null != usuarioEntity){

                    Log.e("usuarioEntity",usuarioEntity.toString());

                    Intent intent = new Intent(LoginActivity.this,NavigationActivity.class);
                    UsuarioModel usuarioModel = new UsuarioModel();
                    usuarioModel.setNombreUsuario(usuarioEntity.getNombreUsuario());
                    usuarioModel.setDescripcionRol(usuarioEntity.getDescripcionRol());
                    intent.putExtra("usuarioModel",usuarioModel);
                    editor.putInt("IdUsers", usuarioEntity.getIdUsers());
                    editor.putString("usuario", usuarioEntity.getNombreUsuario());
                    editor.putString("nombreUsuario", usuarioEntity.getNombreUsuario());
                    editor.putString("rol", usuarioEntity.getDescripcionRol());
                    editor.putString("Descripcion_Rol_Usuario", usuarioEntity.getDescripcionRolUsuario());
                    editor.putInt("Id_Rol_Usuario",usuarioEntity.getIdRolUsuario());
                    editor.putInt("Id_Rol_General", usuarioEntity.getIdRolGeneral());
                    editor.putInt("IdPerfil_Usuario", usuarioEntity.getIdPerfilUsuario());
                    editor.putInt("Id_Empresa",usuarioEntity.getIdEmpresa());
                    editor.apply();
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    Log.e("DB", "DE INICIAR SESION CORRECTAMENTE");
                    UtilMDP.hideProgreesDialog();
                }
            }
        });
    }*/

    public static void verifyStoragePermissions(Activity activity) {
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        } else {
            Log.e("Permiso", " persmiso denegado");
        }
    }

    public Boolean validarCampos() {
        if (!tietUsuario.getText().toString().trim().matches("")) {
            if (!tietPassword.getText().toString().trim().matches("")) {
                return true;
            } else {
                tilPassword.setError(UtilMDP.REQUIRED_FIELD);
                tietPassword.requestFocus();
            }
        } else {
            tilUsuario.setError(UtilMDP.REQUIRED_FIELD);
            tietUsuario.requestFocus();
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                if (validarCampos()) {
                    loginAcceso(tietUsuario.getText().toString(), tietPassword.getText().toString());
                }
                break;
        }
    }
}
