package com.mdp.fusionapp.ui.fragment.inspeccion;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.CrearInspeccionEntity;
import com.mdp.fusionapp.database.entity.ProyectoEntity;
import com.mdp.fusionapp.model.InspeccionModel;
import com.mdp.fusionapp.network.response.InspeccionResponse;
import com.mdp.fusionapp.network.response.ProyectoResponse;
import com.mdp.fusionapp.ui.activity.inspecciones.CrearInspeccionActivity;
import com.mdp.fusionapp.ui.adapter.InspeccionAdapter;
import com.mdp.fusionapp.utilitary.Constante;
import com.mdp.fusionapp.utilitary.NetworkUtil;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.utilitary.helper.RecycleItemTouchHelperInspeccion;
import com.mdp.fusionapp.utilitary.helper.RecycleItemTouchHelperListenerInspeccion;
import com.mdp.fusionapp.viewModel.InspeccionViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class InspeccionFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private InspeccionViewModel inspeccionViewModel;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayoutManager linearLayout;
    private RecyclerView recyclerView;
    private InspeccionAdapter adaptador;
    private List<InspeccionModel> lista;
    private List<InspeccionModel> listaFiltro;
    private FragmentManager fragmentManager;
    private SharedPreferences sharedPreferences;
    private Spinner spProyecto;

    public static InspeccionFragment newInstance() {
        return new InspeccionFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.inspeccion_fragment, container, false);
        inspeccionViewModel = new ViewModelProvider(this).get(InspeccionViewModel.class);
        setToolbar(view);
        setupView(view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        obtenerInspecciones();

    }

    private void setupView(View view) {
        sharedPreferences = getActivity().getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        recyclerView = view.findViewById(R.id.recyclerview);
        spProyecto = view.findViewById(R.id.spProyecto);
        linearLayout = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayout);
        lista = new ArrayList<>();
        listaFiltro = new ArrayList<>();
        adaptador = new InspeccionAdapter(getContext(), lista);
        recyclerView.setAdapter(adaptador);

        swipeRefreshLayout = view.findViewById(R.id.swipeRefresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(this);

        //   ItemTouchHelper.SimpleCallback itemTouchHelperCallBack = new RecycleItemTouchHelperInspeccion(0, ItemTouchHelper.LEFT, this);
        //  new ItemTouchHelper(itemTouchHelperCallBack).attachToRecyclerView(recyclerView);
    }

    private void loadSpinnerProyecto() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("Id_Usuario", sharedPreferences.getInt("IdUsers", 0));
        hashMap.put("Id_Rol_Usuario", sharedPreferences.getInt("Id_Rol_Usuario", 0));
        hashMap.put("Id_Rol_General", sharedPreferences.getInt("Id_Rol_General", 0));
        hashMap.put("Modulo", "Inspe");

        Log.e("map", "" + hashMap);

        List<ProyectoEntity> list = inspeccionViewModel.findAllProjectOffline(hashMap);
        if (null != list) {
            listProyecto(list);
        }
    }

    private void listProyecto(List<ProyectoEntity> proyectoResponses) {
        Log.e("ProyectoEntity", "" + proyectoResponses.size());
        List<ProyectoEntity> olista = new ArrayList<>();
        olista.add(new ProyectoEntity(9999, "TODO"));
        olista.addAll(proyectoResponses);
        ArrayAdapter<ProyectoEntity> adapterTipoDocumento = new ArrayAdapter<>(getActivity(), R.layout.spinner_text2, olista);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spProyecto.setAdapter(adapterTipoDocumento);
        spProyecto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e("ProyectoEntity22", "" + proyectoResponses.size());
                ProyectoEntity proyectoModel = (ProyectoEntity) parent.getSelectedItem();
                listaFiltro.clear();
                Boolean filtro = false;
                for (int x = 0; x < lista.size(); x++) {
                    if (lista.get(x).getIdProyecto() == proyectoModel.getIdProyecto()) {
                        listaFiltro.add(lista.get(x));
                        filtro = true;
                    } else if (proyectoModel.getIdProyecto() == 9999) {
                        filtro = false;
                    } else {
                        filtro = true;
                    }
                }
                adaptador.setInspeccionFiltro(filtro ? listaFiltro : lista);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void obtenerInspecciones() {
        UtilMDP.showProgressDialog(getActivity(), "Obtener Inspección...");
        HashMap<String, Integer> hashMap = new HashMap<>();
        hashMap.put("Id_Usuario", sharedPreferences.getInt("IdUsers", 0));
        hashMap.put("Id_Rol_Usuario", sharedPreferences.getInt("Id_Rol_Usuario", 0));
        hashMap.put("Id_Rol_General", sharedPreferences.getInt("Id_Rol_General", 0));

        if (NetworkUtil.isOnline(getContext())) {
            inspeccionViewModel.listaInpecciones(hashMap, getContext()).observe(getActivity(), new Observer<List<InspeccionResponse>>() {
                @Override
                public void onChanged(List<InspeccionResponse> inspeccionResponse) {
                    lista.clear();
                    if (null != inspeccionResponse) {
                        for (InspeccionResponse data : inspeccionResponse) {
                            lista.add(new InspeccionModel(null, data.getId(), data.getEstado(), data.getFechaRegistro(),
                                    data.getNombreUsuario(), data.getCodigo(), data.getIdEmpresaInspeccionada(), data.getSede(), data.getEmpresaContratista(), data.getIdProyecto()));
                        }
                        adaptador.setInspeccion(lista);
                        loadSpinnerProyecto();
                    }
                }
            });
        } else {
            List<CrearInspeccionEntity> list = inspeccionViewModel.findInspeccionesOffline();
            if (list != null && list.size() > 0) {
                lista.clear();
                for (CrearInspeccionEntity data : list) {
                    lista.add(new InspeccionModel(data.getId(), data.getIdInspeccion(), 4, data.getFechaHoraIncidenteCreate(),
                            data.getNombreUsuario(), data.getNombreFormato(), data.getIdEmpresacontratista(), data.getNombreSede(), data.getNombreContratista(), data.getIdProyecto()));
                }
                adaptador.setInspeccion(lista);
            }
            loadSpinnerProyecto();
        }
        onclickTable();
        UtilMDP.hideProgreesDialog();

    }


    private void onclickTable() {
        adaptador.setOnItemClickListener((int positionL, View vL) -> {
            if (vL.getId() == R.id.cardRoot) {
                setFragment(new InspeccionDetalleFragment().newInstance(adaptador.getInspeccion(positionL)));
            } else if (vL.getId() == R.id.btnBorrar) {
                showModalEliminar(positionL);
            }
        });
    }

    private void setToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        final ActionBar ab = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_menu, null);
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorBlanco));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                DrawerLayout drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
                drawer.openDrawer(Gravity.LEFT);
                break;
            case R.id.action_add:
                Intent intent = new Intent(getActivity(), CrearInspeccionActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_inspeccion, menu);
    }


    public void setFragment(Fragment fragment) {
        if (null != fragment) {
            fragmentManager = getActivity().getSupportFragmentManager();
            boolean existFragment = false;
            List<Fragment> fragments = fragmentManager.getFragments();
            if (fragments != null) {
                for (Fragment mFragment : fragments) {
                    if (mFragment != null && mFragment.isVisible()) {
                        if (mFragment.getClass().getName().equals(fragment.getClass().getSimpleName())) {
                            existFragment = true;
                        }
                    }
                }
            }
            if (!existFragment) {
                fragmentManager
                        .beginTransaction()
                        .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                        .replace(R.id.content_main, fragment)
                        .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(null).commit();
            } else {
                fragmentManager.beginTransaction().remove(fragment)
                        .replace(R.id.content_main, fragment)
                        .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(null).commit();
            }
        }
    }

/*    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direcion, int position) {

    }*/

    private void showModalEliminar(int position) {
        View viewAddProject = getActivity().getLayoutInflater().inflate(R.layout.delete_inspeccion, null);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(false);
        alertDialog.setView(viewAddProject);

        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                adaptador.notifyDataSetChanged();
            }
        });

        alertDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                eliminarInspeccion(position);
                dialog.dismiss();
            }
        });

        final AlertDialog dialog = alertDialog.create();
        dialog.show();
    }

    private void eliminarInspeccion(int deleteIndex) {
        if (NetworkUtil.isOnline(requireContext())) {
            HashMap<String, Integer> hashMap = new HashMap<>();
            hashMap.put("idInspecion", adaptador.getInspeccion(deleteIndex).getIdInspeccion());
            inspeccionViewModel.eliminarInspeccion(hashMap).observe(this, new Observer<Boolean>() {
                @Override
                public void onChanged(Boolean aBoolean) {
                    if (aBoolean)
                        adaptador.removeItem(deleteIndex);
                }
            });
        } else {
            if (adaptador.getInspeccion(deleteIndex).getIdInspeccion() != null &&
                    adaptador.getInspeccion(deleteIndex).getIdInspeccion() != 0) {
                inspeccionViewModel.updateEstadoEliminar(adaptador.getInspeccion(deleteIndex).getIdRowInspeccion());
            } else {
                inspeccionViewModel.eliminarInspeccionOffline(adaptador.getInspeccion(deleteIndex).getIdRowInspeccion());
            }
            adaptador.removeItem(deleteIndex);
        }

    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                obtenerInspecciones();
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 2000);
    }
}
