package com.mdp.fusionapp.ui.fragment.interventoria;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.textfield.TextInputEditText;
import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.ContratistaEntity;
import com.mdp.fusionapp.database.entity.EmpresaSbcEntity;
import com.mdp.fusionapp.database.entity.LugarEntity;
import com.mdp.fusionapp.database.entity.ProyectoEntity;
import com.mdp.fusionapp.database.entity.SedeProyectoEntity;
import com.mdp.fusionapp.database.entity.TipoUbicacionEntity;
import com.mdp.fusionapp.model.EvidenciaInterventoriaModel;
import com.mdp.fusionapp.model.ItemModel;
import com.mdp.fusionapp.model.registro.InsertInterventoriaModel;
import com.mdp.fusionapp.network.response.interventoria.InterventoriaDetalleResponse;
import com.mdp.fusionapp.ui.activity.interventoria.EvidenciaActivity;
import com.mdp.fusionapp.ui.activity.interventoria.InterventoriaActivity;
import com.mdp.fusionapp.utilitary.NetworkUtil;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.viewModel.InspeccionViewModel;
import com.mdp.fusionapp.viewModel.InterventoriaViewModel;
import com.mdp.fusionapp.viewModel.SbcViewModel;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class DatosGeneralesInterFragment extends Fragment implements View.OnClickListener {

    // CONEXION A SERVICIO WEB
    private SbcViewModel mViewModel;
    private InterventoriaViewModel interventoriaViewModel;
    private InspeccionViewModel inspeccionViewModel;
    private InterventoriaDetalleResponse interventoria;
    private List<EvidenciaInterventoriaModel> lstEvidencia;
    private SharedPreferences sharedPreferences;
    // VARIABLES DE VISTA
    private Spinner spSedeProyecto, spLineaSubestacion, spPlantrabajo, spEmpresaIntervenida, spLugar;
    private TextInputEditText tietNroplantrabajo, tietactividad, tietsupervisor, tietsupervisorSusti;
    private EditText edtMensaje;
    private Button btnSiguiente;
    private Integer empresaIntervenida, idPlanTrabajo, lineaSubEstacion, idSedeProyecto;
    private Integer idLugar;
    private String nombreLugar;

    private static final int SECOND_ACTIVITY_REQUEST_CODE = 999;


    public static DatosGeneralesInterFragment newInstance(InterventoriaDetalleResponse detalle) {
        DatosGeneralesInterFragment fragment = new DatosGeneralesInterFragment();
        Bundle args = new Bundle();
        args.putSerializable("detalle", detalle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setHasOptionsMenu(true);
        if (getArguments() != null) {
            interventoria = (InterventoriaDetalleResponse) getArguments().getSerializable("detalle");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_datos_generales_inter, container, false);
        initView(view);
        loadWebService();
        loadDetalle();
        return view;
    }

    private void initView(View view) {
        sharedPreferences = getActivity().getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        spSedeProyecto = view.findViewById(R.id.spSedeProyecto);
        spLineaSubestacion = view.findViewById(R.id.spLineaSubestacion);
        spPlantrabajo = view.findViewById(R.id.spPlantrabajo);
        spEmpresaIntervenida = view.findViewById(R.id.spEmpresaIntervenida);
        tietNroplantrabajo = view.findViewById(R.id.tietNroplantrabajo);
        tietactividad = view.findViewById(R.id.tietactividad);
        tietsupervisor = view.findViewById(R.id.tietsupervisor);
        tietsupervisorSusti = view.findViewById(R.id.tietsupervisorSusti);
        edtMensaje = view.findViewById(R.id.edtMensaje);
        spLugar = view.findViewById(R.id.spLugar);
        btnSiguiente = view.findViewById(R.id.btnSiguiente);
        lstEvidencia = new ArrayList<>();
        btnSiguiente.setOnClickListener(this);
        tietactividad.setText("");
        edtMensaje.setText("");
        tietNroplantrabajo.setText("");
        tietsupervisor.setText("");
        tietsupervisorSusti.setText("");

        spinnerplanTrabajo();
    }

    private Boolean validarObjectoDetalle() {
        return interventoria != null ? true : false;
    }

    private void loadDetalle() {
        if (validarObjectoDetalle()) {
            tietactividad.setText(interventoria.getData().getActividad());
            edtMensaje.setText(interventoria.getData().getConcluciones());
            tietNroplantrabajo.setText(interventoria.getData().getNroPlanTrabajo());
            tietsupervisor.setText(interventoria.getData().getSupervisor());
            tietsupervisorSusti.setText(interventoria.getData().getSupervisorSustituto());
            idPlanTrabajo = interventoria.getData().getIdPlanTrabajo();
            lineaSubEstacion = Integer.parseInt(interventoria.getData().getLineaSubestacion());
        }
    }

    public InsertInterventoriaModel saveDatosGenerales() {
        Date date = new Date();
        DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        InsertInterventoriaModel object = new InsertInterventoriaModel();
        object.setActividad(tietactividad.getText().toString());
        object.setConclucion(edtMensaje.getText().toString());
        object.setEmpresaIntervenida(empresaIntervenida);
        object.setEmpresaInterventor(1);
        object.setEstado(false);
        object.setFechaHora(validarObjectoDetalle() ? interventoria.getData().getFecha_Hora() : hourFormat.format(date));
        object.setFechaRegistro(validarObjectoDetalle() ? dateFormat.format(date) : dateFormat.format(date));
        object.setIdPlanTrabajo(idPlanTrabajo);
        object.setPlanTrabajo(idPlanTrabajo);
        object.setLineaSubEstacion(idLugar);
        object.setLugarZona(idSedeProyecto);
        object.setNroPlanTrabajo(tietNroplantrabajo.getText().toString());
        object.setSupervisor(tietsupervisor.getText().toString());
        object.setSupervisorSustituto(tietsupervisorSusti.getText().toString());
        object.setListEvidencia(lstEvidencia);
        object.setTipoUbicacion(lineaSubEstacion);
        return object;
    }


    private void loadWebService() {
        inspeccionViewModel = new ViewModelProvider(this).get(InspeccionViewModel.class);
        interventoriaViewModel = new ViewModelProvider(this).get(InterventoriaViewModel.class);
        mViewModel = new ViewModelProvider(this).get(SbcViewModel.class);


        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("Modulo", "Interventoria");

        List<SedeProyectoEntity> listProject = mViewModel.obtenerSedeOffline(hashMap);
        List<SedeProyectoEntity> list = new ArrayList<>();
        list.addAll(listProject);
        if (null != list) {
            listProyecto(list);
            if (validarObjectoDetalle()) {
                for (int x = 0; x < list.size(); x++) {
                    if (Integer.parseInt(interventoria.getData().getLugarZona()) == list.get(x).getIdSedeProyecto()) {
                        spSedeProyecto.setSelection(x);
                    }
                }
            }
        }
    }

    private void listProyecto(List<SedeProyectoEntity> proyectoResponses) {
        ArrayAdapter<SedeProyectoEntity> adapterTipoDocumento = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, proyectoResponses);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSedeProyecto.setAdapter(adapterTipoDocumento);
        spSedeProyecto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SedeProyectoEntity proyectoModel = (SedeProyectoEntity) parent.getSelectedItem();
                idSedeProyecto = proyectoModel.getIdSedeProyecto();
                loadContratista(idSedeProyecto);
                loadTipoUbicacion(proyectoModel.getIdSedeProyecto());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void loadContratista(Integer idSedeProyecto) {
        List<ContratistaEntity> contratista = inspeccionViewModel.findContratistaByProyecto(idSedeProyecto);
        if (null != contratista) {
            loadEmpresaIntervenida(contratista);
        }
    }


    private void loadTipoUbicacion(Integer idProyecto) {
        HashMap<String, Integer> hashMap = new HashMap<>();
        hashMap.put("idProyecto", idProyecto);

        if (NetworkUtil.isOnline(getContext())) {
            inspeccionViewModel.tipoUbicacion(hashMap, getContext()).observe(getActivity(), new Observer<List<TipoUbicacionEntity>>() {
                @Override
                public void onChanged(List<TipoUbicacionEntity> tipoUbicacionResponses) {
                    List<TipoUbicacionEntity> list = new ArrayList<>(); //tipoUbicacionResponses
                    list.add(new TipoUbicacionEntity(0, "Seleccionar"));
                    list.addAll(tipoUbicacionResponses);
                    if (null != list) {
                        if (list.size() > 0) {
                            spinnerSubestacion(list);
                        }
                    }
                }
            });
        } else {
            List<TipoUbicacionEntity> list = new ArrayList<>(); //tipoUbicacionResponses
            list.add(new TipoUbicacionEntity(0, "Seleccionar"));
            list.addAll(inspeccionViewModel.findTipoUbicacionByProyecto(idProyecto));
            if (null != list) {
                if (list.size() > 0) {
                    spinnerSubestacion(list);
                }
            }
        }

    }

    private void spinnerSubestacion(List<TipoUbicacionEntity> tipoUbicacionResponse) {
        ArrayAdapter<TipoUbicacionEntity> adapterTipoDocumento = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, tipoUbicacionResponse);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spLineaSubestacion.setAdapter(adapterTipoDocumento);
        spLineaSubestacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TipoUbicacionEntity object = (TipoUbicacionEntity) parent.getSelectedItem();
                loadLugar(object.getIdTipoUbicacion());
                lineaSubEstacion = object.getIdTipoUbicacion();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        //  Log.e("getLineaSubestacion",""+interventoria.getData().getLineaSubestacion());
        if (validarObjectoDetalle()) {
            for (int x = 0; x < tipoUbicacionResponse.size(); x++) {
                Log.e("getLineaSubestacion", tipoUbicacionResponse.get(x).getIdTipoUbicacion().intValue() + " == " + interventoria.getData().getTipoUbicacion());
                if (tipoUbicacionResponse.get(x).getIdTipoUbicacion().intValue() == Integer.parseInt(interventoria.getData().getTipoUbicacion())) {
                    spLineaSubestacion.setSelection(x);
                }
            }
        } else {
            Log.e("spinnerSubestacion", " null");
        }
    }

    private void loadLugar(Integer idTipoUbicacion) {
        HashMap<String, Integer> hashMap = new HashMap<>();
        hashMap.put("idTipoUbicacion", idTipoUbicacion);
        if (NetworkUtil.isOnline(getContext())) {
            inspeccionViewModel.obtenerLugar(hashMap, getContext()).observe(getActivity(), new Observer<List<LugarEntity>>() {
                @Override
                public void onChanged(List<LugarEntity> lugarResponses) { //lugarResponses
                    List<LugarEntity> list = new ArrayList<>(); //tipoUbicacionResponses
                    list.add(new LugarEntity(0, "Seleccionar"));
                    list.addAll(lugarResponses);
                    if (null != list) {
                        listLugar(list);
                        if (validarObjectoDetalle()) {
                            for (int x = 0; x < list.size(); x++) {
                                if (Integer.parseInt(interventoria.getData().getLineaSubestacion()) == list.get(x).getIdLugar().intValue()) {
                                    spLugar.setSelection(x);
                                }
                            }
                        }
                    }
                }
            });
        } else {
            List<LugarEntity> list = new ArrayList<>(); //tipoUbicacionResponses
            list.add(new LugarEntity(0, "Seleccionar"));
            list.addAll(inspeccionViewModel.findLugarByUbicacion(idTipoUbicacion));
            if (null != list) {
                listLugar(list);
                if (validarObjectoDetalle()) {
                    for (int x = 0; x < list.size(); x++) {
                        if (Integer.parseInt(interventoria.getData().getLineaSubestacion()) == list.get(x).getIdLugar().intValue()) {
                            spLugar.setSelection(x);
                        }
                    }
                }
            }
        }

    }

    private void listLugar(List<LugarEntity> lugarResponses) {
        ArrayAdapter<LugarEntity> adapterTipoDocumento = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, lugarResponses);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spLugar.setAdapter(adapterTipoDocumento);
        spLugar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                LugarEntity proyectoModel = (LugarEntity) parent.getSelectedItem();
                idLugar = proyectoModel.getIdLugar();
                nombreLugar = proyectoModel.getNombre();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void spinnerplanTrabajo() {
        List<ItemModel> itemModels = new ArrayList<>();
        itemModels.add(new ItemModel(0, "Seleccionar", "N"));
        itemModels.add(new ItemModel(1, "Nacional", "N"));
        itemModels.add(new ItemModel(2, "Local", "L"));
        itemModels.add(new ItemModel(3, "Sin Consignación", "SC"));

        ArrayAdapter<ItemModel> adapterTipoDocumento = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, itemModels);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPlantrabajo.setAdapter(adapterTipoDocumento);
        spPlantrabajo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ItemModel selectedItem = (ItemModel) parent.getSelectedItem();
                idPlanTrabajo = selectedItem.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (validarObjectoDetalle()) {
            for (int x = 0; x < itemModels.size(); x++) {
                if (itemModels.get(x).getId().intValue() == Integer.parseInt(interventoria.getData().getPlanTrabajo())) {
                    spPlantrabajo.setSelection(x);
                }
            }
        }
    }

    private void loadEmpresaIntervenida(List<ContratistaEntity> empresaSBSResponses) {
        ArrayAdapter<ContratistaEntity> adapterTipoDocumento = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, empresaSBSResponses);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spEmpresaIntervenida.setAdapter(adapterTipoDocumento);
        spEmpresaIntervenida.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ContratistaEntity proyectoModel = (ContratistaEntity) parent.getSelectedItem();
                empresaIntervenida = proyectoModel.getIdEmpresaObservadora();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        if (validarObjectoDetalle()) {
            Log.e("idEmpresaInt", "" + interventoria.getData().getEmpresaIntervenida());
            for (int x = 0; x < empresaSBSResponses.size(); x++) {
                if (empresaSBSResponses.get(x).getIdEmpresaObservadora().intValue() == Integer.parseInt(interventoria.getData().getEmpresaIntervenida())) {
                    spEmpresaIntervenida.setSelection(x);
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SECOND_ACTIVITY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                lstEvidencia = (List<EvidenciaInterventoriaModel>) data.getSerializableExtra("lstEvidencia");
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_foto, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_foto:

                Intent intent = new Intent(getActivity(), EvidenciaActivity.class);
                intent.putExtra("lstEvidencia", (Serializable) lstEvidencia);
                if (null != interventoria && null != interventoria.getData().getIdInterventoria()) {
                    intent.putExtra("idInterventoria", interventoria.getData().getIdInterventoria());
                }
                startActivityForResult(intent, SECOND_ACTIVITY_REQUEST_CODE);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private Boolean valiarCampos() {

        if (idSedeProyecto == 0) {
            Toast.makeText(getContext(), "Seleccione Sede / Proyecto", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (tietNroplantrabajo.getText().toString() == null || tietNroplantrabajo.getText().toString().trim().equals("")) {
            Toast.makeText(getContext(), "Ingrese número de plan de trabajo", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (lineaSubEstacion == 0) {
            Toast.makeText(getContext(), "Seleccione Linea / Subestación", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (idLugar == 0) {
            Toast.makeText(getContext(), "Seleccione Tipo de ubicación", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (tietactividad.getText().toString() == null || tietactividad.getText().toString().trim().equals("")) {
            Toast.makeText(getContext(), "Ingrese actividad", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (idPlanTrabajo == 0) {
            Toast.makeText(getContext(), "Seleccione Plan de trabajo", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (empresaIntervenida == 0) {
            Toast.makeText(getContext(), "Seleccione Empresa intervenida", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (tietsupervisor.getText().toString() == null || tietsupervisor.getText().toString().trim().equals("")) {
            Toast.makeText(getContext(), "Ingrese supervisor de trabajo", Toast.LENGTH_SHORT).show();
            return false;
        }

       /* if (tietsupervisorSusti.getText().toString() == null || tietsupervisorSusti.getText().toString().trim().equals("")) {
            Toast.makeText(getContext(), "Ingrese supervisor de trabajo sustitito", Toast.LENGTH_SHORT).show();
            return false;
        }*/

       /*if (edtMensaje.getText().toString() == null || edtMensaje.getText().toString().trim().equals("")) {
            Toast.makeText(getContext(), "Ingrese observación / Explicaciones / Deficiencia", Toast.LENGTH_SHORT).show();
            return false;
        } */

        return true;
    }

    private void enablePager() {
        if (valiarCampos()) {
            ((InterventoriaActivity) getActivity()).disableScroll(false);
            ((InterventoriaActivity) getActivity()).setViewPager(1);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSiguiente:
                enablePager();
                break;
        }
    }
}
