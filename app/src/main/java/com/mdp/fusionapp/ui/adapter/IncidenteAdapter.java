package com.mdp.fusionapp.ui.adapter;

import android.content.Context;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.network.response.IncidenteResponse;

import java.util.ArrayList;
import java.util.List;

public class IncidenteAdapter extends RecyclerView.Adapter<IncidenteAdapter.ViewHolder> {

    private Context context;
    private List<IncidenteResponse.Data> lista = new ArrayList<>();
    private static ClickListener clickListener;
    private static long mLastClickTime = 0;

    public IncidenteAdapter(Context context, List<IncidenteResponse.Data> lista) {
        this.context = context;
        this.lista = lista;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_incidente, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(lista.get(position));
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public void setLista(List<IncidenteResponse.Data> lista) {
        this.lista = lista;
        notifyDataSetChanged();
    }

    public IncidenteResponse.Data getIncidenteResponse(Integer position) {
        return lista.get(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView txtCodigo, txtFormato, txtFecha;
        private ConstraintLayout cardRoot;

        public ViewHolder(@NonNull View view) {
            super(view);
            txtCodigo = view.findViewById(R.id.txtCodigo);
            txtFormato = view.findViewById(R.id.txtFormato);
            txtFecha = view.findViewById(R.id.txtFecha);
            cardRoot = view.findViewById(R.id.cardRoot);
        }

        public void bind(IncidenteResponse.Data item) {
            if (item.getIdIncidente() != null && item.getIdIncidente() != 0) {
                txtCodigo.setText("INCI-" + item.getIdIncidente());
            } else {
                txtCodigo.setText("INCI-" + item.getgFechaCreado());
            }

            txtFormato.setText(item.getgDescripcionFormato());
            txtFecha.setText(item.getgFechaCreado());
            cardRoot.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            clickListener.onItemClick(getAdapterPosition(), view);
        }
    }

    public interface ClickListener {
        void onItemClick(int postion, View v);
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        IncidenteAdapter.clickListener = clickListener;
    }

}
