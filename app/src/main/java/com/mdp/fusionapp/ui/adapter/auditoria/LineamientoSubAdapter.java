package com.mdp.fusionapp.ui.adapter.auditoria;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.model.CalificacionModel;
import com.mdp.fusionapp.model.LineamientoSubModel;
import com.mdp.fusionapp.model.LugarModel;
import com.mdp.fusionapp.utilitary.UtilMDP;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class LineamientoSubAdapter extends RecyclerView.Adapter<LineamientoSubAdapter.ViewHolder> {

    private Context context;
    private List<LineamientoSubModel> lista = new ArrayList<>();
    private static ClickListener clickListener;

    public LineamientoSubAdapter(Context context, List<LineamientoSubModel> lista) {
        this.context = context;
        this.lista = lista;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lineamientosub, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(lista.get(position), context);
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public void setLista(List<LineamientoSubModel> lista) {
        this.lista = lista;
        notifyDataSetChanged();
    }

    public void setValorImg(int position, String base64Img) {
        lista.get(position).setImgbase64(base64Img);
        notifyDataSetChanged();
    }

    public LineamientoSubModel getLineamientoSub(int position) {
        return lista.get(position);
    }

    public List<LineamientoSubModel> getLista() {
        return lista;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtNombre;
        Spinner spCalificacion, spLugar;
        EditText edtMensaje;
        LinearLayout lnlFoto;
        ImageView imgFoto;
        ImageButton btnGallery,btnCamera;

        public ViewHolder(@NonNull View v) {
            super(v);
            edtMensaje = v.findViewById(R.id.edtMensaje);
            txtNombre = v.findViewById(R.id.txtNombre);
            spCalificacion = v.findViewById(R.id.spCalificacion);
            spLugar = v.findViewById(R.id.spLugar);
            lnlFoto = v.findViewById(R.id.lnlFoto);
            imgFoto = v.findViewById(R.id.imgFoto);
            btnGallery = v.findViewById(R.id.btnGallery);
            btnCamera = v.findViewById(R.id.btnCamera);
        }

        public void bind(LineamientoSubModel item, Context context) {
            txtNombre.setText(item.getDescripcionItems());
            edtMensaje.setText(item.getMensage());
            btnCamera.setOnClickListener(this);
            btnGallery.setOnClickListener(this);
            imgFoto.setOnClickListener(this);

            if (null != item.getImgbase64() && !item.getImgbase64().equals("")) {
                imgFoto.setVisibility(View.VISIBLE);
            }else {
                imgFoto.setVisibility(View.GONE);
            }


            List<CalificacionModel> lista = UtilMDP.calificacionModels();
            ArrayAdapter<CalificacionModel> adapterTipoDocumento = new ArrayAdapter<>(context, R.layout.spinner_text, lista);
            adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spCalificacion.setAdapter(adapterTipoDocumento);
            spCalificacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    CalificacionModel tipoGestionModel = (CalificacionModel) parent.getSelectedItem();
                    item.setIdCalificacion(tipoGestionModel.getId());
                    Log.e("idCal"," "+item.getIdCalificacion());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            Log.e("getIdCalificacion"," "+item.getIdCalificacion());
            if (null != item.getIdCalificacion()) {
                for (int x = 0; x < lista.size(); x++) {
                    Log.e("califi",""+lista.get(x).getId().intValue()+" - "+item.getIdCalificacion().intValue());
                    if (lista.get(x).getId().intValue() == item.getIdCalificacion().intValue()) {
                        spCalificacion.setSelection(x);
                    }

                }
            }


            List<LugarModel> lugarModels = UtilMDP.lugarModels();
            ArrayAdapter<LugarModel> arrayAdapter = new ArrayAdapter<>(context, R.layout.spinner_text, lugarModels);
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spLugar.setAdapter(arrayAdapter);
            spLugar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    LugarModel lugarModel = (LugarModel) parent.getSelectedItem();
                    item.setIdLugar(lugarModel.getId());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            if (null != item.getIdLugar()) {
                for (int x = 0; x < lugarModels.size(); x++) {
                    if (lugarModels.get(x).getId().intValue() == item.getIdLugar().intValue()) {
                        spLugar.setSelection(x);
                    }
                }
            }

            edtMensaje.addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    item.setMensage(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {
                    boolean _ignore = false;
                    if (_ignore)
                        return;

                    _ignore = true; // prevent infinite loop
                    // Change your text here.
                    // myTextView.setText(myNewText);
                    _ignore = false;
                }
            });
        }

        @Override
        public void onClick(View view) {
            clickListener.onItemClick(getAdapterPosition(), view);
        }
    }

    public interface ClickListener {
        void onItemClick(int postion, View v);
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        LineamientoSubAdapter.clickListener = clickListener;
    }


}
