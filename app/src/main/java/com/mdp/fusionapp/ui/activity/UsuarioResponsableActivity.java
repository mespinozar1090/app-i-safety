package com.mdp.fusionapp.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.UsuarioResponsableEntity;
import com.mdp.fusionapp.ui.adapter.UsuarioAdapter;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.viewModel.InspeccionViewModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UsuarioResponsableActivity extends AppCompatActivity {

    private InspeccionViewModel inspeccionViewModel;
    private RecyclerView recyclerview;
    private LinearLayoutManager linearLayout;
    private UsuarioAdapter adaptador;
    private List<UsuarioResponsableEntity> lista;
    private EditText etdBuscar;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario_responsable);
        inspeccionViewModel = new ViewModelProvider(this).get(InspeccionViewModel.class);
        initView();
    }

    private void initView() {
        sharedPreferences = getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        recyclerview = findViewById(R.id.recyclerview);
        etdBuscar = findViewById(R.id.etdBuscar);
        linearLayout = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(linearLayout);
        lista = new ArrayList<>();
        adaptador = new UsuarioAdapter(this, lista);
        recyclerview.setAdapter(adaptador);

        filtrarLista();
        loadUsuarioResponsable();

        onclickTable();
    }

    private void filtrarLista() {
        etdBuscar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence editable, int i, int i1, int i2) {
                String aux = editable.toString().trim();
                Log.e("aux", " : " + aux);
                if (!aux.equals("")) {
                    ArrayList<UsuarioResponsableEntity> newLista = new ArrayList<>();
                    for (UsuarioResponsableEntity usuarioMap : lista) {
                        String nombre = usuarioMap.getNombreUsuario();
                        Log.e("nombre", " : " + nombre);
                        if (nombre.toLowerCase().contains(aux.toLowerCase())) {
                            Log.e("aux 2", " : " + nombre.toLowerCase() + " - " + aux.toLowerCase());
                            newLista.add(usuarioMap);
                        }
                    }
                    adaptador.setFilter(newLista);
                } else {
                    adaptador.setLista(lista);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    private void loadUsuarioResponsable() {
        inspeccionViewModel.obtenerUsuarios().observe(this, new Observer<List<UsuarioResponsableEntity>>() {
            @Override
            public void onChanged(List<UsuarioResponsableEntity> usuarioResponses) {
                if (null != usuarioResponses) {
                    lista.clear();
                    lista.addAll(usuarioResponses);
                    adaptador.setLista(lista);
                }
            }
        });

    }

    private void onclickTable() {
        adaptador.setOnItemClickListener((int positionL, View vL) -> {
            if (vL.getId() == R.id.rootLayout) {
                Intent intent = new Intent();
                intent.putExtra("object", (Serializable) adaptador.getObject(positionL));
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}