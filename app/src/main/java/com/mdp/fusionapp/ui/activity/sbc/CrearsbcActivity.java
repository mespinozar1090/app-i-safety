package com.mdp.fusionapp.ui.activity.sbc;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.material.tabs.TabLayout;
import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.sbc.CrearCategoriaSbcEntity;
import com.mdp.fusionapp.database.entity.sbc.CrearSbcEntity;
import com.mdp.fusionapp.database.entity.sbc.NotificarEntity;
import com.mdp.fusionapp.model.DetalleSBCModel;
import com.mdp.fusionapp.model.registro.InsertSbcModel;
import com.mdp.fusionapp.network.response.Respuesta;
import com.mdp.fusionapp.network.response.detalle.DetalleSbcResponse;
import com.mdp.fusionapp.database.entity.UsuarioNotificarEntity;
import com.mdp.fusionapp.ui.fragment.sbc.CategoriasbcFragment;
import com.mdp.fusionapp.ui.fragment.sbc.GeneralsbcFragment;
import com.mdp.fusionapp.utilitary.Constante;
import com.mdp.fusionapp.utilitary.CustomViewPager;
import com.mdp.fusionapp.utilitary.UtilMDP;
import com.mdp.fusionapp.viewModel.SbcViewModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class CrearsbcActivity extends AppCompatActivity {

    private SbcViewModel viewModel;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private CustomViewPager mViewPager;
    private TabLayout tabLayout;
    private FragmentManager fragmentManager;
    InsertSbcModel insertSbcModel = null;
    private SharedPreferences sharedPreferences;
    DetalleSbcResponse data = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crearsbc);
        viewModel = new ViewModelProvider(this).get(SbcViewModel.class);
        data = (DetalleSbcResponse) getIntent().getSerializableExtra("oDetalleSbc");
        insertSbcModel = new InsertSbcModel();
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        setToolbar();
        initView();
    }


    private void initView() {
        sharedPreferences = getSharedPreferences(UtilMDP.ID_APP, Context.MODE_PRIVATE);
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.disableScroll(true);
        mViewPager.setOffscreenPageLimit(3);
        tabLayout = findViewById(R.id.tabs);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
    }

    public void disableScroll(Boolean valor) {
        mViewPager.disableScroll(valor);
    }

    public void setViewPager(int page) {
        mViewPager.setCurrentItem(page);
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowHomeEnabled(true);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_back, null);
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorBlanco));
            ab.setHomeAsUpIndicator(drawable);
        }
    }

    private Boolean validarDetalle() {
        return data != null ? true : false;
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    GeneralsbcFragment generalsbcFragment = new GeneralsbcFragment().newInstance(data);
                    return generalsbcFragment;
                case 1:
                    CategoriasbcFragment categoriasbcFragment = new CategoriasbcFragment().newInstance(data);
                    return categoriasbcFragment;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    private static String makeFragmentName(int viewId, long id) {
        return "android:switcher:" + viewId + ":" + id;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void obtenerDataGeneral() {
        String fragmentTag = makeFragmentName(mViewPager.getId(), 0);

        FragmentManager fm = getSupportFragmentManager();
        GeneralsbcFragment fragment = (GeneralsbcFragment) fm.findFragmentByTag(fragmentTag);
        if (fragment != null) {
            InsertSbcModel data = fragment.saveDataGeneralSBC();
            insertSbcModel.setNombreObservador(data.getNombreObservador());
            insertSbcModel.setIdSedeProyecto(data.getIdSedeProyecto());
            insertSbcModel.setCargoObservador(data.getCargoObservador());
            insertSbcModel.setLugarTrabajo(data.getLugarTrabajo());
            insertSbcModel.setFechaRegistro(data.getFechaRegistro());
            insertSbcModel.setIdEmpresaObservadora(data.getIdEmpresaObservadora());
            insertSbcModel.setHorarioObservacion(data.getHorarioObservacion());
            insertSbcModel.setTiempoExpObservada(data.getTiempoExpObservada());
            insertSbcModel.setEspecialidadObservado(data.getEspecialidadObservado());
            insertSbcModel.setActividad(data.getActividad());
            insertSbcModel.setIdAreaTrabajo(data.getIdAreaTrabajo());
            insertSbcModel.setDescripcionAreaObservada(data.getDescripcionAreaObservada());
            insertSbcModel.setIdSedeProyectoObservado(data.getIdSedeProyectoObservado());
        }
    }


    public void insertarSBC(List<DetalleSBCModel> lstDetalleSbc, List<UsuarioNotificarEntity> lstNotificacion) {

        UtilMDP.showProgressDialog(this, "Guardando...");
        List<HashMap<String, Object>> lstNotificacions = new ArrayList<>();
        HashMap<String, Object> notificacion = null;
        for (int x = 0; x < lstNotificacion.size(); x++) {
            if (lstNotificacion.get(x).getSelectEmail()) {
                notificacion = new HashMap<>();
                notificacion.put("IdUser", lstNotificacion.get(x).getIdUsers());
                notificacion.put("Nombre", lstNotificacion.get(x).getNombreUsuario());
                notificacion.put("Email", lstNotificacion.get(x).getEmailCorporativo());
                lstNotificacions.add(notificacion);
            }
        }

        List<HashMap<String, Object>> lstDetalle = new ArrayList<>();
        HashMap<String, Object> detalle = null;
        for (int x = 0; x < lstDetalleSbc.size(); x++) {
            detalle = new HashMap<>();
            detalle.put("Id_sbc_detalle", lstDetalleSbc.get(x).getIdSbcDetalle());
            detalle.put("Id_sbc_Categoria", lstDetalleSbc.get(x).getIdSbcCategoria());
            detalle.put("Id_sbc_Categoria_Items", lstDetalleSbc.get(x).getIdSbcCategoriaItems());
            detalle.put("Seguro", lstDetalleSbc.get(x).getSeguro());
            detalle.put("Riesgoso", lstDetalleSbc.get(x).getRiesgoso());
            detalle.put("Idbarrera", lstDetalleSbc.get(x).getIdbarrera());
            detalle.put("Observacion_sbc_detalle", lstDetalleSbc.get(x).getObservacionSbcDetalle());
            detalle.put("Id_sbc", lstDetalleSbc.get(x).getIdSbc());
            lstDetalle.add(detalle);
        }

        obtenerDataGeneral();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        HashMap<String, Object> hashMap = new HashMap<>();
        if (data != null && data.getIdsbc() != null) {
            hashMap.put("Id_sbc", data.getIdsbc());

        } else {
            hashMap.put("Id_sbc", 0);
        }

        hashMap.put("Nombre_observador", insertSbcModel.getNombreObservador());
        hashMap.put("IdSede_Proyecto", insertSbcModel.getIdSedeProyecto());
        hashMap.put("Cargo_Observador", insertSbcModel.getCargoObservador()); //enviar texto
        //IdLugar_trabajo
        hashMap.put("Lugar_trabajo", insertSbcModel.getLugarTrabajo());
        hashMap.put("Fecha_Registro_SBC", insertSbcModel.getFechaRegistro());
        hashMap.put("Id_Empresa_Observadora", insertSbcModel.getIdEmpresaObservadora());
        hashMap.put("Horario_Observacion", insertSbcModel.getHorarioObservacion().toString());
        hashMap.put("Tiempo_exp_observada", insertSbcModel.getTiempoExpObservada());
        hashMap.put("Especialidad_Observado", insertSbcModel.getEspecialidadObservado());
        hashMap.put("Actividad_Observada", insertSbcModel.getActividad());
        hashMap.put("Id_Area_Trabajo", insertSbcModel.getIdAreaTrabajo());
        hashMap.put("Hora_SBC", null);
        hashMap.put("Fecha_registro", dateFormat.format(new Date()));
        hashMap.put("Tipo_SBC", null);
        hashMap.put("IdCargo", null);
        hashMap.put("Descripcion_Area_Observada", insertSbcModel.getDescripcionAreaObservada());
        hashMap.put("lstDetalleSBC", lstDetalle);// tener presente
        hashMap.put("IdSede_Proyecto_Observado", insertSbcModel.getIdSedeProyectoObservado());
        hashMap.put("IdUsuario", sharedPreferences.getInt("IdUsers", 0));
        hashMap.put("Tipo_Registro", 2);
        hashMap.put("Origen_Registro", "App");
        hashMap.put("UsuariosNotificados", lstNotificacions);
        hashMap.put("NombreFormato", "SBC-" + sharedPreferences.getString("rol", "") + sharedPreferences.getString("nombreUsuario", "") + insertSbcModel.getFechaRegistro());

        if (null != data) {
            viewModel.editarSBC(hashMap, getApplicationContext()).observe(this, new Observer<Respuesta>() {
                @Override
                public void onChanged(Respuesta respuesta) {
                    if (null != respuesta) {
                        Log.e("EDIT SBC", " : " + respuesta.getMessage());
                        UtilMDP.hideProgreesDialog();
                        finish();
                    } else {
                        insertSBCOffline(lstDetalleSbc, lstNotificacion);
                    }
                }
            });
        } else {
            viewModel.insertarSBC(hashMap, getApplicationContext()).observe(this, new Observer<Respuesta>() {
                @Override
                public void onChanged(Respuesta respuesta) {
                    if (null != respuesta) {
                        Log.e("INSERT SBC", " : " + respuesta.getMessage());
                        UtilMDP.hideProgreesDialog();
                        finish();
                    } else {
                        insertSBCOffline(lstDetalleSbc, lstNotificacion);

                    }
                }
            });
        }
    }

    private void insertSBCOffline(List<DetalleSBCModel> lstDetalleSbc, List<UsuarioNotificarEntity> lstNotificacion) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        CrearSbcEntity object = new CrearSbcEntity();

        if (data != null && data.getIdsbc() != null) {
            object.setIdSbc(data.getIdsbc());
        }

        object.setEstadoRegistroDB(Constante.ESTADO_CREADO);
        object.setNombreObservador(insertSbcModel.getNombreObservador());
        object.setIdSedeProyecto(insertSbcModel.getIdSedeProyecto());
        object.setCargoObservador(insertSbcModel.getCargoObservador());
        object.setLugarTrabajo(insertSbcModel.getLugarTrabajo());
        object.setFechaRegistroSbc(insertSbcModel.getFechaRegistro());
        object.setIdEmpresaObservadora(insertSbcModel.getIdEmpresaObservadora());
        object.setHorarioObservacion(insertSbcModel.getHorarioObservacion());
        object.setTiempoExpObservada(insertSbcModel.getTiempoExpObservada());
        object.setEspecialidaObservado(insertSbcModel.getEspecialidadObservado());
        object.setActividadObservado(insertSbcModel.getActividad());
        object.setIdAreaTrabajo(insertSbcModel.getIdAreaTrabajo());
        object.setFechaRegristo(dateFormat.format(new Date()));
        object.setDescripcionAreaObservada(insertSbcModel.getDescripcionAreaObservada());
        object.setIdSedeProyectoObservado(insertSbcModel.getIdSedeProyectoObservado());
        object.setIdUsuario(sharedPreferences.getInt("IdUsers", 0));
        object.setNombreFormato("SBC-" + sharedPreferences.getString("rol", "") + sharedPreferences.getString("nombreUsuario", "") + insertSbcModel.getFechaRegistro());

        if (data != null && data.getIdRow() != null) {
            object.setEstadoRegistroDB(Constante.ESTADO_EDITADO);
            object.setId(data.getIdRow());
        }

        Long idRow = viewModel.insertSbcOffline(object);

        if (idRow != null) {
            viewModel.deleteCategoriaByIdSbcOffline(idRow);
            CrearCategoriaSbcEntity crearCategoriaSbcEntity = null;
            for (int x = 0; x < lstDetalleSbc.size(); x++) {
                crearCategoriaSbcEntity = new CrearCategoriaSbcEntity();
                crearCategoriaSbcEntity.setIdsbcdetalle(lstDetalleSbc.get(x).getIdSbcDetalle());
                crearCategoriaSbcEntity.setIdsbcCategoria(lstDetalleSbc.get(x).getIdSbcCategoria());
                crearCategoriaSbcEntity.setIdsbcCategoriaItems(lstDetalleSbc.get(x).getIdSbcCategoriaItems());
                crearCategoriaSbcEntity.setSeguro(lstDetalleSbc.get(x).getSeguro());
                crearCategoriaSbcEntity.setRiesgoso(lstDetalleSbc.get(x).getRiesgoso());
                crearCategoriaSbcEntity.setIdbarrera(lstDetalleSbc.get(x).getIdbarrera());
                crearCategoriaSbcEntity.setDescripcionItem(lstDetalleSbc.get(x).getDescripcionItem());
                crearCategoriaSbcEntity.setObservacionSbcDetalle(lstDetalleSbc.get(x).getObservacionSbcDetalle());
                if (lstDetalleSbc.get(x).getIdSbc() != null) {
                    crearCategoriaSbcEntity.setIdSbc(lstDetalleSbc.get(x).getIdSbc());
                }
                crearCategoriaSbcEntity.setIdOffline(idRow);
                viewModel.insertCategoriaOffline(crearCategoriaSbcEntity);
            }

            for (int x = 0; x < lstNotificacion.size(); x++) {
                NotificarEntity notificarEntity = null;
                if (lstNotificacion.get(x).getSelectEmail()) {
                    notificarEntity = new NotificarEntity();
                    notificarEntity.setIdUser(lstNotificacion.get(x).getIdUsers());
                    notificarEntity.setNombre(lstNotificacion.get(x).getNombreUsuario());
                    notificarEntity.setEmail(lstNotificacion.get(x).getEmailCorporativo());
                    notificarEntity.setIdModulo(Constante.MODULO_SBC);
                    notificarEntity.setIdOffline(idRow);
                    viewModel.insertNotificarOffline(notificarEntity);
                }
            }
            UtilMDP.hideProgreesDialog();
            showCreateRegistro("Estimado, tu registro se ha guardado con éxito. Cuando haya conexión a Internet se sincronizara con la web");
        } else {
            UtilMDP.hideProgreesDialog();
        }
    }

    public void showCreateRegistro(String mensaje) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Registro Guardado");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage(mensaje);
        alertDialogBuilder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                finish();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
