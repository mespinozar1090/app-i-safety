package com.mdp.fusionapp.ui.fragment.incidente;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mdp.fusionapp.R;
import com.mdp.fusionapp.database.entity.EmpresaSbcEntity;
import com.mdp.fusionapp.model.IncidenteInsertModel;
import com.mdp.fusionapp.network.response.ActividadEntity;
import com.mdp.fusionapp.network.response.ComboMaestroEntity;
import com.mdp.fusionapp.network.response.IncidenteDetalleResponse;
import com.mdp.fusionapp.network.response.InspeccionEmpresaEntity;
import com.mdp.fusionapp.ui.activity.incidentes.IncidenteAcitvity;
import com.mdp.fusionapp.utilitary.NetworkUtil;
import com.mdp.fusionapp.viewModel.InspeccionViewModel;
import com.mdp.fusionapp.viewModel.SbcViewModel;

import java.util.List;

public class PrincipalFragment extends Fragment implements View.OnClickListener {

    private SbcViewModel mViewModel;
    private InspeccionViewModel viewModel;
    private RadioButton rbtContratista, rbtREP;
    private LinearLayout lnlRep, lnlContratisa;
    private IncidenteDetalleResponse incidente;
    private Button btnSiguiente;

    //Opcion REP
    Spinner spRazonSocial, spTamanioC, spEconomicaC, spEconomicaI, spRazonSocialC, spRazonSocialI;
    TextInputLayout tilRuc, tiltamanio, tileconomica;
    TextInputEditText tietRuc, tiettamanio, tieteconomica;

    //OpcionContratista
    TextInputLayout tiltRucC;
    TextInputEditText tietRucC;//tietRazonSocialC

    //Intermediacion
    TextInputLayout  tilRucIs, tilNombreTrabajadorI;
    TextInputEditText tietRucI, tietNombreTrabajadorI;
    EditText edtMensaje;

    //variables insert
    String empleadorTipo, razonSocialI, razonSocialC;
    Integer idActividadEconomicas, fEmpleadorIdEmpresa, fEmpleadorIdTamanioEmpresa,
            fEmpleadorIIdActividadEconomica, idEmpresaContratista, idEmpresaContratistaI;


    public static PrincipalFragment newInstance(IncidenteDetalleResponse detalle) {
        PrincipalFragment fragment = new PrincipalFragment();
        Bundle args = new Bundle();
        args.putSerializable("detalle", detalle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            incidente = (IncidenteDetalleResponse) getArguments().getSerializable("detalle");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_principal, container, false);
        viewModel = new ViewModelProvider(this).get(InspeccionViewModel.class);
        initView(view);
        return view;
    }

    private void initView(View view) {
        edtMensaje = view.findViewById(R.id.edtMensaje);
        lnlRep = view.findViewById(R.id.lnlRep);
        lnlContratisa = view.findViewById(R.id.lnlContratisa);
        rbtContratista = view.findViewById(R.id.rbtContratista);
        rbtREP = view.findViewById(R.id.rbtREP);
        rbtREP.setChecked(true);
        empleadorTipo = "REP";
        btnSiguiente = view.findViewById(R.id.btnSiguiente);
        rbtREP.setOnClickListener(this);
        rbtContratista.setOnClickListener(this);
        btnSiguiente.setOnClickListener(this);

        //opcion REP
        spRazonSocial = view.findViewById(R.id.spRazonSocial);
        tilRuc = view.findViewById(R.id.tilRuc);
        tietRuc = view.findViewById(R.id.tietRuc);
        tiltamanio = view.findViewById(R.id.tiltamanio);
        tiettamanio = view.findViewById(R.id.tiettamanio);
        tileconomica = view.findViewById(R.id.tileconomica);
        tieteconomica = view.findViewById(R.id.tieteconomica);

        spRazonSocialC = view.findViewById(R.id.spRazonSocialC);
        spRazonSocialI = view.findViewById(R.id.spRazonSocialI);

        //opcion Contratita
        spTamanioC = view.findViewById(R.id.spTamanioC);
        spEconomicaC = view.findViewById(R.id.spEconomicaC);
        tietRucC = view.findViewById(R.id.tietRucC);
        tiltRucC = view.findViewById(R.id.tiltRucC);


        //Intermediacion
        spEconomicaI = view.findViewById(R.id.spEconomicaI);
        tilRucIs = view.findViewById(R.id.tilRucIs);
        tietRucI = view.findViewById(R.id.tietRucI);
        tilNombreTrabajadorI = view.findViewById(R.id.tilNombreTrabajadorI);
        tietNombreTrabajadorI = view.findViewById(R.id.tietNombreTrabajadorI);

        loadEmpresa();

        edtMensaje.setText("");
        tietRucI.setText("");
        tietNombreTrabajadorI.setText("");

        loadDetalle();
        loadContratista();
    }

    private void loadContratista() {
        mViewModel = new ViewModelProvider(this).get(SbcViewModel.class);
        if (NetworkUtil.isOnline(requireContext())) {
            mViewModel.obtenerEmpresaSBSResponse().observe(getActivity(), new Observer<List<EmpresaSbcEntity>>() {
                @Override
                public void onChanged(List<EmpresaSbcEntity> empresaSBSResponses) {
                    if (null != empresaSBSResponses)
                        loadEmpresas(empresaSBSResponses);
                }
            });
        }else{
            loadEmpresas(mViewModel.obtenerEmpresaSBSResponseOffline());
        }

    }

    private void loadEmpresas(List<EmpresaSbcEntity> empresaSBSResponses) {
        ArrayAdapter<EmpresaSbcEntity> adapterTipoDocumento = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, empresaSBSResponses);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spRazonSocialC.setAdapter(adapterTipoDocumento);
        spRazonSocialC.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                EmpresaSbcEntity selectedItem = (EmpresaSbcEntity) parent.getSelectedItem();
                idEmpresaContratista = selectedItem.getIdEmpresaObservadora();
                fEmpleadorIdEmpresa = selectedItem.getIdEmpresaObservadora();
                razonSocialC = selectedItem.getDescripcion();
                tietRuc.setText(selectedItem.getRuc());

                loadTamanio(null);
                loadActividaEconomica(null);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (validarObject()) {
            if (null != incidente.getEmpleadoridempresa()) {
                Log.e("idEmpresa", ":" + incidente.getEmpleadoridempresa().getIdElemento());
                for (int x = 0; x < empresaSBSResponses.size(); x++) {
                    if (empresaSBSResponses.get(x).getIdEmpresaObservadora().intValue() == incidente.getEmpleadoridempresa().getIdElemento().intValue()) {
                        spRazonSocialC.setSelection(x);
                    }
                }
            }
        }

        spRazonSocialI.setAdapter(adapterTipoDocumento);
        spRazonSocialI.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                EmpresaSbcEntity selectedItem = (EmpresaSbcEntity) parent.getSelectedItem();

                idEmpresaContratistaI = selectedItem.getIdEmpresaObservadora();
                razonSocialI = selectedItem.getDescripcion();
                Log.e("data", "#" + selectedItem.getRuc());
                tietRucI.setText(selectedItem.getRuc());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (validarObject()) {
            tietRucI.setText(incidente.getEmpleadorIRuc());//F_EmpleadorI_IdEmpresa
            if (null != incidente.getFempleadorIIdEmpresa().getIdElemento()) {
                for (int x = 0; x < empresaSBSResponses.size(); x++) {
                    if (empresaSBSResponses.get(x).getIdEmpresaObservadora().intValue() == incidente.getFempleadorIIdEmpresa().getIdElemento().intValue()) {
                        spRazonSocialI.setSelection(x);
                    }
                }
            }
        }

    }

    private Boolean validarObject() {
        return incidente != null ? true : false;
    }

    private void loadDetalle() {
        if (validarObject()) {
            edtMensaje.setText(incidente.getDescriptionIncidente());
            tietRucI.setText(incidente.getEmpleadorIRuc());
            tietNombreTrabajadorI.setText(incidente.getTrabajadorNombreApellido());

            if (incidente.getEmpleadorTipo().equals("REP")) {
                lnlRep.setVisibility(View.VISIBLE);
                lnlContratisa.setVisibility(View.GONE);
                clearAccion(1);
                empleadorTipo = "REP";
                rbtREP.setChecked(true);
                rbtContratista.setChecked(false);

            } else if (incidente.getEmpleadorTipo().equals("CONTRATISTA")) {
                lnlRep.setVisibility(View.GONE);
                lnlContratisa.setVisibility(View.VISIBLE);
                clearAccion(2);
                empleadorTipo = "CONTRATISTA";
                rbtContratista.setChecked(true);
                rbtREP.setChecked(false);
            }
        }
    }

    public IncidenteInsertModel saveIndecentePrincipla() {
        IncidenteInsertModel object = new IncidenteInsertModel();
        object.setEmpleadorTipo(empleadorTipo);
        object.setfDescripcionIncidente(edtMensaje.getText().toString());
        object.setfEmpleadorIdActividadEconomica(idActividadEconomicas);
        object.setfEmpleadorIdEmpresa(fEmpleadorIdEmpresa);
        object.setfEmpleadorIdTamanioEmpresa(fEmpleadorIdTamanioEmpresa);
        object.setfEmpleadorIIdActividadEconomica(fEmpleadorIIdActividadEconomica);
        object.setfEmpleadorIRazonSocial(razonSocialI);

        object.setpEmpleadorIdEmpresa(fEmpleadorIdEmpresa);
        object.setfEmpleadorIIdEmpresa(idEmpresaContratistaI);

        object.setfEmpleadorIRuc(tietRucI.getText().toString());

        object.setfEmpleadorRazonSocial(razonSocialC);
        object.setfEmpleadorRuc(tietRuc.getText().toString().equals("") ? tietRuc.getText().toString() : tietRucI.getText().toString());
        object.setgDescripcionFormato(edtMensaje.getText().toString());
        object.setfTrabajadorNombresApellidos(tietNombreTrabajadorI.getText().toString());
        return object;
    }

    private void loadEmpresa() {

        if (NetworkUtil.isOnline(getContext())) {
            viewModel.obtenerEmpresa().observe(getActivity(), new Observer<List<InspeccionEmpresaEntity>>() {
                @Override
                public void onChanged(List<InspeccionEmpresaEntity> inspeccionEmpresaRespons) {
                    if (null != inspeccionEmpresaRespons) {
                        spinnerEmpresas(inspeccionEmpresaRespons);
                    }
                }
            });
        } else {
            spinnerEmpresas(viewModel.obtenerEmpresaOffline());
        }

    }

    private void spinnerEmpresas(List<InspeccionEmpresaEntity> inspeccionEmpresaRespons) {
        ArrayAdapter<InspeccionEmpresaEntity> adapterTipoDocumento = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, inspeccionEmpresaRespons);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spRazonSocial.setAdapter(adapterTipoDocumento);
        spRazonSocial.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                InspeccionEmpresaEntity object = (InspeccionEmpresaEntity) parent.getSelectedItem();
                if (object.getIdEmpresaMaestra() != 0) {
                    fEmpleadorIdEmpresa = object.getIdEmpresaMaestra();
                    String ruc = object.getRuc().toString();
                    String[] arrOfStr = ruc.split("[.]");
                    tietRuc.setText(arrOfStr[0]);
                    razonSocialC = object.getRazonsocial();
                    loadTamanio(object.getIdEmpresaMaestra());
                    loadActividaEconomica(object.getIdActividadEconomica());
                } else {
                    fEmpleadorIdEmpresa = 0;
                    loadTamanio(null);
                    loadActividaEconomica(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        if (validarObject()) {
            if (null != incidente.getEmpleadoridempresa()) {
                for (int x = 0; x < inspeccionEmpresaRespons.size(); x++) {
                    if (inspeccionEmpresaRespons.get(x).getIdEmpresaMaestra().intValue() == incidente.getEmpleadoridempresa().getIdElemento()) {
                        spRazonSocial.setSelection(x);
                    }
                }
            }
        }
    }

    private void loadTamanio(Integer idEmpresa) {

        if (NetworkUtil.isOnline(requireContext())) {
            String[] array = {"1", "1"};
            viewModel.obtenerComboMaestro(array).observe(getActivity(), new Observer<List<ComboMaestroEntity>>() {
                @Override
                public void onChanged(List<ComboMaestroEntity> comboMaestroRespons) {
                    if (null != comboMaestroRespons) {
                        spinnerTamnioEmpresaC(comboMaestroRespons);
                        for (ComboMaestroEntity data : comboMaestroRespons) {
                            if (idEmpresa != null) {
                                Log.e("getIdElemento", "" + data.getIdElemento().intValue() + " == " + idEmpresa.intValue());
                                Log.e("idEmpresa", "" + idEmpresa);
                                if (data.getIdElemento().intValue() == idEmpresa.intValue()) {//&& data.getEstadoElemento()
                                    fEmpleadorIdTamanioEmpresa = data.getIdElemento();
                                    tiettamanio.setText(data.getDescripcionElemento());
                                }
                            }
                        }
                    }
                }
            });
        } else {
            List<ComboMaestroEntity> comboMaestroRespons = viewModel.obtenerComboMaestroOffline();
            spinnerTamnioEmpresaC(comboMaestroRespons);
            for (ComboMaestroEntity data : comboMaestroRespons) {
                if (idEmpresa != null) {
                    if (data.getIdElemento().intValue() == idEmpresa.intValue()) {
                        fEmpleadorIdTamanioEmpresa = data.getIdElemento();
                        tiettamanio.setText(data.getDescripcionElemento());
                    }
                }

            }
        }

    }

    private void loadActividaEconomica(Integer idActividadEconomica) {
        if (NetworkUtil.isOnline(requireContext())) {
            viewModel.obtenerActividad().observe(getActivity(), new Observer<List<ActividadEntity>>() {
                @Override
                public void onChanged(List<ActividadEntity> actividadRespons) {
                    if (null != actividadRespons) {
                        spinnerEconomia(actividadRespons, idActividadEconomica);

                    }
                }
            });
        } else {
            spinnerEconomia(viewModel.obtenerActividadOffline(), idActividadEconomica);
        }

    }

    private void spinnerTamnioEmpresaC(List<ComboMaestroEntity> comboMaestroRespons) {
        ArrayAdapter<ComboMaestroEntity> adapterTipoDocumento = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, comboMaestroRespons);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTamanioC.setAdapter(adapterTipoDocumento);
        spTamanioC.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ComboMaestroEntity object = (ComboMaestroEntity) parent.getSelectedItem();
                fEmpleadorIdTamanioEmpresa = object.getIdElemento();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (validarObject()) {
            for (int x = 0; x < comboMaestroRespons.size(); x++) {
                if (comboMaestroRespons.get(x).getIdElemento() == incidente.getEmpleadorIdTamanioEmpresa().getIdElemento()) {
                    spTamanioC.setSelection(x);
                }
            }
        }
    }

    private void spinnerEconomia(List<ActividadEntity> actividadRespons, Integer idActividadEconomica) {
        ArrayAdapter<ActividadEntity> adapterTipoDocumento = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, actividadRespons);
        adapterTipoDocumento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spEconomicaC.setAdapter(adapterTipoDocumento);
        spEconomicaC.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ActividadEntity object = (ActividadEntity) parent.getSelectedItem();
                idActividadEconomicas = object.getIdActividadEconomica();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for (ActividadEntity data : actividadRespons) {
            if (idActividadEconomica != null) {
                if (data.getIdActividadEconomica().intValue() == idActividadEconomica.intValue() && data.getEstadoActividadEconomica()) {
                    tieteconomica.setText(data.getDescripcion());
                    idActividadEconomicas = data.getIdActividadEconomica();
                }
            }
        }

        if (validarObject()) {
            for (int x = 0; x < actividadRespons.size(); x++) {
                if (actividadRespons.get(x).getIdActividadEconomica().intValue() == incidente.getEmpleadorIdActividadEconomica().getIdElemento()) {
                    spEconomicaC.setSelection(x);
                }
            }
        }

        spEconomicaI.setAdapter(adapterTipoDocumento);
        spEconomicaI.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ActividadEntity object = (ActividadEntity) parent.getSelectedItem();
                fEmpleadorIIdActividadEconomica = object.getIdActividadEconomica();
                Log.e("TAMANIOID :", " " + object.getDescripcion());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (validarObject()) {
            for (int x = 0; x < actividadRespons.size(); x++) {
                if (actividadRespons.get(x).getIdActividadEconomica().intValue() == incidente.getEmpleadorIIdActividadeconomica().getIdElemento()) {
                    spEconomicaI.setSelection(x);
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rbtREP:
                boolean checkeds = ((RadioButton) v).isChecked();
                if (checkeds)
                    lnlRep.setVisibility(View.VISIBLE);
                lnlContratisa.setVisibility(View.GONE);
                clearAccion(1);
                empleadorTipo = "REP";
                break;
            case R.id.rbtContratista:
                boolean checked = ((RadioButton) v).isChecked();
                if (checked)
                    lnlRep.setVisibility(View.GONE);
                lnlContratisa.setVisibility(View.VISIBLE);
                clearAccion(2);
                empleadorTipo = "CONTRATISTA";
                break;
            case R.id.btnSiguiente:
                enablePager();
                break;
        }
    }

    private Boolean valiarCampos() {

        if (empleadorTipo.equals("REP")) {
            if (fEmpleadorIdEmpresa == 0) {
                Toast.makeText(getContext(), "Seleccione Razon social", Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        if (empleadorTipo.equals("CONTRATISTA")) {
            if(razonSocialC == null ||razonSocialC.equals("")){
                Toast.makeText(getContext(), "Ingrese razon social contratista", Toast.LENGTH_SHORT).show();
                return false;
            }



            if (tietRucC.getText().toString() == null || tietRucC.getText().toString().trim().equals("")) {
                Toast.makeText(getContext(), "Ingrese RUC contratista", Toast.LENGTH_SHORT).show();
                return false;
            }

            if (fEmpleadorIdTamanioEmpresa == 0) {
                Toast.makeText(getContext(), "Seleccione tamaniño de la empresa", Toast.LENGTH_SHORT).show();
                return false;
            }

            if (idActividadEconomicas == 0) {
                Toast.makeText(getContext(), "Seleccione actividad económica de contratista", Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        if (razonSocialI == null || razonSocialI.equals("")) {
            Toast.makeText(getContext(), "Seleccione Razon Social Intermedicación", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (tietRucI.getText().toString() == null || tietRucI.getText().toString().trim().equals("")) {
            Toast.makeText(getContext(), "Ingrese RUC Intermedicación", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (fEmpleadorIIdActividadEconomica == 0) {
            Toast.makeText(getContext(), "Seleccione actividad económica de Intermedicación", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (tietNombreTrabajadorI.getText().toString() == null || tietNombreTrabajadorI.getText().toString().trim().equals("")) {
            Toast.makeText(getContext(), "Ingrese Nombre y Apellido del trabajor", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (edtMensaje.getText().toString() == null || edtMensaje.getText().toString().trim().equals("")) {
            Toast.makeText(getContext(), "Ingrese decripción del incidente / Accidente", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void enablePager() {
        if (valiarCampos()) {
            ((IncidenteAcitvity) getActivity()).disableScroll(false);
            ((IncidenteAcitvity) getActivity()).setViewPager(1);
        }
    }

    private void clearAccion(Integer valor) {
        if (valor == 1) {
          //  tietRazonSocialC.setText("");
            tietRucC.setText("");
        } else {
            tietRuc.setText("");
        }

    }
}
