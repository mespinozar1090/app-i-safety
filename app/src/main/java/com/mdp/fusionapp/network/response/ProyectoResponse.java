package com.mdp.fusionapp.network.response;

import com.google.gson.annotations.SerializedName;

public class ProyectoResponse {

    @SerializedName("idProyecto")
    private int idProyecto;

    @SerializedName("Codigo")
    private String codigo;

    @SerializedName("Nombre")
    private String nombre;

    @SerializedName("Id_Empresa_Maestra")
    private String idEmpresaMaestra;

    public ProyectoResponse() {
    }

    public ProyectoResponse(int idProyecto, String nombre) {
        this.idProyecto = idProyecto;
        this.nombre = nombre;
    }

    public ProyectoResponse(int idProyecto, String codigo, String nombre, String idEmpresaMaestra) {
        this.idProyecto = idProyecto;
        this.codigo = codigo;
        this.nombre = nombre;
        this.idEmpresaMaestra = idEmpresaMaestra;
    }

    public int getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(int idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdEmpresaMaestra() {
        return idEmpresaMaestra;
    }

    public void setIdEmpresaMaestra(String idEmpresaMaestra) {
        this.idEmpresaMaestra = idEmpresaMaestra;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
