package com.mdp.fusionapp.network.response;

import com.google.gson.annotations.SerializedName;

public class BuenaPracticaResponse {

    @SerializedName("idCategoriaBuenaPractica")
    private Integer idCategoriaBuenaPractica;

    @SerializedName("Nombre")
    private String nombre;

    @SerializedName("nota")
    private String nota;

    public Integer getIdCategoriaBuenaPractica() {
        return idCategoriaBuenaPractica;
    }

    public void setIdCategoriaBuenaPractica(Integer idCategoriaBuenaPractica) {
        this.idCategoriaBuenaPractica = idCategoriaBuenaPractica;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    @Override
    public String toString() {
        return nombre;
    }

}
