package com.mdp.fusionapp.network.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Respuesta implements Serializable {

    @SerializedName("Message")
    private String message;
    @SerializedName("Estatus")
    private Boolean estatus;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getEstatus() {
        return estatus;
    }

    public void setEstatus(Boolean estatus) {
        this.estatus = estatus;
    }
}
