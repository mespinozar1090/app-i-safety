package com.mdp.fusionapp.network.response;

import com.google.gson.annotations.SerializedName;

public class SuccesResponse {

    @SerializedName("Message")
    private String message;

    @SerializedName("Estatus")
    private Boolean estatus;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getEstatus() {
        return estatus;
    }

    public void setEstatus(Boolean estatus) {
        this.estatus = estatus;
    }
}
