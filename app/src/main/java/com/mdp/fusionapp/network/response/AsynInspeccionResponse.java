package com.mdp.fusionapp.network.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AsynInspeccionResponse {

    @SerializedName("Id_Inspecciones")
    private Integer idInspecciones;

    @SerializedName("Id_Empresa_contratista")
    private Integer idEmpresaContratista;

    @SerializedName("CodigoInspeccion")
    private String codigoInspeccion;

    @SerializedName("Razon_Social")
    private String razonSocial;

    @SerializedName("Ruc")
    private Double ruc;

    @SerializedName("Id_Usuario")
    private Integer idUsuario;

    @SerializedName("Id_Empresa_Observadora")
    private Integer idEmpresaObservadora;

    @SerializedName("Torre")
    private String torre;

    @SerializedName("fecha_Hora_Inspeccion")
    private String fechaHoraInspeccion;

    @SerializedName("Responsable_Area_Inspeccion")
    private String responsableAreaInspeccion;

    @SerializedName("Responsable_Inspeccion")
    private String responsableInspeccion;

    @SerializedName("Tipo_inspeccion")
    private Integer tipoInspeccion;

    @SerializedName("Area_Inspeccionada")
    private String areaInspeccionada;

    @SerializedName("Estado_Inspeccion")
    private Integer estadoInspeccion;

    @SerializedName("Nombre_Formato")
    private String nombreFormato;

    @SerializedName("NombreAutor")
    private String nombreAutor;

    @SerializedName("idTipoUbicacion")
    private Integer idTipoUbicacion;

    @SerializedName("idProyecto")
    private Integer idProyecto;

    @SerializedName("idLugar")
    private Integer idLugar;

    @SerializedName("Sede")
    private String sede;

    @SerializedName("Id_Actividad_Economica")
    private Integer idActividadEconomica;

    @SerializedName("Domicilio_Legal")
    private String domicilioLegal;

    @SerializedName("Nro_Trabajadores")
    private Integer numeroTrabajadores;


    @SerializedName("Id_Usuario_Registro")
    private Integer idUsuarioRegistro;

    @SerializedName("EmpresaContratista")
    private String empresaContratista;


    private List<BuenaPracticas> lstBuenasPracticas;
    private List<DetalleInspeccion> lstDetalleInspeccion;

    public String getEmpresaContratista() {
        return empresaContratista;
    }

    public void setEmpresaContratista(String empresaContratista) {
        this.empresaContratista = empresaContratista;
    }

    public Integer getIdUsuarioRegistro() {
        return idUsuarioRegistro;
    }

    public void setIdUsuarioRegistro(Integer idUsuarioRegistro) {
        this.idUsuarioRegistro = idUsuarioRegistro;
    }

    public Integer getNumeroTrabajadores() {
        return numeroTrabajadores;
    }

    public void setNumeroTrabajadores(Integer numeroTrabajadores) {
        this.numeroTrabajadores = numeroTrabajadores;
    }

    public String getDomicilioLegal() {
        return domicilioLegal;
    }

    public void setDomicilioLegal(String domicilioLegal) {
        this.domicilioLegal = domicilioLegal;
    }

    public Integer getIdActividadEconomica() {
        return idActividadEconomica;
    }

    public void setIdActividadEconomica(Integer idActividadEconomica) {
        this.idActividadEconomica = idActividadEconomica;
    }

    public Integer getIdInspecciones() {
        return idInspecciones;
    }

    public void setIdInspecciones(Integer idInspecciones) {
        this.idInspecciones = idInspecciones;
    }

    public Integer getIdEmpresaContratista() {
        return idEmpresaContratista;
    }

    public void setIdEmpresaContratista(Integer idEmpresaContratista) {
        this.idEmpresaContratista = idEmpresaContratista;
    }

    public String getCodigoInspeccion() {
        return codigoInspeccion;
    }

    public void setCodigoInspeccion(String codigoInspeccion) {
        this.codigoInspeccion = codigoInspeccion;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }


    public Double getRuc() {
        return ruc;
    }

    public void setRuc(Double ruc) {
        this.ruc = ruc;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdEmpresaObservadora() {
        return idEmpresaObservadora;
    }

    public void setIdEmpresaObservadora(Integer idEmpresaObservadora) {
        this.idEmpresaObservadora = idEmpresaObservadora;
    }

    public String getTorre() {
        return torre;
    }

    public void setTorre(String torre) {
        this.torre = torre;
    }

    public String getFechaHoraInspeccion() {
        return fechaHoraInspeccion;
    }

    public void setFechaHoraInspeccion(String fechaHoraInspeccion) {
        this.fechaHoraInspeccion = fechaHoraInspeccion;
    }

    public String getResponsableAreaInspeccion() {
        return responsableAreaInspeccion;
    }

    public void setResponsableAreaInspeccion(String responsableAreaInspeccion) {
        this.responsableAreaInspeccion = responsableAreaInspeccion;
    }

    public String getResponsableInspeccion() {
        return responsableInspeccion;
    }

    public void setResponsableInspeccion(String responsableInspeccion) {
        this.responsableInspeccion = responsableInspeccion;
    }

    public Integer getTipoInspeccion() {
        return tipoInspeccion;
    }

    public void setTipoInspeccion(Integer tipoInspeccion) {
        this.tipoInspeccion = tipoInspeccion;
    }

    public String getAreaInspeccionada() {
        return areaInspeccionada;
    }

    public void setAreaInspeccionada(String areaInspeccionada) {
        this.areaInspeccionada = areaInspeccionada;
    }

    public Integer getEstadoInspeccion() {
        return estadoInspeccion;
    }

    public void setEstadoInspeccion(Integer estadoInspeccion) {
        this.estadoInspeccion = estadoInspeccion;
    }

    public String getNombreFormato() {
        return nombreFormato;
    }

    public void setNombreFormato(String nombreFormato) {
        this.nombreFormato = nombreFormato;
    }

    public String getNombreAutor() {
        return nombreAutor;
    }

    public void setNombreAutor(String nombreAutor) {
        this.nombreAutor = nombreAutor;
    }

    public Integer getIdTipoUbicacion() {
        return idTipoUbicacion;
    }

    public void setIdTipoUbicacion(Integer idTipoUbicacion) {
        this.idTipoUbicacion = idTipoUbicacion;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public Integer getIdLugar() {
        return idLugar;
    }

    public void setIdLugar(Integer idLugar) {
        this.idLugar = idLugar;
    }

    public String getSede() {
        return sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }

    public List<BuenaPracticas> getLstBuenasPracticas() {
        return lstBuenasPracticas;
    }

    public void setLstBuenasPracticas(List<BuenaPracticas> lstBuenasPracticas) {
        this.lstBuenasPracticas = lstBuenasPracticas;
    }

    public List<DetalleInspeccion> getLstDetalleInspeccion() {
        return lstDetalleInspeccion;
    }

    public void setLstDetalleInspeccion(List<DetalleInspeccion> lstDetalleInspeccion) {
        this.lstDetalleInspeccion = lstDetalleInspeccion;
    }

    public class BuenaPracticas {

        @SerializedName("idBuenasPracticas")
        private Integer idBuenasPracticas;

        @SerializedName("idProyecto")
        private Integer idProyecto;

        @SerializedName("Id_Inspecciones")
        private Integer idInspecciones;

        @SerializedName("idCategoriaBuenaPractica")
        private Integer idCategoriaBuenaPractica;

        @SerializedName("Id_Empresa_contratista")
        private Integer idEmpresaContratista;

        @SerializedName("categoriaBuenaPractica")
        private String categoriaBuenaPractica;

        @SerializedName("Descripcion")
        private String descripcion;

        @SerializedName("urlImagen")
        private String imgB64;

        public Integer getIdBuenasPracticas() {
            return idBuenasPracticas;
        }

        public void setIdBuenasPracticas(Integer idBuenasPracticas) {
            this.idBuenasPracticas = idBuenasPracticas;
        }

        public Integer getIdProyecto() {
            return idProyecto;
        }

        public void setIdProyecto(Integer idProyecto) {
            this.idProyecto = idProyecto;
        }

        public Integer getIdInspecciones() {
            return idInspecciones;
        }

        public void setIdInspecciones(Integer idInspecciones) {
            this.idInspecciones = idInspecciones;
        }

        public Integer getIdCategoriaBuenaPractica() {
            return idCategoriaBuenaPractica;
        }

        public void setIdCategoriaBuenaPractica(Integer idCategoriaBuenaPractica) {
            this.idCategoriaBuenaPractica = idCategoriaBuenaPractica;
        }

        public Integer getIdEmpresaContratista() {
            return idEmpresaContratista;
        }

        public void setIdEmpresaContratista(Integer idEmpresaContratista) {
            this.idEmpresaContratista = idEmpresaContratista;
        }

        public String getCategoriaBuenaPractica() {
            return categoriaBuenaPractica;
        }

        public void setCategoriaBuenaPractica(String categoriaBuenaPractica) {
            this.categoriaBuenaPractica = categoriaBuenaPractica;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        public String getImgB64() {
            return imgB64;
        }

        public void setImgB64(String imgB64) {
            this.imgB64 = imgB64;
        }
    }

    public class DetalleInspeccion {
        @SerializedName("Id_Inspecciones_reporte_detalle")
        private Integer idInspeccionesReporteDetalle;

        @SerializedName("Acto_Subestandar")
        private String actoSubestandar;

        @SerializedName("Riesgo_A")
        private Boolean riesgoA;

        @SerializedName("Riesgo_M")
        private Boolean riesgoM;

        @SerializedName("Riesgo_B")
        private Boolean riesgoB;

        @SerializedName("Resposable_Area_detalle")
        private String resposableAreaDetalle;

        @SerializedName("Evidencia_Fotografica_Observaciones")
        private String evidenciaFotograficaObservaciones;

        @SerializedName("Accion_Mitigadora")
        private String accionMitigadora;

        @SerializedName("PlazoString")
        private String plazo;

        @SerializedName("Evidencia_Cierre")
        private String evidencia_Cierre;

        @SerializedName("Tipo_Riesgo")
        private Integer tipoRiesgo;

        @SerializedName("Descripcion_EvidenciaFotoGraficaObservacion")
        private String descripcionEvidenciaFotoGraficaObservacion;

        private String fileImageDetalleEvidencia;
        private String fileImageDetalleEvidenciaObservacion;

        @SerializedName("id_NoConformidad")
        private Integer idNoConformidad;

        @SerializedName("id_ActoSubestandar")
        private Integer idActoSubestandar;


        @SerializedName("id_SubFamiliaAmbiental")
        private Integer idSubFamiliaAmbiental;

        @SerializedName("id_CondicionSubestandar")
        private Integer idCondicionSubestandar;

        @SerializedName("idTipoGestion")
        private Integer idTipoGestion;

        @SerializedName("idHallazgo")
        private Integer idHallazgo;

        @SerializedName("Estado_CierreInspeccion")
        private Boolean estadoCierreInspeccion;

        @SerializedName("TipoGestion")
        private String nombreTipoGestion;

        @SerializedName("Hallazgo")
        private String nombreTipoHallazgo;

        @SerializedName("Descripcion_ActoSubestandar")
        private String nombreActoSubestandar;

        @SerializedName("Id_Rol_Usuario")
        private Integer usuarioRol;

        @SerializedName("LstEvidencia_Fotografica_Observaciones")
        private List<EvidenciaHallazgo> listEvidenciaHallazgo;

        @SerializedName("LstEvidencia_Cierre")
        private List<EvidenciaHallazgo> lstEvidenciaCierre;

        @SerializedName("Estado_DetalleInspeccion")
        private Boolean estadoDetalleInspeccion;

        @SerializedName("Id_Usuario_Registro")
        private Integer idUsuarioRegistro;

        public Integer getIdUsuarioRegistro() {
            return idUsuarioRegistro;
        }

        public void setIdUsuarioRegistro(Integer idUsuarioRegistro) {
            this.idUsuarioRegistro = idUsuarioRegistro;
        }

        public Integer getIdInspeccionesReporteDetalle() {
            return idInspeccionesReporteDetalle;
        }

        public void setIdInspeccionesReporteDetalle(Integer idInspeccionesReporteDetalle) {
            this.idInspeccionesReporteDetalle = idInspeccionesReporteDetalle;
        }

        public String getActoSubestandar() {
            return actoSubestandar;
        }

        public void setActoSubestandar(String actoSubestandar) {
            this.actoSubestandar = actoSubestandar;
        }

        public Boolean getRiesgoA() {
            return riesgoA;
        }

        public void setRiesgoA(Boolean riesgoA) {
            this.riesgoA = riesgoA;
        }

        public Boolean getRiesgoM() {
            return riesgoM;
        }

        public void setRiesgoM(Boolean riesgoM) {
            this.riesgoM = riesgoM;
        }

        public Boolean getRiesgoB() {
            return riesgoB;
        }

        public void setRiesgoB(Boolean riesgoB) {
            this.riesgoB = riesgoB;
        }

        public String getResposableAreaDetalle() {
            return resposableAreaDetalle;
        }

        public void setResposableAreaDetalle(String resposableAreaDetalle) {
            this.resposableAreaDetalle = resposableAreaDetalle;
        }

        public String getEvidenciaFotograficaObservaciones() {
            return evidenciaFotograficaObservaciones;
        }

        public void setEvidenciaFotograficaObservaciones(String evidenciaFotograficaObservaciones) {
            this.evidenciaFotograficaObservaciones = evidenciaFotograficaObservaciones;
        }

        public String getAccionMitigadora() {
            return accionMitigadora;
        }

        public void setAccionMitigadora(String accionMitigadora) {
            this.accionMitigadora = accionMitigadora;
        }

        public String getPlazo() {
            return plazo;
        }

        public void setPlazo(String plazo) {
            this.plazo = plazo;
        }

        public String getEvidencia_Cierre() {
            return evidencia_Cierre;
        }

        public void setEvidencia_Cierre(String evidencia_Cierre) {
            this.evidencia_Cierre = evidencia_Cierre;
        }

        public Integer getTipoRiesgo() {
            return tipoRiesgo;
        }

        public void setTipoRiesgo(Integer tipoRiesgo) {
            this.tipoRiesgo = tipoRiesgo;
        }

        public String getDescripcionEvidenciaFotoGraficaObservacion() {
            return descripcionEvidenciaFotoGraficaObservacion;
        }

        public void setDescripcionEvidenciaFotoGraficaObservacion(String descripcionEvidenciaFotoGraficaObservacion) {
            this.descripcionEvidenciaFotoGraficaObservacion = descripcionEvidenciaFotoGraficaObservacion;
        }

        public String getFileImageDetalleEvidencia() {
            return fileImageDetalleEvidencia;
        }

        public void setFileImageDetalleEvidencia(String fileImageDetalleEvidencia) {
            this.fileImageDetalleEvidencia = fileImageDetalleEvidencia;
        }

        public String getFileImageDetalleEvidenciaObservacion() {
            return fileImageDetalleEvidenciaObservacion;
        }

        public void setFileImageDetalleEvidenciaObservacion(String fileImageDetalleEvidenciaObservacion) {
            this.fileImageDetalleEvidenciaObservacion = fileImageDetalleEvidenciaObservacion;
        }

        public Integer getIdNoConformidad() {
            return idNoConformidad;
        }

        public void setIdNoConformidad(Integer idNoConformidad) {
            this.idNoConformidad = idNoConformidad;
        }

        public Integer getIdActoSubestandar() {
            return idActoSubestandar;
        }

        public void setIdActoSubestandar(Integer idActoSubestandar) {
            this.idActoSubestandar = idActoSubestandar;
        }

        public Integer getIdSubFamiliaAmbiental() {
            return idSubFamiliaAmbiental;
        }

        public void setIdSubFamiliaAmbiental(Integer idSubFamiliaAmbiental) {
            this.idSubFamiliaAmbiental = idSubFamiliaAmbiental;
        }

        public Integer getIdCondicionSubestandar() {
            return idCondicionSubestandar;
        }

        public void setIdCondicionSubestandar(Integer idCondicionSubestandar) {
            this.idCondicionSubestandar = idCondicionSubestandar;
        }

        public Integer getIdTipoGestion() {
            return idTipoGestion;
        }

        public void setIdTipoGestion(Integer idTipoGestion) {
            this.idTipoGestion = idTipoGestion;
        }

        public Integer getIdHallazgo() {
            return idHallazgo;
        }

        public void setIdHallazgo(Integer idHallazgo) {
            this.idHallazgo = idHallazgo;
        }

        public Boolean getEstadoCierreInspeccion() {
            return estadoCierreInspeccion;
        }

        public void setEstadoCierreInspeccion(Boolean estadoCierreInspeccion) {
            this.estadoCierreInspeccion = estadoCierreInspeccion;
        }

        public String getNombreTipoGestion() {
            return nombreTipoGestion;
        }

        public void setNombreTipoGestion(String nombreTipoGestion) {
            this.nombreTipoGestion = nombreTipoGestion;
        }

        public String getNombreTipoHallazgo() {
            return nombreTipoHallazgo;
        }

        public void setNombreTipoHallazgo(String nombreTipoHallazgo) {
            this.nombreTipoHallazgo = nombreTipoHallazgo;
        }

        public String getNombreActoSubestandar() {
            return nombreActoSubestandar;
        }

        public void setNombreActoSubestandar(String nombreActoSubestandar) {
            this.nombreActoSubestandar = nombreActoSubestandar;
        }

        public Integer getUsuarioRol() {
            return usuarioRol;
        }

        public void setUsuarioRol(Integer usuarioRol) {
            this.usuarioRol = usuarioRol;
        }

        public List<EvidenciaHallazgo> getListEvidenciaHallazgo() {
            return listEvidenciaHallazgo;
        }

        public void setListEvidenciaHallazgo(List<EvidenciaHallazgo> listEvidenciaHallazgo) {
            this.listEvidenciaHallazgo = listEvidenciaHallazgo;
        }

        public List<EvidenciaHallazgo> getLstEvidenciaCierre() {
            return lstEvidenciaCierre;
        }

        public void setLstEvidenciaCierre(List<EvidenciaHallazgo> lstEvidenciaCierre) {
            this.lstEvidenciaCierre = lstEvidenciaCierre;
        }

        public Boolean getEstadoDetalleInspeccion() {
            return estadoDetalleInspeccion;
        }

        public void setEstadoDetalleInspeccion(Boolean estadoDetalleInspeccion) {
            this.estadoDetalleInspeccion = estadoDetalleInspeccion;
        }
    }


    public class EvidenciaHallazgo {
        @SerializedName("urlImagen")
        private String imgB64;
        private String fechaFoto;

        public String getFechaFoto() {
            return fechaFoto;
        }

        public void setFechaFoto(String fechaFoto) {
            this.fechaFoto = fechaFoto;
        }

        public String getImgB64() {
            return imgB64;
        }

        public void setImgB64(String imgB64) {
            this.imgB64 = imgB64;
        }
    }

}
