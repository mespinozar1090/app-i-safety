package com.mdp.fusionapp.network.response.detalle;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class DetalleAuditoriaResponse implements Serializable {

    private Integer idRow;

    @SerializedName("Id_Audit")
    private Integer idAudit;

    @SerializedName("Id_Empresa_contratista")
    private Integer idEmpresaContratista;

    @SerializedName("Sede")
    private Integer sede;

    @SerializedName("Nro_Contrato")
    private String nroContrato;

    @SerializedName("Supervisor_Contrato")
    private String supervisorContrato;

    @SerializedName("Responsable_Contratista")
    private String responsableContratista;

    @SerializedName("Fecha_Audit")
    private String fechaAudit;

    @SerializedName("Codigo_Audit")
    private String codigoAudit;

    @SerializedName("Estado_Audit")
    private Integer estadoAudit;

    @SerializedName("NombreAutor")
    private String nombreAutor;

    @SerializedName("Fecha_Reg")
    private String fechaReg;

    @SerializedName("lstDetalleAudit")
    private List<DetalleAuditoria> lstDetalleAudit;


    public Integer getIdRow() {
        return idRow;
    }

    public void setIdRow(Integer idRow) {
        this.idRow = idRow;
    }

    public Integer getIdAudit() {
        return idAudit;
    }

    public void setIdAudit(Integer idAudit) {
        this.idAudit = idAudit;
    }

    public Integer getIdEmpresaContratista() {
        return idEmpresaContratista;
    }

    public void setIdEmpresaContratista(Integer idEmpresaContratista) {
        this.idEmpresaContratista = idEmpresaContratista;
    }

    public Integer getSede() {
        return sede;
    }

    public void setSede(Integer sede) {
        this.sede = sede;
    }

    public String getNroContrato() {
        return nroContrato;
    }

    public void setNroContrato(String nroContrato) {
        this.nroContrato = nroContrato;
    }

    public String getSupervisorContrato() {
        return supervisorContrato;
    }

    public void setSupervisorContrato(String supervisorContrato) {
        this.supervisorContrato = supervisorContrato;
    }

    public String getResponsableContratista() {
        return responsableContratista;
    }

    public void setResponsableContratista(String responsableContratista) {
        this.responsableContratista = responsableContratista;
    }

    public String getFechaAudit() {
        return fechaAudit;
    }

    public void setFechaAudit(String fechaAudit) {
        this.fechaAudit = fechaAudit;
    }

    public String getNombreAutor() {
        return nombreAutor;
    }

    public void setNombreAutor(String nombreAutor) {
        this.nombreAutor = nombreAutor;
    }

    public String getFechaReg() {
        return fechaReg;
    }

    public void setFechaReg(String fechaReg) {
        this.fechaReg = fechaReg;
    }

    public String getCodigoAudit() {
        return codigoAudit;
    }

    public void setCodigoAudit(String codigoAudit) {
        this.codigoAudit = codigoAudit;
    }

    public Integer getEstadoAudit() {
        return estadoAudit;
    }

    public void setEstadoAudit(Integer estadoAudit) {
        this.estadoAudit = estadoAudit;
    }

    public List<DetalleAuditoria> getLstDetalleAudit() {
        return lstDetalleAudit;
    }

    public void setLstDetalleAudit(List<DetalleAuditoria> lstDetalleAudit) {
        this.lstDetalleAudit = lstDetalleAudit;
    }

    public static class DetalleAuditoria implements Serializable {

        @SerializedName("Id_Audit")
        private Integer idAudit;

        @SerializedName("Id_Audit_Detalle")
        private Integer idAuditDetalle;

        @SerializedName("Id_Audit_Items")
        private Integer idAuditItems;

        @SerializedName("Id_Audit_Lin_SubItems")
        private Integer idAuditLinSubItems;

        @SerializedName("Calificacion")
        private Integer calificacion;

        @SerializedName("Lugar")
        private Integer lugar;

        @SerializedName("Notas")
        private String notas;

        @SerializedName("TextEvidencia")
        private String textEvidencia;

        @SerializedName("Descripcion_Items")
        private String descripcionItem;


        @SerializedName("LstEvidencia")
        private List<EnvidenciaAuditoria> lstEvidencia;

        public String getDescripcionItem() {
            return descripcionItem;
        }

        public void setDescripcionItem(String descripcionItem) {
            this.descripcionItem = descripcionItem;
        }

        public Integer getIdAudit() {
            return idAudit;
        }

        public void setIdAudit(Integer idAudit) {
            this.idAudit = idAudit;
        }

        public Integer getIdAuditDetalle() {
            return idAuditDetalle;
        }

        public void setIdAuditDetalle(Integer idAuditDetalle) {
            this.idAuditDetalle = idAuditDetalle;
        }

        public Integer getIdAuditItems() {
            return idAuditItems;
        }

        public void setIdAuditItems(Integer idAuditItems) {
            this.idAuditItems = idAuditItems;
        }

        public Integer getIdAuditLinSubItems() {
            return idAuditLinSubItems;
        }

        public void setIdAuditLinSubItems(Integer idAuditLinSubItems) {
            this.idAuditLinSubItems = idAuditLinSubItems;
        }

        public Integer getCalificacion() {
            return calificacion;
        }

        public void setCalificacion(Integer calificacion) {
            this.calificacion = calificacion;
        }

        public Integer getLugar() {
            return lugar;
        }

        public void setLugar(Integer lugar) {
            this.lugar = lugar;
        }

        public String getNotas() {
            return notas;
        }

        public void setNotas(String notas) {
            this.notas = notas;
        }

        public String getTextEvidencia() {
            return textEvidencia;
        }

        public void setTextEvidencia(String textEvidencia) {
            this.textEvidencia = textEvidencia;
        }

        public List<EnvidenciaAuditoria> getLstEvidencia() {
            return lstEvidencia;
        }

        public void setLstEvidencia(List<EnvidenciaAuditoria> lstEvidencia) {
            this.lstEvidencia = lstEvidencia;
        }
    }

    public static class EnvidenciaAuditoria implements Serializable {

        @SerializedName("Id_Elemento")
        private Integer idElemento;

        @SerializedName("Imagen")
        private String imagen;

        @SerializedName("NombreImagen")
        private String nombreImagen;

        public EnvidenciaAuditoria() {
        }

        public EnvidenciaAuditoria(String imagen) {
            this.imagen = imagen;
        }

        public Integer getIdElemento() {
            return idElemento;
        }

        public void setIdElemento(Integer idElemento) {
            this.idElemento = idElemento;
        }

        public String getImagen() {
            return imagen;
        }

        public void setImagen(String imagen) {
            this.imagen = imagen;
        }

        public String getNombreImagen() {
            return nombreImagen;
        }

        public void setNombreImagen(String nombreImagen) {
            this.nombreImagen = nombreImagen;
        }
    }


}
