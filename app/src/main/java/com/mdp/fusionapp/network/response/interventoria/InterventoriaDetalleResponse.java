package com.mdp.fusionapp.network.response.interventoria;

import com.google.gson.annotations.SerializedName;
import com.mdp.fusionapp.network.response.Respuesta;

import java.io.Serializable;
import java.util.List;

public class InterventoriaDetalleResponse implements Serializable {

    @SerializedName("Data")
    private InterventoriaDetalle data;

    public InterventoriaDetalle getData() {
        return data;
    }

    public void setData(InterventoriaDetalle data) {
        this.data = data;
    }

    public static class InterventoriaDetalle implements Serializable {

        @SerializedName("Id_interventoria")
        private Integer idInterventoria;

        @SerializedName("IdUsuario")
        private Integer idUsuario;

        @SerializedName("Estado")
        private Integer estado;

        @SerializedName("Empresa_Interventor")
        private Integer empresaInterventor;

        @SerializedName("Id_Empresa_Intervenida")
        private Integer idEmpresaIntervenida;

        @SerializedName("Fecha_Registro")
        private String fechaRegistro;

        @SerializedName("Interventor")
        private String interventor;

        @SerializedName("Fecha_Hora")
        private String fecha_Hora;

        @SerializedName("Actividad")
        private String actividad;

        @SerializedName("Id_Plan_Trabajo")
        private Integer idPlanTrabajo;

        @SerializedName("Empresa_Intervenida")
        private String empresaIntervenida;

        @SerializedName("Supervisor")
        private String Supervisor;

        @SerializedName("Supervisor_Sustituto")
        private String supervisorSustituto;

        @SerializedName("Linea_Subestacion")
        private String lineaSubestacion;

        @SerializedName("Nro_Plan_Trabajo")
        private String nroPlanTrabajo;

        @SerializedName("Plan_Trabajo")
        private String planTrabajo;

        @SerializedName("Lugar_Zona")
        private String lugarZona;

        @SerializedName("Fecha_Registro_Log")
        private String fechaRegistroLog;

        @SerializedName("Codigo_Interventoria")
        private String codigoInterventoria;

        @SerializedName("Concluciones")
        private String concluciones;

        @SerializedName("Tipo_Ubicacion")
        private String tipoUbicacion; //lineaSubestacion

        @SerializedName("LstVerificacionesDet")
        private List<VerificacionDetalle> lstVerificacionesDet;

        @SerializedName("LstControlesDet")
        private List<ControlDetalle> lstControlesDet;

        @SerializedName("LstImagenesDet")
        private List<EvidenciaInterventoria> lstImagenesDet;

        public Integer getIdInterventoria() {
            return idInterventoria;
        }

        public void setIdInterventoria(Integer idInterventoria) {
            this.idInterventoria = idInterventoria;
        }

        public String getTipoUbicacion() {
            return tipoUbicacion;
        }

        public void setTipoUbicacion(String tipoUbicacion) {
            this.tipoUbicacion = tipoUbicacion;
        }

        public Integer getIdUsuario() {
            return idUsuario;
        }

        public void setIdUsuario(Integer idUsuario) {
            this.idUsuario = idUsuario;
        }

        public Integer getEstado() {
            return estado;
        }

        public void setEstado(Integer estado) {
            this.estado = estado;
        }

        public Integer getEmpresaInterventor() {
            return empresaInterventor;
        }

        public void setEmpresaInterventor(Integer empresaInterventor) {
            this.empresaInterventor = empresaInterventor;
        }

        public Integer getIdEmpresaIntervenida() {
            return idEmpresaIntervenida;
        }

        public void setIdEmpresaIntervenida(Integer idEmpresaIntervenida) {
            this.idEmpresaIntervenida = idEmpresaIntervenida;
        }

        public String getFechaRegistro() {
            return fechaRegistro;
        }

        public void setFechaRegistro(String fechaRegistro) {
            this.fechaRegistro = fechaRegistro;
        }

        public String getInterventor() {
            return interventor;
        }

        public void setInterventor(String interventor) {
            this.interventor = interventor;
        }

        public String getFecha_Hora() {
            return fecha_Hora;
        }

        public void setFecha_Hora(String fecha_Hora) {
            this.fecha_Hora = fecha_Hora;
        }

        public String getActividad() {
            return actividad;
        }

        public void setActividad(String actividad) {
            this.actividad = actividad;
        }

        public Integer getIdPlanTrabajo() {
            return idPlanTrabajo;
        }

        public void setIdPlanTrabajo(Integer idPlanTrabajo) {
            this.idPlanTrabajo = idPlanTrabajo;
        }

        public String getEmpresaIntervenida() {
            return empresaIntervenida;
        }

        public void setEmpresaIntervenida(String empresaIntervenida) {
            this.empresaIntervenida = empresaIntervenida;
        }

        public String getSupervisor() {
            return Supervisor;
        }

        public void setSupervisor(String supervisor) {
            Supervisor = supervisor;
        }

        public String getSupervisorSustituto() {
            return supervisorSustituto;
        }

        public void setSupervisorSustituto(String supervisorSustituto) {
            this.supervisorSustituto = supervisorSustituto;
        }

        public String getLineaSubestacion() {
            return lineaSubestacion;
        }

        public void setLineaSubestacion(String lineaSubestacion) {
            this.lineaSubestacion = lineaSubestacion;
        }

        public String getNroPlanTrabajo() {
            return nroPlanTrabajo;
        }

        public void setNroPlanTrabajo(String nroPlanTrabajo) {
            this.nroPlanTrabajo = nroPlanTrabajo;
        }

        public String getPlanTrabajo() {
            return planTrabajo;
        }

        public void setPlanTrabajo(String planTrabajo) {
            this.planTrabajo = planTrabajo;
        }

        public String getLugarZona() {
            return lugarZona;
        }

        public void setLugarZona(String lugarZona) {
            this.lugarZona = lugarZona;
        }

        public String getConcluciones() {
            return concluciones;
        }

        public void setConcluciones(String concluciones) {
            this.concluciones = concluciones;
        }

        public List<VerificacionDetalle> getLstVerificacionesDet() {
            return lstVerificacionesDet;
        }

        public void setLstVerificacionesDet(List<VerificacionDetalle> lstVerificacionesDet) {
            this.lstVerificacionesDet = lstVerificacionesDet;
        }

        public List<ControlDetalle> getLstControlesDet() {
            return lstControlesDet;
        }

        public void setLstControlesDet(List<ControlDetalle> lstControlesDet) {
            this.lstControlesDet = lstControlesDet;
        }
    }

    public class EvidenciaInterventoria implements Serializable {

        @SerializedName("Id_Interventoria_Imagen")
        private Integer idInterventoriaImagen;

        @SerializedName("Id_Interventoria")
        private Integer idInterventoria;

        @SerializedName("Orden")
        private Integer orden;

        @SerializedName("Imagen")
        private String imagen;

        @SerializedName("Descripcion")
        private String descripcion;

        @SerializedName("FechaCreacion")
        private String fechaCreacion;

        public Integer getIdInterventoriaImagen() {
            return idInterventoriaImagen;
        }

        public void setIdInterventoriaImagen(Integer idInterventoriaImagen) {
            this.idInterventoriaImagen = idInterventoriaImagen;
        }

        public Integer getIdInterventoria() {
            return idInterventoria;
        }

        public void setIdInterventoria(Integer idInterventoria) {
            this.idInterventoria = idInterventoria;
        }

        public Integer getOrden() {
            return orden;
        }

        public void setOrden(Integer orden) {
            this.orden = orden;
        }

        public String getImagen() {
            return imagen;
        }

        public void setImagen(String imagen) {
            this.imagen = imagen;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        public String getFechaCreacion() {
            return fechaCreacion;
        }

        public void setFechaCreacion(String fechaCreacion) {
            this.fechaCreacion = fechaCreacion;
        }
    }

    public static class VerificacionDetalle implements Serializable {

        @SerializedName("Id_Interventoria_Detalle")
        private Integer idInterventoriaDetalle;

        @SerializedName("Cumple")
        private Integer cumple;

        @SerializedName("Id_Verificacion_Subitem")
        private Integer idVerificacionSubitem;

        @SerializedName("Id_Verificacion")
        private Integer idVerificacion;

        @SerializedName("Id_Interventoria")
        private Integer idInterventoria;

        @SerializedName("Descripcion")
        private String descripcion;

        @SerializedName("Id_Verificacion_Padre")
        private Integer idVerificacionPadre;

        @SerializedName("Id_Verificacion_Item")
        private Integer idVerificacionItem;

        public Integer getIdVerificacionPadre() {
            return idVerificacionPadre;
        }

        public void setIdVerificacionPadre(Integer idVerificacionPadre) {
            this.idVerificacionPadre = idVerificacionPadre;
        }

        public Integer getIdVerificacionItem() {
            return idVerificacionItem;
        }

        public void setIdVerificacionItem(Integer idVerificacionItem) {
            this.idVerificacionItem = idVerificacionItem;
        }

        public Integer getIdInterventoriaDetalle() {
            return idInterventoriaDetalle;
        }

        public void setIdInterventoriaDetalle(Integer idInterventoriaDetalle) {
            this.idInterventoriaDetalle = idInterventoriaDetalle;
        }

        public Integer getCumple() {
            return cumple;
        }

        public void setCumple(Integer cumple) {
            this.cumple = cumple;
        }

        public Integer getIdVerificacionSubitem() {
            return idVerificacionSubitem;
        }

        public void setIdVerificacionSubitem(Integer idVerificacionSubitem) {
            this.idVerificacionSubitem = idVerificacionSubitem;
        }

        public Integer getIdVerificacion() {
            return idVerificacion;
        }

        public void setIdVerificacion(Integer idVerificacion) {
            this.idVerificacion = idVerificacion;
        }

        public Integer getIdInterventoria() {
            return idInterventoria;
        }

        public void setIdInterventoria(Integer idInterventoria) {
            this.idInterventoria = idInterventoria;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }
    }

    public static class ControlDetalle implements Serializable {

        @SerializedName("Id_Control_Item")
        private Integer idControlItem;

        @SerializedName("Cantidad")
        private Double cantidad;

        @SerializedName("Id_Control_Detalle")
        private Integer idControlDetalle;

        @SerializedName("Id_Control")
        private Integer idControl;

        @SerializedName("Id_Interventoria")
        private Integer idInterventoria;

        @SerializedName("Descripcion")
        private String descripcion;

        public Integer getIdControlItem() {
            return idControlItem;
        }

        public void setIdControlItem(Integer idControlItem) {
            this.idControlItem = idControlItem;
        }

        public Double getCantidad() {
            return cantidad;
        }

        public void setCantidad(Double cantidad) {
            this.cantidad = cantidad;
        }

        public Integer getIdControlDetalle() {
            return idControlDetalle;
        }

        public void setIdControlDetalle(Integer idControlDetalle) {
            this.idControlDetalle = idControlDetalle;
        }

        public Integer getIdControl() {
            return idControl;
        }

        public void setIdControl(Integer idControl) {
            this.idControl = idControl;
        }

        public Integer getIdInterventoria() {
            return idInterventoria;
        }

        public void setIdInterventoria(Integer idInterventoria) {
            this.idInterventoria = idInterventoria;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }
    }
}
