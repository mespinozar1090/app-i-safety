package com.mdp.fusionapp.network.reposiroty;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.mdp.fusionapp.database.repository.ControlSubItemRepository;
import com.mdp.fusionapp.database.repository.VerificacionSubItemRepository;
import com.mdp.fusionapp.database.entity.interventoria.ControlSubItemEntity;
import com.mdp.fusionapp.network.response.InterventoriaResponse;
import com.mdp.fusionapp.network.response.Respuesta;
import com.mdp.fusionapp.network.response.SubEstacionResponse;
import com.mdp.fusionapp.database.entity.interventoria.VerificacionSubItemEntity;
import com.mdp.fusionapp.network.response.interventoria.EvidenciaInterResponse;
import com.mdp.fusionapp.network.response.interventoria.InterventoriaDetalleOffineResponse;
import com.mdp.fusionapp.network.response.interventoria.InterventoriaDetalleResponse;
import com.mdp.fusionapp.network.service.APIService;
import com.mdp.fusionapp.utilitary.UtilMDP;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InterventoriaRespository {

    private final String LOGGER = "InterventoriaRespository";
    private APIService apiService;

    private static InterventoriaRespository instancia;

    public InterventoriaRespository() {
    }

    public static InterventoriaRespository getInstance() {
        if (instancia == null) {
            instancia = new InterventoriaRespository();
        }
        return instancia;
    }

    public MutableLiveData<InterventoriaResponse> listarInterventoria(HashMap hashMap, Context context) {
        final MutableLiveData<InterventoriaResponse> listarSBCMutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<InterventoriaResponse> listCall = apiService.listarInterventoria(hashMap);

        listCall.enqueue(new Callback<InterventoriaResponse>() {
            @Override
            public void onResponse(Call<InterventoriaResponse> call, Response<InterventoriaResponse> response) {
                Log.e("TAG", "onResponse: " + response.body());
                listarSBCMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<InterventoriaResponse> call, Throwable t) {
                Log.e("TAG", "Throwable: " + t.getMessage());
                listarSBCMutableLiveData.setValue(null);
            }
        });
        return listarSBCMutableLiveData;
    }

    public MutableLiveData<Boolean> borrarInterventoria(HashMap hashMap) {
        final MutableLiveData<Boolean> listarSBCMutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<Boolean> listCall = apiService.borrarInterventoria(hashMap);

        listCall.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                Log.e("TAG", "onResponse: " + response.body());
                listarSBCMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.e("TAG", "Throwable: " + t.getMessage());
                listarSBCMutableLiveData.setValue(null);
            }
        });
        return listarSBCMutableLiveData;
    }


    public MutableLiveData<List<SubEstacionResponse>> obtenerSubEstacion(Context context) {
        final MutableLiveData<List<SubEstacionResponse>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<SubEstacionResponse>> listCall = apiService.obtenerSubEstacion();

        listCall.enqueue(new Callback<List<SubEstacionResponse>>() {
            @Override
            public void onResponse(Call<List<SubEstacionResponse>> call, Response<List<SubEstacionResponse>> response) {
                List<SubEstacionResponse> list = new ArrayList<>();
                list.add(new SubEstacionResponse(0, "Seleccionar"));
                list.addAll(response.body());
                mutableLiveData.setValue(list);
            }

            @Override
            public void onFailure(Call<List<SubEstacionResponse>> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<List<VerificacionSubItemEntity>> obtenerVerificacionSubItem(Context context) {
        final MutableLiveData<List<VerificacionSubItemEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<VerificacionSubItemEntity>> listCall = apiService.obtenerVerificacionSubItem();

        listCall.enqueue(new Callback<List<VerificacionSubItemEntity>>() {
            @Override
            public void onResponse(Call<List<VerificacionSubItemEntity>> call, Response<List<VerificacionSubItemEntity>> response) {
                mutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<VerificacionSubItemEntity>> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<List<ControlSubItemEntity>> obtenerControlSubItem(ControlSubItemRepository controlSubItemRepository) {
        final MutableLiveData<List<ControlSubItemEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<ControlSubItemEntity>> listCall = apiService.obtenerControlSubItem();

        listCall.enqueue(new Callback<List<ControlSubItemEntity>>() {
            @Override
            public void onResponse(Call<List<ControlSubItemEntity>> call, Response<List<ControlSubItemEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    controlSubItemRepository.deleteAll();
                    for (int i = 0; i < response.body().size(); i++) {
                        controlSubItemRepository.insert(response.body().get(i));
                    }
                    mutableLiveData.setValue(controlSubItemRepository.findAll());
                }
            }

            @Override
            public void onFailure(Call<List<ControlSubItemEntity>> call, Throwable t) {
                mutableLiveData.setValue(controlSubItemRepository.findAll());
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<Respuesta> insertarInterventoria(HashMap hashMap, Context context) {
        final MutableLiveData<Respuesta> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<Respuesta> listCall = apiService.insertarInterventoria(hashMap);

        listCall.enqueue(new Callback<Respuesta>() {
            @Override
            public void onResponse(Call<Respuesta> call, Response<Respuesta> response) {
                mutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<Respuesta> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<InterventoriaDetalleResponse> interventoriaDetalle(Integer idInteventoria) {
        final MutableLiveData<InterventoriaDetalleResponse> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<InterventoriaDetalleResponse> empresaProyecto = apiService.interventoriaDetalle(idInteventoria);

        empresaProyecto.enqueue(new Callback<InterventoriaDetalleResponse>() {
            @Override
            public void onResponse(Call<InterventoriaDetalleResponse> call, Response<InterventoriaDetalleResponse> response) {
                mutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<InterventoriaDetalleResponse> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<Respuesta> editarInterventoria(HashMap hashMap) {
        final MutableLiveData<Respuesta> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<Respuesta> empresaProyecto = apiService.editarInterventoria(hashMap);

        empresaProyecto.enqueue(new Callback<Respuesta>() {
            @Override
            public void onResponse(Call<Respuesta> call, Response<Respuesta> response) {
                mutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<Respuesta> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<EvidenciaInterResponse> evidenciaInterventoria(Integer idInteventoria) {
        final MutableLiveData<EvidenciaInterResponse> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<EvidenciaInterResponse> empresaProyecto = apiService.evidenciaInterventoria(idInteventoria);

        empresaProyecto.enqueue(new Callback<EvidenciaInterResponse>() {
            @Override
            public void onResponse(Call<EvidenciaInterResponse> call, Response<EvidenciaInterResponse> response) {
                mutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<EvidenciaInterResponse> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<List<VerificacionSubItemEntity>> listaVerificacionesSubitems(VerificacionSubItemRepository verificacionSubItemRepository) {
        final MutableLiveData<List<VerificacionSubItemEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<VerificacionSubItemEntity>> listCall = apiService.listaVerificacionesSubitems();

        listCall.enqueue(new Callback<List<VerificacionSubItemEntity>>() {
            @Override
            public void onResponse(Call<List<VerificacionSubItemEntity>> call, Response<List<VerificacionSubItemEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    verificacionSubItemRepository.deleteAll();
                    for (int i = 0; i < response.body().size(); i++) {
                        verificacionSubItemRepository.insert(response.body().get(i));
                    }
                    mutableLiveData.setValue(verificacionSubItemRepository.findVerificacionSubItem());
                }
            }

            @Override
            public void onFailure(Call<List<VerificacionSubItemEntity>> call, Throwable t) {
                mutableLiveData.setValue(verificacionSubItemRepository.findVerificacionSubItem());
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<List<InterventoriaDetalleOffineResponse>> detalleInterventoriaOffline(HashMap hashMap) {
        final MutableLiveData<List<InterventoriaDetalleOffineResponse>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<InterventoriaDetalleOffineResponse>> empresaProyecto = apiService.detalleInterventoriaOffline(hashMap);

        empresaProyecto.enqueue(new Callback<List<InterventoriaDetalleOffineResponse>>() {
            @Override
            public void onResponse(Call<List<InterventoriaDetalleOffineResponse>> call, Response<List<InterventoriaDetalleOffineResponse>> response) {
                mutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<InterventoriaDetalleOffineResponse>> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }
}

