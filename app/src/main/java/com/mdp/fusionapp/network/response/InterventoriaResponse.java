package com.mdp.fusionapp.network.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class InterventoriaResponse implements Serializable {

    @SerializedName("Data")
    private List<Data> data;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data {

        private Integer idRow;

        @SerializedName("Id")
        private Integer idInterventoria;

        @SerializedName("Estado")
        private Integer estado;

        @SerializedName("Fecha_Registro")
        private String fechaRegistro;

        @SerializedName("Nombre_Formato")
        private String nombreFormato;

        @SerializedName("Nombre_Observador")
        private String nombreUsuario;

        @SerializedName("Codigo")
        private String codigo;

        @SerializedName("IdUsuario")
        private Integer idUsuario;

        @SerializedName("id_Empresa_Intervenida")
        private Integer idEmpresaIntervenida;

        @SerializedName("Id_Empresa_Interventor")
        private Integer idEmpresaInterventor;

        @SerializedName("Empresa_Intervenida")
        private String empresaIntervenida;

        @SerializedName("Id_UsuarioNotificado")
        private String idUsuarioNotificado;

        public Integer getIdInterventoria() {
            return idInterventoria;
        }

        public void setIdInterventoria(Integer idInterventoria) {
            this.idInterventoria = idInterventoria;
        }

        public Integer getIdRow() {
            return idRow;
        }

        public void setIdRow(Integer idRow) {
            this.idRow = idRow;
        }

        public Integer getEstado() {
            return estado;
        }

        public void setEstado(Integer estado) {
            this.estado = estado;
        }

        public String getFechaRegistro() {
            return fechaRegistro;
        }

        public void setFechaRegistro(String fechaRegistro) {
            this.fechaRegistro = fechaRegistro;
        }

        public String getNombreFormato() {
            return nombreFormato;
        }

        public void setNombreFormato(String nombreFormato) {
            this.nombreFormato = nombreFormato;
        }

        public String getNombreUsuario() {
            return nombreUsuario;
        }

        public void setNombreUsuario(String nombreUsuario) {
            this.nombreUsuario = nombreUsuario;
        }

        public String getCodigo() {
            return codigo;
        }

        public void setCodigo(String codigo) {
            this.codigo = codigo;
        }

        public Integer getIdUsuario() {
            return idUsuario;
        }

        public void setIdUsuario(Integer idUsuario) {
            this.idUsuario = idUsuario;
        }

        public Integer getIdEmpresaIntervenida() {
            return idEmpresaIntervenida;
        }

        public void setIdEmpresaIntervenida(Integer idEmpresaIntervenida) {
            this.idEmpresaIntervenida = idEmpresaIntervenida;
        }

        public Integer getIdEmpresaInterventor() {
            return idEmpresaInterventor;
        }

        public void setIdEmpresaInterventor(Integer idEmpresaInterventor) {
            this.idEmpresaInterventor = idEmpresaInterventor;
        }

        public String getEmpresaIntervenida() {
            return empresaIntervenida;
        }

        public void setEmpresaIntervenida(String empresaIntervenida) {
            this.empresaIntervenida = empresaIntervenida;
        }

        public String getIdUsuarioNotificado() {
            return idUsuarioNotificado;
        }

        public void setIdUsuarioNotificado(String idUsuarioNotificado) {
            this.idUsuarioNotificado = idUsuarioNotificado;
        }
    }

}
