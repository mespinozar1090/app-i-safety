package com.mdp.fusionapp.network.response;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "incidente_empresa_table")
public class InspeccionEmpresaEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("Id_Empresa_Maestra")
    private Integer idEmpresaMaestra;

    @SerializedName("Razon_social")
    private String razonsocial;

    @SerializedName("Ruc")
    private Long ruc;

    @SerializedName("Domicilio_Legal")
    private String domicilioLegal;

    @SerializedName("Nro_Trabajadores")
    private Integer nroTrabajadores;

    @SerializedName("Id_Actividad_Economica")
    private Integer idActividadEconomica;

    @SerializedName("Id_Tamanio_Empresa")
    private Integer idTamanioEmpresa;


    public InspeccionEmpresaEntity(Integer idEmpresaMaestra, String razonsocial) {
        this.idEmpresaMaestra = idEmpresaMaestra;
        this.razonsocial = razonsocial;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdEmpresaMaestra() {
        return idEmpresaMaestra;
    }

    public void setIdEmpresaMaestra(Integer idEmpresaMaestra) {
        this.idEmpresaMaestra = idEmpresaMaestra;
    }

    public String getRazonsocial() {
        return razonsocial;
    }

    public void setRazonsocial(String razonsocial) {
        this.razonsocial = razonsocial;
    }

    public Long getRuc() {
        return ruc;
    }

    public void setRuc(Long ruc) {
        this.ruc = ruc;
    }

    public String getDomicilioLegal() {
        return domicilioLegal;
    }

    public void setDomicilioLegal(String domicilioLegal) {
        this.domicilioLegal = domicilioLegal;
    }

    public Integer getNroTrabajadores() {
        return nroTrabajadores;
    }

    public void setNroTrabajadores(Integer nroTrabajadores) {
        this.nroTrabajadores = nroTrabajadores;
    }

    public Integer getIdActividadEconomica() {
        return idActividadEconomica;
    }

    public void setIdActividadEconomica(Integer idActividadEconomica) {
        this.idActividadEconomica = idActividadEconomica;
    }

    public Integer getIdTamanioEmpresa() {
        return idTamanioEmpresa;
    }

    public void setIdTamanioEmpresa(Integer idTamanioEmpresa) {
        this.idTamanioEmpresa = idTamanioEmpresa;
    }

    @Override
    public String toString() {
        return razonsocial;
    }
}
