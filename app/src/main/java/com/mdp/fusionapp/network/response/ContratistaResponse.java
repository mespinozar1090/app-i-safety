package com.mdp.fusionapp.network.response;

import com.google.gson.annotations.SerializedName;

public class ContratistaResponse {

    @SerializedName("Id_Empresa_Observadora")
    private Integer idEmpresaObservadora;

    @SerializedName("Descripcion")
    private String descripcion;

    public ContratistaResponse() {
    }

    public ContratistaResponse(Integer idEmpresaObservadora, String descripcion) {
        this.idEmpresaObservadora = idEmpresaObservadora;
        this.descripcion = descripcion;
    }

    public Integer getIdEmpresaObservadora() {
        return idEmpresaObservadora;
    }

    public void setIdEmpresaObservadora(Integer idEmpresaObservadora) {
        this.idEmpresaObservadora = idEmpresaObservadora;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return  descripcion;
    }
}
