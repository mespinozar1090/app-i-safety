package com.mdp.fusionapp.network.reposiroty;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.mdp.fusionapp.database.entity.CategoriaBuenaPracticaEntity;
import com.mdp.fusionapp.database.entity.ContratistaEntity;
import com.mdp.fusionapp.database.entity.EmpresaMaestraEntity;
import com.mdp.fusionapp.database.entity.LugarEntity;
import com.mdp.fusionapp.database.entity.ProyectoEntity;
import com.mdp.fusionapp.database.entity.TipoUbicacionEntity;
import com.mdp.fusionapp.database.entity.UsuarioResponsableEntity;
import com.mdp.fusionapp.database.entity.incidente.TipoEventoEntity;
import com.mdp.fusionapp.database.repository.AreaTrabajoRepository;
import com.mdp.fusionapp.database.repository.CategoriaBuenaPracticaRepository;
import com.mdp.fusionapp.database.repository.ContratistaRepository;
import com.mdp.fusionapp.database.repository.EmpresaMaestraRepository;
import com.mdp.fusionapp.database.repository.LugarRepository;
import com.mdp.fusionapp.database.repository.ProyectoRepository;
import com.mdp.fusionapp.database.repository.TipoUbicacionRepository;
import com.mdp.fusionapp.database.repository.UsuarioResponsableRepository;
import com.mdp.fusionapp.database.repository.incidente.ActividadRepository;
import com.mdp.fusionapp.database.repository.incidente.ComboMaestroRepository;
import com.mdp.fusionapp.database.repository.incidente.IncidenteEmpresaRepository;
import com.mdp.fusionapp.database.repository.incidente.TipoEventoRepository;
import com.mdp.fusionapp.network.response.AsynInspeccionResponse;
import com.mdp.fusionapp.network.response.CreateInspeccionResponse;
import com.mdp.fusionapp.network.response.TorreLugarResponse;
import com.mdp.fusionapp.network.response.ActividadEntity;
import com.mdp.fusionapp.database.entity.AreaTrabajoEntity;
import com.mdp.fusionapp.network.response.ComboMaestroEntity;
import com.mdp.fusionapp.network.response.InspeccionDetalleResponse;
import com.mdp.fusionapp.network.response.InspeccionEmpresaEntity;
import com.mdp.fusionapp.network.response.InspeccionResponse;
import com.mdp.fusionapp.network.service.APIService;
import com.mdp.fusionapp.utilitary.UtilMDP;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InspeccionRepository {

    private final String LOGGER = "InpeccionRepository";
    private APIService apiService;
    private static InspeccionRepository instancia;

    public InspeccionRepository() {
    }

    public static InspeccionRepository getInstance() {
        if (instancia == null) {
            instancia = new InspeccionRepository();
        }
        return instancia;
    }

    public MutableLiveData<List<InspeccionResponse>> listaInpecciones(HashMap hashMap, Context context) {
        final MutableLiveData<List<InspeccionResponse>> inspeccionResponseMutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<InspeccionResponse>> inspeccionResponseCall = apiService.listaInpecciones(hashMap);

        inspeccionResponseCall.enqueue(new Callback<List<InspeccionResponse>>() {
            @Override
            public void onResponse(Call<List<InspeccionResponse>> call, Response<List<InspeccionResponse>> response) {
                inspeccionResponseMutableLiveData.setValue(response.body());
                Log.e(LOGGER, "" + response.code());
            }

            @Override
            public void onFailure(Call<List<InspeccionResponse>> call, Throwable t) {
                Log.e(LOGGER, "onFailure" + t.getMessage());
                inspeccionResponseMutableLiveData.setValue(null);
            }
        });
        return inspeccionResponseMutableLiveData;
    }

    public MutableLiveData<InspeccionDetalleResponse> detalleInspeccion(HashMap hashMap, Context context) {
        final MutableLiveData<InspeccionDetalleResponse> inspeccionDetalleMutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<InspeccionDetalleResponse> inspeccionResponseCall = apiService.detalleInspeccion(hashMap);

        inspeccionResponseCall.enqueue(new Callback<InspeccionDetalleResponse>() {
            @Override
            public void onResponse(Call<InspeccionDetalleResponse> call, Response<InspeccionDetalleResponse> response) {
                inspeccionDetalleMutableLiveData.setValue(response.body());
                Log.e(LOGGER, "" + response.code());
            }

            @Override
            public void onFailure(Call<InspeccionDetalleResponse> call, Throwable t) {
                Log.e(LOGGER, "onFailure" + t.getMessage());
                inspeccionDetalleMutableLiveData.setValue(null);
            }
        });
        return inspeccionDetalleMutableLiveData;
    }

    public MutableLiveData<List<ProyectoEntity>> obtenerProyectos(HashMap hashMap, ProyectoRepository proyectoRepository) {
        final MutableLiveData<List<ProyectoEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<ProyectoEntity>> listCall = apiService.obtenerProyectosRolUsuario();

        listCall.enqueue(new Callback<List<ProyectoEntity>>() {
            @Override
            public void onResponse(Call<List<ProyectoEntity>> call, Response<List<ProyectoEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    proyectoRepository.deleteAllProject();
                    proyectoRepository.insert(new ProyectoEntity(0, "Seleccionar"));
                    for (int i = 0; i < response.body().size(); i++) {
                        proyectoRepository.insert(response.body().get(i));
                    }
                    mutableLiveData.setValue(proyectoRepository.findAllProject(hashMap));
                }
            }

            @Override
            public void onFailure(Call<List<ProyectoEntity>> call, Throwable t) {
                mutableLiveData.setValue(proyectoRepository.findAllProject(hashMap));
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<List<ContratistaEntity>> obtenerContratista(HashMap hashMap, Context context) {
        final MutableLiveData<List<ContratistaEntity>> contratistaMutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<ContratistaEntity>> listCall = apiService.obtenerContratisa(hashMap);

        listCall.enqueue(new Callback<List<ContratistaEntity>>() {
            @Override
            public void onResponse(Call<List<ContratistaEntity>> call, Response<List<ContratistaEntity>> response) {
                contratistaMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<ContratistaEntity>> call, Throwable t) {
                Log.e("ERRRO:", "" + t.getMessage());
                contratistaMutableLiveData.setValue(null);
            }
        });
        return contratistaMutableLiveData;
    }

    public MutableLiveData<List<TipoUbicacionEntity>> tipoUbicacion(HashMap hashMap, Context context) {
        final MutableLiveData<List<TipoUbicacionEntity>> tipoUbicacionMutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<TipoUbicacionEntity>> listCall = apiService.tipoUbicacion(hashMap);

        listCall.enqueue(new Callback<List<TipoUbicacionEntity>>() {
            @Override
            public void onResponse(Call<List<TipoUbicacionEntity>> call, Response<List<TipoUbicacionEntity>> response) {
                tipoUbicacionMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<TipoUbicacionEntity>> call, Throwable t) {
                Log.e("ERRRO:", "" + t.getMessage());
                tipoUbicacionMutableLiveData.setValue(null);
            }
        });
        return tipoUbicacionMutableLiveData;
    }


    public MutableLiveData<List<LugarEntity>> obtenerLugar(HashMap hashMap, Context context) {
        final MutableLiveData<List<LugarEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<LugarEntity>> listCall = apiService.obtenerLugar(hashMap);

        listCall.enqueue(new Callback<List<LugarEntity>>() {
            @Override
            public void onResponse(Call<List<LugarEntity>> call, Response<List<LugarEntity>> response) {
                mutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<LugarEntity>> call, Throwable t) {
                Log.e("ERRRO:", "" + t.getMessage());
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }


    public MutableLiveData<List<CategoriaBuenaPracticaEntity>> categoriaBuenaPractica(CategoriaBuenaPracticaRepository categoriaBuenaPracticaRepository) {
        final MutableLiveData<List<CategoriaBuenaPracticaEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<CategoriaBuenaPracticaEntity>> listCall = apiService.categoriaBuenaPractica();

        listCall.enqueue(new Callback<List<CategoriaBuenaPracticaEntity>>() {
            @Override
            public void onResponse(Call<List<CategoriaBuenaPracticaEntity>> call, Response<List<CategoriaBuenaPracticaEntity>> response) {

                if (response.body() != null && response.body().size() > 0) {
                    categoriaBuenaPracticaRepository.deleteAll();
                    for (int i = 0; i < response.body().size(); i++) {
                        categoriaBuenaPracticaRepository.insert(response.body().get(i));
                    }
                    mutableLiveData.setValue(categoriaBuenaPracticaRepository.findCategoriaBuenaPractica());
                }
            }

            @Override
            public void onFailure(Call<List<CategoriaBuenaPracticaEntity>> call, Throwable t) {
                mutableLiveData.setValue(categoriaBuenaPracticaRepository.findCategoriaBuenaPractica());
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<EmpresaMaestraEntity> empresaProyecto(HashMap hashMap, Context context) {
        final MutableLiveData<EmpresaMaestraEntity> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<EmpresaMaestraEntity> empresaProyecto = apiService.empresaProyecto(hashMap);

        empresaProyecto.enqueue(new Callback<EmpresaMaestraEntity>() {
            @Override
            public void onResponse(Call<EmpresaMaestraEntity> call, Response<EmpresaMaestraEntity> response) {
                mutableLiveData.setValue(response.body());
                Log.e(LOGGER, "" + response.code());
            }

            @Override
            public void onFailure(Call<EmpresaMaestraEntity> call, Throwable t) {
                Log.e(LOGGER, "onFailure" + t.getMessage());
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }


    public MutableLiveData<CreateInspeccionResponse> insertarInspeccion(HashMap hashMap) {
        final MutableLiveData<CreateInspeccionResponse> insertarInspeccionMutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<CreateInspeccionResponse> empresaProyecto = apiService.insertarInspeccion(hashMap);

        empresaProyecto.enqueue(new Callback<CreateInspeccionResponse>() {
            @Override
            public void onResponse(Call<CreateInspeccionResponse> call, Response<CreateInspeccionResponse> response) {
                insertarInspeccionMutableLiveData.setValue(response.body());
                Log.e(LOGGER, "" + response.code());
            }

            @Override
            public void onFailure(Call<CreateInspeccionResponse> call, Throwable t) {
                Log.e(LOGGER, "onFailure : " + t.getMessage());
                insertarInspeccionMutableLiveData.setValue(null);
            }
        });
        return insertarInspeccionMutableLiveData;
    }

    public MutableLiveData<Boolean> eliminarInspeccion(HashMap hashMap) {
        final MutableLiveData<Boolean> eliminarInspeccionMutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<Boolean> empresaProyecto = apiService.eliminarInspeccion(hashMap);

        empresaProyecto.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                eliminarInspeccionMutableLiveData.setValue(response.body());
                Log.e(LOGGER, "" + response.code());
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.e(LOGGER, "onFailure" + t.getMessage());
                eliminarInspeccionMutableLiveData.setValue(null);
            }
        });
        return eliminarInspeccionMutableLiveData;
    }

    public MutableLiveData<String> insertarBuenaPractica(List<Map<String, Object>> hashMap) {
        final MutableLiveData<String> insertarBuenaPracticaMutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<String> empresaProyecto = apiService.insertarBuenaPractica(hashMap);

        empresaProyecto.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.e(LOGGER, "insertarBuenaPractica  body :" + response.body());
                Log.e(LOGGER, "insertarBuenaPractica :" + response.code());
                insertarBuenaPracticaMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e(LOGGER, "onFailure" + t.getMessage());
                insertarBuenaPracticaMutableLiveData.setValue(null);
            }
        });
        return insertarBuenaPracticaMutableLiveData;
    }

    public MutableLiveData<List<UsuarioResponsableEntity>> obtenerUsuarios(HashMap hashMap, UsuarioResponsableRepository usuarioResponsableRepository) {
        final MutableLiveData<List<UsuarioResponsableEntity>> obtenerLugarMutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<UsuarioResponsableEntity>> listCall = apiService.obtenerUsuarios(hashMap);

        listCall.enqueue(new Callback<List<UsuarioResponsableEntity>>() {
            @Override
            public void onResponse(Call<List<UsuarioResponsableEntity>> call, Response<List<UsuarioResponsableEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    usuarioResponsableRepository.deleteAll();
                    for (int i = 0; i < response.body().size(); i++) {
                        usuarioResponsableRepository.insert(response.body().get(i));
                    }
                }
                obtenerLugarMutableLiveData.setValue(usuarioResponsableRepository.findUsuarioResponsables());
            }

            @Override
            public void onFailure(Call<List<UsuarioResponsableEntity>> call, Throwable t) {
                Log.e("ERRRO:", "" + t.getMessage());
                obtenerLugarMutableLiveData.setValue(usuarioResponsableRepository.findUsuarioResponsables());
            }
        });
        return obtenerLugarMutableLiveData;
    }

  /*   InspeccionRepository.getInstance().obtenerUsuarios(hashMap, usuarioResponsableRepository).observe((LifecycleOwner) activity, new Observer<List<UsuarioResponsableEntity>>() {
        @Override
        public void onChanged(List<UsuarioResponsableEntity> usuarioResponsableEntities) {
            if (usuarioResponsableEntities != null && usuarioResponsableEntities.size() > 0) {
                usuarioResponsableRepository.deleteAll();
                for (int i = 0; i < usuarioResponsableEntities.size(); i++) {
                    usuarioResponsableRepository.insert(usuarioResponsableEntities.get(i));
                }
            }
        }
    });*/

    public MutableLiveData<List<InspeccionEmpresaEntity>> obtenerEmpresa(IncidenteEmpresaRepository incidenteEmpresaRepository) {
        final MutableLiveData<List<InspeccionEmpresaEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<InspeccionEmpresaEntity>> listCall = apiService.obtenerEmpresa();

        listCall.enqueue(new Callback<List<InspeccionEmpresaEntity>>() {
            @Override
            public void onResponse(Call<List<InspeccionEmpresaEntity>> call, Response<List<InspeccionEmpresaEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    incidenteEmpresaRepository.deleteAll();
                    for (int i = 0; i < response.body().size(); i++) {
                        incidenteEmpresaRepository.insert(response.body().get(i));
                    }
                }
                mutableLiveData.setValue(incidenteEmpresaRepository.findAll());
            }

            @Override
            public void onFailure(Call<List<InspeccionEmpresaEntity>> call, Throwable t) {
                mutableLiveData.setValue(incidenteEmpresaRepository.findAll());
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<List<ActividadEntity>> obtenerActividad(ActividadRepository actividadRepository) {
        final MutableLiveData<List<ActividadEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<ActividadEntity>> listCall = apiService.obtenerActividad();

        listCall.enqueue(new Callback<List<ActividadEntity>>() {
            @Override
            public void onResponse(Call<List<ActividadEntity>> call, Response<List<ActividadEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    actividadRepository.deleteAll();
                    actividadRepository.insert(new ActividadEntity(0, "Seleccionar"));
                    for (int i = 0; i < response.body().size(); i++) {
                        actividadRepository.insert(response.body().get(i));
                    }
                    mutableLiveData.setValue(actividadRepository.findAll());
                }
            }

            @Override
            public void onFailure(Call<List<ActividadEntity>> call, Throwable t) {
                mutableLiveData.setValue(actividadRepository.findAll());
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<List<ComboMaestroEntity>> obtenerComboMaestro(String[] array, ComboMaestroRepository comboMaestroRepository) {
        final MutableLiveData<List<ComboMaestroEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<ComboMaestroEntity>> listCall = apiService.obtenerComboMaestro(array);

        listCall.enqueue(new Callback<List<ComboMaestroEntity>>() {
            @Override
            public void onResponse(Call<List<ComboMaestroEntity>> call, Response<List<ComboMaestroEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    comboMaestroRepository.deleteAll();
                    comboMaestroRepository.insert(new ComboMaestroEntity(0, "Seleccionar"));
                    for (int i = 0; i < response.body().size(); i++) {
                        comboMaestroRepository.insert(response.body().get(i));
                    }
                    mutableLiveData.setValue(comboMaestroRepository.findAll());
                }
            }

            @Override
            public void onFailure(Call<List<ComboMaestroEntity>> call, Throwable t) {
                mutableLiveData.setValue(comboMaestroRepository.findAll());
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<List<TipoEventoEntity>> tipoEvento(String[] array, TipoEventoRepository tipoEventoRepository) {
        final MutableLiveData<List<TipoEventoEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<TipoEventoEntity>> listCall = apiService.tipoEvento(array);

        listCall.enqueue(new Callback<List<TipoEventoEntity>>() {
            @Override
            public void onResponse(Call<List<TipoEventoEntity>> call, Response<List<TipoEventoEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    tipoEventoRepository.deleteAll();
                    tipoEventoRepository.insert(new TipoEventoEntity(0, "Seleccionar"));
                    for (int i = 0; i < response.body().size(); i++) {
                        tipoEventoRepository.insert(response.body().get(i));
                    }
                    mutableLiveData.setValue(tipoEventoRepository.findAll());
                }
            }

            @Override
            public void onFailure(Call<List<TipoEventoEntity>> call, Throwable t) {
                mutableLiveData.setValue(tipoEventoRepository.findAll());
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<List<AreaTrabajoEntity>> obtenerAreaInspeccion(AreaTrabajoRepository areaTrabajoRepository) {
        final MutableLiveData<List<AreaTrabajoEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<AreaTrabajoEntity>> listCall = apiService.obtenerAreaInspeccion();

        listCall.enqueue(new Callback<List<AreaTrabajoEntity>>() {
            @Override
            public void onResponse(Call<List<AreaTrabajoEntity>> call, Response<List<AreaTrabajoEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    areaTrabajoRepository.deleteAll();
                    areaTrabajoRepository.insert(new AreaTrabajoEntity(0, "Seleccionar", true));
                    for (int i = 0; i < response.body().size(); i++) {
                        areaTrabajoRepository.insert(response.body().get(i));
                    }
                    mutableLiveData.setValue(areaTrabajoRepository.findAll());
                }
            }

            @Override
            public void onFailure(Call<List<AreaTrabajoEntity>> call, Throwable t) {
                Log.e("ERRRO:", "" + t.getMessage());
                mutableLiveData.setValue(areaTrabajoRepository.findAll());
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<List<TorreLugarResponse>> obtenerTorresPorLugar(HashMap hashMap, Context context) {
        final MutableLiveData<List<TorreLugarResponse>> proyectoMutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<TorreLugarResponse>> listCall = apiService.obtenerTorresPorLugar(hashMap);

        listCall.enqueue(new Callback<List<TorreLugarResponse>>() {
            @Override
            public void onResponse(Call<List<TorreLugarResponse>> call, Response<List<TorreLugarResponse>> response) {
                proyectoMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<TorreLugarResponse>> call, Throwable t) {
                Log.e("ERRRO:", "" + t.getMessage());
                proyectoMutableLiveData.setValue(null);
            }
        });
        return proyectoMutableLiveData;
    }

    public MutableLiveData<String> sendNotification(HashMap hashMap) {
        final MutableLiveData<String> obtenerLugarMutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<String> listCall = apiService.sendNotification(hashMap);

        listCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                obtenerLugarMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e("ERRRO:", "" + t.getMessage());
                obtenerLugarMutableLiveData.setValue(null);
            }
        });
        return obtenerLugarMutableLiveData;
    }

    //TODO OFFLINE
    public MutableLiveData<List<ProyectoEntity>> obtenerProyectoOffline(HashMap hashMap, ProyectoRepository proyectoRepository) {
        final MutableLiveData<List<ProyectoEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<ProyectoEntity>> listCall = apiService.obtenerProyecto(hashMap);

        listCall.enqueue(new Callback<List<ProyectoEntity>>() {
            @Override
            public void onResponse(Call<List<ProyectoEntity>> call, Response<List<ProyectoEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    proyectoRepository.deleteAllProject();
                    for (int i = 0; i < response.body().size(); i++) {
                        proyectoRepository.insert(response.body().get(i));
                    }
                    //  mutableLiveData.setValue(proyectoRepository.findAllProject(hashMap));
                }
            }

            @Override
            public void onFailure(Call<List<ProyectoEntity>> call, Throwable t) {
                // mutableLiveData.setValue(proyectoRepository.findAllProject(hashMap));
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<List<ContratistaEntity>> obtenerContratistaOffline(ContratistaRepository contratistaRepository) {
        final MutableLiveData<List<ContratistaEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<ContratistaEntity>> listCall = apiService.obtenerContratistaOffline();

        listCall.enqueue(new Callback<List<ContratistaEntity>>() {
            @Override
            public void onResponse(Call<List<ContratistaEntity>> call, Response<List<ContratistaEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    contratistaRepository.deleteAll();
                    contratistaRepository.insert(new ContratistaEntity(0, "Seleccionar"));
                    for (int i = 0; i < response.body().size(); i++) {
                        contratistaRepository.insert(response.body().get(i));
                    }
                }
                mutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<ContratistaEntity>> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<List<TipoUbicacionEntity>> obtenerTipoUbicacionOffline(TipoUbicacionRepository tipoUbicacionRepository) {
        final MutableLiveData<List<TipoUbicacionEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<TipoUbicacionEntity>> listCall = apiService.obtenerTipoUbicacionOffline();
        listCall.enqueue(new Callback<List<TipoUbicacionEntity>>() {
            @Override
            public void onResponse(Call<List<TipoUbicacionEntity>> call, Response<List<TipoUbicacionEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    tipoUbicacionRepository.deleteAll();
                    for (int i = 0; i < response.body().size(); i++) {
                        tipoUbicacionRepository.insert(response.body().get(i));
                    }
                }
            }

            @Override
            public void onFailure(Call<List<TipoUbicacionEntity>> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<List<LugarEntity>> obtenerLugarOffline(LugarRepository lugarRepository) {
        final MutableLiveData<List<LugarEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<LugarEntity>> listCall = apiService.obtenerLugarOffline();
        listCall.enqueue(new Callback<List<LugarEntity>>() {
            @Override
            public void onResponse(Call<List<LugarEntity>> call, Response<List<LugarEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    lugarRepository.deleteAll();
                    for (int i = 0; i < response.body().size(); i++) {
                        lugarRepository.insert(response.body().get(i));
                    }
                }
            }

            @Override
            public void onFailure(Call<List<LugarEntity>> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<List<EmpresaMaestraEntity>> obtenerEmpresaMaestraOffline(EmpresaMaestraRepository empresaMaestraRepository) {
        final MutableLiveData<List<EmpresaMaestraEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<EmpresaMaestraEntity>> listCall = apiService.obtenerEmpresaMaestraOffline();
        listCall.enqueue(new Callback<List<EmpresaMaestraEntity>>() {
            @Override
            public void onResponse(Call<List<EmpresaMaestraEntity>> call, Response<List<EmpresaMaestraEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    empresaMaestraRepository.deleteAll();
                    for (int i = 0; i < response.body().size(); i++) {
                        empresaMaestraRepository.insert(response.body().get(i));
                    }
                }
            }

            @Override
            public void onFailure(Call<List<EmpresaMaestraEntity>> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }


    public MutableLiveData<List<AsynInspeccionResponse>> listarInspeccionDetalles(HashMap hashMap) {
        final MutableLiveData<List<AsynInspeccionResponse>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<AsynInspeccionResponse>> listCall = apiService.listarInspeccionDetalles(hashMap);
        listCall.enqueue(new Callback<List<AsynInspeccionResponse>>() {
            @Override
            public void onResponse(Call<List<AsynInspeccionResponse>> call, Response<List<AsynInspeccionResponse>> response) {
                mutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<AsynInspeccionResponse>> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }


}
