package com.mdp.fusionapp.network.reposiroty;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.mdp.fusionapp.database.repository.UsuarioNotificadorRepository;
import com.mdp.fusionapp.network.response.LoginResponse;
import com.mdp.fusionapp.network.response.ProyectoResponse;
import com.mdp.fusionapp.database.entity.UsuarioNotificarEntity;
import com.mdp.fusionapp.network.service.APIService;
import com.mdp.fusionapp.utilitary.UtilMDP;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginRespository {

    private final String LOGGER = "LoginRespository";
    private APIService apiService;

    private static LoginRespository instancia;

    public LoginRespository() {
    }

    public static LoginRespository getInstance() {
        if (instancia == null) {
            instancia = new LoginRespository();
        }
        return instancia;
    }


    public MutableLiveData<LoginResponse> loginAcceso(HashMap hashMap, Context context) {
        final MutableLiveData<LoginResponse> loginResponseMutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<LoginResponse> loginResponseCall = apiService.loginAcceso(hashMap);

        loginResponseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                loginResponseMutableLiveData.setValue(response.body());
                Log.e(LOGGER, "onResponse " + response.body());
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e(LOGGER, "onFailure" + t.getMessage());
                loginResponseMutableLiveData.setValue(null);
            }
        });
        return loginResponseMutableLiveData;
    }

    public MutableLiveData<List<UsuarioNotificarEntity>> obtenerUsuarioNotificar(UsuarioNotificadorRepository usuarioNotificadorRepository) {
        final MutableLiveData<List<UsuarioNotificarEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<UsuarioNotificarEntity>> loginResponseCall = apiService.obtenerUsuarioNotificar();

        loginResponseCall.enqueue(new Callback<List<UsuarioNotificarEntity>>() {
            @Override
            public void onResponse(Call<List<UsuarioNotificarEntity>> call, Response<List<UsuarioNotificarEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    usuarioNotificadorRepository.deleteAll();
                    for (int i = 0; i < response.body().size(); i++) {
                        usuarioNotificadorRepository.insert(response.body().get(i));
                    }
                    mutableLiveData.setValue(usuarioNotificadorRepository.findAll());
                }
            }

            @Override
            public void onFailure(Call<List<UsuarioNotificarEntity>> call, Throwable t) {
                mutableLiveData.setValue(usuarioNotificadorRepository.findAll());
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<ProyectoResponse> obtenerProyecto(HashMap hashMap, Context context) {
        final MutableLiveData<ProyectoResponse> loginResponseMutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<ProyectoResponse> proyectoResponseCall = apiService.obtenerProyecto(hashMap);

        proyectoResponseCall.enqueue(new Callback<ProyectoResponse>() {
            @Override
            public void onResponse(Call<ProyectoResponse> call, Response<ProyectoResponse> response) {
                loginResponseMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<ProyectoResponse> call, Throwable t) {
                loginResponseMutableLiveData.setValue(null);
            }
        });
        return loginResponseMutableLiveData;
    }

}
