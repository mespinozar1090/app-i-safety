package com.mdp.fusionapp.network.response;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "actividad_table")
public class ActividadEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("Id_Actividad_Economica")
    private Integer idActividadEconomica;

    @SerializedName("Descripcion")
    private String descripcion;

    @SerializedName("Estado_ActividadEconomica")
    private Boolean estadoActividadEconomica;



    public ActividadEntity(Integer idActividadEconomica, String descripcion) {
        this.idActividadEconomica = idActividadEconomica;
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdActividadEconomica() {
        return idActividadEconomica;
    }

    public void setIdActividadEconomica(Integer idActividadEconomica) {
        this.idActividadEconomica = idActividadEconomica;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getEstadoActividadEconomica() {
        return estadoActividadEconomica;
    }

    public void setEstadoActividadEconomica(Boolean estadoActividadEconomica) {
        this.estadoActividadEconomica = estadoActividadEconomica;
    }

    @Override
    public String toString() {
        return descripcion;
    }
}
