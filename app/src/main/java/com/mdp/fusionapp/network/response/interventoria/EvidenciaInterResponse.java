package com.mdp.fusionapp.network.response.interventoria;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class EvidenciaInterResponse implements Serializable {

    @SerializedName("Estatus")
    private Boolean estatus;

    @SerializedName("Data")
    private List<Data> data;

    public Boolean getEstatus() {
        return estatus;
    }

    public void setEstatus(Boolean estatus) {
        this.estatus = estatus;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("Id_Interventoria_Imagen")
        private Integer idInterventoria;

        @SerializedName("urlImagen")
        private String imgBase64;

        @SerializedName("Descripcion")
        private String descripcion;

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        public Integer getIdInterventoria() {
            return idInterventoria;
        }

        public void setIdInterventoria(Integer idInterventoria) {
            this.idInterventoria = idInterventoria;
        }

        public String getImgBase64() {
            return imgBase64;
        }

        public void setImgBase64(String imgBase64) {
            this.imgBase64 = imgBase64;
        }
    }

}
