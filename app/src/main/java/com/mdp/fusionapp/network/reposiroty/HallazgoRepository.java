package com.mdp.fusionapp.network.reposiroty;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import com.mdp.fusionapp.network.service.APIService;
import com.mdp.fusionapp.utilitary.UtilMDP;
import java.util.HashMap;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HallazgoRepository {

    private final String LOGGER = "AuditoriaRepository";
    private APIService apiService;

    private static HallazgoRepository instancia;

    public HallazgoRepository() {
    }

    public static HallazgoRepository getInstance() {
        if (instancia == null) {
            instancia = new HallazgoRepository();
        }
        return instancia;
    }

    public MutableLiveData<Boolean> insertarHallazgo(HashMap hashMap){
        final MutableLiveData<Boolean> insertarHallazgoMutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<Boolean> empresaProyecto = apiService.insertarHallazgo(hashMap);

        empresaProyecto.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                Log.e(LOGGER,"BODY :"+response.body());
                Log.e(LOGGER,"CODE "+response.code());
                insertarHallazgoMutableLiveData.setValue(response.body());

            }
            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.e(LOGGER, "onFailure"+t.getMessage());
                insertarHallazgoMutableLiveData.setValue(null);
            }
        });
        return  insertarHallazgoMutableLiveData;
    }
}
