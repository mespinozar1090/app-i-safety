package com.mdp.fusionapp.network.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginResponse extends Respuesta implements Serializable {

    @SerializedName("Data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data{

        @SerializedName("IdUsers")
        private Integer idUsers;

        @SerializedName("Usuario_Login")
        private String usuario_Login;

        @SerializedName("Contraseña_Login")
        private String contraseña_Login;

        @SerializedName("IdPerfil_Usuario")
        private Integer idPerfilUsuario;

        @SerializedName("Nombre_Usuario")
        private String nombreUsuario;

        @SerializedName("Razon_Social")
        private Integer idRazonSocial;

        @SerializedName("DescripcionEmpresa")
        private String descripcionEmpresa;

        @SerializedName("Email_Corporativo")
        private String emailCorporativo;

        @SerializedName("RUC")
        private String Ruc;

        @SerializedName("Id_Empresa")
        private Integer idEmpresa;

        @SerializedName("Descripcion_Rol")
        private String descripcionRol;

        @SerializedName("Descripcion_Rol_Usuario")
        private String descripcionRolUsuario;

        @SerializedName("Id_Rol_Usuario")
        private Integer idRolUsuario;

        @SerializedName("Id_Rol_General")
        private Integer idRolGeneral;

        public String getDescripcionRolUsuario() {
            return descripcionRolUsuario;
        }

        public void setDescripcionRolUsuario(String descripcionRolUsuario) {
            this.descripcionRolUsuario = descripcionRolUsuario;
        }

        public Integer getIdUsers() {
            return idUsers;
        }

        public void setIdUsers(Integer idUsers) {
            this.idUsers = idUsers;
        }

        public String getUsuario_Login() {
            return usuario_Login;
        }

        public void setUsuario_Login(String usuario_Login) {
            this.usuario_Login = usuario_Login;
        }

        public String getContraseña_Login() {
            return contraseña_Login;
        }

        public void setContraseña_Login(String contraseña_Login) {
            this.contraseña_Login = contraseña_Login;
        }

        public Integer getIdPerfilUsuario() {
            return idPerfilUsuario;
        }

        public void setIdPerfilUsuario(Integer idPerfilUsuario) {
            this.idPerfilUsuario = idPerfilUsuario;
        }

        public String getNombreUsuario() {
            return nombreUsuario;
        }

        public void setNombreUsuario(String nombreUsuario) {
            this.nombreUsuario = nombreUsuario;
        }

        public Integer getIdRazonSocial() {
            return idRazonSocial;
        }

        public void setIdRazonSocial(Integer idRazonSocial) {
            this.idRazonSocial = idRazonSocial;
        }

        public String getDescripcionEmpresa() {
            return descripcionEmpresa;
        }

        public void setDescripcionEmpresa(String descripcionEmpresa) {
            this.descripcionEmpresa = descripcionEmpresa;
        }

        public String getEmailCorporativo() {
            return emailCorporativo;
        }

        public void setEmailCorporativo(String emailCorporativo) {
            this.emailCorporativo = emailCorporativo;
        }

        public String getRuc() {
            return Ruc;
        }

        public void setRuc(String ruc) {
            Ruc = ruc;
        }


        public String getDescripcion_Rol() {
            return descripcionRol;
        }

        public void setDescripcion_Rol(String descripcion_Rol) {
            descripcionRol = descripcion_Rol;
        }

        public String getDescripcionRol() {
            return descripcionRol;
        }

        public void setDescripcionRol(String descripcionRol) {
            this.descripcionRol = descripcionRol;
        }

        public Integer getIdRolUsuario() {
            return idRolUsuario;
        }

        public void setIdRolUsuario(Integer idRolUsuario) {
            this.idRolUsuario = idRolUsuario;
        }

        public Integer getIdRolGeneral() {
            return idRolGeneral;
        }

        public void setIdRolGeneral(Integer idRolGeneral) {
            this.idRolGeneral = idRolGeneral;
        }

        public Integer getIdEmpresa() {
            return idEmpresa;
        }

        public void setIdEmpresa(Integer idEmpresa) {
            this.idEmpresa = idEmpresa;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "idUsers=" + idUsers +
                    ", usuario_Login='" + usuario_Login + '\'' +
                    ", contraseña_Login='" + contraseña_Login + '\'' +
                    ", idPerfilUsuario=" + idPerfilUsuario +
                    ", nombreUsuario='" + nombreUsuario + '\'' +
                    ", idRazonSocial=" + idRazonSocial +
                    ", descripcionEmpresa='" + descripcionEmpresa + '\'' +
                    ", emailCorporativo='" + emailCorporativo + '\'' +
                    ", Ruc='" + Ruc + '\'' +
                    ", idEmpresa='" + idEmpresa + '\'' +
                    '}';
        }
    }

}
