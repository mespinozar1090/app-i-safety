package com.mdp.fusionapp.network.response;

import com.google.gson.annotations.SerializedName;

public class TipoUbicacionResponse {

    @SerializedName("idTipoUbicacion")
    private Integer idTipoUbicacion;

    @SerializedName("Descripcion")
    private String descripcion;

    public TipoUbicacionResponse() {
    }

    public TipoUbicacionResponse(Integer idTipoUbicacion, String descripcion) {
        this.idTipoUbicacion = idTipoUbicacion;
        this.descripcion = descripcion;
    }

    public Integer getIdTipoUbicacion() {
        return idTipoUbicacion;
    }

    public void setIdTipoUbicacion(Integer idTipoUbicacion) {
        this.idTipoUbicacion = idTipoUbicacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return descripcion;
    }
}
