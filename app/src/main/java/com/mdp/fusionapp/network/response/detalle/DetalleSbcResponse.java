package com.mdp.fusionapp.network.response.detalle;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class DetalleSbcResponse implements Serializable {

    private Integer idRow;

    @SerializedName("Id_sbc")
    private Integer idsbc;

    @SerializedName("Nombre_observador")
    private String nombreobservador;

    @SerializedName("IdLugar_trabajo")
    private Integer idLugartrabajo;

    @SerializedName("Fecha_registro")
    private String fecharegistro;

    @SerializedName("Id_Empresa_Observadora")
    private Integer idEmpresaObservadora;

    @SerializedName("Horario_Observacion")
    private String horarioObservacion;

    @SerializedName("Tiempo_exp_observada")
    private Integer tiempoexpobservada;

    @SerializedName("Especialidad_Observado")
    private Integer especialidadObservado;

    @SerializedName("Actividad_Observada")
    private String actividadObservada;

    @SerializedName("Id_Area_Trabajo")
    private Integer idAreaTrabajo;

    @SerializedName("Estado_SBC")
    private Integer estadoSBC;

    @SerializedName("Descripcion_Area_Observada")
    private String descripcionAreaObservada;

    @SerializedName("Fecha_Registro_SBC")
    private String fechaRegistroSBC;

    @SerializedName("IdUsuario")
    private Integer idUsuario;

    @SerializedName("Tipo_SBC")
    private Integer tipoSBC;

    @SerializedName("IdSede_Proyecto")
    private Integer idSedeProyecto;

    @SerializedName("IdCargo")
    private Integer idCargo;

    @SerializedName("IdSede_Proyecto_Observado")
    private Integer idSedeProyectoObservado;

    @SerializedName("Cargo_Observador")
    private String cargoObservador;

    @SerializedName("Lugar_trabajo")
    private String lugarTrabajo;

    private List<Categoria> lstDetalleSBC;

    public String getLugarTrabajo() {
        return lugarTrabajo;
    }

    public void setLugarTrabajo(String lugarTrabajo) {
        this.lugarTrabajo = lugarTrabajo;
    }


    public Integer getIdRow() {
        return idRow;
    }

    public void setIdRow(Integer idRow) {
        this.idRow = idRow;
    }

    public List<Categoria> getLstDetalleSBC() {
        return lstDetalleSBC;
    }


    public void setLstDetalleSBC(List<Categoria> lstDetalleSBC) {
        this.lstDetalleSBC = lstDetalleSBC;
    }

    public Integer getIdSedeProyecto() {
        return idSedeProyecto;
    }

    public void setIdSedeProyecto(Integer idSedeProyecto) {
        this.idSedeProyecto = idSedeProyecto;
    }

    public Integer getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Integer idCargo) {
        this.idCargo = idCargo;
    }

    public Integer getIdSedeProyectoObservado() {
        return idSedeProyectoObservado;
    }

    public void setIdSedeProyectoObservado(Integer idSedeProyectoObservado) {
        this.idSedeProyectoObservado = idSedeProyectoObservado;
    }

    public String getCargoObservador() {
        return cargoObservador;
    }

    public void setCargoObservador(String cargoObservador) {
        this.cargoObservador = cargoObservador;
    }

    public Integer getIdsbc() {
        return idsbc;
    }

    public void setIdsbc(Integer idsbc) {
        this.idsbc = idsbc;
    }

    public String getNombreobservador() {
        return nombreobservador;
    }

    public void setNombreobservador(String nombreobservador) {
        this.nombreobservador = nombreobservador;
    }

    public Integer getIdLugartrabajo() {
        return idLugartrabajo;
    }

    public void setIdLugartrabajo(Integer idLugartrabajo) {
        this.idLugartrabajo = idLugartrabajo;
    }

    public String getFecharegistro() {
        return fecharegistro;
    }

    public void setFecharegistro(String fecharegistro) {
        this.fecharegistro = fecharegistro;
    }

    public Integer getIdEmpresaObservadora() {
        return idEmpresaObservadora;
    }

    public void setIdEmpresaObservadora(Integer idEmpresaObservadora) {
        this.idEmpresaObservadora = idEmpresaObservadora;
    }

    public String getHorarioObservacion() {
        return horarioObservacion;
    }

    public void setHorarioObservacion(String horarioObservacion) {
        this.horarioObservacion = horarioObservacion;
    }

    public Integer getTiempoexpobservada() {
        return tiempoexpobservada;
    }

    public void setTiempoexpobservada(Integer tiempoexpobservada) {
        this.tiempoexpobservada = tiempoexpobservada;
    }

    public Integer getEspecialidadObservado() {
        return especialidadObservado;
    }

    public void setEspecialidadObservado(Integer especialidadObservado) {
        this.especialidadObservado = especialidadObservado;
    }

    public String getActividadObservada() {
        return actividadObservada;
    }

    public void setActividadObservada(String actividadObservada) {
        this.actividadObservada = actividadObservada;
    }

    public Integer getIdAreaTrabajo() {
        return idAreaTrabajo;
    }

    public void setIdAreaTrabajo(Integer idAreaTrabajo) {
        this.idAreaTrabajo = idAreaTrabajo;
    }

    public Integer getEstadoSBC() {
        return estadoSBC;
    }

    public void setEstadoSBC(Integer estadoSBC) {
        this.estadoSBC = estadoSBC;
    }

    public String getDescripcionAreaObservada() {
        return descripcionAreaObservada;
    }

    public void setDescripcionAreaObservada(String descripcionAreaObservada) {
        this.descripcionAreaObservada = descripcionAreaObservada;
    }

    public String getFechaRegistroSBC() {
        return fechaRegistroSBC;
    }

    public void setFechaRegistroSBC(String fechaRegistroSBC) {
        this.fechaRegistroSBC = fechaRegistroSBC;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getTipoSBC() {
        return tipoSBC;
    }

    public void setTipoSBC(Integer tipoSBC) {
        this.tipoSBC = tipoSBC;
    }

    public static class Categoria implements Serializable {
        @SerializedName("Id_sbc_detalle")
        private Integer idSBCDetalle;

        @SerializedName("Id_sbc_Categoria")
        private Integer idSBCCategoria;

        @SerializedName("Id_sbc_Categoria_Items")
        private Integer idSBCCategoriaItems;

        @SerializedName("Seguro")
        private Boolean seguro;

        @SerializedName("Riesgoso")
        private Boolean riesgoso;

        @SerializedName("Idbarrera")
        private Integer idbarrera;

        @SerializedName("Observacion_sbc_detalle")
        private String observacionSBCDetalle;

        @SerializedName("Id_sbc")
        private Integer idsbc;

        @SerializedName("Descripcion_Item")
        private String descripcionItem;

        public String getDescripcionItem() {
            return descripcionItem;
        }

        public void setDescripcionItem(String descripcionItem) {
            this.descripcionItem = descripcionItem;
        }

        public Integer getIdSBCDetalle() {
            return idSBCDetalle;
        }

        public void setIdSBCDetalle(Integer idSBCDetalle) {
            this.idSBCDetalle = idSBCDetalle;
        }

        public Integer getIdSBCCategoria() {
            return idSBCCategoria;
        }

        public void setIdSBCCategoria(Integer idSBCCategoria) {
            this.idSBCCategoria = idSBCCategoria;
        }

        public Integer getIdSBCCategoriaItems() {
            return idSBCCategoriaItems;
        }

        public void setIdSBCCategoriaItems(Integer idSBCCategoriaItems) {
            this.idSBCCategoriaItems = idSBCCategoriaItems;
        }

        public Boolean getSeguro() {
            return seguro;
        }

        public void setSeguro(Boolean seguro) {
            this.seguro = seguro;
        }

        public Boolean getRiesgoso() {
            return riesgoso;
        }

        public void setRiesgoso(Boolean riesgoso) {
            this.riesgoso = riesgoso;
        }

        public Integer getIdbarrera() {
            return idbarrera;
        }

        public void setIdbarrera(Integer idbarrera) {
            this.idbarrera = idbarrera;
        }

        public String getObservacionSBCDetalle() {
            return observacionSBCDetalle;
        }

        public void setObservacionSBCDetalle(String observacionSBCDetalle) {
            this.observacionSBCDetalle = observacionSBCDetalle;
        }

        public Integer getIdsbc() {
            return idsbc;
        }

        public void setIdsbc(Integer idsbc) {
            this.idsbc = idsbc;
        }
    }
}
