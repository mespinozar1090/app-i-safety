package com.mdp.fusionapp.network.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UsuarioResponse  implements Serializable {

    @SerializedName("IdPerfil_Usuario")
    private Integer idPerfilUsuario;

    @SerializedName("Nombre_Usuario")
    private String nombreUsuario;

    @SerializedName("IdUsuario")
    private Integer idUsuario;

    @SerializedName("Apellido_Usuario")
    private String apellidoUsuario;

    @SerializedName("Email_Corporativo")
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getIdPerfilUsuario() {
        return idPerfilUsuario;
    }

    public void setIdPerfilUsuario(Integer idPerfilUsuario) {
        this.idPerfilUsuario = idPerfilUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getApellidoUsuario() {
        return apellidoUsuario;
    }

    public void setApellidoUsuario(String apellidoUsuario) {
        this.apellidoUsuario = apellidoUsuario;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public String toString() {
        return nombreUsuario+" "+apellidoUsuario;
    }
}
