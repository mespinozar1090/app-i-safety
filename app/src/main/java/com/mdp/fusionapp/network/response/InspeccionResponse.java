package com.mdp.fusionapp.network.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InspeccionResponse {
    @SerializedName("Id_Inspecciones")
    private Integer id;

    @SerializedName("Estado_Inspeccion")
    private Integer estado;

    @SerializedName("Fecha_Registro")
    private String fechaRegistro;

    @SerializedName("Nombre_Formato")
    private String nombreFormato;

    @SerializedName("NombreAutor")
    private String nombreUsuario;

    @SerializedName("CodigoInspeccion")
    private String codigo;

    @SerializedName("IdUsuario")
    private Integer idUsuario;

    @SerializedName("Id_Empresa_contratista")
    private Integer idEmpresaInspeccionada;

    @SerializedName("Id_Empresa_Inspector")
    private Integer idEmpresaInspector;

    @SerializedName("Id_UsuarioNotificado")
    private String idUsuarioNotificado;

    @SerializedName("Sede")
    private String sede;

    @SerializedName("EmpresaContratista")
    private String empresaContratista;

    @SerializedName("idProyecto")
    private Integer idProyecto;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getNombreFormato() {
        return nombreFormato;
    }

    public void setNombreFormato(String nombreFormato) {
        this.nombreFormato = nombreFormato;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdEmpresaInspeccionada() {
        return idEmpresaInspeccionada;
    }

    public void setIdEmpresaInspeccionada(Integer idEmpresaInspeccionada) {
        this.idEmpresaInspeccionada = idEmpresaInspeccionada;
    }

    public Integer getIdEmpresaInspector() {
        return idEmpresaInspector;
    }

    public void setIdEmpresaInspector(Integer idEmpresaInspector) {
        this.idEmpresaInspector = idEmpresaInspector;
    }

    public String getIdUsuarioNotificado() {
        return idUsuarioNotificado;
    }

    public void setIdUsuarioNotificado(String idUsuarioNotificado) {
        this.idUsuarioNotificado = idUsuarioNotificado;
    }

    public String getSede() {
        return sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }

    public String getEmpresaContratista() {
        return empresaContratista;
    }

    public void setEmpresaContratista(String empresaContratista) {
        this.empresaContratista = empresaContratista;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }
}
