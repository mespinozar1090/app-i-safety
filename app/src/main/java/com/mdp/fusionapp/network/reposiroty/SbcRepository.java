package com.mdp.fusionapp.network.reposiroty;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.mdp.fusionapp.database.repository.CategoriaSbcItemRepository;
import com.mdp.fusionapp.database.repository.CategoriaSbcRepository;
import com.mdp.fusionapp.database.repository.EmpresaSbcRepository;
import com.mdp.fusionapp.database.repository.EspecilidadRepository;
import com.mdp.fusionapp.database.repository.SedeProyectoRepository;
import com.mdp.fusionapp.network.response.AsynSbcResponse;
import com.mdp.fusionapp.database.entity.CategoriaSbcItemEntity;
import com.mdp.fusionapp.database.entity.CategoriaSbcEntity;
import com.mdp.fusionapp.database.entity.EmpresaSbcEntity;
import com.mdp.fusionapp.database.entity.EspecialidadEntity;
import com.mdp.fusionapp.network.response.Respuesta;
import com.mdp.fusionapp.network.response.SbcResponse;
import com.mdp.fusionapp.database.entity.SedeProyectoEntity;
import com.mdp.fusionapp.network.response.detalle.DetalleSbcResponse;
import com.mdp.fusionapp.network.service.APIService;
import com.mdp.fusionapp.utilitary.UtilMDP;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SbcRepository {
    private final String LOGGER = "SbsRepository";
    private APIService apiService;

    private static SbcRepository instancia;

    public SbcRepository() {
    }

    public static SbcRepository getInstance() {
        if (instancia == null) {
            instancia = new SbcRepository();
        }
        return instancia;
    }

    public MutableLiveData<List<SbcResponse>> listarSBC(HashMap hashMap, Context context) {
        final MutableLiveData<List<SbcResponse>> listarSBCMutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<SbcResponse>> listCall = apiService.listarSBC(hashMap);

        listCall.enqueue(new Callback<List<SbcResponse>>() {
            @Override
            public void onResponse(Call<List<SbcResponse>> call, Response<List<SbcResponse>> response) {
                listarSBCMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<SbcResponse>> call, Throwable t) {
                listarSBCMutableLiveData.setValue(null);
            }
        });
        return listarSBCMutableLiveData;
    }


    public MutableLiveData<List<EmpresaSbcEntity>> obtenerEmpresaSBSResponse(EmpresaSbcRepository empresaSbcRepository) {
        final MutableLiveData<List<EmpresaSbcEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<EmpresaSbcEntity>> listCall = apiService.obtenerEmpresaSBSResponse();

        listCall.enqueue(new Callback<List<EmpresaSbcEntity>>() {
            @Override
            public void onResponse(Call<List<EmpresaSbcEntity>> call, Response<List<EmpresaSbcEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    empresaSbcRepository.deleteAll();
                    empresaSbcRepository.insert(new EmpresaSbcEntity(0, "Seleccionar"));
                    for (int i = 0; i < response.body().size(); i++) {
                        empresaSbcRepository.insert(response.body().get(i));
                    }
                    mutableLiveData.setValue(empresaSbcRepository.findAll());
                }
            }

            @Override
            public void onFailure(Call<List<EmpresaSbcEntity>> call, Throwable t) {
                mutableLiveData.setValue(empresaSbcRepository.findAll());
            }
        });
        return mutableLiveData;
    }



    public MutableLiveData<List<SedeProyectoEntity>> obtenerSede(HashMap hashMap) {
        final MutableLiveData<List<SedeProyectoEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<SedeProyectoEntity>> listCall = apiService.obtenerSedeM(hashMap);

        listCall.enqueue(new Callback<List<SedeProyectoEntity>>() {
            @Override
            public void onResponse(Call<List<SedeProyectoEntity>> call, Response<List<SedeProyectoEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    mutableLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<SedeProyectoEntity>> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }


    public MutableLiveData<List<SedeProyectoEntity>> obtenerSedeM(HashMap hashMap ,SedeProyectoRepository sedeProyectoRepository) {
        final MutableLiveData<List<SedeProyectoEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<SedeProyectoEntity>> listCall = apiService.obtenerSedeM(hashMap);

        listCall.enqueue(new Callback<List<SedeProyectoEntity>>() {
            @Override
            public void onResponse(Call<List<SedeProyectoEntity>> call, Response<List<SedeProyectoEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    sedeProyectoRepository.deleteAll();
                    sedeProyectoRepository.insert(new SedeProyectoEntity(0, "Seleccionar", true));
                    for (int i = 0; i < response.body().size(); i++) {
                        sedeProyectoRepository.insert(response.body().get(i));
                    }
                    mutableLiveData.setValue(sedeProyectoRepository.findAll(hashMap));
                }
            }

            @Override
            public void onFailure(Call<List<SedeProyectoEntity>> call, Throwable t) {
                mutableLiveData.setValue(sedeProyectoRepository.findAll(hashMap));
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<List<EspecialidadEntity>> obtenerEspecialidad(EspecilidadRepository especialidaRepository) {
        final MutableLiveData<List<EspecialidadEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<EspecialidadEntity>> listCall = apiService.obtenerEspecialidad();

        listCall.enqueue(new Callback<List<EspecialidadEntity>>() {
            @Override
            public void onResponse(Call<List<EspecialidadEntity>> call, Response<List<EspecialidadEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    especialidaRepository.deleteAll();
                    especialidaRepository.insert(new EspecialidadEntity(0, "Seleccionar", true));
                    for (int i = 0; i < response.body().size(); i++) {
                        especialidaRepository.insert(response.body().get(i));
                    }
                    mutableLiveData.setValue(especialidaRepository.findAll());
                }
            }

            @Override
            public void onFailure(Call<List<EspecialidadEntity>> call, Throwable t) {
                mutableLiveData.setValue(especialidaRepository.findAll());
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<List<CategoriaSbcEntity>> obtenerCategoriaSBC(CategoriaSbcRepository categoriaSbcRepository) {
        final MutableLiveData<List<CategoriaSbcEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<CategoriaSbcEntity>> listCall = apiService.obtenerCategoriaSBC();

        listCall.enqueue(new Callback<List<CategoriaSbcEntity>>() {
            @Override
            public void onResponse(Call<List<CategoriaSbcEntity>> call, Response<List<CategoriaSbcEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    categoriaSbcRepository.deleteAll();
                    for (int i = 0; i < response.body().size(); i++) {
                        categoriaSbcRepository.insert(response.body().get(i));
                    }
                }
                mutableLiveData.setValue(categoriaSbcRepository.findAll());
            }

            @Override
            public void onFailure(Call<List<CategoriaSbcEntity>> call, Throwable t) {
                mutableLiveData.setValue(categoriaSbcRepository.findAll());
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<List<CategoriaSbcItemEntity>> obtenerCategoriaItemSBC(CategoriaSbcItemRepository categoriaSbcItemRepository) {
        final MutableLiveData<List<CategoriaSbcItemEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<CategoriaSbcItemEntity>> listCall = apiService.obtenerCategoriaItemSBC();

        listCall.enqueue(new Callback<List<CategoriaSbcItemEntity>>() {
            @Override
            public void onResponse(Call<List<CategoriaSbcItemEntity>> call, Response<List<CategoriaSbcItemEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    categoriaSbcItemRepository.deleteAll();
                    for (int i = 0; i < response.body().size(); i++) {
                        categoriaSbcItemRepository.insert(response.body().get(i));
                    }
                }
                mutableLiveData.setValue(categoriaSbcItemRepository.findAll());
            }

            @Override
            public void onFailure(Call<List<CategoriaSbcItemEntity>> call, Throwable t) {
                mutableLiveData.setValue(categoriaSbcItemRepository.findAll());
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<Respuesta> insertarSBC(HashMap hashMap, Context context) {
        final MutableLiveData<Respuesta> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<Respuesta> listCall = apiService.insertarSBC(hashMap);
        listCall.enqueue(new Callback<Respuesta>() {
            @Override
            public void onResponse(Call<Respuesta> call, Response<Respuesta> response) {

                mutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<Respuesta> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<Boolean> borrarSBC(HashMap hashMap) {
        final MutableLiveData<Boolean> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<Boolean> listCall = apiService.borrarSBC(hashMap);
        listCall.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                mutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<Respuesta> editarSBC(HashMap hashMap, Context context) {
        final MutableLiveData<Respuesta> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<Respuesta> listCall = apiService.editarSBC(hashMap);

        listCall.enqueue(new Callback<Respuesta>() {
            @Override
            public void onResponse(Call<Respuesta> call, Response<Respuesta> response) {
                mutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<Respuesta> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }


    public MutableLiveData<DetalleSbcResponse> obtenerRegistroSBC(Integer id, Context context) {
        final MutableLiveData<DetalleSbcResponse> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<DetalleSbcResponse> listCall = apiService.obtenerRegistroSBC(id);

        listCall.enqueue(new Callback<DetalleSbcResponse>() {
            @Override
            public void onResponse(Call<DetalleSbcResponse> call, Response<DetalleSbcResponse> response) {
                Log.e("body: ", "" + response.body());
                Log.e("message: ", "" + response.message().toUpperCase());
                mutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<DetalleSbcResponse> call, Throwable t) {
                Log.e("error", "" + t.getMessage());
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<List<AsynSbcResponse>> listarSbcDetalles(HashMap hashMap) {
        final MutableLiveData<List<AsynSbcResponse>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<AsynSbcResponse>> listCall = apiService.listarSbcDetalles(hashMap);
        listCall.enqueue(new Callback<List<AsynSbcResponse>>() {
            @Override
            public void onResponse(Call<List<AsynSbcResponse>> call, Response<List<AsynSbcResponse>> response) {
                mutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<AsynSbcResponse>> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }
}
