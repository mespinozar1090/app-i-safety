package com.mdp.fusionapp.network.response;

import com.google.gson.annotations.SerializedName;

public class AuditoriaResponse {

    @SerializedName("Id_Audit")
    private Integer idAudit;

    @SerializedName("Id_Empresa_contratista")
    private Integer idEmpresacontratista;

    @SerializedName("Sede")
    private Integer sede;

    @SerializedName("Estado_Audit")
    private Integer estadoAudit;

    @SerializedName("Fecha_Reg")
    private String fechaReg;

    @SerializedName("Id_Usuario")
    private Integer idUsuario;

    @SerializedName("Codigo_Audit")
    private String codigoAudit;

    @SerializedName("Nombre_Formato")
    private String nombreFormato;

    @SerializedName("IdUsuarioNotificado")
    private String idUsuarioNotificado;

    @SerializedName("NombreAutor")
    private String nombreAutor;

    @SerializedName("Id_Rol_Usuario")
    private Integer idRolUsuario;

    @SerializedName("Id_Empresa")
    private Integer idEmpresa;

    public Integer getIdAudit() {
        return idAudit;
    }

    public void setIdAudit(Integer idAudit) {
        this.idAudit = idAudit;
    }

    public Integer getIdEmpresacontratista() {
        return idEmpresacontratista;
    }

    public void setIdEmpresacontratista(Integer idEmpresacontratista) {
        this.idEmpresacontratista = idEmpresacontratista;
    }

    public Integer getSede() {
        return sede;
    }

    public void setSede(Integer sede) {
        this.sede = sede;
    }

    public Integer getEstadoAudit() {
        return estadoAudit;
    }

    public void setEstadoAudit(Integer estadoAudit) {
        this.estadoAudit = estadoAudit;
    }

    public String getFechaReg() {
        return fechaReg;
    }

    public void setFechaReg(String fechaReg) {
        this.fechaReg = fechaReg;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getCodigoAudit() {
        return codigoAudit;
    }

    public void setCodigoAudit(String codigoAudit) {
        this.codigoAudit = codigoAudit;
    }

    public String getNombreFormato() {
        return nombreFormato;
    }

    public void setNombreFormato(String nombreFormato) {
        this.nombreFormato = nombreFormato;
    }

    public String getIdUsuarioNotificado() {
        return idUsuarioNotificado;
    }

    public void setIdUsuarioNotificado(String idUsuarioNotificado) {
        this.idUsuarioNotificado = idUsuarioNotificado;
    }

    public String getNombreAutor() {
        return nombreAutor;
    }

    public void setNombreAutor(String nombreAutor) {
        this.nombreAutor = nombreAutor;
    }

    public Integer getIdRolUsuario() {
        return idRolUsuario;
    }

    public void setIdRolUsuario(Integer idRolUsuario) {
        this.idRolUsuario = idRolUsuario;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }
}
