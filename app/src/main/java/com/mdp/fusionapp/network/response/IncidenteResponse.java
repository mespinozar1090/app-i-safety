package com.mdp.fusionapp.network.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IncidenteResponse {

    @SerializedName("Data")
    private List<Data> data;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data {

        private Integer idRow;

        @SerializedName("Id_Incidente")
        private Integer idIncidente;

        @SerializedName("Tipo_InformeP_F")
        private String tipoInformePF;

        @SerializedName("G_IdProyecto_Sede")
        private Integer gIdProyectoSede;

        @SerializedName("G_IdUsuario_Modifica")
        private String gIdUsuarioModifica;

        @SerializedName("G_Fecha_Creado")
        private String gFechaCreado;

        @SerializedName("G_Fecha_Modifica")
        private String gFechaModifica;

        @SerializedName("G_DescripcionFormato")
        private String gDescripcionFormato;

        @SerializedName("F_IdEstado_FormatoInicial")
        private String fIdEstadoFormatoInicial;

        @SerializedName("P_Id_UsuarioNotificado")
        private String pIdUsuarioNotificado;

        @SerializedName("F_Id_UsuarioNotificado")
        private String fIdUsuarioNotificado;

        @SerializedName("G_IdUsuario_Creado")
        private GIdUsuarioCreado gIdUsuarioCreado;

        @SerializedName("P_IdEstato_Formato")
        private PIdEstatoFormato pIdEstatoFormato;

        @SerializedName("P_EVENTO_IdTipoEvento")
        private PEVENTOIdTipoEvento pEVENTOIdTipoEvento;

        @SerializedName("F_EVENTO_IdTipoEvento")
        private FEVENTOIdTipoEvento fEVENTOIdTipoEvento;

        private String horaCreacion;

        public String getHoraCreacion() {
            return horaCreacion;
        }

        public void setHoraCreacion(String horaCreacion) {
            this.horaCreacion = horaCreacion;
        }

        public Integer getIdIncidente() {
            return idIncidente;
        }

        public void setIdIncidente(Integer idIncidente) {
            this.idIncidente = idIncidente;
        }

        public String getTipoInformePF() {
            return tipoInformePF;
        }

        public void setTipoInformePF(String tipoInformePF) {
            this.tipoInformePF = tipoInformePF;
        }

        public Integer getgIdProyectoSede() {
            return gIdProyectoSede;
        }

        public void setgIdProyectoSede(Integer gIdProyectoSede) {
            this.gIdProyectoSede = gIdProyectoSede;
        }

        public String getgIdUsuarioModifica() {
            return gIdUsuarioModifica;
        }

        public void setgIdUsuarioModifica(String gIdUsuarioModifica) {
            this.gIdUsuarioModifica = gIdUsuarioModifica;
        }

        public String getgFechaCreado() {
            return gFechaCreado;
        }

        public void setgFechaCreado(String gFechaCreado) {
            this.gFechaCreado = gFechaCreado;
        }

        public String getgFechaModifica() {
            return gFechaModifica;
        }

        public void setgFechaModifica(String gFechaModifica) {
            this.gFechaModifica = gFechaModifica;
        }

        public String getgDescripcionFormato() {
            return gDescripcionFormato;
        }

        public void setgDescripcionFormato(String gDescripcionFormato) {
            this.gDescripcionFormato = gDescripcionFormato;
        }

        public String getfIdEstadoFormatoInicial() {
            return fIdEstadoFormatoInicial;
        }

        public void setfIdEstadoFormatoInicial(String fIdEstadoFormatoInicial) {
            this.fIdEstadoFormatoInicial = fIdEstadoFormatoInicial;
        }

        public String getpIdUsuarioNotificado() {
            return pIdUsuarioNotificado;
        }

        public void setpIdUsuarioNotificado(String pIdUsuarioNotificado) {
            this.pIdUsuarioNotificado = pIdUsuarioNotificado;
        }

        public String getfIdUsuarioNotificado() {
            return fIdUsuarioNotificado;
        }

        public void setfIdUsuarioNotificado(String fIdUsuarioNotificado) {
            this.fIdUsuarioNotificado = fIdUsuarioNotificado;
        }

        public GIdUsuarioCreado getgIdUsuarioCreado() {
            return gIdUsuarioCreado;
        }

        public void setgIdUsuarioCreado(GIdUsuarioCreado gIdUsuarioCreado) {
            this.gIdUsuarioCreado = gIdUsuarioCreado;
        }

        public PIdEstatoFormato getpIdEstatoFormato() {
            return pIdEstatoFormato;
        }

        public void setpIdEstatoFormato(PIdEstatoFormato pIdEstatoFormato) {
            this.pIdEstatoFormato = pIdEstatoFormato;
        }

        public PEVENTOIdTipoEvento getpEVENTOIdTipoEvento() {
            return pEVENTOIdTipoEvento;
        }

        public void setpEVENTOIdTipoEvento(PEVENTOIdTipoEvento pEVENTOIdTipoEvento) {
            this.pEVENTOIdTipoEvento = pEVENTOIdTipoEvento;
        }

        public FEVENTOIdTipoEvento getfEVENTOIdTipoEvento() {
            return fEVENTOIdTipoEvento;
        }

        public void setfEVENTOIdTipoEvento(FEVENTOIdTipoEvento fEVENTOIdTipoEvento) {
            this.fEVENTOIdTipoEvento = fEVENTOIdTipoEvento;
        }


        public Integer getIdRow() {
            return idRow;
        }

        public void setIdRow(Integer idRow) {
            this.idRow = idRow;
        }

        public class GIdUsuarioCreado{

            @SerializedName("Id_Elemento")
            private Integer idElemento;

            @SerializedName("Descripcion_Elemento")
            private String descripcionElemento;

            @SerializedName("Estado_Elemento")
            private Boolean estadoElemento;

            public Integer getIdElemento() {
                return idElemento;
            }

            public void setIdElemento(Integer idElemento) {
                this.idElemento = idElemento;
            }

            public String getDescripcionElemento() {
                return descripcionElemento;
            }

            public void setDescripcionElemento(String descripcionElemento) {
                this.descripcionElemento = descripcionElemento;
            }

            public Boolean getEstadoElemento() {
                return estadoElemento;
            }

            public void setEstadoElemento(Boolean estadoElemento) {
                this.estadoElemento = estadoElemento;
            }
        }

        public class PIdEstatoFormato{

            @SerializedName("Id_Elemento")
            private Integer idElemento;

            @SerializedName("Descripcion_Elemento")
            private String descripcionElemento;

            @SerializedName("Estado_Elemento")
            private Boolean estadoElemento;

            public Integer getIdElemento() {
                return idElemento;
            }

            public void setIdElemento(Integer idElemento) {
                this.idElemento = idElemento;
            }

            public String getDescripcionElemento() {
                return descripcionElemento;
            }

            public void setDescripcionElemento(String descripcionElemento) {
                this.descripcionElemento = descripcionElemento;
            }

            public Boolean getEstadoElemento() {
                return estadoElemento;
            }

            public void setEstadoElemento(Boolean estadoElemento) {
                this.estadoElemento = estadoElemento;
            }
        }

        private class PEVENTOIdTipoEvento{

            @SerializedName("Id_Elemento")
            private Integer idElemento;

            @SerializedName("Descripcion_Elemento")
            private String descripcionElemento;

            @SerializedName("Estado_Elemento")
            private Boolean estadoElemento;

            public Integer getIdElemento() {
                return idElemento;
            }

            public void setIdElemento(Integer idElemento) {
                this.idElemento = idElemento;
            }

            public String getDescripcionElemento() {
                return descripcionElemento;
            }

            public void setDescripcionElemento(String descripcionElemento) {
                this.descripcionElemento = descripcionElemento;
            }

            public Boolean getEstadoElemento() {
                return estadoElemento;
            }

            public void setEstadoElemento(Boolean estadoElemento) {
                this.estadoElemento = estadoElemento;
            }
        }

        private class FEVENTOIdTipoEvento{

            @SerializedName("Id_Elemento")
            private Integer idElemento;

            @SerializedName("Descripcion_Elemento")
            private String descripcionElemento;

            @SerializedName("Estado_Elemento")
            private Boolean estadoElemento;

            public Integer getIdElemento() {
                return idElemento;
            }

            public void setIdElemento(Integer idElemento) {
                this.idElemento = idElemento;
            }

            public String getDescripcionElemento() {
                return descripcionElemento;
            }

            public void setDescripcionElemento(String descripcionElemento) {
                this.descripcionElemento = descripcionElemento;
            }

            public Boolean getEstadoElemento() {
                return estadoElemento;
            }

            public void setEstadoElemento(Boolean estadoElemento) {
                this.estadoElemento = estadoElemento;
            }
        }
    }
}
