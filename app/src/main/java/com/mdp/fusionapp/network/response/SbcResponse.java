package com.mdp.fusionapp.network.response;

import com.google.gson.annotations.SerializedName;

public class SbcResponse {

    @SerializedName("Id_sbc")
    private Integer idSbc;

    @SerializedName("Estado_SBC")
    private Integer estadoSBC;

    @SerializedName("Fecha_Registro")
    private String fechaRegistro;

    @SerializedName("Nombre_Formato")
    private String nombreFormato;

    @SerializedName("Nombre_Observador")
    private String nombreObservador;

    @SerializedName("Codigo_SBC")
    private String codigoSBC;

    @SerializedName("Observacion")
    private String observacion;

    @SerializedName("IdUsuario")
    private String idUsuario;

    @SerializedName("Id_Empresa_Observada")
    private String idEmpresaObservada;

    private Long idRowSbc;

    public Long getIdRowSbc() {
        return idRowSbc;
    }

    public void setIdRowSbc(Long idRowSbc) {
        this.idRowSbc = idRowSbc;
    }

    public Integer getIdSbc() {
        return idSbc;
    }

    public void setIdSbc(Integer idSbc) {
        this.idSbc = idSbc;
    }

    public Integer getEstadoSBC() {
        return estadoSBC;
    }

    public void setEstadoSBC(Integer estadoSBC) {
        this.estadoSBC = estadoSBC;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getNombreFormato() {
        return nombreFormato;
    }

    public void setNombreFormato(String nombreFormato) {
        this.nombreFormato = nombreFormato;
    }

    public String getNombreObservador() {
        return nombreObservador;
    }

    public void setNombreObservador(String nombreObservador) {
        this.nombreObservador = nombreObservador;
    }

    public String getCodigoSBC() {
        return codigoSBC;
    }

    public void setCodigoSBC(String codigoSBC) {
        this.codigoSBC = codigoSBC;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getIdEmpresaObservada() {
        return idEmpresaObservada;
    }

    public void setIdEmpresaObservada(String idEmpresaObservada) {
        this.idEmpresaObservada = idEmpresaObservada;
    }
}
