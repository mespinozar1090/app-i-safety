package com.mdp.fusionapp.network.reposiroty;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.mdp.fusionapp.database.entity.incidente.EquipoEntity;
import com.mdp.fusionapp.database.entity.incidente.ParteCuerpoEntity;
import com.mdp.fusionapp.database.repository.incidente.EquipoRepository;
import com.mdp.fusionapp.database.repository.incidente.ParteCuerpoRepository;
import com.mdp.fusionapp.network.response.ComboMaestroEntity;
import com.mdp.fusionapp.network.response.IncidenteDetalleResponse;
import com.mdp.fusionapp.network.response.IncidenteResponse;
import com.mdp.fusionapp.network.response.Respuesta;
import com.mdp.fusionapp.network.service.APIService;
import com.mdp.fusionapp.utilitary.UtilMDP;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IncidenteRepository {

    private final String LOGGER = "IncidenteRepository";
    private APIService apiService;

    private static IncidenteRepository instancia;

    public IncidenteRepository() {
    }

    public static IncidenteRepository getInstance() {
        if (instancia == null) {
            instancia = new IncidenteRepository();
        }
        return instancia;
    }

    public MutableLiveData<IncidenteResponse> listarIncidente(HashMap hashMap,Context context){
        final MutableLiveData<IncidenteResponse> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<IncidenteResponse> listCall = apiService.listarIncidente(hashMap);

        listCall.enqueue(new Callback<IncidenteResponse>() {
            @Override
            public void onResponse(Call<IncidenteResponse> call, Response<IncidenteResponse> response) {
                mutableLiveData.setValue(response.body());
            }
            @Override
            public void onFailure(Call<IncidenteResponse> call, Throwable t) {
                Log.e(LOGGER,":"+t.getMessage());
                mutableLiveData.setValue(null);
            }
        });
        return  mutableLiveData;
    }

    public MutableLiveData<List<ComboMaestroEntity>> obtenerComboMaestroGenerico (String[] array){
        final MutableLiveData<List<ComboMaestroEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<ComboMaestroEntity>> listCall = apiService.obtenerComboMaestroGenerico(array);

        listCall.enqueue(new Callback<List<ComboMaestroEntity>>() {
            @Override
            public void onResponse(Call<List<ComboMaestroEntity>> call, Response<List<ComboMaestroEntity>> response) {
                List<ComboMaestroEntity> list = new ArrayList<>();
                list.add(new ComboMaestroEntity(0,"Seleccionar"));
                list.addAll(response.body());
                mutableLiveData.setValue(list);
            }
            @Override
            public void onFailure(Call<List<ComboMaestroEntity>> call, Throwable t) {
                Log.e(LOGGER,":"+t.getMessage());
                mutableLiveData.setValue(null);
            }
        });
        return  mutableLiveData;
    }

    public MutableLiveData<List<EquipoEntity>> equipoInvolucrado (String[] array , EquipoRepository repository){
        final MutableLiveData<List<EquipoEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<EquipoEntity>> listCall = apiService.equipoInvolucrado(array);

        listCall.enqueue(new Callback<List<EquipoEntity>>() {
            @Override
            public void onResponse(Call<List<EquipoEntity>> call, Response<List<EquipoEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    repository.deleteAll();
                    repository.insert(new EquipoEntity(0, "Seleccionar"));
                    for (int i = 0; i < response.body().size(); i++) {
                        repository.insert(response.body().get(i));
                    }
                    mutableLiveData.setValue(repository.findAll());
                }
            }
            @Override
            public void onFailure(Call<List<EquipoEntity>> call, Throwable t) {
                mutableLiveData.setValue(repository.findAll());
            }
        });
        return  mutableLiveData;
    }

    public MutableLiveData<List<ParteCuerpoEntity>> parteCuerpoInvolucrado (String[] array , ParteCuerpoRepository repository){
        final MutableLiveData<List<ParteCuerpoEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<ParteCuerpoEntity>> listCall = apiService.parteCuerpoInvolucrado(array);

        listCall.enqueue(new Callback<List<ParteCuerpoEntity>>() {
            @Override
            public void onResponse(Call<List<ParteCuerpoEntity>> call, Response<List<ParteCuerpoEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    repository.deleteAll();
                    repository.insert(new ParteCuerpoEntity(0, "Seleccionar"));
                    for (int i = 0; i < response.body().size(); i++) {
                        repository.insert(response.body().get(i));
                    }
                    mutableLiveData.setValue(repository.findAll());
                }
            }
            @Override
            public void onFailure(Call<List<ParteCuerpoEntity>> call, Throwable t) {
                mutableLiveData.setValue(repository.findAll());
            }
        });
        return  mutableLiveData;
    }

    public MutableLiveData<Respuesta> insertaIncidente(HashMap hashMap, Context context){
        final MutableLiveData<Respuesta> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call listCall = apiService.insertaIncidente(hashMap);

        listCall.enqueue(new Callback<Respuesta>() {
            @Override
            public void onResponse(Call<Respuesta> call, Response<Respuesta> response) {
                Log.e("onResponse:"," mesagge :"+response.message());
                mutableLiveData.setValue(response.body());
            }
            @Override
            public void onFailure(Call<Respuesta> call, Throwable t) {
                Log.e("onFailure:",t.getMessage());
                mutableLiveData.setValue(null);
            }
        });
        return  mutableLiveData;
    }

    public MutableLiveData<Respuesta> editarIncidente(HashMap hashMap, Context context){
        final MutableLiveData<Respuesta> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<Respuesta> listCall = apiService.editarIncidente(hashMap);

        listCall.enqueue(new Callback<Respuesta>() {
            @Override
            public void onResponse(Call<Respuesta> call, Response<Respuesta> response) {
                Log.e("onResponse:"," mesagge :"+response.message());
                mutableLiveData.setValue(response.body());
            }
            @Override
            public void onFailure(Call<Respuesta> call, Throwable t) {
                Log.e("onFailure:",t.getMessage());
                mutableLiveData.setValue(null);
            }
        });
        return  mutableLiveData;
    }

    public MutableLiveData<IncidenteDetalleResponse> obtenerRegistoIncidente(String[] array){
        final MutableLiveData<IncidenteDetalleResponse> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<IncidenteDetalleResponse> listCall = apiService.obtenerRegistoIncidente(array);

        listCall.enqueue(new Callback<IncidenteDetalleResponse>() {
            @Override
            public void onResponse(Call<IncidenteDetalleResponse> call, Response<IncidenteDetalleResponse> response) {
                Log.e("onResponse:",response.message());
                mutableLiveData.setValue(response.body());
            }
            @Override
            public void onFailure(Call<IncidenteDetalleResponse> call, Throwable t) {
                Log.e("onFailure:",t.getMessage());
                mutableLiveData.setValue(null);
            }
        });
        return  mutableLiveData;
    }

    public MutableLiveData<List<IncidenteDetalleResponse>> detalleIncidenteOffline(HashMap hashMap){
        final MutableLiveData<List<IncidenteDetalleResponse>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<IncidenteDetalleResponse>> listCall = apiService.detalleIncidenteOffline(hashMap);

        listCall.enqueue(new Callback<List<IncidenteDetalleResponse>>() {
            @Override
            public void onResponse(Call<List<IncidenteDetalleResponse>> call, Response<List<IncidenteDetalleResponse>> response) {
                Log.e("eror:",response.message());
                mutableLiveData.setValue(response.body());
            }
            @Override
            public void onFailure(Call<List<IncidenteDetalleResponse>> call, Throwable t) {
                Log.e("eror:",t.getMessage());
                mutableLiveData.setValue(null);
            }
        });
        return  mutableLiveData;
    }


}
