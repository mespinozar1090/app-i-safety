package com.mdp.fusionapp.network.response;

import com.google.gson.annotations.SerializedName;
import com.mdp.fusionapp.model.ElementoObject;

import java.io.Serializable;

public class IncidenteDetalleResponse implements Serializable {

    private Integer idRow;

    @SerializedName("Id_Incidente")
    private Integer idIncidente;

    @SerializedName("Tipo_InformeP_F")
    private String tipoInforme;

    @SerializedName("G_IdProyecto_Sede")
    private GProyectoSede idProyectoSede;

    @SerializedName("G_IdUsuario_Creado")
    private GUsuarioCreado G_IdUsuario_Creado;

    @SerializedName("G_IdUsuario_Modifica")
    private GUsarioModifica G_IdUsuario_Modifica;

    @SerializedName("G_Fecha_Creado")
    private String G_Fecha_Creado;

    @SerializedName("G_Fecha_Modifica")
    private String G_Fecha_Modifica;

    @SerializedName("G_DescripcionFormato")
    private String G_DescripcionFormato;

    @SerializedName("Empleador_Tipo")
    private String empleadorTipo;

    @SerializedName("F_Empleador_IdEmpresa")
    private Empleadoridempresa empleadoridempresa;

    @SerializedName("F_Empleador_RazonSocial")
    private String empleadorRazonSocial;

    @SerializedName("F_Empleador_RUC")
    private String empleadorRuc;

    @SerializedName("F_Empleador_IdActividadEconomica")
    private EmpleadoridActividadEconomica empleadorIdActividadEconomica;

    @SerializedName("F_Empleador_IdTamanioEmpresa")
    private EmpleadoidtamanioEmpresa empleadorIdTamanioEmpresa;

    @SerializedName("F_EmpleadorI_RazonSocial")
    private String empleadorIRazonSocial;

    @SerializedName("F_EmpleadorI_RUC")
    private String empleadorIRuc;

    @SerializedName("F_EmpleadorI_IdActividadEconomica")
    private EmpleadorIIdActividadeconomica empleadorIIdActividadeconomica;

    @SerializedName("F_EVENTO_IdTipoEvento")
    private EventoIdTipoEvento eventoIdTipoEvento;

    @SerializedName("F_EVENTO_HuboDanioMaterial")
    private String eventoHuboDanioMaterial;

    @SerializedName("F_EVENTO_NumTrabajadoresAfectadas")
    private String eventoNumeroTrabajadoresAfectados;

    @SerializedName("F_EVENTO_FechaAccidente")
    private String eventoFechaAccidente;

    @SerializedName("F_EVENTO_HoraAccidente")
    private String eventoHoraAccidente;

    @SerializedName("F_EVENTO_FechaInicioInvestigacion")
    private String eventoFechaInicioInvestagacion;

    @SerializedName("F_EVENTO_LugarExacto")
    private String eventoLugarExacto;

    @SerializedName("F_EVENTO_IdParteAfectada")
    private EventoIdParteAfectada eventoIdParteAfectada;

    @SerializedName("F_EVENTO_IdEquipoAfectado")
    private EventoIdEquipoAfectado eventoIdEquipoAfectado;

    @SerializedName("F_DESCRIPCION_Incidente")
    private String descriptionIncidente;

    @SerializedName("F_Trabajador_NombresApellidos")
    private String trabajadorNombreApellido;

    @SerializedName("F_EmpleadorI_IdEmpresa")
    private FempleadorIIdEmpresa fempleadorIIdEmpresa;

    public Integer getIdRow() {
        return idRow;
    }

    public void setIdRow(Integer idRow) {
        this.idRow = idRow;
    }

    public FempleadorIIdEmpresa getFempleadorIIdEmpresa() {
        return fempleadorIIdEmpresa;
    }

    public void setFempleadorIIdEmpresa(FempleadorIIdEmpresa fempleadorIIdEmpresa) {
        this.fempleadorIIdEmpresa = fempleadorIIdEmpresa;
    }

    public String getTrabajadorNombreApellido() {
        return trabajadorNombreApellido;
    }

    public void setTrabajadorNombreApellido(String trabajadorNombreApellido) {
        this.trabajadorNombreApellido = trabajadorNombreApellido;
    }

    public Integer getIdIncidente() {
        return idIncidente;
    }

    public void setIdIncidente(Integer idIncidente) {
        this.idIncidente = idIncidente;
    }

    public String getTipoInforme() {
        return tipoInforme;
    }

    public void setTipoInforme(String tipoInforme) {
        this.tipoInforme = tipoInforme;
    }

    public GProyectoSede getIdProyectoSede() {
        return idProyectoSede;
    }

    public void setIdProyectoSede(GProyectoSede idProyectoSede) {
        this.idProyectoSede = idProyectoSede;
    }

    public GUsuarioCreado getG_IdUsuario_Creado() {
        return G_IdUsuario_Creado;
    }

    public void setG_IdUsuario_Creado(GUsuarioCreado g_IdUsuario_Creado) {
        G_IdUsuario_Creado = g_IdUsuario_Creado;
    }

    public GUsarioModifica getG_IdUsuario_Modifica() {
        return G_IdUsuario_Modifica;
    }

    public void setG_IdUsuario_Modifica(GUsarioModifica g_IdUsuario_Modifica) {
        G_IdUsuario_Modifica = g_IdUsuario_Modifica;
    }

    public String getG_Fecha_Creado() {
        return G_Fecha_Creado;
    }

    public void setG_Fecha_Creado(String g_Fecha_Creado) {
        G_Fecha_Creado = g_Fecha_Creado;
    }

    public String getG_Fecha_Modifica() {
        return G_Fecha_Modifica;
    }

    public void setG_Fecha_Modifica(String g_Fecha_Modifica) {
        G_Fecha_Modifica = g_Fecha_Modifica;
    }

    public String getG_DescripcionFormato() {
        return G_DescripcionFormato;
    }

    public void setG_DescripcionFormato(String g_DescripcionFormato) {
        G_DescripcionFormato = g_DescripcionFormato;
    }

    public String getEmpleadorTipo() {
        return empleadorTipo;
    }

    public void setEmpleadorTipo(String empleadorTipo) {
        this.empleadorTipo = empleadorTipo;
    }

    public Empleadoridempresa getEmpleadoridempresa() {
        return empleadoridempresa;
    }

    public void setEmpleadoridempresa(Empleadoridempresa empleadoridempresa) {
        this.empleadoridempresa = empleadoridempresa;
    }

    public String getEmpleadorRazonSocial() {
        return empleadorRazonSocial;
    }

    public void setEmpleadorRazonSocial(String empleadorRazonSocial) {
        this.empleadorRazonSocial = empleadorRazonSocial;
    }

    public String getEmpleadorRuc() {
        return empleadorRuc;
    }

    public void setEmpleadorRuc(String empleadorRuc) {
        this.empleadorRuc = empleadorRuc;
    }

    public EmpleadoridActividadEconomica getEmpleadorIdActividadEconomica() {
        return empleadorIdActividadEconomica;
    }

    public void setEmpleadorIdActividadEconomica(EmpleadoridActividadEconomica empleadorIdActividadEconomica) {
        this.empleadorIdActividadEconomica = empleadorIdActividadEconomica;
    }

    public EmpleadoidtamanioEmpresa getEmpleadorIdTamanioEmpresa() {
        return empleadorIdTamanioEmpresa;
    }

    public void setEmpleadorIdTamanioEmpresa(EmpleadoidtamanioEmpresa empleadorIdTamanioEmpresa) {
        this.empleadorIdTamanioEmpresa = empleadorIdTamanioEmpresa;
    }

    public String getEmpleadorIRazonSocial() {
        return empleadorIRazonSocial;
    }

    public void setEmpleadorIRazonSocial(String empleadorIRazonSocial) {
        this.empleadorIRazonSocial = empleadorIRazonSocial;
    }

    public String getEmpleadorIRuc() {
        return empleadorIRuc;
    }

    public void setEmpleadorIRuc(String empleadorIRuc) {
        this.empleadorIRuc = empleadorIRuc;
    }

    public EmpleadorIIdActividadeconomica getEmpleadorIIdActividadeconomica() {
        return empleadorIIdActividadeconomica;
    }

    public void setEmpleadorIIdActividadeconomica(EmpleadorIIdActividadeconomica empleadorIIdActividadeconomica) {
        this.empleadorIIdActividadeconomica = empleadorIIdActividadeconomica;
    }

    public EventoIdTipoEvento getEventoIdTipoEvento() {
        return eventoIdTipoEvento;
    }

    public void setEventoIdTipoEvento(EventoIdTipoEvento eventoIdTipoEvento) {
        this.eventoIdTipoEvento = eventoIdTipoEvento;
    }

    public String getEventoHuboDanioMaterial() {
        return eventoHuboDanioMaterial;
    }

    public void setEventoHuboDanioMaterial(String eventoHuboDanioMaterial) {
        this.eventoHuboDanioMaterial = eventoHuboDanioMaterial;
    }

    public String getEventoNumeroTrabajadoresAfectados() {
        return eventoNumeroTrabajadoresAfectados;
    }

    public void setEventoNumeroTrabajadoresAfectados(String eventoNumeroTrabajadoresAfectados) {
        this.eventoNumeroTrabajadoresAfectados = eventoNumeroTrabajadoresAfectados;
    }

    public String getEventoFechaAccidente() {
        return eventoFechaAccidente;
    }

    public void setEventoFechaAccidente(String eventoFechaAccidente) {
        this.eventoFechaAccidente = eventoFechaAccidente;
    }

    public String getEventoHoraAccidente() {
        return eventoHoraAccidente;
    }

    public void setEventoHoraAccidente(String eventoHoraAccidente) {
        this.eventoHoraAccidente = eventoHoraAccidente;
    }

    public String getEventoFechaInicioInvestagacion() {
        return eventoFechaInicioInvestagacion;
    }

    public void setEventoFechaInicioInvestagacion(String eventoFechaInicioInvestagacion) {
        this.eventoFechaInicioInvestagacion = eventoFechaInicioInvestagacion;
    }

    public String getEventoLugarExacto() {
        return eventoLugarExacto;
    }

    public void setEventoLugarExacto(String eventoLugarExacto) {
        this.eventoLugarExacto = eventoLugarExacto;
    }

    public EventoIdParteAfectada getEventoIdParteAfectada() {
        return eventoIdParteAfectada;
    }

    public void setEventoIdParteAfectada(EventoIdParteAfectada eventoIdParteAfectada) {
        this.eventoIdParteAfectada = eventoIdParteAfectada;
    }

    public EventoIdEquipoAfectado getEventoIdEquipoAfectado() {
        return eventoIdEquipoAfectado;
    }

    public void setEventoIdEquipoAfectado(EventoIdEquipoAfectado eventoIdEquipoAfectado) {
        this.eventoIdEquipoAfectado = eventoIdEquipoAfectado;
    }

    public String getDescriptionIncidente() {
        return descriptionIncidente;
    }

    public void setDescriptionIncidente(String descriptionIncidente) {
        this.descriptionIncidente = descriptionIncidente;
    }

    public static class GProyectoSede extends ElementoObject {
    }

    public static class GUsuarioCreado extends ElementoObject {
    }

    public static class GUsarioModifica extends ElementoObject {
    }

    public static class Empleadoridempresa extends ElementoObject {
    }

    public static class EmpleadoridActividadEconomica extends ElementoObject {
    }

    public static class EmpleadoidtamanioEmpresa extends ElementoObject {
    }

    public static class EmpleadorIIdActividadeconomica extends ElementoObject {
    }

    public static class EventoIdTipoEvento extends ElementoObject {
    }

    public static class EventoIdParteAfectada extends ElementoObject {
    }

    public static class EventoIdEquipoAfectado extends ElementoObject {
    }

    public static class FempleadorIIdEmpresa extends ElementoObject {

    }

}
