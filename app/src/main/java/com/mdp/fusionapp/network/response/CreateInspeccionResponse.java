package com.mdp.fusionapp.network.response;

import com.google.gson.annotations.SerializedName;

public class CreateInspeccionResponse {

    @SerializedName("Id_Inspecciones")
    private Integer Id_Inspecciones;

    @SerializedName("codigoInspecion")
    private String codigoInspecion;

    @SerializedName("proyecto")
    private String proyecto;

    public Integer getId_Inspecciones() {
        return Id_Inspecciones;
    }

    public void setId_Inspecciones(Integer id_Inspecciones) {
        Id_Inspecciones = id_Inspecciones;
    }

    public String getCodigoInspecion() {
        return codigoInspecion;
    }

    public void setCodigoInspecion(String codigoInspecion) {
        this.codigoInspecion = codigoInspecion;
    }

    public String getProyecto() {
        return proyecto;
    }

    public void setProyecto(String proyecto) {
        this.proyecto = proyecto;
    }
}
