package com.mdp.fusionapp.network.response;

import com.google.gson.annotations.SerializedName;

public class LugarResponse {

    @SerializedName("idLugar")
    private Integer idLugar;

    @SerializedName("Nombre")
    private String nombre;

    public LugarResponse() {
    }

    public LugarResponse(Integer idLugar, String nombre) {
        this.idLugar = idLugar;
        this.nombre = nombre;
    }

    public Integer getIdLugar() {
        return idLugar;
    }

    public void setIdLugar(Integer idLugar) {
        this.idLugar = idLugar;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        nombre = nombre;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
