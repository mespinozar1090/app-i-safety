package com.mdp.fusionapp.network.response;

import com.google.gson.annotations.SerializedName;

public class SubEstacionResponse {

    @SerializedName("Id_Linea_Subestacion")
    private Integer idLineaSubestacion;

    @SerializedName("Descripcion")
    private String descripcion;

    public SubEstacionResponse() {
    }

    public SubEstacionResponse(Integer idLineaSubestacion, String descripcion) {
        this.idLineaSubestacion = idLineaSubestacion;
        this.descripcion = descripcion;
    }

    public Integer getIdLineaSubestacion() {
        return idLineaSubestacion;
    }

    public void setIdLineaSubestacion(Integer idLineaSubestacion) {
        this.idLineaSubestacion = idLineaSubestacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return descripcion;
    }
}
