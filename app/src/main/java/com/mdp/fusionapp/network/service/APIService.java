package com.mdp.fusionapp.network.service;

import com.mdp.fusionapp.database.entity.CategoriaBuenaPracticaEntity;
import com.mdp.fusionapp.database.entity.ContratistaEntity;
import com.mdp.fusionapp.database.entity.EmpresaMaestraEntity;
import com.mdp.fusionapp.database.entity.LugarEntity;
import com.mdp.fusionapp.database.entity.ProyectoEntity;
import com.mdp.fusionapp.database.entity.TipoUbicacionEntity;
import com.mdp.fusionapp.database.entity.UsuarioResponsableEntity;
import com.mdp.fusionapp.database.entity.incidente.ParteCuerpoEntity;
import com.mdp.fusionapp.database.entity.incidente.TipoEventoEntity;
import com.mdp.fusionapp.database.entity.incidente.EquipoEntity;
import com.mdp.fusionapp.network.response.ActividadEntity;
import com.mdp.fusionapp.database.entity.AreaTrabajoEntity;
import com.mdp.fusionapp.network.response.AsynAutoriaResponse;
import com.mdp.fusionapp.network.response.AsynInspeccionResponse;
import com.mdp.fusionapp.network.response.AsynSbcResponse;
import com.mdp.fusionapp.network.response.AuditoriaResponse;
import com.mdp.fusionapp.database.entity.CategoriaSbcItemEntity;
import com.mdp.fusionapp.database.entity.CategoriaSbcEntity;
import com.mdp.fusionapp.network.response.ComboMaestroEntity;
import com.mdp.fusionapp.database.entity.interventoria.ControlSubItemEntity;
import com.mdp.fusionapp.network.response.CreateInspeccionResponse;
import com.mdp.fusionapp.database.entity.EmpresaSbcEntity;
import com.mdp.fusionapp.database.entity.EspecialidadEntity;
import com.mdp.fusionapp.network.response.IncidenteDetalleResponse;
import com.mdp.fusionapp.network.response.IncidenteResponse;
import com.mdp.fusionapp.network.response.InspeccionDetalleResponse;
import com.mdp.fusionapp.network.response.InspeccionEmpresaEntity;
import com.mdp.fusionapp.network.response.InspeccionResponse;
import com.mdp.fusionapp.network.response.InterventoriaResponse;
import com.mdp.fusionapp.database.entity.LineamientoEntity;
import com.mdp.fusionapp.database.entity.LineamientoSubEntity;
import com.mdp.fusionapp.network.response.LoginResponse;
import com.mdp.fusionapp.network.response.Respuesta;
import com.mdp.fusionapp.network.response.SbcResponse;
import com.mdp.fusionapp.database.entity.SedeProyectoEntity;
import com.mdp.fusionapp.network.response.SubEstacionResponse;
import com.mdp.fusionapp.network.response.TorreLugarResponse;
import com.mdp.fusionapp.database.entity.interventoria.VerificacionSubItemEntity;
import com.mdp.fusionapp.network.response.detalle.DetalleAuditoriaResponse;
import com.mdp.fusionapp.network.response.detalle.DetalleSbcResponse;
import com.mdp.fusionapp.database.entity.UsuarioNotificarEntity;
import com.mdp.fusionapp.network.response.interventoria.EvidenciaInterResponse;
import com.mdp.fusionapp.network.response.interventoria.InterventoriaDetalleOffineResponse;
import com.mdp.fusionapp.network.response.interventoria.InterventoriaDetalleResponse;

import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface APIService {

    @POST("api/Login/Acceso")
    Call<LoginResponse> loginAcceso(@Body Map<String, Object> body);

    //TODO - INSPECCIONES
    @POST("api/Inspeccion/ListarInspeccionFiltrosBandeja")
    Call<List<InspeccionResponse>> listaInpecciones(@Body Map<String, Object> body);

    @POST("api/Inspeccion/ListarInspeccionDetalle")
    Call<InspeccionDetalleResponse> detalleInspeccion(@Body Map<String, Object> body);

    @POST("api/Inspeccion/obtenerProyectosRolUsuario_M")
    Call<List<ProyectoEntity>> obtenerProyecto(@Body Map<String, Object> body);

   /* @POST("api/Inspeccion/obtenerEmpresa_Proyecto")
    Call<List<ContratistaEntity>> obtenerContratisa(@Body Map<String, Object> body);*/

    @POST("api/Inspeccion/obtenerEmpresa_ProyectoRolUsuario")
    Call<List<ContratistaEntity>> obtenerContratisa(@Body Map<String, Object> body);

    @POST("api/Inspeccion/obtenerTipoUbicacion")
    Call<List<TipoUbicacionEntity>> tipoUbicacion(@Body Map<String, Object> body);

    @POST("api/Inspeccion/obtenerLugar")
    Call<List<LugarEntity>> obtenerLugar(@Body Map<String, Object> body);

    @GET("api/Inspeccion/CategoriaBuenaPractica")
    Call<List<CategoriaBuenaPracticaEntity>> categoriaBuenaPractica();

    @POST("api/Inspeccion/obtenerEmpresaMaestra_Proyecto")
    Call<EmpresaMaestraEntity> empresaProyecto(@Body Map<String, Object> body);

    @POST("api/Inspeccion/borrarInspecion")
    Call<Boolean> eliminarInspeccion(@Body Map<String, Object> body);

    @POST("api/Inspeccion/insertarBuenaPractica")
    Call<String> insertarBuenaPractica(@Body List<Map<String, Object>> body);

    @POST("api/Inspeccion/insertarDetalleInscripcion")
    Call<Boolean> insertarHallazgo(@Body Map<String, Object> body);

    @POST("api/Inspeccion/RegistroInspeccion")
    Call<CreateInspeccionResponse> insertarInspeccion(@Body Map<String, Object> body);

    @GET("api/Inspeccion/Sede")
    Call<List<SedeProyectoEntity>> obtenerSede();

    @POST("api/Inspeccion/Sede_M")
    Call<List<SedeProyectoEntity>> obtenerSedeM(@Body Map<String, Object> body);




    //@GET("api/Inspeccion/Sede")
    //Call<List<SedeProyectoEntity>> obtenerSede();

    @GET("api/Inspeccion/Empresa")
    Call<List<InspeccionEmpresaEntity>> obtenerEmpresa();

    @GET("api/Inspeccion/Actividad")
    Call<List<ActividadEntity>> obtenerActividad();

    //TODO - MODULO SBC
    @POST("api/SBC/ListarSBCFiltrosBandeja")
    Call<List<SbcResponse>> listarSBC(@Body Map<String, Object> body);

    @GET("api/SBC/ListaEmpresas")
    Call<List<EmpresaSbcEntity>> obtenerEmpresaSBSResponse();

    @GET("api/SBC/ListaEspecialidades")
    Call<List<EspecialidadEntity>> obtenerEspecialidad();

    @GET("api/SBC/ListaCategoria")
    Call<List<CategoriaSbcEntity>> obtenerCategoriaSBC();

    @GET("api/SBC/ListaItemcategoria")
    Call<List<CategoriaSbcItemEntity>> obtenerCategoriaItemSBC();

    @POST("api/SBC/RegistroSBC")
    Call<Respuesta> insertarSBC(@Body Map<String, Object> body);

    @POST("api/SBC/EditarSBC")
    Call<Respuesta> editarSBC(@Body Map<String, Object> body);

    @POST("api/SBC/ObtenerRegistroSBC")
    Call<DetalleSbcResponse> obtenerRegistroSBC(@Body Integer id);

    @POST("api/SBC/borrarSBC")
    Call<Boolean> borrarSBC(@Body Map<String, Object> body);


    @POST("api/Interventoria/FiltrosBandeja")
    Call<InterventoriaResponse> listarInterventoria(@Body Map<String, Object> body);

    @POST("api/Interventoria/borrarInterventoria")
    Call<Boolean> borrarInterventoria(@Body Map<String, Object> body);

    @GET("api/Maestro/ListaLineaSubestacion")
    Call<List<SubEstacionResponse>> obtenerSubEstacion();

    @GET("api/Interventoria/ListaVerificacionesSubitems")
    Call<List<VerificacionSubItemEntity>> obtenerVerificacionSubItem();

    @GET("api/Interventoria/ListaControlItems")
    Call<List<ControlSubItemEntity>> obtenerControlSubItem();

    @POST("api/Interventoria/Registro")
    Call<Respuesta> insertarInterventoria(@Body Map<String, Object> body);

    @POST("api/Inspeccion/obtenerTorresPorLugar")
    Call<List<TorreLugarResponse>> obtenerTorresPorLugar(@Body Map<String, Object> body);

    @POST("api/Interventoria/Detalle")
    Call<InterventoriaDetalleResponse> interventoriaDetalle(@Body Integer id);

    @POST("api/Interventoria/ListaImagenes")
    Call<EvidenciaInterResponse> evidenciaInterventoria(@Body Integer id);

    @POST("api/Interventoria/Editar")
    Call<Respuesta> editarInterventoria(@Body Map<String, Object> body);

    @GET("api/Interventoria/ListaVerificacionesSubitems")
    Call<List<VerificacionSubItemEntity>> listaVerificacionesSubitems();

    @POST("api/Interventoria/ListarInterventoriaDetalles_offline")
    Call<List<InterventoriaDetalleOffineResponse>> detalleInterventoriaOffline(@Body Map<String, Object> body);


    //TODO - MODULO AUDITORIA
    @POST("api/Auditoria/ListarAuditoriaFiltrosBandeja")
    Call<List<AuditoriaResponse>> listaAuditoria(@Body Map<String, Object> body);

    @GET("api/Auditoria/ListaLineamientosItems")
    Call<List<LineamientoEntity>> obtenerLineaminetos();

    @GET("api/Auditoria/ListaLineamientosSubItems")
    Call<List<LineamientoSubEntity>> obtenerLineaminetosSub();

    @POST("api/Auditoria/RegistroAuditoria")
    Call<Respuesta> insertaAuditoria(@Body Map<String, Object> body);

    @POST("api/Auditoria/ObtenerRegistroAuditoria")
    Call<DetalleAuditoriaResponse> obtenerRegistroAuditoria(@Body Integer id);

    @POST("api/Auditoria/EditarAuditoria")
    Call<Respuesta> editarAuditoria(@Body Map<String, Object> body);


    //TODO - MODULO INCIDENTE
    @POST("api/Incidente/FiltrosBandeja")
    Call<IncidenteResponse> listarIncidente(@Body Map<String, Object> body);

    @POST("api/Incidente/CargarComboMestro")
    Call<List<ComboMaestroEntity>> obtenerComboMaestro(@Body String[] body);

    @POST("api/Incidente/CargarComboMestro")
    Call<List<TipoEventoEntity>> tipoEvento(@Body String[] body);

    @POST("api/Incidente/CargarComboMestroGenerico")
    Call<List<ComboMaestroEntity>> obtenerComboMaestroGenerico(@Body String[] body);

    @POST("api/Incidente/CargarComboMestroGenerico")
    Call<List<ParteCuerpoEntity>> parteCuerpoInvolucrado(@Body String[] body);

    @POST("api/Incidente/CargarComboMestroGenerico")
    Call<List<EquipoEntity>> equipoInvolucrado(@Body String[] body);

    @GET("api/Inspeccion/ListarAreaInspeccionada")
    Call<List<AreaTrabajoEntity>> obtenerAreaInspeccion();

    @POST("api/Incidente/Registro")
    Call<Respuesta> insertaIncidente(@Body Map<String, Object> body);

    @POST("api/Incidente/VerIncidente")
    Call<IncidenteDetalleResponse> obtenerRegistoIncidente(@Body String[] body);

    @POST("api/Incidente/ListarIncidenteDetalles_offline")
    Call<List<IncidenteDetalleResponse>> detalleIncidenteOffline(@Body Map<String, Object> body);

    @POST("api/Incidente/Editar")
    Call<Respuesta> editarIncidente(@Body Map<String, Object> body);

    //Usuarios
    @GET("api/Administracion/ListarUsuarioRol")
    Call<List<UsuarioNotificarEntity>> obtenerUsuarioNotificar();

    @POST("api/Usuario/obtenerTodosUsuariosEmpresa")
    Call<List<UsuarioResponsableEntity>> obtenerUsuarios(@Body Map<String, Object> body);


    @POST("api/Inspeccion/NotificacionInspeccion")
    Call<String> sendNotification(@Body Map<String, Object> body);

    //TODO OFFLINE

   /* @GET("api/inspeccion/obtenerProyectos_offline")
    Call<List<ProyectoEntity>> obtenerProyectoOffline();*/

    @GET("api/inspeccion/obtenerProyectos_offline")
    Call<List<ProyectoEntity>> obtenerProyectosRolUsuario();

    @GET("api/inspeccion/obtenerEmpresa_offline")
    Call<List<ContratistaEntity>> obtenerContratistaOffline();

    @GET("api/inspeccion/obtenerTipoUbicacion_offline")
    Call<List<TipoUbicacionEntity>> obtenerTipoUbicacionOffline();

    @GET("api/inspeccion/obtenerLugar_offline")
    Call<List<LugarEntity>> obtenerLugarOffline();

    @GET("api/inspeccion/obtenerEmpresaMaestra_offline")
    Call<List<EmpresaMaestraEntity>> obtenerEmpresaMaestraOffline();


    @POST("api/inspeccion/ListarInspeccionDetalles_offline")
    Call<List<AsynInspeccionResponse>> listarInspeccionDetalles(@Body Map<String, Object> body);

    @POST("api/SBC/ListarSBCDetalles_offline")
    Call<List<AsynSbcResponse>> listarSbcDetalles(@Body Map<String, Object> body);

    @POST("api/Auditoria/ListarAuditoriaDetalles_offline")
    Call<List<AsynAutoriaResponse>> listaAuditoriaDetalles(@Body Map<String, Object> body);

    @GET
    Call<ResponseBody> downloadImageUrlSync(@Url String fileUrl);
}
