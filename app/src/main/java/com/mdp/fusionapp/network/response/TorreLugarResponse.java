package com.mdp.fusionapp.network.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TorreLugarResponse implements Serializable {

    @SerializedName("idTorre")
    private Integer idTorre;

    @SerializedName("Nombre")
    private String nombre;

    public Integer getIdTorre() {
        return idTorre;
    }

    public void setIdTorre(Integer idTorre) {
        this.idTorre = idTorre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return  nombre;
    }
}
