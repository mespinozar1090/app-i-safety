package com.mdp.fusionapp.network.reposiroty;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.mdp.fusionapp.database.repository.LineamientoRepository;
import com.mdp.fusionapp.database.repository.LineamientoSubRepository;
import com.mdp.fusionapp.network.response.AsynAutoriaResponse;
import com.mdp.fusionapp.network.response.AuditoriaResponse;
import com.mdp.fusionapp.database.entity.LineamientoEntity;
import com.mdp.fusionapp.database.entity.LineamientoSubEntity;
import com.mdp.fusionapp.network.response.Respuesta;
import com.mdp.fusionapp.network.response.detalle.DetalleAuditoriaResponse;
import com.mdp.fusionapp.network.service.APIService;
import com.mdp.fusionapp.utilitary.UtilMDP;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuditoriaRepository {

    private final String LOGGER = "AuditoriaRepository";
    private APIService apiService;

    private static AuditoriaRepository instancia;

    public AuditoriaRepository() {
    }

    public static AuditoriaRepository getInstance() {
        if (instancia == null) {
            instancia = new AuditoriaRepository();
        }
        return instancia;
    }

    public MutableLiveData<List<AuditoriaResponse>> listaAuditoria(HashMap hashMap, Context context) {
        final MutableLiveData<List<AuditoriaResponse>> auditoriaMutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<AuditoriaResponse>> listCall = apiService.listaAuditoria(hashMap);

        listCall.enqueue(new Callback<List<AuditoriaResponse>>() {
            @Override
            public void onResponse(Call<List<AuditoriaResponse>> call, Response<List<AuditoriaResponse>> response) {
                auditoriaMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<AuditoriaResponse>> call, Throwable t) {
                Log.e(LOGGER, ":" + t.getMessage());
                auditoriaMutableLiveData.setValue(null);
            }
        });
        return auditoriaMutableLiveData;
    }

    public MutableLiveData<List<LineamientoEntity>> obtenerLineaminetos(LineamientoRepository lineamientoRepository) {
        final MutableLiveData<List<LineamientoEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<LineamientoEntity>> listCall = apiService.obtenerLineaminetos();

        listCall.enqueue(new Callback<List<LineamientoEntity>>() {
            @Override
            public void onResponse(Call<List<LineamientoEntity>> call, Response<List<LineamientoEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    lineamientoRepository.deleteAll();
                    for (int i = 0; i < response.body().size(); i++) {
                        lineamientoRepository.insert(response.body().get(i));
                    }
                    mutableLiveData.setValue(lineamientoRepository.findAll());
                }
            }

            @Override
            public void onFailure(Call<List<LineamientoEntity>> call, Throwable t) {
                mutableLiveData.setValue(lineamientoRepository.findAll());
            }
        });
        return mutableLiveData;
    }


    public MutableLiveData<List<LineamientoSubEntity>> obtenerLineaminetosSub(LineamientoSubRepository lineamientoSubRepository) {
        final MutableLiveData<List<LineamientoSubEntity>> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<List<LineamientoSubEntity>> listCall = apiService.obtenerLineaminetosSub();

        listCall.enqueue(new Callback<List<LineamientoSubEntity>>() {
            @Override
            public void onResponse(Call<List<LineamientoSubEntity>> call, Response<List<LineamientoSubEntity>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    lineamientoSubRepository.deleteAll();
                    for (int i = 0; i < response.body().size(); i++) {
                        lineamientoSubRepository.insert(response.body().get(i));
                    }
                    mutableLiveData.setValue(lineamientoSubRepository.findAll());
                }
            }

            @Override
            public void onFailure(Call<List<LineamientoSubEntity>> call, Throwable t) {
                mutableLiveData.setValue(lineamientoSubRepository.findAll());
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<Respuesta> insertaAuditoria(HashMap hashMap, Context context) {
        final MutableLiveData<Respuesta> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<Respuesta> listCall = apiService.insertaAuditoria(hashMap);

        listCall.enqueue(new Callback<Respuesta>() {
            @Override
            public void onResponse(Call<Respuesta> call, Response<Respuesta> response) {
                mutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<Respuesta> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<DetalleAuditoriaResponse> obtenerRegistroAuditoria(Integer idAuditoria) {
        Log.e("idAuditoria", " : " + idAuditoria);
        final MutableLiveData<DetalleAuditoriaResponse> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<DetalleAuditoriaResponse> listCall = apiService.obtenerRegistroAuditoria(idAuditoria);

        listCall.enqueue(new Callback<DetalleAuditoriaResponse>() {
            @Override
            public void onResponse(Call<DetalleAuditoriaResponse> call, Response<DetalleAuditoriaResponse> response) {
                Log.e("success", " : " + response.message());
                mutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<DetalleAuditoriaResponse> call, Throwable t) {
                Log.e("error", " : " + t.getMessage());
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<Respuesta> editarAuditoria(HashMap hashMap) {
        final MutableLiveData<Respuesta> mutableLiveData = new MutableLiveData<>();
        apiService = UtilMDP.getRetrofitBuilder(UtilMDP.BASE_URL).create(APIService.class);
        Call<Respuesta> listCall = apiService.editarAuditoria(hashMap);

        listCall.enqueue(new Callback<Respuesta>() {
            @Override
            public void onResponse(Call<Respuesta> call, Response<Respuesta> response) {
                Log.e("success", " : " + response.body());
                mutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<Respuesta> call, Throwable t) {
                Log.e("error", " : " + t.getMessage());
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }

}

